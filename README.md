Super Mario World for MSX
=========================

A warning for fellow coders!!
-----------------------------

Please note that page0 is full. It is no longer possible to add code or variables
to this particular page meaning that engine.asm and angine2.asm are off limits!
The same goes for enginepage3! If you like to add, define variables or need code 
to run perse from RAM stuff it in enginepage32.asm. There is plenty of space left
in there.

Memory layout of code
---------------------

Because of ROM size restrictions. The game engine and code that runs in RAM has
been splitted up in multiple files. The gameengine itself starts at adress $03e
and continues all the way to $4000. From there the engine continues at page 3 and
from there at adress $d880. See figure below on how the files stack up in RAM

---------------------------- Page 0
$003e - engine.asm          |
        variables.asm       |
        engine2.asm         |
---------------------------- Page 1/2
ROM Area/MAP Area           |
---------------------------- Page 3
$c000 - enginepage3.asm     |
        variablespage3.asm  |
$D880 - enginepage32.asm    |
        globalvars.asm      |
$FFDD -  Stack              |
----------------------------


Level editing information
-------------------------

SMW maps overview: <http://www.mariouniverse.com/maps/snes/smw>

For editing the tile set it is recommended to use Paint Shop Pro 6 for Windows.
For Mac OS X Asesprite is a nice editor, Gimp also works, and Photoshop is also
said to be quite good for pixel art.

When editing tiles, you have to take care to not exceed 16 colours maximum. Use
the colour counting function in the editor to check this. It is also a good idea
to restrict the RGB values to the following 8 values the MSX is capable of
displaying:

    0, 36, 73, 109, 146, 182, 219, 255

It’s often useful to enable an 8x8 grid, the shortcut for this in PSP6 is
CTRL-ALT-G. Note that the native grid of SMW on the SNES is 16x16, so sometimes
it may also be handy to enable this grid size.

Once the tiles are ready, they can be converted to MSX with BMP2MSX for Windows.
To run BMP2MSX on Mac OS X or Linux you can use Wine. In BMP2MSX you define the
order of the palette. The fixed colours which you can’t modify are:

    3, (4) 7, 9, 10, 13, 14 and 15

By convention, colour 0 is used for the primary background colour.

Tile maps are edited using mapedit for Windows. To run mapedit on Mac OS X you
can use Wine, however you must set the DirectDrawRenderer to GDI with a register
key (see mapedit.command for details). Mapedit does not appear to work well in
VirtualBox.

The maximum tile map size is 32K. In other words: H x W <= 32768. Additionally,
the length of the tile set filename must be 8 characters or less.

The tiles have special meaning based on their position in the tile set:

  * 0 - 191 Background (186, 187, 188, 189, 190, 191 fixed animation)
  * 192 - 207 Do not change, location is fixed.
  * 192 - 233 Solid foreground (Mario can not pass through)
  * 234 - 255 Soft foreground (Mario can pass through and jump on top of it)
  * 242 - 246 Slanted tiles (optional, bottom left -> top right)
  * 247 - 255 Slanted tiles (optional, top left -> bottom right)


Development information
-----------------------

Please note that the smw engine is slow compared to other projects as we had to create
a basic like language to do the object coding. Please use the commands from the internal
library as much as possible to prevent stray code in the engine using up even more
ROM/RAM space than we can spare.

We are still looking for fellow coders who can optimise the engine's speed.
Otherwise slowdowns on z80 are inevitable.


Getting the source code
-----------------------

The SMW project uses the Mercurial DVCS for source control. To get a copy of the
source code you need to install a Mercurial client:

  * SourceTree
    * Visual client for Windows and Mac. User-friendly.
    * Website: <https://www.sourcetreeapp.com/>
  * TortoiseHg
    * Visual client for Windows, Linux and Mac. Has advanced functions.
    * Website: <http://tortoisehg.bitbucket.org/>
  * Mercurial command line client
    * This is the core client of Mercurial. Also included in the visual clients.
    * Website: <https://mercurial.selenic.com/>

Now a short introduction on how to get started.

In a DVCS, every user has a complete working copy and all of the history on his
machine. You can add a set of changes to the history with `commit`. You can
change the working copy to an older or newer version with `update`.

As for sharing your changes, you can send your commits to Bitbucket with `push`,
and you can retrieve the latest commits with `pull`. Through this mechanism
everyone’s copies stay in sync with each other. After you’ve pulled, if you have
not made any un-pushed commits you can use `update` (often done together with
the pull), otherwise you need to use `merge` to combine the branches of history.

To get an initial working copy you need to use `clone`. Select the clone command
in the visual client and enter the following source URL:

    https://bitbucket.org/noramos/smw

It will create a new `smw` directory in the current directory, and inside it
will put a complete working copy of the latest version of the project, as well
as a hidden .hg directory containing all the history information in a compressed
format (do not touch it).

Now you can edit, add and remove files. When you view the working copy in the
visual clients, it will tell you which files have changed, and you can also see
how they were changed, revert them back to their original state, et cetera.

When you have a few changes ready, you can `commit` them. Before committing, it
is recommended to `pull` and `update` to the latest changes first. Then, select
the files you want to commit and press the commit button and you will be
prompted to enter a commit description. Enter a short summary of the changes, it
will help others see what and why it has changed. Then press commit and the
changes are safely recorded in the history.

Note, the first time you commit it will tell you that you need to configure your
user name, the format is `Firstname Lastname <email@example.com>`. This is
recorded immutably so be sure to set it correctly :).

When you’re ready to share your changes with the others, use the `push` command
to send it back to Bitbucket, so that the others can retrieve them. If you want
to avoid merges, it is better to not to wait too long to push after you’ve
committed.

Those are the basics. Some final notes: to prevent the repository from growing
unnecessarily large, avoid adding software, especially big ones like PSP6, also
do not add any non-free software. Additionally, if you have any files or
directories in your working copy which you want to ignore, you can edit the
`.hgignore` file.

For more information, here are some tutorials:

  * [Mercurial Tutorial](https://mercurial.selenic.com/wiki/Tutorial)
  * [Joel Spolsky’s Hg Init](http://hginit.com/)
  * [TortoiseHg getting started](http://tortoisehg.readthedocs.org/en/latest/quick.html)
  
Building The ROM
----------------

To compile the source into a working rom you need to set your path to the sjasmbinaries.
These binaries are located in the /toolchain/bin/yourarchitecture folder. If you are on linux
you could link the compiler to your /usr/bin or set the path in bash. Then run the 1.command
If you have set everything correct the compiler will output the rom file.

If you are on Windows. Make sure to set the path to the correct binary. In your case located
at \toolchain\Windowsx86 and run 1.bat

Making changes to the graphics or source has the same rules: run 1.command from linux and 1.bat
in windows to compile new graphics. If you plan to make source changes on linux you will need
wine. So far any wine version works but is required to run bmp2msx.exe and pack.exe.

The SCC files are build using special tools. You can only get them at Artrag.

Note to linux users: you will have to build the sjasmbinary yourself. You only require glibc headers.


