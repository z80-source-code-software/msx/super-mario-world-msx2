What and how.

These are 3 converters build by Grauw for the smw project which are capable of converting raw csv made by tiled to raw mapdata and the other way around.

Building the source:

use G++ or clang and build the binaries on your machine of choice

Usage:

There are 3 tools present. 
csv2asm = Convert csv export data from tiled to assembly file which can directly be inserted into source. Use the included script to write output to file.
csv2stg = Convert csv export data from tiled directly to a stg file which can then be packed and used into the engine
stg2csv = Convert a stg fil made in mapedit.exe to a csv output. You can copy/paste this output into a new CSV base tmx file. You can extract the
mapsize and tiledata by looking at the lev file with a hexeditor.


for example: csv2stg mymap.csv mymap.stg

IMPORTANT!!!:

Please keep the toolchain folder free from any extra data! link or set a path to the binaries so you can work from your map folder. Thank you!


