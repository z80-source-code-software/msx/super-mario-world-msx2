#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void process(istream& in, ostream& out)
{
  string csvline;
  while (!in.eof())
  {
    getline(in, csvline);
    if (!csvline.empty())
    {
      out << "\tdb " << csvline << "\n";
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    process(cin, cout);
  }
  else if (argc == 2)
  {
    ifstream fin;
    fin.open(argv[1]);

    process(fin, cout);

    fin.close();
  }
  else if (argc == 3)
  {
    ifstream fin;
    ofstream fout;
    fin.open(argv[1]);
    fout.open(argv[2]);

    process(fin, fout);

    fin.close();
    fout.close();
  }
  return 0;
}
