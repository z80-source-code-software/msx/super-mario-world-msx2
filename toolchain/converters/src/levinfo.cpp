#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdio>
//#include <iomanip>
//#include <locale>

using namespace std;


//fetch info starting @ 0x17

void process(istream& in, ostream& out)
{
    string line;
    string chr;
    int x = 0;
    int y = 0;
    char l; //yes assembly z80 style this is the only way my brains will not refuse
    char h;
    char e;
    char d;
    int loop = 0;
    
  if (in.eof() == true || in.fail() == true)
  {
      return;
  }    
     
    while (loop < 16) //skip mapname and all that stuff and fetch the gfxdata string
    {
        in.get();
         loop = loop + 1;                
    }
     
     
    while (loop < 23)
    {
        chr = in.get(); //fetch bytes
        line.append(chr); 
        //in.get(); //skip bytes until reaching the x y size area of file
        loop = loop + 1;  
    }

                    //fetch all data into bytes
                    l = in.get(); //x
                    h = in.get();
                    in.get();
                    in.get();
                    e = in.get(); //y
                    d = in.get();
                    //combine raw data to int
                    x = (h * 256) + l;
                    y = (d * 256) + e;
                    
                                    
                 //   out << static_cast < int > (stof(x));    
                   out << "gfx=" << line << " x=" << x << " y=" << y; 
                    
                    
                    return;
  
}


int main(int argc, char* argv[])
{ 
    
 
    
  if (argc < 2)
  {
    cerr << "levinfo X x Y reader. input = *.lev file outputs X and Y size of maplevel\n";
    return 1;
  }

  
  if (argc == 2)
  {
  
    ifstream fin;
    fin.open(argv[1]);

    process(fin, cout);

    fin.close();
    
    cerr << "\n"; //trow whiteline to make output look nice
    
        return 0;
  }
  
  cerr << "ERROR: too many arguments\n";
      
  
}
