//lev to tmx converter. This tool basicly reads the contents of a lev file and extracts the x and y sizes. It outputs the gfx data as wel so you know which tileset to pick in the
//tiled editor after opening the output. Then it reads the raw stg data and writes it into the tmx data template which is output as a new file.

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdio>
//#include <iomanip>
//#include <locale>

using namespace std;


void getlevinfoonly(istream& in, ostream& out)
{
    string line;
    string chr;
    int x = 0;
    int y = 0;
    char l; //yes assembly z80 style this is the only way my brains will not refuse
    char h;
    char e;
    char d;
    int loop = 0;
    
  if (in.eof() == true || in.fail() == true)
  {
      return;
  }    
     
    while (loop < 16) //skip mapname and all that stuff and fetch the gfxdata string
    {
        in.get();
         loop = loop + 1;                
    }
     
     
    while (loop < 23)
    {
        chr = in.get(); //fetch bytes
        line.append(chr); 
        //in.get(); //skip bytes until reaching the x y size area of file
        loop = loop + 1;  
    }

                    //fetch all data into bytes
                    l = in.get(); //x
                    h = in.get();
                    in.get();
                    in.get();
                    e = in.get(); //y
                    d = in.get();
                    //combine raw data to int
                    x = (h * 256) + l;
                    y = (d * 256) + e;
                    
                                       
                   cerr << "gfx=" << line << " x=" << x << " y=" << y; //trow lev info into terminal
                    
                    return;
  
}



//fetch info starting @ 0x17

void getlevinfo(istream& in, ostream& out)
{
    string line;
    string chr;
    int x = 0;
    int y = 0;
    char l; //yes assembly z80 style this is the only way my brains will not refuse
    char h;
    char e;
    char d;
    int loop = 0;

  //define data part for writing into file  
    
    string part1 = R"(<?xml version="1.0" encoding="UTF-8"?>
<map version="1.2" tiledversion="1.2.0" orientation="orthogonal" renderorder="left-down" width=")";
    string part2 = R"(" height=")";
    string part3 = R"(" tilewidth="8" tileheight="8" infinite="0" nextlayerid="" nextobjectid="">
 <tileset firstgid="1" name="grapx" tilewidth="8" tileheight="8" tilecount="" columns="">
  <image source="grapx.bmp" width="" height=""/>
 </tileset>
 <layer id="1" name="Tile Layer 1" width=")";
 string part4 = R"(" height=")";
 string part5 = R"(">
  <data encoding="csv">)"; //remember to add a \n thingy here

    
  if (in.eof() == true || in.fail() == true)
  {
      return;
  }    
     
    while (loop < 16) //skip mapname and all that stuff and fetch the gfxdata string
    {
        in.get();
         loop = loop + 1;                
    }
     
     
    while (loop < 23)
    {
        chr = in.get(); //fetch bytes
        line.append(chr); 
        //in.get(); //skip bytes until reaching the x y size area of file
        loop = loop + 1;  
    }

                    //fetch all data into bytes
                    l = in.get(); //x
                    h = in.get();
                    in.get();
                    in.get();
                    e = in.get(); //y
                    d = in.get();
                    //combine raw data to int
                    x = (h * 256) + l;
                    y = (d * 256) + e;
                    
                                       
                   cerr << "gfx=" << line << " x=" << x << " y=" << y; //trow lev info into terminal
                   out << part1 << x << part2 << y << part3 << x << part4 << y << part5 << "\n"; // << part4; comes in the end after parsing the csv data
                    
                    return;
  
}


void writecsvdata(istream& in, ostream& out)
{
 
string part6 = R"(</data>
 </layer>
</map>)"; //end of file
    
    
bool first = true;
  while (!in.eof())
  {
    int value = in.get();
    if (!in.eof())
    {
      if (first)
      {
        first = false;
      }
      else
      {
        out << ",";
      }
      out << value + 1;
    }
  }
  out << "\n" << part6;
    
    
    
    
}



int main(int argc, char* argv[])
{ 

 
    
  if (argc < 2)
  {
    cerr << "LEV to TMX converter.\n" << "Usage: " << argv[0] << " in.lev\n";
    return 1;
  }

  
  if (argc == 2)
  {
  
    std::string arg1(argv[1]);
      
    if (arg1.compare("-h") == 0)
    {
        cerr << "Kickass LEV to TMX converter by Daemos with many many many thanks to Grauw!\n Usage:" << argv[0] << " [options] in.lev\n\nTool will automaticly generate a working *.TMX file with the same name as the input file\n\nOptions:\n\n-h = show this help and exit\n-i = show only the mapinfo for the lev file (process nothing)\n";
        return 0;
    }
    
      
//generate output filename and properly set its extension      
  string output = argv[1];
        output.pop_back();
        output.pop_back();
        output.pop_back();
        output.pop_back();
        string input = output; //define input filename for matching stg file
  output.append(".tmx");
  input.append(".stg");
      
  ifstream fin;
  ofstream fout;
  fin.open(argv[1]);
  
    //check if file exsist
    if(!fin)
    {
        cerr << "ERROR: the file you specified does not exsist!\n";
        return 0;
    } 
  
  
  
  fout.open(output);

  getlevinfo(fin, fout);

  fin.close();
//  fout.close();
  
  fin.open(input);
    //check if file exsist
    if(!fin)
    {
        cerr << "\nERROR: the stg file needed to create the tmx file is missing! your TMX file will be disfunctional!\n";
        fout.close();  //close the fout file since there is nothing to write to     
        return 0;
    } 
    
  
  writecsvdata(fin, fout);

  fin.close();
  fout.close();
  
  
  
    cerr << "\n"; //trow whiteline to make output look nice
    
        return 0;
  }
  
      std::string arg1(argv[1]);
  
    if (arg1.compare("-i") == 0)
    {
        cerr << "Displaying lev info:\n";
        
        ifstream fin;
        ofstream fout;
        fin.open(argv[2]);

        getlevinfoonly(fin, fout);

        fin.close();        

        cerr << "\n"; //trow whiteline to make output look nice        
        
        return 0;
    }
  
  
  cerr << "ERROR: too many arguments\n";
      
  
}
