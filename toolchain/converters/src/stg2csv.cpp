#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void process(istream& in, ostream& out)
{
  bool first = true;
  while (!in.eof())
  {
    int value = in.get();
    if (!in.eof())
    {
      if (first)
      {
        first = false;
      }
      else
      {
        out << ",";
      }
      out << value + 1;
    }
  }
  out << "\n";
}

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    cerr << "Usage: " << argv[0] << " in.stg [out.csv]\n";
    return 1;
  }

  if (argc == 2)
  {
    ifstream fin;
    fin.open(argv[1]);

    process(fin, cout);

    fin.close();
  }
  else
  {
    ifstream fin;
    ofstream fout;
    fin.open(argv[1]);
    fout.open(argv[2]);

    process(fin, fout);

    fin.close();
    fout.close();
  }

  return 0;
}
