//Version2 now has layer support. Many thnx again to grauw for writing the code I couldn't understand and helping me out.
//Version3 sorts the x entries in the object layer. Currently only 1!!! object layer is supported
//version3.1 supports sjasmplus fully

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <vector>
using namespace std;

void process(istream& in, ostream& out)
{
	vector<string> layers;

	string line;
	while (getline(in, line))
	{
		if (line.find("<data ") != string::npos)
		{
			ostringstream sout;

			while (getline(in, line) && line.find("</data>") == string::npos)
			{
				string value;
				istringstream sin(line);
				while (getline(sin, value, ','))
				{
					if (!value.empty())
					{
						sout << static_cast<char>(stoi(value) - 1);
					}
				}
			}

			layers.push_back(sout.str());
		}
	}

	//error detection in case of faulty tmx file
	if (!layers.size())
    {
        cerr << "ERROR: Cannot find TMX,CSV data layers: Your TMX file is either in the wrong format, corrupt or missing.";
        return;
    }
    
	
	
	
	for (int i = 0; i < layers[0].size(); i++)
	{
		char value(0);
		for (string layer : layers)
		{
			if (layer[i])
			{
				value = layer[i];
			}
		}
		out << value;
	}
}




void processl(istream& in, ostream& out)
{
    
 	vector<string> objectdata;
    
    
      out << "\tdb 00\t\t\t:\tdw 000\t:\tdw 000,000\t;x=000 this defines the leftborder of the levelobjectlist\n\n";  
  
      

   
      
  string line;
  while (getline(in, line))
  {
    
      
    if (line.find("<object ") != string::npos)
    {
    
        
      ostringstream sout; //redefine here    
        
    int idstartpos = line.find(" type=\"") + 7;
    int idendpos = line.find("\"", idstartpos);
    string id = line.substr(idstartpos, idendpos - idstartpos);        
        
    idstartpos = line.find(" x=\"") + 4;
    idendpos = line.find("\"", idstartpos);
    string x = line.substr(idstartpos, idendpos - idstartpos);        

    idstartpos = line.find(" y=\"") + 4;
    idendpos = line.find("\"", idstartpos);
    string y = line.substr(idstartpos, idendpos - idstartpos);      

    idstartpos = line.find(" name=\"") + 7;
    idendpos = line.find("\"", idstartpos);
    string name = line.substr(idstartpos, idendpos - idstartpos);      
      
    
/*      sout << "\tdb available\t|\tdw " << setw(3) << setfill('0') << static_cast < int > (stof(x)/8 + 0.5f) << "\t|\tdw " << setw(3) << setfill('0') << static_cast < int > (stof(y)/8 + 0.5f) << "," << setw(3) << setfill('0') << id << "\t;" << name << "\n";      */
      sout << setw(3) << setfill('0') << static_cast < int > (stof(x)/8 + 0.5f) << "\t:\tdw " << setw(3) << setfill('0') << static_cast < int > (stof(y)/8 + 0.5f) << "," << setw(3) << setfill('0') << id << "\t;" << name << "\n";
      
      			objectdata.push_back(sout.str());
      
      
    }
  }
  

  
  //insert find and sort code here we now have all the layer data in each vector.
  string xvalue;
  string next;
  int xv; //convert into these ints for compare
  int found = 0;
  int boundary = 0; //lowest boundary of number found
 
  for (int i = 0; i < objectdata.size(); i++)
  {
 
        int xn = 999; //highest number possible
        
  for (int i = 0; i < objectdata.size(); i++)
  {
  

      
      xvalue = objectdata[i].substr (0,3); //put first value in next then compare in a second loop
      xv = (stof(xvalue)); //convert to value
      //catch duplicate x problems
            if (xv == xn)
            {
                cerr << "WARNING: duplicate X object detected! This means that one of both objects will not work on your map.\n";
                    out << objectdata[i]; //out data anyway just warn the user of this problem. This warning will point him/her to the problem too :)
                    break;
            }
      
      
            if (xv < xn && xv > boundary)
            {
                found = i; //found lower number keep going until lowest number is found
                xn = xv; //search next lower number
            }
                  
  }


            out << "\tdb available\t:\tdw " << objectdata[found]; 
    
            boundary = xn; //set this number as the lowest boundary        
 
                             
                               
                   
  }
  

  
  
  out << "\n\tdb 00\t\t\t:\tdw 999\t:\tdw 000,000\t;x=999 this defines the rightborder of the levelobjectlist\n";
  
}



int main(int argc, char* argv[])
{ 
    
 
    
  if (argc < 2)
  {
    cerr << "Kickass TMX (CSV left down) to raw stg map converter V3.1, use -h for help\nUsage: " << argv[0] << " in.tmx out.stg\n";
    return 1;
  }

  if (argc == 2 && argc != 4)
  {
   
    std::string arg1(argv[1]);   
      
    if (arg1.compare("-h") == 0)
    {
        cerr << "Kickass TMX (CSV left down) to raw stg map converter by Daemos and Grauw V3.1\nUsage: " << argv[0] << " in.tmx out.stg or out.asm if using the -l command\n\n\nCommands:\n-h = display this help tekst and exit\n-l = extract and generate objectslist from the tmx objects layer\nNo command will generate a working stg mapfile from the tmx file\n\nPlease note that the TMX file must be saved in the CSV form!\n";
        return 0;
    }

        if (arg1.compare("-l") == 0)
        {
            return 0;
        }
            
            
    ifstream fin;
    fin.open(argv[1]);

    process(fin, cout);

    fin.close();
    
    cerr << "\n"; //trow whiteline to make output look nice
    
        return 0;
    
  }
  else if (argc == 3)  
  {

    std::string arg1(argv[1]);
         
     if (arg1.compare("-l") == 0) 
    
    {
        
    ifstream fin;
    fin.open(argv[2]);

    processl(fin, cout);

    fin.close();
    
    cerr << "\n"; //trow whiteline to make output look nice        
    
    return 0;
    
    }
  
      if (arg1.compare("-h") == 0)
    {
        cerr << "Kickass TMX (CSV left down) to raw stg map converter by Daemos and Grauw\nUsage: " << argv[0] << " in.tmx out.stg or out.asm if using the -l command\n\n\nCommands:\n-h = display this help tekst and exit\n-l = extract and generate objectslist from the tmx objects layer\nNo command will generate a working stg mapfile from the tmx file\n\nPlease note that the TMX file must be saved in the CSV form!\n";
        return 0;
    }
  
  ifstream fin;
  ofstream fout;
  fin.open(argv[1]);
  fout.open(argv[2]);

  process(fin, fout);

  fin.close();
  fout.close();

  return 0;
  }
  else if (argc == 4)
  {
    std::string arg1(argv[1]);
      
    
        if (arg1.compare("-h") == 0)
    {
        cerr << "Kickass TMX (CSV left down) to raw stg map converter by Daemos and Grauw\nUsage: " << argv[0] << " in.tmx out.stg or out.asm if using the -l command\n\n\nCommands:\n-h = display this help tekst and exit\n-l = extract and generate objectslist from the tmx objects layer\nNo command will generate a working stg mapfile from the tmx file\n\nPlease note that the TMX file must be saved in the CSV form!\n";
        return 0;
    }
    
     if (arg1.compare("-l") == 0) 
    
    {    
  ifstream fin;
  ofstream fout;
  fin.open(argv[2]);
  fout.open(argv[3]);

  processl(fin, fout);

  fin.close();
  fout.close();

  return 0;
    }

     
  }  
      
  
}
