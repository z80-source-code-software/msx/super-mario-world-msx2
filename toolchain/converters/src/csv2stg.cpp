#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

void process(istream& in, ostream& out)
{
  string line;
  while (!in.eof())
  {
    getline(in, line, ',');
    stringstream sin(line);

    while (!sin.eof())
    {
      string value;
      getline(sin, value);
      if (!value.empty())
      {
        out << static_cast<char>(stoi(value));
      }
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    cerr << "Usage: " << argv[0] << " in.csv out.stg\n";
    return 1;
  }

  ifstream fin;
  ofstream fout;
  fin.open(argv[1]);
  fout.open(argv[2]);

  process(fin, fout);

  fin.close();
  fout.close();

  return 0;
}
