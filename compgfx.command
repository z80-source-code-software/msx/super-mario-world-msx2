#!/bin/bash


#main command utility for gfx conversion using sjasmplus

wine BMP2MSX.exe grapx.bmp


sjasmplus --msg=err --dirbol --fullpath ../../../sjasm_preamble.asm conversion.asm
#tniasm conversion.asm
cp grapx.sc4 ../../../level_editor/_levels/gfx7-01a.sc4
rm grapx.sc4

#gfx packs from here

cp grapx.SC2 packedgrpx/
cd packedgrpx
sjasmplus --msg=err --dirbol --fullpath ../../../../sjasm_preamble.asm packgrpx.asm
#tniasm packgrpx.asm
wine pack grpx
rm grpx
rm grapx.SC2
cd ..
