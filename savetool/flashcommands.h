eraseflashrom: 

 ;   di    
    
    ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    
    ;erase block command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$80
    ld ($4aaa),a
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$30
    ld ($4000),a    
    
 ;   ei
    
    
    ld    a,$FF   ; // This is the value of the flashROM memory after erasing it.
    ld    de,$4555   ; // Any address in the flashROM area should work.
    call    check    ;// Waits until the command has finished, and checks if there was any error (do not use wait loops!)
    
    
    ;read command
    ld a,$f0
    ld ($4000),a      
    

  
    ret


    
;-------------------------------------------------------------------------------
; Check flash command
; Out: Cy = error
;-------------------------------------------------------------------------------
check:
        push    bc
        ld    c,a
Check1:
        ld    a,(de)
        xor    c
        jp    p,Check2
        xor    c
        and    $20
        jp    z,Check1
        ld    a,(de)
        xor    c
        jp    p,Check2
        scf
Check2:
        pop    bc    
    
        ret


    
;usage: Just like ldir
ldirtoflashrom:
 
 
.writeloop: ;loop and reinitiate each byte increase write adress at each write 
 
  ;  di
 
    ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    ;write command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$a0
    ld ($4aaa),a

    ld a,(hl)
    ld (de),a

  ;  ei
    
.wait:    ;wait for successfull write
    ld a,(de)
    cp (hl)
    jp nz,.wait
    
    inc hl
    inc de
    dec bc
    
    ld a,b
    or c
    
    jp nz,.writeloop
  

 
    ;read command
    ld a,$f0
    ld ($4000),a   

    	
    ret 


    
    
