 ;special program to save smw file to disk and the other way around. 
 ;uses dos2 routines so it only works in dos2
 fname "savetool.com"
 
 org $100 ;dos header
 
 
  ;fetch parameters first
  ld hl,$80
  ld a,(hl) ;get size of stuff
  and a
  jp z,showhelp
  
  
.readparamloop:
  ld a,(hl)
  inc hl
  and a
  jp z,nothing ;end of string
  cp "/"
  jp z,checkparam
  jp .readparamloop
  

 
 ret ;quit
 

 
loadtoflash:

    call fetchfilehandlenonewfile ;get filedata if non exsistent say goodbye

    ;copy filedata to ram
    call copyfiledatatoram
    ;check header of filedata
    ld hl,savedataheader
    ld de,compareheader
.getheaderloop:
    ld a,(hl)
    ld c,a
    inc hl
    ld a,(de)
    inc de
    cp c
    jp nz,trowheadererror
    cp "e" ;keep comparing until last byte
    jp nz,.getheaderloop
    
    ;file is good than we can start reading the file into the flashrom
   
    ;first get primary and secondary slot data of the current slot in page 2
    call getslot
    ;then after we got getslot we are going to search for the unique header thats in our game
    call searchslot
    jp nc,nocartfound      
    
    
    di ;??? yes we must keep the interrupts off during the entire flash cycle. It seems that the nextor/dos2 int handler does something that this program does not like at all
    ;what is even more bizar is that I cannot reproduce this problem in openmsx!
    
    ;first perform a eraseflash
    ld a,$F8
    ld ($5000),a
    call eraseflashrom

    ld hl,savedata
  
    
    ld bc,globalvarsize+1
    ld de,$4000
    push hl
    call ldirtoflashrom
    pop hl
    ld de,globalvarsize+1
    add hl,de
    
    ld a,$F9
    ld ($5000),a    
    
    ld bc,globalvarsize+1
    ld de,$4000
    push hl
    call ldirtoflashrom
    pop hl  
    ld de,globalvarsize+1
    add hl,de    

    ld a,$FA
    ld ($5000),a    
    
    ld bc,globalvarsize+1
    ld de,$4000
 ;   push hl
    call ldirtoflashrom
 ;   pop hl  
   
    ;done flashing quit system close file handle and restore slot
    xor a
    ld ($5000),a ;restore mapper bank
    
    
    call restoreslot
    
    ei
    
    call closefile
    
    ld de,allgood
    ld c,9
    call 5    
    
    

    ret ;quit

    
allgood: db "DONE: Flashing to ROM completed without errors. Enjoy your game",$24    
fileexsists: db "WARNING: file allready exists: overwrite? Y= yes N = no",13,10,$24


trowfilexisterror:

    ld de,fileexsists
    ld c,9
    call 5 
 
.looperror: 
 
    ld b,5
  
    in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A    
    
    bit 6,a
    ret z
    
    ld b,4
  
    in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A    
    
    bit 3,a
    jp nz,.looperror
    
    inc sp
    inc sp
    
    ret ;quit
    


    
savetofile:

    call fetchfilehandle
    ;either something went wrong or we return here and can start our search for the smw cart
    
    
    
    ;first get primary and secondary slot data of the current slot in page 2
    call getslot
    ;then after we got getslot we are going to search for the unique header thats in our game
    call searchslot
    jp nc,nocartfound


    call copyflashdatatoram ;copy the savegame to ram
    call restoreslot ;put slot back
    
    call writesavedatatofile
    
    call closefile ;wel zo netjes ;)

    ld de,allgood2
    ld c,9
    call 5     
    
    
    ret ;quit 

allgood2: db "DONE: Saving of game completed. Do not forget to reset your MSX!",$24      
    

trowheadererror:

 ;   call restoreslot
    call closefile
 
 
    ld de,headererror
    ld c,9
    call 5

    ret ;quit
    
    
    
copyfiledatatoram:


    ld de,savedataheader
    ld hl,$1000
    ld c,$48
    ld a,(filehandle)
    ld b,a
    call 5


    ret ;ret

    
    
    
writesavedatatofile:

    ld a,(filehandle)
    ld b,a
    ld de,savedataheader ;write to file with header on front
    ld hl,$1000
    ld c,$49
    call 5
    

    ret ;ret
 
 
copyflashdatatoram:

    ;set block and then copy 1k of each block to ram
    ld a,$F8
    ld ($5000),a
    
    ld de,savedata
    ld hl,$4000
    ld bc,globalvarsize+1
    ldir
    
    ld a,$F9
    ld ($5000),a
    
   ; ld de,savedata pointer stays
    ld hl,$4000
    ld bc,globalvarsize+1
    ldir    

    ld a,$FA
    ld ($5000),a
    
   ; ld de,savedata pointer stays
    ld hl,$4000
    ld bc,globalvarsize+1
    ldir     

    xor a
    ld ($5000),a ;restore block
    

    ret ;ret
 
 
 
 
nothing:


    ld de,shownothingerror
    ld c,9
    call 5
    ret ;quit   
    
nocartfound:

    call restoreslot ;fix the old slotsetting and put everything back the way it was

    ld de,nocarterror
    ld c,9
    call 5
    ret ;quit   


 
 
checkparam:

    ld a,(hl)
    cp "l"
    jp z,loadtoflash
    cp "s"
    jp z,savetofile
    ;support upper cases as well
    cp "L"
    jp z,loadtoflash
    cp "S"
    jp z,savetofile
    
    ld de,showparamerror
    ld c,9
    call 5
    ret ;quit    


;returns carry if smw rom is found
searchslot:

  xor a
  ld (searchpoint),a ;start at slot 0
  ld a,%10000000
  ld (searchpoint2),a ;reset expanded slot
  
.primaryslot:  
  
  ld a,(searchpoint)
  ld c,a
  ld b,0
  ld hl,EXPTBL
  add hl,bc
  ld a,(hl) ;read if expanded slot or not
  cp $80
  jp z,.expandedslot

  ld h,%01000000 ;page 1
  ld a,(searchpoint)
  call ENASLT

 ; call .compare  
 ; ret c
  
.continue:  
  
  ld a,(searchpoint) ;increase primary slot
  inc a
  cp 4
  ret nc ;done searching nothing found
  ld (searchpoint),a
  
  ld c,a
  ld a,%10000000
  or c
  ld (searchpoint2),a ;reset expanded slot  
  
  
  jp .primaryslot
  
  
  
.expandedslot:

    
  ld h,%01000000 ;page 1
  ld a,(searchpoint2)
  call ENASLT    

  call .compare
  ret c
  
  ld a,(searchpoint2)
  add a,4
  ld (searchpoint2),a
  and %00011100
  cp 16
  jp nc,.continue ;all 4 subslots searched continue where we left off  
  
  jp .expandedslot

  
.compare:

    ;set mapper to 0
      ;  xor a
    ;	ld		($9000),a


  ld hl,$5100 ;pointer of unique game cart
  ld de,header ;header for comparison
  ld b,0 ;must match in the end for lenght
.compareloop:  
  ld a,(hl)
  ld c,a
  ld a,(de)
  cp c
  inc hl
  inc de
  push af
  inc b
  pop af ;transfer flag
  jp z,.compareloop ;match found do next
  
  ld a,14
  cp b
  ret c
  
  xor a ;otherwise there is no carry flag set and the search must continue
  ret
  
    
    
;get the current slot of page2 so we can restore that later
getslot:    
 
	in a,($A8)
	RRCA			;move it to bit 0,1 of [Acc]
	RRCA
;	RRCA
;	RRCA
	AND	%00000011
	LD	C,A
	LD	B,0
	LD	HL,EXPTBL	;see if this slot is expanded or not
	ADD	HL,BC
	LD	C,A		;save primary slot #
	LD	A,(HL)		;See if the slot is expanded or not
	AND	$80
	OR	C		;set MSB if so
	LD	C,A		;save it to [C]
	INC	HL		;Point to SLTTBL entry
	INC	HL
	INC	HL
	INC	HL
	LD	A,(HL)		;Get what is currently output
				;to expansion slot register
    rrca
    rrca
	AND	%00001100
	OR	C		;Finally form slot address
;	LD	H,$80
	ld (romslot),a ;save obtained slot adress
	
	ret ;ret
	
restoreslot:

    ld a,(romslot)
    ld h,%01000000 ;page1
    call ENASLT

    ret ;ret

    
fetchfilehandlenonewfile:


    inc hl ;fetch filename and discard space
    inc hl

    ;filename is in pointer hl so we can directly trow the filename in the open filehandle routine
    ex de,hl
    ld c,$43
    xor a
    push de
    call 5 ;open file handle
    pop de
    jp nz,trowfilenotexisterror
    
    ;file handle in b now save it somewhere in RAM
    ld a,b
    ld (filehandle),a
    
    ;now search for a smw game in any slot. Find the unique smw header in the start of the rom
    
    ret ;ret    
    
    
    
    
;get the filename and see if the filename exsist on the system otherwhise trow ERROR or create the file 
;returns filehandle in ram and register b
fetchfilehandle:


    inc hl ;fetch filename and discard space
    inc hl

    ;filename is in pointer hl so we can directly trow the filename in the open filehandle routine
    ex de,hl
    ld c,$43
    xor a
    push de
    call 5 ;open file handle
    pop de
    jp nz,createfile
    
    ;file handle in b now save it somewhere in RAM
    ld a,b
    ld (filehandle),a
    
    
    ;overwrite question
    jp trowfilexisterror
    
    ;now search for a smw game in any slot. Find the unique smw header in the start of the rom
    
   ; ret ;ret
 
createfile:

    ;create the file instead
    xor a
    ld b,a
    ld c,$44
    call 5
    jp nz,trowfileerror
 
    ;file handle in b now save it somewhere in RAM
    ld a,b
    ld (filehandle),a 
 
    
    ret ;ret 

  
closefile:

    ld c,$45
    ld a,(filehandle)
    ld b,a
    call 5
    
    ret ;ret
 

trowfilenotexisterror:

    ld de,fileerror2
    ld c,9
    call 5
    
    inc sp
    inc sp
    ret ;quit  
 
    
trowfileerror:

    ld de,fileerror
    ld c,9
    call 5
    
    inc sp
    inc sp
    ret ;quit 
 
 
 
showhelp:

    ld de,helptext
    ld c,9
    call 5
    ret ;quit


include "flashcommands.h" ;flashcart needs flashing    
    
    

headererror: db "ERROR: savefile contains invalid savedata",$24    
    
nocarterror: db "ERROR: Could not find the game cartridge in any slot. Is it inserted?",$24
    
fileerror:  db "ERROR: savefile creation failed. filename too long or short?",$24  
fileerror2:  db "ERROR: file does not exsist",$24  
  
    
shownothingerror:   db "ERROR: no parameters given.",$24    
    

showparamerror: db "ERROR: parameter not recognized",$24    
    

 
helptext: db "savetool V1.0 by Daemos. Load and save your smw games from and to file." 
          db "Usage: /l = load to flashrom /s = save to file. Example savetool /s savegame.sav saves your savegame to the file savegame.sav",$24 


ENASLT:     	EQU	$24		;enable slot
EXPTBL:     	EQU	$FCC1  ;slot is expanded or not          
globalvarsize: equ $24E ;Size of the globalvariables. THIS MUST MATCH with endglobalvars IN THE SupermarioWorld.sym file!!!!         

          
flashrom?:  db 1 ;carnivore2 and mfr+sd 
filehandle: db 0 ;filehandle of the savefile  
romslot: db 0 ;current slot of page2  
searchpoint: db 0 ;slot search pointer 
searchpoint2: db 0 ;slot search pointer for expanded slots 
header:    db  "SupermarioWorld"
compareheader:  db "smwsave"
savedataheader: db "smwsave"
savedata: db 0
;EVERYTHING BELOW SAVEDATA WILL BE OVERWRITTEN!!!

 
