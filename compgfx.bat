BMP2MSX.exe grapx.bmp


sjasmplus --msg=err --dirbol --fullpath ..\..\..\sjasm_preamble.asm conversion.asm
copy grapx.sc4 ../../../level_editor/_levels/gfx7-01a.sc4
del grapx.sc4


copy grapx.SC2 packedgrpx/
cd packedgrpx
sjasmplus --msg=err --dirbol --fullpath ..\..\..\..\sjasm_preamble.asm packgrpx.asm
pack grpx
del grpx
del grapx.SC2
cd ..
