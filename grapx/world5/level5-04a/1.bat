BMP2MSX.exe grapx.bmp

tniasm conversion.asm
del tniasm.out
del tniasm.sym
del tniasm.tmp
del grapx.sc5
copy grapx.sc4 ..\..\..\level_editor\_levels\gfx5-04a.sc4
del grapx.sc4

rem *** this is the part that packs the grapx ***

copy grapx.sc2 packedgrpx
cd packedgrpx
tniasm packgrpx.asm
del tniasm.out
del tniasm.sym
del tniasm.tmp
pack grpx
del grpx
del grapx.SC2
cd..
