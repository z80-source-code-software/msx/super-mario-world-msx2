#!/bin/bash
cd -- "$(dirname "$0")"

echo --- Generating grapx.sc2 ---
wine BMP2MSX.exe grapx.bmp

rm grapx.SC5
rm bmp2msx.cfg

echo --- Creating grapx.sc4 ---
dd bs=1 skip=7047 count=32 < grapx.SC2 > grapx.sc4
dd bs=1 skip=7 count=2048 < grapx.SC2 >> grapx.sc4
dd bs=1 skip=8199 count=2048 < grapx.SC2 >> grapx.sc4

echo --- Copying grapx.sc4 to levels ---
cp grapx.sc4 ../../../level_editor/_Levels/gfx3-04a.sc4
rm grapx.sc4

echo --- Generating grpx.pck ---
dd bs=1 skip=7 count=2048 < grapx.SC2 > packedgrpx/grpx
dd bs=1 skip=8199 count=2048 < grapx.SC2 >> packedgrpx/grpx
dd bs=1 skip=7047 count=32 < grapx.SC2 >> packedgrpx/grpx
cd packedgrpx
wine pack grpx
rm grpx
cd ..

rm grapx.SC2
