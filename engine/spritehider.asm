;Spritehider. This routine checks if there is a sprite that
;matches the X coordinate of the hider object in screen.
;It then reads the VRAM data and writes back zero's at the appropiate pixel precise position

hidesprites:

  ld a,(spritehideractive)
  or 2
  and 1
  ret z ;not active


  ld hl,spritehidertable ;get table
  
  
hidespritesloop:  
  
  ld a,(hl)
  ld e,a
  inc hl
  ld a,(hl) ;read entry
  ld d,a
  or e
  ret z ;last entry means we are done
  
  inc hl
  ld a,(hl)
  ld (spritehidersize),a
  
  inc hl ;next entry
  
  push hl
  

;first get position in screen. If out of screen forget it

  ld hl,(camerax) ;where is the camera??
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8
  
  ld    a,(v9958found?)
  or    a
  jp    nz,.v9958found
  
  ld		a,(VDP_8+10)
  ld c,a
  ld b,0
  xor a
  sbc hl,bc
  ld bc,7
  add hl,bc
  ld b,h ;transfer hl->bc
  ld c,l


.v9958found: 
  ;add pixel precise position of camera
  ld		a,(VDP_8+21)
  ld c,a
  ld b,0
  xor a
  sbc hl,bc
  ld b,h ;transfer hl->bc
  ld c,l
 
    push de
    pop hl
    
  
  xor a
  sbc hl,bc ;positie van de sprite in ons beeld in hl
  
  ld a,h
  and a
  jp nz,.nextobject ;spritehider is out of screen so forget it
 ;result in l

  ld a,l
  ld (spritehiderx),a ;save x coordinate of spritehider in screen


  ld hl,spat+1 ;read spat X ;only current (invisible!!) spat that will become visible in the next frame

  ld b,32 ;32 sprites on screen
.readspriteloop:
    
    ld a,(spritehiderx)
    ld c,a
    ld a,(spritehidersize)
    add a,c ;set right boundary
    ld c,a
    ld a,(hl)
    ld (spatx),a
    cp c
    jp nc,.next ;out of right boundary
    
    ld a,(spritehiderx) ;reversed the order.
    ld c,a
    ld a,(hl)
    cp c ;left boundary
    
    
    ld e,b
    push de
    exx
    pop de ;put sprite number in alternate de
    call nc,handlespritehide
    exx
  
.next:
    ld de,4
    add hl,de ;next sprite
  

  djnz .readspriteloop
  
  
.nextobject:

   pop hl
   jp hidespritesloop
  

;ret

handlespritehide:


  ld hl,spritepositiontable
  ld a,32
  sub e
  add a,a ;*2 the table consists of WORDS!!

  ld e,a
  ld d,0
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) ;sprite position in VRAM almost obtained
  
  ld		hl,(invissprchatableaddress)
  add hl,de ;we got the address on VRAM
  
  
  ld a,1
  push hl
  call SetVdp_read
  pop de ;trow address in DE
  
 ld b,32
 ld hl,tempspritedata 
 ld c,$98
 call inix32 ;sprite data is now in RAM ready to get worked up :)


;branch to right boundary if sprite is on the right edge
    ld a,(spritehiderx)
    ld c,a
    ld a,(spritehidersize)
    add a,c ;set right boundary
    sub 16
    ld c,a
    ld a,(spatx)
    cp c
    jp nc,handlespritehideright ;out of right boundary

;get value from and table

  push de
  ld d,0
  ld a,(spritehiderx)
  ld c,a
  ld a,(spatx)
  sub c
  ld e,a ;we are lucky the distance from the 0 point is in e thats how we can get the right boundary to work
  ld hl,andtable
  add hl,de
;  inc hl
  ld a,(hl)
  cpl
  ld c,a ;this will be our and table value

;now work up sprite
; #0  #2
; #1  #3
;this means we need to work the sprite up in blocks of four bummer...

  ld de,andtable
  xor a
  sbc hl,de ;get andtable position
  ld a,l
  ld b,a ;store table position
  cp 8
  push bc
  jp c,.workitfully

  ld c,0
  call workspritestep1
  jp .workdone
  
.workitfully:
  call workspritestep1

.workdone:
  
  pop bc

  pop de ;get sprite VRAM position back


  ld a,b ;does second sprite need workup?
  cp 8
  jp c,.skipworkup2
  cp 15
  jp nc,.workupfully
  
  call workspritestep2
  jp .skipworkup2

.workupfully:

  ld c,0
  call workspritestep2  
  

.skipworkup2:


;write back new sprite
  
  push de
  pop hl
  
	ld		a,1
	call	SetVdp_Write

  ld hl,tempspritedata
  ld c,$98
  call outix32


  ret


workspritestep1:
  ld b,16
  ld hl,tempspritedata+16; work up #2 and #3 
.worksprite:
  ld a,(hl)
  and c
  ld (hl),a
  inc hl
  djnz .worksprite

  ret

workspritestep2:
;second workup
  ld b,16
  ld hl,tempspritedata 
.worksprite2:
  ld a,(hl)
  and c
  ld (hl),a
  inc hl
  djnz .worksprite2
  
  ret


handlespritehideright:

;get value from and table

  push de
  ld d,0
  ld a,(spritehiderx)
  ld c,a
  ld a,(spatx)
  sub c
  ld e,a ;we are lucky the distance from the 0 point is in e thats how we can get the right boundary to work
  ld a,(spritehidersize) ;now substract spritehidersize etc to get new zero coordinate on table
  sub 16-2 ;- 16 pixels - some compensation for whatever
  ld c,a
  ld a,e
  sub c
  ld e,a
  
  ld hl,andtable
  add hl,de
;  inc hl
  ld a,(hl)
;  cpl
  ld c,a ;this will be our and table value

;now work up sprite
; #0  #2
; #1  #3
;this means we need to work the sprite up in blocks of four bummer...

  ld de,andtable
  xor a
  sbc hl,de ;get andtable position
  ld a,l
  ld b,a ;store table position
  cp 8
  push bc
  jp c,.workitfully
  cp 16
  jp c,.workdone

  ld c,0
  call workspritestep1
  jp .workdone
  
.workitfully:
  call workspritestep1

.workdone:
  
  pop bc
  pop de ;get sprite VRAM position back


  ld a,b ;does second sprite need workup?
  cp 8
  jp c,.workupfully;skipworkup2
  cp 15
  jp nc,.workupfully
  
  call workspritestep2
  jp .skipworkup2

.workupfully:

  ld c,0
  call workspritestep2  
  

.skipworkup2:


;write back new sprite
  
  push de
  pop hl
  
	ld		a,1
	call	SetVdp_Write

  ld hl,tempspritedata
  ld c,$98
  call outix32

  ret



andtable: db 1,3,7,15,31,63,127,255,1,3,7,15,31,63,127,255
spritepositiontable: dw 0,32,64,96,128,160,192,224,256,288,320,352,384,416,448,480,512,544,576,608,640,672,704,736,768,800,832,864,896,928,960,992

