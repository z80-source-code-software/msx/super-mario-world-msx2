;special eventhandler to handle Y coordinate based objects in screen
;we will not use any mapflags. We simply directly pump this code into the levelengine from the loader. sloppy but effective. We are out of ram
handleObjectlisty:
.loop:
  ;set sprite y coordinate in spat
  ld    hl,(yobjectmirror)
  ld    e,(ix+offsets+0)        ;y offset of sprite (16 bits)
  ld    d,(ix+offsets+1)        ;y offset of sprite (16 bits)
  add   hl,de                   ;y value objects+offset

  ld    de,(cameraytimes8)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract y value of camera from y value of sprite
  jp    c,.outofscreentop       ;sprite is out of screen (top side of camera)
  
  ld    c,l                     ;8 bits y value of sprite

  ld    de,212
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.outofscreen         ;sprite is out of screen (bottom side of camera)

  ld    (iy+00),c               ;set sprite y in spat
  ;/set sprite y coordinate in spat
;  jp .checkbottom

.checkforx:

  ;set sprite x coordinate in spat
  ld    hl,(xobjectmirror)
  ld    e,(ix+offsets+2)        ;x offset of sprite (16 bits)
  ld    d,(ix+offsets+3)        ;x offset of sprite (16 bits)
  add   hl,de                   ;x value objects+offset

  ld    de,(cameraxprecise)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract x value of camera from x value of sprite  
  jp    c,.outofscreenleft      ;sprite is out of screen (left side of camera)
  
  ld    c,l                     ;8 bits x value of sprite

  ld    de,256
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.outofscreenright    ;sprite is out of screen (right side of camera)  
  
  ld    (iy+01),c               ;set sprite x in spat
  ;/set sprite x coordinate in spat

.nextsprite:
  ld    de,4
  add   iy,de                   ;iy increased by 4 to write to next sprites spat values
  add   ix,de                   ;ix increased by 4 to read out next sprites offset values

  djnz  .loop                   ;next sprite
  ;/set sprite coordinates in spat

;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

  ret

.outofscreentop:

  ld    (iy+00),220             ;set sprite y in spat

;if the camera is fully on top we still want to check a little bit higher. So you can trow objects a little bit out of the upper map edge 
  push af
  push hl
  ld    hl,(cameraytimes8)
  ld de,0
  xor a
  sbc hl,de
  pop hl
  jp z,.checkboundary
  
  pop af
 
  ld    de,128;64              ;if sprite is "de" pixels out of screen top, then set sprite inactive
  add   hl,de                   ;carry isnt set here
  jp    c,.nextsprite           ;if carry then sprite is still within active range
 
 
  jp    handleObjectlist.setspriteinactive


.checkboundary:
  pop af

  ld    de,64              ;if sprite is "de" pixels out of screen top, then set sprite inactive
  add   hl,de                   ;carry isnt set here
  jp    c,.nextsprite           ;if carry then sprite is still within active range

  jp    handleObjectlist.setspriteinactive


.outofscreen: ;bottom
  ld    (iy+00),220             ;set sprite y in spat

;make the bottom limitless in depth and compare the sprite coordinates to the absolute maximum

.checkbottom:
  ld    hl,(yobjectmirror) 
  ld de,256
  xor a
  sbc hl,de
  ld    de,(cameraytimes8)
  sbc   hl,de                   ;carry isnt set here
  jp    c,.checkforx           ;if carry then sprite is still within active range
  jp    handleObjectlist.setspriteinactive

.outofscreenright:
  ld    (iy+00),220             ;set sprite y in spat

  jp .nextsprite

.outofscreenleft:
  ld    (iy+00),220             ;set sprite y in spat

  jp .nextsprite

  jp handleObjectlist.setspriteinactive

;end of first part












adjustlevObjListpointery:        ;set objectlistpointer left or right (alternate each frame) of nearest object
  ld    iy,(levelObjectListpointer)                                                            ;1130
  ld    l,(iy+3)                ;y object in tiles (16 bit)
  ld    h,(iy+4)                ;y object in tiles (16 bit)



  ;subtract 16 tiles from enemy
  ld    de,16
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract 16 tiles from enemy
  jr    nc,.notcarry
  ld    hl,0
.notcarry:
  ex    de,hl
  ;subtract 16 tiles from enemy
  
  ld    hl,(cameray)            ;camera y in tiles (16 bit)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de
  ld    de,-lenghtlevelObjectList
  jp    c,.setobjectlistpointer
  ld    de,lenghtlevelObjectList

.setobjectlistpointer:        ;move the objectlistpointer 1 object to the left or right (alternate each frame)
  add   iy,de

  ld    a,iyh
  ld    (levelObjectListpointer+1),a
  ld    a,iyl
  ld    (levelObjectListpointer),a
  ret


setnewobjectsy:                  ;we are going to check if an object is 8 tiles out of screen left or right
  call  setobjectrightofcameray
  call  setobjectlefttofcameray

  ;check if the level just started, and see if objects need to be placed in screen
  ld    a,(setobjectscurrentscreen?)
  and    a
  ret   z

.setcurrentscreenobjects:
  
  
  ld    hl,levelObjectList+lenghtlevelObjectList
  push  hl
  pop   iy
  
;find object nearest to left camera boundary and start from there

.findfirstobject:
    ld e,(iy+3)
    ld d,(iy+4)
    ld hl,(cameray)
    xor a
    sbc hl,de
    jp c,.foundfirstobject
    
    ld de,lenghtlevelObjectList
    add iy,de
    
    jp nz,.findfirstobject 
  

.foundfirstobject: 

  ld de,-lenghtlevelObjectList
  add iy,de

  ld    b,AmountofPossibleActiveObjects

.loop:
  call .checkforcoin
  push  bc
  call  activateobject
  pop   bc
.skip:
  ld    de,lenghtlevelObjectList
  add   iy,de                   ;check next object to the right
  djnz  .loop
  ;/check if the level just started, and see if objects need to be placed in screen  
  
  
  xor   a
  ld    (setobjectscurrentscreen?),a
  
  ret
  

;coin list
.checkforcoin:

  ld    a,(setobjectscurrentscreen?)
  cp 2
  ret nz

  ld a,(iy+4)
  cp 78
  ret z
  cp 83
  ret z
  
  inc sp
  inc sp
  jp .skip
 
  ;ret

  
  
setobjectlefttofcameray: ;top
  ld    hl,(cameray)            ;camera y in tiles (for pixels we need to multiply by 8, but this is not needed here)
  or    a                       ;reset carry first for sbc hl,de
  ld    de,6;8
  sbc   hl,de                   ;check 8 tiles left of screen to see if there is an object
  ret   c
  ld    (cameraposition),hl

  ld    hl,(levelObjectListpointer)
 
  push  hl                      ;hl now points to object in objectlist to check
  pop   iy

  ld    bc,-lenghtlevelObjectList

.loopleft:
  ;check x=000 this defines the leftborder of the levelobjectlist
  ld    a,(iy+3)                ;x object (16 bit)
  ld    d,(iy+4)                ;x object (16 bit)
  or    d
  ret   z
  ;/check x=000 this defines the leftborder of the levelobjectlist

  call  checkobjecty
  jp    z,.activateobject        ;if zero then object is exactly 8 tiles out of screen left, so activate
  ret   nc                      ;if not carry then we checked enough to the left

  add   iy,bc                   ;check next object to the left
  jp    .loopleft



.activateobject: ;TODO: check for equal objects as well.

  push iy
  push bc

  ld e,checkaroundamount ;check for 2 equals
  
.loopcheck:

  push de
  ld    bc,-lenghtlevelObjectList
  add iy,bc
  call checkobjecty
  call z,activateobject
  pop de
  
  dec e
;  ld a,e
;  or d
  jp nz,.loopcheck

  pop bc
  pop iy

  jp activateobject
;ret



setobjectrightofcameray: ;bottom
  ld    hl,(cameray)            ;camera y in tiles (for pixels we need to multiply by 8, but this is not needed here)
  ld    de,26
  add   hl,de                   ;check 8 tiles right of screen to see if there is an object
  ld    (cameraposition),hl

  ld    hl,(levelObjectListpointer)
 
  push  hl                      ;hl now points to object in objectlist to check
  pop   iy

  ld    bc,lenghtlevelObjectList

.loopright:
  call  checkobjecty
  jp    z,.activateobjectdown        ;if zero then object is exactly 8 tiles out of screen right, so activate
  ret   c                       ;if carry then we checked enough to the right                      

  add   iy,bc                   ;check next object to the right
  jp    .loopright


;ret

.activateobjectdown: ;TODO: check for equal objects as well.

  push iy
  push bc

  ld e,checkaroundamount ;check for 2 equals
  
.loopcheckdown:

  push de
  ld    bc,lenghtlevelObjectList
  add iy,bc
  call checkobjecty
  call z,activateobject
  pop de
  
  dec e
;  ld a,e
;  or d
  jp nz,.loopcheckdown

  pop bc
  pop iy

  jp activateobject
;ret



;Problem: sometimes a object does not spawn. Perhaps at the frame its coordinates should match the camera there is a difference so maybe we should make a range in which the coordinates react?


checkobjecty:                    ;we check if object in objectlist should be activated
  ld    hl,(cameraposition)     ;check 8 tiles left/right of screen

  ld    e,(iy+3)                ;x object in tiles (16 bit)
  ld    d,(iy+4)                ;x object in tiles (16 bit)
  
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de
  ret c
  
  ;match difference
  ld a,h
  or l
  ret nz ;difference huge
  
  ld a,l
  cp 5
  jp c,.setz
  
;  xor a ;reset carry flag and return
  scf
  
  ret

.setz:

  xor a
  and a ;set zero flag
  ret

;end of leftright code

checkaroundamount: equ 2 ;maximum of equals to search in the objectslist pointer
