		fname	"smw.rom"
	org		$4000
fullintroenable: equ 1 ;1 is on, any other number is off
debugenable:  equ 0 ;same as above but enables the debug jumps
allmapsavailable: equ 0 ;make all maps available on worldmap use 2 for custom map availability.
beta:             equ 0 ;show beta logo

SupermarioWorld:

;
; SupermarioWorld - ROM header
;

    db		"AB"
	dw		init,0,0,0,0,0,0
	
  include "kinrou5.asm"
	
  include "rominit.asm"

  include "sccreplayer/sccinit.asm"

;initcode is now placed in a separate file so that the ROM library stays nice and clean
  include "initcode.asm" ;DO NOT MOVE THIS FILE TO ANOTHER LOCATION!!!


endinit: ;must never exceed $6000 ends @ about $4000+

	ds		$5000-$,$ff		;fill the rest up and add 256 byte header from here

	include "header.asm" ;this simple piece of code tells the megaflashrom utility we are dealing with a game that loves to write to the megaflashrom SCC+ SD
	;If my assumptions are correct RBSC will use this header as well to detect writable flash code for carnivore2. 
	;Special thanks to russian bear crew, pencioner and manuel pazos ramones for helping me out on this one
    db  "SupermarioWorld" ;cartridge header for detection code of the load save tool


; block 00 - 02 engine 
;	
enginepage3:
	include	"enginepage3.asm"	

 
; block 00 - 02 engine 
;	
engine:
phase	engaddr
	include	"engine.asm"	;problem is that engine.asm cannot be as large as the full RAM page. Moving this file should work
endengine:
dephase
enlength:	equ	$-engine

;
; fill remainder of blocks 0-2
;
endstartblocks:

	ds		$a000-$,$ff		

;
; block $03 - $04
;
phase	$8000
db $FF
db $FF ;write in here at the start once so that we know what flashable cart the user has. Upon a successfull write never write again to prevent damage.
grpx00addr:	incbin "../grapx/world1/level1-02a yoshis-island-1/packedgrpx/grpx.pck" 
grpxblckl00: equ $03
grpx01addr:	incbin "../grapx/world1/level1-02b yoshis-island-1/packedgrpx/grpx.pck" 
grpxblckl01: equ $03
grpx02addr:	incbin "../grapx/world1/level1-03a yoshis-island-2/packedgrpx/grpx.pck" 
grpxblckl02: equ $03
grpx03addr:	incbin "../grapx/world1/level1-03b yoshis-island-2/packedgrpx/grpx.pck" 
grpxblckl03: equ $03
grpx04addr:	incbin "../grapx/world1/level1-04a yoshis-island-3/packedgrpx/grpx.pck" 
grpxblckl04: equ $03
grpx05addr:	incbin "../grapx/world1/level1-04b yoshis-island-3/packedgrpx/grpx.pck" 
grpxblckl05: equ $03
	ds		$c000-$,$ff
dephase

;
; block $05 - $06
;
phase	$8000

;======================= vliegende mario ========================================
;0
include "spritedata/spriteconverter/l_flyingmario_jump.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_jump.tcs.gen"
;192
include "spritedata/spriteconverter/r_flyingmario_jump.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_jump.tcs.gen"
;384
include "spritedata/spriteconverter/l_flyingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sprintjump.tcs.gen"
;576
include "spritedata/spriteconverter/r_flyingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sprintjump.tcs.gen"
;768
include "spritedata/spriteconverter/l_flyingmario_fall.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fall.tcs.gen"
;960
include "spritedata/spriteconverter/r_flyingmario_fall.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fall.tcs.gen"
;1152
include "spritedata/spriteconverter/l_flyingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walk1.tcs.gen"
;1344
include "spritedata/spriteconverter/l_flyingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walk2.tcs.gen"
;1536
include "spritedata/spriteconverter/l_flyingmario_walk3.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walk3.tcs.gen"
;1728
include "spritedata/spriteconverter/r_flyingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walk1.tcs.gen"
;1920
include "spritedata/spriteconverter/r_flyingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walk2.tcs.gen"
;2112
include "spritedata/spriteconverter/r_flyingmario_walk3.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walk3.tcs.gen"
;2304
include "spritedata/spriteconverter/l_flyingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_turnaround.tcs.gen"
;2496
include "spritedata/spriteconverter/r_flyingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_turnaround.tcs.gen"
;2688
include "spritedata/spriteconverter/l_flyingmario_down.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_down.tcs.gen"
;2880
include "spritedata/spriteconverter/r_flyingmario_down.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_down.tcs.gen"
;3072
include "spritedata/spriteconverter/l_flyingmario_lookingup.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_lookingup.tcs.gen"
;3264
include "spritedata/spriteconverter/r_flyingmario_lookingup.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_lookingup.tcs.gen"
;3456
include "spritedata/spriteconverter/l_flyingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sprint1.tcs.gen"
;3648
include "spritedata/spriteconverter/l_flyingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sprint2.tcs.gen"
;3840
include "spritedata/spriteconverter/l_flyingmario_sprint3.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sprint3.tcs.gen"
;4032
include "spritedata/spriteconverter/r_flyingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sprint1.tcs.gen"
;4224
include "spritedata/spriteconverter/r_flyingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sprint2.tcs.gen"
;4416
include "spritedata/spriteconverter/r_flyingmario_sprint3.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sprint3.tcs.gen"
;4608
include "spritedata/spriteconverter/l_flyingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_holdshell.tcs.gen"
;4800
include "spritedata/spriteconverter/l_flyingmario_runwithshell2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_runwithshell2.tcs.gen"
;4992
include "spritedata/spriteconverter/l_flyingmario_runwithshell3.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_runwithshell3.tcs.gen"
;5184
include "spritedata/spriteconverter/r_flyingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_holdshell.tcs.gen"
;5376
include "spritedata/spriteconverter/r_flyingmario_runwithshell2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_runwithshell2.tcs.gen"
;5568
include "spritedata/spriteconverter/r_flyingmario_runwithshell3.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_runwithshell3.tcs.gen"
;5760
include "spritedata/spriteconverter/flyingmario_turnwithshell1.tgs.gen"
include "spritedata/spriteconverter/flyingmario_turnwithshell1.tcs.gen"
;5952
include "spritedata/spriteconverter/flyingmario_turnwithshell2.tgs.gen"
include "spritedata/spriteconverter/flyingmario_turnwithshell2.tcs.gen"
;6144
include "spritedata/spriteconverter/l_flyingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_takeshell.tcs.gen"
;6336
include "spritedata/spriteconverter/r_flyingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_takeshell.tcs.gen"
;6528
include "spritedata/spriteconverter/l_flyingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_lookupwithshell.tcs.gen"
;6720
include "spritedata/spriteconverter/r_flyingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_lookupwithshell.tcs.gen"
;6912
include "spritedata/spriteconverter/l_flyingmario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_kickshell.tcs.gen"
;7104
include "spritedata/spriteconverter/r_flyingmario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_kickshell.tcs.gen"
;7296
include "spritedata/spriteconverter/flyingmario_front2camera.tgs.gen"
include "spritedata/spriteconverter/flyingmario_front2camera.tcs.gen"
;7488
include "spritedata/spriteconverter/flyingmario_back2camera.tgs.gen"
include "spritedata/spriteconverter/flyingmario_back2camera.tcs.gen"
;7680
include "spritedata/spriteconverter/l_flyingmario_fall_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fall_outlined.tcs.gen"
;7872
include "spritedata/spriteconverter/r_flyingmario_fall_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fall_outlined.tcs.gen"
;8064
include "spritedata/spriteconverter/l_flyingmario_sprintjump_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sprintjump_outlined.tcs.gen"
;8256
include "spritedata/spriteconverter/r_flyingmario_sprintjump_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sprintjump_outlined.tcs.gen"
;8448
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up.tcs.gen"
;8640
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up.tcs.gen"
;8832
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally.tcs.gen"
;9024
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally.tcs.gen"
;9216
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down.tcs.gen"
;9408
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down.tcs.gen"
;9600
include "spritedata/spriteconverter/l_flyingmario_fly_straight_down.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_straight_down.tcs.gen"
;9792
include "spritedata/spriteconverter/r_flyingmario_fly_straight_down.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_straight_down.tcs.gen"
;============ rest sprites zijn 2 groot dus 96=============
;9984
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_rest.tcs.gen"
;10080
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_rest.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_rest.tcs.gen"
;10176
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_rest.tcs.gen"
;10272
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_rest.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_rest.tcs.gen"
;10368
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_rest.tcs.gen"
;10464
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_rest.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_rest.tcs.gen"
;=================================normale sprites weer==========
;10560
include "spritedata/spriteconverter/l_flyingmario_jump_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_jump_outlined.tcs.gen"
;10752
include "spritedata/spriteconverter/r_flyingmario_jump_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_jump_outlined.tcs.gen"
;10944
include "spritedata/spriteconverter/l_flyingmario_takeshell_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_takeshell_outlined.tcs.gen"
;11136
include "spritedata/spriteconverter/r_flyingmario_takeshell_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_takeshell_outlined.tcs.gen"
;11328
include "spritedata/spriteconverter/l_flyingmario_down_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_down_outlined.tcs.gen"
;11520
include "spritedata/spriteconverter/r_flyingmario_down_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_down_outlined.tcs.gen"
;11712
include "spritedata/spriteconverter/l_flyingmario_runwithshell2_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_runwithshell2_outlined.tcs.gen"
;11904
include "spritedata/spriteconverter/r_flyingmario_runwithshell2_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_runwithshell2_outlined.tcs.gen"
;==========================outlined sprites van het vliegen===================
;12096
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_outlined.tcs.gen"
;12288
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_outlined.tcs.gen"
;12480
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_outlined.tcs.gen"
;12672
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_outlined.tcs.gen"
;12864
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_outlined.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_outlined.tcs.gen"
;13056
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_outlined.tcs.gen"
;==========================outlined rest sprites van het vliegen in stappen van 96=========
;13248
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_outlined_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_up_outlined_rest.tcs.gen"
;13344
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_outlined_rest.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_up_outlined_rest.tcs.gen"
;13440
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_outlined_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_horizontally_outlined_rest.tcs.gen"
;13536
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_rest_outlined.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_horizontally_rest_outlined.tcs.gen"
;13632
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_outlined_rest.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_fly_diagonally_down_outlined_rest.tcs.gen"
;13728
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_outlined_rest.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_fly_diagonally_down_outlined_rest.tcs.gen"
;=================================[slides]===========================================
;13824
include "spritedata/spriteconverter/l_flyingmario_slide.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_slide.tcs.gen"
;14016 
include "spritedata/spriteconverter/r_flyingmario_slide.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_slide.tcs.gen"



	ds		$c000-$,$ff
dephase

;
; block $07 - $08
;
phase	$8000
backgrl00:  incbin "../level_editor/_levels/maplevel1-02a.stg.pck" 
blockbackgrl00:  equ $07
backgrl01:  incbin "../level_editor/_levels/maplevel1-02b.stg.pck" 
blockbackgrl01:  equ $07
backgrl02:  incbin "../level_editor/_levels/maplevel1-03a.stg.pck" 
blockbackgrl02:  equ $07
backgrl03:  incbin "../level_editor/_levels/maplevel1-03b.stg.pck" 
blockbackgrl03:  equ $07
backgrl04:  incbin "../level_editor/_levels/maplevel1-04a.stg.pck" 
blockbackgrl04:  equ $07
backgrl05:  incbin "../level_editor/_levels/maplevel1-04b.stg.pck" 
blockbackgrl05:  equ $07
	ds		$c000-$,$ff
dephase


;
; block $09 - $0a
;
phase	$8000

;=========Vuur mario======================
;0
include "spritedata/spriteconverter/l_shootingmario_jump.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_jump.tcs.gen"
;192
include "spritedata/spriteconverter/r_shootingmario_jump.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_jump.tcs.gen"
;384
include "spritedata/spriteconverter/l_shootingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_sprintjump.tcs.gen"
;576
include "spritedata/spriteconverter/r_shootingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sprintjump.tcs.gen"
;768
include "spritedata/spriteconverter/l_shootingmario_fall.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_fall.tcs.gen"
;960
include "spritedata/spriteconverter/r_shootingmario_fall.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_fall.tcs.gen"
;1152
include "spritedata/spriteconverter/l_shootingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walk1.tcs.gen"
;1344
include "spritedata/spriteconverter/l_shootingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walk2.tcs.gen"
;1536
include "spritedata/spriteconverter/l_shootingmario_walk3.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walk3.tcs.gen"
;1728
include "spritedata/spriteconverter/r_shootingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walk1.tcs.gen"
;1920
include "spritedata/spriteconverter/r_shootingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walk2.tcs.gen"
;2112
include "spritedata/spriteconverter/r_shootingmario_walk3.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walk3.tcs.gen"
;2304
include "spritedata/spriteconverter/l_shootingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_turnaround.tcs.gen"
;2496
include "spritedata/spriteconverter/r_shootingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_turnaround.tcs.gen"
;2688
include "spritedata/spriteconverter/l_shootingmario_down.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_down.tcs.gen"
;2880
include "spritedata/spriteconverter/r_shootingmario_down.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_down.tcs.gen"
;3072
include "spritedata/spriteconverter/l_shootingmario_lookingup.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_lookingup.tcs.gen"
;3264
include "spritedata/spriteconverter/r_shootingmario_lookingup.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_lookingup.tcs.gen"
;3456
include "spritedata/spriteconverter/l_shootingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_sprint1.tcs.gen"
;3648
include "spritedata/spriteconverter/l_shootingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_sprint2.tcs.gen"
;3840
include "spritedata/spriteconverter/l_shootingmario_sprint3.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_sprint3.tcs.gen"
;4032
include "spritedata/spriteconverter/r_shootingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sprint1.tcs.gen"
;4224
include "spritedata/spriteconverter/r_shootingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sprint2.tcs.gen"
;4416
include "spritedata/spriteconverter/r_shootingmario_sprint3.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sprint3.tcs.gen"
;4608
include "spritedata/spriteconverter/l_shootingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_holdshell.tcs.gen"
;4800
include "spritedata/spriteconverter/l_shootingmario_runwithshell2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_runwithshell2.tcs.gen"
;4992
include "spritedata/spriteconverter/l_shootingmario_runwithshell3.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_runwithshell3.tcs.gen"
;5184
include "spritedata/spriteconverter/r_shootingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_holdshell.tcs.gen"
;5376
include "spritedata/spriteconverter/r_shootingmario_runwithshell2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_runwithshell2.tcs.gen"
;5568
include "spritedata/spriteconverter/r_shootingmario_runwithshell3.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_runwithshell3.tcs.gen"
;5760
include "spritedata/spriteconverter/shootingmario_turnwithshell1.tgs.gen"
include "spritedata/spriteconverter/shootingmario_turnwithshell1.tcs.gen"
;5952
include "spritedata/spriteconverter/shootingmario_turnwithshell2.tgs.gen"
include "spritedata/spriteconverter/shootingmario_turnwithshell2.tcs.gen"
;6144
include "spritedata/spriteconverter/l_shootingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_takeshell.tcs.gen"
;6336
include "spritedata/spriteconverter/r_shootingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_takeshell.tcs.gen"
;6528
include "spritedata/spriteconverter/l_shootingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_lookupwithshell.tcs.gen"
;6720
include "spritedata/spriteconverter/r_shootingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_lookupwithshell.tcs.gen"
;6912
include "spritedata/spriteconverter/l_shootingmario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_kickshell.tcs.gen"
;7104
include "spritedata/spriteconverter/r_shootingmario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_kickshell.tcs.gen"
;7296
include "spritedata/spriteconverter/shootingmario_front2camera.tgs.gen"
include "spritedata/spriteconverter/shootingmario_front2camera.tcs.gen"
;7488
include "spritedata/spriteconverter/shootingmario_back2camera.tgs.gen"
include "spritedata/spriteconverter/shootingmario_back2camera.tcs.gen"
;===========================[slide]=====================
;7680 
include "spritedata/spriteconverter/l_shootingmario_slide.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_slide.tcs.gen"
;7872 
include "spritedata/spriteconverter/r_shootingmario_slide.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_slide.tcs.gen"


	ds		$c000-$,$ff
dephase

;
; block $0b - $0c
;
;
loaderblock:  equ $0b
phase	$8000
loaderstart:
	include "loader.asm"
loaderend:
	ds		$c000-$,$ff
dephase

supermariospritesblock: equ $0d
;
; block $0d - $0e
;
phase	$4000

;0
include "spritedata/spriteconverter/r_mario_walk1.tgs.gen"
include "spritedata/spriteconverter/r_mario_walk1.tcs.gen"
;192
include "spritedata/spriteconverter/r_mario_walk2.tgs.gen"
include "spritedata/spriteconverter/r_mario_walk2.tcs.gen"
;384
include "spritedata/spriteconverter/l_mario_walk1.tgs.gen"
include "spritedata/spriteconverter/l_mario_walk1.tcs.gen"
;576
include "spritedata/spriteconverter/l_mario_walk2.tgs.gen"
include "spritedata/spriteconverter/l_mario_walk2.tcs.gen"
;768
include "spritedata/spriteconverter/l_mario_jump.tgs.gen"
include "spritedata/spriteconverter/l_mario_jump.tcs.gen"
;960
include "spritedata/spriteconverter/r_mario_jump.tgs.gen"
include "spritedata/spriteconverter/r_mario_jump.tcs.gen"
;1152
include "spritedata/spriteconverter/l_mario_fall.tgs.gen"
include "spritedata/spriteconverter/l_mario_fall.tcs.gen"
;1344
include "spritedata/spriteconverter/r_mario_fall.tgs.gen"
include "spritedata/spriteconverter/r_mario_fall.tcs.gen"
;1536
include "spritedata/spriteconverter/l_mario_sprint1.tgs.gen"
include "spritedata/spriteconverter/l_mario_sprint1.tcs.gen"
;1728
include "spritedata/spriteconverter/l_mario_sprint2.tgs.gen"
include "spritedata/spriteconverter/l_mario_sprint2.tcs.gen"
;1920
include "spritedata/spriteconverter/r_mario_sprint1.tgs.gen"
include "spritedata/spriteconverter/r_mario_sprint1.tcs.gen"
;2112
include "spritedata/spriteconverter/r_mario_sprint2.tgs.gen"
include "spritedata/spriteconverter/r_mario_sprint2.tcs.gen"
;2304
include "spritedata/spriteconverter/l_mario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/l_mario_sprintjump.tcs.gen"
;2496
include "spritedata/spriteconverter/r_mario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/r_mario_sprintjump.tcs.gen"
;2688
include "spritedata/spriteconverter/l_mario_down.tgs.gen"
include "spritedata/spriteconverter/l_mario_down.tcs.gen"
;2880
include "spritedata/spriteconverter/r_mario_down.tgs.gen"
include "spritedata/spriteconverter/r_mario_down.tcs.gen"
;3072
include "spritedata/spriteconverter/l_mario_lookup.tgs.gen"
include "spritedata/spriteconverter/l_mario_lookup.tcs.gen"
;3264
include "spritedata/spriteconverter/r_mario_lookup.tgs.gen"
include "spritedata/spriteconverter/r_mario_lookup.tcs.gen"
;3456
include "spritedata/spriteconverter/l_mario_turnaround.tgs.gen"
include "spritedata/spriteconverter/l_mario_turnaround.tcs.gen"
;3648
include "spritedata/spriteconverter/r_mario_turnaround.tgs.gen"
include "spritedata/spriteconverter/r_mario_turnaround.tcs.gen"
;3840
include "spritedata/spriteconverter/mario_back2camera.tgs.gen"
include "spritedata/spriteconverter/mario_back2camera.tcs.gen"
;4032
include "spritedata/spriteconverter/mario_front2camera.tgs.gen"
include "spritedata/spriteconverter/mario_front2camera.tcs.gen"
;4224
include "spritedata/spriteconverter/l_mario_holdshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_holdshell.tcs.gen"
;4416
include "spritedata/spriteconverter/r_mario_holdshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_holdshell.tcs.gen"
;4608
include "spritedata/spriteconverter/l_mario_runwithshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_runwithshell.tcs.gen"
;4800
include "spritedata/spriteconverter/r_mario_runwithshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_runwithshell.tcs.gen"
;4992
include "spritedata/spriteconverter/mario_turnwithshell1.tgs.gen"
include "spritedata/spriteconverter/mario_turnwithshell1.tcs.gen"
;5184
include "spritedata/spriteconverter/mario_turnwithshell2.tgs.gen"
include "spritedata/spriteconverter/mario_turnwithshell2.tcs.gen"
;5376
include "spritedata/spriteconverter/l_mario_takeshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_takeshell.tcs.gen"
;5568
include "spritedata/spriteconverter/r_mario_takeshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_takeshell.tcs.gen"
;5760
include "spritedata/spriteconverter/l_mario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_lookupwithshell.tcs.gen"
;5952
include "spritedata/spriteconverter/r_mario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_lookupwithshell.tcs.gen"
;6144
include "spritedata/spriteconverter/l_mario_swim1.tgs.gen"
include "spritedata/spriteconverter/l_mario_swim1.tcs.gen"
;6336
include "spritedata/spriteconverter/r_mario_swim1.tgs.gen"
include "spritedata/spriteconverter/r_mario_swim1.tcs.gen"
;6528
include "spritedata/spriteconverter/l_mario_swim2.tgs.gen"
include "spritedata/spriteconverter/l_mario_swim2.tcs.gen"
;6720
include "spritedata/spriteconverter/r_mario_swim2.tgs.gen"
include "spritedata/spriteconverter/r_mario_swim2.tcs.gen"
;=========grote mario======================
;6912
characteraddressmariojumpl:
include "spritedata/spriteconverter/l_supermario_jump.tgs.gen"
coloraddressmariojumpl:
include "spritedata/spriteconverter/l_supermario_jump.tcs.gen"
;7104
characteraddressmariojump:
include "spritedata/spriteconverter/r_supermario_jump.tgs.gen"
coloraddressmariojump:
include "spritedata/spriteconverter/r_supermario_jump.tcs.gen"
;7296
include "spritedata/spriteconverter/l_supermario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/l_supermario_sprintjump.tcs.gen"
;7488
include "spritedata/spriteconverter/r_supermario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sprintjump.tcs.gen"
;7680
include "spritedata/spriteconverter/l_supermario_fall.tgs.gen"
include "spritedata/spriteconverter/l_supermario_fall.tcs.gen"
;7872
characteraddressmariofall:
include "spritedata/spriteconverter/r_supermario_fall.tgs.gen"
coloraddressmariofall:
include "spritedata/spriteconverter/r_supermario_fall.tcs.gen"
;8064 
characteraddressmariowalk1:
include "spritedata/spriteconverter/l_supermario_walk1.tgs.gen"
coloraddressmariowalk1:
include "spritedata/spriteconverter/l_supermario_walk1.tcs.gen"
;8256
characteraddressmariowalk2:
include "spritedata/spriteconverter/l_supermario_walk2.tgs.gen"
coloraddressmariowalk2:
include "spritedata/spriteconverter/l_supermario_walk2.tcs.gen"
;8448
characteraddressmariowalk3:
include "spritedata/spriteconverter/l_supermario_walk3.tgs.gen"
coloraddressmariowalk3:
include "spritedata/spriteconverter/l_supermario_walk3.tcs.gen"
;8640
characteraddressmariowalk1r:
include "spritedata/spriteconverter/r_supermario_walk1.tgs.gen"
coloraddressmariowalk1r:
include "spritedata/spriteconverter/r_supermario_walk1.tcs.gen"
;8832
characteraddressmariowalk2r:
include "spritedata/spriteconverter/r_supermario_walk2.tgs.gen"
coloraddressmariowalk2r:
include "spritedata/spriteconverter/r_supermario_walk2.tcs.gen"
;9024
characteraddressmariowalk3r:
include "spritedata/spriteconverter/r_supermario_walk3.tgs.gen"
coloraddressmariowalk3r:
include "spritedata/spriteconverter/r_supermario_walk3.tcs.gen"
;9216
include "spritedata/spriteconverter/l_supermario_turnaround.tgs.gen"
include "spritedata/spriteconverter/l_supermario_turnaround.tcs.gen"
;9408
characteraddressmarioturnr:
include "spritedata/spriteconverter/r_supermario_turnaround.tgs.gen"
coloraddressmarioturnr:
include "spritedata/spriteconverter/r_supermario_turnaround.tcs.gen"
;9600
include "spritedata/spriteconverter/l_supermario_down.tgs.gen"
include "spritedata/spriteconverter/l_supermario_down.tcs.gen"
;9792
characteraddressmariodown:
include "spritedata/spriteconverter/r_supermario_down.tgs.gen"
coloraddressmariodown:
include "spritedata/spriteconverter/r_supermario_down.tcs.gen"
;9984
include "spritedata/spriteconverter/l_supermario_lookingup.tgs.gen"
include "spritedata/spriteconverter/l_supermario_lookingup.tcs.gen"
;10176
characteraddressmariolookingup:
include "spritedata/spriteconverter/r_supermario_lookingup.tgs.gen"
coloraddressmariolookingup:
include "spritedata/spriteconverter/r_supermario_lookingup.tcs.gen"
;10368
include "spritedata/spriteconverter/l_supermario_sprint1.tgs.gen"
include "spritedata/spriteconverter/l_supermario_sprint1.tcs.gen"
;10560
include "spritedata/spriteconverter/l_supermario_sprint2.tgs.gen"
include "spritedata/spriteconverter/l_supermario_sprint2.tcs.gen"
;10752
include "spritedata/spriteconverter/l_supermario_sprint3.tgs.gen"
include "spritedata/spriteconverter/l_supermario_sprint3.tcs.gen"
;10944
include "spritedata/spriteconverter/r_supermario_sprint1.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sprint1.tcs.gen"
;11136
include "spritedata/spriteconverter/r_supermario_sprint2.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sprint2.tcs.gen"
;11328
include "spritedata/spriteconverter/r_supermario_sprint3.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sprint3.tcs.gen"
;11520
characteraddressmariohold1:
include "spritedata/spriteconverter/l_supermario_holdshell.tgs.gen"
coloraddressmariohold1:
include "spritedata/spriteconverter/l_supermario_holdshell.tcs.gen"
;11712
characteraddressmariohold2:
include "spritedata/spriteconverter/l_supermario_runwithshell2.tgs.gen"
coloraddressmariohold2:
include "spritedata/spriteconverter/l_supermario_runwithshell2.tcs.gen"
;11904
characteraddressmariohold3:
include "spritedata/spriteconverter/l_supermario_runwithshell3.tgs.gen"
coloraddressmariohold3:
include "spritedata/spriteconverter/l_supermario_runwithshell3.tcs.gen"
;12096
include "spritedata/spriteconverter/r_supermario_holdshell.tgs.gen"
include "spritedata/spriteconverter/r_supermario_holdshell.tcs.gen"
;12288
include "spritedata/spriteconverter/r_supermario_runwithshell2.tgs.gen"
include "spritedata/spriteconverter/r_supermario_runwithshell2.tcs.gen"
;12480
include "spritedata/spriteconverter/r_supermario_runwithshell3.tgs.gen"
include "spritedata/spriteconverter/r_supermario_runwithshell3.tcs.gen"
;12672
include "spritedata/spriteconverter/supermario_turnwithshell1.tgs.gen"
include "spritedata/spriteconverter/supermario_turnwithshell1.tcs.gen"
;12864
include "spritedata/spriteconverter/supermario_turnwithshell2.tgs.gen"
include "spritedata/spriteconverter/supermario_turnwithshell2.tcs.gen"
;13056
characteraddressmarioputdown:
include "spritedata/spriteconverter/l_supermario_takeshell.tgs.gen"
coloraddressmarioputdown:
include "spritedata/spriteconverter/l_supermario_takeshell.tcs.gen"
;13248
include "spritedata/spriteconverter/r_supermario_takeshell.tgs.gen"
include "spritedata/spriteconverter/r_supermario_takeshell.tcs.gen"
;13440
include "spritedata/spriteconverter/l_supermario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/l_supermario_lookupwithshell.tcs.gen"
;13632
include "spritedata/spriteconverter/r_supermario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/r_supermario_lookupwithshell.tcs.gen"
;13824
include "spritedata/spriteconverter/l_mario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_kickshell.tcs.gen"
;14016
include "spritedata/spriteconverter/r_mario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_kickshell.tcs.gen"
;14208
include "spritedata/spriteconverter/l_supermario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_supermario_kickshell.tcs.gen"
;14400
include "spritedata/spriteconverter/r_supermario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_supermario_kickshell.tcs.gen"
;14592
characteraddressmariofront: 
include "spritedata/spriteconverter/supermario_front2camera.tgs.gen"
coloraddressmariofront:
include "spritedata/spriteconverter/supermario_front2camera.tcs.gen"
;14784 
characteraddressmarioback:
include "spritedata/spriteconverter/supermario_back2camera.tgs.gen"
coloraddressmarioback:
include "spritedata/spriteconverter/supermario_back2camera.tcs.gen"
;=====================================[slidesprites]============================
;14976 
include "spritedata/spriteconverter/l_mario_slide.tgs.gen"
include "spritedata/spriteconverter/l_mario_slide.tcs.gen"
;15168 
include "spritedata/spriteconverter/r_mario_slide.tgs.gen"
include "spritedata/spriteconverter/r_mario_slide.tcs.gen"
;15360 
include "spritedata/spriteconverter/l_supermario_slide.tgs.gen"
include "spritedata/spriteconverter/l_supermario_slide.tcs.gen"
;15552 
include "spritedata/spriteconverter/r_supermario_slide.tgs.gen"
include "spritedata/spriteconverter/r_supermario_slide.tcs.gen"
;====================================[climbsprites]=============================
;15744 
include "spritedata/spriteconverter/mario_climb1.tgs.gen"
include "spritedata/spriteconverter/mario_climb1.tcs.gen"
;15936 
include "spritedata/spriteconverter/mario_climb2.tgs.gen"
include "spritedata/spriteconverter/mario_climb2.tcs.gen"
	ds		$8000-$,$ff
dephase


;tilegfx block
;
; block $0f - 10
;
phase	$8000
grpx06addr:	incbin "../grapx/world1/level1-05a yoshis-island-4/packedgrpx/grpx.pck" 
grpxblckl06: equ $0f
;grpx07addr:	incbin "../grapx/level07/packedgrpx/grpx07.pck" | grpxblckl07: equ $0f
grpx08addr:	incbin "../grapx/world1/level1-05c yoshis-island-4/packedgrpx/grpx.pck" 
grpxblckl08: equ $0f
grpx09addr:	incbin "../grapx/world1/level1-07a iggys-castle/packedgrpx/grpx.pck" 
grpxblckl09: equ $0f
grpx10addr:	incbin "../grapx/world1/level1-07b iggys-castle/packedgrpx/grpx.pck" 
grpxblckl10: equ $0f

grpx22baddr:	incbin "../grapx/world2/level2-05a donut-ghosthouse/packedgrpx/grpx.pck" 
grpxblckl22b: equ $0f
grpx22caddr:	incbin "../grapx/world2/level2-05a donut-ghosthouse/packedgrpx/grpx.pck" 
grpxblckl22c: equ $0f
grpx22daddr:	incbin "../grapx/world2/level2-05a donut-ghosthouse/packedgrpx/grpx.pck" 
grpxblckl22d: equ $0f

	ds		$c000-$,$ff
dephase

;packed level block
;
; block $11 - 12
;
phase	$8000
backgrl06:  incbin "../level_editor/_levels/maplevel1-05a.stg.pck" 
blockbackgrl06:  equ $11
backgrl07:  incbin "../level_editor/_levels/maplevel1-05b.stg.pck" 
blockbackgrl07:  equ $11
backgrl08:  incbin "../level_editor/_levels/maplevel1-05c.stg.pck" 
blockbackgrl08:  equ $11
backgrl09:  incbin "../level_editor/_levels/maplevel1-07a.stg.pck" 
blockbackgrl09:  equ $11
backgrl10:  incbin "../level_editor/_levels/maplevel1-07b.stg.pck" 
blockbackgrl10:  equ $11
	ds		$c000-$,$ff
dephase



;tilenanimation
;
; block $13-$14
;
animationtilesblock: equ   $13
phase   $4000
animationtilescharacteraddr:      incbin "../grapx/tileanimationgraphics/animationtiles.SC2",$0000+7,8*40  ;character pattern table
animationtilescoloraddr:          incbin "../grapx/tileanimationgraphics/animationtiles.SC2",$2000+7,8*40  ;color table
animationtilesCavecharacteraddr:  incbin "../grapx/tileanimationgraphics/animationtilesCave.SC2",$0000+7,8*12  ;character pattern table
animationtilesCavecoloraddr:      incbin "../grapx/tileanimationgraphics/animationtilesCave.SC2",$2000+7,8*12  ;color table
shiftingboxcharacteraddr:         incbin "../grapx/world1/level1-04a yoshis-island-3/horizontallyexpandingboxes.SC2",$0000+7,8*180  ;character pattern table
shiftingboxcoloraddr:             incbin "../grapx/world1/level1-04a yoshis-island-3/horizontallyexpandingboxes.SC2",$2000+7,8*180  ;color table
shiftingboxcharacteraddrv:        incbin "../grapx/world1/level1-04a yoshis-island-3/verticallyexpandingboxes.SC2",$0000+7,8*180  ;character pattern table
shiftingboxcoloraddrv:            incbin "../grapx/world1/level1-04a yoshis-island-3/verticallyexpandingboxes.SC2",$2000+7,8*180  ;color table
blackholeanimationgfx1:           incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation1.SC2",$0000+7,8*12  ;character pattern table
blackholeanimationgfxclr1:        incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation1.SC2",$2000+7,8*12  ;color table
blackholeanimationgfx2:           incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation2.SC2",$0000+7,8*12  ;character pattern table
blackholeanimationgfxclr2:        incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation2.SC2",$2000+7,8*12  ;color table
blackholeanimationgfx3:           incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation3.SC2",$0000+7,8*12  ;character pattern table
blackholeanimationgfxclr3:        incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation3.SC2",$2000+7,8*12  ;color table
blackholeanimationgfx4:           incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation4.SC2",$0000+7,8*12  ;character pattern table
blackholeanimationgfxclr4:        incbin "../grapx/world1/level1-04a yoshis-island-3/blackholeanimation4.SC2",$2000+7,8*12  ;color table
LavaanimationtilesCave:           incbin "../grapx/tileanimationgraphics/LavaanimationtilesCave.SC2",$0000+7,8*8  ;character table
LavaanimationtilesCaveclr:        incbin "../grapx/tileanimationgraphics/LavaanimationtilesCave.SC2",$2000+7,8*8  ;color table
wateranimation1:                  incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation1.SC2",$0000+7,8*23  ;character table
wateranimationclr1:               incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation1.SC2",$2000+7,8*23  ;color table
wateranimation2:                  incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation2.SC2",$0000+7,8*23  ;character table
wateranimationclr2:               incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation2.SC2",$2000+7,8*23  ;color table
wateranimation3:                  incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation3.SC2",$0000+7,8*23  ;character table
wateranimationclr3:               incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation3.SC2",$2000+7,8*23  ;color table
wateranimation4:                  incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation4.SC2",$0000+7,8*23  ;character table
wateranimationclr4:               incbin "../grapx/world1/level1-05a yoshis-island-4/wateranimation4.SC2",$2000+7,8*23  ;color table
IggyCastleanimation1:             incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation1.SC2",$0000+7,8*15  ;character table
IggyCastleanimationclr1:          incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation1.SC2",$2000+7,8*15  ;color table
IggyCastleanimation2:             incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation2.SC2",$0000+7,8*15  ;character table
IggyCastleanimationclr2:          incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation2.SC2",$2000+7,8*15  ;color table
IggyCastleanimation3:             incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation3.SC2",$0000+7,8*15  ;character table
IggyCastleanimationclr3:          incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation3.SC2",$2000+7,8*15  ;color table
IggyCastleanimation4:             incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation4.SC2",$0000+7,8*15  ;character table
IggyCastleanimationclr4:          incbin "../grapx/world1/level1-07a iggys-castle/IggyCastleanimation4.SC2",$2000+7,8*15  ;color table
level26animation:             incbin "../grapx/world2/level2-06a donut-secret-1/animationb.SC2",$0000+7,8*32  ;character table
level26animationclr:          incbin "../grapx/world2/level2-06a donut-secret-1/animationb.SC2",$2000+7,8*32  ;color table
level41animation:             incbin "../grapx/world3/level3-04a/animations.SC2",$0000+7,8*40  ;character table
level41animationclr:          incbin "../grapx/world3/level3-04a/animations.SC2",$2000+7,8*40  ;color table
iggylava1:                         incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation1.SC2",$0000+7,8*6  ;character table
iggylavaclr1:                      incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation1.SC2",$2000+7,8*6  ;color table
iggylava2:                         incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation2.SC2",$0000+7,8*6  ;character table
iggylavaclr2:                      incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation2.SC2",$2000+7,8*6  ;color table
iggylava3:                         incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation3.SC2",$0000+7,8*6  ;character table
iggylavaclr3:                      incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation3.SC2",$2000+7,8*6  ;color table
iggylava4:                         incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation4.SC2",$0000+7,8*6  ;character table
iggylavaclr4:                      incbin "../grapx/world1/level1-07c iggys-castle/lavaanimation4.SC2",$2000+7,8*6  ;color table

tubemove1:                      incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation1.SC2",$0000+7,8*18  ;character table
tubemove1clr:                   incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation1.SC2",$2000+7,8*18  ;color table
tubemove2:                      incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation2.SC2",$0000+7,8*18  ;character table
tubemove2clr:                   incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation2.SC2",$2000+7,8*18  ;color table
tubemove3:                      incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation3.SC2",$0000+7,8*18  ;character table
tubemove3clr:                   incbin "../grapx/world2/level2-04a donut-plains-4/tubeanimation3.SC2",$2000+7,8*18  ;color table


box02a:                      incbin "../grapx/world2/level2-02a donut-plains-2/box.SC2",$0000+7,8*4  ;character table
box02aclr:                   incbin "../grapx/world2/level2-02a donut-plains-2/box.SC2",$2000+7,8*4  ;color table
box04a:                      incbin "../grapx/world2/level2-04a donut-plains-4/box.SC2",$0000+7,8*4  ;character table
box04aclr:                   incbin "../grapx/world2/level2-04a donut-plains-4/box.SC2",$2000+7,8*4  ;color table
;donut secret2
animationtiles0207:         incbin "../grapx/world2/level2-07a donut-secret-2/animationtiles.SC2",$0000+7,8*12  ;character pattern table
animationtiles0207clr:      incbin "../grapx/world2/level2-07a donut-secret-2/animationtiles.SC2",$2000+7,8*12  ;color table

;morton's castle
animationtiles0211a:         incbin "../grapx/world2/level2-11a mortons-castle/Animation.SC2",$0000+7,8*62;58  ;character pattern table
animationtiles0211aclr:      incbin "../grapx/world2/level2-11a mortons-castle/Animation.SC2",$2000+7,8*62;58  ;color table
animationtiles0211d:         incbin "../grapx/world2/level2-11d mortons-castle/animationtiles.SC2",$0000+7,8*16  ;character pattern table
animationtiles0211dclr:      incbin "../grapx/world2/level2-11d mortons-castle/animationtiles.SC2",$2000+7,8*16  ;color table

;vanilla 03-01c
animationtiles0301c:         incbin "../grapx/world3/level3-01c/Animation.SC2",$0000+7,8*24  ;character pattern table
animationtiles0301cclr:      incbin "../grapx/world3/level3-01c/Animation.SC2",$2000+7,8*24  ;color table

;vanilla 03-03a
animationtiles0303a:         incbin "../grapx/world3/level3-03a/Animation.SC2",$0000+7,8*40  ;character pattern table
animationtiles0303aclr:      incbin "../grapx/world3/level3-03a/Animation.SC2",$2000+7,8*40  ;color table

;vanilla 03-03b
animationtiles0303b:         incbin "../grapx/world3/level3-03b/Animation.SC2",$0000+7,8*44  ;character pattern table
animationtiles0303bclr:      incbin "../grapx/world3/level3-03b/Animation.SC2",$2000+7,8*44  ;color table


;vanilla 03-07a
animationtiles0307a:         incbin "../grapx/world3/level3-07a vanillasecret-2/animation.SC2",$0000+7,8*16  ;character pattern table
animationtiles0307aclr:      incbin "../grapx/world3/level3-07a vanillasecret-2/animation.SC2",$2000+7,8*16  ;color table


;there is still some room for exactly 64 tiles

  ds                $8000-$,$ff
dephase


;tilenanimation2
;
; block $15
;
phase	$4000
animationtilesblock2: equ   $15

;vanilla 03-04b
animationtiles0304b:         incbin "../grapx/world3/level3-04b/animation.SC2",$0000+7,8*72  ;character pattern table
animationtiles0304bclr:      incbin "../grapx/world3/level3-04b/animation.SC2",$2000+7,8*72  ;color table

;twinbridges 04-06d
animationtiles0406d:         incbin "../grapx/world4/level4-06d/animationtiles.SC2",$0000+7,8*24  ;character pattern table
animationtiles0406dclr:      incbin "../grapx/world4/level4-06d/animationtiles.SC2",$2000+7,8*24  ;color table

;forest fortress
animationtiles0507a:         incbin "../grapx/world5/level5-07a/animationtiles.SC2",$0000+7,8*16  ;character pattern table
animationtiles0507aclr:      incbin "../grapx/world5/level5-07a/animationtiles.SC2",$2000+7,8*16  ;color table

;chocolate4a
animationtiles0604a:         incbin "../grapx/world6/level6-04a/Animation.SC2",$0000+7,8*16  ;character pattern table
animationtiles0604aclr:      incbin "../grapx/world6/level6-04a/Animation.SC2",$2000+7,8*16  ;color table

;chocolate8a
animationtiles0608a:         incbin "../grapx/world6/level6-08a/Animation.SC2",$0000+7,8*16  ;character pattern table
animationtiles0608aclr:      incbin "../grapx/world6/level6-08a/Animation.SC2",$2000+7,8*16  ;color table

;bowserva1a
animationtiles0701aa:         incbin "../grapx/world7/level7-01a/animationa.SC2",$0000+7,8*32  ;character pattern table
animationtiles0701aclra:      incbin "../grapx/world7/level7-01a/animationa.SC2",$2000+7,8*32  ;color table


	ds		$6000-$,$ff
dephase

;
; block $16
; ;was phased at $8000 and $a000
savemenublock:           equ $16
phase	$4000

include "savemenu.asm"  

	ds		$6000-$,$ff
dephase
;
; block $17-$18
;
; romprogblock
romprogblock:             equ $17
phase	$8000
  include "mariosprite.asm"
  include "mariotransform.asm"
;  include "cheat.asm"
  include "mariophysics.asm"
  include "mariofun.asm"
	ds		$c000-$,$ff
dephase

;
; block $19-1A
;
supermariospritesblock2: equ $19
phase	$4000
;=======================================[overige climbing mario]============================
;0 
include "spritedata/spriteconverter/supermario_climb1.tgs.gen"
include "spritedata/spriteconverter/supermario_climb1.tcs.gen"
;192 
include "spritedata/spriteconverter/supermario_climb2.tgs.gen"
include "spritedata/spriteconverter/supermario_climb2.tcs.gen"
;384 
include "spritedata/spriteconverter/shootingmario_climb1.tgs.gen"
include "spritedata/spriteconverter/shootingmario_climb1.tcs.gen"
;576
include "spritedata/spriteconverter/shootingmario_climb2.tgs.gen"
include "spritedata/spriteconverter/shootingmario_climb2.tcs.gen"
;768 
include "spritedata/spriteconverter/flyingmario_climb1.tgs.gen"
include "spritedata/spriteconverter/flyingmario_climb1.tcs.gen"
;960
include "spritedata/spriteconverter/flyingmario_climb2.tgs.gen"
include "spritedata/spriteconverter/flyingmario_climb2.tcs.gen"
;========================================[alle zwemmende marios op de kleine na]=============
;1152 
include "spritedata/spriteconverter/l_supermario_swim1.tgs.gen"
include "spritedata/spriteconverter/l_supermario_swim1.tcs.gen"
;1344 
include "spritedata/spriteconverter/l_supermario_swim2.tgs.gen"
include "spritedata/spriteconverter/l_supermario_swim2.tcs.gen"
;1536 
include "spritedata/spriteconverter/l_supermario_swim3.tgs.gen"
include "spritedata/spriteconverter/l_supermario_swim3.tcs.gen"
;1728 
include "spritedata/spriteconverter/r_supermario_swim1.tgs.gen"
include "spritedata/spriteconverter/r_supermario_swim1.tcs.gen"
;1920 
include "spritedata/spriteconverter/r_supermario_swim2.tgs.gen"
include "spritedata/spriteconverter/r_supermario_swim2.tcs.gen"
;2112 
include "spritedata/spriteconverter/r_supermario_swim3.tgs.gen"
include "spritedata/spriteconverter/r_supermario_swim3.tcs.gen"
;2304 
include "spritedata/spriteconverter/l_mario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/l_mario_holdshellswim1.tcs.gen"
;2496 
include "spritedata/spriteconverter/r_mario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/r_mario_holdshellswim1.tcs.gen"
;2688 
include "spritedata/spriteconverter/l_supermario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/l_supermario_holdshellswim1.tcs.gen"
;2880 
include "spritedata/spriteconverter/r_supermario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/r_supermario_holdshellswim1.tcs.gen"
;3072 
include "spritedata/spriteconverter/l_shootingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_swim1.tcs.gen"
;3264 
include "spritedata/spriteconverter/l_shootingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_swim2.tcs.gen"
;3456 
include "spritedata/spriteconverter/l_shootingmario_swim3.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_swim3.tcs.gen"
;3648 
include "spritedata/spriteconverter/r_shootingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_swim1.tcs.gen"
;3840 
include "spritedata/spriteconverter/r_shootingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_swim2.tcs.gen"
;4032 
include "spritedata/spriteconverter/r_shootingmario_swim3.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_swim3.tcs.gen"
;4224 
include "spritedata/spriteconverter/l_shootingmario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_holdshellswim1.tcs.gen"
;4416 
include "spritedata/spriteconverter/r_shootingmario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_holdshellswim1.tcs.gen"
;4608 
include "spritedata/spriteconverter/l_flyingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_swim1.tcs.gen"
;4800 
include "spritedata/spriteconverter/l_flyingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_swim2.tcs.gen"
;4992 
include "spritedata/spriteconverter/l_flyingmario_swim3.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_swim3.tcs.gen"
;5184 
include "spritedata/spriteconverter/r_flyingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_swim1.tcs.gen"
;5376 
include "spritedata/spriteconverter/r_flyingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_swim2.tcs.gen"
;5568 
include "spritedata/spriteconverter/r_flyingmario_swim3.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_swim3.tcs.gen"
;5760 
include "spritedata/spriteconverter/l_flyingmario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_holdshellswim1.tcs.gen"
;5952
include "spritedata/spriteconverter/r_flyingmario_holdshellswim1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_holdshellswim1.tcs.gen"
;==========================================[Mario sterft]=============================
;6144 
include "spritedata/spriteconverter/mario_dead1.tgs.gen"
include "spritedata/spriteconverter/mario_dead1.tcs.gen"
;6336
include "spritedata/spriteconverter/mario_dead2.tgs.gen"
include "spritedata/spriteconverter/mario_dead2.tcs.gen"
;=====================================================================================
;6528
include "spritedata/spriteconverter/pauze.tgs.gen"
include "spritedata/spriteconverter/pauze.tcs.gen"
;6720
include "spritedata/ingame sprites/pinkshell back.tgs.gen"
include "spritedata/ingame sprites/pinkshell back.tcs.gen"
;=======================================[de wolkjes van mario]========================
;6816
include "spritedata/ingame sprites/cloudatfeet1.tgs.gen"
include "spritedata/ingame sprites/cloudatfeet1.tcs.gen"
;6912
include "spritedata/ingame sprites/cloudatfeet2.tgs.gen"
include "spritedata/ingame sprites/cloudatfeet2.tcs.gen"
;7008
include "spritedata/ingame sprites/cloudatfeet3.tgs.gen"
include "spritedata/ingame sprites/cloudatfeet3.tcs.gen"
spriteadrkickorexplosioncloudleft:
include "spritedata/ingame sprites/kickorexplosioncloudleft.tgs.gen"
spriteadrkickorexplosioncloudright:
include "spritedata/ingame sprites/kickorexplosioncloudright.tgs.gen"
colradrwhitesprite:
include "spritedata/ingame sprites/WhiteSprite.tcs.gen"
spriteadrthunderleft:
include "spritedata/spriteconverter/Thunderleft.tgs.gen"
spriteadrthunderright:
include "spritedata/spriteconverter/thunderright.tgs.gen"
spriteadrthunderleft2:
include "spritedata/spriteconverter/thunderleft2.tgs.gen"
spriteadrthunderright2:
include "spritedata/spriteconverter/thunderright2.tgs.gen"
spriteadrthunderleft3:
include "spritedata/spriteconverter/thunderleft3.tgs.gen"
spriteadrthunderright3:
include "spritedata/spriteconverter/thunderright3.tgs.gen"
;=====================================================================================

;=============================[mariorunningup sprites]================================
;normal
spriteadrrunningupleft1:
include "spritedata/spriteconverter/l_mario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/l_mario_walksupwall1.tcs.gen"
spriteadrrunningupleft2:
include "spritedata/spriteconverter/l_mario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/l_mario_walksupwall2.tcs.gen"
spriteadrrunningupright1:
include "spritedata/spriteconverter/r_mario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/r_mario_walksupwall1.tcs.gen"
spriteadrrunningupright2:
include "spritedata/spriteconverter/r_mario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/r_mario_walksupwall2.tcs.gen"
;super
spriteadrrunningupbigleft1:
include "spritedata/spriteconverter/l_supermario_walksupwall1.tgs.gen"
coloradrrunningupbigleft1:
include "spritedata/spriteconverter/l_supermario_walksupwall1.tcs.gen"
spriteadrrunningupbigleft2:
include "spritedata/spriteconverter/l_supermario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/l_supermario_walksupwall2.tcs.gen"
spriteadrrunningupbigleft3:
include "spritedata/spriteconverter/l_supermario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/l_supermario_walksupwall3.tcs.gen"
spriteadrrunningupbigright1:
include "spritedata/spriteconverter/r_supermario_walksupwall1.tgs.gen"
coloradrrunningupbigright1:
include "spritedata/spriteconverter/r_supermario_walksupwall1.tcs.gen"
spriteadrrunningupbigright2:
include "spritedata/spriteconverter/r_supermario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/r_supermario_walksupwall2.tcs.gen"
spriteadrrunningupbigright3:
include "spritedata/spriteconverter/r_supermario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/r_supermario_walksupwall3.tcs.gen"
;fire
spriteadrrunningupfireleft1:
include "spritedata/spriteconverter/l_shootingmario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walksupwall1.tcs.gen"
spriteadrrunningupfireleft2:
include "spritedata/spriteconverter/l_shootingmario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walksupwall2.tcs.gen"
spriteadrrunningupfireleft3:
include "spritedata/spriteconverter/l_shootingmario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_walksupwall3.tcs.gen"
spriteadrrunningupfireright1:
include "spritedata/spriteconverter/r_shootingmario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walksupwall1.tcs.gen"
spriteadrrunningupfireright2:
include "spritedata/spriteconverter/r_shootingmario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walksupwall2.tcs.gen"
spriteadrrunningupfireright3:
include "spritedata/spriteconverter/r_shootingmario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_walksupwall3.tcs.gen"
;flying
spriteadrrunningupflyingleft1:
include "spritedata/spriteconverter/l_flyingmario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walksupwall1.tcs.gen"
spriteadrrunningupflyingleft2:
include "spritedata/spriteconverter/l_flyingmario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walksupwall2.tcs.gen"
spriteadrrunningupflyingleft3:
include "spritedata/spriteconverter/l_flyingmario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_walksupwall3.tcs.gen"
spriteadrrunningupflyingright1:
include "spritedata/spriteconverter/r_flyingmario_walksupwall1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walksupwall1.tcs.gen"
spriteadrrunningupflyingright2:
include "spritedata/spriteconverter/r_flyingmario_walksupwall2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walksupwall2.tcs.gen"
spriteadrrunningupflyingright3:
include "spritedata/spriteconverter/r_flyingmario_walksupwall3.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_walksupwall3.tcs.gen"
;Balloon mario
;TODO: Add the idiot
mariosmallballoon:
include "spritedata/spriteconverter/marioballoon.tgs.gen"
include "spritedata/spriteconverter/marioballoon.tcs.gen"
mariosmallballoonsmall:
include "spritedata/spriteconverter/mario_front2camera.tgs.gen"
include "spritedata/spriteconverter/mario_front2camera.tcs.gen"
;big
supermarioballoon:
include "spritedata/spriteconverter/supermarioballoon.tgs.gen"
include "spritedata/spriteconverter/supermarioballoon.tcs.gen"
supermarioballoonsmall:
include "spritedata/spriteconverter/supermario_front2camera.tgs.gen"
include "spritedata/spriteconverter/supermario_front2camera.tcs.gen"
shootingmarioballoon:
include "spritedata/spriteconverter/firemarioballoon.tgs.gen"
include "spritedata/spriteconverter/firemarioballoon.tcs.gen"
shootingmarioballoonsmall:
include "spritedata/spriteconverter/shootingmario_front2camera.tgs.gen"
include "spritedata/spriteconverter/shootingmario_front2camera.tcs.gen"
flyingmarioballoon:
include "spritedata/spriteconverter/flyingmarioballoon.tgs.gen"
include "spritedata/spriteconverter/flyingmarioballoon.tcs.gen"
flyingmarioballoonsmall:
include "spritedata/spriteconverter/flyingmario_front2camera.tgs.gen"
include "spritedata/spriteconverter/flyingmario_front2camera.tcs.gen"


	ds		$8000-$,$ff
dephase

;
; block $1b-1e
;
ingamespritesblock1: equ $1b
phase	$4000

;pink shell + its animations
characteraddresspinkshell:
include "spritedata/ingame sprites/pinkshell.tgs.gen"
coloraddresspinkshell:
include "spritedata/ingame sprites/pinkshell.tcs.gen"
characteraddresspinkshellfront:
include "spritedata/ingame sprites/pinkshell front.tgs.gen"
coloraddresspinkshellfront:
include "spritedata/ingame sprites/pinkshell front.tcs.gen"
characteraddresspinkshellback:
include "spritedata/ingame sprites/pinkshell back.tgs.gen"
coloraddresspinkshellback:
include "spritedata/ingame sprites/pinkshell back.tcs.gen"
characteraddresspinkshellleft:
include "spritedata/ingame sprites/pinkshell left.tgs.gen"
coloraddresspinkshellleft:
include "spritedata/ingame sprites/pinkshell left.tcs.gen"
characteraddresspinkshellright:
include "spritedata/ingame sprites/pinkshell right.tgs.gen"
coloraddresspinkshellright:
include "spritedata/ingame sprites/pinkshell right.tcs.gen"
characteraddresspinkshelldown:
include "spritedata/ingame sprites/pinkshellupsidedown.tgs.gen"
coloraddresspinkshelldown:
include "spritedata/ingame sprites/pinkshellupsidedown.tcs.gen"

;diagonal background sprites donut plains
characteraddresdonutpldiaback:
include "spritedata/ingame sprites/donut plains diagonal background sprite1.tgs.gen"
coloraddresdonutpldiaback:
include "spritedata/ingame sprites/donut plains diagonal background sprite1.tcs.gen"
characteraddresdiagonalbackgroundsprite2:
include "spritedata/ingame sprites/diagonalbackgroundsprite2.tgs.gen"
coloraddresdiagonalbackgroundsprite2:
include "spritedata/ingame sprites/diagonalbackgroundsprite2.tcs.gen"

;rex
characteraddresrex_left1:
include "spritedata/ingame sprites/rex_left1.tgs.gen"
coloraddresrex_left1:
include "spritedata/ingame sprites/rex_left1.tcs.gen"
characteraddresrex_left2:
include "spritedata/ingame sprites/rex_left2.tgs.gen"
coloraddresrex_left2:
include "spritedata/ingame sprites/rex_left2.tcs.gen"
characteraddresrex_right1:
include "spritedata/ingame sprites/rex_right1.tgs.gen"
coloraddresrex_right1:
include "spritedata/ingame sprites/rex_right1.tcs.gen"
characteraddresrex_right2:
include "spritedata/ingame sprites/rex_right2.tgs.gen"
coloraddresrex_right2:
include "spritedata/ingame sprites/rex_right2.tcs.gen"
characteraddresrex_jumpedon_left:
include "spritedata/ingame sprites/rex_jumpedon_left.tgs.gen"
coloraddresrex_jumpedon_left:
include "spritedata/ingame sprites/rex_jumpedon_left.tcs.gen"
characteraddresrex_jumpedon_right:
include "spritedata/ingame sprites/rex_jumpedon_right.tgs.gen"
coloraddresrex_jumpedon_right:
include "spritedata/ingame sprites/rex_jumpedon_right.tcs.gen"
characteraddresrexsmall_left1:
include "spritedata/ingame sprites/rexsmall_left1.tgs.gen"
coloraddresrexsmall_left1:
include "spritedata/ingame sprites/rexsmall_left1.tcs.gen"
characteraddresrexsmall_left2:
include "spritedata/ingame sprites/rexsmall_left2.tgs.gen"
coloraddresrexsmall_left2:
include "spritedata/ingame sprites/rexsmall_left2.tcs.gen"
characteraddresrexsmall_right1:
include "spritedata/ingame sprites/rexsmall_right1.tgs.gen"
coloraddresrexsmall_right1:
include "spritedata/ingame sprites/rexsmall_right1.tcs.gen"
characteraddresrexsmall_right2:
include "spritedata/ingame sprites/rexsmall_right2.tgs.gen"
coloraddresrexsmall_right2:
include "spritedata/ingame sprites/rexsmall_right2.tcs.gen"
characteraddresrexdead_left:
include "spritedata/ingame sprites/rex_dead_left.tgs.gen"
coloraddresrexdead_left:
include "spritedata/ingame sprites/rex_dead_left.tcs.gen"
characteraddresrexdead_right:
include "spritedata/ingame sprites/rex_dead_right.tgs.gen"
coloraddresrexdead_right:
include "spritedata/ingame sprites/rex_dead_right.tcs.gen"


;checkpoint
characteraddresscheckpointtop:
include "spritedata/ingame sprites/checkpointtop.tgs.gen"
coloraddresscheckpointtop:
include "spritedata/ingame sprites/checkpointtop.tcs.gen"
characteraddresscheckpointbar:
include "spritedata/ingame sprites/checkpointbar.tgs.gen"
coloraddresscheckpointbar:
include "spritedata/ingame sprites/checkpointbar.tcs.gen"
characteraddresscheckpointpoleright1:
include "spritedata/ingame sprites/checkpointpoleright1.tgs.gen"
coloraddresscheckpointpoleright1:
include "spritedata/ingame sprites/checkpointpoleright1.tcs.gen"
characteraddresscheckpointpoleright2:
include "spritedata/ingame sprites/checkpointpoleright2.tgs.gen"
coloraddresscheckpointpoleright2:
include "spritedata/ingame sprites/checkpointpoleright2.tcs.gen"
characteraddresscheckpointpoleright3:
include "spritedata/ingame sprites/checkpointpoleright3.tgs.gen"
coloraddresscheckpointpoleright3:
include "spritedata/ingame sprites/checkpointpoleright3.tcs.gen"
characteraddresscheckpointpoleright4:
include "spritedata/ingame sprites/checkpointpoleright4.tgs.gen"
coloraddresscheckpointpoleright4:
include "spritedata/ingame sprites/checkpointpoleright4.tcs.gen"


;endflag
characteraddressendflagtop:
include "spritedata/ingame sprites/endflagtop.tgs.gen"
coloraddressendflagtop:
include "spritedata/ingame sprites/endflagtop.tcs.gen"
characteraddressendflagbar:
include "spritedata/ingame sprites/endflagbar.tgs.gen"
coloraddressendflagbar:
include "spritedata/ingame sprites/endflagbar.tcs.gen"

;Jumpin Piranha Plant
characteraddressJumpinPiranhaPl1:
include "spritedata/ingame sprites/JumpinPiranhaPlant1.tgs.gen"
coloraddressJumpinPiranhaPl1:
include "spritedata/ingame sprites/JumpinPiranhaPlant1.tcs.gen"
characteraddressJumpinPiranhaPl2:
include "spritedata/ingame sprites/JumpinPiranhaPlant2.tgs.gen"
coloraddressJumpinPiranhaPl2:
include "spritedata/ingame sprites/JumpinPiranhaPlant2.tcs.gen"
characteraddressJumpinPiranhaPl3:
include "spritedata/ingame sprites/JumpinPiranhaPlant3.tgs.gen"
coloraddressJumpinPiranhaPl3:
include "spritedata/ingame sprites/JumpinPiranhaPlant3.tcs.gen"
characteraddressJumpinPiranhaPl4:
include "spritedata/ingame sprites/JumpinPiranhaPlant4.tgs.gen"
coloraddressJumpinPiranhaPl4:
include "spritedata/ingame sprites/JumpinPiranhaPlant4.tcs.gen"

;tube cover up (goes in front of Jumpin Piranha Plant)
characteraddresstubecoverup:
include "spritedata/ingame sprites/tubecoverup.tgs.gen"
coloraddresstubecoverup:
include "spritedata/ingame sprites/tubecoverup.tcs.gen"

;questionmark block
characteraddressquestionmarkblock:
include "spritedata/ingame sprites/questionmarkblock.tgs.gen"
coloraddressquestionmarkblock:
include "spritedata/ingame sprites/questionmarkblock.tcs.gen"

;exclamationmark block
characteraddressexclamationmarkblock:
include "spritedata/ingame sprites/exclamationmarkblock.tgs.gen"
coloraddressexclamationmarkblock:
include "spritedata/ingame sprites/exclamationmarkblock.tcs.gen"

;rotating block
characteraddressrotatingblock:
include "spritedata/ingame sprites/rotatingblock.tgs.gen"
coloraddressrotatingblock:
include "spritedata/ingame sprites/rotatingblock.tcs.gen"

;brown block
characteraddressbrownblock:
include "spritedata/ingame sprites/brownblock.tgs.gen"
coloraddressbrownblock:
include "spritedata/ingame sprites/brownblock.tcs.gen"
characteraddressbrownblockav:
include "spritedata/ingame sprites/brownblockalternativepalettevalue.tgs.gen"
coloraddressbrownblockav:
include "spritedata/ingame sprites/brownblockalternativepalettevalue.tcs.gen"


;powerup sprites
characteraddressredmushroom:
include "spritedata/ingame sprites/redmushroom.tgs.gen"
coloraddressredmushroom:
include "spritedata/ingame sprites/redmushroom.tcs.gen"
characteraddressgreenmushroom:
include "spritedata/ingame sprites/greenmushroom.tgs.gen"
coloraddressgreenmushroom:
include "spritedata/ingame sprites/greenmushroom.tcs.gen"
characteraddressfeatherleft:
include "spritedata/ingame sprites/featherleft.tgs.gen"
coloraddressfeatherleft:
include "spritedata/ingame sprites/featherleft.tcs.gen"
characteraddressfeatherright:
include "spritedata/ingame sprites/featherright.tgs.gen"
coloraddressfeatherright:
include "spritedata/ingame sprites/featherright.tcs.gen"
characteraddressredflowerleft:
include "spritedata/ingame sprites/redflowerleft.tgs.gen"
coloraddressredflowerleft:
include "spritedata/ingame sprites/redflowerleft.tcs.gen"
characteraddressredflowerright:
include "spritedata/ingame sprites/redflowerright.tgs.gen"
coloraddressredflowerright:
include "spritedata/ingame sprites/redflowerright.tcs.gen"
characteraddressyellowflowerleft:
include "spritedata/ingame sprites/yellowflowerleft.tgs.gen"
coloraddressyellowflowerleft:
include "spritedata/ingame sprites/yellowflowerleft.tcs.gen"
characteraddressyellowflowerright:
include "spritedata/ingame sprites/yellowflowerright.tgs.gen"
coloraddressyellowflowerright:
include "spritedata/ingame sprites/yellowflowerright.tcs.gen"
characteraddressstaryellow:
include "spritedata/ingame sprites/staryellow.tgs.gen"
coloraddressstaryellow:
include "spritedata/ingame sprites/staryellow.tcs.gen"
characteraddressstarred:
include "spritedata/ingame sprites/starred.tgs.gen"
coloraddressstarred:
include "spritedata/ingame sprites/starred.tcs.gen"
characteraddressmoonpowerupsprite:
include "spritedata/ingame sprites/moonpowerupsprite.tgs.gen"
coloraddressmoonpowerupsprite:
include "spritedata/ingame sprites/moonpowerupsprite.tcs.gen"



;wolkjes na bijv killen object
coloraddresswhitesprite:
include "spritedata/ingame sprites/WhiteSprite.tcs.gen"
characteraddresscloud1sprite:
include "spritedata/ingame sprites/wolkje1.tgs.gen"
characteraddresscloud2sprite:
include "spritedata/ingame sprites/wolkje2.tgs.gen"
characteraddresscloud3sprite:
include "spritedata/ingame sprites/wolkje3.tgs.gen"
characteraddresscloud4sprite:
include "spritedata/ingame sprites/wolkje4.tgs.gen"
characteraddresscloud5sprite:
include "spritedata/ingame sprites/wolkje5.tgs.gen"
characteraddresscloud6sprite:
include "spritedata/ingame sprites/wolkje6.tgs.gen"
characteraddreskickorexplosioncloudleft:
include "spritedata/ingame sprites/kickorexplosioncloudleft.tgs.gen"
characteraddreskickorexplosioncloudright:
include "spritedata/ingame sprites/kickorexplosioncloudright.tgs.gen"
coloraddressyellowsprite:
include "spritedata/ingame sprites/singleyellowsprite.tcs.gen"
characteraddressstarlow:
include "spritedata/ingame sprites/star_low.tgs.gen"
characteraddressstarhigh:
include "spritedata/ingame sprites/star_high.tgs.gen"





;het ventje op de schuine tile in het begin van map 2
characteraddressslidingguysliding:
include "spritedata/ingame sprites/sliding_guy_sliding.tgs.gen"
coloraddressslidingguysliding:
include "spritedata/ingame sprites/sliding_guy_sliding.tcs.gen"
characteraddressslidingguyleft1:
include "spritedata/ingame sprites/sliding_guy_left1.tgs.gen"
coloraddressslidingguyleft1:
include "spritedata/ingame sprites/sliding_guy_left1.tcs.gen"
characteraddressslidingguyleft2:
include "spritedata/ingame sprites/sliding_guy_left2.tgs.gen"
coloraddressslidingguyleft2:
include "spritedata/ingame sprites/sliding_guy_left2.tcs.gen"
characteraddressslidingguyright1:
include "spritedata/ingame sprites/sliding_guy_right1.tgs.gen"
coloraddressslidingguyright1:
include "spritedata/ingame sprites/sliding_guy_right1.tcs.gen"
characteraddressslidingguyright2:
include "spritedata/ingame sprites/sliding_guy_right2.tgs.gen"
coloraddressslidingguyright2:
include "spritedata/ingame sprites/sliding_guy_right2.tcs.gen"
characteraddressslidingguyjumpedon:
include "spritedata/ingame sprites/sliding_guy_jumpedon.tgs.gen"
coloraddressslidingguyjumpedon:
include "spritedata/ingame sprites/sliding_guy_jumpedon.tcs.gen"

;mario bullets
coloraddressmariobulletyellow:
include "spritedata/ingame sprites/mariobullet_yellow.tcs.gen"
coloraddressmariobulletorange:
include "spritedata/ingame sprites/mariobullet_orange.tcs.gen"
characteraddressmariobullet1:
include "spritedata/ingame sprites/mariobullet1.tgs.gen"
characteraddressmariobullet2:
include "spritedata/ingame sprites/mariobullet2.tgs.gen"
characteraddressmariobullet3:
include "spritedata/ingame sprites/mariobullet3.tgs.gen"
characteraddressmariobullet4:
include "spritedata/ingame sprites/mariobullet4.tgs.gen"

;blockdestruction
characteraddressdestroyblocksprite1:
include "spritedata/ingame sprites/destroyblocksprite1.tgs.gen"
coloraddressdestroyblocksprite1:
include "spritedata/ingame sprites/destroyblocksprite1.tcs.gen"
characteraddressdestroyblocksprite2:
include "spritedata/ingame sprites/destroyblocksprite2.tgs.gen"
coloraddressdestroyblocksprite2:
include "spritedata/ingame sprites/destroyblocksprite2.tcs.gen"
characteraddressdestroyblocksprite3:
include "spritedata/ingame sprites/destroyblocksprite3.tgs.gen"
coloraddressdestroyblocksprite3:
include "spritedata/ingame sprites/destroyblocksprite3.tcs.gen"
characteraddressdestroyblocksprite4:
include "spritedata/ingame sprites/destroyblocksprite4.tgs.gen"
coloraddressdestroyblocksprite4:
include "spritedata/ingame sprites/destroyblocksprite4.tcs.gen"
characteraddressdestroyblocksprite5:
include "spritedata/ingame sprites/destroyblocksprite5.tgs.gen"
coloraddressdestroyblocksprite5:
include "spritedata/ingame sprites/destroyblocksprite5.tcs.gen"
characteraddressdestroyblocksprite6:
include "spritedata/ingame sprites/destroyblocksprite6.tgs.gen"
coloraddressdestroyblocksprite6:
include "spritedata/ingame sprites/destroyblocksprite6.tcs.gen"


;banzaibilldetsruction
characteraddressbigdestroyblocksprite1:
include "spritedata/ingame sprites/Bigdestroyblocksprite1.tgs.gen"
coloraddressbigdestroyblocksprite1:
include "spritedata/ingame sprites/Bigdestroyblocksprite1.tcs.gen"
characteraddressbigdestroyblocksprite2:
include "spritedata/ingame sprites/Bigdestroyblocksprite2.tgs.gen"
coloraddressbigdestroyblocksprite2:
include "spritedata/ingame sprites/Bigdestroyblocksprite2.tcs.gen"
characteraddressbigdestroyblocksprite3:
include "spritedata/ingame sprites/Bigdestroyblocksprite3.tgs.gen"
coloraddressbigdestroyblocksprite3:
include "spritedata/ingame sprites/Bigdestroyblocksprite3.tcs.gen"
characteraddressbigdestroyblocksprite4:
include "spritedata/ingame sprites/Bigdestroyblocksprite4.tgs.gen"
coloraddressbigdestroyblocksprite4:
include "spritedata/ingame sprites/Bigdestroyblocksprite4.tcs.gen"
characteraddressbigdestroyblocksprite5:
include "spritedata/ingame sprites/Bigdestroyblocksprite5.tgs.gen"
coloraddressbigdestroyblocksprite5:
include "spritedata/ingame sprites/Bigdestroyblocksprite5.tcs.gen"
characteraddressbigdestroyblocksprite6:
include "spritedata/ingame sprites/Bigdestroyblocksprite6.tgs.gen"
coloraddressbigdestroyblocksprite6:
include "spritedata/ingame sprites/Bigdestroyblocksprite6.tcs.gen"



;chuck
characteraddresschucksitlookingforwardjumpedon:
include "spritedata/ingame sprites/chucksitlookingforwardjumpedon.tgs.gen"
coloraddresschucksitlookingforwardjumpedon:
include "spritedata/ingame sprites/chucksitlookingforwardjumpedon.tcs.gen"
characteraddresschucksitlookingforward:
include "spritedata/ingame sprites/chucksitlookingforward.tgs.gen"
coloraddresschucksitlookingforward:
include "spritedata/ingame sprites/chucksitlookingforward.tcs.gen"
characteraddresschuckleftclap:
include "spritedata/ingame sprites/chuckleftclap.tgs.gen"
coloraddresschuckleftclap:
include "spritedata/ingame sprites/chuckleftclap.tcs.gen"
characteraddresschuckrightclap:
include "spritedata/ingame sprites/chuckrightclap.tgs.gen"
coloraddresschuckrightclap:
include "spritedata/ingame sprites/chuckrightclap.tcs.gen"
characteraddresschuckleftsit:
include "spritedata/ingame sprites/chuckleftsit.tgs.gen"
coloraddresschuckleftsit:
include "spritedata/ingame sprites/chuckleftsit.tcs.gen"
characteraddresschuckrightsit:
include "spritedata/ingame sprites/chuckrightsit.tgs.gen"
coloraddresschuckrightsit:
include "spritedata/ingame sprites/chuckrightsit.tcs.gen"
characteraddresschuckleftjump:
include "spritedata/ingame sprites/chuckleftjump.tgs.gen"
coloraddresschuckleftjump:
include "spritedata/ingame sprites/chuckleftjump.tcs.gen"
characteraddresschuckrightjump:
include "spritedata/ingame sprites/chuckrightjump.tgs.gen"
coloraddresschuckrightjump:
include "spritedata/ingame sprites/chuckrightjump.tcs.gen"
characteraddresschuckleftrun1:
include "spritedata/ingame sprites/chuckleftrun1.tgs.gen"
coloraddresschuckleftrun1:
include "spritedata/ingame sprites/chuckleftrun1.tcs.gen"
characteraddresschuckleftrun2:
include "spritedata/ingame sprites/chuckleftrun2.tgs.gen"
coloraddresschuckleftrun2:
include "spritedata/ingame sprites/chuckleftrun2.tcs.gen"
characteraddresschuckrightrun1:
include "spritedata/ingame sprites/chuckrightrun1.tgs.gen"
coloraddresschuckrightrun1:
include "spritedata/ingame sprites/chuckrightrun1.tcs.gen"
characteraddresschuckrightrun2:
include "spritedata/ingame sprites/chuckrightrun2.tgs.gen"
coloraddresschuckrightrun2:
include "spritedata/ingame sprites/chuckrightrun2.tcs.gen"


;animated questionmark
coloraddressquestionmarksinglesprite:
include "spritedata/ingame sprites/questionmark singlesprite.tcs.gen"
characteraddressquestionmarksinglesprite1:
include "spritedata/ingame sprites/questionmark1 singlesprite.tgs.gen"
characteraddressquestionmarksinglesprite2:
include "spritedata/ingame sprites/questionmark2 singlesprite.tgs.gen"
characteraddressquestionmarksinglesprite3:
include "spritedata/ingame sprites/questionmark3 singlesprite.tgs.gen"
characteraddressquestionmarksinglesprite4:
include "spritedata/ingame sprites/questionmark4 singlesprite.tgs.gen"

;exclamationmark 
coloraddressexclamationmarksinglesprite:
include "spritedata/ingame sprites/exclamationmarksinglesprite.tcs.gen"
characteraddressexclamationmarksinglesprite:
include "spritedata/ingame sprites/exclamationmarksinglesprite.tgs.gen"

;mariostar related sprites
characteraddressempty:
include "spritedata/ingame sprites/empty.tgs.gen"
coloraddressmariowhite:
include "spritedata/spriteconverter/mariowhite.tcs.gen"

;flyingbox
characteraddressflyingbox1:
include "spritedata/ingame sprites/flyingbox1.tgs.gen"
coloraddressflyingbox1:
include "spritedata/ingame sprites/flyingbox1.tcs.gen"
characteraddressflyingbox2:
include "spritedata/ingame sprites/flyingbox2.tgs.gen"
coloraddressflyingbox2:
include "spritedata/ingame sprites/flyingbox2.tcs.gen"

;bonus sprites
characteraddressoneupsprite:
include "spritedata/ingame sprites/oneupsprite.tgs.gen"
coloraddressoneupsprite:
include "spritedata/ingame sprites/oneupsprite.tcs.gen"


;banzai BILL!!
characteraddressbanzaibillbottom:
include "spritedata/ingame sprites/banzaibillbottom.tgs.gen"
coloraddressbanzaibillbottom:
include "spritedata/ingame sprites/banzaibillbottom.tcs.gen"
characteraddressbanzaibilltop:
include "spritedata/ingame sprites/banzaibilltop.tgs.gen"
coloraddressbanzaibilltop:
include "spritedata/ingame sprites/banzaibilltop.tcs.gen"

;mariofun
characteraddressredbaloon:
include "spritedata/ingame sprites/baloon.tgs.gen"
coloraddressredbaloon:
include "spritedata/ingame sprites/baloon.tcs.gen"

;tubecoverups
characteraddresstubecoverupspritevertical:
include "spritedata/ingame sprites level 01/tubecoverupspritevertical.tgs.gen"
coloraddresstubecoverupspritevertical:
include "spritedata/ingame sprites level 01/tubecoverupspritevertical.tcs.gen"
characteraddresstubecoverupspritehorizontal:
include "spritedata/ingame sprites level 01/tubecoverupspritehorizontal.tgs.gen"
coloraddresstubecoverupspritehorizontal:
include "spritedata/ingame sprites level 01/tubecoverupspritehorizontal.tcs.gen"

;yoshi coin
characteraddressyoshicoin1:
include "spritedata/ingame sprites/yoshicoin1.tgs.gen"
coloraddressyoshicoin1:
include "spritedata/ingame sprites/yoshicoin1.tcs.gen"
characteraddressyoshicoin2:
include "spritedata/ingame sprites/yoshicoin2.tgs.gen"
coloraddressyoshicoin2:
include "spritedata/ingame sprites/yoshicoin2.tcs.gen"
characteraddressyoshicoin3:
include "spritedata/ingame sprites/yoshicoin3.tgs.gen"
coloraddressyoshicoin3:
include "spritedata/ingame sprites/yoshicoin3.tcs.gen"


;normal coin
characteraddresscoin1a:
include "spritedata/ingame sprites/coin1a.tgs.gen"
coloraddresscoin1a:
include "spritedata/ingame sprites/coin1a.tcs.gen"
characteraddresscoin1b:
include "spritedata/ingame sprites/coin1b.tgs.gen"
coloraddresscoin1b:
include "spritedata/ingame sprites/coin1b.tcs.gen"
characteraddresscoin1c:
include "spritedata/ingame sprites/coin1c.tgs.gen"
coloraddresscoin1c:
include "spritedata/ingame sprites/coin1c.tcs.gen"
characteraddresscoin1d:
include "spritedata/ingame sprites/coin1d.tgs.gen"
coloraddresscoin1d:
include "spritedata/ingame sprites/coin1d.tcs.gen"


;brown coopa map02
characteraddressKoopaLeft1:
include "spritedata/ingame sprites level 02/KoopaLeft1.tgs.gen"
coloraddressKoopaLeft1:
include "spritedata/ingame sprites level 02/KoopaLeft1.tcs.gen"
characteraddressKoopaLeft2:
include "spritedata/ingame sprites level 02/KoopaLeft2.tgs.gen"
coloraddressKoopaLeft2:
include "spritedata/ingame sprites level 02/KoopaLeft2.tcs.gen"
characteraddressKooparight1:
include "spritedata/ingame sprites level 02/KoopaRight1.tgs.gen"
coloraddressKooparight1:
include "spritedata/ingame sprites level 02/KoopaRight1.tcs.gen"
characteraddressKooparight2:
include "spritedata/ingame sprites level 02/KoopaRight2.tgs.gen"
coloraddressKooparight2:
include "spritedata/ingame sprites level 02/KoopaRight2.tcs.gen"
characteraddressKoopaLeftTurn:
include "spritedata/ingame sprites level 02/KoopaLeftTurn.tgs.gen"
coloraddressKoopaLeftTurn:
include "spritedata/ingame sprites level 02/KoopaLeftTurn.tcs.gen"
characteraddressKooparightTurn:
include "spritedata/ingame sprites level 02/KoopaRightTurn.tgs.gen"
coloraddressKooparightTurn:
include "spritedata/ingame sprites level 02/KoopaRightTurn.tcs.gen"


;naked brown coopa
characteraddressNakedKoopaLeftSlide1:
include "spritedata/ingame sprites level 02/NakedKoopaLeftSlide1.tgs.gen"
coloraddressNakedKoopaLeftSlide1:
include "spritedata/ingame sprites level 02/NakedKoopaLeftSlide1.tcs.gen"
characteraddressNakedKoopaLeftSlide2:
include "spritedata/ingame sprites level 02/NakedKoopaLeftSlide2.tgs.gen"
coloraddressNakedKoopaLeftSlide2:
include "spritedata/ingame sprites level 02/NakedKoopaLeftSlide2.tcs.gen"
characteraddressNakedKooparightSlide1:
include "spritedata/ingame sprites level 02/NakedKoopaRightSlide1.tgs.gen"
coloraddressNakedKooparightSlide1:
include "spritedata/ingame sprites level 02/NakedKoopaRightSlide1.tcs.gen"
characteraddressNakedKooparightSlide2:
include "spritedata/ingame sprites level 02/NakedKoopaRightSlide2.tgs.gen"
coloraddressNakedKooparightSlide2:
include "spritedata/ingame sprites level 02/NakedKoopaRightSlide2.tcs.gen"
characteraddressNakedKoopaLeft1:
include "spritedata/ingame sprites level 02/NakedKoopaLeft1.tgs.gen"
coloraddressNakedKoopaLeft1:
include "spritedata/ingame sprites level 02/NakedKoopaLeft1.tcs.gen"
characteraddressNakedKoopaLeft2:
include "spritedata/ingame sprites level 02/NakedKoopaLeft2.tgs.gen"
coloraddressNakedKoopaLeft2:
include "spritedata/ingame sprites level 02/NakedKoopaLeft2.tcs.gen"
characteraddressNakedKooparight1:
include "spritedata/ingame sprites level 02/NakedKoopaRight1.tgs.gen"
coloraddressNakedKooparight1:
include "spritedata/ingame sprites level 02/NakedKoopaRight1.tcs.gen"
characteraddressNakedKooparight2:
include "spritedata/ingame sprites level 02/NakedKoopaRight2.tgs.gen"
coloraddressNakedKooparight2:
include "spritedata/ingame sprites level 02/NakedKoopaRight2.tcs.gen"
characteraddressNakedKoopaLeftUpSideDown:
include "spritedata/ingame sprites level 02/NakedKoopaLeftUpSideDown.tgs.gen"
coloraddressNakedKoopaLeftUpSideDown:
include "spritedata/ingame sprites level 02/NakedKoopaLeftUpSideDown.tcs.gen"
characteraddressNakedKooparightUpSideDown:
include "spritedata/ingame sprites level 02/NakedKoopaRightUpSideDown.tgs.gen"
coloraddressNakedKooparightUpSideDown:
include "spritedata/ingame sprites level 02/NakedKoopaRightUpSideDown.tcs.gen"

characteraddressShellBrownKoopaEnters:
include "spritedata/ingame sprites level 02/ShellBrownKoopaEnters.tgs.gen"
coloraddressShellBrownKoopaEnters:
include "spritedata/ingame sprites level 02/ShellBrownKoopaEnters.tcs.gen"

;brown shell
characteraddressShellBrown1:
include "spritedata/ingame sprites level 02/ShellBrown1.tgs.gen"
coloraddressShellBrown1:
include "spritedata/ingame sprites level 02/ShellBrown1.tcs.gen"
characteraddressShellBrown2:
include "spritedata/ingame sprites level 02/ShellBrown2.tgs.gen"
coloraddressShellBrown2:
include "spritedata/ingame sprites level 02/ShellBrown2.tcs.gen"
characteraddressShellBrown3:
include "spritedata/ingame sprites level 02/ShellBrown3.tgs.gen"
coloraddressShellBrown3:
include "spritedata/ingame sprites level 02/ShellBrown3.tcs.gen"
characteraddressShellBrown4:
include "spritedata/ingame sprites level 02/ShellBrown4.tgs.gen"
coloraddressShellBrown4:
include "spritedata/ingame sprites level 02/ShellBrown4.tcs.gen"
characteraddressShellBrownupsidedown:
include "spritedata/ingame sprites level 02/ShellBrownupsidedown.tgs.gen"
coloraddressShellBrownupsidedown:
include "spritedata/ingame sprites level 02/ShellBrownupsidedown.tcs.gen"
characteraddressShellBrownupsidedownturn:
include "spritedata/ingame sprites level 02/ShellBrownupsidedownturn.tgs.gen"
coloraddressShellBrownupsidedownturn:
include "spritedata/ingame sprites level 02/ShellBrownupsidedownturn.tcs.gen"
characteraddressShellBrownKoopaEntersupsidedown:
include "spritedata/ingame sprites level 02/ShellBrownKoopaEntersupsidedown.tgs.gen"
coloraddressShellBrownKoopaEntersupsidedown:
include "spritedata/ingame sprites level 02/ShellBrownKoopaEntersupsidedown.tcs.gen"

;=================================================================================

;green coopa map02
characteraddressgreen_koopaLeft1map02:
include "spritedata/ingame sprites level 02/Green_KoopaLeft1.tgs.gen"
coloraddressgreen_koopaLeft1map02:
include "spritedata/ingame sprites level 02/Green_KoopaLeft1.tcs.gen"
characteraddressgreen_koopaLeft2map02:
include "spritedata/ingame sprites level 02/Green_KoopaLeft2.tgs.gen"
coloraddressgreen_koopaLeft2map02:
include "spritedata/ingame sprites level 02/Green_KoopaLeft2.tcs.gen"
characteraddressgreen_kooparight1map02:
include "spritedata/ingame sprites level 02/Green_KoopaRight1.tgs.gen"
coloraddressgreen_kooparight1map02:
include "spritedata/ingame sprites level 02/Green_KoopaRight1.tcs.gen"
characteraddressgreen_kooparight2map02:
include "spritedata/ingame sprites level 02/Green_KoopaRight2.tgs.gen"
coloraddressgreen_kooparight2map02:
include "spritedata/ingame sprites level 02/Green_KoopaRight2.tcs.gen"
characteraddressgreen_koopaLeftTurnmap02:
include "spritedata/ingame sprites level 02/Green_KoopaLeftTurn.tgs.gen"
coloraddressgreen_koopaLeftTurnmap02:
include "spritedata/ingame sprites level 02/Green_KoopaLeftTurn.tcs.gen"
characteraddressgreen_kooparightTurnmap02:
include "spritedata/ingame sprites level 02/Green_KoopaRightTurn.tgs.gen"
coloraddressgreen_kooparightTurnmap02:
include "spritedata/ingame sprites level 02/Green_KoopaRightTurn.tcs.gen"


;naked green coopa map02
characteraddressgreen_nakedkoopaLeftSlide1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftSlide1.tgs.gen"
coloraddressgreen_nakedkoopaLeftSlide1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftSlide1.tcs.gen"
characteraddressgreen_nakedkoopaLeftSlide2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftSlide2.tgs.gen"
coloraddressgreen_nakedkoopaLeftSlide2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftSlide2.tcs.gen"
characteraddressgreen_nakedkooparightSlide1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightSlide1.tgs.gen"
coloraddressgreen_nakedkooparightSlide1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightSlide1.tcs.gen"
characteraddressgreen_nakedkooparightSlide2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightSlide2.tgs.gen"
coloraddressgreen_nakedkooparightSlide2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightSlide2.tcs.gen"
characteraddressgreen_nakedkoopaLeft1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeft1.tgs.gen"
coloraddressgreen_nakedkoopaLeft1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeft1.tcs.gen"
characteraddressgreen_nakedkoopaLeft2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeft2.tgs.gen"
coloraddressgreen_nakedkoopaLeft2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeft2.tcs.gen"
characteraddressgreen_nakedkooparight1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRight1.tgs.gen"
coloraddressgreen_nakedkooparight1map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRight1.tcs.gen"
characteraddressgreen_nakedkooparight2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRight2.tgs.gen"
coloraddressgreen_nakedkooparight2map02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRight2.tcs.gen"
characteraddressgreen_nakedkoopaLeftUpSideDownmap02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftUpSideDown.tgs.gen"
coloraddressgreen_nakedkoopaLeftUpSideDownmap02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaLeftUpSideDown.tcs.gen"
characteraddressgreen_nakedkooparightUpSideDownmap02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightUpSideDown.tgs.gen"
coloraddressgreen_nakedkooparightUpSideDownmap02:
include "spritedata/ingame sprites level 02/Green_NakedKoopaRightUpSideDown.tcs.gen"

characteraddressShellgreenKoopaEntersmap02:
include "spritedata/ingame sprites level 02/ShellGreenKoopaEnters.tgs.gen"
coloraddressShellgreenKoopaEntersmap02:
include "spritedata/ingame sprites level 02/ShellGreenKoopaEnters.tcs.gen"

;green shell
characteraddressShellgreen1map02:
include "spritedata/ingame sprites level 02/ShellGreen1.tgs.gen"
coloraddressShellgreen1map02:
include "spritedata/ingame sprites level 02/ShellGreen1.tcs.gen"
characteraddressShellgreen2map02:
include "spritedata/ingame sprites level 02/ShellGreen2.tgs.gen"
coloraddressShellgreen2map02:
include "spritedata/ingame sprites level 02/ShellGreen2.tcs.gen"
characteraddressShellgreen3map02:
include "spritedata/ingame sprites level 02/ShellGreen3.tgs.gen"
coloraddressShellgreen3map02:
include "spritedata/ingame sprites level 02/ShellGreen3.tcs.gen"
characteraddressShellgreen4map02:
include "spritedata/ingame sprites level 02/ShellGreen4.tgs.gen"
coloraddressShellgreen4map02:
include "spritedata/ingame sprites level 02/ShellGreen4.tcs.gen"
characteraddressShellgreenupsidedownmap02:
include "spritedata/ingame sprites level 02/ShellGreenupsidedown.tgs.gen"
coloraddressShellgreenupsidedownmap02:
include "spritedata/ingame sprites level 02/ShellGreenupsidedown.tcs.gen"
characteraddressShellgreenupsidedownturnmap02:
include "spritedata/ingame sprites level 02/ShellGreenupsidedownturn.tgs.gen"
coloraddressShellgreenupsidedownturnmap02:
include "spritedata/ingame sprites level 02/ShellGreenupsidedownturn.tcs.gen"
characteraddressShellgreenKoopaEntersupsidedownmap02:
include "spritedata/ingame sprites level 02/ShellGreenKoopaEntersupsidedown.tgs.gen"
coloraddressShellgreenKoopaEntersupsidedownmap02:
include "spritedata/ingame sprites level 02/ShellGreenKoopaEntersupsidedown.tcs.gen"

;-------------------------------------------------------------------------------------


;monty mole
characteraddressMontyMoleAppears1:
include "spritedata/ingame sprites level 02/MontyMoleAppears1.tgs.gen"
coloraddressMontyMoleAppears1:
include "spritedata/ingame sprites level 02/MontyMoleAppears1.tcs.gen"
characteraddressMontyMoleAppears2:
include "spritedata/ingame sprites level 02/MontyMoleAppears2.tgs.gen"
coloraddressMontyMoleAppears2:
include "spritedata/ingame sprites level 02/MontyMoleAppears2.tcs.gen"
characteraddressMontyMoleAppears3:
include "spritedata/ingame sprites level 02/MontyMoleAppears3.tgs.gen"
coloraddressMontyMoleAppears3:
include "spritedata/ingame sprites level 02/MontyMoleAppears3.tcs.gen"
characteraddressMontyMoleAppears4:
include "spritedata/ingame sprites level 02/MontyMoleAppears4.tgs.gen"
coloraddressMontyMoleAppears4:
include "spritedata/ingame sprites level 02/MontyMoleAppears4.tcs.gen"
characteraddressMontyMoleAppears5:
include "spritedata/ingame sprites level 02/MontyMoleAppears5.tgs.gen"
coloraddressMontyMoleAppears5:
include "spritedata/ingame sprites level 02/MontyMoleAppears5.tcs.gen"
;monty debris
characteraddressMontyMolePieceofRock1:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock1.tgs.gen"
coloraddressMontyMolePieceofRock1:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock1.tcs.gen"
characteraddressMontyMolePieceofRock2:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock2.tgs.gen"
coloraddressMontyMolePieceofRock2:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock2.tcs.gen"
characteraddressMontyMolePieceofRock3:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock3.tgs.gen"
coloraddressMontyMolePieceofRock3:
include "spritedata/ingame sprites level 02/MontyMolePieceofRock3.tcs.gen"
;monty himself
characteraddressMontyMoleJumpUp:
include "spritedata/ingame sprites level 02/MontyMoleJumpUp.tgs.gen"
coloraddressMontyMoleJumpUp:
include "spritedata/ingame sprites level 02/MontyMoleJumpUp.tcs.gen"
characteraddressMontyMoleLeft1:
include "spritedata/ingame sprites level 02/MontyMoleLeft1.tgs.gen"
coloraddressMontyMoleLeft1:
include "spritedata/ingame sprites level 02/MontyMoleLeft1.tcs.gen"
characteraddressMontyMoleLeft2:
include "spritedata/ingame sprites level 02/MontyMoleLeft2.tgs.gen"
coloraddressMontyMoleLeft2:
include "spritedata/ingame sprites level 02/MontyMoleLeft2.tcs.gen"
characteraddressMontyMoleright1:
include "spritedata/ingame sprites level 02/MontyMoleRight1.tgs.gen"
coloraddressMontyMoleright1:
include "spritedata/ingame sprites level 02/MontyMoleRight1.tcs.gen"
characteraddressMontyMoleright2:
include "spritedata/ingame sprites level 02/MontyMoleRight2.tgs.gen"
coloraddressMontyMoleright2:
include "spritedata/ingame sprites level 02/MontyMoleRight2.tcs.gen"

characteraddressMontyMoleUpsidedownleft:
include "spritedata/ingame sprites level 02/MontyMoleUpsidedownleft.tgs.gen"
coloraddressMontyMoleUpsidedownleft:
include "spritedata/ingame sprites level 02/MontyMoleUpsidedownleft.tcs.gen"
characteraddressMontyMoleUpsidedownright:
include "spritedata/ingame sprites level 02/MontyMoleUpsidedownright.tgs.gen"
coloraddressMontyMoleUpsidedownright:
include "spritedata/ingame sprites level 02/MontyMoleUpsidedownright.tcs.gen"

;klimplant
characteraddressgrowingplant1:
include "spritedata/ingame sprites level 02/growingplant1.tgs.gen"
coloraddressgrowingplant1:
include "spritedata/ingame sprites level 02/growingplant1.tcs.gen"
characteraddressgrowingplant2:
include "spritedata/ingame sprites level 02/growingplant2.tgs.gen"
coloraddressgrowingplant2:
include "spritedata/ingame sprites level 02/growingplant2.tcs.gen"


;sparkles
characteraddresssparkles1:
include "spritedata/ingame sprites/sparkles1.tgs.gen"
characteraddresssparkles2:
include "spritedata/ingame sprites/sparkles2.tgs.gen"
characteraddresssparkles3:
include "spritedata/ingame sprites/sparkles3.tgs.gen"
characteraddresssparkles4:
include "spritedata/ingame sprites/sparkles4.tgs.gen"
characteraddresssparkles5:
include "spritedata/ingame sprites/sparkles5.tgs.gen"
characteraddresssparkles6:
include "spritedata/ingame sprites/sparkles6.tgs.gen"
characteraddresssparkles7:
include "spritedata/ingame sprites/sparkles7.tgs.gen"
characteraddresssparkles8:
include "spritedata/ingame sprites/sparkles8.tgs.gen"
characteraddresssparkles9:
include "spritedata/ingame sprites/sparkles9.tgs.gen"
coloraddresssparklescolor:
include "spritedata/ingame sprites/sparklescolor.tcs.gen"

;p-box
characteraddresspbox:
include "spritedata/ingame sprites/P-Box.tgs.gen"
coloraddresspbox:
include "spritedata/ingame sprites/P-Box.tcs.gen"
characteraddresspboxjumpedon:
include "spritedata/ingame sprites/P-BoxJumpedOn.tgs.gen"
coloraddresspboxjumpedon:
include "spritedata/ingame sprites/P-BoxJumpedOn.tcs.gen"


;yoshi all green sprites including the baby yoshi
;body
;left
characteraddressyoshiebodygrableft:
include "spritedata/ingame sprites level 02/l_yoshiebody_grab.tgs.gen"
coloraddressyoshiebodygrableft:
include "spritedata/ingame sprites level 02/l_yoshiebody_grab.tcs.gen"
characteraddressyoshiebodyjumpleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_jump.tgs.gen"
coloraddressyoshiebodyjumpleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_jump.tcs.gen"
characteraddressyoshiebodysitleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_sit.tgs.gen"
coloraddressyoshiebodysitleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_sit.tcs.gen"
characteraddressyoshiebodyturnleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_turn.tgs.gen"
coloraddressyoshiebodyturnleft:
include "spritedata/ingame sprites level 02/l_yoshiebody_turn.tcs.gen"
characteraddressyoshiebodywalk1left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk1.tgs.gen"
coloraddressyoshiebodywalk1left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk1.tcs.gen"
characteraddressyoshiebodywalk2left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk2.tgs.gen"
coloraddressyoshiebodywalk2left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk2.tcs.gen"
characteraddressyoshiebodywalk3left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk3.tgs.gen"
coloraddressyoshiebodywalk3left:
include "spritedata/ingame sprites level 02/l_yoshiebody_walk3.tcs.gen"

;right
characteraddressyoshiebodygrabright:
include "spritedata/ingame sprites level 02/r_yoshiebody_grab.tgs.gen"
coloraddressyoshiebodygrabright:
include "spritedata/ingame sprites level 02/r_yoshiebody_grab.tcs.gen"
characteraddressyoshiebodyjumpright:
include "spritedata/ingame sprites level 02/r_yoshiebody_jump.tgs.gen"
coloraddressyoshiebodyjumpright:
include "spritedata/ingame sprites level 02/r_yoshiebody_jump.tcs.gen"
characteraddressyoshiebodysitright:
include "spritedata/ingame sprites level 02/r_yoshiebody_sit.tgs.gen"
coloraddressyoshiebodysitright:
include "spritedata/ingame sprites level 02/r_yoshiebody_sit.tcs.gen"
characteraddressyoshiebodyturnright:
include "spritedata/ingame sprites level 02/r_yoshiebody_turn.tgs.gen"
coloraddressyoshiebodyturnright:
include "spritedata/ingame sprites level 02/r_yoshiebody_turn.tcs.gen"
characteraddressyoshiebodywalk1right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk1.tgs.gen"
coloraddressyoshiebodywalk1right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk1.tcs.gen"
characteraddressyoshiebodywalk2right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk2.tgs.gen"
coloraddressyoshiebodywalk2right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk2.tcs.gen"
characteraddressyoshiebodywalk3right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk3.tgs.gen"
coloraddressyoshiebodywalk3right:
include "spritedata/ingame sprites level 02/r_yoshiebody_walk3.tcs.gen"

;head
;left
characteraddressyoshieheadgrableft:
include "spritedata/ingame sprites level 02/l_yoshiehead_grab.tgs.gen"
coloraddressyoshieheadgrableft:
include "spritedata/ingame sprites level 02/l_yoshiehead_grab.tcs.gen"
characteraddressyoshieheadhurtleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_hurt.tgs.gen"
coloraddressyoshieheadhurtleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_hurt.tcs.gen"
characteraddressyoshieheadnormalleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_normal.tgs.gen"
coloraddressyoshieheadnormalleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_normal.tcs.gen"
characteraddressyoshieheadopenmouthleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_openmouth.tgs.gen"
coloraddressyoshieheadopenmouthleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_openmouth.tcs.gen"
characteraddressyoshieheadturnleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_turn.tgs.gen"
coloraddressyoshieheadturnleft:
include "spritedata/ingame sprites level 02/l_yoshiehead_turn.tcs.gen"
characteraddressyoshieheadeat1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat1.tgs.gen"
coloraddressyoshieheadeat1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat1.tcs.gen"
characteraddressyoshieheadeat2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat2.tgs.gen"
coloraddressyoshieheadeat2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat2.tcs.gen"
characteraddressyoshieheadeat3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat3.tgs.gen"
coloraddressyoshieheadeat3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_eat3.tcs.gen"


;right
characteraddressyoshieheadgrabright:
include "spritedata/ingame sprites level 02/r_yoshiehead_grab.tgs.gen"
coloraddressyoshieheadgrabright:
include "spritedata/ingame sprites level 02/r_yoshiehead_grab.tcs.gen"
characteraddressyoshieheadhurtright:
include "spritedata/ingame sprites level 02/r_yoshiehead_hurt.tgs.gen"
coloraddressyoshieheadhurtright:
include "spritedata/ingame sprites level 02/r_yoshiehead_hurt.tcs.gen"
characteraddressyoshieheadnormalright:
include "spritedata/ingame sprites level 02/r_yoshiehead_normal.tgs.gen"
coloraddressyoshieheadnormalright:
include "spritedata/ingame sprites level 02/r_yoshiehead_normal.tcs.gen"
characteraddressyoshieheadopenmouthright:
include "spritedata/ingame sprites level 02/r_yoshiehead_openmouth.tgs.gen"
coloraddressyoshieheadopenmouthright:
include "spritedata/ingame sprites level 02/r_yoshiehead_openmouth.tcs.gen"
characteraddressyoshieheadturnright:
include "spritedata/ingame sprites level 02/r_yoshiehead_turn.tgs.gen"
coloraddressyoshieheadturnright:
include "spritedata/ingame sprites level 02/r_yoshiehead_turn.tcs.gen"
characteraddressyoshieheadeat1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat1.tgs.gen"
coloraddressyoshieheadeat1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat1.tcs.gen"
characteraddressyoshieheadeat2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat2.tgs.gen"
coloraddressyoshieheadeat2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat2.tcs.gen"
characteraddressyoshieheadeat3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat3.tgs.gen"
coloraddressyoshieheadeat3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_eat3.tcs.gen"


;yoshi egg
characteraddressyoshieEgg:
include "spritedata/ingame sprites level 02/yoshieEgg.tgs.gen"
coloraddressyoshieEgg:
include "spritedata/ingame sprites level 02/yoshieEgg.tcs.gen"
characteraddressyoshieEggcracked:
include "spritedata/ingame sprites level 02/yoshieEggCracked.tgs.gen"
coloraddressyoshieEggcracked:
include "spritedata/ingame sprites level 02/yoshieEggCracked.tcs.gen"


;baby yoshi
characteraddressbabyyoshiebig:
include "spritedata/ingame sprites level 02/babyyoshiebig.tgs.gen"
coloraddressbabyyoshiebig:
include "spritedata/ingame sprites level 02/babyyoshiebig.tcs.gen"
characteraddressbabyyoshiesmall:
include "spritedata/ingame sprites level 02/babyyoshiesmall.tgs.gen"
coloraddressbabyyoshiesmall:
include "spritedata/ingame sprites level 02/babyyoshiesmall.tcs.gen"

;egg crack feather sprite
characteraddressfeather1:
include "spritedata/ingame sprites level 02/feather1.tgs.gen"
coloraddressfeather1:
include "spritedata/ingame sprites level 02/feather1.tcs.gen"
characteraddressfeather2:
include "spritedata/ingame sprites level 02/feather2.tgs.gen"
coloraddressfeather2:
include "spritedata/ingame sprites level 02/feather2.tcs.gen"


endingamespritesblock:

	ds		$c000-$,$ff
dephase

;
; block $1f
;
SeeSfxReplayerblock:  equ $1f
phase	$4000
  include "seesfxreplayer.asm"
  include "seesfxreplayerd.asm"
  include "seesfxreplayerd2.asm"
	ds		$6000-$,$ff
dephase

;
; block $20 - $21
;
Sfxblock: equ $20
phase	$8000
incbin "sfxdata/SMW.SEE"
	ds		$c000-$,$ff
dephase

;
; block $22 - $23
;
romprogblock2: equ $22
phase	$4000


include "handleobjectmovement/animatekoopa.asm"
include "handleobjectmovement/animateflyingkoopa.asm" ;merge with above file when done!!
include "handleobjectmovement/handleelevator.asm"
include "handleobjectmovement/generalcode.asm"
include "handleyoshitongue.asm" ;run from here because of object spawning isues
include "handlescorebar.asm"

romprogblock2end:

	ds		$8000-$,$ff
dephase

;
; block $24 - $25
;
romprogblock3: equ $24
phase	$8000
  
  include "handleobjectmovement/handleobjectmovement.asm"

romprogblock3end:  
  
	ds		$c000-$,$ff
dephase


;
; block $26 - $27
;
;mariosmallfire sprite block
phase	$4000

;0
include "spritedata/spriteconverter/r_smallshootingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_walk1.tcs.gen"
;192
include "spritedata/spriteconverter/r_smallshootingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_walk2.tcs.gen"
;384
include "spritedata/spriteconverter/l_smallshootingmario_walk1.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_walk1.tcs.gen"
;576
include "spritedata/spriteconverter/l_smallshootingmario_walk2.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_walk2.tcs.gen"
;768
include "spritedata/spriteconverter/l_smallshootingmario_jump.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_jump.tcs.gen"
;960
include "spritedata/spriteconverter/r_smallshootingmario_jump.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_jump.tcs.gen"
;1152
include "spritedata/spriteconverter/l_smallshootingmario_fall.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_fall.tcs.gen"
;1344
include "spritedata/spriteconverter/r_smallshootingmario_fall.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_fall.tcs.gen"
;1536
include "spritedata/spriteconverter/l_smallshootingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_sprint1.tcs.gen"
;1728
include "spritedata/spriteconverter/l_smallshootingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_sprint2.tcs.gen"
;1920
include "spritedata/spriteconverter/r_smallshootingmario_sprint1.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_sprint1.tcs.gen"
;2112
include "spritedata/spriteconverter/r_smallshootingmario_sprint2.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_sprint2.tcs.gen"
;2304
include "spritedata/spriteconverter/l_smallshootingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_sprintjump.tcs.gen"
;2496
include "spritedata/spriteconverter/r_smallshootingmario_sprintjump.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_sprintjump.tcs.gen"
;2688
include "spritedata/spriteconverter/l_smallshootingmario_down.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_down.tcs.gen"
;2880
include "spritedata/spriteconverter/r_smallshootingmario_down.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_down.tcs.gen"
;3072
include "spritedata/spriteconverter/l_smallshootingmario_lookup.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_lookup.tcs.gen"
;3264
include "spritedata/spriteconverter/r_smallshootingmario_lookup.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_lookup.tcs.gen"
;3456
include "spritedata/spriteconverter/l_smallshootingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_turnaround.tcs.gen"
;3648
include "spritedata/spriteconverter/r_smallshootingmario_turnaround.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_turnaround.tcs.gen"
;3840
include "spritedata/spriteconverter/smallshootingmario_back2camera.tgs.gen"
include "spritedata/spriteconverter/smallshootingmario_back2camera.tcs.gen"
;4032
include "spritedata/spriteconverter/smallshootingmario_front2camera.tgs.gen"
include "spritedata/spriteconverter/smallshootingmario_front2camera.tcs.gen"
;4224
include "spritedata/spriteconverter/l_smallshootingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_holdshell.tcs.gen"
;4416
include "spritedata/spriteconverter/r_smallshootingmario_holdshell.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_holdshell.tcs.gen"
;4608
include "spritedata/spriteconverter/l_smallshootingmario_runwithshell.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_runwithshell.tcs.gen"
;4800
include "spritedata/spriteconverter/r_smallshootingmario_runwithshell.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_runwithshell.tcs.gen"
;4992
include "spritedata/spriteconverter/smallshootingmario_turnwithshell1.tgs.gen"
include "spritedata/spriteconverter/smallshootingmario_turnwithshell1.tcs.gen"
;5184
include "spritedata/spriteconverter/smallshootingmario_turnwithshell2.tgs.gen"
include "spritedata/spriteconverter/smallshootingmario_turnwithshell2.tcs.gen"
;5376
include "spritedata/spriteconverter/l_smallshootingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_takeshell.tcs.gen"
;5568
include "spritedata/spriteconverter/r_smallshootingmario_takeshell.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_takeshell.tcs.gen"
;5760
include "spritedata/spriteconverter/l_smallshootingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_lookupwithshell.tcs.gen"
;5952
include "spritedata/spriteconverter/r_smallshootingmario_lookupwithshell.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_lookupwithshell.tcs.gen"
;6144
include "spritedata/spriteconverter/l_smallshootingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_swim1.tcs.gen"
;6336
include "spritedata/spriteconverter/r_smallshootingmario_swim1.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_swim1.tcs.gen"
;6528
include "spritedata/spriteconverter/l_smallshootingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/l_smallshootingmario_swim2.tcs.gen"
;6720
include "spritedata/spriteconverter/r_smallshootingmario_swim2.tgs.gen"
include "spritedata/spriteconverter/r_smallshootingmario_swim2.tcs.gen"
;=========peace marios============================================================
;6912
include "spritedata/spriteconverter/mario_peace.tgs.gen"
include "spritedata/spriteconverter/mario_peace.tcs.gen"
;7104
include "spritedata/spriteconverter/supermario_peace.tgs.gen"
include "spritedata/spriteconverter/supermario_peace.tcs.gen"
;7296
include "spritedata/spriteconverter/shootingmario_peace.tgs.gen"
include "spritedata/spriteconverter/shootingmario_peace.tcs.gen"
;7488
include "spritedata/spriteconverter/flyingmario_peace.tgs.gen"
include "spritedata/spriteconverter/flyingmario_peace.tcs.gen"
;=====================================[supermarioonyoshi]==========================================
;7680
include "spritedata/spriteconverter/l_supermario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/l_supermario_sityoshie.tcs.gen"
;7872
include "spritedata/spriteconverter/r_supermario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sityoshie.tcs.gen"
;8064 
include "spritedata/spriteconverter/l_supermario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/l_supermario_attackyoshie1.tcs.gen"
;8256
include "spritedata/spriteconverter/l_supermario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/l_supermario_attackyoshie2.tcs.gen"
;8448
include "spritedata/spriteconverter/r_supermario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/r_supermario_attackyoshie1.tcs.gen"
;8640
include "spritedata/spriteconverter/r_supermario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/r_supermario_attackyoshie2.tcs.gen"
;8832
include "spritedata/spriteconverter/l_supermario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/l_supermario_turnyoshie.tcs.gen"
;9024
include "spritedata/spriteconverter/r_supermario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/r_supermario_turnyoshie.tcs.gen"
;======================================[firemario on yoshi]==============================
;9216
include "spritedata/spriteconverter/l_shootingmario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_sityoshie.tcs.gen"
;9408
include "spritedata/spriteconverter/r_shootingmario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sityoshie.tcs.gen"
;9600
include "spritedata/spriteconverter/l_shootingmario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_attackyoshie1.tcs.gen"
;9792
include "spritedata/spriteconverter/l_shootingmario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_attackyoshie2.tcs.gen"
;9984
include "spritedata/spriteconverter/r_shootingmario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_attackyoshie1.tcs.gen"
;10176
include "spritedata/spriteconverter/r_shootingmario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_attackyoshie2.tcs.gen"
;10368
include "spritedata/spriteconverter/l_shootingmario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/l_shootingmario_turnyoshie.tcs.gen"
;10560
include "spritedata/spriteconverter/r_shootingmario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_turnyoshie.tcs.gen"
;======================================[flying mario on yoshi]===========================
;10752
include "spritedata/spriteconverter/l_flyingmario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_sityoshie.tcs.gen"
;10944
include "spritedata/spriteconverter/r_flyingmario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sityoshie.tcs.gen"
;11136
include "spritedata/spriteconverter/l_flyingmario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_attackyoshie1.tcs.gen"
;11328
include "spritedata/spriteconverter/l_flyingmario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_attackyoshie2.tcs.gen"
;11520
include "spritedata/spriteconverter/r_flyingmario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_attackyoshie1.tcs.gen"
;11712
include "spritedata/spriteconverter/r_flyingmario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_attackyoshie2.tcs.gen"
;11904
include "spritedata/spriteconverter/l_flyingmario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/l_flyingmario_turnyoshie.tcs.gen"
;12096
include "spritedata/spriteconverter/r_flyingmario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_turnyoshie.tcs.gen"
;======================================[normal mario on yoshi]==============================
;12288
include "spritedata/spriteconverter/l_mario_sityoshie.tgs.gen"
;include "spritedata/spriteconverter/l_mario_sityoshie.tcs.gen"
include "spritedata/spriteconverter/r_mario_sityoshie.tcs.gen"
;12480
include "spritedata/spriteconverter/r_mario_sityoshie.tgs.gen"
include "spritedata/spriteconverter/r_mario_sityoshie.tcs.gen"
;12672
include "spritedata/spriteconverter/l_mario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/l_mario_attackyoshie1.tcs.gen"
;12864
include "spritedata/spriteconverter/l_mario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/l_mario_attackyoshie2.tcs.gen"
;13056
include "spritedata/spriteconverter/r_mario_attackyoshie1.tgs.gen"
include "spritedata/spriteconverter/r_mario_attackyoshie1.tcs.gen"
;13248
include "spritedata/spriteconverter/r_mario_attackyoshie2.tgs.gen"
include "spritedata/spriteconverter/r_mario_attackyoshie2.tcs.gen"
;13440
include "spritedata/spriteconverter/l_mario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/l_mario_turnyoshie.tcs.gen"
;13632
include "spritedata/spriteconverter/r_mario_turnyoshie.tgs.gen"
include "spritedata/spriteconverter/r_mario_turnyoshie.tcs.gen"
;==========================================[normal fire mario]==============================
;13824
include "spritedata/spriteconverter/l_mario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_mario_kickshell.tcs.gen"
;14016
include "spritedata/spriteconverter/r_mario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_mario_kickshell.tcs.gen"
;14208
include "spritedata/spriteconverter/l_supermario_kickshell.tgs.gen"
include "spritedata/spriteconverter/l_supermario_kickshell.tcs.gen"
;14400
include "spritedata/spriteconverter/r_supermario_kickshell.tgs.gen"
include "spritedata/spriteconverter/r_supermario_kickshell.tcs.gen"
;14592 
include "spritedata/spriteconverter/supermario_front2camera.tgs.gen"
include "spritedata/spriteconverter/supermario_front2camera.tcs.gen"
;14784 
include "spritedata/spriteconverter/supermario_back2camera.tgs.gen"
include "spritedata/spriteconverter/supermario_back2camera.tcs.gen"


;===================[free positions below this line]====================

;=====================================[slidesprites]============================
;14976 
include "spritedata/spriteconverter/r_mario_sityoshiepeace.tgs.gen"
include "spritedata/spriteconverter/r_mario_sityoshiepeace.tcs.gen"
;15168 
include "spritedata/spriteconverter/r_supermario_sityoshiepeace.tgs.gen"
include "spritedata/spriteconverter/r_supermario_sityoshiepeace.tcs.gen"
;15360 
include "spritedata/spriteconverter/r_shootingmario_sityoshiepeace.tgs.gen"
include "spritedata/spriteconverter/r_shootingmario_sityoshiepeace.tcs.gen"
;15552 
include "spritedata/spriteconverter/r_flyingmario_sityoshiepeace.tgs.gen"
include "spritedata/spriteconverter/r_flyingmario_sityoshiepeace.tcs.gen"

;====================================[climbsprites]=============================
;15744 
include "spritedata/spriteconverter/mario_climb1.tgs.gen"
include "spritedata/spriteconverter/mario_climb1.tcs.gen"
;15936 
include "spritedata/spriteconverter/mario_climb2.tgs.gen"
include "spritedata/spriteconverter/mario_climb2.tcs.gen"


	ds		$8000-$,$ff
dephase


;huge tile animation block!
; block $28 - $29
;
hugetileanimationsblock:  equ $28
phase	$4000

bigblockanimation1:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation1.SC2",$0000+7,8*88  ;character table
bigblockanimationclr1:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation1.SC2",$2000+7,8*88  ;character table
bigblockanimation2:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation2.SC2",$0000+7,8*88  ;character table
bigblockanimationclr2:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation2.SC2",$2000+7,8*88  ;character table
bigblockanimation3:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation3.SC2",$0000+7,8*88  ;character table
bigblockanimationclr3:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation3.SC2",$2000+7,8*88  ;character table
bigblockanimation4:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation4.SC2",$0000+7,8*88  ;character table
bigblockanimationclr4:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation4.SC2",$2000+7,8*88  ;character table
bigblockanimation5:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation5.SC2",$0000+7,8*88  ;character table
bigblockanimationclr5:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation5.SC2",$2000+7,8*88  ;character table
bigblockanimation6:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation6.SC2",$0000+7,8*88  ;character table
bigblockanimationclr6:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation6.SC2",$2000+7,8*88  ;character table
bigblockanimation7:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation7.SC2",$0000+7,8*88  ;character table
bigblockanimationclr7:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation7.SC2",$2000+7,8*88  ;character table
bigblockanimation8:               incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation8.SC2",$0000+7,8*88  ;character table
bigblockanimationclr8:            incbin "../grapx/world1/level1-07b iggys-castle/bigblockanimation8.SC2",$2000+7,8*88  ;character table



	ds		$8000-$,$ff
dephase

;huge tile animation block2!
; block $2a - $2b
;

hugetileanimationsblock2:  equ $2a

phase	$4000

level19animationsblock:               equ $2a
animationlvchr19:               incbin "../grapx/world2/level2-02a donut-plains-2/Animation.SC2",$0000+7,8*136  ;character table
animationlvclr19:               incbin "../grapx/world2/level2-02a donut-plains-2/Animation.SC2",$2000+7,8*136  ;character table


hugeblocktilesheet:               incbin "../grapx/world1/level1-07b iggys-castle/lower tiles Big block animation sheet.SC2",$0000+7,8*640  ;character table
hugeblocktilesheetclr:            incbin "../grapx/world1/level1-07b iggys-castle/lower tiles Big block animation sheet.SC2",$2000+7,8*640  ;character table
	ds		$8000-$,$ff
dephase


; objectslist block
;
; block $2c - $2d
;
objectslistblock: equ $2c
phase	$4000
include "objectlists/level000.asm"
include "objectlists/level001.asm"
include "objectlists/level002.asm"
include "objectlists/level003.asm"
include "objectlists/level004.asm"
include "objectlists/level005.asm"
include "objectlists/level006.asm"
include "objectlists/level007.asm"
include "objectlists/level008.asm"
include "objectlists/level009.asm"
include "objectlists/level010.asm"
include "objectlists/level011.asm"
include "objectlists/level012.asm"
include "objectlists/level013.asm"
include "objectlists/level014.asm"
include "objectlists/level015.asm"
include "objectlists/level015s.asm"
include "objectlists/level016.asm"
include "objectlists/level017.asm"
include "objectlists/level018.asm"
include "objectlists/level019.asm"
include "objectlists/level020.asm"
include "objectlists/level021.asm"
include "objectlists/level022.asm"
include "objectlists/level022b.asm"
include "objectlists/level022c.asm"
include "objectlists/level022d.asm"
include "objectlists/level023.asm"
include "objectlists/level023b.asm"
include "objectlists/level024.asm"
include "objectlists/level025.asm"
include "objectlists/level025b.asm"
include "objectlists/level025c.asm"
include "objectlists/level026.asm"
include "objectlists/level026b.asm"
include "objectlists/level027.asm"
include "objectlists/level027b.asm"
include "objectlists/level028.asm"
include "objectlists/level028b.asm"
include "objectlists/level028c.asm"
include "objectlists/level028d.asm"
include "objectlists/level029.asm"
include "objectlists/level029b.asm"
include "objectlists/level029c.asm"
include "objectlists/level029d.asm"
include "objectlists/level030.asm"
include "objectlists/level030b.asm"
include "objectlists/level031.asm"
include "objectlists/level032.asm"
include "objectlists/level032b.asm"
include "objectlists/level032c.asm"
include "objectlists/level033.asm"
include "objectlists/level033b.asm"
include "objectlists/level034.asm"
include "objectlists/level034b.asm"
include "objectlists/level034c.asm"
include "objectlists/level035.asm"
include "objectlists/level035b.asm"
include "objectlists/level036.asm"
include "objectlists/level036b.asm"
include "objectlists/level037.asm"
include "objectlists/level037b.asm"
include "objectlists/level037c.asm"
include "objectlists/level038.asm"
include "objectlists/level038b.asm"
include "objectlists/level039.asm"
include "objectlists/level039b.asm"
include "objectlists/level040.asm"
include "objectlists/level040b.asm"
include "objectlists/level040c.asm"
include "objectlists/level041.asm"
include "objectlists/level041b.asm"
include "objectlists/level041c.asm"
include "objectlists/level041d.asm"
include "objectlists/level042.asm"
include "objectlists/level042b.asm"
include "objectlists/level043.asm"
include "objectlists/level043b.asm"
include "objectlists/level044.asm"
include "objectlists/level044b.asm"
include "objectlists/level044c.asm"
include "objectlists/level045.asm"
include "objectlists/level045b.asm"
include "objectlists/level045s.asm"
include "objectlists/level046.asm"
include "objectlists/level046b.asm"
include "objectlists/level047.asm"
include "objectlists/level047b.asm"
include "objectlists/level047c.asm"
include "objectlists/level047d.asm"
include "objectlists/level048.asm"
	ds		$8000-$,$ff
dephase

;
; block $2e
;
overworldsongblock:               equ $2e
phase   $8000
overworlbgm:
  db 1
  ;incbin "../music/OVERWORL.BGM"
  incbin "../music/OVERWORI.BGM"
lenghtoverworlbgm: equ $ - overworlbgm
  ds 1000 - lenghtoverworlbgm
  incbin "../music/OVERWORM.BGM"
  
      ds                $a000-$,$ff
dephase

;music blocks

;
; block $2f
;
cavesongblock:                    equ $2f
phase   $8000
  db 0
  incbin "../music/CAVE.BGM"
   
  
  
        ds                $a000-$,$ff
dephase

;
; block $30
;
mariodiesongblock:                equ $30
phase     $8000
  db 0
  incbin "../music/MARIODIE.BGM"

grpx43addr:	incbin "../grapx/world3/level3-06a/packedgrpx/grpx.pck" 
grpxblckl43: equ $30 ;vanilla 06a
;grpx44addr:	incbin "../grapx/world3/level3-06c/packedgrpx/grpx.pck" | grpxblckl44: equ $30 ;vanilla 06c
 
backgrl52:  incbin "../level_editor/_levels/maplevel3-05a.stg.pck" 
blockbackgrl52:  equ $30 ;vanilla 05a  
backgrl53:  incbin "../level_editor/_levels/maplevel3-05b.stg.pck" 
blockbackgrl53:  equ $30 ;vanilla 05b  
backgrl54:  incbin "../level_editor/_levels/maplevel3-06a.stg.pck" 
blockbackgrl54:  equ $30 ;vanilla 06a  
backgrl55:  incbin "../level_editor/_levels/maplevel3-06b.stg.pck" 
blockbackgrl55:  equ $30 ;vanilla 06b  
backgrl56:  incbin "../level_editor/_levels/maplevel3-06c.stg.pck" 
blockbackgrl56:  equ $30 ;vanilla 06c   
  
  
      ds                $a000-$,$ff
dephase

;
; block $31
;
mariodwinsongblock:                equ $31
phase     $8000
  db 0
  incbin "../music/LEVELCLE.BGM"


grpx62addr:	incbin "../grapx/world3/level3-07a vanillasecret-2/packedgrpx/grpx.pck" 
grpxblckl62: equ $31 ;vanilla 07a
  
backgrl61:  incbin "../level_editor/_levels/maplevel3-03c.stg.pck" 
blockbackgrl61:  equ $31 ;vanilla 03c   
backgrl62:  incbin "../level_editor/_levels/maplevel3-07a.stg.pck" 
blockbackgrl62:  equ $31 ;vanilla 07a   
backgrl63:  incbin "../level_editor/_levels/maplevel3-07b.stg.pck" 
blockbackgrl63:  equ $31 ;vanilla 07b   



 
      ds                $a000-$,$ff
dephase


; ultra huge tile animations block
; block $32 - $33
;
phase	$4000

level11animationblockadress: equ $32

grapxl11a:               incbin "../grapx/world1/level1-07c iggys-castle/grapxa.SC2",$0000+7,8*256  ;character table
grapxl11aclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxa.SC2",$2000+7,8*256  ;character table
grapxl11b:               incbin "../grapx/world1/level1-07c iggys-castle/grapxb.SC2",$0000+7,8*256  ;character table
grapxl11bclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxb.SC2",$2000+7,8*256  ;character table
grapxl11c:               incbin "../grapx/world1/level1-07c iggys-castle/grapxc.SC2",$0000+7,8*256  ;character table
grapxl11cclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxc.SC2",$2000+7,8*256  ;character table
grapxl11d:               incbin "../grapx/world1/level1-07c iggys-castle/grapxd.SC2",$0000+7,8*256  ;character table
grapxl11dclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxd.SC2",$2000+7,8*256  ;character table

	ds		$8000-$,$ff
dephase


; ultra huge tile animations block
; block $34 - $35
;
phase	$4000

level11animationblockadress2: equ $34


grapxl11e:               incbin "../grapx/world1/level1-07c iggys-castle/grapxe.SC2",$0000+7,8*256  ;character table
grapxl11eclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxe.SC2",$2000+7,8*256  ;character table
grapxl11f:               incbin "../grapx/world1/level1-07c iggys-castle/grapxf.SC2",$0000+7,8*256  ;character table
grapxl11fclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxf.SC2",$2000+7,8*256  ;character table
grapxl11g:               incbin "../grapx/world1/level1-07c iggys-castle/grapxg.SC2",$0000+7,8*256  ;character table
grapxl11gclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxg.SC2",$2000+7,8*256  ;character table
grapxl11h:               incbin "../grapx/world1/level1-07c iggys-castle/grapxh.SC2",$0000+7,8*256  ;character table
grapxl11hclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxh.SC2",$2000+7,8*256  ;character table


	ds		$8000-$,$ff
dephase


;infolist block merge with block below if required
;
;block $36 - $37 $38 - $39
;
infolistblock:        equ $36
phase	$4000
include "infolist.asm"
;	ds		$8000-$,$ff
;dephase

;can you believe this!!!????

	ds		$c000-$,$ff
dephase


;
; block $3a
;
pswitchsong:        equ $3a
scorebarblock:      equ $3a
phase	$8000
  db 0
  incbin "../music/PSWITCH.BGM"
  
  
;white scorebar letter fonts
characteraddressnumber0white:
include "spritedata/scorebar/whitenumber0.tgs.gen" 
coloraddressnumber0white:
include "spritedata/scorebar/whitenumber0.tcs.gen"
characteraddressnumber1white:
include "spritedata/scorebar/whitenumber1.tgs.gen" 
coloraddressnumber1white:
include "spritedata/scorebar/whitenumber1.tcs.gen"
characteraddressnumber2white:
include "spritedata/scorebar/whitenumber2.tgs.gen" 
coloraddressnumber2white:
include "spritedata/scorebar/whitenumber2.tcs.gen"
characteraddressnumber3white:
include "spritedata/scorebar/whitenumber3.tgs.gen" 
coloraddressnumber3white:
include "spritedata/scorebar/whitenumber3.tcs.gen"
characteraddressnumber4white:
include "spritedata/scorebar/whitenumber4.tgs.gen" 
coloraddressnumber4white:
include "spritedata/scorebar/whitenumber4.tcs.gen"
characteraddressnumber5white:
include "spritedata/scorebar/whitenumber5.tgs.gen" 
coloraddressnumber5white:
include "spritedata/scorebar/whitenumber5.tcs.gen"
characteraddressnumber6white:
include "spritedata/scorebar/whitenumber6.tgs.gen" 
coloraddressnumber6white:
include "spritedata/scorebar/whitenumber6.tcs.gen"
characteraddressnumber7white:
include "spritedata/scorebar/whitenumber7.tgs.gen" 
coloraddressnumber7white:
include "spritedata/scorebar/whitenumber7.tcs.gen"
characteraddressnumber8white:
include "spritedata/scorebar/whitenumber8.tgs.gen" 
coloraddressnumber8white:
include "spritedata/scorebar/whitenumber8.tcs.gen"
characteraddressnumber9white:
include "spritedata/scorebar/whitenumber9.tgs.gen" 
coloraddressnumber9white:
include "spritedata/scorebar/whitenumber9.tcs.gen"

;yellow
characteraddressnumber0yellow:
include "spritedata/scorebar/spritenumber0.tgs.gen" 
coloraddressnumber0yellow:
include "spritedata/scorebar/spritenumber0.tcs.gen"
characteraddressnumber1yellow:
include "spritedata/scorebar/spritenumber1.tgs.gen" 
coloraddressnumber1yellow:
include "spritedata/scorebar/spritenumber1.tcs.gen"
characteraddressnumber2yellow:
include "spritedata/scorebar/spritenumber2.tgs.gen" 
coloraddressnumber2yellow:
include "spritedata/scorebar/spritenumber2.tcs.gen"
characteraddressnumber3yellow:
include "spritedata/scorebar/spritenumber3.tgs.gen" 
coloraddressnumber3yellow:
include "spritedata/scorebar/spritenumber3.tcs.gen"
characteraddressnumber4yellow:
include "spritedata/scorebar/spritenumber4.tgs.gen" 
coloraddressnumber4yellow:
include "spritedata/scorebar/spritenumber4.tcs.gen"
characteraddressnumber5yellow:
include "spritedata/scorebar/spritenumber5.tgs.gen" 
coloraddressnumber5yellow:
include "spritedata/scorebar/spritenumber5.tcs.gen"
characteraddressnumber6yellow:
include "spritedata/scorebar/spritenumber6.tgs.gen" 
coloraddressnumber6yellow:
include "spritedata/scorebar/spritenumber6.tcs.gen"
characteraddressnumber7yellow:
include "spritedata/scorebar/spritenumber7.tgs.gen" 
coloraddressnumber7yellow:
include "spritedata/scorebar/spritenumber7.tcs.gen"
characteraddressnumber8yellow:
include "spritedata/scorebar/spritenumber8.tgs.gen" 
coloraddressnumber8yellow:
include "spritedata/scorebar/spritenumber8.tcs.gen"
characteraddressnumber9yellow:
include "spritedata/scorebar/spritenumber9.tgs.gen" 
coloraddressnumber9yellow:
include "spritedata/scorebar/spritenumber9.tcs.gen"
;starx coinx and time
characteraddressstarx:
include "spritedata/scorebar/starx.tgs.gen" 
coloraddressstarx:
include "spritedata/scorebar/starx.tcs.gen"
characteraddresscoinx:
include "spritedata/scorebar/coinsx.tgs.gen" 
coloraddresscoinx:
include "spritedata/scorebar/coinsx.tcs.gen"
characteraddresstime:
include "spritedata/scorebar/time1.tgs.gen" 
coloraddresstime:
include "spritedata/scorebar/time1.tcs.gen"
;coin
characteraddresscoin:
include "spritedata/scorebar/coin.tgs.gen" 
coloraddresscoin:
include "spritedata/scorebar/coin.tcs.gen"

;powerup sprites
characteraddressredmushroom2:
include "spritedata/ingame sprites/redmushroom.tgs.gen"
coloraddressredmushroom2:
include "spritedata/ingame sprites/redmushroom.tcs.gen"
characteraddressgreenmushroom2:
include "spritedata/ingame sprites/greenmushroom.tgs.gen"
coloraddressgreenmushroom2:
include "spritedata/ingame sprites/greenmushroom.tcs.gen"
characteraddressfeatherleft2:
include "spritedata/ingame sprites/featherleft.tgs.gen"
coloraddressfeatherleft2:
include "spritedata/ingame sprites/featherleft.tcs.gen"
characteraddressyellowflowerleft2:
include "spritedata/ingame sprites/yellowflowerleft.tgs.gen"
coloraddressyellowflowerleft2:
include "spritedata/ingame sprites/yellowflowerleft.tcs.gen"
characteraddressstaryellow2:
include "spritedata/ingame sprites/staryellow.tgs.gen"
coloraddressstaryellow2:
include "spritedata/ingame sprites/staryellow.tcs.gen"

grpx64addr:	incbin "../grapx/world3/level3-08/packedgrpx/grpx.pck" 
grpxblckl64: equ $3a ;vanilla 08a

backgrl64:  incbin "../level_editor/_levels/maplevel3-08a.stg.pck" 
blockbackgrl64:  equ $3a ;vanilla 08a   
backgrl65:  incbin "../level_editor/_levels/maplevel3-08b.stg.pck" 
blockbackgrl65:  equ $3a ;vanilla 08b 
  
	ds		$a000-$,$ff
dephase


;new sprite block this will be the second ingame sprites block


;
; block $3b-3e
;
ingamespritesblock2: equ $3b
phase	$4000

;yoshi

;head
;left
characteraddressyoshieheadsitsuck1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck1.tgs.gen"
coloraddressyoshieheadsitsuck1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck1.tcs.gen"
characteraddressyoshieheadsitsuck2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck2.tgs.gen"
coloraddressyoshieheadsitsuck2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck2.tcs.gen"
characteraddressyoshieheadsitsuck3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck3.tgs.gen"
coloraddressyoshieheadsitsuck3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_sit_suck3.tcs.gen"
characteraddressyoshieheadstandsuck1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck1.tgs.gen"
coloraddressyoshieheadstandsuck1left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck1.tcs.gen"
characteraddressyoshieheadstandsuck2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck2.tgs.gen"
coloraddressyoshieheadstandsuck2left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck2.tcs.gen"
characteraddressyoshieheadstandsuck3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck3.tgs.gen"
coloraddressyoshieheadstandsuck3left:
include "spritedata/ingame sprites level 02/l_yoshiehead_stand_suck3.tcs.gen"


;right
characteraddressyoshieheadsitsuck1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck1.tgs.gen"
coloraddressyoshieheadsitsuck1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck1.tcs.gen"
characteraddressyoshieheadsitsuck2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck2.tgs.gen"
coloraddressyoshieheadsitsuck2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck2.tcs.gen"
characteraddressyoshieheadsitsuck3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck3.tgs.gen"
coloraddressyoshieheadsitsuck3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_sit_suck3.tcs.gen"
characteraddressyoshieheadstandsuck1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck1.tgs.gen"
coloraddressyoshieheadstandsuck1right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck1.tcs.gen"
characteraddressyoshieheadstandsuck2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck2.tgs.gen"
coloraddressyoshieheadstandsuck2right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck2.tcs.gen"
characteraddressyoshieheadstandsuck3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck3.tgs.gen"
coloraddressyoshieheadstandsuck3right:
include "spritedata/ingame sprites level 02/r_yoshiehead_stand_suck3.tcs.gen"


;horizontal fireballs
characteraddressyoshiefireballyellow:
include "spritedata/ingame sprites/yoshie fireballyellow.tgs.gen"
coloraddressyoshiefireballyellow:
include "spritedata/ingame sprites/yoshie fireballyellow.tcs.gen"
characteraddressyoshiefireballred:
include "spritedata/ingame sprites/yoshie fireballred.tgs.gen"
coloraddressyoshiefireballred:
include "spritedata/ingame sprites/yoshie fireballred.tcs.gen"

characteraddressyoshiefireballyellowleft:
include "spritedata/ingame sprites/yoshie fireballyellow_left.tgs.gen"
coloraddressyoshiefireballyellowleft:
include "spritedata/ingame sprites/yoshie fireballyellow_left.tcs.gen"
characteraddressyoshiefireballredleft:
include "spritedata/ingame sprites/yoshie fireballred_left.tgs.gen"
coloraddressyoshiefireballredleft:
include "spritedata/ingame sprites/yoshie fireballred_left.tcs.gen"

;green tubecoverup
characteraddressGreentubecoverupsprite:
include "spritedata/ingame sprites level 02/Green tube coverup sprite.tgs.gen"
coloraddressGreentubecoverupsprite:
include "spritedata/ingame sprites level 02/Green tube coverup sprite.tcs.gen"

;blue rotating block
characteraddressrotatingblockblue:
include "spritedata/ingame sprites/rotatingblock.tgs.gen"
coloraddressrotatingblockblue:
include "spritedata/ingame sprites/rotatingblock_Blue.tcs.gen"
coloraddressrotatingblockred:
include "spritedata/ingame sprites/rotatingblock_Red.tcs.gen"

;endflagbarcoverup
characteraddressendpoleflag1:
include "spritedata/ingame sprites/endpoleflag1.tgs.gen"
coloraddressendpoleflag1:
include "spritedata/ingame sprites/endpoleflag1.tcs.gen"
characteraddressendpoleflag2:
include "spritedata/ingame sprites/endpoleflag2.tgs.gen"
coloraddressendpoleflag2:
include "spritedata/ingame sprites/endpoleflag2.tcs.gen"
characteraddressendpoleflag3:
include "spritedata/ingame sprites/endpoleflag3.tgs.gen"
coloraddressendpoleflag3:
include "spritedata/ingame sprites/endpoleflag3.tcs.gen"
characteraddressendpoleflag4:
include "spritedata/ingame sprites/endpoleflag4.tgs.gen"
coloraddressendpoleflag4:
include "spritedata/ingame sprites/endpoleflag4.tcs.gen"

;flying koopa red
characteraddressflyingkoopaLeft1:
include "spritedata/ingame sprites level 04/l_flyingkoopa1.tgs.gen"
coloraddressflyingkoopaLeft1:
include "spritedata/ingame sprites level 04/l_flyingkoopa1.tcs.gen"
characteraddressflyingkoopaLeft2:
include "spritedata/ingame sprites level 04/l_flyingkoopa2.tgs.gen"
coloraddressflyingkoopaLeft2:
include "spritedata/ingame sprites level 04/l_flyingkoopa2.tcs.gen"
characteraddressflyingkoopaLeft3:
include "spritedata/ingame sprites level 04/l_flyingkoopa3.tgs.gen"
coloraddressflyingkoopaLeft3:
include "spritedata/ingame sprites level 04/l_flyingkoopa3.tcs.gen"
characteraddressflyingkooparight1:
include "spritedata/ingame sprites level 04/r_flyingkoopa1.tgs.gen"
coloraddressflyingkooparight1:
include "spritedata/ingame sprites level 04/r_flyingkoopa1.tcs.gen"
characteraddressflyingkooparight2:
include "spritedata/ingame sprites level 04/r_flyingkoopa2.tgs.gen"
coloraddressflyingkooparight2:
include "spritedata/ingame sprites level 04/r_flyingkoopa2.tcs.gen"
characteraddressflyingkooparight3:
include "spritedata/ingame sprites level 04/r_flyingkoopa3.tgs.gen"
coloraddressflyingkooparight3:
include "spritedata/ingame sprites level 04/r_flyingkoopa3.tcs.gen"


;flying koopa green
characteraddressgreenflyingkoopaLeft1:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa1.tgs.gen"
coloraddressgreenflyingkoopaLeft1:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa1.tcs.gen"
characteraddressgreenflyingkoopaLeft2:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa2.tgs.gen"
coloraddressgreenflyingkoopaLeft2:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa2.tcs.gen"
characteraddressgreenflyingkoopaLeft3:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa3.tgs.gen"
coloraddressgreenflyingkoopaLeft3:
include "spritedata/ingame sprites level 04/green_l_flyingkoopa3.tcs.gen"
characteraddressgreenflyingkooparight1:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa1.tgs.gen"
coloraddressgreenflyingkooparight1:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa1.tcs.gen"
characteraddressgreenflyingkooparight2:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa2.tgs.gen"
coloraddressgreenflyingkooparight2:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa2.tcs.gen"
characteraddressgreenflyingkooparight3:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa3.tgs.gen"
coloraddressgreenflyingkooparight3:
include "spritedata/ingame sprites level 04/green_r_flyingkoopa3.tcs.gen"

;special green star box
characteraddressSpecialGreenStarBox:
include "spritedata/ingame sprites level 04/SpecialGreenStarBox.tgs.gen"
coloraddressSpecialGreenStarBox:
include "spritedata/ingame sprites level 04/SpecialGreenStarBox.tcs.gen"

;naked koopa level04
;left
characteraddressgreennakedkoopa1leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa1.tgs.gen"
coloraddressgreennakedkoopa1leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa1.tcs.gen"
characteraddressgreennakedkoopa2leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa2.tgs.gen"
coloraddressgreennakedkoopa2leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa2.tcs.gen"
characteraddressgreennakedkoopaupsidedownleftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_upsidedown.tgs.gen"
coloraddressgreennakedkoopaupsidedownleftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_upsidedown.tcs.gen"
characteraddressgreennakedkoopaslide1leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_slide1.tgs.gen"
coloraddressgreennakedkoopaslide1leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_slide1.tcs.gen"
characteraddressgreennakedkoopaslide2leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_slide2.tgs.gen"
coloraddressgreennakedkoopaslide2leftlevel04:
include "spritedata/ingame sprites level 04/green_l_nakedkoopa_slide2.tcs.gen"
;right
characteraddressgreennakedkoopa1rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa1.tgs.gen"
coloraddressgreennakedkoopa1rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa1.tcs.gen"
characteraddressgreennakedkoopa2rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa2.tgs.gen"
coloraddressgreennakedkoopa2rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa2.tcs.gen"
characteraddressgreennakedkoopaupsidedownrightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_upsidedown.tgs.gen"
coloraddressgreennakedkoopaupsidedownrightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_upsidedown.tcs.gen"
characteraddressgreennakedkoopaslide1rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_slide1.tgs.gen"
coloraddressgreennakedkoopaslide1rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_slide1.tcs.gen"
characteraddressgreennakedkoopaslide2rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_slide2.tgs.gen"
coloraddressgreennakedkoopaslide2rightlevel04:
include "spritedata/ingame sprites level 04/green_r_nakedkoopa_slide2.tcs.gen"

;grey platform
characteraddressSquarePatternedPlatform:
include "spritedata/ingame sprites level 04/SquarePatternedPlatform.tgs.gen"
coloraddressSquarePatternedPlatform:
include "spritedata/ingame sprites level 04/SquarePatternedPlatform.tcs.gen"

;tubecoveruplevel04
characteraddresstubecoveruplevel04:
include "spritedata/ingame sprites level 04/tubecoveruplevel04.tgs.gen"
coloraddresstubecoveruplevel04:
include "spritedata/ingame sprites level 04/tubecoveruplevel04.tcs.gen"


;black rotating platform
characteraddressblackholeplatform:
include "spritedata/ingame sprites level 04/blackholeplatform.tgs.gen"
coloraddressblackholeplatform:
include "spritedata/ingame sprites level 04/blackholeplatform.tcs.gen"
characteraddressblackholeplatform2:
include "spritedata/ingame sprites level 04/blackholeplatform2.tgs.gen"
coloraddressblackholeplatform2:
include "spritedata/ingame sprites level 04/blackholeplatform2.tcs.gen"

;water coverup sprite
coloraddresswatercoverupsprite1:
include "spritedata/ingame sprites/watercoverupsprite1.tcs.gen"

characteraddresswatercoverupsprite1:
include "spritedata/ingame sprites/watercoverupsprite1.tgs.gen"
characteraddresswatercoverupsprite2:
include "spritedata/ingame sprites/watercoverupsprite2.tgs.gen"
characteraddresswatercoverupsprite3:
include "spritedata/ingame sprites/watercoverupsprite3.tgs.gen"
characteraddresswatercoverupsprite4:
include "spritedata/ingame sprites/watercoverupsprite4.tgs.gen"
characteraddresswatercoverupsprite5:
include "spritedata/ingame sprites/watercoverupsprite5.tgs.gen"
characteraddresswatercoverupsprite6:
include "spritedata/ingame sprites/watercoverupsprite6.tgs.gen"
characteraddresswatercoverupsprite7:
include "spritedata/ingame sprites/watercoverupsprite7.tgs.gen"
characteraddresswatercoverupsprite8:
include "spritedata/ingame sprites/watercoverupsprite8.tgs.gen"
characteraddresswatercoverupsprite9:
include "spritedata/ingame sprites/watercoverupsprite9.tgs.gen"
characteraddresswatercoverupsprite10:
include "spritedata/ingame sprites/watercoverupsprite10.tgs.gen"
characteraddresswatercoverupsprite11:
include "spritedata/ingame sprites/watercoverupsprite11.tgs.gen"
characteraddresswatercoverupsprite12:
include "spritedata/ingame sprites/watercoverupsprite12.tgs.gen"
characteraddresswatercoverupsprite13:
include "spritedata/ingame sprites/watercoverupsprite13.tgs.gen"
characteraddresswatercoverupsprite14:
include "spritedata/ingame sprites/watercoverupsprite14.tgs.gen"
characteraddresswatercoverupsprite15:
include "spritedata/ingame sprites/watercoverupsprite15.tgs.gen"
characteraddresswatercoverupsprite16:
include "spritedata/ingame sprites/watercoverupsprite16.tgs.gen"

characteraddresswater_Alt_coverupsprite1:
include "spritedata/ingame sprites/water_Alt_coverupsprite1.tgs.gen"
characteraddresswater_Alt_coverupsprite2:
include "spritedata/ingame sprites/water_Alt_coverupsprite2.tgs.gen"
characteraddresswater_Alt_coverupsprite3:
include "spritedata/ingame sprites/water_Alt_coverupsprite3.tgs.gen"
characteraddresswater_Alt_coverupsprite4:
include "spritedata/ingame sprites/water_Alt_coverupsprite4.tgs.gen"
characteraddresswater_Alt_coverupsprite5:
include "spritedata/ingame sprites/water_Alt_coverupsprite5.tgs.gen"
characteraddresswater_Alt_coverupsprite6:
include "spritedata/ingame sprites/water_Alt_coverupsprite6.tgs.gen"
characteraddresswater_Alt_coverupsprite7:
include "spritedata/ingame sprites/water_Alt_coverupsprite7.tgs.gen"
characteraddresswater_Alt_coverupsprite8:
include "spritedata/ingame sprites/water_Alt_coverupsprite8.tgs.gen"
characteraddresswater_Alt_coverupsprite9:
include "spritedata/ingame sprites/water_Alt_coverupsprite9.tgs.gen"
characteraddresswater_Alt_coverupsprite10:
include "spritedata/ingame sprites/water_Alt_coverupsprite10.tgs.gen"
characteraddresswater_Alt_coverupsprite11:
include "spritedata/ingame sprites/water_Alt_coverupsprite11.tgs.gen"
characteraddresswater_Alt_coverupsprite12:
include "spritedata/ingame sprites/water_Alt_coverupsprite12.tgs.gen"
characteraddresswater_Alt_coverupsprite13:
include "spritedata/ingame sprites/water_Alt_coverupsprite13.tgs.gen"
characteraddresswater_Alt_coverupsprite14:
include "spritedata/ingame sprites/water_Alt_coverupsprite14.tgs.gen"
characteraddresswater_Alt_coverupsprite15:
include "spritedata/ingame sprites/water_Alt_coverupsprite15.tgs.gen"
characteraddresswater_Alt_coverupsprite16:
include "spritedata/ingame sprites/water_Alt_coverupsprite16.tgs.gen"


;fish
characteraddressfish1left:
include "spritedata/ingame sprites level 06/l_fish1.tgs.gen"
coloraddressfish1left:
include "spritedata/ingame sprites level 06/l_fish1.tcs.gen"
characteraddressfish2left:
include "spritedata/ingame sprites level 06/l_fish2.tgs.gen"
coloraddressfish2left:
include "spritedata/ingame sprites level 06/l_fish2.tcs.gen"
characteraddressfish1right:
include "spritedata/ingame sprites level 06/r_fish1.tgs.gen"
coloraddressfish1right:
include "spritedata/ingame sprites level 06/r_fish1.tcs.gen"
characteraddressfish2right:
include "spritedata/ingame sprites level 06/r_fish2.tgs.gen"
coloraddressfish2right:
include "spritedata/ingame sprites level 06/r_fish2.tcs.gen"
;inwater
characteraddressfish1leftinwater:
include "spritedata/ingame sprites level 06/l_fish1_in_water.tgs.gen"
coloraddressfish1leftinwater:
include "spritedata/ingame sprites level 06/l_fish1_in_water.tcs.gen"
characteraddressfish2leftinwater:
include "spritedata/ingame sprites level 06/l_fish2_in_water.tgs.gen"
coloraddressfish2leftinwater:
include "spritedata/ingame sprites level 06/l_fish2_in_water.tcs.gen"
characteraddressfish1rightinwater:
include "spritedata/ingame sprites level 06/r_fish1_in_water.tgs.gen"
coloraddressfish1rightinwater:
include "spritedata/ingame sprites level 06/r_fish1_in_water.tcs.gen"
characteraddressfish2rightinwater:
include "spritedata/ingame sprites level 06/r_fish2_in_water.tgs.gen"
coloraddressfish2rightinwater:
include "spritedata/ingame sprites level 06/r_fish2_in_water.tcs.gen"
;inwater alternate
characteraddressfish1leftinwateralt:
include "spritedata/ingame sprites level 06/l_fish1_in_water_alt.tgs.gen"
coloraddressfish1leftinwateralt:
include "spritedata/ingame sprites level 06/l_fish1_in_water_alt.tcs.gen"
characteraddressfish2leftinwateralt:
include "spritedata/ingame sprites level 06/l_fish2_in_water_alt.tgs.gen"
coloraddressfish2leftinwateralt:
include "spritedata/ingame sprites level 06/l_fish2_in_water_alt.tcs.gen"
characteraddressfish1rightinwateralt:
include "spritedata/ingame sprites level 06/r_fish1_in_water_alt.tgs.gen"
coloraddressfish1rightinwateralt:
include "spritedata/ingame sprites level 06/r_fish1_in_water_alt.tcs.gen"
characteraddressfish2rightinwateralt:
include "spritedata/ingame sprites level 06/r_fish2_in_water_alt.tgs.gen"
coloraddressfish2rightinwateralt:
include "spritedata/ingame sprites level 06/r_fish2_in_water_alt.tcs.gen"

;water splash
coloraddresswatersplash:
include "spritedata/ingame sprites level 06/water_splash.tcs.gen"
characteraddresswatersplash1:
include "spritedata/ingame sprites level 06/water_splash1.tgs.gen"
characteraddresswatersplash2:
include "spritedata/ingame sprites level 06/water_splash2.tgs.gen"
characteraddresswatersplash3:
include "spritedata/ingame sprites level 06/water_splash3.tgs.gen"
characteraddresswatersplash4:
include "spritedata/ingame sprites level 06/water_splash4.tgs.gen"

;huge watermine
characteraddressHugeWaterMine1:
include "spritedata/ingame sprites level 06/HugeWaterMine1.tgs.gen"
coloraddressHugeWaterMine1:
include "spritedata/ingame sprites level 06/HugeWaterMine1.tcs.gen"
characteraddressHugeWaterMine2:
include "spritedata/ingame sprites level 06/HugeWaterMine2.tgs.gen"
coloraddressHugeWaterMine2:
include "spritedata/ingame sprites level 06/HugeWaterMine2.tcs.gen"
characteraddressHugeWaterMine3:
include "spritedata/ingame sprites level 06/HugeWaterMine3.tgs.gen"
coloraddressHugeWaterMine3:
include "spritedata/ingame sprites level 06/HugeWaterMine3.tcs.gen"
characteraddressHugeWaterMine4:
include "spritedata/ingame sprites level 06/HugeWaterMine4.tgs.gen"
coloraddressHugeWaterMine4:
include "spritedata/ingame sprites level 06/HugeWaterMine4.tcs.gen"

;waterplatform
characteraddressWaterPlatform1:
include "spritedata/ingame sprites level 06/WaterPlatform1.tgs.gen"
coloraddressWaterPlatform1:
include "spritedata/ingame sprites level 06/WaterPlatform1.tcs.gen"
characteraddressWaterPlatform2:
include "spritedata/ingame sprites level 06/WaterPlatform2.tgs.gen"
coloraddressWaterPlatform2:
include "spritedata/ingame sprites level 06/WaterPlatform2.tcs.gen"

;tubecoveruplv6
characteraddresstubecoveruphorizontallevel06:
include "spritedata/ingame sprites level 06/tubecoveruphorizontal.tgs.gen"
coloraddresstubecoveruphorizontallevel06:
include "spritedata/ingame sprites level 06/tubecoveruphorizontal.tcs.gen"
characteraddresstubecoverupverticallevel06:
include "spritedata/ingame sprites level 06/tubecoverupvertical.tgs.gen"
coloraddresstubecoverupverticallevel06:
include "spritedata/ingame sprites level 06/tubecoverupvertical.tcs.gen"

;tubecoveruplv7
characteraddresstubecoveruphorizontallevel07:
include "spritedata/ingame sprites level 07/tubecoveruphorizontallevel07.tgs.gen"
coloraddresstubecoveruphorizontallevel07:
include "spritedata/ingame sprites level 07/tubecoveruphorizontallevel07.tcs.gen"

;cactuscat
characteraddresscactusplantbody:
include "spritedata/ingame sprites level 08/cactusplantbody.tgs.gen"
coloraddresscactusplantbody:
include "spritedata/ingame sprites level 08/cactusplantbody.tcs.gen"
characteraddresscactusplanthead:
include "spritedata/ingame sprites level 08/cactusplanthead.tgs.gen"
coloraddresscactusplanthead:
include "spritedata/ingame sprites level 08/cactusplanthead.tcs.gen"

;fencekoopa level 09
;green climbing koopa
characteraddressgreenkoopashelltocameraclimb1:
include "spritedata/ingame sprites level 09/greenkoopa_shelltocamera_climb1.tgs.gen"
coloraddressgreenkoopashelltocameraclimb1:
include "spritedata/ingame sprites level 09/greenkoopa_shelltocamera_climb1.tcs.gen"
characteraddressgreenkoopashelltocameraclimb2:
include "spritedata/ingame sprites level 09/greenkoopa_shelltocamera_climb2.tgs.gen"
coloraddressgreenkoopashelltocameraclimb2:
include "spritedata/ingame sprites level 09/greenkoopa_shelltocamera_climb2.tcs.gen"
characteraddressgreenkoopafall:
include "spritedata/ingame sprites level 09/greenkoopa_fall.tgs.gen"
coloraddressgreenkoopafall:
include "spritedata/ingame sprites level 09/greenkoopa_fall.tcs.gen"
characteraddressgreenkoopaturnshellleftclimb:
include "spritedata/ingame sprites level 09/greenkoopa_turnshellleft_climb.tgs.gen"
coloraddressgreenkoopaturnshellleftclimb:
include "spritedata/ingame sprites level 09/greenkoopa_turnshellleft_climb.tcs.gen"
characteraddressgreenkoopaturnshellrightclimb:
include "spritedata/ingame sprites level 09/greenkoopa_turnshellright_climb.tgs.gen"
coloraddressgreenkoopaturnshellrightclimb:
include "spritedata/ingame sprites level 09/greenkoopa_turnshellright_climb.tcs.gen"
;behind fence
characteraddressGreenClimbBehindFence1:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence1.tgs.gen"
coloraddressGreenClimbBehindFence1:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence1.tcs.gen"
characteraddressGreenClimbBehindFence2:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence2.tgs.gen"
coloraddressGreenClimbBehindFence2:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence2.tcs.gen"
characteraddressGreenClimbBehindFence3:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence3.tgs.gen"
coloraddressGreenClimbBehindFence3:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence3.tcs.gen"
characteraddressGreenClimbBehindFence4:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence4.tgs.gen"
coloraddressGreenClimbBehindFence4:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence4.tcs.gen"
characteraddressGreenClimbBehindFence5:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence5.tgs.gen"
coloraddressGreenClimbBehindFence5:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence5.tcs.gen"
characteraddressGreenClimbBehindFence6:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence6.tgs.gen"
coloraddressGreenClimbBehindFence6:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence6.tcs.gen"
characteraddressGreenClimbBehindFence7:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence7.tgs.gen"
coloraddressGreenClimbBehindFence7:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence7.tcs.gen"
characteraddressGreenClimbBehindFence8:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence8.tgs.gen"
coloraddressGreenClimbBehindFence8:
include "spritedata/ingame sprites level 09/Green_Climb_BehindFence8.tcs.gen"
;vertical turn
characteraddressverticalClimbingGreenkoopaturn1left:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn1_left.tgs.gen"
coloraddressverticalClimbingGreenkoopaturn1left:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn1_left.tcs.gen"
characteraddressverticalClimbingGreenkoopaturn2left:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn2_left.tgs.gen"
coloraddressverticalClimbingGreenkoopaturn2left:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn2_left.tcs.gen"
characteraddressverticalClimbingGreenkoopaturn1right:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn1_right.tgs.gen"
coloraddressverticalClimbingGreenkoopaturn1right:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn1_right.tcs.gen"
characteraddressverticalClimbingGreenkoopaturn2right:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn2_right.tgs.gen"
coloraddressverticalClimbingGreenkoopaturn2right:
include "spritedata/ingame sprites level 09/verticalClimbingGreenkoopa_turn2_right.tcs.gen"


;red climbing koopa
characteraddressredkoopashelltocameraclimb1:
include "spritedata/ingame sprites level 09/redkoopa_shelltocamera_climb1.tgs.gen"
coloraddressredkoopashelltocameraclimb1:
include "spritedata/ingame sprites level 09/redkoopa_shelltocamera_climb1.tcs.gen"
characteraddressredkoopashelltocameraclimb2:
include "spritedata/ingame sprites level 09/redkoopa_shelltocamera_climb2.tgs.gen"
coloraddressredkoopashelltocameraclimb2:
include "spritedata/ingame sprites level 09/redkoopa_shelltocamera_climb2.tcs.gen"
characteraddressredkoopafall:
include "spritedata/ingame sprites level 09/redkoopa_fall.tgs.gen"
coloraddressredkoopafall:
include "spritedata/ingame sprites level 09/redkoopa_fall.tcs.gen"
characteraddressredkoopaturnshellleftclimb:
include "spritedata/ingame sprites level 09/redkoopa_turnshellleft_climb.tgs.gen"
coloraddressredkoopaturnshellleftclimb:
include "spritedata/ingame sprites level 09/redkoopa_turnshellleft_climb.tcs.gen"
characteraddressredkoopaturnshellrightclimb:
include "spritedata/ingame sprites level 09/redkoopa_turnshellright_climb.tgs.gen"
coloraddressredkoopaturnshellrightclimb:
include "spritedata/ingame sprites level 09/redkoopa_turnshellright_climb.tcs.gen"
;behind fence
characteraddressredClimbBehindFence1:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence1.tgs.gen"
coloraddressredClimbBehindFence1:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence1.tcs.gen"
characteraddressredClimbBehindFence2:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence2.tgs.gen"
coloraddressredClimbBehindFence2:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence2.tcs.gen"
characteraddressredClimbBehindFence3:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence3.tgs.gen"
coloraddressredClimbBehindFence3:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence3.tcs.gen"
characteraddressredClimbBehindFence4:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence4.tgs.gen"
coloraddressredClimbBehindFence4:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence4.tcs.gen"
characteraddressredClimbBehindFence5:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence5.tgs.gen"
coloraddressredClimbBehindFence5:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence5.tcs.gen"
characteraddressredClimbBehindFence6:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence6.tgs.gen"
coloraddressredClimbBehindFence6:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence6.tcs.gen"
characteraddressredClimbBehindFence7:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence7.tgs.gen"
coloraddressredClimbBehindFence7:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence7.tcs.gen"
characteraddressredClimbBehindFence8:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence8.tgs.gen"
coloraddressredClimbBehindFence8:
include "spritedata/ingame sprites level 09/Red_Climb_BehindFence8.tcs.gen"
;vertical turn
characteraddressverticalClimbingredkoopaturn1left:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn1_left.tgs.gen"
coloraddressverticalClimbingredkoopaturn1left:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn1_left.tcs.gen"
characteraddressverticalClimbingredkoopaturn2left:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn2_left.tgs.gen"
coloraddressverticalClimbingredkoopaturn2left:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn2_left.tcs.gen"
characteraddressverticalClimbingredkoopaturn1right:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn1_right.tgs.gen"
coloraddressverticalClimbingredkoopaturn1right:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn1_right.tcs.gen"
characteraddressverticalClimbingredkoopaturn2right:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn2_right.tgs.gen"
coloraddressverticalClimbingredkoopaturn2right:
include "spritedata/ingame sprites level 09/verticalClimbingRedkoopa_turn2_right.tcs.gen"


;flamegoingup
characteraddressflamegoingup1:
include "spritedata/ingame sprites level 09/flameGoingUp1.tgs.gen"
coloraddressflamegoingup1:
include "spritedata/ingame sprites level 09/flameGoingUp1.tcs.gen"
characteraddressflamegoingup2:
include "spritedata/ingame sprites level 09/flameGoingUp2.tgs.gen"
coloraddressflamegoingup2:
include "spritedata/ingame sprites level 09/flameGoingUp2.tcs.gen"
characteraddressflamegoingdown1:
include "spritedata/ingame sprites level 09/flameGoingDown1.tgs.gen"
coloraddressflamegoingdown1:
include "spritedata/ingame sprites level 09/flameGoingDown1.tcs.gen"
characteraddressflamegoingdown2:
include "spritedata/ingame sprites level 09/flameGoingDown2.tgs.gen"
coloraddressflamegoingdown2:
include "spritedata/ingame sprites level 09/flameGoingDown2.tcs.gen"
characteraddresssmallFlamebiggest:
include "spritedata/ingame sprites level 09/smallFlame_biggest.tgs.gen"
coloraddresssmallFlamebiggest:
include "spritedata/ingame sprites level 09/smallFlame_biggest.tcs.gen"
characteraddresssmallFlamemiddle:
include "spritedata/ingame sprites level 09/smallFlame_middle.tgs.gen"
coloraddresssmallFlamemiddle:
include "spritedata/ingame sprites level 09/smallFlame_middle.tcs.gen"
characteraddresssmallFlamesmallest:
include "spritedata/ingame sprites level 09/smallFlame_smallest.tgs.gen"
coloraddresssmallFlamesmallest:
include "spritedata/ingame sprites level 09/smallFlame_smallest.tcs.gen"


;castle
characteraddressdoor:
include "spritedata/ingame sprites level 09/door.tgs.gen"
coloraddressdoor:
include "spritedata/ingame sprites level 09/door.tcs.gen"

;hugeiggypillar
characteraddressBigBlockCoverupPiece:
include "spritedata/ingame sprites level 10/BigBlockCoverupPiece.tgs.gen"
coloraddressBigBlockCoverupPiece:
include "spritedata/ingame sprites level 10/BigBlockCoverupPiece.tcs.gen"

characteraddressBigBlockPiece:
include "spritedata/ingame sprites level 10/Bigblockpiece.tgs.gen"
coloraddressBigBlockPiece:
include "spritedata/ingame sprites level 10/Bigblockpiece.tcs.gen"



;boss door
characteraddressCastleBigDoor:
include "spritedata/ingame sprites level 10/CastleBigDoor.tgs.gen"
coloraddressCastleBigDoor:
include "spritedata/ingame sprites level 10/CastleBigDoor.tcs.gen"


;Iggy boss
;left
characteraddressLeftBoss1:
include "spritedata/ingame sprites level 11/Left_Boss1.tgs.gen"
coloraddressLeftBoss1:
include "spritedata/ingame sprites level 11/Left_Boss1.tcs.gen"
characteraddressLeftBoss2:
include "spritedata/ingame sprites level 11/Left_Boss2.tgs.gen"
coloraddressLeftBoss2:
include "spritedata/ingame sprites level 11/Left_Boss2.tcs.gen"
characteraddressLeftBoss3:
include "spritedata/ingame sprites level 11/Left_Boss3.tgs.gen"
coloraddressLeftBoss3:
include "spritedata/ingame sprites level 11/Left_Boss3.tcs.gen"
characteraddressLeftBoss4:
include "spritedata/ingame sprites level 11/Left_Boss4.tgs.gen"
coloraddressLeftBoss4:
include "spritedata/ingame sprites level 11/Left_Boss4.tcs.gen"
characteraddressLeftBoss5:
include "spritedata/ingame sprites level 11/Left_Boss5.tgs.gen"
coloraddressLeftBoss5:
include "spritedata/ingame sprites level 11/Left_Boss5.tcs.gen"
characteraddressLeftBoss6:
include "spritedata/ingame sprites level 11/Left_Boss6.tgs.gen"
coloraddressLeftBoss6:
include "spritedata/ingame sprites level 11/Left_Boss6.tcs.gen"
characteraddressLeftBoss7:
include "spritedata/ingame sprites level 11/Left_Boss7.tgs.gen"
coloraddressLeftBoss7:
include "spritedata/ingame sprites level 11/Left_Boss7.tcs.gen"
characteraddressLeftBoss8:
include "spritedata/ingame sprites level 11/Left_Boss8.tgs.gen"
coloraddressLeftBoss8:
include "spritedata/ingame sprites level 11/Left_Boss8.tcs.gen"
characteraddressLeftBoss9:
include "spritedata/ingame sprites level 11/Left_Boss9.tgs.gen"
coloraddressLeftBoss9:
include "spritedata/ingame sprites level 11/Left_Boss9.tcs.gen"


;right
characteraddressrightBoss1:
include "spritedata/ingame sprites level 11/Right_Boss1.tgs.gen"
coloraddressrightBoss1:
include "spritedata/ingame sprites level 11/Right_Boss1.tcs.gen"
characteraddressrightBoss2:
include "spritedata/ingame sprites level 11/Right_Boss2.tgs.gen"
coloraddressrightBoss2:
include "spritedata/ingame sprites level 11/Right_Boss2.tcs.gen"
characteraddressrightBoss3:
include "spritedata/ingame sprites level 11/Right_Boss3.tgs.gen"
coloraddressrightBoss3:
include "spritedata/ingame sprites level 11/Right_Boss3.tcs.gen"
characteraddressrightBoss4:
include "spritedata/ingame sprites level 11/Right_Boss4.tgs.gen"
coloraddressrightBoss4:
include "spritedata/ingame sprites level 11/Right_Boss4.tcs.gen"
characteraddressrightBoss5:
include "spritedata/ingame sprites level 11/Right_Boss5.tgs.gen"
coloraddressrightBoss5:
include "spritedata/ingame sprites level 11/Right_Boss5.tcs.gen"
characteraddressrightBoss6:
include "spritedata/ingame sprites level 11/Right_Boss6.tgs.gen"
coloraddressrightBoss6:
include "spritedata/ingame sprites level 11/Right_Boss6.tcs.gen"
characteraddressrightBoss7:
include "spritedata/ingame sprites level 11/Right_Boss7.tgs.gen"
coloraddressrightBoss7:
include "spritedata/ingame sprites level 11/Right_Boss7.tcs.gen"
characteraddressrightBoss8:
include "spritedata/ingame sprites level 11/Right_Boss8.tgs.gen"
coloraddressrightBoss8:
include "spritedata/ingame sprites level 11/Right_Boss8.tcs.gen"
characteraddressrightBoss9:
include "spritedata/ingame sprites level 11/Right_Boss9.tgs.gen"
coloraddressrightBoss9:
include "spritedata/ingame sprites level 11/Right_Boss9.tcs.gen"

;iggyshell
characteraddressBossRollingCentre:
include "spritedata/ingame sprites level 11/BossRollingCentre.tgs.gen"
coloraddressBossRollingCentre:
include "spritedata/ingame sprites level 11/BossRollingCentre.tcs.gen"
characteraddressBossRollingleft:
include "spritedata/ingame sprites level 11/BossRollingLeft.tgs.gen"
coloraddressBossRollingleft:
include "spritedata/ingame sprites level 11/BossRollingLeft.tcs.gen"
characteraddressBossRollingright:
include "spritedata/ingame sprites level 11/BossRollingRight.tgs.gen"
coloraddressBossRollingright:
include "spritedata/ingame sprites level 11/BossRollingRight.tcs.gen"

;iggy ball
characteraddressBossWeapon1:
include "spritedata/ingame sprites level 11/BossWeapon1.tgs.gen"
coloraddressBossWeapon1:
include "spritedata/ingame sprites level 11/BossWeapon1.tcs.gen"
characteraddressBossWeapon2:
include "spritedata/ingame sprites level 11/BossWeapon2.tgs.gen"
coloraddressBossWeapon2:
include "spritedata/ingame sprites level 11/BossWeapon2.tcs.gen"
characteraddressBossWeapon3:
include "spritedata/ingame sprites level 11/BossWeapon3.tgs.gen"
coloraddressBossWeapon3:
include "spritedata/ingame sprites level 11/BossWeapon3.tcs.gen"



	ds		$c000-$,$ff
dephase


;
; block $3f
;
marioathsongblock:                equ $3f
phase     $8000
athleticbgm:
;  incbin "../music/athletic.bgm"
  db 1 ;if 1 loop with intro
  incbin "../music/ATHLETII.BGM"
lenghtathleticbgm: equ $ - athleticbgm
  ds 1000 - lenghtathleticbgm
  incbin "../music/ATHLETIM.BGM"
  
  
      ds                $a000-$,$ff
dephase


;
; block $40-41
;
romprogblock4:                equ $40
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement2.asm"

romprogblock4end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $42-43
;
romprogblock5:                equ $42
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement3.asm"

romprogblock5end:
  
  
      ds                $c000-$,$ff
dephase


;All the worldmap1data


;
; block $44 - $45
;
;
phase	$4000
Worldmap1tilesblock: equ $44
world1tiles:	  incbin "../grapx/worldmap/world1.SC5",7,$4000  ;128 lines high (128*128=$4000)
	ds		$8000-$,$ff
dephase

;
; block $46
;
;
phase	$4000
worldmaptoppartofscreenblock: equ $46
worldmaptoppartofscreen:	  incbin "../grapx/worldmap/world1.SC5",7+$4000,$1500  ;42 lines high (42*128=$1500)
yellowswitchbkgchr:         incbin "../grapx/world1/level1-06a yellow-switchpalace/animation.SC2",$0000+7,8*64  ;character table 
yellowswitchbkgclr:         incbin "../grapx/world1/level1-06a yellow-switchpalace/animation.SC2",$2000+7,8*64  ;character table 
	ds		$6000-$,$ff
dephase

;
; block $47
;
;
phase	$4000
	ds		$6000-$,$ff
dephase

;
; block $48
;
;
phase	$4000

	ds		$6000-$,$ff
dephase


;/all the worldmap1 data



;tilegfx block3
;
; block $49 - 4a
;
phase	$8000

grpx11addr:	incbin "../grapx/world1/level1-07c iggys-castle/packedgrpx/grpx.pck" 
grpxblckl11: equ $49
grpx12addr:	incbin "../grapx/world2/level2-01a donut-plains-1/packedgrpx/grpx.pck" 
grpxblckl12: equ $49
grpx13addr:	incbin "../grapx/world1/level1-06a yellow-switchpalace/packedgrpx/grpx.pck" 
grpxblckl13: equ $49
grpx14addr:	incbin "../grapx/world1/level1-01 yoshis-house/packedgrpx/grpx.pck" 
grpxblckl14: equ $49
grpx16addr:	incbin "../grapx/world2/level2-01c donut-plains-1/packedgrpx/grpx.pck" 
grpxblckl16: equ $49
grpx18addr:	incbin "../grapx/title screen/title screen/packedgrpx/grpx.pck" 
grpxblckl18: equ $49
;grpx17addr:	incbin "../grapx/level17/packedgrpx/grpx17.pck" | grpxblckl17: equ $49
;grpx19addr:	incbin "../grapx/level19/packedgrpx/grpx19.pck" | grpxblckl19: equ $49 ;deprecated
grpx19addr:	incbin "../grapx/world2/level2-02a donut-plains-2/packedgrpx/grpx.pck" 
grpxblckl19: equ $49
grpx20addr:	incbin "../grapx/world2/level2-02b donut-plains-2/packedgrpx/grpx.pck" 
grpxblckl20: equ $49
grpx21addr:	incbin "../grapx/world2/level2-02c donut-plains-2/packedgrpx/grpx.pck" 
grpxblckl21: equ $49
grpx22addr:	incbin "../grapx/world2/level2-05a donut-ghosthouse/packedgrpx/grpx.pck" 
grpxblckl22: equ $49
grpx23addr:	incbin "../grapx/world2/level2-05e donut-ghosthouse/packedgrpx/grpx.pck" 
grpxblckl23: equ $49
grpx24addr:	incbin "../grapx/world2/level2-03a donut-plains-3/packedgrpx/grpx.pck" 
grpxblckl24: equ $49
;grpx25addr:	incbin "../grapx/level25/packedgrpx/grpx25.pck" | grpxblckl25: equ $49
	ds		$c000-$,$ff
dephase

;packed level block3
;
; block $4b - 4c
;
phase	$8000

backgrl11:  incbin "../level_editor/_levels/maplevel1-07c.stg.pck" 
blockbackgrl11:  equ $4b
backgrl12:  incbin "../level_editor/_levels/maplevel2-01a.stg.pck" 
blockbackgrl12:  equ $4b
backgrl13:  incbin "../level_editor/_levels/maplevel1-06a.stg.pck" 
blockbackgrl13:  equ $4b
backgrl14:  incbin "../level_editor/_levels/maplevel1-01.stg.pck" 
blockbackgrl14:  equ $4b
backgrl16:  incbin "../level_editor/_levels/maplevel2-01c.stg.pck" 
blockbackgrl16:  equ $4b
backgrl17:  incbin "../level_editor/_levels/maplevel2-01b.stg.pck" 
blockbackgrl17:  equ $4b
backgrl18:  incbin "../level_editor/_levels/titlescreen.stg.pck" 
blockbackgrl18:  equ $4b
backgrl19:  incbin "../level_editor/_levels/maplevel2-02a.stg.pck" 
blockbackgrl19:  equ $4b
backgrl20:  incbin "../level_editor/_levels/maplevel2-02b.stg.pck" 
blockbackgrl20:  equ $4b
backgrl21:  incbin "../level_editor/_levels/maplevel2-02c.stg.pck" 
blockbackgrl21:  equ $4b
backgrl22:  incbin "../level_editor/_levels/maplevel2-05a.stg.pck" 
blockbackgrl22:  equ $4b
backgrl22b:  incbin "../level_editor/_levels/maplevel2-05b.stg.pck" 
blockbackgrl22b:  equ $4b
backgrl22c:  incbin "../level_editor/_levels/maplevel2-05c.stg.pck" 
blockbackgrl22c:  equ $4b
backgrl22d:  incbin "../level_editor/_levels/maplevel2-05d.stg.pck" 
blockbackgrl22d:  equ $4b
backgrl23:  incbin "../level_editor/_levels/maplevel2-05e.stg.pck" 
blockbackgrl23:  equ $4b
backgrl24:  incbin "../level_editor/_levels/maplevel2-03a.stg.pck" 
blockbackgrl24:  equ $4b
backgrlts:  incbin "../level_editor/_levels/topsecret.stg.pck" 
blockbacktops:  equ $4b
;backgrl25:  incbin "../level_editor/_levels/maplevel25.stg.pck" | blockbackgrl25:  equ $4b
	ds		$c000-$,$ff
dephase




; ultra huge tile animations block2
; block $4d - $4e
;
phase	$4000

level11animationblockadress3: equ $4d

grapxl11i:               incbin "../grapx/world1/level1-07c iggys-castle/grapxi.SC2",$0000+7,8*256  ;character table
grapxl11iclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxi.SC2",$2000+7,8*256  ;character table
grapxl11j:               incbin "../grapx/world1/level1-07c iggys-castle/grapxj.SC2",$0000+7,8*256  ;character table
grapxl11jclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxj.SC2",$2000+7,8*256  ;character table
grapxl11k:               incbin "../grapx/world1/level1-07c iggys-castle/grapxk.SC2",$0000+7,8*256  ;character table
grapxl11kclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxk.SC2",$2000+7,8*256  ;character table
grapxl11l:               incbin "../grapx/world1/level1-07c iggys-castle/grapxl.SC2",$0000+7,8*256  ;character table
grapxl11lclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxl.SC2",$2000+7,8*256  ;character table


	ds		$8000-$,$ff
dephase


; ultra huge tile animations block2
; block $4f
;
phase	$4000

level11animationblockadress4: equ $4f


grapxl11m:               incbin "../grapx/world1/level1-07c iggys-castle/grapxm.SC2",$0000+7,8*256  ;character table
grapxl11mclr:            incbin "../grapx/world1/level1-07c iggys-castle/grapxm.SC2",$2000+7,8*256  ;character table

specialsheertilesblock: equ $4f

sheertablenormalleft:  db 32+8,31+8,30+8,29+8,28+8,27+8,26+8,25+8,24+8,23+8,22+8,21+8,20+8,19+8,18+8,17+8,16+8,15+8,14+8,13+8,12+8,11+8,10+8,9+8,7+8,6+8,5+8,4+8,3+8,2+8,1+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8,0+8
sheertablenormalright: db 0+8,1+8,2+8,3+8,4+8,5+8,6+8,7+8,8+8,9+8,10+8,11+8,12+8,13+8,14+8,15+8,16+8,17+8,18+8,19+8,20+8,21+8,22+8,23+8,24+8,25+8,26+8,27+8,28+8,29+8,30+8,31+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8,32+8
sheertableslightleft:  db 24+8,24+8,23+8,23+8,23+8,22+8,22+8,21+8,21+8,21+8,20+8,20+8,20+8,19+8,19+8,18+8,18+8,18+8,17+8,17+8,17+8,16+8,16+8,15+8,15+8,15+8,14+8,14+8,14+8,13+8,13+8,12+8,12+8,12+8,11+8,11+8,11+8,10+8,10+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8 ;12deg.
sheertableslightright: db 16,16+1,16+2,16+3,16+4,16+5,16+6,16+7,16+8,16+8,16+8,16+8,16+8,16+8,16+8,16+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,18+8,18+8,19+8,19+8,19+8,20+8,20+8,20+8,21+8,21+8,22+8,22+8,22+8,23+8,23+8,23+8,24+8,24+8,25+8,25+8,25+8,26+8,26+8,26+8,27+8,27+8,28+8,28+8,28+8,29+8,29+8,29+8,30+8,30+8,31+8,31+8,31+8,32+8,32+8 ;12deg.
sheertablemediumleft:  db 44,43,41,40,40,39,39,37,37,36,34,34,34,32,32,31,31,31,29,29,29,28,28,26,26,26,25,25,25,23,23,22,22,22,20,20,20,18,18,17,17,17,17,17,17,16,16,15,14,14,14,14,13,12,12,12,12,12,12,12,12,12,12,12,12  ;24 deg.
;sheertablemediumright: db 12+9,13+9,13+9,14+9,14+9,15+9,15+9,16+9,16+9,17+9,18+9,18+9,19+9,19+9,20+9,20+9,21+9,22+9,22+9,23+9,23+9,24+9,24+9,25+9,25+9,26+9,27+9,27+9,28+9,28+9,29+9,29+9,30+9,30+9,31+9,32+9,32+9,33+9,33+9,34+9,34+9,35+9,36+9,36+9,37+9,37+9,38+9,38+9,39+9,39+9,40+9,41+9,41+9,42+9,42+9,43+9,43+9,51,51,51,51,51,53,40,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53  ;24 deg.
sheertablemediumright: db 12+9,13+9,13+9,14+9,14+9,15+9,15+9,16+9,16+9,17+9,18+9,18+9,19+9,19+9,20+9,20+9,21+9,22+9,22+9,23+9,23+9,24+9,24+9,25+9,25+9,26+9,27+9,27+9,28+9,28+9,29+9,29+9,30+9,30+9,31+9,32+9,32+9,33+9,33+9,34+9,34+9,35+9,36+9,36+9,37+9,37+9,38+9,38+9,39+9,39+9,40+9,41+9,41+9,42+9,42+9,43+9,43+9,51,51,51,51,51,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53  ;24 deg.
sheertablemaxleft:     db 104+40,96+40,84+40,82+40,80+40,78+40,76+40,74+40,62+40,60+40,58+40,56+40,54+40,52+40,50+40,48+40,46+40,44+40,42+40,40+40,38+40,36+40,34+40,32+40,30+40,28+40,26+40,24+40,22+40,20+40,14+40,12+40,10+40,08+40,06+40,04+40,02+40,40,38,36,34,32,30,28,26,24,22,20,18,16,16,16,16,16,16,16; >45 deg
sheertablemaxright:    db 16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,74,76,78,80,82+0,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,144,144,144,144; >45 deg


iggysheertable:
    db  8, 8, 9, 9, 9, 9,10,10,10,11,11,11,11,12,12,12,13,13,13,14,14,14,14,15,15,15,16,16,16,16,17,17
    db 17,18,18,18,18,19,19,19,20,20,20,20,21,21,21,22,22,22,23,23,23,23,24,24,24,25,25,25,25,26,26,26
    db 27,27,27,27,28,28,28,29,29,29,29,30,30,30,31,31,31,31,32,32,32,33,33,33,34,34,34,34,35,35,35,36
    db 36,36,36,37,37,37,38,38,38,38,39,39,39,40,40,40,40,41,41,41,42,42,42,43,43,43,43,44,44,44,45,45
    db 45,45,46,46,46,47,47,47,47,48,48,48,49,49,49,49,50,50,50,51,51,51,51,52,52,52,53,53,53,54,54,54
    db 54,55,55,55,56,56,56,56,57,57,57,58,58,58,58,59,59,59,60,60,60,60,61,61,61,62,62,62,63,63,63,63
iggysheertablea:
    db  8, 8, 9, 9, 9, 9,10,10,10,11,11,11,11,12,12,12,13,13,13,14,14,14,14,15,15,15,16,16,16,16,17,17
    db 17,18,18,18,18,19,19,19,20,20,20,20,21,21,21,22,22,22,23,23,23,23,24,24,24,25,25,25,25,26,26,26
    db 27,27,27,27,28,28,28,29,29,29,29,30,30,30,31,31,31,31,32,32,32,33,33,33,34,34,34,34,35,35,35,36
    db 36,36,36,37,37,37,38,38,38,38,39,39,39,40,40,40,40,41,41,41,42,42,42,43,43,43,43,44,44,44,45,45
    db 45,45,46,46,46,47,47,47,47,48,48,48,49,49,49,49,50,50,50,51,51,51,51,52,52,52,53,53,53,54,54,54
    db 54,55,55,55,56,56,56,56,57,57,57,58,58,58,58,59,59,59,60,60,60,60,61,61,61,62,62,62,63,63,63,63
iggysheertableb:
    db 13-1,13-1,13-1,14-1,14-1,14-1,14-1,15-1,15-1,15-1,15-1,16-1,16-1,16-1,16-1,17-1
    db 17-1,17-1,17-1,18-1,18-1,18-1,18-1,19-1,19-1,19-1,19-1,20-1,20-1,20-1,20-1,21-1
    db 21-1,21-1,21-1,22-1,22-1,22-1,22-1,23-1,23-1,23-1,23-1,24-1,24-1,24-1,24-1,25-1
    db 25-1,25-1,25-1,25-1,26-1,26-1,26-1,26-1,27-1,27-1,27-1,27-1,28-1,28-1,28-1,28-1
    db 29-1,29-1,29-1,29-1,30-1,30-1,30-1,30-1,31-1,31-1,31-1,31-1,32-1,32-1,32-1,32-1
    db 33-1,33-1,33-1,33-1,34-1,34-1,34-1,34-1,35-1,35-1,35-1,35-1,36-1,36-1,36-1,36-1
    db 37-1,37-1,37-1,37-1,37-1,38-1,38-1,38-1,38-1,39-1,39-1,39-1,39-1,40-1,40-1,40-1
    db 40-1,41-1,41-1,41-1,41-1,42-1,42-1,42-1,42-1,43-1,43-1,43-1,43-1,44-1,44-1,44-1
    db 44-1,45-1,45-1,45-1,45-1,46-1,46-1,46-1,46-1,47-1,47-1,47-1,47-1,48-1,48-1,48-1
    db 48-1,48-1,49-1,49-1,49-1,49-1,50-1,50-1,50-1,50-1,51-1,51-1,51-1,51-1,52-1,52-1
    db 52-1,52-1,53-1,53-1,53-1,53-1,54-1,54-1,54-1,54-1,55-1,55-1,55-1,55-1,56-1,56-1
    db 56-1,56-1,57-1,57-1,57-1,57-1,58-1,58-1,58-1,58-1,59-1,59-1,59-1,59-1,60-1,60-1
iggysheertablec:
    db 17-2,17-2,17-2,18-2,18-2,18-2,18-2,18-2,19-2,19-2,19-2,19-2,19-2,20-2,20-2,20-2
    db 20-2,20-2,20-2,21-2,21-2,21-2,21-2,21-2,22-2,22-2,22-2,22-2,22-2,23-2,23-2,23-2
    db 23-2,23-2,24-2,24-2,24-2,24-2,24-2,25-2,25-2,25-2,25-2,25-2,25-2,26-2,26-2,26-2
    db 26-2,26-2,27-2,27-2,27-2,27-2,27-2,28-2,28-2,28-2,28-2,28-2,29-2,29-2,29-2,29-2
    db 29-2,30-2,30-2,30-2,30-2,30-2,30-2,31-2,31-2,31-2,31-2,31-2,32-2,32-2,32-2,32-2
    db 32-2,33-2,33-2,33-2,33-2,33-2,34-2,34-2,34-2,34-2,34-2,35-2,35-2,35-2,35-2,35-2
    db 35-2,36-2,36-2,36-2,36-2,36-2,37-2,37-2,37-2,37-2,37-2,38-2,38-2,38-2,38-2,38-2
    db 39-2,39-2,39-2,39-2,39-2,40-2,40-2,40-2,40-2,40-2,41-2,41-2,41-2,41-2,41-2,41-2
    db 42-2,42-2,42-2,42-2,42-2,43-2,43-2,43-2,43-2,43-2,44-2,44-2,44-2,44-2,44-2,45-2
    db 45-2,45-2,45-2,45-2,46-2,46-2,46-2,46-2,46-2,46-2,47-2,47-2,47-2,47-2,47-2,48-2
    db 48-2,48-2,48-2,48-2,49-2,49-2,49-2,49-2,49-2,50-2,50-2,50-2,50-2,50-2,51-2,51-2
    db 51-2,51-2,51-2,51-2,52-2,52-2,52-2,52-2,52-2,53-2,53-2,53-2,53-2,53-2,54-2,54-2
iggysheertabled:
    db 22-2,22-2,22-2,22-2,23-2,23-2,23-2,23-2,23-2,23-2,23-2,24-2,24-2,24-2,24-2,24-2
    db 24-2,24-2,25-2,25-2,25-2,25-2,25-2,25-2,25-2,26-2,26-2,26-2,26-2,26-2,26-2,27-2
    db 27-2,27-2,27-2,27-2,27-2,27-2,28-2,28-2,28-2,28-2,28-2,28-2,28-2,29-2,29-2,29-2
    db 29-2,29-2,29-2,29-2,30-2,30-2,30-2,30-2,30-2,30-2,30-2,31-2,31-2,31-2,31-2,31-2
    db 31-2,31-2,32-2,32-2,32-2,32-2,32-2,32-2,32-2,33-2,33-2,33-2,33-2,33-2,33-2,34-2
    db 34-2,34-2,34-2,34-2,34-2,34-2,35-2,35-2,35-2,35-2,35-2,35-2,35-2,36-2,36-2,36-2
    db 36-2,36-2,36-2,36-2,37-2,37-2,37-2,37-2,37-2,37-2,37-2,38-2,38-2,38-2,38-2,38-2
    db 38-2,38-2,39-2,39-2,39-2,39-2,39-2,39-2,39-2,40-2,40-2,40-2,40-2,40-2,40-2,41-2
    db 41-2,41-2,41-2,41-2,41-2,41-2,42-2,42-2,42-2,42-2,42-2,42-2,42-2,43-2,43-2,43-2
    db 43-2,43-2,43-2,43-2,44-2,44-2,44-2,44-2,44-2,44-2,44-2,45-2,45-2,45-2,45-2,45-2
    db 45-2,45-2,46-2,46-2,46-2,46-2,46-2,46-2,46-2,47-2,47-2,47-2,47-2,47-2,47-2,48-2
    db 48-2,48-2,48-2,48-2,48-2,48-2,49-2,49-2,49-2,49-2,49-2,49-2,49-2,50-2,50-2,50-2
iggysheertablee:
    db 26-3,26-3,26-3,26-3,26-3,26-3,27-3,27-3,27-3,27-3,27-3,27-3,27-3,27-3,27-3,27-3
    db 28-3,28-3,28-3,28-3,28-3,28-3,28-3,28-3,28-3,28-3,29-3,29-3,29-3,29-3,29-3,29-3
    db 29-3,29-3,29-3,29-3,30-3,30-3,30-3,30-3,30-3,30-3,30-3,30-3,30-3,30-3,31-3,31-3
    db 31-3,31-3,31-3,31-3,31-3,31-3,31-3,31-3,32-3,32-3,32-3,32-3,32-3,32-3,32-3,32-3
    db 32-3,32-3,33-3,33-3,33-3,33-3,33-3,33-3,33-3,33-3,33-3,33-3,34-3,34-3,34-3,34-3
    db 34-3,34-3,34-3,34-3,34-3,34-3,35-3,35-3,35-3,35-3,35-3,35-3,35-3,35-3,35-3,35-3
    db 36-3,36-3,36-3,36-3,36-3,36-3,36-3,36-3,36-3,36-3,36-3,37-3,37-3,37-3,37-3,37-3
    db 37-3,37-3,37-3,37-3,37-3,38-3,38-3,38-3,38-3,38-3,38-3,38-3,38-3,38-3,38-3,39-3
    db 39-3,39-3,39-3,39-3,39-3,39-3,39-3,39-3,39-3,40-3,40-3,40-3,40-3,40-3,40-3,40-3
    db 40-3,40-3,40-3,41-3,41-3,41-3,41-3,41-3,41-3,41-3,41-3,41-3,41-3,42-3,42-3,42-3
    db 42-3,42-3,42-3,42-3,42-3,42-3,42-3,43-3,43-3,43-3,43-3,43-3,43-3,43-3,43-3,43-3
    db 43-3,44-3,44-3,44-3,44-3,44-3,44-3,44-3,44-3,44-3,44-3,45-3,45-3,45-3,45-3,45-3
iggysheertablef:
    db 31-4,31-4,31-4,31-4,31-4,31-4,31-4,31-4,31-4,31-4,31-4,32-4,32-4,32-4,32-4,32-4
    db 32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4
    db 33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4,33-4
    db 33-4,33-4,33-4,33-4,33-4,33-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4
    db 34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,34-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4,36-4
    db 36-4,36-4,36-4,36-4,36-4,36-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4
    db 37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,38-4,38-4,38-4,38-4,38-4
    db 38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4,38-4
    db 39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4
    db 39-4,39-4,39-4,39-4,39-4,39-4,40-4,40-4,40-4,40-4,40-4,40-4,40-4,40-4,40-4,40-4
iggysheertableg:
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4
iggysheertableh:
    db 40-5,40-5,40-5,40-5,40-5,40-5,40-5,40-5,40-5,40-5,40-5,39-5,39-5,39-5,39-5,39-5
    db 39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5
    db 39-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5
    db 38-5,38-5,38-5,38-5,38-5,38-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5
    db 37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,37-5,36-5,36-5,36-5,36-5,36-5
    db 36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5
    db 36-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5
    db 35-5,35-5,35-5,35-5,35-5,35-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5
    db 34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,34-5,33-5,33-5,33-5,33-5,33-5
    db 33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5
    db 33-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5
    db 32-5,32-5,32-5,32-5,32-5,32-5,31-5,31-5,31-5,31-5,31-5,31-5,31-5,31-5,31-5,31-5
iggysheertablei:
    db 45-5,45-5,45-5,45-5,45-5,45-5,44-5,44-5,44-5,44-5,44-5,44-5,44-5,44-5,44-5,44-5
    db 43-5,43-5,43-5,43-5,43-5,43-5,43-5,43-5,43-5,43-5,42-5,42-5,42-5,42-5,42-5,42-5
    db 42-5,42-5,42-5,42-5,41-5,41-5,41-5,41-5,41-5,41-5,41-5,41-5,41-5,41-5,40-5,40-5
    db 40-5,40-5,40-5,40-5,40-5,40-5,40-5,40-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5,39-5
    db 39-5,39-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,38-5,37-5,37-5,37-5,37-5
    db 37-5,37-5,37-5,37-5,37-5,37-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5,36-5
    db 35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,35-5,34-5,34-5,34-5,34-5,34-5
    db 34-5,34-5,34-5,34-5,34-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,33-5,32-5
    db 32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,32-5,31-5,31-5,31-5,31-5,31-5,31-5,31-5
    db 31-5,31-5,31-5,30-5,30-5,30-5,30-5,30-5,30-5,30-5,30-5,30-5,30-5,29-5,29-5,29-5
    db 29-5,29-5,29-5,29-5,29-5,29-5,29-5,28-5,28-5,28-5,28-5,28-5,28-5,28-5,28-5,28-5
    db 28-5,27-5,27-5,27-5,27-5,27-5,27-5,27-5,27-5,27-5,27-5,26-5,26-5,26-5,26-5,26-5
iggysheertablej:
    db 49-6,49-6,49-6,49-6,48-6,48-6,48-6,48-6,48-6,48-6,48-6,47-6,47-6,47-6,47-6,47-6
    db 47-6,47-6,46-6,46-6,46-6,46-6,46-6,46-6,46-6,45-6,45-6,45-6,45-6,45-6,45-6,44-6
    db 44-6,44-6,44-6,44-6,44-6,44-6,43-6,43-6,43-6,43-6,43-6,43-6,43-6,42-6,42-6,42-6
    db 42-6,42-6,42-6,42-6,41-6,41-6,41-6,41-6,41-6,41-6,41-6,40-6,40-6,40-6,40-6,40-6
    db 40-6,40-6,39-6,39-6,39-6,39-6,39-6,39-6,39-6,38-6,38-6,38-6,38-6,38-6,38-6,37-6
    db 37-6,37-6,37-6,37-6,37-6,37-6,36-6,36-6,36-6,36-6,36-6,36-6,36-6,35-6,35-6,35-6
    db 35-6,35-6,35-6,35-6,34-6,34-6,34-6,34-6,34-6,34-6,34-6,33-6,33-6,33-6,33-6,33-6
    db 33-6,33-6,32-6,32-6,32-6,32-6,32-6,32-6,32-6,31-6,31-6,31-6,31-6,31-6,31-6,30-6
    db 30-6,30-6,30-6,30-6,30-6,30-6,29-6,29-6,29-6,29-6,29-6,29-6,29-6,28-6,28-6,28-6
    db 28-6,28-6,28-6,28-6,27-6,27-6,27-6,27-6,27-6,27-6,27-6,26-6,26-6,26-6,26-6,26-6
    db 26-6,26-6,25-6,25-6,25-6,25-6,25-6,25-6,25-6,24-6,24-6,24-6,24-6,24-6,24-6,23-6
    db 23-6,23-6,23-6,23-6,23-6,23-6,22-6,22-6,22-6,22-6,22-6,22-6,22-6,21-6,21-6,21-6
iggysheertablek:
    db 54-7,54-7,54-7,53-7,53-7,53-7,53-7,53-7,52-7,52-7,52-7,52-7,52-7,51-7,51-7,51-7
    db 51-7,51-7,51-7,50-7,50-7,50-7,50-7,50-7,49-7,49-7,49-7,49-7,49-7,48-7,48-7,48-7
    db 48-7,48-7,47-7,47-7,47-7,47-7,47-7,46-7,46-7,46-7,46-7,46-7,46-7,45-7,45-7,45-7
    db 45-7,45-7,44-7,44-7,44-7,44-7,44-7,43-7,43-7,43-7,43-7,43-7,42-7,42-7,42-7,42-7
    db 42-7,41-7,41-7,41-7,41-7,41-7,41-7,40-7,40-7,40-7,40-7,40-7,39-7,39-7,39-7,39-7
    db 39-7,38-7,38-7,38-7,38-7,38-7,37-7,37-7,37-7,37-7,37-7,36-7,36-7,36-7,36-7,36-7
    db 36-7,35-7,35-7,35-7,35-7,35-7,34-7,34-7,34-7,34-7,34-7,33-7,33-7,33-7,33-7,33-7
    db 32-7,32-7,32-7,32-7,32-7,31-7,31-7,31-7,31-7,31-7,30-7,30-7,30-7,30-7,30-7,30-7
    db 29-7,29-7,29-7,29-7,29-7,28-7,28-7,28-7,28-7,28-7,27-7,27-7,27-7,27-7,27-7,26-7
    db 26-7,26-7,26-7,26-7,25-7,25-7,25-7,25-7,25-7,25-7,24-7,24-7,24-7,24-7,24-7,23-7
    db 23-7,23-7,23-7,23-7,22-7,22-7,22-7,22-7,22-7,21-7,21-7,21-7,21-7,21-7,20-7,20-7
    db 20-7,20-7,20-7,20-7,19-7,19-7,19-7,19-7,19-7,18-7,18-7,18-7,18-7,18-7,17-7,17-7
iggysheertablel:
    db 58-7,58-7,58-7,57-7,57-7,57-7,57-7,56-7,56-7,56-7,56-7,55-7,55-7,55-7,55-7,54-7
    db 54-7,54-7,54-7,53-7,53-7,53-7,53-7,52-7,52-7,52-7,52-7,51-7,51-7,51-7,51-7,50-7
    db 50-7,50-7,50-7,49-7,49-7,49-7,49-7,48-7,48-7,48-7,48-7,47-7,47-7,47-7,47-7,46-7
    db 46-7,46-7,46-7,46-7,45-7,45-7,45-7,45-7,44-7,44-7,44-7,44-7,43-7,43-7,43-7,43-7
    db 42-7,42-7,42-7,42-7,41-7,41-7,41-7,41-7,40-7,40-7,40-7,40-7,39-7,39-7,39-7,39-7
    db 38-7,38-7,38-7,38-7,37-7,37-7,37-7,37-7,36-7,36-7,36-7,36-7,35-7,35-7,35-7,35-7
    db 34-7,34-7,34-7,34-7,34-7,33-7,33-7,33-7,33-7,32-7,32-7,32-7,32-7,31-7,31-7,31-7
    db 31-7,30-7,30-7,30-7,30-7,29-7,29-7,29-7,29-7,28-7,28-7,28-7,28-7,27-7,27-7,27-7
    db 27-7,26-7,26-7,26-7,26-7,25-7,25-7,25-7,25-7,24-7,24-7,24-7,24-7,23-7,23-7,23-7
    db 23-7,23-7,22-7,22-7,22-7,22-7,21-7,21-7,21-7,21-7,20-7,20-7,20-7,20-7,19-7,19-7
    db 19-7,19-7,18-7,18-7,18-7,18-7,17-7,17-7,17-7,17-7,16-7,16-7,16-7,16-7,15-7,15-7
    db 15-7,15-7,14-7,14-7,14-7,14-7,13-7,13-7,13-7,13-7,12-7,12-7,12-7,12-7,11-7,11-7
iggysheertablem:
    db 63-8,63-8,62-8,62-8,62-8,62-8,61-8,61-8,61-8,60-8,60-8,60-8,60-8,59-8,59-8,59-8
    db 58-8,58-8,58-8,57-8,57-8,57-8,57-8,56-8,56-8,56-8,55-8,55-8,55-8,55-8,54-8,54-8
    db 54-8,53-8,53-8,53-8,53-8,52-8,52-8,52-8,51-8,51-8,51-8,51-8,50-8,50-8,50-8,49-8
    db 49-8,49-8,49-8,48-8,48-8,48-8,47-8,47-8,47-8,46-8,46-8,46-8,46-8,45-8,45-8,45-8
    db 44-8,44-8,44-8,44-8,43-8,43-8,43-8,42-8,42-8,42-8,42-8,41-8,41-8,41-8,40-8,40-8
    db 40-8,40-8,39-8,39-8,39-8,38-8,38-8,38-8,37-8,37-8,37-8,37-8,36-8,36-8,36-8,35-8
    db 35-8,35-8,35-8,34-8,34-8,34-8,33-8,33-8,33-8,33-8,32-8,32-8,32-8,31-8,31-8,31-8
    db 31-8,30-8,30-8,30-8,29-8,29-8,29-8,28-8,28-8,28-8,28-8,27-8,27-8,27-8,26-8,26-8
    db 26-8,26-8,25-8,25-8,25-8,24-8,24-8,24-8,24-8,23-8,23-8,23-8,22-8,22-8,22-8,22-8
    db 21-8,21-8,21-8,20-8,20-8,20-8,20-8,19-8,19-8,19-8,18-8,18-8,18-8,17-8,17-8,17-8
    db 17-8,16-8,16-8,16-8,15-8,15-8,15-8,15-8,14-8,14-8,14-8,13-8,13-8,13-8,13-8,12-8
    db 12-8,12-8,11-8,11-8,11-8,11-8,10-8,10-8,10-8, 9-8, 9-8, 9-8, 8-8, 8-8, 8-8, 8-8


	ds		$6000-$,$ff
dephase


; specialsheertilesblock
; block $50
;
phase	$8000

specialsheertilesblock34: equ $50


sheertablenormalleftobject:  db 0
sheertablenormalrightobject: db 0
sheertableleftobject:        db 24+8,24+8,23+8,23+8,23+8,22+8,22+8,21+8,21+8,21+8,20+8,20+8,20+8,19+8,19+8,18+8,18+8,18+8,17+8,17+8,17+8,16+8,16+8,15+8,15+8,15+8,14+8,14+8,14+8,13+8,13+8,12+8,12+8,12+8,11+8,11+8,11+8,10+8,10+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,9+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8,8+8
sheertablerightobject:       db 16,16+1,16+2,16+3,16+4,16+5,16+6,16+7,16+8,16+8,16+8,16+8,16+8,16+8,16+8,16+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,17+8,18+8,18+8,19+8,19+8,19+8,20+8,20+8,20+8,21+8,21+8,22+8,22+8,22+8,23+8,23+8,23+8,24+8,24+8,25+8,25+8,25+8,26+8,26+8,26+8,27+8,27+8,28+8,28+8,28+8,29+8,29+8,29+8,30+8,30+8,31+8,31+8,31+8,32+8,32+8 ;12deg.
sheertableleftmediumobject:  db 40,39,39,38,38,37,37,36,36,35,35,34,33,33,32,32,31,31,30,30,29,28,28,27,27,26,26,25,25,24,24,23,22,22,21,21,20,20,19,19,18,17,17,16,16,15,15,14,14,13,13,12,11,11,10,10,9,9,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8
sheertablerightmediumobject: db 8+8,9+8,9+8,10+8,10+8,11+8,11+8,12+8,12+8,13+8,14+8,14+8,15+8,15+8,16+8,16+8,17+8,18+8,18+8,19+8,19+8,20+8,20+8,21+8,21+8,22+8,23+8,23+8,24+8,24+8,25+8,25+8,26+8,26+8,27+8,28+8,28+8,29+8,29+8,30+8,30+8,31+8,32+8,32+8,33+8,33+8,34+8,34+8,35+8,35+8,36+8,37+8,37+8,38+8,38+8,39+8,39+8,40+8,40+8,41+8,42+8,42+8,43+8,43+8,44+8,44+8,45+8,46+8,46+8,47+8,47+8,48+8,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52,52
sheertablemaxleftobject:     db 86+40,86+40,84+40,82+40,80+40,78+40,76+40,74+40,62+40,60+40,58+40,56+40,54+40,52+40,50+40,48+40,46+40,44+40,42+40,40+40,38+40,36+40,34+40,32+40,30+40,28+40,26+40,24+40,22+40,20+40,18+40,16+40,14+40,12+40,10+40,08+40,06+40,04+40,02+40,40,38,36,34,32,30,28,26,24,22,20,18,10,8,8,8,8,8,8,8,8,8,8,8 ;>45 deg
sheertablemaxrightobject:    db 20-16,22-16,24-16,26-16,28-16,30-16,32-16,34-16,36-16,38-16,40-16,42-16,44-16,46-16,48-16,50-16,52-16,54-16,56-16,58-16,60-16,62-16,74-16,76-16,78-16,80-16,82-16,84-16,86-16,88-16,90-16,92-16,94-16,96-16,98-16,100-16,102-16,104-16,106-16,108-16,110-16,112-16,114-16,116-16,118-16,120-16,122-16,124-16,126-16,128-16,130-16,132-16,134-16,136-16,138-16,140-16,126,128,130,132,134,136,138,140,142,142,142,142,142,142,142 ; >45 deg


iggysheertableobject:
    db  8-2, 8-2, 9-2, 9-2, 9-2, 9-2,10-2,10-2,10-2,11-2,11-2,11-2,11-2,12-2,12-2,12-2
    db 13-2,13-2,13-2,14-2,14-2,14-2,14-2,15-2,15-2,15-2,16-2,16-2,16-2,16-2,17-2,17-2
    db 17-2,18-2,18-2,18-2,18-2,19-2,19-2,19-2,20-2,20-2,20-2,20-2,21-2,21-2,21-2,22-2
    db 22-2,22-2,23-2,23-2,23-2,23-2,24-2,24-2,24-2,25-2,25-2,25-2,25-2,26-2,26-2,26-2
    db 27-2,27-2,27-2,27-2,28-2,28-2,28-2,29-2,29-2,29-2,29-2,30-2,30-2,30-2,31-2,31-2
    db 31-2,31-2,32-2,32-2,32-2,33-2,33-2,33-2,34-2,34-2,34-2,34-2,35-2,35-2,35-2,36-2
    db 36-2,36-2,36-2,37-2,37-2,37-2,38-2,38-2,38-2,38-2,39-2,39-2,39-2,40-2,40-2,40-2
    db 40-2,41-2,41-2,41-2,42-2,42-2,42-2,43-2,43-2,43-2,43-2,44-2,44-2,44-2,45-2,45-2
    db 45-2,45-2,46-2,46-2,46-2,47-2,47-2,47-2,47-2,48-2,48-2,48-2,49-2,49-2,49-2,49-2
    db 50-2,50-2,50-2,51-2,51-2,51-2,51-2,52-2,52-2,52-2,53-2,53-2,53-2,54-2,54-2,54-2
    db 54-2,55-2,55-2,55-2,56-2,56-2,56-2,56-2,57-2,57-2,57-2,58-2,58-2,58-2,58-2,59-2
    db 59-2,59-2,60-2,60-2,60-2,60-2,61-2,61-2,61-2,62-2,62-2,62-2,63-2,63-2,63-2,63-2
iggysheertableaobject:
    db  8-2, 8-2, 9-2, 9-2, 9-2, 9-2,10-2,10-2,10-2,11-2,11-2,11-2,11-2,12-2,12-2,12-2
    db 13-2,13-2,13-2,14-2,14-2,14-2,14-2,15-2,15-2,15-2,16-2,16-2,16-2,16-2,17-2,17-2
    db 17-2,18-2,18-2,18-2,18-2,19-2,19-2,19-2,20-2,20-2,20-2,20-2,21-2,21-2,21-2,22-2
    db 22-2,22-2,23-2,23-2,23-2,23-2,24-2,24-2,24-2,25-2,25-2,25-2,25-2,26-2,26-2,26-2
    db 27-2,27-2,27-2,27-2,28-2,28-2,28-2,29-2,29-2,29-2,29-2,30-2,30-2,30-2,31-2,31-2
    db 31-2,31-2,32-2,32-2,32-2,33-2,33-2,33-2,34-2,34-2,34-2,34-2,35-2,35-2,35-2,36-2
    db 36-2,36-2,36-2,37-2,37-2,37-2,38-2,38-2,38-2,38-2,39-2,39-2,39-2,40-2,40-2,40-2
    db 40-2,41-2,41-2,41-2,42-2,42-2,42-2,43-2,43-2,43-2,43-2,44-2,44-2,44-2,45-2,45-2
    db 45-2,45-2,46-2,46-2,46-2,47-2,47-2,47-2,47-2,48-2,48-2,48-2,49-2,49-2,49-2,49-2
    db 50-2,50-2,50-2,51-2,51-2,51-2,51-2,52-2,52-2,52-2,53-2,53-2,53-2,54-2,54-2,54-2
    db 54-2,55-2,55-2,55-2,56-2,56-2,56-2,56-2,57-2,57-2,57-2,58-2,58-2,58-2,58-2,59-2
    db 59-2,59-2,60-2,60-2,60-2,60-2,61-2,61-2,61-2,62-2,62-2,62-2,63-2,63-2,63-2,63-2
iggysheertablebobject:
    db 13-2,13-2,13-2,14-2,14-2,14-2,14-2,15-2,15-2,15-2,15-2,16-2,16-2,16-2,16-2,17-2
    db 17-2,17-2,17-2,18-2,18-2,18-2,18-2,19-2,19-2,19-2,19-2,20-2,20-2,20-2,20-2,21-2
    db 21-2,21-2,21-2,22-2,22-2,22-2,22-2,23-2,23-2,23-2,23-2,24-2,24-2,24-2,24-2,25-2
    db 25-2,25-2,25-2,25-2,26-2,26-2,26-2,26-2,27-2,27-2,27-2,27-2,28-2,28-2,28-2,28-2
    db 29-2,29-2,29-2,29-2,30-2,30-2,30-2,30-2,31-2,31-2,31-2,31-2,32-2,32-2,32-2,32-2
    db 33-2,33-2,33-2,33-2,34-2,34-2,34-2,34-2,35-2,35-2,35-2,35-2,36-2,36-2,36-2,36-2
    db 37-2,37-2,37-2,37-2,37-2,38-2,38-2,38-2,38-2,39-2,39-2,39-2,39-2,40-2,40-2,40-2
    db 40-2,41-2,41-2,41-2,41-2,42-2,42-2,42-2,42-2,43-2,43-2,43-2,43-2,44-2,44-2,44-2
    db 44-2,45-2,45-2,45-2,45-2,46-2,46-2,46-2,46-2,47-2,47-2,47-2,47-2,48-2,48-2,48-2
    db 48-2,48-2,49-2,49-2,49-2,49-2,50-2,50-2,50-2,50-2,51-2,51-2,51-2,51-2,52-2,52-2
    db 52-2,52-2,53-2,53-2,53-2,53-2,54-2,54-2,54-2,54-2,55-2,55-2,55-2,55-2,56-2,56-2
    db 56-2,56-2,57-2,57-2,57-2,57-2,58-2,58-2,58-2,58-2,59-2,59-2,59-2,59-2,60-2,60-2
iggysheertablecobject:
    db 17-4,17-4,17-4,18-4,18-4,18-4,18-4,18-4,19-4,19-4,19-4,19-4,19-4,20-4,20-4,20-4
    db 20-4,20-4,20-4,21-4,21-4,21-4,21-4,21-4,22-4,22-4,22-4,22-4,22-4,23-4,23-4,23-4
    db 23-4,23-4,24-4,24-4,24-4,24-4,24-4,25-4,25-4,25-4,25-4,25-4,25-4,26-4,26-4,26-4
    db 26-4,26-4,27-4,27-4,27-4,27-4,27-4,28-4,28-4,28-4,28-4,28-4,29-4,29-4,29-4,29-4
    db 29-4,30-4,30-4,30-4,30-4,30-4,30-4,31-4,31-4,31-4,31-4,31-4,32-4,32-4,32-4,32-4
    db 32-4,33-4,33-4,33-4,33-4,33-4,34-4,34-4,34-4,34-4,34-4,35-4,35-4,35-4,35-4,35-4
    db 35-4,36-4,36-4,36-4,36-4,36-4,37-4,37-4,37-4,37-4,37-4,38-4,38-4,38-4,38-4,38-4
    db 39-4,39-4,39-4,39-4,39-4,40-4,40-4,40-4,40-4,40-4,41-4,41-4,41-4,41-4,41-4,41-4
    db 42-4,42-4,42-4,42-4,42-4,43-4,43-4,43-4,43-4,43-4,44-4,44-4,44-4,44-4,44-4,45-4
    db 45-4,45-4,45-4,45-4,46-4,46-4,46-4,46-4,46-4,46-4,47-4,47-4,47-4,47-4,47-4,48-4
    db 48-4,48-4,48-4,48-4,49-4,49-4,49-4,49-4,49-4,50-4,50-4,50-4,50-4,50-4,51-4,51-4
    db 51-4,51-4,51-4,51-4,52-4,52-4,52-4,52-4,52-4,53-4,53-4,53-4,53-4,53-4,54-4,54-4
iggysheertabledobject:
    db 22-4,22-4,22-4,22-4,23-4,23-4,23-4,23-4,23-4,23-4,23-4,24-4,24-4,24-4,24-4,24-4
    db 24-4,24-4,25-4,25-4,25-4,25-4,25-4,25-4,25-4,26-4,26-4,26-4,26-4,26-4,26-4,27-4
    db 27-4,27-4,27-4,27-4,27-4,27-4,28-4,28-4,28-4,28-4,28-4,28-4,28-4,29-4,29-4,29-4
    db 29-4,29-4,29-4,29-4,30-4,30-4,30-4,30-4,30-4,30-4,30-4,31-4,31-4,31-4,31-4,31-4
    db 31-4,31-4,32-4,32-4,32-4,32-4,32-4,32-4,32-4,33-4,33-4,33-4,33-4,33-4,33-4,34-4
    db 34-4,34-4,34-4,34-4,34-4,34-4,35-4,35-4,35-4,35-4,35-4,35-4,35-4,36-4,36-4,36-4
    db 36-4,36-4,36-4,36-4,37-4,37-4,37-4,37-4,37-4,37-4,37-4,38-4,38-4,38-4,38-4,38-4
    db 38-4,38-4,39-4,39-4,39-4,39-4,39-4,39-4,39-4,40-4,40-4,40-4,40-4,40-4,40-4,41-4
    db 41-4,41-4,41-4,41-4,41-4,41-4,42-4,42-4,42-4,42-4,42-4,42-4,42-4,43-4,43-4,43-4
    db 43-4,43-4,43-4,43-4,44-4,44-4,44-4,44-4,44-4,44-4,44-4,45-4,45-4,45-4,45-4,45-4
    db 45-4,45-4,46-4,46-4,46-4,46-4,46-4,46-4,46-4,47-4,47-4,47-4,47-4,47-4,47-4,48-4
    db 48-4,48-4,48-4,48-4,48-4,48-4,49-4,49-4,49-4,49-4,49-4,49-4,49-4,50-4,50-4,50-4
iggysheertableeobject:
    db 26-6,26-6,26-6,26-6,26-6,26-6,27-6,27-6,27-6,27-6,27-6,27-6,27-6,27-6,27-6,27-6
    db 28-6,28-6,28-6,28-6,28-6,28-6,28-6,28-6,28-6,28-6,29-6,29-6,29-6,29-6,29-6,29-6
    db 29-6,29-6,29-6,29-6,30-6,30-6,30-6,30-6,30-6,30-6,30-6,30-6,30-6,30-6,31-6,31-6
    db 31-6,31-6,31-6,31-6,31-6,31-6,31-6,31-6,32-6,32-6,32-6,32-6,32-6,32-6,32-6,32-6
    db 32-6,32-6,33-6,33-6,33-6,33-6,33-6,33-6,33-6,33-6,33-6,33-6,34-6,34-6,34-6,34-6
    db 34-6,34-6,34-6,34-6,34-6,34-6,35-6,35-6,35-6,35-6,35-6,35-6,35-6,35-6,35-6,35-6
    db 36-6,36-6,36-6,36-6,36-6,36-6,36-6,36-6,36-6,36-6,36-6,37-6,37-6,37-6,37-6,37-6
    db 37-6,37-6,37-6,37-6,37-6,38-6,38-6,38-6,38-6,38-6,38-6,38-6,38-6,38-6,38-6,39-6
    db 39-6,39-6,39-6,39-6,39-6,39-6,39-6,39-6,39-6,40-6,40-6,40-6,40-6,40-6,40-6,40-6
    db 40-6,40-6,40-6,41-6,41-6,41-6,41-6,41-6,41-6,41-6,41-6,41-6,41-6,42-6,42-6,42-6
    db 42-6,42-6,42-6,42-6,42-6,42-6,42-6,43-6,43-6,43-6,43-6,43-6,43-6,43-6,43-6,43-6
    db 43-6,44-6,44-6,44-6,44-6,44-6,44-6,44-6,44-6,44-6,44-6,45-6,45-6,45-6,45-6,45-6
iggysheertablefobject:
    db 31-8,31-8,31-8,31-8,31-8,31-8,31-8,31-8,31-8,31-8,31-8,32-8,32-8,32-8,32-8,32-8
    db 32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8,32-8
    db 33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8,33-8
    db 33-8,33-8,33-8,33-8,33-8,33-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8
    db 34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,34-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8,36-8
    db 36-8,36-8,36-8,36-8,36-8,36-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8
    db 37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,37-8,38-8,38-8,38-8,38-8,38-8
    db 38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8,38-8
    db 39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8,39-8
    db 39-8,39-8,39-8,39-8,39-8,39-8,40-8,40-8,40-8,40-8,40-8,40-8,40-8,40-8,40-8,40-8
iggysheertablegobject:
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
    db 35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8,35-8
iggysheertablehobject:
    db 40-10,40-10,40-10,40-10,40-10,40-10,40-10,40-10,40-10,40-10,40-10,39-10,39-10,39-10,39-10,39-10
    db 39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10
    db 39-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10
    db 38-10,38-10,38-10,38-10,38-10,38-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10
    db 37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,37-10,36-10,36-10,36-10,36-10,36-10
    db 36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10
    db 36-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10
    db 35-10,35-10,35-10,35-10,35-10,35-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10
    db 34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,34-10,33-10,33-10,33-10,33-10,33-10
    db 33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10
    db 33-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10
    db 32-10,32-10,32-10,32-10,32-10,32-10,31-10,31-10,31-10,31-10,31-10,31-10,31-10,31-10,31-10,31-10
iggysheertableiobject:
    db 45-10,45-10,45-10,45-10,45-10,45-10,44-10,44-10,44-10,44-10,44-10,44-10,44-10,44-10,44-10,44-10
    db 43-10,43-10,43-10,43-10,43-10,43-10,43-10,43-10,43-10,43-10,42-10,42-10,42-10,42-10,42-10,42-10
    db 42-10,42-10,42-10,42-10,41-10,41-10,41-10,41-10,41-10,41-10,41-10,41-10,41-10,41-10,40-10,40-10
    db 40-10,40-10,40-10,40-10,40-10,40-10,40-10,40-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10,39-10
    db 39-10,39-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,38-10,37-10,37-10,37-10,37-10
    db 37-10,37-10,37-10,37-10,37-10,37-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10,36-10
    db 35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,35-10,34-10,34-10,34-10,34-10,34-10
    db 34-10,34-10,34-10,34-10,34-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,33-10,32-10
    db 32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,32-10,31-10,31-10,31-10,31-10,31-10,31-10,31-10
    db 31-10,31-10,31-10,30-10,30-10,30-10,30-10,30-10,30-10,30-10,30-10,30-10,30-10,29-10,29-10,29-10
    db 29-10,29-10,29-10,29-10,29-10,29-10,29-10,28-10,28-10,28-10,28-10,28-10,28-10,28-10,28-10,28-10
    db 28-10,27-10,27-10,27-10,27-10,27-10,27-10,27-10,27-10,27-10,27-10,26-10,26-10,26-10,26-10,26-10
iggysheertablejobject:
    db 49-12,49-12,49-12,49-12,48-12,48-12,48-12,48-12,48-12,48-12,48-12,47-12,47-12,47-12,47-12,47-12
    db 47-12,47-12,46-12,46-12,46-12,46-12,46-12,46-12,46-12,45-12,45-12,45-12,45-12,45-12,45-12,44-12
    db 44-12,44-12,44-12,44-12,44-12,44-12,43-12,43-12,43-12,43-12,43-12,43-12,43-12,42-12,42-12,42-12
    db 42-12,42-12,42-12,42-12,41-12,41-12,41-12,41-12,41-12,41-12,41-12,40-12,40-12,40-12,40-12,40-12
    db 40-12,40-12,39-12,39-12,39-12,39-12,39-12,39-12,39-12,38-12,38-12,38-12,38-12,38-12,38-12,37-12
    db 37-12,37-12,37-12,37-12,37-12,37-12,36-12,36-12,36-12,36-12,36-12,36-12,36-12,35-12,35-12,35-12
    db 35-12,35-12,35-12,35-12,34-12,34-12,34-12,34-12,34-12,34-12,34-12,33-12,33-12,33-12,33-12,33-12
    db 33-12,33-12,32-12,32-12,32-12,32-12,32-12,32-12,32-12,31-12,31-12,31-12,31-12,31-12,31-12,30-12
    db 30-12,30-12,30-12,30-12,30-12,30-12,29-12,29-12,29-12,29-12,29-12,29-12,29-12,28-12,28-12,28-12
    db 28-12,28-12,28-12,28-12,27-12,27-12,27-12,27-12,27-12,27-12,27-12,26-12,26-12,26-12,26-12,26-12
    db 26-12,26-12,25-12,25-12,25-12,25-12,25-12,25-12,25-12,24-12,24-12,24-12,24-12,24-12,24-12,23-12
    db 23-12,23-12,23-12,23-12,23-12,23-12,22-12,22-12,22-12,22-12,22-12,22-12,22-12,21-12,21-12,21-12
iggysheertablekobject:
    db 54-14,54-14,54-14,53-14,53-14,53-14,53-14,53-14,52-14,52-14,52-14,52-14,52-14,51-14,51-14,51-14
    db 51-14,51-14,51-14,50-14,50-14,50-14,50-14,50-14,49-14,49-14,49-14,49-14,49-14,48-14,48-14,48-14
    db 48-14,48-14,47-14,47-14,47-14,47-14,47-14,46-14,46-14,46-14,46-14,46-14,46-14,45-14,45-14,45-14
    db 45-14,45-14,44-14,44-14,44-14,44-14,44-14,43-14,43-14,43-14,43-14,43-14,42-14,42-14,42-14,42-14
    db 42-14,41-14,41-14,41-14,41-14,41-14,41-14,40-14,40-14,40-14,40-14,40-14,39-14,39-14,39-14,39-14
    db 39-14,38-14,38-14,38-14,38-14,38-14,37-14,37-14,37-14,37-14,37-14,36-14,36-14,36-14,36-14,36-14
    db 36-14,35-14,35-14,35-14,35-14,35-14,34-14,34-14,34-14,34-14,34-14,33-14,33-14,33-14,33-14,33-14
    db 32-14,32-14,32-14,32-14,32-14,31-14,31-14,31-14,31-14,31-14,30-14,30-14,30-14,30-14,30-14,30-14
    db 29-14,29-14,29-14,29-14,29-14,28-14,28-14,28-14,28-14,28-14,27-14,27-14,27-14,27-14,27-14,26-14
    db 26-14,26-14,26-14,26-14,25-14,25-14,25-14,25-14,25-14,25-14,24-14,24-14,24-14,24-14,24-14,23-14
    db 23-14,23-14,23-14,23-14,22-14,22-14,22-14,22-14,22-14,21-14,21-14,21-14,21-14,21-14,20-14,20-14
    db 20-14,20-14,20-14,20-14,19-14,19-14,19-14,19-14,19-14,18-14,18-14,18-14,18-14,18-14,17-14,17-14
iggysheertablelobject:
    db 58-14,58-14,58-14,57-14,57-14,57-14,57-14,56-14,56-14,56-14,56-14,55-14,55-14,55-14,55-14,54-14
    db 54-14,54-14,54-14,53-14,53-14,53-14,53-14,52-14,52-14,52-14,52-14,51-14,51-14,51-14,51-14,50-14
    db 50-14,50-14,50-14,49-14,49-14,49-14,49-14,48-14,48-14,48-14,48-14,47-14,47-14,47-14,47-14,46-14
    db 46-14,46-14,46-14,46-14,45-14,45-14,45-14,45-14,44-14,44-14,44-14,44-14,43-14,43-14,43-14,43-14
    db 42-14,42-14,42-14,42-14,41-14,41-14,41-14,41-14,40-14,40-14,40-14,40-14,39-14,39-14,39-14,39-14
    db 38-14,38-14,38-14,38-14,37-14,37-14,37-14,37-14,36-14,36-14,36-14,36-14,35-14,35-14,35-14,35-14
    db 34-14,34-14,34-14,34-14,34-14,33-14,33-14,33-14,33-14,32-14,32-14,32-14,32-14,31-14,31-14,31-14
    db 31-14,30-14,30-14,30-14,30-14,29-14,29-14,29-14,29-14,28-14,28-14,28-14,28-14,27-14,27-14,27-14
    db 27-14,26-14,26-14,26-14,26-14,25-14,25-14,25-14,25-14,24-14,24-14,24-14,24-14,23-14,23-14,23-14
    db 23-14,23-14,22-14,22-14,22-14,22-14,21-14,21-14,21-14,21-14,20-14,20-14,20-14,20-14,19-14,19-14
    db 19-14,19-14,18-14,18-14,18-14,18-14,17-14,17-14,17-14,17-14,16-14,16-14,16-14,16-14,15-14,15-14
    db 15-14,15-14,14-14,14-14,14-14,14-14,13-14,13-14,13-14,13-14,12-14,12-14,12-14,12-14,11-14,11-14
iggysheertablemobject:
    db 63-16,63-16,62-16,62-16,62-16,62-16,61-16,61-16,61-16,60-16,60-16,60-16,60-16,59-16,59-16,59-16
    db 58-16,58-16,58-16,57-16,57-16,57-16,57-16,56-16,56-16,56-16,55-16,55-16,55-16,55-16,54-16,54-16
    db 54-16,53-16,53-16,53-16,53-16,52-16,52-16,52-16,51-16,51-16,51-16,51-16,50-16,50-16,50-16,49-16
    db 49-16,49-16,49-16,48-16,48-16,48-16,47-16,47-16,47-16,46-16,46-16,46-16,46-16,45-16,45-16,45-16
    db 44-16,44-16,44-16,44-16,43-16,43-16,43-16,42-16,42-16,42-16,42-16,41-16,41-16,41-16,40-16,40-16
    db 40-16,40-16,39-16,39-16,39-16,38-16,38-16,38-16,37-16,37-16,37-16,37-16,36-16,36-16,36-16,35-16
    db 35-16,35-16,35-16,34-16,34-16,34-16,33-16,33-16,33-16,33-16,32-16,32-16,32-16,31-16,31-16,31-16
    db 31-16,30-16,30-16,30-16,29-16,29-16,29-16,28-16,28-16,28-16,28-16,27-16,27-16,27-16,26-16,26-16
    db 26-16,26-16,25-16,25-16,25-16,24-16,24-16,24-16,24-16,23-16,23-16,23-16,22-16,22-16,22-16,22-16
    db 21-16,21-16,21-16,20-16,20-16,20-16,20-16,19-16,19-16,19-16,18-16,18-16,18-16,17-16,17-16,17-16
    db 17-16,16-16,16-16,16-16,15-16,15-16,15-16,15-16,14-16,14-16,14-16,13-16,13-16,13-16,13-16,12-16
    db 12-16,12-16,11-16,11-16,11-16,11-16,10-16,10-16,10-16, 9-16, 9-16, 9-16, 8-16, 8-16, 8-16, 8-16


	ds		$a000-$,$ff
dephase


;
; block $51
;
bosssongblock:               equ $51
phase   $8000
bossbgm:
  db 1
  ;incbin "../music/BOSS.BGM"
  incbin "../music/BOSSI.BGM"
lenghtbossbgm: equ $ - bossbgm
  ds 1000 - lenghtbossbgm
  incbin "../music/BOSSM.BGM"
   
  
      ds                $a000-$,$ff
dephase

;
; block $52
;
;worldmapyoshisongblock:               equ $52
phase   $8000
;  db 0
;  incbin "../music/YOISLAND.BGM"
;  incbin "../music/testbgm.BGM"


grpx66addr:	incbin "../grapx/world3/level3-10a/packedgrpx/grpx.pck" 
grpxblckl66: equ $52 ;vanilla fortress a  
grpx67addr:	incbin "../grapx/world3/level3-10b/packedgrpx/grpx.pck" 
grpxblckl67: equ $52 ;vanilla fortress b  
grpx68addr:	incbin "../grapx/world3/level3-10c/packedgrpx/grpx.pck" 
grpxblckl68: equ $52 ;vanilla fortress c  


backgrl66:  incbin "../level_editor/_levels/maplevel3-10a.stg.pck" 
blockbackgrl66:  equ $52 ;vanilla fortress a     
backgrl67:  incbin "../level_editor/_levels/maplevel3-10b.stg.pck" 
blockbackgrl67:  equ $52 ;vanilla fortress b     
backgrl68:  incbin "../level_editor/_levels/maplevel3-10c.stg.pck" 
blockbackgrl68:  equ $52 ;vanilla fortress c     
   

      ds                $a000-$,$ff
dephase

;
; block $53-$54
;
fortresssongblock:               equ $53
phase   $8000
fortressbgm:
  db 2
  incbin "../music/FORTRESI.BGM"
lenghtfortressbgm: equ $ - fortressbgm
  ds 5000 - lenghtfortressbgm  
  incbin "../music/FORTRESM.BGM"
      ds                $c000-$,$ff
dephase

;
; block $55
;
optionsmenublock:               equ $55
intromenuspritesblock:               equ $55
phase   $8000

backgrl69:  incbin "../level_editor/_levels/maplevel3-11a.stg.pck" 
blockbackgrl69:  equ $55 ;lemmys castle a  
 


  include "optionsmenu.asm"

;the intro menu sprites
coloraddressa:
include "spritedata/ingame sprites level 18/A.tcs.gen"
coloraddressb:
include "spritedata/ingame sprites level 18/B.tcs.gen"
coloraddressc:
include "spritedata/ingame sprites level 18/C.tcs.gen"
coloraddressarrow:
include "spritedata/ingame sprites level 18/arrow.tcs.gen"
coloraddresserase:
include "spritedata/ingame sprites level 18/ERASE.tcs.gen"
coloraddressback:
include "spritedata/ingame sprites level 18/back.tcs.gen"

characteraddressa:
include "spritedata/ingame sprites level 18/A.tgs.gen"
characteraddressb:
include "spritedata/ingame sprites level 18/B.tgs.gen"
characteraddressc:
include "spritedata/ingame sprites level 18/C.tgs.gen"
characteraddressarrow:
include "spritedata/ingame sprites level 18/arrow.tgs.gen"
characteraddresserase:
include "spritedata/ingame sprites level 18/ERASE.tgs.gen"
characteraddressback:
include "spritedata/ingame sprites level 18/back.tgs.gen"  
  
      ds                $a000-$,$ff
dephase


;
; block $56-59
;
ingamespritesblock3: equ $56
phase	$4000

;yellowsuperswitch
characteraddressyellowswitch:
include "spritedata/ingame sprites level 13/yellowswitch.tgs.gen"
coloraddressyellowswitch:
include "spritedata/ingame sprites level 13/yellowswitch.tcs.gen"
characteraddressyellowswitchpressed:
include "spritedata/ingame sprites level 13/yellowswitchpressed.tgs.gen"
coloraddressyellowswitchpressed:
include "spritedata/ingame sprites level 13/yellowswitchpressed.tcs.gen"

;yoshi's house birds
;red
characteraddressredbird1:
include "spritedata/ingame sprites level 14/redbird1.tgs.gen"
coloraddressredbird1:
include "spritedata/ingame sprites level 14/redbird1.tcs.gen"
characteraddressredbird2:
include "spritedata/ingame sprites level 14/redbird2.tgs.gen"
coloraddressredbird2:
include "spritedata/ingame sprites level 14/redbird2.tcs.gen"
characteraddressredbird3:
include "spritedata/ingame sprites level 14/redbird3.tgs.gen"
coloraddressredbird3:
include "spritedata/ingame sprites level 14/redbird3.tcs.gen"
characteraddressredbird4:
include "spritedata/ingame sprites level 14/redbird4.tgs.gen"
coloraddressredbird4:
include "spritedata/ingame sprites level 14/redbird4.tcs.gen"
characteraddressredbird5:
include "spritedata/ingame sprites level 14/redbird5.tgs.gen"
coloraddressredbird5:
include "spritedata/ingame sprites level 14/redbird5.tcs.gen"


;green
characteraddressgreenbird1:
include "spritedata/ingame sprites level 14/greenbird1.tgs.gen"
coloraddressgreenbird1:
include "spritedata/ingame sprites level 14/greenbird1.tcs.gen"
characteraddressgreenbird2:
include "spritedata/ingame sprites level 14/greenbird2.tgs.gen"
coloraddressgreenbird2:
include "spritedata/ingame sprites level 14/greenbird2.tcs.gen"
characteraddressgreenbird3:
include "spritedata/ingame sprites level 14/greenbird3.tgs.gen"
coloraddressgreenbird3:
include "spritedata/ingame sprites level 14/greenbird3.tcs.gen"
characteraddressgreenbird4:
include "spritedata/ingame sprites level 14/greenbird4.tgs.gen"
coloraddressgreenbird4:
include "spritedata/ingame sprites level 14/greenbird4.tcs.gen"
characteraddressgreenbird5:
include "spritedata/ingame sprites level 14/greenbird5.tgs.gen"
coloraddressgreenbird5:
include "spritedata/ingame sprites level 14/greenbird5.tcs.gen"


;yellow
characteraddressyellowbird1:
include "spritedata/ingame sprites level 14/yellowbird1.tgs.gen"
coloraddressyellowbird1:
include "spritedata/ingame sprites level 14/yellowbird1.tcs.gen"
characteraddressyellowbird2:
include "spritedata/ingame sprites level 14/yellowbird2.tgs.gen"
coloraddressyellowbird2:
include "spritedata/ingame sprites level 14/yellowbird2.tcs.gen"
characteraddressyellowbird3:
include "spritedata/ingame sprites level 14/yellowbird3.tgs.gen"
coloraddressyellowbird3:
include "spritedata/ingame sprites level 14/yellowbird3.tcs.gen"
characteraddressyellowbird4:
include "spritedata/ingame sprites level 14/yellowbird4.tgs.gen"
coloraddressyellowbird4:
include "spritedata/ingame sprites level 14/yellowbird4.tcs.gen"
characteraddressyellowbird5:
include "spritedata/ingame sprites level 14/yellowbird5.tgs.gen"
coloraddressyellowbird5:
include "spritedata/ingame sprites level 14/yellowbird5.tcs.gen"

;yoshihousefire
characteraddressfire1:
include "spritedata/ingame sprites level 14/fire1.tgs.gen"
coloraddressfire1:
include "spritedata/ingame sprites level 14/fire1.tcs.gen"
characteraddressfire2:
include "spritedata/ingame sprites level 14/fire2.tgs.gen"
coloraddressfire2:
include "spritedata/ingame sprites level 14/fire2.tcs.gen"


;yoshi apple
characteraddressapple1:
include "spritedata/ingame sprites level 14/apple1.tgs.gen"
coloraddressapple1:
include "spritedata/ingame sprites level 14/apple1.tcs.gen"
characteraddressapple2:
include "spritedata/ingame sprites level 14/apple2.tgs.gen"
coloraddressapple2:
include "spritedata/ingame sprites level 14/apple2.tcs.gen"
characteraddressapple3:
include "spritedata/ingame sprites level 14/apple3.tgs.gen"
coloraddressapple3:
include "spritedata/ingame sprites level 14/apple3.tcs.gen"

;treeline yoshi
characteraddresstreeline:
include "spritedata/ingame sprites level 14/treeline.tgs.gen"
coloraddresstreeline:
include "spritedata/ingame sprites level 14/treeline.tcs.gen"

;demomap text
characteraddresssuper:
include "spritedata/ingame sprites level 18/super.tgs.gen"
coloraddresssuper:
include "spritedata/ingame sprites level 18/super.tcs.gen"
characteraddressmarioworld1:
include "spritedata/ingame sprites level 18/marioworld1.tgs.gen"
coloraddressmarioworld1:
include "spritedata/ingame sprites level 18/marioworld1.tcs.gen"
characteraddressmarioworld2:
include "spritedata/ingame sprites level 18/marioworld2.tgs.gen"
coloraddressmarioworld2:
include "spritedata/ingame sprites level 18/marioworld2.tcs.gen"


;superkoopa
;left
characteraddresssuperkoopaleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left1.tgs.gen"
coloraddresssuperkoopaleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left1.tcs.gen"
characteraddresssuperkoopaleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left2.tgs.gen"
coloraddresssuperkoopaleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left2.tcs.gen"
characteraddresssuperkoopaleft3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left3.tgs.gen"
coloraddresssuperkoopaleft3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left3.tcs.gen"
characteraddresssuperkoopaleft4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left4.tgs.gen"
coloraddresssuperkoopaleft4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left4.tcs.gen"
characteraddresssuperkoopaleft5:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left5.tgs.gen"
coloraddresssuperkoopaleft5:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-left5.tcs.gen"
;right
characteraddresssuperkooparight1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right1.tgs.gen"
coloraddresssuperkooparight1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right1.tcs.gen"
characteraddresssuperkooparight2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right2.tgs.gen"
coloraddresssuperkooparight2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right2.tcs.gen"
characteraddresssuperkooparight3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right3.tgs.gen"
coloraddresssuperkooparight3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right3.tcs.gen"
characteraddresssuperkooparight4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right4.tgs.gen"
coloraddresssuperkooparight4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right4.tcs.gen"
characteraddresssuperkooparight5:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right5.tgs.gen"
coloraddresssuperkooparight5:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-cape-right5.tcs.gen"

;dying
;left
characteraddresssuperkoopadyingleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-left1.tgs.gen"
coloraddresssuperkoopadyingleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-left1.tcs.gen"
characteraddresssuperkoopadyingleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-left2.tgs.gen"
coloraddresssuperkoopadyingleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-left2.tcs.gen"
;right
characteraddresssuperkoopadyingright1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-right1.tgs.gen"
coloraddresssuperkoopadyingright1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-right1.tcs.gen"
characteraddresssuperkoopadyingright2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-right2.tgs.gen"
coloraddresssuperkoopadyingright2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-dying-right2.tcs.gen"


;flying
;left
characteraddresssuperkoopaflyingleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left1.tgs.gen"
coloraddresssuperkoopaflyingleft1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left1.tcs.gen"
characteraddresssuperkoopaflyingleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left2.tgs.gen"
coloraddresssuperkoopaflyingleft2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left2.tcs.gen"
characteraddresssuperkoopaflyingleft3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left3.tgs.gen"
coloraddresssuperkoopaflyingleft3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left3.tcs.gen"
characteraddresssuperkoopaflyingleft4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left4.tgs.gen"
coloraddresssuperkoopaflyingleft4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-left4.tcs.gen"
;right
characteraddresssuperkoopaflyingright1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right1.tgs.gen"
coloraddresssuperkoopaflyingright1:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right1.tcs.gen"
characteraddresssuperkoopaflyingright2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right2.tgs.gen"
coloraddresssuperkoopaflyingright2:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right2.tcs.gen"
characteraddresssuperkoopaflyingright3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right3.tgs.gen"
coloraddresssuperkoopaflyingright3:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right3.tcs.gen"
characteraddresssuperkoopaflyingright4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right4.tgs.gen"
coloraddresssuperkoopaflyingright4:
include "spritedata/sprites level2-01a donut-plains-1/super-koopa-flying-right4.tcs.gen"

;spuugplant
characteraddressspuugplant1:
include "spritedata/sprites level2-01a donut-plains-1/plant1.tgs.gen"
coloraddressspuugplant1:
include "spritedata/sprites level2-01a donut-plains-1/plant1.tcs.gen"
characteraddressspuugplant2:
include "spritedata/sprites level2-01a donut-plains-1/plant2.tgs.gen"
coloraddressspuugplant2:
include "spritedata/sprites level2-01a donut-plains-1/plant2.tcs.gen"
characteraddressspuugplant3:
include "spritedata/sprites level2-01a donut-plains-1/plant3.tgs.gen"
coloraddressspuugplant3:
include "spritedata/sprites level2-01a donut-plains-1/plant3.tcs.gen"
characteraddressspuugplant4:
include "spritedata/sprites level2-01a donut-plains-1/plant4.tgs.gen"
coloraddressspuugplant4:
include "spritedata/sprites level2-01a donut-plains-1/plant4.tcs.gen"

;baseball
characteraddressbaseball1:
include "spritedata/sprites level2-01a donut-plains-1/baseball1.tgs.gen"
coloraddressbaseball1:
include "spritedata/sprites level2-01a donut-plains-1/baseball1.tcs.gen"
characteraddressbaseball2:
include "spritedata/sprites level2-01a donut-plains-1/baseball2.tgs.gen"
coloraddressbaseball2:
include "spritedata/sprites level2-01a donut-plains-1/baseball2.tcs.gen"
characteraddressbaseball3:
include "spritedata/sprites level2-01a donut-plains-1/baseball3.tgs.gen"
coloraddressbaseball3:
include "spritedata/sprites level2-01a donut-plains-1/baseball3.tcs.gen"
characteraddressbaseball4:
include "spritedata/sprites level2-01a donut-plains-1/baseball4.tgs.gen"
coloraddressbaseball4:
include "spritedata/sprites level2-01a donut-plains-1/baseball4.tcs.gen"

;plantbullet
characteraddressplantbullet1:
include "spritedata/sprites level2-01a donut-plains-1/plantbullet1.tgs.gen"
coloraddressplantbullet1:
include "spritedata/sprites level2-01a donut-plains-1/plantbullet1.tcs.gen"
characteraddressplantbullet2:
include "spritedata/sprites level2-01a donut-plains-1/plantbullet2.tgs.gen"
coloraddressplantbullet2:
include "spritedata/sprites level2-01a donut-plains-1/plantbullet2.tcs.gen"


;keybrown
characteraddresskeybrown:
include "spritedata/sprites level2-01a donut-plains-1/key-brown.tgs.gen"
coloraddresskeybrown:
include "spritedata/sprites level2-01a donut-plains-1/key-brown.tcs.gen"

;keypink
characteraddresskeypink:
include "spritedata/sprites level2-01a donut-plains-1/key-pink.tgs.gen"
coloraddresskeypink:
include "spritedata/sprites level2-01a donut-plains-1/key-pink.tcs.gen"

;keyhole
characteraddresskeyhole:
include "spritedata/sprites level2-01a donut-plains-1/keyhole.tgs.gen"
coloraddresskeyhole:
include "spritedata/sprites level2-01a donut-plains-1/keyhole.tcs.gen"

;trowing chuck
;left
characteraddresschucktrowleft1:
include "spritedata/ingame sprites/chuckthrowleft1.tgs.gen"
coloraddresschucktrowleft1:
include "spritedata/ingame sprites/chuckthrowleft1.tcs.gen"
characteraddresschucktrowleft2:
include "spritedata/ingame sprites/chuckthrowleft2.tgs.gen"
coloraddresschucktrowleft2:
include "spritedata/ingame sprites/chuckthrowleft2.tcs.gen"
characteraddresschucktrowleft3:
include "spritedata/ingame sprites/chuckthrowleft3.tgs.gen"
coloraddresschucktrowleft3:
include "spritedata/ingame sprites/chuckthrowleft3.tcs.gen"
characteraddresschucktrowleft4:
include "spritedata/ingame sprites/chuckthrowleft4.tgs.gen"
coloraddresschucktrowleft4:
include "spritedata/ingame sprites/chuckthrowleft4.tcs.gen"
characteraddresschucktrowleft5:
include "spritedata/ingame sprites/chuckthrowleft5.tgs.gen"
coloraddresschucktrowleft5:
include "spritedata/ingame sprites/chuckthrowleft5.tcs.gen"

;right
characteraddresschucktrowright1:
include "spritedata/ingame sprites/chuckthrowright1.tgs.gen"
coloraddresschucktrowright1:
include "spritedata/ingame sprites/chuckthrowright1.tcs.gen"
characteraddresschucktrowright2:
include "spritedata/ingame sprites/chuckthrowright2.tgs.gen"
coloraddresschucktrowright2:
include "spritedata/ingame sprites/chuckthrowright2.tcs.gen"
characteraddresschucktrowright3:
include "spritedata/ingame sprites/chuckthrowright3.tgs.gen"
coloraddresschucktrowright3:
include "spritedata/ingame sprites/chuckthrowright3.tcs.gen"
characteraddresschucktrowright4:
include "spritedata/ingame sprites/chuckthrowright4.tgs.gen"
coloraddresschucktrowright4:
include "spritedata/ingame sprites/chuckthrowright4.tcs.gen"
characteraddresschucktrowright5:
include "spritedata/ingame sprites/chuckthrowright5.tgs.gen"
coloraddresschucktrowright5:
include "spritedata/ingame sprites/chuckthrowright5.tcs.gen"

;round circle
characteraddressboxcircle:
include "spritedata/sprites level2-01b donut-plains-1/singlespritecircle.tgs.gen"
coloraddressboxcircle:
include "spritedata/sprites level2-01b donut-plains-1/singlespritecircle.tcs.gen"


;diagonal step up block
;left
characteraddressdiagonalblockleft:
include "spritedata/sprites level2-01c donut-plains-1/diagonalblockleft.tgs.gen"
coloraddressdiagonalblockleft:
include "spritedata/sprites level2-01c donut-plains-1/diagonalblockleft.tcs.gen"
;right
characteraddressdiagonalblockright:
include "spritedata/sprites level2-01c donut-plains-1/diagonalblockright.tgs.gen"
coloraddressdiagonalblockright:
include "spritedata/sprites level2-01c donut-plains-1/diagonalblockright.tcs.gen"


;cavekoopa
;left
characteraddresscavekoopaleft1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_left1.tgs.gen"
coloraddresscavekoopaleft1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_left1.tcs.gen"
characteraddresscavekoopaleft2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_left2.tgs.gen"
coloraddresscavekoopaleft2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_left2.tcs.gen"
;right
characteraddresscavekooparight1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_right1.tgs.gen"
coloraddresscavekooparight1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_right1.tcs.gen"
characteraddresscavekooparight2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_right2.tgs.gen"
coloraddresscavekooparight2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_right2.tcs.gen"
;dead
characteraddresscavekoopadead:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_dead.tgs.gen"
coloraddresscavekoopadead:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_dead.tcs.gen"

;cavekoopa shell
characteraddresscavekoopashell1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate1.tgs.gen"
coloraddresscavekoopashell1:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate1.tcs.gen"
characteraddresscavekoopashell2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate2.tgs.gen"
coloraddresscavekoopashell2:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate2.tcs.gen"
characteraddresscavekoopashell3:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate3.tgs.gen"
coloraddresscavekoopashell3:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate3.tcs.gen"
characteraddresscavekoopashell4:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate4.tgs.gen"
coloraddresscavekoopashell4:
include "spritedata/sprites level2-02a donut-plains-2/BuzzyBeetle_rotate4.tcs.gen"

;bat
characteraddressbatfly1:
include "spritedata/sprites level2-02a donut-plains-2/batfly1.tgs.gen"
coloraddressbatfly1:
include "spritedata/sprites level2-02a donut-plains-2/batfly1.tcs.gen"
characteraddressbatfly2:
include "spritedata/sprites level2-02a donut-plains-2/batfly2.tgs.gen"
coloraddressbatfly2:
include "spritedata/sprites level2-02a donut-plains-2/batfly2.tcs.gen"
characteraddressbatdead:
include "spritedata/sprites level2-02a donut-plains-2/batdead.tgs.gen"
coloraddressbatdead:
include "spritedata/sprites level2-02a donut-plains-2/batdead.tcs.gen"
characteraddressbatsleep:
include "spritedata/sprites level2-02a donut-plains-2/batsleep.tgs.gen"
coloraddressbatsleep:
include "spritedata/sprites level2-02a donut-plains-2/batsleep.tcs.gen"

;spiketop
characteraddressSpikeTop1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop1.tgs.gen"
coloraddressSpikeTop1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop1.tcs.gen"
characteraddressSpikeTop2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop2.tgs.gen"
coloraddressSpikeTop2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop2.tcs.gen"

characteraddressSpikeTop_turndown:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_turndown.tgs.gen"
coloraddressSpikeTop_turndown:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_turndown.tcs.gen"

characteraddressSpikeTop_down1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_down1.tgs.gen"
coloraddressSpikeTop_down1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_down1.tcs.gen"
characteraddressSpikeTop_down2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_down2.tgs.gen"
coloraddressSpikeTop_down2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_down2.tcs.gen"

characteraddressSpikeTop_turnup:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_turnup.tgs.gen"
coloraddressSpikeTop_turnup:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_turnup.tcs.gen"

characteraddressSpikeTop_up1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_up1.tgs.gen"
coloraddressSpikeTop_up1:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_up1.tcs.gen"
characteraddressSpikeTop_up2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_up2.tgs.gen"
coloraddressSpikeTop_up2:
include "spritedata/sprites level2-02a donut-plains-2/SpikeTop_up2.tcs.gen"
;upsidedown
characteraddressSpikeTop_upsidedown1:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_upsidedown1.tgs.gen"
coloraddressSpikeTop_upsidedown1:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_upsidedown1.tcs.gen"
characteraddressSpikeTop_upsidedown2:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_upsidedown2.tgs.gen"
coloraddressSpikeTop_upsidedown2:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_upsidedown2.tcs.gen"

characteraddressSpikeTop_turnupsidedown:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_turnupsidedown.tgs.gen"
coloraddressSpikeTop_turnupsidedown:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_turnupsidedown.tcs.gen"
characteraddressSpikeTop_turnupsidedownup:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_turnupsidedownup.tgs.gen"
coloraddressSpikeTop_turnupsidedownup:
include "spritedata/sprites level2-02a donut-plains-2/spiketop_turnupsidedownup.tcs.gen"


;football
characteraddressfootballleft:
include "spritedata/sprites level2-02b donut-plains-2/footballleft.tgs.gen"
coloraddressfootballleft:
include "spritedata/sprites level2-02b donut-plains-2/footballleft.tcs.gen"
characteraddressfootballright:
include "spritedata/sprites level2-02b donut-plains-2/footballright.tgs.gen"
coloraddressfootballright:
include "spritedata/sprites level2-02b donut-plains-2/footballright.tcs.gen"


;small ghost
;left
characteraddressghost1left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left1.tgs.gen"
coloraddressghost1left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left1.tcs.gen"
characteraddressghost1left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left2.tgs.gen"
coloraddressghost1left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left2.tcs.gen"
characteraddressghost1left3:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left3.tgs.gen"
coloraddressghost1left3:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left3.tcs.gen"
characteraddressghost1left4:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left4.tgs.gen"
coloraddressghost1left4:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left4.tcs.gen"
characteraddressghost1left5:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left5.tgs.gen"
coloraddressghost1left5:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left5.tcs.gen"
characteraddressghost1left6:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left6.tgs.gen"
coloraddressghost1left6:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1left6.tcs.gen"
;right
characteraddressghost1right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right1.tgs.gen"
coloraddressghost1right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right1.tcs.gen"
characteraddressghost1right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right2.tgs.gen"
coloraddressghost1right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right2.tcs.gen"
characteraddressghost1right3:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right3.tgs.gen"
coloraddressghost1right3:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right3.tcs.gen"
characteraddressghost1right4:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right4.tgs.gen"
coloraddressghost1right4:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right4.tcs.gen"
characteraddressghost1right5:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right5.tgs.gen"
coloraddressghost1right5:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right5.tcs.gen"
characteraddressghost1right6:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right6.tgs.gen"
coloraddressghost1right6:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost1right6.tcs.gen"

characteraddressghost2left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2left1.tgs.gen"
coloraddressghost2left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2left1.tcs.gen"
characteraddressghost3left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3left1.tgs.gen"
coloraddressghost3left1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3left1.tcs.gen"

characteraddressghost2left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2left2.tgs.gen"
coloraddressghost2left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2left2.tcs.gen"
characteraddressghost3left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3left2.tgs.gen"
coloraddressghost3left2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3left2.tcs.gen"

characteraddressghost2right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2right1.tgs.gen"
coloraddressghost2right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2right1.tcs.gen"
characteraddressghost3right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3right1.tgs.gen"
coloraddressghost3right1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3right1.tcs.gen"

characteraddressghost2right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2right2.tgs.gen"
coloraddressghost2right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost2right2.tcs.gen"
characteraddressghost3right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3right2.tgs.gen"
coloraddressghost3right2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghost3right2.tcs.gen"

;pboxdoor
characteraddresspdoor:
include "spritedata/sprites level2-05a donut-ghosthouse/pdoor.tgs.gen"
coloraddresspdoor:
include "spritedata/sprites level2-05a donut-ghosthouse/pdoor.tcs.gen"


;switchbox
characteraddressswitchoff:
include "spritedata/sprites level2-03a donut-plains-3/SwitchOff.tgs.gen"
coloraddressswitchoff:
include "spritedata/sprites level2-03a donut-plains-3/SwitchOff.tcs.gen"
characteraddressswitchon:
include "spritedata/sprites level2-03a donut-plains-3/SwitchOn.tgs.gen"
coloraddressswitchon:
include "spritedata/sprites level2-03a donut-plains-3/SwitchOn.tcs.gen"


;fuzzy
characteraddressfuzzy1:
include "spritedata/sprites level2-03a donut-plains-3/Fuzzy1.tgs.gen"
coloraddressfuzzy1:
include "spritedata/sprites level2-03a donut-plains-3/Fuzzy1.tcs.gen"
characteraddressfuzzy2:
include "spritedata/sprites level2-03a donut-plains-3/Fuzzy2.tgs.gen"
coloraddressfuzzy2:
include "spritedata/sprites level2-03a donut-plains-3/Fuzzy2.tcs.gen"

;yellow shell
characteraddressyellowshell:
include "spritedata/ingame sprites/yellowshell.tgs.gen"
coloraddressyellowshell:
include "spritedata/ingame sprites/yellowshell.tcs.gen"
characteraddressyellowshellback:
include "spritedata/ingame sprites/yellowshell back.tgs.gen"
coloraddressyellowshellback:
include "spritedata/ingame sprites/yellowshell back.tcs.gen"
characteraddressyellowshellfront:
include "spritedata/ingame sprites/yellowshell front.tgs.gen"
coloraddressyellowshellfront:
include "spritedata/ingame sprites/yellowshell front.tcs.gen"
characteraddressyellowshellleft:
include "spritedata/ingame sprites/yellowshell left.tgs.gen"
coloraddressyellowshellleft:
include "spritedata/ingame sprites/yellowshell left.tcs.gen"
characteraddressyellowshellright:
include "spritedata/ingame sprites/yellowshell right.tgs.gen"
coloraddressyellowshellright:
include "spritedata/ingame sprites/yellowshell right.tcs.gen"
characteraddressyellowshelldown:
include "spritedata/ingame sprites/yellowshellupsidedown.tgs.gen"
coloraddressyellowshelldown:
include "spritedata/ingame sprites/yellowshellupsidedown.tcs.gen"


;naked yellow coopa
characteraddressNakedKoopaLeftSlide1yellow:
include "spritedata/yellow koopa/NakedKoopaLeftSlide1.tgs.gen"
coloraddressNakedKoopaLeftSlide1yellow:
include "spritedata/yellow koopa/NakedKoopaLeftSlide1.tcs.gen"
characteraddressNakedKoopaLeftSlide2yellow:
include "spritedata/yellow koopa/NakedKoopaLeftSlide2.tgs.gen"
coloraddressNakedKoopaLeftSlide2yellow:
include "spritedata/yellow koopa/NakedKoopaLeftSlide2.tcs.gen"
characteraddressNakedKooparightSlide1yellow:
include "spritedata/yellow koopa/NakedKoopaRightSlide1.tgs.gen"
coloraddressNakedKooparightSlide1yellow:
include "spritedata/yellow koopa/NakedKoopaRightSlide1.tcs.gen"
characteraddressNakedKooparightSlide2yellow:
include "spritedata/yellow koopa/NakedKoopaRightSlide2.tgs.gen"
coloraddressNakedKooparightSlide2yellow:
include "spritedata/yellow koopa/NakedKoopaRightSlide2.tcs.gen"
characteraddressNakedKoopaLeft1yellow:
include "spritedata/yellow koopa/NakedKoopaLeft1.tgs.gen"
coloraddressNakedKoopaLeft1yellow:
include "spritedata/yellow koopa/NakedKoopaLeft1.tcs.gen"
characteraddressNakedKoopaLeft2yellow:
include "spritedata/yellow koopa/NakedKoopaLeft2.tgs.gen"
coloraddressNakedKoopaLeft2yellow:
include "spritedata/yellow koopa/NakedKoopaLeft2.tcs.gen"
characteraddressNakedKooparight1yellow:
include "spritedata/yellow koopa/NakedKoopaRight1.tgs.gen"
coloraddressNakedKooparight1yellow:
include "spritedata/yellow koopa/NakedKoopaRight1.tcs.gen"
characteraddressNakedKooparight2yellow:
include "spritedata/yellow koopa/NakedKoopaRight2.tgs.gen"
coloraddressNakedKooparight2yellow:
include "spritedata/yellow koopa/NakedKoopaRight2.tcs.gen"
characteraddressNakedKoopaLeftUpSideDownyellow:
include "spritedata/yellow koopa/NakedKoopaLeftUpSideDown.tgs.gen"
coloraddressNakedKoopaLeftUpSideDownyellow:
include "spritedata/yellow koopa/NakedKoopaLeftUpSideDown.tcs.gen"
characteraddressNakedKooparightUpSideDownyellow:
include "spritedata/yellow koopa/NakedKoopaRightUpSideDown.tgs.gen"
coloraddressNakedKooparightUpSideDownyellow:
include "spritedata/yellow koopa/NakedKoopaRightUpSideDown.tcs.gen"


;goomba
;left
characteraddressgoombaleft1:
include "spritedata/sprites level2-04a donut-plains-4/goombaleft1.tgs.gen"
coloraddressgoombaleft1:
include "spritedata/sprites level2-04a donut-plains-4/goombaleft1.tcs.gen"
characteraddressgoombaleft2:
include "spritedata/sprites level2-04a donut-plains-4/goombaleft2.tgs.gen"
coloraddressgoombaleft2:
include "spritedata/sprites level2-04a donut-plains-4/goombaleft2.tcs.gen"
;right
characteraddressgoombaright1:
include "spritedata/sprites level2-04a donut-plains-4/goombaright1.tgs.gen"
coloraddressgoombaright1:
include "spritedata/sprites level2-04a donut-plains-4/goombaright1.tcs.gen"
characteraddressgoombaright2:
include "spritedata/sprites level2-04a donut-plains-4/goombaright2.tgs.gen"
coloraddressgoombaright2:
include "spritedata/sprites level2-04a donut-plains-4/goombaright2.tcs.gen"

;upsidedownleft
characteraddressgoombaupsidedownleft1:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownleft1.tgs.gen"
coloraddressgoombaupsidedownleft1:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownleft1.tcs.gen"
characteraddressgoombaupsidedownleft2:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownleft2.tgs.gen"
coloraddressgoombaupsidedownleft2:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownleft2.tcs.gen"
;upsidedownright
characteraddressgoombaupsidedownright1:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownright1.tgs.gen"
coloraddressgoombaupsidedownright1:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownright1.tcs.gen"
characteraddressgoombaupsidedownright2:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownright2.tgs.gen"
coloraddressgoombaupsidedownright2:
include "spritedata/sprites level2-04a donut-plains-4/goombaupsidedownright2.tcs.gen"

;FlyingHammerBrotherBlock
characteraddressFlyingHammerBrotherBlock:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrotherBlock.tgs.gen"
coloraddressFlyingHammerBrotherBlock:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrotherBlock.tcs.gen"
;the wings
characteraddresssmallwingleft:
include "spritedata/sprites level2-04a donut-plains-4/smallwingleft.tgs.gen"
coloraddresssmallwingleft:
include "spritedata/sprites level2-04a donut-plains-4/smallwingleft.tcs.gen"
characteraddresssmallwingright:
include "spritedata/sprites level2-04a donut-plains-4/smallwingright.tgs.gen"
coloraddresssmallwingright:
include "spritedata/sprites level2-04a donut-plains-4/smallwingright.tcs.gen"
characteraddressbigwingleft:
include "spritedata/sprites level2-04a donut-plains-4/bigwingleft.tgs.gen"
coloraddressbigwingleft:
include "spritedata/sprites level2-04a donut-plains-4/bigwingleft.tcs.gen"
characteraddressbigwingright:
include "spritedata/sprites level2-04a donut-plains-4/bigwingright.tgs.gen"
coloraddressbigwingright:
include "spritedata/sprites level2-04a donut-plains-4/bigwingright.tcs.gen"
;thebrother
characteraddressFlyingHammerBrother1:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrother1.tgs.gen"
coloraddressFlyingHammerBrother1:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrother1.tcs.gen"
characteraddressFlyingHammerBrother2:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrother2.tgs.gen"
coloraddressFlyingHammerBrother2:
include "spritedata/sprites level2-04a donut-plains-4/FlyingHammerBrother2.tcs.gen"
;the hammer
characteraddressHammer1:
include "spritedata/sprites level2-04a donut-plains-4/hammer1.tgs.gen"
coloraddressHammer1:
include "spritedata/sprites level2-04a donut-plains-4/hammer1.tcs.gen"
characteraddressHammer2:
include "spritedata/sprites level2-04a donut-plains-4/hammer2.tgs.gen"
coloraddressHammer2:
include "spritedata/sprites level2-04a donut-plains-4/hammer2.tcs.gen"
characteraddressHammer3:
include "spritedata/sprites level2-04a donut-plains-4/hammer3.tgs.gen"
coloraddressHammer3:
include "spritedata/sprites level2-04a donut-plains-4/hammer3.tcs.gen"
characteraddressHammer4:
include "spritedata/sprites level2-04a donut-plains-4/hammer4.tgs.gen"
coloraddressHammer4:
include "spritedata/sprites level2-04a donut-plains-4/hammer4.tcs.gen"

;paragoomba
characteraddressparagoomba1:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute1.tgs.gen"
coloraddressparagoomba1:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute1.tcs.gen"
characteraddressparagoomba2:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute2.tgs.gen"
coloraddressparagoomba2:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute2.tcs.gen"
characteraddressparagoomba3:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute3.tgs.gen"
coloraddressparagoomba3:
include "spritedata/sprites level2-04a donut-plains-4/goombafallingwithparachute3.tcs.gen"
;parachute
characteraddressparachute1:
include "spritedata/sprites level2-04a donut-plains-4/parachute1.tgs.gen"
coloraddressparachute1:
include "spritedata/sprites level2-04a donut-plains-4/parachute1.tcs.gen"
characteraddressparachute2:
include "spritedata/sprites level2-04a donut-plains-4/parachute2.tgs.gen"
coloraddressparachute2:
include "spritedata/sprites level2-04a donut-plains-4/parachute2.tcs.gen"
characteraddressparachute3:
include "spritedata/sprites level2-04a donut-plains-4/parachute3.tgs.gen"
coloraddressparachute3:
include "spritedata/sprites level2-04a donut-plains-4/parachute3.tcs.gen"

;rip van fish
characteraddressRipVanFishsleep1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishsleep1.tgs.gen"
coloraddressRipVanFishsleep1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishsleep1.tcs.gen"
characteraddressRipVanFishsleep2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishsleep2.tgs.gen"
coloraddressRipVanFishsleep2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishsleep2.tcs.gen"
characteraddressRipVanFishleft1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishleft1.tgs.gen"
coloraddressRipVanFishleft1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishleft1.tcs.gen"
characteraddressRipVanFishleft2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishleft2.tgs.gen"
coloraddressRipVanFishleft2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishleft2.tcs.gen"
characteraddressRipVanFishright1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishright1.tgs.gen"
coloraddressRipVanFishright1:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishright1.tcs.gen"
characteraddressRipVanFishright2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishright2.tgs.gen"
coloraddressRipVanFishright2:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishright2.tcs.gen"
;RipVanFishdead
characteraddressRipVanFishdead:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishdead.tgs.gen"
coloraddressRipVanFishdead:
include "spritedata/sprites level2-06a donut-secret-1/RipVanFishdead.tcs.gen"



;z bubble
characteraddresssmallzzz:
include "spritedata/sprites level2-06a donut-secret-1/smallzzz.tgs.gen"
coloraddresssmallzzz:
include "spritedata/sprites level2-06a donut-secret-1/smallzzz.tcs.gen"
characteraddressbigzzz:
include "spritedata/sprites level2-06a donut-secret-1/bigzzz.tgs.gen"
coloraddressbigzzz:
include "spritedata/sprites level2-06a donut-secret-1/bigzzz.tcs.gen"
characteraddresszzzpop:
include "spritedata/sprites level2-06a donut-secret-1/zzzpop.tgs.gen"
coloraddresszzzpop:
include "spritedata/sprites level2-06a donut-secret-1/zzzpop.tcs.gen"
characteraddressairbubble:
include "spritedata/sprites level2-06a donut-secret-1/airbubble.tgs.gen"
coloraddressairbubble:
include "spritedata/sprites level2-06a donut-secret-1/airbubble.tcs.gen"


;blurp
characteraddressBlurpleft1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpleft1.tgs.gen"
coloraddressBlurpleft1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpleft1.tcs.gen"
characteraddressBlurpleft2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpleft2.tgs.gen"
coloraddressBlurpleft2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpleft2.tcs.gen"
;right
characteraddressBlurpright1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpright1.tgs.gen"
coloraddressBlurpright1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpright1.tcs.gen"
characteraddressBlurpright2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpright2.tgs.gen"
coloraddressBlurpright2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpright2.tcs.gen"
;dead
characteraddressBlurpdead1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpdead1.tgs.gen"
coloraddressBlurpdead1:
include "spritedata/sprites level2-06a donut-secret-1/Blurpdead1.tcs.gen"
characteraddressBlurpdead2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpdead2.tgs.gen"
coloraddressBlurpdead2:
include "spritedata/sprites level2-06a donut-secret-1/Blurpdead2.tcs.gen"

;p-balloon
characteraddresspballoon:
include "spritedata/ingame sprites/p-balloon.tgs.gen"
coloraddresspballoon:
include "spritedata/ingame sprites/p-balloon.tcs.gen"

;spring
characteraddressspring1:
include "spritedata/sprites level2-07a donut-secret-2/spring1.tgs.gen"
coloraddressspring1:
include "spritedata/sprites level2-07a donut-secret-2/spring1.tcs.gen"
characteraddressspring2:
include "spritedata/sprites level2-07a donut-secret-2/spring2.tgs.gen"
coloraddressspring2:
include "spritedata/sprites level2-07a donut-secret-2/spring2.tcs.gen"
characteraddressspring3:
include "spritedata/sprites level2-07a donut-secret-2/spring3.tgs.gen"
coloraddressspring3:
include "spritedata/sprites level2-07a donut-secret-2/spring3.tcs.gen"

;muncher
characteraddressmuncher1:
include "spritedata/sprites level2-07a donut-secret-2/Muncher1.tgs.gen"
coloraddressmuncher1:
include "spritedata/sprites level2-07a donut-secret-2/Muncher1.tcs.gen"
characteraddressmuncher2:
include "spritedata/sprites level2-07a donut-secret-2/Muncher2.tgs.gen"
coloraddressmuncher2:
include "spritedata/sprites level2-07a donut-secret-2/Muncher2.tcs.gen"


;bigboo
characteraddressbigbooleft1:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooleft1.tgs.gen"
coloraddressbigbooleft1:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooleft1.tcs.gen"
characteraddressbigbooleft2:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooleft2.tgs.gen"
coloraddressbigbooleft2:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooleft2.tcs.gen"
characteraddressbigbooright1:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooright1.tgs.gen"
coloraddressbigbooright1:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooright1.tcs.gen"
characteraddressbigbooright2:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooright2.tgs.gen"
coloraddressbigbooright2:
include "spritedata/sprites level2-05a donut-ghosthouse/BigBooright2.tcs.gen"


;eerie
characteraddresseerieleft1:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieleft1.tgs.gen"
coloraddresseerieleft1:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieleft1.tcs.gen"
characteraddresseerieleft2:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieleft2.tgs.gen"
coloraddresseerieleft2:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieleft2.tcs.gen"
characteraddresseerieright1:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieright1.tgs.gen"
coloraddresseerieright1:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieright1.tcs.gen"
characteraddresseerieright2:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieright2.tgs.gen"
coloraddresseerieright2:
include "spritedata/sprites level2-05a donut-ghosthouse/eerieright2.tcs.gen"

;Thwimp
characteraddressThwimp:
include "spritedata/sprites level2-011a mortons-castle/Thwimp.tgs.gen"
coloraddressThwimp:
include "spritedata/sprites level2-011a mortons-castle/Thwimp.tcs.gen"

;ball n chain
characteraddressball:
include "spritedata/sprites level2-011a mortons-castle/ball.tgs.gen"
coloraddressball:
include "spritedata/sprites level2-011a mortons-castle/ball.tcs.gen"
characteraddresschain:
include "spritedata/sprites level2-011a mortons-castle/chain.tgs.gen"
coloraddresschain:
include "spritedata/sprites level2-011a mortons-castle/chain.tcs.gen"

;thwomp1
characteraddressthwomp1:
include "spritedata/sprites level2-011a mortons-castle/Thwomp1.tgs.gen"
coloraddressthwomp1:
include "spritedata/sprites level2-011a mortons-castle/Thwomp1.tcs.gen"
characteraddressthwomp2:
include "spritedata/sprites level2-011a mortons-castle/Thwomp2.tgs.gen"
coloraddressthwomp2:
include "spritedata/sprites level2-011a mortons-castle/Thwomp2.tcs.gen"
characteraddressthwomp3:
include "spritedata/sprites level2-011a mortons-castle/Thwomp3.tgs.gen"
coloraddressthwomp3:
include "spritedata/sprites level2-011a mortons-castle/Thwomp3.tcs.gen"



      ds                $c000-$,$ff
dephase



;
; block $5a
;
demodatablock:               equ $5a
phase   $4000
introdemo:
  incbin "demodata/introdemo.bin"  
level1demo:
  incbin "demodata/level00.bin"
level2demo:
  incbin "demodata/level02.bin"
include "intromenu.asm" ;is the menu on start   
    
      ds                $6000-$,$ff
dephase

;
; block $5b
;
titlemusicblock:               equ $5b
phase   $8000
  db 0
incbin "../music/TITLE.BGM"


backgrl70:  incbin "../level_editor/_levels/maplevel3-11c.stg.pck" 
blockbackgrl70:  equ $5b ;lemmys castle c 
backgrl71:  incbin "../level_editor/_levels/maplevel3-11b.stg.pck" 
blockbackgrl71:  equ $5b ;lemmys castle b 


    
      ds                $a000-$,$ff
dephase

;
; block $5c
;
logoblock:               equ $5c
phase   $8000

include "logo.asm"

;extra gfx data
grpx38addr:	incbin "../grapx/world3/level3-03b/packedgrpx/grpx.pck" 
grpxblckl38: equ $5c ;vanilla 03b
grpxtmaddr:	incbin "../grapx/testmap/packedgrpx/grpx.pck" 
grpxblckltm: equ $5c ;testmap

    
      ds                $a000-$,$ff
dephase

;tilegfx block4
;
; block $5d - 5e
;
phase   $8000

;grpx25addr:	incbin "../grapx/level25/packedgrpx/grpx25.pck" | grpxblckl25: equ $5d
grpx25addr:	incbin "../grapx/world2/level2-04a donut-plains-4/packedgrpx/grpx.pck" 
grpxblckl25: equ $5d
grpx26addr:	incbin "../grapx/world2/level2-06a donut-secret-1/packedgrpx/grpx.pck" 
grpxblckl26: equ $5d
grpx27addr:	incbin "../grapx/world2/level2-06b donut-secret-1/packedgrpx/grpx.pck" 
grpxblckl27: equ $5d
grpx28addr:	incbin "../grapx/world2/level2-07a donut-secret-2/packedgrpx/grpx.pck" 
grpxblckl28: equ $5d
grpx29addr:	incbin "../grapx/world2/level2-07b donut-secret-2/packedgrpx/grpx.pck" 
grpxblckl29: equ $5d
grpx30addr:	incbin "../grapx/world2/level2-11a mortons-castle/packedgrpx/grpx.pck" 
grpxblckl30: equ $5d ;donut castle
grpx31addr:	incbin "../grapx/world2/level2-11c mortons-castle/packedgrpx/grpx.pck" 
grpxblckl31: equ $5d
grpx32addr:	incbin "../grapx/world2/level2-11d mortons-castle/packedgrpx/grpx.pck" 
grpxblckl32: equ $5d
grpx33addr:	incbin "../grapx/world3/level3-04a/packedgrpx/grpx.pck" 
grpxblckl33: equ $5d ;vanilla 04a
grpx34addr:	incbin "../grapx/world3/level3-01a/packedgrpx/grpx.pck" 
grpxblckl34: equ $5d ;vanilla 01a
grpx35addr:	incbin "../grapx/world3/level3-01b/packedgrpx/grpx.pck" 
grpxblckl35: equ $5d ;vanilla 01b
grpx36addr:	incbin "../grapx/world3/level3-01c/packedgrpx/grpx.pck" 
grpxblckl36: equ $5d ;vanilla 01c

    
      ds                $c000-$,$ff
dephase

;packed level block4
;
; block $5f - 60
;
phase   $8000

backgrl25:  incbin "../level_editor/_levels/maplevel2-04a.stg.pck" 
blockbackgrl25:  equ $5f
backgrl26:  incbin "../level_editor/_levels/maplevel2-06a.stg.pck" 
blockbackgrl26:  equ $5f
backgrl27:  incbin "../level_editor/_levels/maplevel2-06b.stg.pck" 
blockbackgrl27:  equ $5f
backgrl28:  incbin "../level_editor/_levels/maplevel2-07a.stg.pck" 
blockbackgrl28:  equ $5f
backgrl29:  incbin "../level_editor/_levels/maplevel2-07b.stg.pck" 
blockbackgrl29:  equ $5f
backgrl30:  incbin "../level_editor/_levels/maplevel2-08a.stg.pck" 
blockbackgrl30:  equ $5f
backgrl31:  incbin "../level_editor/_levels/maplevel2-08b.stg.pck" 
blockbackgrl31:  equ $5f
backgrl32:  incbin "../level_editor/_levels/maplevel2-08c.stg.pck" 
blockbackgrl32:  equ $5f
backgrl33:  incbin "../level_editor/_levels/maplevel2-08d.stg.pck" 
blockbackgrl33:  equ $5f
backgrl34:  incbin "../level_editor/_levels/maplevel2-11a.stg.pck" 
blockbackgrl34:  equ $5f
backgrltm:  incbin "../level_editor/_levels/testmap.stg.pck"
    ;vol
      ds                $c000-$,$ff
dephase


;packed level block5
;
; block $61 - 62
;
phase   $8000

backgrl35:  incbin "../level_editor/_levels/maplevel2-11b.stg.pck" 
blockbackgrl35:  equ $61
backgrl36:  incbin "../level_editor/_levels/maplevel2-11c.stg.pck" 
blockbackgrl36:  equ $61
backgrl37:  incbin "../level_editor/_levels/maplevel2-11d.stg.pck" 
blockbackgrl37:  equ $61
backgrl38:  incbin "../level_editor/_levels/maplevel2-10.stg.pck" 
blockbackgrl38:  equ $61
backgrl39:  incbin "../level_editor/_levels/maplevel2-04b.stg.pck" 
blockbackgrl39:  equ $61
backgrl40:  incbin "../level_editor/_levels/maplevel2-04c.stg.pck" 
blockbackgrl40:  equ $61
backgrl41:  incbin "../level_editor/_levels/maplevel3-04a.stg.pck" 
blockbackgrl41:  equ $61 ;vanilla 04a
backgrl42:  incbin "../level_editor/_levels/maplevel3-01a.stg.pck" 
blockbackgrl42:  equ $61 ;vanilla 01a
backgrl43:  incbin "../level_editor/_levels/maplevel3-01b.stg.pck" 
blockbackgrl43:  equ $61 ;vanilla 01b
backgrl44:  incbin "../level_editor/_levels/maplevel3-01c.stg.pck" 
blockbackgrl44:  equ $61 ;vanilla 01c

    
      ds                $c000-$,$ff
dephase



;
; block $63 - 64
;
romprogblock6:                equ $63
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement4.asm"
  include "spritehider.asm"


romprogblock6end:
  
  
      ds                $c000-$,$ff
dephase

;
; block $65
;
engine2block:      equ $65
phase engineend

engine2start:

  include "engine2.asm"

lenghtengine2block: equ $ - engine2start
  dephase
  
  ds $2000 - lenghtengine2block
  
;done 


;
; block $66-$67
;
;belangrijk!!: hier zitten alle routines in die niets op de map hoeven uit te voeren. Zij worden gecalled vanuit de engine.
;OBSELETE: Can safely be removed after finishing final version. You can safely add code or data in this block
cheatblock:             equ $66
phase	$8000
  include "cheat.asm"  

grpx72addr:	incbin "../grapx/world3/level3-11d/packedgrpx/grpx.pck" 
grpxblckl72: equ $66 ;lemmys castle d 
grpx73addr:	incbin "../grapx/world4/level4-01a/packedgrpx/grpx.pck" 
grpxblckl73: equ $66 ;butterbridge 1a b
grpx75addr:	incbin "../grapx/world4/level4-02a/packedgrpx/grpx.pck" 
grpxblckl75: equ $66 ;butterbridge 2a b
grpx77addr:	incbin "../grapx/world4/level4-03a/packedgrpx/grpx.pck" 
grpxblckl77: equ $66 ;cheesebridge 3a c
grpx78addr:	incbin "../grapx/world4/level4-03b/packedgrpx/grpx.pck" 
grpxblckl78: equ $66 ;cheesebridge 3b

backgrl72:  incbin "../level_editor/_levels/maplevel3-11d.stg.pck" 
blockbackgrl72:  equ $66 ;lemmys castle d
backgrl73:  incbin "../level_editor/_levels/maplevel4-01a.stg.pck" 
blockbackgrl73:  equ $66 ;butterbridge 1a
backgrl74:  incbin "../level_editor/_levels/maplevel4-01b.stg.pck" 
blockbackgrl74:  equ $66 ;butterbridge 1b
backgrl75:  incbin "../level_editor/_levels/maplevel4-02a.stg.pck" 
blockbackgrl75:  equ $66 ;butterbridge 2a
backgrl76:  incbin "../level_editor/_levels/maplevel4-02b.stg.pck" 
blockbackgrl76:  equ $66 ;butterbridge 2b
backgrl77:  incbin "../level_editor/_levels/maplevel4-03a.stg.pck" 
blockbackgrl77:  equ $66 ;cheesebridge 3a
backgrl78:  incbin "../level_editor/_levels/maplevel4-03b.stg.pck" 
blockbackgrl78:  equ $66 ;cheesebridge 3b
backgrl79:  incbin "../level_editor/_levels/maplevel4-03c.stg.pck" 
blockbackgrl79:  equ $66 ;cheesebridge 3c


  
	ds		$c000-$,$ff
dephase


;
; block $68
;
hauntedmusicblock:               equ $68
phase   $8000
  db 0
incbin "../music/HAUNTED.BGM"

;extra packed stuff
grpx37addr:	incbin "../grapx/world3/level3-03a/packedgrpx/grpx.pck" 
grpxblckl37: equ $68 ;vanilla 03a
    
      ds                $a000-$,$ff
dephase


;
; block $69 - 6a
;
romprogblock7:                equ $69
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement5.asm"


romprogblock7end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $6b
;
bvictoryblock:                    equ $6b
phase   $8000
  db 0
  incbin "../music/BVICTORY.BGM"


;extra background levels  
backgrl45:  incbin "../level_editor/_levels/maplevel3-03a.stg.pck" 
blockbackgrl45:  equ $6b ;vanilla 03a
backgrl46:  incbin "../level_editor/_levels/maplevel3-02a.stg.pck" 
blockbackgrl46:  equ $6b ;vanilla 02a    


        ds                $a000-$,$ff
dephase



;
; block $6c-6f
;
ingamespritesblock4: equ $6c
phase	$4000


;DryBonesLeft
characteraddressDryBonesLeft1:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft1.tgs.gen"
coloraddressDryBonesLeft1:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft1.tcs.gen"
characteraddressDryBonesleft2:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft2.tgs.gen"
coloraddressDryBonesleft2:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft2.tcs.gen"
characteraddressDryBonesleft3:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft3.tgs.gen"
coloraddressDryBonesleft3:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft3.tcs.gen"
characteraddressDryBonesleft4:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft4.tgs.gen"
coloraddressDryBonesleft4:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLeft4.tcs.gen"

;DryBonesright
characteraddressDryBonesright1:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight1.tgs.gen"
coloraddressDryBonesright1:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight1.tcs.gen"
characteraddressDryBonesright2:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight2.tgs.gen"
coloraddressDryBonesright2:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight2.tcs.gen"
characteraddressDryBonesright3:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight3.tgs.gen"
coloraddressDryBonesright3:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight3.tcs.gen"
characteraddressDryBonesright4:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight4.tgs.gen"
coloraddressDryBonesright4:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRight4.tcs.gen"


;morton
;left
characteraddressmortonleft1:
include "spritedata/sprites level2-011d mortons-castle/mortonleft1.tgs.gen"
coloraddressmortonleft1:
include "spritedata/sprites level2-011d mortons-castle/mortonleft1.tcs.gen"
characteraddressmortonleft2:
include "spritedata/sprites level2-011d mortons-castle/mortonleft2.tgs.gen"
coloraddressmortonleft2:
include "spritedata/sprites level2-011d mortons-castle/mortonleft2.tcs.gen"
;right
characteraddressmortonright1:
include "spritedata/sprites level2-011d mortons-castle/mortonright1.tgs.gen"
coloraddressmortonright1:
include "spritedata/sprites level2-011d mortons-castle/mortonright1.tcs.gen"
characteraddressmortonright2:
include "spritedata/sprites level2-011d mortons-castle/mortonright2.tgs.gen"
coloraddressmortonright2:
include "spritedata/sprites level2-011d mortons-castle/mortonright2.tcs.gen"
;upwallleft
characteraddressmortonleftupwall1:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupwall1.tgs.gen"
coloraddressmortonleftupwall1:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupwall1.tcs.gen"
characteraddressmortonleftupwall2:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupwall2.tgs.gen"
coloraddressmortonleftupwall2:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupwall2.tcs.gen"
;upwallright
characteraddressmortonrightupwall1:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupwall1.tgs.gen"
coloraddressmortonrightupwall1:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupwall1.tcs.gen"
characteraddressmortonrightupwall2:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupwall2.tgs.gen"
coloraddressmortonrightupwall2:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupwall2.tcs.gen"
;upsidedownleft
characteraddressmortonleftupsidedown1:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupsidedown1.tgs.gen"
coloraddressmortonleftupsidedown1:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupsidedown1.tcs.gen"
characteraddressmortonleftupsidedown2:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupsidedown2.tgs.gen"
coloraddressmortonleftupsidedown2:
include "spritedata/sprites level2-011d mortons-castle/mortonleftupsidedown2.tcs.gen"
;upsidedownright
characteraddressmortonrightupsidedown1:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupsidedown1.tgs.gen"
coloraddressmortonrightupsidedown1:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupsidedown1.tcs.gen"
characteraddressmortonrightupsidedown2:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupsidedown2.tgs.gen"
coloraddressmortonrightupsidedown2:
include "spritedata/sprites level2-011d mortons-castle/mortonrightupsidedown2.tcs.gen"
;hit
characteraddressmortonhit1:
include "spritedata/sprites level2-011d mortons-castle/mortonhit1.tgs.gen"
coloraddressmortonhit1:
include "spritedata/sprites level2-011d mortons-castle/mortonhit1.tcs.gen"
characteraddressmortonhit2:
include "spritedata/sprites level2-011d mortons-castle/mortonhit2.tgs.gen"
coloraddressmortonhit2:
include "spritedata/sprites level2-011d mortons-castle/mortonhit2.tcs.gen"
characteraddressmortonhit3:
include "spritedata/sprites level2-011d mortons-castle/mortonhit3.tgs.gen"
coloraddressmortonhit3:
include "spritedata/sprites level2-011d mortons-castle/mortonhit3.tcs.gen"
characteraddressmortonhit4:
include "spritedata/sprites level2-011d mortons-castle/mortonhit4.tgs.gen"
coloraddressmortonhit4:
include "spritedata/sprites level2-011d mortons-castle/mortonhit4.tcs.gen"


;brown platform
characteraddressplatformbrown:
include "spritedata/sprites level2-03a donut-plains-3/platformbrown.tgs.gen"
coloraddressplatformbrown:
include "spritedata/sprites level2-03a donut-plains-3/platformbrown.tcs.gen"

;cutscene objects
characteraddressblastthing1:
include "cutscenes/blastthing1.tgs.gen"
coloraddressblastthing1:
include "cutscenes/blastthing1.tcs.gen"
characteraddressblastthing2:
include "cutscenes/blastthing2.tgs.gen"
coloraddressblastthing2:
include "cutscenes/blastthing2.tcs.gen"
characteraddressfuse1:
include "cutscenes/fuse1.tgs.gen"
coloraddressfuse1:
include "cutscenes/fuse1.tcs.gen"
characteraddressfuse2:
include "cutscenes/fuse2.tgs.gen"
coloraddressfuse2:
include "cutscenes/fuse2.tcs.gen"
characteraddresspfewcloudsimple1:
include "cutscenes/pfewcloudsimple1.tgs.gen"
coloraddresspfewcloudsimple1:
include "cutscenes/pfewcloudsimple1.tcs.gen"
characteraddresspfewcloudsimple2:
include "cutscenes/pfewcloudsimple2.tgs.gen"
coloraddresspfewcloudsimple2:
include "cutscenes/pfewcloudsimple2.tcs.gen"
characteraddresspfewcloudsimple3:
include "cutscenes/pfewcloudsimple3.tgs.gen"
coloraddresspfewcloudsimple3:
include "cutscenes/pfewcloudsimple3.tcs.gen"
characteraddresspfewcloudsimple4:
include "cutscenes/pfewcloudsimple4.tgs.gen"
coloraddresspfewcloudsimple4:
include "cutscenes/pfewcloudsimple4.tcs.gen"

;minichuck
characteraddressminichucksit1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichucksit1.tgs.gen"
coloraddressminichucksit1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichucksit1.tcs.gen"
characteraddressminichucksit2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichucksit2.tgs.gen"
coloraddressminichucksit2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichucksit2.tcs.gen"
characteraddressminichuckjumpleft:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckjumpleft.tgs.gen"
coloraddressminichuckjumpleft:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckjumpleft.tcs.gen"
characteraddressminichuckjumpright:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckjump.tgs.gen"
coloraddressminichuckjumpright:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckjump.tcs.gen"
characteraddressminichuckrunleft1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunleft1.tgs.gen"
coloraddressminichuckrunleft1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunleft1.tcs.gen"
characteraddressminichuckrunleft2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunleft2.tgs.gen"
coloraddressminichuckrunleft2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunleft2.tcs.gen"
characteraddressminichuckrunright1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunright1.tgs.gen"
coloraddressminichuckrunright1:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunright1.tcs.gen"
characteraddressminichuckrunright2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunright2.tgs.gen"
coloraddressminichuckrunright2:
include "spritedata/sprites level3-02a/minichuck 2 layered sprite/minichuckrunright2.tcs.gen"


;floating skulls
characteraddressfloatingskull1:
include "spritedata/sprites level3-03a/floatingskull1.tgs.gen"
coloraddressfloatingskull1:
include "spritedata/sprites level3-03a/floatingskull1.tcs.gen"
characteraddressfloatingskull2:
include "spritedata/sprites level3-03a/floatingskull2.tgs.gen"
coloraddressfloatingskull2:
include "spritedata/sprites level3-03a/floatingskull2.tcs.gen"

;blargh
characteraddressblargheye:
include "spritedata/sprites level3-03a/blargheye.tgs.gen"
coloraddressblargheye:
include "spritedata/sprites level3-03a/blargheye.tcs.gen"
characteraddressblargheye2:
include "spritedata/sprites level3-03a/blargheye2.tgs.gen"
coloraddressblargheye2:
include "spritedata/sprites level3-03a/blargheye2.tcs.gen"
characteraddressblargh:
include "spritedata/sprites level3-03a/blargh.tgs.gen"
coloraddressblargh:
include "spritedata/sprites level3-03a/blargh.tcs.gen"
characteraddressblarghhalf:
include "spritedata/sprites level3-03a/blarghhalf.tgs.gen"
emptysprite6c:
include "spritedata/ingame sprites/empty.tgs.gen"

;piranjaplant2
characteraddresspiranjaplant2:
include "spritedata/sprites level3-03a/piranjaplant2.tgs.gen"
coloraddresspiranjaplant2:
include "spritedata/sprites level3-03a/piranjaplant2.tcs.gen"
characteraddresspiranjaplant22:
include "spritedata/sprites level3-03a/piranjaplant22.tgs.gen"
coloraddresspiranjaplant22:
include "spritedata/sprites level3-03a/piranjaplant22.tcs.gen"

;cavekoopa shelpje ondersteboven
characteraddresscavekoopaupsidedown:
include "spritedata/sprites level2-02a donut-plains-2/cavekoopa_upsidedown.tgs.gen"
coloraddresscavekoopaupsidedown:
include "spritedata/sprites level2-02a donut-plains-2/cavekoopa_upsidedown.tcs.gen"


;the big ghost blob
characteraddressghostblob1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghostblob1.tgs.gen"
coloraddressghostblob1:
include "spritedata/sprites level2-05a donut-ghosthouse/ghostblob1.tcs.gen"
characteraddressghostblob2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghostblob2.tgs.gen"
coloraddressghostblob2:
include "spritedata/sprites level2-05a donut-ghosthouse/ghostblob2.tcs.gen"




;springboard

characteraddressspringboard:
include "spritedata/sprites level3-04a/springboard.tgs.gen"
coloraddressspringboard:
include "spritedata/sprites level3-04a/springboard.tcs.gen"

characteraddressspringboardleft1:
include "spritedata/sprites level3-04a/springboardleft1.tgs.gen"
coloraddressspringboardleft1:
include "spritedata/sprites level3-04a/springboardleft1.tcs.gen"
characteraddressspringboardleft2:
include "spritedata/sprites level3-04a/springboardleft2.tgs.gen"
coloraddressspringboardleft2:
include "spritedata/sprites level3-04a/springboardleft2.tcs.gen"
characteraddressspringboardleft3:
include "spritedata/sprites level3-04a/springboardleft3.tgs.gen"
coloraddressspringboardleft3:
include "spritedata/sprites level3-04a/springboardleft3.tcs.gen"
characteraddressspringboardleft4:
include "spritedata/sprites level3-04a/springboardleft4.tgs.gen"
coloraddressspringboardleft4:
include "spritedata/sprites level3-04a/springboardleft4.tcs.gen"

characteraddressspringboardright1:
include "spritedata/sprites level3-04a/springboardright1.tgs.gen"
coloraddressspringboardright1:
include "spritedata/sprites level3-04a/springboardright1.tcs.gen"
characteraddressspringboardright2:
include "spritedata/sprites level3-04a/springboardright2.tgs.gen"
coloraddressspringboardright2:
include "spritedata/sprites level3-04a/springboardright2.tcs.gen"
characteraddressspringboardright3:
include "spritedata/sprites level3-04a/springboardright3.tgs.gen"
coloraddressspringboardright3:
include "spritedata/sprites level3-04a/springboardright3.tcs.gen"
characteraddressspringboardright4:
include "spritedata/sprites level3-04a/springboardright4.tgs.gen"
coloraddressspringboardright4:
include "spritedata/sprites level3-04a/springboardright4.tcs.gen"




;bill
characteraddressbillleft:
include "spritedata/sprites level3-04a/billleft.tgs.gen"
coloraddressbillleft:
include "spritedata/sprites level3-04a/billleft.tcs.gen"
characteraddressbillright:
include "spritedata/sprites level3-04a/billright.tgs.gen"
coloraddressbillright:
include "spritedata/sprites level3-04a/billright.tcs.gen"
characteraddressbillup:
include "spritedata/sprites level3-04a/billup.tgs.gen"
coloraddressbillup:
include "spritedata/sprites level3-04a/billup.tcs.gen"

;spikey
characteraddressspikeyleft1:
include "spritedata/sprites level3-07a/spikeyleft1.tgs.gen"
coloraddressspikeyleft1:
include "spritedata/sprites level3-07a/spikeyleft1.tcs.gen"
characteraddressspikeyleft2:
include "spritedata/sprites level3-07a/spikeyleft2.tgs.gen"
coloraddressspikeyleft2:
include "spritedata/sprites level3-07a/spikeyleft2.tcs.gen"
characteraddressspikeyright1:
include "spritedata/sprites level3-07a/spikeyright1.tgs.gen"
coloraddressspikeyright1:
include "spritedata/sprites level3-07a/spikeyright1.tcs.gen"
characteraddressspikeyright2:
include "spritedata/sprites level3-07a/spikeyright2.tgs.gen"
coloraddressspikeyright2:
include "spritedata/sprites level3-07a/spikeyright2.tcs.gen"

characteraddressspikeball1:
include "spritedata/sprites level3-07a/spikeball1.tgs.gen"
coloraddressspikeball1:
include "spritedata/sprites level3-07a/spikeball1.tcs.gen"
characteraddressspikeball2:
include "spritedata/sprites level3-07a/spikeball2.tgs.gen"
coloraddressspikeball2:
include "spritedata/sprites level3-07a/spikeball2.tcs.gen"

characteraddressnotebox:
include "spritedata/sprites level3-07a/notebox.tgs.gen"
coloraddressnotebox:
include "spritedata/sprites level3-07a/notebox.tcs.gen"




;lakitu
characteraddresslakitupipe1left:
include "spritedata/sprites level3-07a/lakitupipe1left.tgs.gen"
coloraddresslakitupipe1left:
include "spritedata/sprites level3-07a/lakitupipe1left.tcs.gen"
characteraddresslakitupipe1right:
include "spritedata/sprites level3-07a/lakitupipe1right.tgs.gen"
coloraddresslakitupipe1right:
include "spritedata/sprites level3-07a/lakitupipe1right.tcs.gen"
;comming up
characteraddresslakitupipe1:
include "spritedata/sprites level3-07a/lakitupipe1.tgs.gen"
coloraddresslakitupipe1:
include "spritedata/sprites level3-07a/lakitupipe1.tcs.gen"
characteraddresslakitupipe2:
include "spritedata/sprites level3-07a/lakitupipe2.tgs.gen"
coloraddresslakitupipe2:
include "spritedata/sprites level3-07a/lakitupipe2.tcs.gen"
characteraddresslakitupipe3:
include "spritedata/sprites level3-07a/lakitupipe3.tgs.gen"
coloraddresslakitupipe3:
include "spritedata/sprites level3-07a/lakitupipe3.tcs.gen"
;dying
characteraddresslakitudying:
include "spritedata/sprites level3-07a/lakitudying.tgs.gen"
coloraddresslakitudying:
include "spritedata/sprites level3-07a/lakitudying.tcs.gen"



;boomb
characteraddressboomb1:
include "spritedata/sprites level3-07a/boomb1.tgs.gen"
coloraddressboomb1:
include "spritedata/sprites level3-07a/boomb1.tcs.gen"
characteraddressboomb2:
include "spritedata/sprites level3-07a/boomb2.tgs.gen"
coloraddressboomb2:
include "spritedata/sprites level3-07a/boomb2.tcs.gen"
characteraddressboomb3:
include "spritedata/sprites level3-07a/boomb3.tgs.gen"
coloraddressboomb3:
include "spritedata/sprites level3-07a/boomb3.tcs.gen"
characteraddressboomb1left:
include "spritedata/sprites level3-07a/boomb1left.tgs.gen"
coloraddressboomb1left:
include "spritedata/sprites level3-07a/boomb1left.tcs.gen"
characteraddressboomb2left:
include "spritedata/sprites level3-07a/boomb2left.tgs.gen"
coloraddressboomb2left:
include "spritedata/sprites level3-07a/boomb2left.tcs.gen"
characteraddressboomb3left:
include "spritedata/sprites level3-07a/boomb3left.tgs.gen"
coloraddressboomb3left:
include "spritedata/sprites level3-07a/boomb3left.tcs.gen"
characteraddressboomb4left:
include "spritedata/sprites level3-07a/boomb4left.tgs.gen"
coloraddressboomb4left:
include "spritedata/sprites level3-07a/boomb4left.tcs.gen"
characteraddressboomb1right:
include "spritedata/sprites level3-07a/boomb1right.tgs.gen"
coloraddressboomb1right:
include "spritedata/sprites level3-07a/boomb1right.tcs.gen"
characteraddressboomb2right:
include "spritedata/sprites level3-07a/boomb2right.tgs.gen"
coloraddressboomb2right:
include "spritedata/sprites level3-07a/boomb2right.tcs.gen"
characteraddressboomb3right:
include "spritedata/sprites level3-07a/boomb3right.tgs.gen"
coloraddressboomb3right:
include "spritedata/sprites level3-07a/boomb3right.tcs.gen"
characteraddressboomb4right:
include "spritedata/sprites level3-07a/boomb4right.tgs.gen"
coloraddressboomb4right:
include "spritedata/sprites level3-07a/boomb4right.tcs.gen"
;upsidedown
characteraddressboombupsidedownleft1:
include "spritedata/sprites level3-07a/boombupsidedownleft1.tgs.gen"
coloraddressboombupsidedownleft1:
include "spritedata/sprites level3-07a/boombupsidedownleft1.tcs.gen"
characteraddressboombupsidedownleft2:
include "spritedata/sprites level3-07a/boombupsidedownleft2.tgs.gen"
coloraddressboombupsidedownleft2:
include "spritedata/sprites level3-07a/boombupsidedownleft2.tcs.gen"
characteraddressboombupsidedownright1:
include "spritedata/sprites level3-07a/boombupsidedownright1.tgs.gen"
coloraddressboombupsidedownright1:
include "spritedata/sprites level3-07a/boombupsidedownright1.tcs.gen"
characteraddressboombupsidedownright2:
include "spritedata/sprites level3-07a/boombupsidedownright2.tgs.gen"
coloraddressboombupsidedownright2:
include "spritedata/sprites level3-07a/boombupsidedownright2.tcs.gen"


;pbox grey
characteraddresspboxgrey:
include "spritedata/ingame sprites/p-boxgrey.tgs.gen"
coloraddresspboxgrey:
include "spritedata/ingame sprites/p-boxgrey.tcs.gen"
characteraddresspboxgreyjumpedon:
include "spritedata/ingame sprites/p-boxgreyjumpedon.tgs.gen"
coloraddresspboxgreyjumpedon:
include "spritedata/ingame sprites/p-boxgreyjumpedon.tcs.gen"

;silver coin
characteraddresscoin2a:
include "spritedata/ingame sprites/coin2a.tgs.gen"
coloraddresscoin2a:
include "spritedata/ingame sprites/coin2a.tcs.gen"
characteraddresscoin2b:
include "spritedata/ingame sprites/coin2b.tgs.gen"
coloraddresscoin2b:
include "spritedata/ingame sprites/coin2b.tcs.gen"
characteraddresscoin2c:
include "spritedata/ingame sprites/coin2c.tgs.gen"
coloraddresscoin2c:
include "spritedata/ingame sprites/coin2c.tcs.gen"
characteraddresscoin2d:
include "spritedata/ingame sprites/coin2d.tgs.gen"
coloraddresscoin2d:
include "spritedata/ingame sprites/coin2d.tcs.gen"


;special extra watercoverup for vanilla secret 3...
characteraddressextracover1:
include "spritedata/ingame sprites level 06/extracover1.tgs.gen"
coloraddressextracover:
include "spritedata/ingame sprites level 06/extracover.tcs.gen"
characteraddressextracover2:
include "spritedata/ingame sprites level 06/extracover2.tgs.gen"


;porcupuffer
characteraddressporcupufferleft11:
include "spritedata/sprites level3-08a/porcupufferleft11.tgs.gen"
coloraddressporcupufferleft11:
include "spritedata/sprites level3-08a/porcupufferleft11.tcs.gen"
characteraddressporcupufferleft12:
include "spritedata/sprites level3-08a/porcupufferleft12.tgs.gen"
coloraddressporcupufferleft12:
include "spritedata/sprites level3-08a/porcupufferleft12.tcs.gen"
characteraddressporcupufferleft21:
include "spritedata/sprites level3-08a/porcupufferleft21.tgs.gen"
coloraddressporcupufferleft21:
include "spritedata/sprites level3-08a/porcupufferleft21.tcs.gen"
characteraddressporcupufferleft22:
include "spritedata/sprites level3-08a/porcupufferleft22.tgs.gen"
coloraddressporcupufferleft22:
include "spritedata/sprites level3-08a/porcupufferleft22.tcs.gen"

characteraddressporcupufferright11:
include "spritedata/sprites level3-08a/porcupufferright11.tgs.gen"
coloraddressporcupufferright11:
include "spritedata/sprites level3-08a/porcupufferright11.tcs.gen"
characteraddressporcupufferright12:
include "spritedata/sprites level3-08a/porcupufferright12.tgs.gen"
coloraddressporcupufferright12:
include "spritedata/sprites level3-08a/porcupufferright12.tcs.gen"
characteraddressporcupufferright21:
include "spritedata/sprites level3-08a/porcupufferright21.tgs.gen"
coloraddressporcupufferright21:
include "spritedata/sprites level3-08a/porcupufferright21.tcs.gen"
characteraddressporcupufferright22:
include "spritedata/sprites level3-08a/porcupufferright22.tgs.gen"
coloraddressporcupufferright22:
include "spritedata/sprites level3-08a/porcupufferright22.tcs.gen"

;dolphins
characteraddressdolphinleft1:
include "spritedata/sprites level3-08a/dolphinleft1.tgs.gen"
coloraddressdolphinleft1:
include "spritedata/sprites level3-08a/dolphinleft1.tcs.gen"
characteraddressdolphinleft2:
include "spritedata/sprites level3-08a/dolphinleft2.tgs.gen"
coloraddressdolphinleft2:
include "spritedata/sprites level3-08a/dolphinleft2.tcs.gen"

characteraddressdolphinright1:
include "spritedata/sprites level3-08a/dolphinright1.tgs.gen"
coloraddressdolphinright1:
include "spritedata/sprites level3-08a/dolphinright1.tcs.gen"
characteraddressdolphinright2:
include "spritedata/sprites level3-08a/dolphinright2.tgs.gen"
coloraddressdolphinright2:
include "spritedata/sprites level3-08a/dolphinright2.tcs.gen"

characteraddressdolphinup1:
include "spritedata/sprites level3-08a/dolphinup1.tgs.gen"
coloraddressdolphinup1:
include "spritedata/sprites level3-08a/dolphinup1.tcs.gen"
characteraddressdolphinup2:
include "spritedata/sprites level3-08a/dolphinup2.tgs.gen"
coloraddressdolphinup2:
include "spritedata/sprites level3-08a/dolphinup2.tcs.gen"

characteraddressdolphinleft11:
include "spritedata/sprites level3-08a/dolphinleft11.tgs.gen"
coloraddressdolphinleft11:
include "spritedata/sprites level3-08a/dolphinleft11.tcs.gen"
characteraddressdolphinleft21:
include "spritedata/sprites level3-08a/dolphinleft21.tgs.gen"
coloraddressdolphinleft21:
include "spritedata/sprites level3-08a/dolphinleft21.tcs.gen"

characteraddressdolphinright11:
include "spritedata/sprites level3-08a/dolphinright11.tgs.gen"
coloraddressdolphinright11:
include "spritedata/sprites level3-08a/dolphinright11.tcs.gen"
characteraddressdolphinright21:
include "spritedata/sprites level3-08a/dolphinright21.tgs.gen"
coloraddressdolphinright21:
include "spritedata/sprites level3-08a/dolphinright21.tcs.gen"

characteraddressdolphinup11:
include "spritedata/sprites level3-08a/dolphinup11.tgs.gen"
coloraddressdolphinup11:
include "spritedata/sprites level3-08a/dolphinup11.tcs.gen"
characteraddressdolphinup21:
include "spritedata/sprites level3-08a/dolphinup21.tgs.gen"
coloraddressdolphinup21:
include "spritedata/sprites level3-08a/dolphinup21.tcs.gen"


characteraddressdolphinleft12:
include "spritedata/sprites level3-08a/dolphinleft12.tgs.gen"
coloraddressdolphinleft12:
include "spritedata/sprites level3-08a/dolphinleft12.tcs.gen"
characteraddressdolphinleft22:
include "spritedata/sprites level3-08a/dolphinleft22.tgs.gen"
coloraddressdolphinleft22:
include "spritedata/sprites level3-08a/dolphinleft22.tcs.gen"

characteraddressdolphinright12:
include "spritedata/sprites level3-08a/dolphinright12.tgs.gen"
coloraddressdolphinright12:
include "spritedata/sprites level3-08a/dolphinright12.tcs.gen"
characteraddressdolphinright22:
include "spritedata/sprites level3-08a/dolphinright22.tgs.gen"
coloraddressdolphinright22:
include "spritedata/sprites level3-08a/dolphinright22.tcs.gen"

characteraddressdolphinup12:
include "spritedata/sprites level3-08a/dolphinup12.tgs.gen"
coloraddressdolphinup12:
include "spritedata/sprites level3-08a/dolphinup12.tcs.gen"
characteraddressdolphinup22:
include "spritedata/sprites level3-08a/dolphinup22.tgs.gen"
coloraddressdolphinup22:
include "spritedata/sprites level3-08a/dolphinup22.tcs.gen"


;deadfish
characteraddressdeadfishleft1:
include "spritedata/sprites level3-10a/deadfishleft1.tgs.gen"
coloraddressdeadfishleft1:
include "spritedata/sprites level3-10a/deadfishleft1.tcs.gen"
characteraddressdeadfishleft2:
include "spritedata/sprites level3-10a/deadfishleft2.tgs.gen"
coloraddressdeadfishleft2:
include "spritedata/sprites level3-10a/deadfishleft2.tcs.gen"
characteraddressdeadfishleft3:
include "spritedata/sprites level3-10a/deadfishleft3.tgs.gen"
coloraddressdeadfishleft3:
include "spritedata/sprites level3-10a/deadfishleft3.tcs.gen"
characteraddressdeadfishright1:
include "spritedata/sprites level3-10a/deadfishright1.tgs.gen"
coloraddressdeadfishright1:
include "spritedata/sprites level3-10a/deadfishright1.tcs.gen"
characteraddressdeadfishright2:
include "spritedata/sprites level3-10a/deadfishright2.tgs.gen"
coloraddressdeadfishright2:
include "spritedata/sprites level3-10a/deadfishright2.tcs.gen"
characteraddressdeadfishright3:
include "spritedata/sprites level3-10a/deadfishright3.tgs.gen"
coloraddressdeadfishright3:
include "spritedata/sprites level3-10a/deadfishright3.tcs.gen"



;falling spike
characteraddressspikedown:
include "spritedata/sprites level3-10a/spikedown.tgs.gen"
coloraddressspikedown:
include "spritedata/sprites level3-10a/spikedown.tcs.gen"
;characteraddressspikeup: ;not needed!
;include "spritedata/sprites level3-10a/spikeup.tgs.gen"
;coloraddressspikeup:
;include "spritedata/sprites level3-10a/spikeup.tcs.gen"



      ds                $c000-$,$ff
dephase

;
; block $70
;
underwaterblock:                    equ $70
phase   $8000
unwaterbgm:
  db 1 ;if 1 loop with intro
  incbin "../music/UNWATERI.BGM"
lenghtunwaterbgm: equ $ - unwaterbgm
  ds 1000 - lenghtunwaterbgm
  incbin "../music/UNWATERM.BGM"

;extra packed stuff
grpx39addr:	incbin "../grapx/world3/level3-02a/packedgrpx/grpx.pck" 
grpxblckl39: equ $70 ;vanilla 02a
  
;extra maps   
backgrl47:  incbin "../level_editor/_levels/maplevel3-02b.stg.pck" 
blockbackgrl47:  equ $70 ;vanilla 02b  
backgrl48:  incbin "../level_editor/_levels/maplevel3-03b.stg.pck" 
blockbackgrl48:  equ $70 ;vanilla 03b  
  
  
        ds                $a000-$,$ff
dephase

;
; block $71
;
bonusblock:                    equ $71
phase   $8000
bonusbgm:
  db 1 ;if 1 loop with intro
  incbin "../music/BONUSGAI.BGM"
lenghtbonusbgm: equ $ - bonusbgm
  ds 1000 - lenghtbonusbgm
  incbin "../music/BONUSGAM.BGM"
  
grpx40addr:	incbin "../grapx/world3/level3-03b/packedgrpx/grpx.pck" 
grpxblckl40: equ $71 ;vanilla 03b  
  
backgrl49:  incbin "../level_editor/_levels/maplevel3-03c.stg.pck" 
blockbackgrl49:  equ $71 ;vanilla 03c 
  
        ds                $a000-$,$ff
dephase


;
; block $72
;
engine32block:      equ $72
phase $d880

engine32start:

  include "engine32.asm"

lenghtengine32block: equ $ - engine32start
  dephase
  
  ds $2000 - lenghtengine32block


;
; block $73 - 74
;
cutsceneblock:                    equ $73
phase   $4000

  include "cutscenes/cutscene.asm"
        ds                $8000-$,$ff
dephase


;
; block $75
;
beginmusicblock:                    equ $75
phase   $8000
  db 0
  incbin "../music/BEGIN.BGM"

grpx41addr:	incbin "../grapx/world3/level3-04a/packedgrpx/grpx.pck" 
grpxblckl41: equ $75 ;vanilla 04a 
grpx42addr:	incbin "../grapx/world3/level3-04b/packedgrpx/grpx.pck" 
grpxblckl42: equ $75 ;vanilla 04b 
  
backgrl50:  incbin "../level_editor/_levels/maplevel3-04a.stg.pck" 
blockbackgrl50:  equ $75 ;vanilla 04a  
backgrl51:  incbin "../level_editor/_levels/maplevel3-04b.stg.pck" 
blockbackgrl51:  equ $75 ;vanilla 04b  
  
  
        ds                $a000-$,$ff
dephase

;
; block $76 - 77
;
cutscenedatablock:                    equ $76
phase   $8000

  include "cutscenes/cutscenedata.asm"
        ds                $c000-$,$ff
dephase


;
; block $78 - 79
;
romprogblock8:                equ $78
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement6.asm"


romprogblock8end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $7a - $7b
;
errorblock:                equ $7a
phase     $4000

  incbin "../grapx/MSX1/mariomsx12.sc2",$0000+7

  include "msx1.asm"
    include "version.asm"

      ds                $8000-$,$ff
dephase
















;
; block $7c - $7f
;
;
phase	$4000
world1and2and4tilesblock: equ $7c
world1and2and4tiles:  incbin "../worldmap_tileeditor/world1and2and4tiles.SC5",7
	ds		$c000-$,$ff
dephase

;
; block $80
;
;
phase	$4000
worldmapblockWorld2: equ $80
;world2map:              incbin "../worldmap_tileeditor/world2.MAR",0,$2000 -32
world2map:              incbin "../worldmap_tileeditor/world2.stg",0,$2000-32
world1and2and4palette:	incbin "../worldmap_tileeditor/world1and2and4tiles.SC5",$7680+7,32
	ds		$6000-$,$ff
dephase

;
; block $81
;
;
phase	$4000
worldmapblockWorld1: equ $81
world1map:              incbin "../worldmap_tileeditor/world1.MAR",0,$600
	ds		$6000-$,$ff
dephase

;
; block $82 - $83
;
;
phase	$4000
world3and7tilesblock: equ $82
world3and7tiles:  incbin "../worldmap_tileeditor/world3and7tiles.SC5",7,$3000
world5tilesblock: equ $82
world5tiles:  incbin "../worldmap_tileeditor/world5tiles.SC5",7,$1000
	ds		$8000-$,$ff
dephase

;
; block $84
;
;
phase	$4000
worldmapblockWorld3: equ $84
world3map:              incbin "../worldmap_tileeditor/world3.MAR",0
world3and7palette:      incbin "../worldmap_tileeditor/world3and7tiles.SC5",$7680+7,32
	ds		$6000-$,$ff
dephase

;
; block $85
;
;
phase	$4000
worldmapblockWorld7: equ $85
world7map:              incbin "../worldmap_tileeditor/world7.MAR",0,$600
	ds		$6000-$,$ff
dephase


;
; block $86 - $87
;
;
phase	$4000
worldmapblockWorld5: equ $86
;world5map:              incbin "../worldmap_tileeditor/world5.MAR",0
world5map:              incbin "../worldmap_tileeditor/world5.stg",0,32*23 * 2
world5palette:          incbin "../worldmap_tileeditor/world5tiles.SC5",$7680+7,32
starandspecialworldtilesblock: equ $86
starandspecialworldtiles:  incbin "../worldmap_tileeditor/starandspecialworldtiles.SC5",7,80*128
	ds		$8000-$,$ff
dephase

;
; block $88 - $89
;
;
phase	$8000
worldmapengineblock: equ $88
include "worldmap.asm"
	ds		$c000-$,$ff
dephase

;
; block $8a - $8b
;
;
phase	$4000
specialworldmapblock: equ $8a
specialworldmap:              incbin "../worldmap_tileeditor/specialworld.stg",0,32*23 * 2
worldmapinfobarblock: equ $8a
worldmapinfobar:              incbin "../grapx/worldmap/worldmapinfobar.SC5",7,71*128
	ds		$8000-$,$ff
dephase


worldmapmusic_YOISLANDblock:               equ $8c
phase   $8000
  db 0
  incbin "../music/YOISLAND.BGM"
  ds                $a000-$,$ff
dephase

worldmapmusic_WORLDMAPblock:               equ $8d
phase   $8000
  db 0
  incbin "../music/WORLDMAP.BGM"
  ds                $a000-$,$ff
dephase

worldmapmusic_BOWSERVAblock:               equ $8e
phase   $8000
  db 0
  incbin "../music/BOWSERVA.BGM"
  ds                $a000-$,$ff
dephase

worldmapmusic_VANILLAblock:               equ $8f
phase   $8000
  db 0
  incbin "../music/VANILLA.BGM"
  ds                $a000-$,$ff
dephase



;
; block $90
;
;
phase	$4000
worldmapspritesblock: equ $90
WM_sprite_mariobackstraight:
include "../grapx/worldmap/mariobackstraight.tgs.gen"
include "../grapx/worldmap/mariobackstraight.tcs.gen"
WM_sprite_mariobackleft:
include "../grapx/worldmap/mariobackleft.tgs.gen"
include "../grapx/worldmap/mariobackleft.tcs.gen"
WM_sprite_mariobackright:
include "../grapx/worldmap/mariobackright.tgs.gen"
include "../grapx/worldmap/mariobackright.tcs.gen"
WM_sprite_mariofrontstraight:
include "../grapx/worldmap/mariofrontstraight.tgs.gen"
include "../grapx/worldmap/mariofrontstraight.tcs.gen"
WM_sprite_mariofrontleft:
include "../grapx/worldmap/mariofrontleft.tgs.gen"
include "../grapx/worldmap/mariofrontleft.tcs.gen"
WM_sprite_mariofrontright:
include "../grapx/worldmap/mariofrontright.tgs.gen"
include "../grapx/worldmap/mariofrontright.tcs.gen"
WM_sprite_marioright1:
include "../grapx/worldmap/marioright1.tgs.gen"
include "../grapx/worldmap/marioright1.tcs.gen"
WM_sprite_marioright2:
include "../grapx/worldmap/marioright2.tgs.gen"
include "../grapx/worldmap/marioright2.tcs.gen"
WM_sprite_marioleft1:
include "../grapx/worldmap/marioleft1.tgs.gen"
include "../grapx/worldmap/marioleft1.tcs.gen"
WM_sprite_marioleft2:
include "../grapx/worldmap/marioleft2.tgs.gen"
include "../grapx/worldmap/marioleft2.tcs.gen"
WM_sprite_mariowaterleft1:
include "../grapx/worldmap/mariowaterleft1.tgs.gen"
include "../grapx/worldmap/mariowaterleft1.tcs.gen"
WM_sprite_mariowaterleft2:
include "../grapx/worldmap/mariowaterleft2.tgs.gen"
include "../grapx/worldmap/mariowaterleft2.tcs.gen"
WM_sprite_mariowaterright1:
include "../grapx/worldmap/mariowaterright1.tgs.gen"
include "../grapx/worldmap/mariowaterright1.tcs.gen"
WM_sprite_mariowaterright2:
include "../grapx/worldmap/mariowaterright2.tgs.gen"
include "../grapx/worldmap/mariowaterright2.tcs.gen"
WM_sprite_mariowaterfront1:
include "../grapx/worldmap/mariowaterfront1.tgs.gen"
include "../grapx/worldmap/mariowaterfront1.tcs.gen"
WM_sprite_mariowaterfront2:
include "../grapx/worldmap/mariowaterfront2.tgs.gen"
include "../grapx/worldmap/mariowaterfront2.tcs.gen"
WM_sprite_mariowaterback1:
include "../grapx/worldmap/mariowaterback1.tgs.gen"
include "../grapx/worldmap/mariowaterback1.tcs.gen"
WM_sprite_mariowaterback2:
include "../grapx/worldmap/mariowaterback2.tgs.gen"
include "../grapx/worldmap/mariowaterback2.tcs.gen"
WM_sprite_marioraisehand:
include "../grapx/worldmap/marioraisehand.tgs.gen"
include "../grapx/worldmap/marioraisehand.tcs.gen"
WM_sprite_marioraisehandinwater:
include "../grapx/worldmap/marioraisehandinwater.tgs.gen"
include "../grapx/worldmap/marioraisehandinwater.tcs.gen"
WM_sprite_marioclimbing1:
include "../grapx/worldmap/marioclimbing1.tgs.gen"
include "../grapx/worldmap/marioclimbing1.tcs.gen"
WM_sprite_marioclimbing2:
include "../grapx/worldmap/marioclimbing2.tgs.gen"
include "../grapx/worldmap/marioclimbing2.tcs.gen"

WM_sprite_bordermasksprite:
include "../grapx/worldmap/bordermasksprite.tgs.gen"
include "../grapx/worldmap/bordermasksprite.tcs.gen"
include "../grapx/worldmap/bordermaskspriteECbit.tcs.gen"

WM_sprite_marioandyoshileft1:
include "../grapx/worldmap/marioandyoshileft1.tgs.gen"
include "../grapx/worldmap/marioandyoshileft1.tcs.gen"
WM_sprite_marioandyoshileft2:
include "../grapx/worldmap/marioandyoshileft2.tgs.gen"
include "../grapx/worldmap/marioandyoshileft2.tcs.gen"
WM_sprite_marioandyoshiright1:
include "../grapx/worldmap/marioandyoshiright1.tgs.gen"
include "../grapx/worldmap/marioandyoshiright1.tcs.gen"
WM_sprite_marioandyoshiright2:
include "../grapx/worldmap/marioandyoshiright2.tgs.gen"
include "../grapx/worldmap/marioandyoshiright2.tcs.gen"
WM_sprite_marioandyoshifront1:
include "../grapx/worldmap/marioandyoshifront1.tgs.gen"
include "../grapx/worldmap/marioandyoshifront1.tcs.gen"
WM_sprite_marioandyoshifront2:
include "../grapx/worldmap/marioandyoshifront2.tgs.gen"
include "../grapx/worldmap/marioandyoshifront2.tcs.gen"
WM_sprite_marioandyoshiback1:
include "../grapx/worldmap/marioandyoshiback1.tgs.gen"
include "../grapx/worldmap/marioandyoshiback1.tcs.gen"
WM_sprite_marioandyoshiback2:
include "../grapx/worldmap/marioandyoshiback2.tgs.gen"
include "../grapx/worldmap/marioandyoshiback2.tcs.gen"

WM_sprite_marioandyoshileftwater1:
include "../grapx/worldmap/marioandyoshileftwater1.tgs.gen"
include "../grapx/worldmap/marioandyoshileftwater1.tcs.gen"
WM_sprite_marioandyoshileftwater2:
include "../grapx/worldmap/marioandyoshileftwater2.tgs.gen"
include "../grapx/worldmap/marioandyoshileftwater2.tcs.gen"
WM_sprite_marioandyoshirightwater1:
include "../grapx/worldmap/marioandyoshirightwater1.tgs.gen"
include "../grapx/worldmap/marioandyoshirightwater1.tcs.gen"
WM_sprite_marioandyoshirightwater2:
include "../grapx/worldmap/marioandyoshirightwater2.tgs.gen"
include "../grapx/worldmap/marioandyoshirightwater2.tcs.gen"
WM_sprite_marioandyoshifrontwater1:
include "../grapx/worldmap/marioandyoshifrontwater1.tgs.gen"
include "../grapx/worldmap/marioandyoshifrontwater1.tcs.gen"
WM_sprite_marioandyoshifrontwater2:
include "../grapx/worldmap/marioandyoshifrontwater2.tgs.gen"
include "../grapx/worldmap/marioandyoshifrontwater2.tcs.gen"
WM_sprite_marioandyoshibackwater1:
include "../grapx/worldmap/marioandyoshibackwater1.tgs.gen"
include "../grapx/worldmap/marioandyoshibackwater1.tcs.gen"
WM_sprite_marioandyoshibackwater2:
include "../grapx/worldmap/marioandyoshibackwater2.tgs.gen"
include "../grapx/worldmap/marioandyoshibackwater2.tcs.gen"

WM_sprite_marioandyoshiraisehand:
include "../grapx/worldmap/marioandyoshiraisehand.tgs.gen"
include "../grapx/worldmap/marioandyoshiraisehand.tcs.gen"
WM_sprite_marioandyoshiraisehandwater:
include "../grapx/worldmap/marioandyoshiraisehandwater.tgs.gen"
include "../grapx/worldmap/marioandyoshiraisehandwater.tcs.gen"

WM_sprite_ghostleft:
include "../grapx/worldmap/ghostleft.tgs.gen"
include "../grapx/worldmap/ghostleft.tcs.gen"
WM_sprite_ghostright:
include "../grapx/worldmap/ghostright.tgs.gen"
include "../grapx/worldmap/ghostright.tcs.gen"

WM_sprite_fish1:
include "../grapx/worldmap/fish1.tgs.gen"
include "../grapx/worldmap/fish1.tcs.gen"
WM_sprite_fish2:
include "../grapx/worldmap/fish2.tgs.gen"
include "../grapx/worldmap/fish2.tcs.gen"
WM_sprite_fish3:
include "../grapx/worldmap/fish3.tgs.gen"
include "../grapx/worldmap/fish3.tcs.gen"
WM_sprite_fish4:
include "../grapx/worldmap/fish4.tgs.gen"
include "../grapx/worldmap/fish4.tcs.gen"

WM_sprite_fishsplash1:
include "../grapx/worldmap/fishsplash1.tgs.gen"
include "../grapx/worldmap/white sprite.tcs.gen"
WM_sprite_fishsplash2:
include "../grapx/worldmap/fishsplash2.tgs.gen"
include "../grapx/worldmap/white sprite.tcs.gen"
WM_sprite_fishsplash3:
include "../grapx/worldmap/fishsplash3.tgs.gen"
include "../grapx/worldmap/white sprite.tcs.gen"

WM_sprite_cloud16wide:
include "../grapx/worldmap/cloud16wide.log"
include "../grapx/worldmap/white sprite.tcs.gen"
WM_sprite_cloud26wide:
include "../grapx/worldmap/cloudleft2c.log"
include "../grapx/worldmap/cloudright2c.log"
include "../grapx/worldmap/white sprite.tcs.gen"
include "../grapx/worldmap/white sprite.tcs.gen"

WM_sprite_star0:
include "../grapx/worldmap/star0.tgs.gen"
include "../grapx/worldmap/star0.tcs.gen"
WM_sprite_star1:
include "../grapx/worldmap/star1.tgs.gen"
include "../grapx/worldmap/star1.tcs.gen"
WM_sprite_star2:
include "../grapx/worldmap/star2.tgs.gen"
include "../grapx/worldmap/star2.tcs.gen"
WM_sprite_star3:
include "../grapx/worldmap/star3.tgs.gen"
include "../grapx/worldmap/star3.tcs.gen"
WM_sprite_star4:
include "../grapx/worldmap/star4.tgs.gen"
include "../grapx/worldmap/star4.tcs.gen"
WM_sprite_star5:
include "../grapx/worldmap/star5.tgs.gen"
include "../grapx/worldmap/star5.tcs.gen"
WM_sprite_star6:
include "../grapx/worldmap/star6.tgs.gen"
include "../grapx/worldmap/star6.tcs.gen"
WM_sprite_star7:
include "../grapx/worldmap/star7.tgs.gen"
include "../grapx/worldmap/star7.tcs.gen"
WM_sprite_star8:
include "../grapx/worldmap/star8.tgs.gen"
include "../grapx/worldmap/star8.tcs.gen"
WM_sprite_star9:
include "../grapx/worldmap/star9.tgs.gen"
include "../grapx/worldmap/star9.tcs.gen"


	ds		$6000-$,$ff
dephase


;
; block $91
;
invinsib:                    equ $91
phase   $8000
  db 0
  incbin "../music/INVINCIB.BGM"
    
  
        ds                $a000-$,$ff
dephase



;
; block $92 - 93
;
romprogblock9:                equ $92
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement7.asm"


romprogblock9end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $94 - 97
;
ingamespritesblock5:                equ $94
phase     $4000


;DryBonesLefttrow
characteraddressDryBoneslefttrow:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLefttrow.tgs.gen"
coloraddressDryBoneslefttrow:
include "spritedata/sprites level2-011a mortons-castle/DryBonesLefttrow.tcs.gen"
characteraddressDryBonesrighttrow:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRighttrow.tgs.gen"
coloraddressDryBonesrighttrow:
include "spritedata/sprites level2-011a mortons-castle/DryBonesRighttrow.tcs.gen"
;bone
characteraddressbone11:
include "spritedata/sprites level2-011a mortons-castle/bone11.tgs.gen"
coloraddressbone11:
include "spritedata/sprites level2-011a mortons-castle/bone11.tcs.gen"
characteraddressbone12:
include "spritedata/sprites level2-011a mortons-castle/bone12.tgs.gen"
coloraddressbone12:
include "spritedata/sprites level2-011a mortons-castle/bone12.tcs.gen"
characteraddressbone2:
include "spritedata/sprites level2-011a mortons-castle/bone2.tgs.gen"
coloraddressbone2:
include "spritedata/sprites level2-011a mortons-castle/bone2.tcs.gen"
characteraddressbone3:
include "spritedata/sprites level2-011a mortons-castle/bone3.tgs.gen"
coloraddressbone3:
include "spritedata/sprites level2-011a mortons-castle/bone3.tcs.gen"
characteraddressbone4:
include "spritedata/sprites level2-011a mortons-castle/bone4.tgs.gen"
coloraddressbone4:
include "spritedata/sprites level2-011a mortons-castle/bone4.tcs.gen"
characteraddressbone5:
include "spritedata/sprites level2-011a mortons-castle/bone5.tgs.gen"
coloraddressbone5:
include "spritedata/sprites level2-011a mortons-castle/bone5.tcs.gen"


;bonybeetle
characteraddressbonybeetleleft1:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft1.tgs.gen"
coloraddressbonybeetleleft1:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft1.tcs.gen"
characteraddressbonybeetleleft2:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft2.tgs.gen"
coloraddressbonybeetleleft2:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft2.tcs.gen"
characteraddressbonybeetleleft3:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft3.tgs.gen"
coloraddressbonybeetleleft3:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft3.tcs.gen"
characteraddressbonybeetleleft4:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft4.tgs.gen"
coloraddressbonybeetleleft4:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft4.tcs.gen"
characteraddressbonybeetleleft5:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft5.tgs.gen"
coloraddressbonybeetleleft5:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleleft5.tcs.gen"

characteraddressbonybeetleright1:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright1.tgs.gen"
coloraddressbonybeetleright1:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright1.tcs.gen"
characteraddressbonybeetleright2:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright2.tgs.gen"
coloraddressbonybeetleright2:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright2.tcs.gen"
characteraddressbonybeetleright3:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright3.tgs.gen"
coloraddressbonybeetleright3:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright3.tcs.gen"
characteraddressbonybeetleright4:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright4.tgs.gen"
coloraddressbonybeetleright4:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright4.tcs.gen"
characteraddressbonybeetleright5:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright5.tgs.gen"
coloraddressbonybeetleright5:
include "spritedata/sprites level2-011a mortons-castle/bonybeetleright5.tcs.gen"

;msx
characteraddressmsx:
include "spritedata/sprites level2-011a mortons-castle/msx.tgs.gen"
coloraddressmsx:
include "spritedata/sprites level2-011a mortons-castle/msx.tcs.gen"


;reznor
characteraddressreznorleft1:
include "spritedata/sprites level3-10c/reznorleft1.tgs.gen"
coloraddressreznorleft1:
include "spritedata/sprites level3-10c/reznorleft1.tcs.gen"
characteraddressreznorleft2:
include "spritedata/sprites level3-10c/reznorleft2.tgs.gen"
coloraddressreznorleft2:
include "spritedata/sprites level3-10c/reznorleft2.tcs.gen"
characteraddressreznorright1:
include "spritedata/sprites level3-10c/reznorright1.tgs.gen"
coloraddressreznorright1:
include "spritedata/sprites level3-10c/reznorright1.tcs.gen"
characteraddressreznorright2:
include "spritedata/sprites level3-10c/reznorright2.tgs.gen"
coloraddressreznorright2:
include "spritedata/sprites level3-10c/reznorright2.tcs.gen"
characteraddressreznorturn:
include "spritedata/sprites level3-10c/reznorturn.tgs.gen"
coloraddressreznorturn:
include "spritedata/sprites level3-10c/reznorturn.tcs.gen"
characteraddressreznorfireball1:
include "spritedata/sprites level3-10c/reznorfireball1.tgs.gen"
coloraddressreznorfireball1:
include "spritedata/sprites level3-10c/reznorfireball1.tcs.gen"
characteraddressreznorfireball2:
include "spritedata/sprites level3-10c/reznorfireball2.tgs.gen"
coloraddressreznorfireball2:
include "spritedata/sprites level3-10c/reznorfireball2.tcs.gen"
characteraddressreznorfireball3:
include "spritedata/sprites level3-10c/reznorfireball3.tgs.gen"
coloraddressreznorfireball3:
include "spritedata/sprites level3-10c/reznorfireball3.tcs.gen"
characteraddressreznorfireball4:
include "spritedata/sprites level3-10c/reznorfireball4.tgs.gen"
coloraddressreznorfireball4:
include "spritedata/sprites level3-10c/reznorfireball4.tcs.gen"
characteraddressreznorplatform:
include "spritedata/sprites level3-10c/reznorplatform.tgs.gen"
coloraddressreznorplatform:
include "spritedata/sprites level3-10c/reznorplatform.tcs.gen"

;magikoopa
;spawn left
characteraddressmagikoopasleft1:
include "spritedata/sprites level3-11a/magikoopasleft1.tgs.gen"
coloraddressmagikoopasleft1:
include "spritedata/sprites level3-11a/magikoopasleft1.tcs.gen"
characteraddressmagikoopasleft2:
include "spritedata/sprites level3-11a/magikoopasleft2.tgs.gen"
coloraddressmagikoopasleft2:
include "spritedata/sprites level3-11a/magikoopasleft2.tcs.gen"
characteraddressmagikoopasleft3:
include "spritedata/sprites level3-11a/magikoopasleft3.tgs.gen"
coloraddressmagikoopasleft3:
include "spritedata/sprites level3-11a/magikoopasleft3.tcs.gen"
characteraddressmagikoopasleft4:
include "spritedata/sprites level3-11a/magikoopasleft4.tgs.gen"
coloraddressmagikoopasleft4:
include "spritedata/sprites level3-11a/magikoopasleft4.tcs.gen"
;spawn right
characteraddressmagikoopasright1:
include "spritedata/sprites level3-11a/magikoopasright1.tgs.gen"
coloraddressmagikoopasright1:
include "spritedata/sprites level3-11a/magikoopasright1.tcs.gen"
characteraddressmagikoopasright2:
include "spritedata/sprites level3-11a/magikoopasright2.tgs.gen"
coloraddressmagikoopasright2:
include "spritedata/sprites level3-11a/magikoopasright2.tcs.gen"
characteraddressmagikoopasright3:
include "spritedata/sprites level3-11a/magikoopasright3.tgs.gen"
coloraddressmagikoopasright3:
include "spritedata/sprites level3-11a/magikoopasright3.tcs.gen"
characteraddressmagikoopasright4:
include "spritedata/sprites level3-11a/magikoopasright4.tgs.gen"
coloraddressmagikoopasright4:
include "spritedata/sprites level3-11a/magikoopasright4.tcs.gen"
;left
characteraddressmagikoopaleft1:
include "spritedata/sprites level3-11a/magikoopaleft1.tgs.gen"
coloraddressmagikoopaleft1:
include "spritedata/sprites level3-11a/magikoopaleft1.tcs.gen"
characteraddressmagikoopaleft2:
include "spritedata/sprites level3-11a/magikoopaleft2.tgs.gen"
coloraddressmagikoopaleft2:
include "spritedata/sprites level3-11a/magikoopaleft2.tcs.gen"
characteraddressmagikoopaleft3:
include "spritedata/sprites level3-11a/magikoopaleft3.tgs.gen"
coloraddressmagikoopaleft3:
include "spritedata/sprites level3-11a/magikoopaleft3.tcs.gen"
characteraddressmagikoopaleft4:
include "spritedata/sprites level3-11a/magikoopaleft4.tgs.gen"
coloraddressmagikoopaleft4:
include "spritedata/sprites level3-11a/magikoopaleft4.tcs.gen"
;right
characteraddressmagikooparight1:
include "spritedata/sprites level3-11a/magikooparight1.tgs.gen"
coloraddressmagikooparight1:
include "spritedata/sprites level3-11a/magikooparight1.tcs.gen"
characteraddressmagikooparight2:
include "spritedata/sprites level3-11a/magikooparight2.tgs.gen"
coloraddressmagikooparight2:
include "spritedata/sprites level3-11a/magikooparight2.tcs.gen"
characteraddressmagikooparight3:
include "spritedata/sprites level3-11a/magikooparight3.tgs.gen"
coloraddressmagikooparight3:
include "spritedata/sprites level3-11a/magikooparight3.tcs.gen"
characteraddressmagikooparight4:
include "spritedata/sprites level3-11a/magikooparight4.tgs.gen"
coloraddressmagikooparight4:
include "spritedata/sprites level3-11a/magikooparight4.tcs.gen"

;the magic ball
characteraddressmagiball1:
include "spritedata/sprites level3-11a/magiball1.tgs.gen"
coloraddressmagiball1:
include "spritedata/sprites level3-11a/magiball1.tcs.gen"
characteraddressmagiball2:
include "spritedata/sprites level3-11a/magiball2.tgs.gen"
coloraddressmagiball2:
include "spritedata/sprites level3-11a/magiball2.tcs.gen"
characteraddressmagiball3:
include "spritedata/sprites level3-11a/magiball3.tgs.gen"
coloraddressmagiball3:
include "spritedata/sprites level3-11a/magiball3.tcs.gen"
characteraddressmagiball4:
include "spritedata/sprites level3-11a/magiball4.tgs.gen"
characteraddressmagiball5:
include "spritedata/sprites level3-11a/magiball5.tgs.gen"
characteraddressmagiball6:
include "spritedata/sprites level3-11a/magiball6.tgs.gen"

characteraddressplatformbr:
include "spritedata/sprites level3-11a/platformbr.tgs.gen"
coloraddressplatformbr:
include "spritedata/sprites level3-11a/platformbr.tcs.gen"

;special helper sprite for animation block :P
characteraddressblocksprite:
include "spritedata/sprites level3-11c/blocksprite.tgs.gen"
coloraddressblocksprite:
include "spritedata/sprites level3-11c/blocksprite.tcs.gen"

;moving flame. the one that bounces around
characteraddressmovingflame1upleft:
include "spritedata/sprites level3-11d/movingflame1upleft.tgs.gen"
coloraddressmovingflame1upleft:
include "spritedata/sprites level3-11d/movingflame1upleft.tcs.gen"
characteraddressmovingflame1downleft:
include "spritedata/sprites level3-11d/movingflame1downleft.tgs.gen"
coloraddressmovingflame1downleft:
include "spritedata/sprites level3-11d/movingflame1downleft.tcs.gen"
characteraddressmovingflame1upright:
include "spritedata/sprites level3-11d/movingflame1upright.tgs.gen"
coloraddressmovingflame1upright:
include "spritedata/sprites level3-11d/movingflame1upright.tcs.gen"
characteraddressmovingflame1downright:
include "spritedata/sprites level3-11d/movingflame1downright.tgs.gen"
coloraddressmovingflame1downright:
include "spritedata/sprites level3-11d/movingflame1downright.tcs.gen"

characteraddressmovingflame2upleft:
include "spritedata/sprites level3-11d/movingflame2upleft.tgs.gen"
coloraddressmovingflame2upleft:
include "spritedata/sprites level3-11d/movingflame2upleft.tcs.gen"
characteraddressmovingflame2downleft:
include "spritedata/sprites level3-11d/movingflame2downleft.tgs.gen"
coloraddressmovingflame2downleft:
include "spritedata/sprites level3-11d/movingflame2downleft.tcs.gen"
characteraddressmovingflame2upright:
include "spritedata/sprites level3-11d/movingflame2upright.tgs.gen"
coloraddressmovingflame2upright:
include "spritedata/sprites level3-11d/movingflame2upright.tcs.gen"
characteraddressmovingflame2downright:
include "spritedata/sprites level3-11d/movingflame2downright.tgs.gen"
coloraddressmovingflame2downright:
include "spritedata/sprites level3-11d/movingflame2downright.tcs.gen"


;lemmy
characteraddresslemmyfront1:
include "spritedata/sprites level3-11d/lemmyfront1.tgs.gen"
coloraddresslemmyfront1:
include "spritedata/sprites level3-11d/lemmyfront1.tcs.gen"
characteraddresslemmyfront2:
include "spritedata/sprites level3-11d/lemmyfront2.tgs.gen"
coloraddresslemmyfront2:
include "spritedata/sprites level3-11d/lemmyfront2.tcs.gen"

characteraddresslemmypissed1:
include "spritedata/sprites level3-11d/lemmypissed1.tgs.gen"
coloraddresslemmypissed1:
include "spritedata/sprites level3-11d/lemmypissed1.tcs.gen"
characteraddresslemmypissed2:
include "spritedata/sprites level3-11d/lemmypissed2.tgs.gen"
coloraddresslemmypissed2:
include "spritedata/sprites level3-11d/lemmypissed2.tcs.gen"

characteraddresslemmyleft1:
include "spritedata/sprites level3-11d/lemmyleft1.tgs.gen"
coloraddresslemmyleft1:
include "spritedata/sprites level3-11d/lemmyleft1.tcs.gen"
characteraddresslemmyleft2:
include "spritedata/sprites level3-11d/lemmyleft2.tgs.gen"
coloraddresslemmyleft2:
include "spritedata/sprites level3-11d/lemmyleft2.tcs.gen"

characteraddresslemmyright1:
include "spritedata/sprites level3-11d/lemmyright1.tgs.gen"
coloraddresslemmyright1:
include "spritedata/sprites level3-11d/lemmyright1.tcs.gen"
characteraddresslemmyright2:
include "spritedata/sprites level3-11d/lemmyright2.tgs.gen"
coloraddresslemmyright2:
include "spritedata/sprites level3-11d/lemmyright2.tcs.gen"

characteraddresslemmyfront3:
include "spritedata/sprites level3-11d/lemmyfront3.tgs.gen"
coloraddresslemmyfront3:
include "spritedata/sprites level3-11d/lemmyfront3.tcs.gen"
characteraddresslemmyfront4:
include "spritedata/sprites level3-11d/lemmyfront4.tgs.gen"
coloraddresslemmyfront4:
include "spritedata/sprites level3-11d/lemmyfront4.tcs.gen"


characteraddresslemmydoll1:
include "spritedata/sprites level3-11d/lemmydoll1.tgs.gen"
coloraddresslemmydoll1:
include "spritedata/sprites level3-11d/lemmydoll1.tcs.gen"
characteraddresslemmydoll2:
include "spritedata/sprites level3-11d/lemmydoll2.tgs.gen"
coloraddresslemmydoll2:
include "spritedata/sprites level3-11d/lemmydoll2.tcs.gen"
characteraddresslemmydoll3:
include "spritedata/sprites level3-11d/lemmydoll3.tgs.gen"
coloraddresslemmydoll3:
include "spritedata/sprites level3-11d/lemmydoll3.tcs.gen"

characteraddresslemmydead:
include "spritedata/sprites level3-11d/lemmydead.tgs.gen"
coloraddresslemmydead:
include "spritedata/sprites level3-11d/lemmydead.tcs.gen"


emptysprite94:
include "spritedata/ingame sprites/empty.tgs.gen"


;cutscene objects
characteraddressmariohammer1:
include "cutscenes/hammer1.tgs.gen"
coloraddressmariohammer1:
include "cutscenes/hammer1.tcs.gen"
characteraddressmariohammer2:
include "cutscenes/hammer2.tgs.gen"
coloraddressmariohammer2:
include "cutscenes/hammer2.tcs.gen"
characteraddressmariohammer3:
include "cutscenes/hammer3.tgs.gen"
coloraddressmariohammer3:
include "cutscenes/hammer3.tcs.gen"
characteraddressmariohammer4:
include "cutscenes/hammer4.tgs.gen"
coloraddressmariohammer4:
include "cutscenes/hammer4.tcs.gen"
characteraddressmariohammer5:
include "cutscenes/hammer5.tgs.gen"
coloraddressmariohammer5:
include "cutscenes/hammer5.tcs.gen"
characteraddressmariohammer6:
include "cutscenes/hammer6.tgs.gen"
coloraddressmariohammer6:
include "cutscenes/hammer6.tcs.gen"
characteraddressmariohammer7:
include "cutscenes/hammer7.tgs.gen"
coloraddressmariohammer7:
include "cutscenes/hammer7.tcs.gen"

characteraddressblack:
include "cutscenes/black.tgs.gen"
coloraddressblack:
include "cutscenes/black.tcs.gen"



characteraddressridecloud:
include "spritedata/ingame sprites/ridecloud.tgs.gen"
coloraddressridecloud:
include "spritedata/ingame sprites/ridecloud.tcs.gen"
characteraddressridecloud2:
include "spritedata/ingame sprites/ridecloud2.tgs.gen"
coloraddressridecloud2:
include "spritedata/ingame sprites/ridecloud2.tcs.gen"
characteraddressridecloud3:
include "spritedata/ingame sprites/ridecloud3.tgs.gen"
coloraddressridecloud3:
include "spritedata/ingame sprites/ridecloud3.tcs.gen"
characteraddressridecloud4:
include "spritedata/ingame sprites/ridecloud4.tgs.gen"
coloraddressridecloud4:
include "spritedata/ingame sprites/ridecloud4.tcs.gen"

characteraddressplatformshort:
include "spritedata/ingame sprites/platformshort.tgs.gen"
coloraddressplatformshort:
include "spritedata/ingame sprites/platformshort.tcs.gen"

characteraddressflywings1:
include "spritedata/ingame sprites/flywings1.tgs.gen"
coloraddressflywings1:
include "spritedata/ingame sprites/flywings1.tcs.gen"
characteraddressflywings2:
include "spritedata/ingame sprites/flywings2.tgs.gen"
coloraddressflywings2:
include "spritedata/ingame sprites/flywings2.tcs.gen"

;sumokoopa
characteraddresssumokoopaleft1:
include "spritedata/sprites level4-04a/sumokoopaleft1.tgs.gen"
coloraddresssumokoopaleft1:
include "spritedata/sprites level4-04a/sumokoopaleft1.tcs.gen"
characteraddresssumokoopaleft2:
include "spritedata/sprites level4-04a/sumokoopaleft2.tgs.gen"
coloraddresssumokoopaleft2:
include "spritedata/sprites level4-04a/sumokoopaleft2.tcs.gen"
characteraddresssumokoopaleft3:
include "spritedata/sprites level4-04a/sumokoopaleft3.tgs.gen"
coloraddresssumokoopaleft3:
include "spritedata/sprites level4-04a/sumokoopaleft3.tcs.gen"
characteraddresssumokoopaleft4:
include "spritedata/sprites level4-04a/sumokoopaleft4.tgs.gen"
coloraddresssumokoopaleft4:
include "spritedata/sprites level4-04a/sumokoopaleft4.tcs.gen"
characteraddresssumokoopaleft5:
include "spritedata/sprites level4-04a/sumokoopaleft5.tgs.gen"
coloraddresssumokoopaleft5:
include "spritedata/sprites level4-04a/sumokoopaleft5.tcs.gen"

characteraddresssumokooparight1:
include "spritedata/sprites level4-04a/sumokooparight1.tgs.gen"
coloraddresssumokooparight1:
include "spritedata/sprites level4-04a/sumokooparight1.tcs.gen"
characteraddresssumokooparight2:
include "spritedata/sprites level4-04a/sumokooparight2.tgs.gen"
coloraddresssumokooparight2:
include "spritedata/sprites level4-04a/sumokooparight2.tcs.gen"
characteraddresssumokooparight3:
include "spritedata/sprites level4-04a/sumokooparight3.tgs.gen"
coloraddresssumokooparight3:
include "spritedata/sprites level4-04a/sumokooparight3.tcs.gen"
characteraddresssumokooparight4:
include "spritedata/sprites level4-04a/sumokooparight4.tgs.gen"
coloraddresssumokooparight4:
include "spritedata/sprites level4-04a/sumokooparight4.tcs.gen"
characteraddresssumokooparight5:
include "spritedata/sprites level4-04a/sumokooparight5.tgs.gen"
coloraddresssumokooparight5:
include "spritedata/sprites level4-04a/sumokooparight5.tcs.gen"

characteraddressshard:
include "spritedata/sprites level4-04a/shard.tgs.gen"
coloraddressshard:
include "spritedata/sprites level4-04a/shard.tcs.gen"
characteraddressshard2:
include "spritedata/sprites level4-04a/shard2.tgs.gen"
coloraddressshard2:
include "spritedata/sprites level4-04a/shard2.tcs.gen"

characteraddresslongflame1:
include "spritedata/sprites level4-04a/longflame1.tgs.gen"
coloraddresslongflame1:
include "spritedata/sprites level4-04a/longflame1.tcs.gen"
characteraddresslongflame2:
include "spritedata/sprites level4-04a/longflame2.tgs.gen"
coloraddresslongflame2:
include "spritedata/sprites level4-04a/longflame2.tcs.gen"
characteraddresslongflame3:
include "spritedata/sprites level4-04a/longflame3.tgs.gen"
coloraddresslongflame3:
include "spritedata/sprites level4-04a/longflame3.tcs.gen"
characteraddresslongflame4:
include "spritedata/sprites level4-04a/longflame4.tgs.gen"
coloraddresslongflame4:
include "spritedata/sprites level4-04a/longflame4.tcs.gen"
characteraddresslongflame5:
include "spritedata/sprites level4-04a/longflame5.tgs.gen"
coloraddresslongflame5:
include "spritedata/sprites level4-04a/longflame5.tcs.gen"
characteraddresslongflame6:
include "spritedata/sprites level4-04a/longflame6.tgs.gen"
coloraddresslongflame6:
include "spritedata/sprites level4-04a/longflame6.tcs.gen"
characteraddresslongflame7:
include "spritedata/sprites level4-04a/longflame7.tgs.gen"
coloraddresslongflame7:
include "spritedata/sprites level4-04a/longflame7.tcs.gen"
characteraddresslongflame8:
include "spritedata/sprites level4-04a/longflame8.tgs.gen"
coloraddresslongflame8:
include "spritedata/sprites level4-04a/longflame8.tcs.gen"

;torpedoted
characteraddresstorpedotedl1:
include "spritedata/ingame sprites/torpedotedl1.tgs.gen"
coloraddresstorpedotedl1:
include "spritedata/ingame sprites/torpedotedl1.tcs.gen"
characteraddresstorpedotedl2:
include "spritedata/ingame sprites/torpedotedl2.tgs.gen"
coloraddresstorpedotedl2:
include "spritedata/ingame sprites/torpedotedl2.tcs.gen"
characteraddresstorpedotedl3:
include "spritedata/ingame sprites/torpedotedl3.tgs.gen"
coloraddresstorpedotedl3:
include "spritedata/ingame sprites/torpedotedl3.tcs.gen"

characteraddresstorpedotedr1:
include "spritedata/ingame sprites/torpedotedr1.tgs.gen"
coloraddresstorpedotedr1:
include "spritedata/ingame sprites/torpedotedr1.tcs.gen"
characteraddresstorpedotedr2:
include "spritedata/ingame sprites/torpedotedr2.tgs.gen"
coloraddresstorpedotedr2:
include "spritedata/ingame sprites/torpedotedr2.tcs.gen"
characteraddresstorpedotedr3:
include "spritedata/ingame sprites/torpedotedr3.tgs.gen"
coloraddresstorpedotedr3:
include "spritedata/ingame sprites/torpedotedr3.tcs.gen"

characteraddresstorpedotedhandl1:
include "spritedata/ingame sprites/torpedotedhandl1.tgs.gen"
coloraddresstorpedotedhandl1:
include "spritedata/ingame sprites/torpedotedhandl1.tcs.gen"
characteraddresstorpedotedhandl2:
include "spritedata/ingame sprites/torpedotedhandl2.tgs.gen"
coloraddresstorpedotedhandl2:
include "spritedata/ingame sprites/torpedotedhandl2.tcs.gen"

characteraddresstorpedotedhandr1:
include "spritedata/ingame sprites/torpedotedhandr1.tgs.gen"
coloraddresstorpedotedhandr1:
include "spritedata/ingame sprites/torpedotedhandr1.tcs.gen"
characteraddresstorpedotedhandr2:
include "spritedata/ingame sprites/torpedotedhandr2.tgs.gen"
coloraddresstorpedotedhandr2:
include "spritedata/ingame sprites/torpedotedhandr2.tcs.gen"


  
  
      ds                $c000-$,$ff
dephase




;
; block $98 - 99
;
romprogblock10:                equ $98
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement8.asm"


romprogblock10end:
  
  
      ds                $c000-$,$ff
dephase




;
; block $9A - 9B
;
cutscenedatablock2:                    equ $9a
phase   $8000

  include "cutscenes/cutscenedata2.asm"
        ds                $c000-$,$ff
dephase



;extra mapdata
;
; block $9C - 9D
;
phase   $8000


grpx80addr:	incbin "../grapx/world4/level4-04a/packedgrpx/grpx.pck" 
grpxblckl80: equ $9c ;cookiemountain 4a
grpx81addr:	incbin "../grapx/world4/level4-04b/packedgrpx/grpx.pck" 
grpxblckl81: equ $9c ;cookiemountain 4b
grpx82addr:	incbin "../grapx/world4/level4-05a/packedgrpx/grpx.pck" 
grpxblckl82: equ $9c ;sodalake 5a
grpx84addr:	incbin "../grapx/world4/level4-06a/packedgrpx/grpx.pck" 
grpxblckl84: equ $9c ;ludwig's castle 6abc
grpx85addr:	incbin "../grapx/world4/level4-06d/packedgrpx/grpx.pck" 
grpxblckl85: equ $9c ;ludwig's castle 6d
; grpx86addr:	incbin "../grapx/world5/level5-01a/packedgrpx/grpx.pck" | grpxblckl86: equ $9c ;illusion1


backgrl80:  incbin "../level_editor/_levels/maplevel4-04a.stg.pck" 
blockbackgrl80:  equ $9c ;cookiemountain 4a 
backgrl81:  incbin "../level_editor/_levels/maplevel4-04b.stg.pck" 
blockbackgrl81:  equ $9c ;cookiemountain 4b 
backgrl82:  incbin "../level_editor/_levels/maplevel4-05a.stg.pck" 
blockbackgrl82:  equ $9c ;sodalake 5a 
backgrl83:  incbin "../level_editor/_levels/maplevel4-05b.stg.pck" 
blockbackgrl83:  equ $9c ;sodalake 5b 
backgrl84:  incbin "../level_editor/_levels/maplevel4-06a.stg.pck" 
blockbackgrl84:  equ $9c ;ludwig's castle 6a 
backgrl85:  incbin "../level_editor/_levels/maplevel4-06b.stg.pck" 
blockbackgrl85:  equ $9c ;ludwig's castle 6b 
backgrl86:  incbin "../level_editor/_levels/maplevel4-06c.stg.pck" 
blockbackgrl86:  equ $9c ;ludwig's castle 6c 
backgrl87:  incbin "../level_editor/_levels/maplevel4-06d.stg.pck" 
blockbackgrl87:  equ $9c ;ludwig's castle 6d 
; backgrl88:  incbin "../level_editor/_levels/maplevel5-01.stg.pck" | blockbackgrl88:  equ $9c ;illusion1 


        ds                $c000-$,$ff
dephase



;
; block $9E - 9F
;
romprogblock11:                equ $9E
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement9.asm"


romprogblock11end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $A0 - A3
;
ingamespritesblock6:                equ $A0
phase     $4000




;ludwig boss
characteraddressludwigleft1:
include "spritedata/sprites level4-04d/ludwigleft1.tgs.gen"
coloraddressludwigleft1:
include "spritedata/sprites level4-04d/ludwigleft1.tcs.gen"
characteraddressludwigleft2:
include "spritedata/sprites level4-04d/ludwigleft2.tgs.gen"
coloraddressludwigleft2:
include "spritedata/sprites level4-04d/ludwigleft2.tcs.gen"


characteraddressludwigleftattack1:
include "spritedata/sprites level4-04d/ludwigleftattack1.tgs.gen"
coloraddressludwigleftattack1:
include "spritedata/sprites level4-04d/ludwigleftattack1.tcs.gen"
characteraddressludwigleftattack2:
include "spritedata/sprites level4-04d/ludwigleftattack2.tgs.gen"
coloraddressludwigleftattack2:
include "spritedata/sprites level4-04d/ludwigleftattack2.tcs.gen"
characteraddressludwigleftattack3:
include "spritedata/sprites level4-04d/ludwigleftattack3.tgs.gen"
coloraddressludwigleftattack3:
include "spritedata/sprites level4-04d/ludwigleftattack3.tcs.gen"


characteraddressludwigturnleft:
include "spritedata/sprites level4-04d/ludwigturnleft.tgs.gen"
coloraddressludwigturnleft:
include "spritedata/sprites level4-04d/ludwigturnleft.tcs.gen"

;right
characteraddressludwigright1:
include "spritedata/sprites level4-04d/ludwigright1.tgs.gen"
coloraddressludwigright1:
include "spritedata/sprites level4-04d/ludwigright1.tcs.gen"
characteraddressludwigright2:
include "spritedata/sprites level4-04d/ludwigright2.tgs.gen"
coloraddressludwigright2:
include "spritedata/sprites level4-04d/ludwigright2.tcs.gen"


characteraddressludwigrightattack1:
include "spritedata/sprites level4-04d/ludwigrightattack1.tgs.gen"
coloraddressludwigrightattack1:
include "spritedata/sprites level4-04d/ludwigrightattack1.tcs.gen"
characteraddressludwigrightattack2:
include "spritedata/sprites level4-04d/ludwigrightattack2.tgs.gen"
coloraddressludwigrightattack2:
include "spritedata/sprites level4-04d/ludwigrightattack2.tcs.gen"
characteraddressludwigrightattack3:
include "spritedata/sprites level4-04d/ludwigrightattack3.tgs.gen"
coloraddressludwigrightattack3:
include "spritedata/sprites level4-04d/ludwigrightattack3.tcs.gen"


characteraddressludwigturnright:
include "spritedata/sprites level4-04d/ludwigturnright.tgs.gen"
coloraddressludwigturnright:
include "spritedata/sprites level4-04d/ludwigturnright.tcs.gen"


;jumping
characteraddressludwigjump1:
include "spritedata/sprites level4-04d/ludwigjump1.tgs.gen"
coloraddressludwigjump1:
include "spritedata/sprites level4-04d/ludwigjump1.tcs.gen"

characteraddressludwigjump2:
include "spritedata/sprites level4-04d/ludwigjump2.tgs.gen"
coloraddressludwigjump2:
include "spritedata/sprites level4-04d/ludwigjump2.tcs.gen"

;ludwigshell
characteraddressludwigshell1:
include "spritedata/sprites level4-04d/ludwigshell1.tgs.gen"
coloraddressludwigshell1:
include "spritedata/sprites level4-04d/ludwigshell1.tcs.gen"
characteraddressludwigshell2:
include "spritedata/sprites level4-04d/ludwigshell2.tgs.gen"
coloraddressludwigshell2:
include "spritedata/sprites level4-04d/ludwigshell2.tcs.gen"
characteraddressludwigshell3:
include "spritedata/sprites level4-04d/ludwigshell3.tgs.gen"
coloraddressludwigshell3:
include "spritedata/sprites level4-04d/ludwigshell3.tcs.gen"


;ludwigs fireball
characteraddressludwigfireball1left:
include "spritedata/sprites level4-04d/ludwigfireball1left.tgs.gen"
coloraddressludwigfireball1left:
include "spritedata/sprites level4-04d/ludwigfireball1left.tcs.gen"
characteraddressludwigfireball2left:
include "spritedata/sprites level4-04d/ludwigfireball2left.tgs.gen"
coloraddressludwigfireball2left:
include "spritedata/sprites level4-04d/ludwigfireball2left.tcs.gen"

characteraddressludwigfireball1right:
include "spritedata/sprites level4-04d/ludwigfireball1right.tgs.gen"
coloraddressludwigfireball1right:
include "spritedata/sprites level4-04d/ludwigfireball1right.tcs.gen"
characteraddressludwigfireball2right:
include "spritedata/sprites level4-04d/ludwigfireball2right.tgs.gen"
coloraddressludwigfireball2right:
include "spritedata/sprites level4-04d/ludwigfireball2right.tcs.gen"



;wiggler
characteraddresswiggler1left:
include "spritedata/sprites level5-01/wiggler1left.tgs.gen"
coloraddresswiggler1left:
include "spritedata/sprites level5-01/wiggler1left.tcs.gen"
characteraddresswiggler2left:
include "spritedata/sprites level5-01/wiggler2left.tgs.gen"
coloraddresswiggler2left:
include "spritedata/sprites level5-01/wiggler2left.tcs.gen"
characteraddresswiggler3left:
include "spritedata/sprites level5-01/wiggler3left.tgs.gen"
coloraddresswiggler3left:
include "spritedata/sprites level5-01/wiggler3left.tcs.gen"
characteraddresswiggler4left:
include "spritedata/sprites level5-01/wiggler4left.tgs.gen"
coloraddresswiggler4left:
include "spritedata/sprites level5-01/wiggler4left.tcs.gen"

characteraddresswiggler1right:
include "spritedata/sprites level5-01/wiggler1right.tgs.gen"
coloraddresswiggler1right:
include "spritedata/sprites level5-01/wiggler1right.tcs.gen"
characteraddresswiggler2right:
include "spritedata/sprites level5-01/wiggler2right.tgs.gen"
coloraddresswiggler2right:
include "spritedata/sprites level5-01/wiggler2right.tcs.gen"
characteraddresswiggler3right:
include "spritedata/sprites level5-01/wiggler3right.tgs.gen"
coloraddresswiggler3right:
include "spritedata/sprites level5-01/wiggler3right.tcs.gen"
characteraddresswiggler4right:
include "spritedata/sprites level5-01/wiggler4right.tgs.gen"
coloraddresswiggler4right:
include "spritedata/sprites level5-01/wiggler4right.tcs.gen"

;pissed
characteraddresswigglerpissed1left:
include "spritedata/sprites level5-01/wigglerpissed1left.tgs.gen"
coloraddresswigglerpissed1left:
include "spritedata/sprites level5-01/wigglerpissed1left.tcs.gen"
characteraddresswigglerpissed2left:
include "spritedata/sprites level5-01/wigglerpissed2left.tgs.gen"
coloraddresswigglerpissed2left:
include "spritedata/sprites level5-01/wigglerpissed2left.tcs.gen"
characteraddresswigglerpissed3left:
include "spritedata/sprites level5-01/wigglerpissed3left.tgs.gen"
coloraddresswigglerpissed3left:
include "spritedata/sprites level5-01/wigglerpissed3left.tcs.gen"
characteraddresswigglerpissed4left:
include "spritedata/sprites level5-01/wigglerpissed4left.tgs.gen"
coloraddresswigglerpissed4left:
include "spritedata/sprites level5-01/wigglerpissed4left.tcs.gen"

characteraddresswigglerpissed1right:
include "spritedata/sprites level5-01/wigglerpissed1right.tgs.gen"
coloraddresswigglerpissed1right:
include "spritedata/sprites level5-01/wigglerpissed1right.tcs.gen"
characteraddresswigglerpissed2right:
include "spritedata/sprites level5-01/wigglerpissed2right.tgs.gen"
coloraddresswigglerpissed2right:
include "spritedata/sprites level5-01/wigglerpissed2right.tcs.gen"
characteraddresswigglerpissed3right:
include "spritedata/sprites level5-01/wigglerpissed3right.tgs.gen"
coloraddresswigglerpissed3right:
include "spritedata/sprites level5-01/wigglerpissed3right.tcs.gen"
characteraddresswigglerpissed4right:
include "spritedata/sprites level5-01/wigglerpissed4right.tgs.gen"
coloraddresswigglerpissed4right:
include "spritedata/sprites level5-01/wigglerpissed4right.tcs.gen"

;wiggler turns around or dies

characteraddresswigglerhead1turnleft:
include "spritedata/sprites level5-01/wigglerhead1turnleft.tgs.gen"
coloraddresswigglerhead1turnleft:
include "spritedata/sprites level5-01/wigglerhead1turnleft.tcs.gen"
characteraddresswigglerhead2turnleft:
include "spritedata/sprites level5-01/wigglerhead2turnleft.tgs.gen"
coloraddresswigglerhead2turnleft:
include "spritedata/sprites level5-01/wigglerhead2turnleft.tcs.gen"


characteraddresswigglerbody1turnleft1:
include "spritedata/sprites level5-01/wigglerbody1turnleft1.tgs.gen"
coloraddresswigglerbody1turnleft1:
include "spritedata/sprites level5-01/wigglerbody1turnleft1.tcs.gen"
characteraddresswigglerbody1turnleft2:
include "spritedata/sprites level5-01/wigglerbody1turnleft2.tgs.gen"
coloraddresswigglerbody1turnleft2:
include "spritedata/sprites level5-01/wigglerbody1turnleft2.tcs.gen"

characteraddresswigglerbody2turnleft1:
include "spritedata/sprites level5-01/wigglerbody2turnleft1.tgs.gen"
coloraddresswigglerbody2turnleft1:
include "spritedata/sprites level5-01/wigglerbody2turnleft1.tcs.gen"
characteraddresswigglerbody2turnleft2:
include "spritedata/sprites level5-01/wigglerbody2turnleft2.tgs.gen"
coloraddresswigglerbody2turnleft2:
include "spritedata/sprites level5-01/wigglerbody2turnleft2.tcs.gen"


characteraddresswigglerhead1turnright:
include "spritedata/sprites level5-01/wigglerhead1turnright.tgs.gen"
coloraddresswigglerhead1turnright:
include "spritedata/sprites level5-01/wigglerhead1turnright.tcs.gen"
characteraddresswigglerhead2turnright:
include "spritedata/sprites level5-01/wigglerhead2turnright.tgs.gen"
coloraddresswigglerhead2turnright:
include "spritedata/sprites level5-01/wigglerhead2turnright.tcs.gen"


characteraddresswigglerbody1turnright1:
include "spritedata/sprites level5-01/wigglerbody1turnright1.tgs.gen"
coloraddresswigglerbody1turnright1:
include "spritedata/sprites level5-01/wigglerbody1turnright1.tcs.gen"
characteraddresswigglerbody1turnright2:
include "spritedata/sprites level5-01/wigglerbody1turnright2.tgs.gen"
coloraddresswigglerbody1turnright2:
include "spritedata/sprites level5-01/wigglerbody1turnright2.tcs.gen"

characteraddresswigglerbody2turnright1:
include "spritedata/sprites level5-01/wigglerbody2turnright1.tgs.gen"
coloraddresswigglerbody2turnright1:
include "spritedata/sprites level5-01/wigglerbody2turnright1.tcs.gen"
characteraddresswigglerbody2turnright2:
include "spritedata/sprites level5-01/wigglerbody2turnright2.tgs.gen"
coloraddresswigglerbody2turnright2:
include "spritedata/sprites level5-01/wigglerbody2turnright2.tcs.gen"


;seaurchin
characteraddressseaurchin1:
include "spritedata/sprites level5-02a/seaurchin1.tgs.gen"
coloraddressseaurchin1:
include "spritedata/sprites level5-02a/seaurchin1.tcs.gen"
characteraddressseaurchin2:
include "spritedata/sprites level5-02a/seaurchin2.tgs.gen"
coloraddressseaurchin2:
include "spritedata/sprites level5-02a/seaurchin2.tcs.gen"
characteraddressseaurchin3:
include "spritedata/sprites level5-02a/seaurchin3.tgs.gen"
coloraddressseaurchin3:
include "spritedata/sprites level5-02a/seaurchin3.tcs.gen"


;bubble
characteraddressbubble1:
include "spritedata/sprites level5-03a/bubble1.tgs.gen"
coloraddressbubble1:
include "spritedata/sprites level5-03a/bubble1.tcs.gen"
characteraddressbubble2:
include "spritedata/sprites level5-03a/bubble2.tgs.gen"
coloraddressbubble2:
include "spritedata/sprites level5-03a/bubble2.tcs.gen"


;fishin lakitu
characteraddressfishinlakitu:
include "spritedata/sprites level3-07a/fishinlakitu.tgs.gen"
coloraddressfishinlakitu:
include "spritedata/sprites level3-07a/fishinlakitu.tcs.gen"
characteraddressfishinlakituright:
include "spritedata/sprites level3-07a/fishinlakituright.tgs.gen"
coloraddressfishinlakituright:
include "spritedata/sprites level3-07a/fishinlakituright.tcs.gen"
characteraddresslakitucloud:
include "spritedata/sprites level3-07a/lakitucloud.tgs.gen"
coloraddresslakitucloud:
include "spritedata/sprites level3-07a/lakitucloud.tcs.gen"

characteraddresslakitutrowright:
include "spritedata/sprites level3-07a/lakitutrowright.tgs.gen"
coloraddresslakitutrowright:
include "spritedata/sprites level3-07a/lakitutrowright.tcs.gen"

characteraddresssmileyboxplatform:
include "spritedata/sprites level5-06a/smileyboxplatform.tgs.gen"
coloraddresssmileyboxplatform:
include "spritedata/sprites level5-06a/smileyboxplatform.tcs.gen"


;coverupsprite forest fortress
characteraddressffcoverup:
include "spritedata/sprites level5-07a/ffcoverup.tgs.gen"
coloraddressffcoverup:
include "spritedata/sprites level5-07a/ffcoverup.tcs.gen"


;circlesaw
characteraddresscirclesaw:
include "spritedata/sprites level5-07a/circlesaw.tgs.gen"
coloraddresscirclesaw:
include "spritedata/sprites level5-07a/circlesaw.tcs.gen"
characteraddresscirclesaw1:
include "spritedata/sprites level5-07a/circlesaw1.tgs.gen"
coloraddresscirclesaw1:
include "spritedata/sprites level5-07a/circlesaw1.tcs.gen"

;circlesawhalf
characteraddresscirclesawhalf:
include "spritedata/sprites level5-07a/circlesawhalf.tgs.gen"
coloraddresscirclesawhalf:
include "spritedata/sprites level5-07a/circlesawhalf.tcs.gen"
characteraddresscirclesawhalf1:
include "spritedata/sprites level5-07a/circlesawhalf1.tgs.gen"
coloraddresscirclesawhalf1:
include "spritedata/sprites level5-07a/circlesawhalf1.tcs.gen"


;stonekoopa
characteraddressstonekoopa:
include "spritedata/sprites level2-011a mortons-castle/stonekoopa.tgs.gen"
coloraddressstonekoopa:
include "spritedata/sprites level2-011a mortons-castle/stonekoopa.tcs.gen"
characteraddressstonekoopafireball:
include "spritedata/sprites level2-011a mortons-castle/fireball.tgs.gen"
coloraddressstonekoopafireball:
include "spritedata/sprites level2-011a mortons-castle/fireball.tcs.gen"
characteraddressstonekoopafireball1:
include "spritedata/sprites level2-011a mortons-castle/fireball1.tgs.gen"
coloraddressstonekoopafireball1:
include "spritedata/sprites level2-011a mortons-castle/fireball1.tcs.gen"

characteraddressstonekoopafireball2:
include "spritedata/sprites level2-011a mortons-castle/fireball2.tgs.gen"
coloraddressstonekoopafireball2:
include "spritedata/sprites level2-011a mortons-castle/fireball2.tcs.gen"
characteraddressstonekoopafireball3:
include "spritedata/sprites level2-011a mortons-castle/fireball3.tgs.gen"
coloraddressstonekoopafireball3:
include "spritedata/sprites level2-011a mortons-castle/fireball3.tcs.gen"
characteraddressstonekoopafireball4:
include "spritedata/sprites level2-011a mortons-castle/fireball4.tgs.gen"
coloraddressstonekoopafireball4:
include "spritedata/sprites level2-011a mortons-castle/fireball4.tcs.gen"
characteraddressstonekoopafireball5:
include "spritedata/sprites level2-011a mortons-castle/fireball5.tgs.gen"
coloraddressstonekoopafireball5:
include "spritedata/sprites level2-011a mortons-castle/fireball5.tcs.gen"



;cutscene objects
characteraddresscastleflame:
include "cutscenes/castleflame.tgs.gen"
coloraddresscastleflame:
include "cutscenes/castleflame.tcs.gen"
characteraddresscastleflame1:
include "cutscenes/castleflame1.tgs.gen"
coloraddresscastleflame1:
include "cutscenes/castleflame1.tcs.gen"

characteraddressflyingcastle:
include "cutscenes/flyingcastle.tgs.gen"
coloraddressflyingcastle:
include "cutscenes/flyingcastle.tcs.gen"
characteraddressflyingcastle1:
include "cutscenes/flyingcastle1.tgs.gen"
coloraddressflyingcastle1:
include "cutscenes/flyingcastle1.tcs.gen"

characteraddressmariolooking:
include "cutscenes/mariolooking.tgs.gen"
coloraddressmariolooking:
include "cutscenes/mariolooking.tcs.gen"
characteraddressmariolooking1:
include "cutscenes/mariolooking1.tgs.gen"
coloraddressmariolooking1:
include "cutscenes/mariolooking1.tcs.gen"

characteraddressquestionmark:
include "cutscenes/questionmark.tgs.gen"
coloraddressquestionmark:
include "cutscenes/questionmark.tcs.gen"


characteraddressmariofried:
include "cutscenes/mariofried.tgs.gen"
coloraddressmariofried:
include "cutscenes/mariofried.tcs.gen"
characteraddressmariofried1:
include "cutscenes/mariofried1.tgs.gen"
coloraddressmariofried1:
include "cutscenes/mariofried1.tcs.gen"



;dino rhino (big)
characteraddressdinorhino:
include "spritedata/sprites level6-01a/dinorhino.tgs.gen"
coloraddressdinorhino:
include "spritedata/sprites level6-01a/dinorhino.tcs.gen"
characteraddressdinorhino1:
include "spritedata/sprites level6-01a/dinorhino1.tgs.gen"
coloraddressdinorhino1:
include "spritedata/sprites level6-01a/dinorhino1.tcs.gen"
;right
characteraddressdinorhino2:
include "spritedata/sprites level6-01a/dinorhino2.tgs.gen"
coloraddressdinorhino2:
include "spritedata/sprites level6-01a/dinorhino2.tcs.gen"
characteraddressdinorhino3:
include "spritedata/sprites level6-01a/dinorhino3.tgs.gen"
coloraddressdinorhino3:
include "spritedata/sprites level6-01a/dinorhino3.tcs.gen"


;dino rhino (small)
characteraddressdinorhinosmall:
include "spritedata/sprites level6-01a/dinorhinosmall.tgs.gen"
coloraddressdinorhinosmall:
include "spritedata/sprites level6-01a/dinorhinosmall.tcs.gen"
characteraddressdinorhinosmall1:
include "spritedata/sprites level6-01a/dinorhinosmall1.tgs.gen"
coloraddressdinorhinosmall1:
include "spritedata/sprites level6-01a/dinorhinosmall1.tcs.gen"
characteraddressdinorhinosmall2:
include "spritedata/sprites level6-01a/dinorhinosmall2.tgs.gen"
coloraddressdinorhinosmall2:
include "spritedata/sprites level6-01a/dinorhinosmall2.tcs.gen"
characteraddressdinorhinosmall3:
include "spritedata/sprites level6-01a/dinorhinosmall3.tgs.gen"
coloraddressdinorhinosmall3:
include "spritedata/sprites level6-01a/dinorhinosmall3.tcs.gen"
;right
characteraddressdinorhinosmall4:
include "spritedata/sprites level6-01a/dinorhinosmall4.tgs.gen"
coloraddressdinorhinosmall4:
include "spritedata/sprites level6-01a/dinorhinosmall4.tcs.gen"
characteraddressdinorhinosmall5:
include "spritedata/sprites level6-01a/dinorhinosmall5.tgs.gen"
coloraddressdinorhinosmall5:
include "spritedata/sprites level6-01a/dinorhinosmall5.tcs.gen"
characteraddressdinorhinosmall6:
include "spritedata/sprites level6-01a/dinorhinosmall6.tgs.gen"
coloraddressdinorhinosmall6:
include "spritedata/sprites level6-01a/dinorhinosmall6.tcs.gen"
characteraddressdinorhinosmall7:
include "spritedata/sprites level6-01a/dinorhinosmall7.tgs.gen"
coloraddressdinorhinosmall7:
include "spritedata/sprites level6-01a/dinorhinosmall7.tcs.gen"
;up
characteraddressdinorhinospitup:
include "spritedata/sprites level6-01a/dinorhinospitup.tgs.gen"
coloraddressdinorhinospitup:
include "spritedata/sprites level6-01a/dinorhinospitup.tcs.gen"
characteraddressdinorhinospitup1:
include "spritedata/sprites level6-01a/dinorhinospitup1.tgs.gen"
coloraddressdinorhinospitup1:
include "spritedata/sprites level6-01a/dinorhinospitup1.tcs.gen"



;sheerplatform
characteraddresssheerplatform:
include "spritedata/sprites level6-04a/sheerplatform.tgs.gen"
coloraddresssheerplatform:
include "spritedata/sprites level6-04a/sheerplatform.tcs.gen"
characteraddresssheerplatform1:
include "spritedata/sprites level6-04a/sheerplatform1.tgs.gen"
coloraddresssheerplatform1:
include "spritedata/sprites level6-04a/sheerplatform1.tcs.gen"



;megamole
characteraddressmegamole:
include "spritedata/sprites level6-04a/megamole.tgs.gen"
coloraddressmegamole:
include "spritedata/sprites level6-04a/megamole.tcs.gen"
characteraddressmegamole1:
include "spritedata/sprites level6-04a/megamole1.tgs.gen"
coloraddressmegamole1:
include "spritedata/sprites level6-04a/megamole1.tcs.gen"
characteraddressmegamole2:
include "spritedata/sprites level6-04a/megamole2.tgs.gen"
coloraddressmegamole2:
include "spritedata/sprites level6-04a/megamole2.tcs.gen"
characteraddressmegamole3:
include "spritedata/sprites level6-04a/megamole3.tgs.gen"
coloraddressmegamole3:
include "spritedata/sprites level6-04a/megamole3.tcs.gen"



;fishjump
characteraddressfishjump:
include "spritedata/ingame sprites level 06/fishjump.tgs.gen"
coloraddressfishjump:
include "spritedata/ingame sprites level 06/fishjump.tcs.gen"
characteraddressfishjump1:
include "spritedata/ingame sprites level 06/fishjump1.tgs.gen"
coloraddressfishjump1:
include "spritedata/ingame sprites level 06/fishjump1.tcs.gen"
characteraddressfishjump2:
include "spritedata/ingame sprites level 06/fishjump2.tgs.gen"
coloraddressfishjump2:
include "spritedata/ingame sprites level 06/fishjump2.tcs.gen"
characteraddressfishjump3:
include "spritedata/ingame sprites level 06/fishjump3.tgs.gen"
coloraddressfishjump3:
include "spritedata/ingame sprites level 06/fishjump3.tcs.gen"

;hole in ghosthouse that moves
characteraddressghosthole:
include "spritedata/sprites level6-06a/ghosthole.tgs.gen"
coloraddressghosthole:
include "spritedata/sprites level6-06a/ghosthole.tcs.gen"
characteraddressghosthole1:
include "spritedata/sprites level6-06a/ghosthole1.tgs.gen"
coloraddressghosthole1:
include "spritedata/sprites level6-06a/ghosthole1.tcs.gen"

;fishin boo
characteraddressfishinboo:
include "spritedata/sprites level6-06a/fishinboo.tgs.gen"
coloraddressfishinboo:
include "spritedata/sprites level6-06a/fishinboo.tcs.gen"
characteraddressfishinboo1:
include "spritedata/sprites level6-06a/fishinboo1.tgs.gen"
coloraddressfishinboo1:
include "spritedata/sprites level6-06a/fishinboo1.tcs.gen"
characteraddressfishinboo2:
include "spritedata/sprites level6-06a/fishinboo2.tgs.gen"
coloraddressfishinboo2:
include "spritedata/sprites level6-06a/fishinboo2.tcs.gen"
characteraddressfishinboo3:
include "spritedata/sprites level6-06a/fishinboo3.tgs.gen"
coloraddressfishinboo3:
include "spritedata/sprites level6-06a/fishinboo3.tcs.gen"
characteraddressfishinboo4:
include "spritedata/sprites level6-06a/fishinboo4.tgs.gen"
coloraddressfishinboo4:
include "spritedata/sprites level6-06a/fishinboo4.tcs.gen"
characteraddressfishinboo5:
include "spritedata/sprites level6-06a/fishinboo5.tgs.gen"
coloraddressfishinboo5:
include "spritedata/sprites level6-06a/fishinboo5.tcs.gen"
characteraddressfishinboo6:
include "spritedata/sprites level6-06a/fishinboo6.tgs.gen"
coloraddressfishinboo6:
include "spritedata/sprites level6-06a/fishinboo6.tcs.gen"
characteraddressfishinboo7:
include "spritedata/sprites level6-06a/fishinboo7.tgs.gen"
coloraddressfishinboo7:
include "spritedata/sprites level6-06a/fishinboo7.tcs.gen"

characteraddressblueflame:
include "spritedata/sprites level6-06a/blueflame.tgs.gen"
coloraddressblueflame:
include "spritedata/sprites level6-06a/blueflame.tcs.gen"
characteraddressblueflame1:
include "spritedata/sprites level6-06a/blueflame1.tgs.gen"
coloraddressblueflame1:
include "spritedata/sprites level6-06a/blueflame1.tcs.gen"


;blockboo
characteraddressblockboo:
include "spritedata/sprites level6-06a/blockboo.tgs.gen"
coloraddressblockboo:
include "spritedata/sprites level6-06a/blockboo.tcs.gen"
characteraddressblockboo1:
include "spritedata/sprites level6-06a/blockboo1.tgs.gen"
coloraddressblockboo1:
include "spritedata/sprites level6-06a/blockboo1.tcs.gen"
characteraddressblockboo2:
include "spritedata/sprites level6-06a/blockboo2.tgs.gen"
coloraddressblockboo2:
include "spritedata/sprites level6-06a/blockboo2.tcs.gen"


;woodenpillar
characteraddresswoodenpillar:
include "spritedata/sprites level6-08a/woodenpillar.tgs.gen"
coloraddresswoodenpillar:
include "spritedata/sprites level6-08a/woodenpillar.tcs.gen"
characteraddresswoodenpillar1:
include "spritedata/sprites level6-08a/woodenpillar1.tgs.gen"
coloraddresswoodenpillar1:
include "spritedata/sprites level6-08a/woodenpillar1.tcs.gen"



;wendyblock
characteraddresswendyblock:
include "spritedata/sprites level6-09b/wendyblock.tgs.gen"
coloraddresswendyblock:
include "spritedata/sprites level6-09b/wendyblock.tcs.gen"

;wendyfireball
characteraddresswendyfireball:
include "spritedata/sprites level6-09b/wendyfireball.tgs.gen"
coloraddresswendyfireball:
include "spritedata/sprites level6-09b/wendyfireball.tcs.gen"
characteraddresswendyfireball1:
include "spritedata/sprites level6-09b/wendyfireball1.tgs.gen"
coloraddresswendyfireball1:
include "spritedata/sprites level6-09b/wendyfireball1.tcs.gen"




      ds                $c000-$,$ff
dephase






;extra mapdata 2
;
; block $A4 - A5
;
phase   $8000

grpx86addr:	incbin "../grapx/world5/level5-01a/packedgrpx/grpx.pck" 
grpxblckl86: equ $A4 ;illusion1
grpx87addr:	incbin "../grapx/world5/level5-02a/packedgrpx/grpx.pck" 
grpxblckl87: equ $A4 ;illusion2
grpx88addr:	incbin "../grapx/world5/level5-03a/packedgrpx/grpx.pck" 
grpxblckl88: equ $A4 ;illusion3
grpx89addr:	incbin "../grapx/world5/level5-04a/packedgrpx/grpx.pck" 
grpxblckl89: equ $A4 ;illusion4
grpx90addr:	incbin "../grapx/world5/level5-06a/packedgrpx/grpx.pck" 
grpxblckl90: equ $A4 ;illusion6


backgrl88:  incbin "../level_editor/_levels/maplevel5-01.stg.pck" 
blockbackgrl88:  equ $A4 ;illusion1 
backgrl89:  incbin "../level_editor/_levels/maplevel5-02a.stg.pck" 
blockbackgrl89:  equ $A4 ;illusion2 


        ds                $c000-$,$ff
dephase




;extra mapdata 3
;
; block $A6 - A7
;
phase   $8000

grpx91addr:	incbin "../grapx/world5/level5-07a/packedgrpx/grpx.pck" 
grpxblckl91: equ $A6 ;illusion7a
grpx92addr:	incbin "../grapx/world5/level5-07b/packedgrpx/grpx.pck" 
grpxblckl92: equ $A6 ;illusion7b

backgrl90:  incbin "../level_editor/_levels/maplevel5-03a.stg.pck" 
blockbackgrl90:  equ $A6 ;illusion3a 
backgrl91:  incbin "../level_editor/_levels/maplevel5-03b.stg.pck" 
blockbackgrl91:  equ $A6 ;illusion3b 
backgrl92:  incbin "../level_editor/_levels/maplevel5-04a.stg.pck" 
blockbackgrl92:  equ $A6 ;illusion4a 
backgrl93:  incbin "../level_editor/_levels/maplevel5-04b.stg.pck" 
blockbackgrl93:  equ $A6 ;illusion4b 
backgrl94:  incbin "../level_editor/_levels/maplevel5-04c.stg.pck" 
blockbackgrl94:  equ $A6 ;illusion4c 
backgrl95:  incbin "../level_editor/_levels/maplevel5-05a.stg.pck" 
blockbackgrl95:  equ $A6 ;illusion5a 
backgrl96:  incbin "../level_editor/_levels/maplevel5-05b.stg.pck" 
blockbackgrl96:  equ $A6 ;illusion5b 
backgrl97:  incbin "../level_editor/_levels/maplevel5-06a.stg.pck" 
blockbackgrl97:  equ $A6 ;illusion6a 
backgrl98:  incbin "../level_editor/_levels/maplevel5-07a.stg.pck" 
blockbackgrl98:  equ $A6 ;illusion7a 



        ds                $c000-$,$ff
dephase

;extra mapdata 4
;
; block $A8 - A9
;
phase   $8000


grpx93addr:	incbin "../grapx/world6/level6-01a/packedgrpx/grpx.pck" 
grpxblckl93: equ $A8 ;chocolate1a
grpx94addr:	incbin "../grapx/world6/level6-01b/packedgrpx/grpx.pck" 
grpxblckl94: equ $A8 ;chocolate1b
grpx95addr:	incbin "../grapx/world6/level6-02/packedgrpx/grpx.pck" 
grpxblckl95: equ $A8 ;chocolate2




backgrl99:  incbin "../level_editor/_levels/maplevel5-07b.stg.pck" 
blockbackgrl99:  equ $A8 ;illusion7b 
backgrl100: incbin "../level_editor/_levels/maplevel5-08a.stg.pck" 
blockbackgrl100: equ $A8 ;illusion8a 
backgrl102: incbin "../level_editor/_levels/maplevel5-09.stg.pck" 
blockbackgrl102: equ $A8 ;blue switch palace 
backgrl104: incbin "../level_editor/_levels/maplevel3-12.stg.pck" 
blockbackgrl104: equ $A8 ;red switch palace

backgrl105: incbin "../level_editor/_levels/maplevel6-01a.stg.pck" 
blockbackgrl105: equ $A8 ;chocolate1a 
backgrl106: incbin "../level_editor/_levels/maplevel6-01b.stg.pck" 
blockbackgrl106: equ $A8 ;chocolate1b 
backgrl107: incbin "../level_editor/_levels/maplevel6-02a.stg.pck" 
blockbackgrl107: equ $A8 ;chocolate2a 
;backgrl108: incbin "../level_editor/_levels/maplevel6-02b.stg.pck" | blockbackgrl108: equ $A8 ;chocolate2b 
;backgrl109: incbin "../level_editor/_levels/maplevel6-02c.stg.pck" | blockbackgrl109: equ $A8 ;chocolate2c 
;backgrl110: incbin "../level_editor/_levels/maplevel6-02d.stg.pck" | blockbackgrl110: equ $A8 ;chocolate2d 
;backgrl111: incbin "../level_editor/_levels/maplevel6-02e.stg.pck" | blockbackgrl111: equ $A8 ;chocolate2e 
;backgrl112: incbin "../level_editor/_levels/maplevel6-02f.stg.pck" | blockbackgrl112: equ $A8 ;chocolate2f 
;backgrl113: incbin "../level_editor/_levels/maplevel6-02g.stg.pck" | blockbackgrl113: equ $A8 ;chocolate2g 
;backgrl114: incbin "../level_editor/_levels/maplevel6-02h.stg.pck" | blockbackgrl114: equ $A8 ;chocolate2h 
;backgrl115: incbin "../level_editor/_levels/maplevel6-02i.stg.pck" | blockbackgrl115: equ $A8 ;chocolate2i 






        ds                $c000-$,$ff
dephase

;extra mapdata 5
;
; block $AA - AB
;
phase   $8000



grpx96addr:	incbin "../grapx/world6/level6-03a/packedgrpx/grpx.pck" 
grpxblckl96: equ $AA ;chocolate3a
grpx97addr:	incbin "../grapx/world6/level6-04a/packedgrpx/grpx.pck" 
grpxblckl97: equ $AA ;chocolate4a


backgrl108: incbin "../level_editor/_levels/maplevel6-02b.stg.pck" 
blockbackgrl108: equ $AA ;chocolate2b 
backgrl109: incbin "../level_editor/_levels/maplevel6-02c.stg.pck" 
blockbackgrl109: equ $AA ;chocolate2c 
backgrl110: incbin "../level_editor/_levels/maplevel6-02d.stg.pck" 
blockbackgrl110: equ $AA ;chocolate2d 
backgrl111: incbin "../level_editor/_levels/maplevel6-02e.stg.pck" 
blockbackgrl111: equ $AA ;chocolate2e 
backgrl112: incbin "../level_editor/_levels/maplevel6-02f.stg.pck" 
blockbackgrl112: equ $AA ;chocolate2f 
backgrl113: incbin "../level_editor/_levels/maplevel6-02g.stg.pck" 
blockbackgrl113: equ $AA ;chocolate2g 
backgrl114: incbin "../level_editor/_levels/maplevel6-02h.stg.pck" 
blockbackgrl114: equ $AA ;chocolate2h 
backgrl115: incbin "../level_editor/_levels/maplevel6-02i.stg.pck" 
blockbackgrl115: equ $AA ;chocolate2i

backgrl116: incbin "../level_editor/_levels/maplevel6-03a.stg.pck" 
blockbackgrl116: equ $AA ;chocolate3a 
backgrl117: incbin "../level_editor/_levels/maplevel6-03b.stg.pck" 
blockbackgrl117: equ $AA ;chocolate3b 

;backgrl118: incbin "../level_editor/_levels/maplevel6-04a.stg.pck" | blockbackgrl118: equ $AA ;chocolate4a 
;backgrl119: incbin "../level_editor/_levels/maplevel6-04b.stg.pck" | blockbackgrl119: equ $AA ;chocolate4b 

;backgrl120: incbin "../level_editor/_levels/maplevel6-05a.stg.pck" | blockbackgrl120: equ $AA ;chocolate5a 
;backgrl121: incbin "../level_editor/_levels/maplevel6-05b.stg.pck" | blockbackgrl121: equ $AA ;chocolate5b 




        ds                $c000-$,$ff
dephase



; objectslist block 2
;
; block $AC - $AD
;
objectslistblock2: equ $AC
phase	$4000
include "objectlists/level049.asm"
include "objectlists/level050.asm"
include "objectlists/level050b.asm"
include "objectlists/level051.asm"
include "objectlists/level051b.asm"
include "objectlists/level051c.asm"
include "objectlists/level052.asm"
include "objectlists/level052b.asm"
include "objectlists/level053.asm"
include "objectlists/level054.asm"
include "objectlists/level054b.asm"
include "objectlists/level055.asm"
include "objectlists/level056.asm"
include "objectlists/level057.asm"
include "objectlists/level058.asm"
include "objectlists/level059.asm"
include "objectlists/level059b.asm"
include "objectlists/level060.asm"
include "objectlists/level060b.asm"
include "objectlists/level060c.asm"
include "objectlists/level060d.asm"
include "objectlists/level060e.asm"
include "objectlists/level060f.asm"
include "objectlists/level060g.asm"
include "objectlists/level060h.asm"
include "objectlists/level060i.asm"
include "objectlists/level061.asm"
include "objectlists/level061b.asm"
include "objectlists/level062.asm"
include "objectlists/level062b.asm"
include "objectlists/level063.asm"
include "objectlists/level063b.asm"
include "objectlists/level064.asm"
include "objectlists/level064b.asm"
include "objectlists/level065.asm"
include "objectlists/level065b.asm"
include "objectlists/level065c.asm"
include "objectlists/level065d.asm"
include "objectlists/level065e.asm"
include "objectlists/level066.asm"
include "objectlists/level066b.asm"
include "objectlists/level067.asm"
include "objectlists/level067b.asm"
include "objectlists/level067c.asm"
include "objectlists/level068.asm"
include "objectlists/level068b.asm"
include "objectlists/level068c.asm"
include "objectlists/level069.asm"
include "objectlists/level070.asm"
include "objectlists/level070b.asm"
include "objectlists/level070c.asm"
include "objectlists/level071.asm"
include "objectlists/level072.asm"
include "objectlists/level072b.asm"
include "objectlists/level073.asm"
include "objectlists/level073b.asm"
include "objectlists/level073c.asm"
include "objectlists/level074.asm"
include "objectlists/level074b.asm"
include "objectlists/level075.asm"
include "objectlists/level075b.asm"
include "objectlists/level076.asm"
include "objectlists/level077.asm"
include "objectlists/cutscenefortress.asm"
include "objectlists/cutsceneghost.asm"
include "objectlists/testmap.asm"
include "objectlists/intromap.asm"
include "objectlists/starworld1.asm"
include "objectlists/starworld2.asm"
include "objectlists/starworld3.asm"
include "objectlists/starworld4.asm"
include "objectlists/starworld5.asm"
include "objectlists/level078a.asm"
include "objectlists/level078b.asm"
include "objectlists/level078c.asm"
include "objectlists/level078d.asm"
include "objectlists/level078e.asm"
include "objectlists/level078f.asm"
include "objectlists/level078g.asm"
include "objectlists/level078h.asm"
include "objectlists/level078i.asm"
include "objectlists/level078j.asm"
include "objectlists/level078k.asm"
include "objectlists/level078l.asm"
include "objectlists/special1.asm"
include "objectlists/special1b.asm"
include "objectlists/special2.asm"
include "objectlists/special2b.asm"
include "objectlists/special3.asm"
include "objectlists/special3b.asm"
include "objectlists/level023s.asm"
include "objectlists/level023bs.asm"
	ds		$8000-$,$ff
dephase



;extra mapdata 6
;
; block $AE - AF
;
phase   $8000



backgrl118: incbin "../level_editor/_levels/maplevel6-04a.stg.pck" 
blockbackgrl118: equ $AE ;chocolate4a 
backgrl119: incbin "../level_editor/_levels/maplevel6-04b.stg.pck" 
blockbackgrl119: equ $AE ;chocolate4b 


backgrl120: incbin "../level_editor/_levels/maplevel6-05a.stg.pck" 
blockbackgrl120: equ $AE ;chocolate5a 
backgrl121: incbin "../level_editor/_levels/maplevel6-05b.stg.pck" 
blockbackgrl121: equ $AE ;chocolate5b 

backgrl122: incbin "../level_editor/_levels/maplevel6-06a.stg.pck" 
blockbackgrl122: equ $AE ;chocolate6a 
backgrl123: incbin "../level_editor/_levels/maplevel6-06b.stg.pck" 
blockbackgrl123: equ $AE ;chocolate6b 

;backgrl124: incbin "../level_editor/_levels/maplevel6-07a.stg.pck" | blockbackgrl124: equ $AE ;chocolate7a
;backgrl125: incbin "../level_editor/_levels/maplevel6-07b.stg.pck" | blockbackgrl125: equ $AE ;chocolate7b
;backgrl126: incbin "../level_editor/_levels/maplevel6-07c.stg.pck" | blockbackgrl126: equ $AE ;chocolate7c
;backgrl127: incbin "../level_editor/_levels/maplevel6-07d.stg.pck" | blockbackgrl127: equ $AE ;chocolate7d
;backgrl128: incbin "../level_editor/_levels/maplevel6-07e.stg.pck" | blockbackgrl128: equ $AE ;chocolate7e

;backgrl129: incbin "../level_editor/_levels/maplevel6-08a.stg.pck" | blockbackgrl129: equ $AE ;chocolate8a
;backgrl130: incbin "../level_editor/_levels/maplevel6-08b.stg.pck" | blockbackgrl130: equ $AE ;chocolate8b




        ds                $c000-$,$ff
dephase


;extra mapdata 7
;
; block $B0 - B1
;
phase   $8000


grpx98addr:	incbin "../grapx/world6/level6-05a/packedgrpx/grpx.pck" 
grpxblckl98: equ $B0 ;chocolate5a
grpx99addr:	incbin "../grapx/world6/level6-05b/packedgrpx/grpx.pck" 
grpxblckl99: equ $B0 ;chocolate5b
grpx100addr:	incbin "../grapx/world6/level6-08a/packedgrpx/grpx.pck" 
grpxblckl100: equ $B0 ;chocolate8a



backgrl124: incbin "../level_editor/_levels/maplevel6-07a.stg.pck" 
blockbackgrl124: equ $B0 ;chocolate7a
backgrl125: incbin "../level_editor/_levels/maplevel6-07b.stg.pck" 
blockbackgrl125: equ $B0 ;chocolate7b
backgrl126: incbin "../level_editor/_levels/maplevel6-07c.stg.pck" 
blockbackgrl126: equ $B0 ;chocolate7c
backgrl127: incbin "../level_editor/_levels/maplevel6-07d.stg.pck" 
blockbackgrl127: equ $B0 ;chocolate7d
backgrl128: incbin "../level_editor/_levels/maplevel6-07e.stg.pck" 
blockbackgrl128: equ $B0 ;chocolate7e

backgrl129: incbin "../level_editor/_levels/maplevel6-08a.stg.pck" 
blockbackgrl129: equ $B0 ;chocolate8a
backgrl130: incbin "../level_editor/_levels/maplevel6-08b.stg.pck" 
blockbackgrl130: equ $B0 ;chocolate8b

;backgrl131: incbin "../level_editor/_levels/maplevel6-09a.stg.pck" | blockbackgrl131: equ $B0 ;chocolate9a
;backgrl132: incbin "../level_editor/_levels/maplevel6-09b.stg.pck" | blockbackgrl132: equ $B0 ;chocolate9b



        ds                $c000-$,$ff
dephase


;
; block $B2 - B3
;
cutscenedatablock3:                    equ $B2
phase   $8000

  include "cutscenes/cutscenedata3.asm"
        ds                $c000-$,$ff
dephase



;extra mapdata 8
;
; block $B4 - B5
;
phase   $8000


grpx101addr:	incbin "../grapx/world6/level6-06a/packedgrpx/grpx.pck" 
grpxblckl101: equ $B4 ;chocolate6a
grpx102addr:	incbin "../grapx/world6/level6-09a/packedgrpx/grpx.pck" 
grpxblckl102: equ $B4 ;chocolate9a

grpx103addr:	incbin "../grapx/world7/level7-01a/packedgrpx/grpx.pck" 
grpxblckl103: equ $B4 ;bowserva1a
grpx104addr:	incbin "../grapx/world7/level7-01b/packedgrpx/grpx.pck" 
grpxblckl104: equ $B4 ;bowserva1b



backgrl131: incbin "../level_editor/_levels/maplevel6-09a.stg.pck" 
blockbackgrl131: equ $B4 ;chocolate9a
backgrl132: incbin "../level_editor/_levels/maplevel6-09b.stg.pck" 
blockbackgrl132: equ $B4 ;chocolate9b
backgrl133: incbin "../level_editor/_levels/maplevel6-09c.stg.pck" 
blockbackgrl133: equ $B4 ;chocolate9c

backgrl134: incbin "../level_editor/_levels/maplevel7-01a.stg.pck" 
blockbackgrl134: equ $B4 ;bowserva1a
backgrl135: incbin "../level_editor/_levels/maplevel7-01b.stg.pck" 
blockbackgrl135: equ $B4 ;bowserva1b

;backgrl136: incbin "../level_editor/_levels/maplevel7-01c.stg.pck" 
;blockbackgrl136: equ $B4 ;bowserva1c



        ds                $c000-$,$ff
dephase



;
; block $B6 - B7
;
romprogblock12:                equ $B6
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement10.asm"


romprogblock12end:
  
  
      ds                $c000-$,$ff
dephase


;
; block $B8 - BB
;
ingamespritesblock7:                equ $B8
phase     $4000



;wendyfireballxl left
characteraddresswendyfireballxl:
include "spritedata/sprites level6-09b/wendyfireballxl.tgs.gen"
coloraddresswendyfireballxl:
include "spritedata/sprites level6-09b/wendyfireballxl.tcs.gen"
characteraddresswendyfireballxl1:
include "spritedata/sprites level6-09b/wendyfireballxl1.tgs.gen"
coloraddresswendyfireballxl1:
include "spritedata/sprites level6-09b/wendyfireballxl1.tcs.gen"
characteraddresswendyfireballxl2:
include "spritedata/sprites level6-09b/wendyfireballxl2.tgs.gen"
coloraddresswendyfireballxl2:
include "spritedata/sprites level6-09b/wendyfireballxl2.tcs.gen"
characteraddresswendyfireballxl3:
include "spritedata/sprites level6-09b/wendyfireballxl3.tgs.gen"
coloraddresswendyfireballxl3:
include "spritedata/sprites level6-09b/wendyfireballxl3.tcs.gen"


;wendyfireballxl right
characteraddresswendyfireballxlr:
include "spritedata/sprites level6-09b/wendyfireballxlr.tgs.gen"
coloraddresswendyfireballxlr:
include "spritedata/sprites level6-09b/wendyfireballxlr.tcs.gen"
characteraddresswendyfireballxlr1:
include "spritedata/sprites level6-09b/wendyfireballxlr1.tgs.gen"
coloraddresswendyfireballxlr1:
include "spritedata/sprites level6-09b/wendyfireballxlr1.tcs.gen"
characteraddresswendyfireballxlr2:
include "spritedata/sprites level6-09b/wendyfireballxlr2.tgs.gen"
coloraddresswendyfireballxlr2:
include "spritedata/sprites level6-09b/wendyfireballxlr2.tcs.gen"
characteraddresswendyfireballxlr3:
include "spritedata/sprites level6-09b/wendyfireballxlr3.tgs.gen"
coloraddresswendyfireballxlr3:
include "spritedata/sprites level6-09b/wendyfireballxlr3.tcs.gen"


;wendy boss
;wendy
characteraddresswendyfront1:
include "spritedata/sprites level6-09c/wendyfront1.tgs.gen"
coloraddresswendyfront1:
include "spritedata/sprites level6-09c/wendyfront1.tcs.gen"
characteraddresswendyfront2:
include "spritedata/sprites level6-09c/wendyfront2.tgs.gen"
coloraddresswendyfront2:
include "spritedata/sprites level6-09c/wendyfront2.tcs.gen"

characteraddresswendypissed1:
include "spritedata/sprites level6-09c/wendypissed1.tgs.gen"
coloraddresswendypissed1:
include "spritedata/sprites level6-09c/wendypissed1.tcs.gen"
characteraddresswendypissed2:
include "spritedata/sprites level6-09c/wendypissed2.tgs.gen"
coloraddresswendypissed2:
include "spritedata/sprites level6-09c/wendypissed2.tcs.gen"

characteraddresswendyleft1:
include "spritedata/sprites level6-09c/wendyleft1.tgs.gen"
coloraddresswendyleft1:
include "spritedata/sprites level6-09c/wendyleft1.tcs.gen"
characteraddresswendyleft2:
include "spritedata/sprites level6-09c/wendyleft2.tgs.gen"
coloraddresswendyleft2:
include "spritedata/sprites level6-09c/wendyleft2.tcs.gen"

characteraddresswendyright1:
include "spritedata/sprites level6-09c/wendyright1.tgs.gen"
coloraddresswendyright1:
include "spritedata/sprites level6-09c/wendyright1.tcs.gen"
characteraddresswendyright2:
include "spritedata/sprites level6-09c/wendyright2.tgs.gen"
coloraddresswendyright2:
include "spritedata/sprites level6-09c/wendyright2.tcs.gen"

characteraddresswendyfront3:
include "spritedata/sprites level6-09c/wendyfront3.tgs.gen"
coloraddresswendyfront3:
include "spritedata/sprites level6-09c/wendyfront3.tcs.gen"
characteraddresswendyfront4:
include "spritedata/sprites level6-09c/wendyfront4.tgs.gen"
coloraddresswendyfront4:
include "spritedata/sprites level6-09c/wendyfront4.tcs.gen"


characteraddresswendydoll1:
include "spritedata/sprites level6-09c/wendydoll1.tgs.gen"
coloraddresswendydoll1:
include "spritedata/sprites level6-09c/wendydoll1.tcs.gen"
characteraddresswendydoll2:
include "spritedata/sprites level6-09c/wendydoll2.tgs.gen"
coloraddresswendydoll2:
include "spritedata/sprites level6-09c/wendydoll2.tcs.gen"
characteraddresswendydoll3:
include "spritedata/sprites level6-09c/wendydoll3.tgs.gen"
coloraddresswendydoll3:
include "spritedata/sprites level6-09c/wendydoll3.tcs.gen"

characteraddresswendydead:
include "spritedata/sprites level6-09c/wendydead.tgs.gen"
coloraddresswendydead:
include "spritedata/sprites level6-09c/wendydead.tcs.gen"

emptysprite95:
include "spritedata/ingame sprites/empty.tgs.gen"


;countplateau counting from 4 to 1
characteraddresscountplateau:
include "spritedata/sprites level7-04a/countplateau.tgs.gen"
coloraddresscountplateau:
include "spritedata/sprites level7-04a/countplateau.tcs.gen"
characteraddresscountplateau1:
include "spritedata/sprites level7-04a/countplateau1.tgs.gen"
coloraddresscountplateau1:
include "spritedata/sprites level7-04a/countplateau1.tcs.gen"
characteraddresscountplateau2:
include "spritedata/sprites level7-04a/countplateau2.tgs.gen"
coloraddresscountplateau2:
include "spritedata/sprites level7-04a/countplateau2.tcs.gen"
characteraddresscountplateau3:
include "spritedata/sprites level7-04a/countplateau3.tgs.gen"
coloraddresscountplateau3:
include "spritedata/sprites level7-04a/countplateau3.tcs.gen"
characteraddresscountplateau4:
include "spritedata/sprites level7-04a/countplateau4.tgs.gen"
coloraddresscountplateau4:
include "spritedata/sprites level7-04a/countplateau4.tcs.gen"


;lavaplateau
characteraddresslavaplateau:
include "spritedata/sprites level7-05a/lavaplateaus.tgs.gen"
coloraddresslavaplateau:
include "spritedata/sprites level7-05a/lavaplateaus.tcs.gen"

;scoopingchuck
characteraddressscoopingchuck:
include "spritedata/sprites level7-05a/scoopingchuck.tgs.gen"
coloraddressscoopingchuck:
include "spritedata/sprites level7-05a/scoopingchuck.tcs.gen"
characteraddressscoopingchuck1:
include "spritedata/sprites level7-05a/scoopingchuck1.tgs.gen"
coloraddressscoopingchuck1:
include "spritedata/sprites level7-05a/scoopingchuck1.tcs.gen"
characteraddressscoopingchuck2:
include "spritedata/sprites level7-05a/scoopingchuck2.tgs.gen"
coloraddressscoopingchuck2:
include "spritedata/sprites level7-05a/scoopingchuck2.tcs.gen"
characteraddressscoopingchuck3:
include "spritedata/sprites level7-05a/scoopingchuck3.tgs.gen"
coloraddressscoopingchuck3:
include "spritedata/sprites level7-05a/scoopingchuck3.tcs.gen"
characteraddressscoopingchuck4:
include "spritedata/sprites level7-05a/scoopingchuck4.tgs.gen"
coloraddressscoopingchuck4:
include "spritedata/sprites level7-05a/scoopingchuck4.tcs.gen"

;chuckball
characteraddresschuckball:
include "spritedata/sprites level7-05a/chuckball.tgs.gen"
coloraddresschuckball:
include "spritedata/sprites level7-05a/chuckball.tcs.gen"
characteraddresschuckball1:
include "spritedata/sprites level7-05a/chuckball1.tgs.gen"
coloraddresschuckball1:
include "spritedata/sprites level7-05a/chuckball1.tcs.gen"
characteraddresschuckball2:
include "spritedata/sprites level7-05a/chuckball2.tgs.gen"
coloraddresschuckball2:
include "spritedata/sprites level7-05a/chuckball2.tcs.gen"
characteraddresschuckball3:
include "spritedata/sprites level7-05a/chuckball3.tgs.gen"
coloraddresschuckball3:
include "spritedata/sprites level7-05a/chuckball3.tcs.gen"


;cutscene castledoor
characteraddresscastledoor:
include "spritedata/castle1/castledoor.tgs.gen"
coloraddresscastledoor:
include "spritedata/castle1/castledoor.tcs.gen"
;ghost cutscene
characteraddressghostcloud:
include "spritedata/ghost/ghostcloud.tgs.gen"
coloraddressghostcloud:
include "spritedata/ghost/ghostcloud.tcs.gen"

;permayoshi baby
characteraddressbabyyoshi:
include "spritedata/permayoshi/babyyoshi.tgs.gen"
coloraddressbabyyoshi:
include "spritedata/permayoshi/babyyoshi.tcs.gen"
characteraddressbabyyoshi1:
include "spritedata/permayoshi/babyyoshi1.tgs.gen"
coloraddressbabyyoshi1:
include "spritedata/permayoshi/babyyoshi1.tcs.gen"
characteraddressbabyyoshi2:
include "spritedata/permayoshi/babyyoshi2.tgs.gen"
coloraddressbabyyoshi2:
include "spritedata/permayoshi/babyyoshi2.tcs.gen"
characteraddressbabyyoshi3:
include "spritedata/permayoshi/babyyoshi3.tgs.gen"
coloraddressbabyyoshi3:
include "spritedata/permayoshi/babyyoshi3.tcs.gen"
characteraddressbabyyoshi4:
include "spritedata/permayoshi/babyyoshi4.tgs.gen"
coloraddressbabyyoshi4:
include "spritedata/permayoshi/babyyoshi4.tcs.gen"
characteraddressbabyyoshi5:
include "spritedata/permayoshi/babyyoshi5.tgs.gen"
coloraddressbabyyoshi5:
include "spritedata/permayoshi/babyyoshi5.tcs.gen"
characteraddressbabyyoshi6:
include "spritedata/permayoshi/babyyoshi6.tgs.gen"
coloraddressbabyyoshi6:
include "spritedata/permayoshi/babyyoshi6.tcs.gen"
characteraddressbabyyoshi7:
include "spritedata/permayoshi/babyyoshi7.tgs.gen"
coloraddressbabyyoshi7:
include "spritedata/permayoshi/babyyoshi7.tcs.gen"




;mechakoopa
;left
characteraddressmechakoopaleft1:
include "spritedata/sprites level8-01k/mechakoopal.tgs.gen"
coloraddressmechakoopaleft1:
include "spritedata/sprites level8-01k/mechakoopal.tcs.gen"
characteraddressmechakoopaleft2:
include "spritedata/sprites level8-01k/mechakoopal1.tgs.gen"
coloraddressmechakoopaleft2:
include "spritedata/sprites level8-01k/mechakoopal1.tcs.gen"
characteraddressmechakoopaleft3:
include "spritedata/sprites level8-01k/mechakoopal2.tgs.gen"
coloraddressmechakoopaleft3:
include "spritedata/sprites level8-01k/mechakoopal2.tcs.gen"
characteraddressmechakoopaleft4:
include "spritedata/sprites level8-01k/mechakoopal3.tgs.gen"
coloraddressmechakoopaleft4:
include "spritedata/sprites level8-01k/mechakoopal3.tcs.gen"


;right
characteraddressmechakooparight1:
include "spritedata/sprites level8-01k/mechakoopar.tgs.gen"
coloraddressmechakooparight1:
include "spritedata/sprites level8-01k/mechakoopar.tcs.gen"
characteraddressmechakooparight2:
include "spritedata/sprites level8-01k/mechakoopar1.tgs.gen"
coloraddressmechakooparight2:
include "spritedata/sprites level8-01k/mechakoopar1.tcs.gen"
characteraddressmechakooparight3:
include "spritedata/sprites level8-01k/mechakoopar2.tgs.gen"
coloraddressmechakooparight3:
include "spritedata/sprites level8-01k/mechakoopar2.tcs.gen"
characteraddressmechakooparight4:
include "spritedata/sprites level8-01k/mechakoopar3.tgs.gen"
coloraddressmechakooparight4:
include "spritedata/sprites level8-01k/mechakoopar3.tcs.gen"

;down
characteraddressmechakoopadown:
include "spritedata/sprites level8-01k/mechakoopadown.tgs.gen"
coloraddressmechakoopadown:
include "spritedata/sprites level8-01k/mechakoopadown.tcs.gen"
characteraddressmechakoopadown1:
include "spritedata/sprites level8-01k/mechakoopadown1.tgs.gen"
coloraddressmechakoopadown1:
include "spritedata/sprites level8-01k/mechakoopadown1.tcs.gen"


;jumpingstonekoopa
characteraddressjumpingstonekoopa:
include "spritedata/sprites level6-09a/jumpingstonekoopa.tgs.gen"
coloraddressjumpingstonekoopa:
include "spritedata/sprites level6-09a/jumpingstonekoopa.tcs.gen"
characteraddressjumpingstonekoopa1:
include "spritedata/sprites level6-09a/jumpingstonekoopa1.tgs.gen"
coloraddressjumpingstonekoopa1:
include "spritedata/sprites level6-09a/jumpingstonekoopa1.tcs.gen"
characteraddressjumpingstonekoopa2:
include "spritedata/sprites level6-09a/jumpingstonekoopa2.tgs.gen"
coloraddressjumpingstonekoopa2:
include "spritedata/sprites level6-09a/jumpingstonekoopa2.tcs.gen"
characteraddressjumpingstonekoopa3:
include "spritedata/sprites level6-09a/jumpingstonekoopa3.tgs.gen"
coloraddressjumpingstonekoopa3:
include "spritedata/sprites level6-09a/jumpingstonekoopa3.tcs.gen"


;ninja
characteraddressninja:
include "spritedata/sprites level8-01e/ninja.tgs.gen"
coloraddressninja:
include "spritedata/sprites level8-01e/ninja.tcs.gen"
characteraddressninja1:
include "spritedata/sprites level8-01e/ninja1.tgs.gen"
coloraddressninja1:
include "spritedata/sprites level8-01e/ninja1.tcs.gen"


      ds                $c000-$,$ff
dephase



;
; block $BC - BD
;
mapdatablock:                equ $BC
phase     $4000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "mapdata.asm"

  
  
      ds                $8000-$,$ff
dephase


;
; block $BE - BF
;
cutscenedatablock4:                    equ $BE
phase   $8000

  include "cutscenes/cutscenedata4.asm"
        ds                $c000-$,$ff
dephase



;extra mapdata 9
;
; block $C0 - C1
;
phase   $8000


;grpx101addr:	incbin "../grapx/world6/level6-06a/packedgrpx/grpx.pck" 
;grpxblckl101: equ $C0 ;chocolate6a



backgrl136: incbin "../level_editor/_levels/maplevel7-01c.stg.pck" 
blockbackgrl136: equ $C0 ;bowserva1c
backgrl137: incbin "../level_editor/_levels/maplevel7-02a.stg.pck" 
blockbackgrl137: equ $C0 ;bowserva2a
backgrl138: incbin "../level_editor/_levels/maplevel7-03a.stg.pck" 
blockbackgrl138: equ $C0 ;bowserva3a
backgrl139: incbin "../level_editor/_levels/maplevel7-03b.stg.pck" 
blockbackgrl139: equ $C0 ;bowserva3b
backgrl140: incbin "../level_editor/_levels/maplevel7-03c.stg.pck" 
blockbackgrl140: equ $C0 ;bowserva3c

backgrl141: incbin "../level_editor/_levels/maplevel7-04a.stg.pck" 
blockbackgrl141: equ $C0 ;bowserva4a

;backgrl142: incbin "../level_editor/_levels/maplevel7-05a.stg.pck" 
;blockbackgrl142: equ $C0 ;bowserva5a


        ds                $c000-$,$ff
dephase




;extra mapdata 10
;
; block $C2 - C3
;
phase   $8000


grpx105addr:	incbin "../grapx/world7/level7-04a/packedgrpx/grpx.pck" 
grpxblckl105: equ $C2 ;bowserva4a
grpx106addr:	incbin "../grapx/world7/level7-05a/packedgrpx/grpx.pck" 
grpxblckl106: equ $C2 ;bowserva5a
grpx147addr:	incbin "../grapx/world7/level7-07a/packedgrpx/grpx.pck" 
grpxblckl147: equ $C2 ;bowserva7a




backgrl142: incbin "../level_editor/_levels/maplevel7-05a.stg.pck" 
blockbackgrl142: equ $C2 ;bowserva5a
backgrl143: incbin "../level_editor/_levels/maplevel7-05b.stg.pck" 
blockbackgrl143: equ $C2 ;bowserva5a

backgrl144: incbin "../level_editor/_levels/maplevel7-06a.stg.pck" 
blockbackgrl144: equ $C2 ;bowserva6a
backgrl145: incbin "../level_editor/_levels/maplevel7-06b.stg.pck" 
blockbackgrl145: equ $C2 ;bowserva6b
backgrl146: incbin "../level_editor/_levels/maplevel7-06c.stg.pck" 
blockbackgrl146: equ $C2 ;bowserva6c

backgrl147: incbin "../level_editor/_levels/maplevel7-07a.stg.pck" 
blockbackgrl147: equ $C2 ;bowserva7a

;backgrl148: incbin "../level_editor/_levels/maplevel7-08a.stg.pck" 
;blockbackgrl148: equ $C2 ;bowserva8a


        ds                $c000-$,$ff
dephase


;
; block $C4
;
yellowpillarinitblock:                equ $C4
phase     $8000


  include "handleobjectmovement/inityellowpillars.asm"
  
  
      ds                $a000-$,$ff
dephase


;extra mapdata 11
;
; block $C5 - C6
;
phase   $8000


grpx149addr:	incbin "../grapx/world7/level7-08b/packedgrpx/grpx.pck" 
grpxblckl149: equ $C5 ;bowserva8b

;castle1 cutscene grpx
grpx160addr:	incbin "../grapx/cutscenes/castle1/packedgrpx/grpx.pck" 
grpxblckl160: equ $C5 ;castle 1 & 2 cutscene

;ghost cutscene grpx
grpx161addr:	incbin "../grapx/cutscenes/ghost/packedgrpx/grpx.pck" 
grpxblckl161: equ $C5 ;ghost cutscene

;intro cutscene grpx
grpx162addr:	incbin "../grapx/cutscenes/intro/packedgrpx/grpx.pck" 
grpxblckl162: equ $C5 ;intro cutscene

;starworld2
grpx163addr:	incbin "../grapx/starworld/starworld2/packedgrpx/grpx.pck" 
grpxblckl163: equ $C5 ;starworld2



backgrl148: incbin "../level_editor/_levels/maplevel7-08a.stg.pck" 
blockbackgrl148: equ $C5 ;bowserva8a
backgrl149: incbin "../level_editor/_levels/maplevel7-08b.stg.pck" 
blockbackgrl149: equ $C5 ;bowserva8b

;castle1 cutscene map
backgrl160: incbin "../level_editor/_levels/castle1.stg.pck" 
blockbackgrl160: equ $C5 ;castle 1 cutscene
backgrl161: incbin "../level_editor/_levels/castle2.stg.pck" 
blockbackgrl161: equ $C5 ;castle 2 cutscene

;ghost cutscene map
backgrl162: incbin "../level_editor/_levels/ghost.stg.pck" 
blockbackgrl162: equ $C5 ;ghost cutscene

;intro cutscene map
backgrl163: incbin "../level_editor/_levels/blablabla.stg.pck" 
blockbackgrl163: equ $C5 ;intro cutscene


;starworld1
;backgrl164: incbin "../level_editor/_levels/starworld1.stg.pck" 
;blockbackgrl164: equ $C5 ;starworld1
;starworld2
;backgrl165: incbin "../level_editor/_levels/starworld2.stg.pck" 
;blockbackgrl165: equ $C5 ;starworld2


        ds                $c000-$,$ff
dephase


;
; block $C7
;
startingcoordinatesblock:                equ $C7
phase     $4000

  include "startingcoordinates.asm"
  
      ds                $6000-$,$ff
dephase



;
; block $C8 - C9
;
romprogblock13:                equ $C8
phase     $8000
;yeah we need an additional 16Kb to get all the stuff to work :S

  include "handleobjectmovement/handleobjectmovement11.asm"


romprogblock13end:
  
  
      ds                $c000-$,$ff
dephase














;
; block $ca - $cb
;
;
WorldMapTileAnimationsBlock:    equ $ca
phase	$4000
	include	"worldmap_tileanimations.asm"	
	ds		$8000-$,$ff
dephase

;
; block $cc - $cd
;
;
WorldMapExtraBlock:    equ $cc
phase	$8000
;include "worldmap.asm"
	ds		$c000-$,$ff
dephase

;
; block $ce
;
;
worldmapmusic_FORILLUblock:               equ $ce
phase   $8000
  db 0
  incbin "../music/ILLUSION.BGM"
  ds                $a000-$,$ff
dephase

;
; block $cf
;
;
phase	$4000
starworldmapblock: equ $cf
starworldmap:              incbin "../worldmap_tileeditor/starworld.MAR",0,$600
starandspecialworldpalette:      incbin "../worldmap_tileeditor/starandspecialworldtiles.SC5",$7680+7,32
	ds		$6000-$,$ff
dephase




;extra mapdata 12
;
; block $D0 - D1
;
phase   $8000


;starworld2
grpx164addr:	incbin "../grapx/starworld/starworld2/packedgrpx/grpx.pck" 
grpxblckl164: equ $D0 ;starworld2
;starworld3
grpx165addr:	incbin "../grapx/starworld/starworld3/packedgrpx/grpx.pck" 
grpxblckl165: equ $D0 ;starworld3
;starworld4
grpx166addr:	incbin "../grapx/starworld/starworld4/packedgrpx/grpx.pck" 
grpxblckl166: equ $D0 ;starworld4

;bowser castle 1a
grpx167addr:	incbin "../grapx/world8/level8-01a/packedgrpx/grpx.pck" 
grpxblckl167: equ $D0 ;bowser castle 1a



;starworld1
backgrl164: incbin "../level_editor/_levels/starworld1.stg.pck" 
blockbackgrl164: equ $D0 ;starworld1
;starworld2
backgrl165: incbin "../level_editor/_levels/starworld2.stg.pck" 
blockbackgrl165: equ $D0 ;starworld2
;starworld3
backgrl166: incbin "../level_editor/_levels/starworld3.stg.pck" 
blockbackgrl166: equ $D0 ;starworld3
;starworld4
backgrl167: incbin "../level_editor/_levels/starworld4.stg.pck" 
blockbackgrl167: equ $D0 ;starworld4
;starworld5
backgrl168: incbin "../level_editor/_levels/starworld5.stg.pck" 
blockbackgrl168: equ $D0 ;starworld5

;bowsers castle 1a
backgrl169: incbin "../level_editor/_levels/maplevel8-01a.stg.pck" 
blockbackgrl169: equ $D0 ;bowsers castle 1a
;bowsers castle 1b
backgrl170: incbin "../level_editor/_levels/maplevel8-01b.stg.pck" 
blockbackgrl170: equ $D0 ;bowsers castle 1b
;bowsers castle 1c
backgrl171: incbin "../level_editor/_levels/maplevel8-01c.stg.pck" 
blockbackgrl171: equ $D0 ;bowsers castle 1c
;bowsers castle 1d
backgrl172: incbin "../level_editor/_levels/maplevel8-01d.stg.pck" 
blockbackgrl172: equ $D0 ;bowsers castle 1d
;bowsers castle 1e
;backgrl173: incbin "../level_editor/_levels/maplevel8-01e.stg.pck" 
;blockbackgrl173: equ $D0 ;bowsers castle 1e

        ds                $c000-$,$ff
dephase



;
; block $D2 - animation code
;
;
phase	$8000
animationcodeblock: equ $D2
    include "animationcode.asm"


	ds		$a000-$,$ff
dephase

;
; block $D3
;
timeupmusicblock:                    equ $D3
phase   $8000
  db 0
  incbin "../music/TIMEUP.BGM"
   
  
  
        ds                $a000-$,$ff
dephase


;
; block $D4
;
starworldmusicblock:                    equ $D4
phase   $8000
  db 0
  incbin "../music/STARW.BGM"
   
  
  
        ds                $a000-$,$ff
dephase


;
; block $D5
;
eventhandlertablesblock:                    equ $D5
phase   $4000

   include "eventhandlertable.asm"
  
  
        ds                $6000-$,$ff
dephase


;
; block $D6
;
msxsongblock:                    equ $D6
phase   $8000
  db 0
  incbin "../music/test.bgm"
   
  
  
        ds                $a000-$,$ff
dephase



;extra mapdata 12
;
; block $D7 - D8
;
phase   $8000

;bowser castle 1e
grpx169addr:	incbin "../grapx/world8/level8-01e/packedgrpx/grpx.pck" 
grpxblckl169: equ $D7 ;bowser castle 1e
;bowser castle 1k
grpx168addr:	incbin "../grapx/world8/level8-01k/packedgrpx/grpx.pck" 
grpxblckl168: equ $D7 ;bowser castle 1k
;the final boss
grpx170addr:	incbin "../grapx/world8/finalboss/packedgrpx/grpx.pck" 
grpxblckl170: equ $D7 ;the final boss
;special1
grpx171addr:	incbin "../grapx/world9/level9-01a/packedgrpx/grpx.pck" 
grpxblckl171: equ $D7 ;special1
;special2
;grpx172addr:	incbin "../grapx/world9/level9-02a/packedgrpx/grpx.pck" 
;grpxblckl172: equ $D7 ;special2




;bowsers castle 1e
backgrl173: incbin "../level_editor/_levels/maplevel8-01e.stg.pck" 
blockbackgrl173: equ $D7 ;bowsers castle 1e
;bowsers castle 1f
backgrl174: incbin "../level_editor/_levels/maplevel8-01f.stg.pck" 
blockbackgrl174: equ $D7 ;bowsers castle 1f
;bowsers castle 1g
backgrl175: incbin "../level_editor/_levels/maplevel8-01g.stg.pck" 
blockbackgrl175: equ $D7 ;bowsers castle 1g
;bowsers castle 1h
backgrl176: incbin "../level_editor/_levels/maplevel8-01h.stg.pck" 
blockbackgrl176: equ $D7 ;bowsers castle 1h
;bowsers castle 1i
backgrl177: incbin "../level_editor/_levels/maplevel8-01i.stg.pck" 
blockbackgrl177: equ $D7 ;bowsers castle 1i
;bowsers castle 1j
backgrl178: incbin "../level_editor/_levels/maplevel8-01j.stg.pck" 
blockbackgrl178: equ $D7 ;bowsers castle 1j
;bowsers castle 1k
backgrl179: incbin "../level_editor/_levels/maplevel8-01k.stg.pck" 
blockbackgrl179: equ $D7 ;bowsers castle 1k
;bowsers castle 1l
backgrl180: incbin "../level_editor/_levels/maplevel8-01l.stg.pck" 
blockbackgrl180: equ $D7 ;bowsers castle 1l

;special1
backgrl181: incbin "../level_editor/_levels/maplevel9-01a.stg.pck" 
blockbackgrl181: equ $D7 ;special1
;special1b
backgrl182: incbin "../level_editor/_levels/maplevel9-01b.stg.pck" 
blockbackgrl182: equ $D7 ;special1b

        ds                $c000-$,$ff
dephase





;
; block $D9 - DC
;
ingamespritesblock8:                equ $D9
phase     $4000



;the final boss. Message to the brave (those who have helped): Thank you so much! Now lets finish this F*CK!
;left
characteraddressbossrunleft:
include "spritedata/finalboss/bossrunleft.tgs.gen"
coloraddressbossrunleft:
include "spritedata/finalboss/bossrunleft.tcs.gen"
characteraddressbossrunleft1:
include "spritedata/finalboss/bossrunleft1.tgs.gen"
coloraddressbossrunleft1:
include "spritedata/finalboss/bossrunleft1.tcs.gen"
characteraddressbossrunleft2:
include "spritedata/finalboss/bossrunleft2.tgs.gen"
coloraddressbossrunleft2:
include "spritedata/finalboss/bossrunleft2.tcs.gen"
characteraddressbossrunleft3:
include "spritedata/finalboss/bossrunleft3.tgs.gen"
coloraddressbossrunleft3:
include "spritedata/finalboss/bossrunleft3.tcs.gen"

;right
characteraddressbossrunright:
include "spritedata/finalboss/bossrunright.tgs.gen"
coloraddressbossrunright:
include "spritedata/finalboss/bossrunright.tcs.gen"
characteraddressbossrunright1:
include "spritedata/finalboss/bossrunright1.tgs.gen"
coloraddressbossrunright1:
include "spritedata/finalboss/bossrunright1.tcs.gen"
characteraddressbossrunright2:
include "spritedata/finalboss/bossrunright2.tgs.gen"
coloraddressbossrunright2:
include "spritedata/finalboss/bossrunright2.tcs.gen"
characteraddressbossrunright3:
include "spritedata/finalboss/bossrunright3.tgs.gen"
coloraddressbossrunright3:
include "spritedata/finalboss/bossrunright3.tcs.gen"


;spit left
characteraddressbossspitleft:
include "spritedata/finalboss/bossspitleft.tgs.gen"
coloraddressbossspitleft:
include "spritedata/finalboss/bossspitleft.tcs.gen"
characteraddressbossspitleft1:
include "spritedata/finalboss/bossspitleft1.tgs.gen"
coloraddressbossspitleft1:
include "spritedata/finalboss/bossspitleft1.tcs.gen"
characteraddressbossspitleft2:
include "spritedata/finalboss/bossspitleft2.tgs.gen"
coloraddressbossspitleft2:
include "spritedata/finalboss/bossspitleft2.tcs.gen"

;spit right
characteraddressbossspitright:
include "spritedata/finalboss/bossspitright.tgs.gen"
coloraddressbossspitright:
include "spritedata/finalboss/bossspitright.tcs.gen"
characteraddressbossspitright1:
include "spritedata/finalboss/bossspitright1.tgs.gen"
coloraddressbossspitright1:
include "spritedata/finalboss/bossspitright1.tcs.gen"
characteraddressbossspitright2:
include "spritedata/finalboss/bossspitright2.tgs.gen"
coloraddressbossspitright2:
include "spritedata/finalboss/bossspitright2.tcs.gen"



;trow left
characteraddressbosstrowleft:
include "spritedata/finalboss/bosstrowleft.tgs.gen"
coloraddressbosstrowleft:
include "spritedata/finalboss/bosstrowleft.tcs.gen"
characteraddressbosstrowleft1:
include "spritedata/finalboss/bosstrowleft1.tgs.gen"
coloraddressbosstrowleft1:
include "spritedata/finalboss/bosstrowleft1.tcs.gen"

;trow right
characteraddressbosstrowright:
include "spritedata/finalboss/bosstrowright.tgs.gen"
coloraddressbosstrowright:
include "spritedata/finalboss/bosstrowright.tcs.gen"
characteraddressbosstrowright1:
include "spritedata/finalboss/bosstrowright1.tgs.gen"
coloraddressbosstrowright1:
include "spritedata/finalboss/bosstrowright1.tcs.gen"


;pissed
characteraddressbosspissed:
include "spritedata/finalboss/bosspissed.tgs.gen"
coloraddressbosspissed:
include "spritedata/finalboss/bosspissed.tcs.gen"
characteraddressbosspissed1:
include "spritedata/finalboss/bosspissed1.tgs.gen"
coloraddressbosspissed1:
include "spritedata/finalboss/bosspissed1.tcs.gen"



;jump
characteraddressbossjump:
include "spritedata/finalboss/bossjump.tgs.gen"
coloraddressbossjump:
include "spritedata/finalboss/bossjump.tcs.gen"
characteraddressbossjump1:
include "spritedata/finalboss/bossjump1.tgs.gen"
coloraddressbossjump1:
include "spritedata/finalboss/bossjump1.tcs.gen"


;fall
characteraddressbossfall:
include "spritedata/finalboss/bossfall.tgs.gen"
coloraddressbossfall:
include "spritedata/finalboss/bossfall.tcs.gen"


;shell
characteraddressbossshell:
include "spritedata/finalboss/bossshell.tgs.gen"
coloraddressbossshell:
include "spritedata/finalboss/bossshell.tcs.gen"
characteraddressbossshell1:
include "spritedata/finalboss/bossshell1.tgs.gen"
coloraddressbossshell1:
include "spritedata/finalboss/bossshell1.tcs.gen"
characteraddressbossshell2:
include "spritedata/finalboss/bossshell2.tgs.gen"
coloraddressbossshell2:
include "spritedata/finalboss/bossshell2.tcs.gen"
characteraddressbossshell3:
include "spritedata/finalboss/bossshell3.tgs.gen"
coloraddressbossshell3:
include "spritedata/finalboss/bossshell3.tcs.gen"


;bossfireball
characteraddressbossfireball:
include "spritedata/finalboss/fireball.tgs.gen"
coloraddressbossfireball:
include "spritedata/finalboss/fireball.tcs.gen"
characteraddressbossfireball1:
include "spritedata/finalboss/fireball1.tgs.gen"
coloraddressbossfireball1:
include "spritedata/finalboss/fireball1.tcs.gen"
characteraddressbossfireball2:
include "spritedata/finalboss/fireball2.tgs.gen"
coloraddressbossfireball2:
include "spritedata/finalboss/fireball2.tcs.gen"
characteraddressbossfireball3:
include "spritedata/finalboss/fireball3.tgs.gen"
coloraddressbossfireball3:
include "spritedata/finalboss/fireball3.tcs.gen"


;bossfireballx
characteraddressbossfireballx:
include "spritedata/finalboss/fireballx.tgs.gen"
coloraddressbossfireballx:
include "spritedata/finalboss/fireballx.tcs.gen"
characteraddressbossfireballx1:
include "spritedata/finalboss/fireballx1.tgs.gen"
coloraddressbossfireballx1:
include "spritedata/finalboss/fireballx1.tcs.gen"
characteraddressbossfireballx2:
include "spritedata/finalboss/fireballx2.tgs.gen"
coloraddressbossfireballx2:
include "spritedata/finalboss/fireballx2.tcs.gen"
characteraddressbossfireballx3:
include "spritedata/finalboss/fireballx3.tgs.gen"
coloraddressbossfireballx3:
include "spritedata/finalboss/fireballx3.tcs.gen"
characteraddressbossfireballx4:
include "spritedata/finalboss/fireballx4.tgs.gen"
coloraddressbossfireballx4:
include "spritedata/finalboss/fireballx4.tcs.gen"
characteraddressbossfireballx5:
include "spritedata/finalboss/fireballx5.tgs.gen"
coloraddressbossfireballx5:
include "spritedata/finalboss/fireballx5.tcs.gen"



;bossfloat
characteraddressbossfloat:
include "spritedata/finalboss/bossfloat.tgs.gen"
coloraddressbossfloat:
include "spritedata/finalboss/bossfloat.tcs.gen"
characteraddressbossfloat1:
include "spritedata/finalboss/bossfloat1.tgs.gen"
coloraddressbossfloat1:
include "spritedata/finalboss/bossfloat1.tcs.gen"


;firebasllsheer
characteraddressfireballsheer:
include "spritedata/finalboss/fireballsheer.tgs.gen"
coloraddressfireballsheer:
include "spritedata/finalboss/fireballsheer.tcs.gen"
characteraddressfireballsheer1:
include "spritedata/finalboss/fireballsheer1.tgs.gen"
coloraddressfireballsheer1:
include "spritedata/finalboss/fireballsheer1.tcs.gen"


;princess
characteraddressprincess:
include "spritedata/finalboss/princess.tgs.gen"
coloraddressprincess:
include "spritedata/finalboss/princess.tcs.gen"


;thankyou
characteraddressthankyou:
include "spritedata/finalboss/thankyou.tgs.gen"
coloraddressthankyou:
include "spritedata/finalboss/thankyou.tcs.gen"

;princesssitting
characteraddressprincesssitting:
include "../grapx/cutscenes/final/princesssitting.tgs.gen"
coloraddressprincesssitting:
include "../grapx/cutscenes/final/princesssitting.tcs.gen"
characteraddressprincesssitting1:
include "../grapx/cutscenes/final/princesssitting1.tgs.gen"
coloraddressprincesssitting1:
include "../grapx/cutscenes/final/princesssitting1.tcs.gen"
characteraddressprincesssitting2:
include "../grapx/cutscenes/final/princesssitting2.tgs.gen"
coloraddressprincesssitting2:
include "../grapx/cutscenes/final/princesssitting2.tcs.gen"
characteraddressprincesssitting3:
include "../grapx/cutscenes/final/princesssitting3.tgs.gen"
coloraddressprincesssitting3:
include "../grapx/cutscenes/final/princesssitting3.tcs.gen"

;msxlogo
characteraddressmsxlogo:
include "spritedata/sprites level9-02a/msxlogo.tgs.gen"
coloraddressmsxlogo:
include "spritedata/sprites level9-02a/msxlogo.tcs.gen"

;bart
characteraddressbart:
include "spritedata/sprites level9-03b/bart.tgs.gen"
coloraddressbart:
include "spritedata/sprites level9-03b/bart.tcs.gen"

;bartsmoke
characteraddressbartsmoke:
include "spritedata/sprites level9-03b/bartsmoke.tgs.gen"
coloraddressbartsmoke:
include "spritedata/sprites level9-03b/bartsmoke.tcs.gen"
characteraddressbartsmoke1:
include "spritedata/sprites level9-03b/bartsmoke1.tgs.gen"
coloraddressbartsmoke1:
include "spritedata/sprites level9-03b/bartsmoke1.tcs.gen"
characteraddressbartsmoke2:
include "spritedata/sprites level9-03b/bartsmoke2.tgs.gen"
coloraddressbartsmoke2:
include "spritedata/sprites level9-03b/bartsmoke2.tcs.gen"
characteraddressbartsmoke3:
include "spritedata/sprites level9-03b/bartsmoke3.tgs.gen"
coloraddressbartsmoke3:
include "spritedata/sprites level9-03b/bartsmoke3.tcs.gen"

;bartdialogue
characteraddressbartdialogue:
include "spritedata/sprites level9-03b/bartdialogue.tgs.gen"
coloraddressbartdialogue:
include "spritedata/sprites level9-03b/bartdialogue.tcs.gen"
characteraddressbartdialogue1:
include "spritedata/sprites level9-03b/bartdialogue1.tgs.gen"
coloraddressbartdialogue1:
include "spritedata/sprites level9-03b/bartdialogue1.tcs.gen"
characteraddressbartdialogue2:
include "spritedata/sprites level9-03b/bartdialogue2.tgs.gen"
coloraddressbartdialogue2:
include "spritedata/sprites level9-03b/bartdialogue2.tcs.gen"
characteraddressbartdialogue3:
include "spritedata/sprites level9-03b/bartdialogue3.tgs.gen"
coloraddressbartdialogue3:
include "spritedata/sprites level9-03b/bartdialogue3.tcs.gen"
characteraddressbartdialogue4:
include "spritedata/sprites level9-03b/bartdialogue4.tgs.gen"
coloraddressbartdialogue4:
include "spritedata/sprites level9-03b/bartdialogue4.tcs.gen"

;superlaser
characteraddresssuperlaser:
include "spritedata/sprites level9-03b/superlaser.tgs.gen"
coloraddresssuperlaser:
include "spritedata/sprites level9-03b/superlaser.tcs.gen"


;laserbeam
characteraddresslaserbeam:
include "spritedata/sprites level9-03b/laserbeam.tgs.gen"
coloraddresslaserbeam:
include "spritedata/sprites level9-03b/laserbeam.tcs.gen"
characteraddresslaserbeam1:
include "spritedata/sprites level9-03b/laserbeam1.tgs.gen"
coloraddresslaserbeam1:
include "spritedata/sprites level9-03b/laserbeam1.tcs.gen"
characteraddresslaserbeam2:
include "spritedata/sprites level9-03b/laserbeam2.tgs.gen"
coloraddresslaserbeam2:
include "spritedata/sprites level9-03b/laserbeam2.tcs.gen"
characteraddresslaserbeam3:
include "spritedata/sprites level9-03b/laserbeam3.tgs.gen"
coloraddresslaserbeam3:
include "spritedata/sprites level9-03b/laserbeam3.tcs.gen"
characteraddresslaserbeam4:
include "spritedata/sprites level9-03b/laserbeam4.tgs.gen"
coloraddresslaserbeam4:
include "spritedata/sprites level9-03b/laserbeam4.tcs.gen"
characteraddresslaserbeam5:
include "spritedata/sprites level9-03b/laserbeam5.tgs.gen"
coloraddresslaserbeam5:
include "spritedata/sprites level9-03b/laserbeam5.tcs.gen"
characteraddresslaserbeam6:
include "spritedata/sprites level9-03b/laserbeam6.tgs.gen"
coloraddresslaserbeam6:
include "spritedata/sprites level9-03b/laserbeam6.tcs.gen"
characteraddresslaserbeam7:
include "spritedata/sprites level9-03b/laserbeam7.tgs.gen"
coloraddresslaserbeam7:
include "spritedata/sprites level9-03b/laserbeam7.tcs.gen"

;ghehehehehe
characteraddressbossdead:
include "spritedata/finalboss/bossdead.tgs.gen"
coloraddressbossdead:
include "spritedata/finalboss/bossdead.tcs.gen"


;beta
characteraddressbeta:
include "spritedata/beta.tgs.gen"
coloraddressbeta:
include "spritedata/beta.tcs.gen"


;hiersprite





      ds                $c000-$,$ff
dephase


;
; block $DD
;
finalsongblock:                    equ $DD
phase   $8000
  db 0
  incbin "../music/JJKMSC3.BGM"
   
  
  
        ds                $a000-$,$ff
dephase



;
; block $DE - DF
;
cutscenedatablock5:                    equ $DE
phase   $8000

  include "cutscenes/cutscenedata5.asm"
        ds                $c000-$,$ff
dephase



;extra mapdata 13
;
; block $E0 - E1
;
phase   $8000


;special2
grpx172addr:	incbin "../grapx/world9/level9-02a/packedgrpx/grpx.pck" 
grpxblckl172: equ $E0 ;special2
;special3
grpx173addr:	incbin "../grapx/world9/level9-03a/packedgrpx/grpx.pck" 
grpxblckl173: equ $E0 ;special3

;castle3 cutscene grpx
grpx187addr:	incbin "../grapx/cutscenes/castle3/packedgrpx/grpx.pck" 
grpxblckl187: equ $E0 ;castle 3 cutscene

;hiermap


;special2a
backgrl183: incbin "../level_editor/_levels/maplevel9-02a.stg.pck" 
blockbackgrl183: equ $E0 ;special2a
;special3a
backgrl184: incbin "../level_editor/_levels/maplevel9-03a.stg.pck" 
blockbackgrl184: equ $E0 ;special3a
backgrl185: incbin "../level_editor/_levels/maplevel9-03b.stg.pck" 
blockbackgrl185: equ $E0 ;special3b
;special2b
backgrl186: incbin "../level_editor/_levels/maplevel9-02b.stg.pck" 
blockbackgrl186: equ $E0 ;special2b

backgrl187: incbin "../level_editor/_levels/castle3.stg.pck" 
blockbackgrl187: equ $E0 ;castle 3 cutscene



        ds                $c000-$,$ff
dephase


;
; block $E2 - E3
;
halloffameblock:                    equ $E2
phase   $4000

halloffameaddress:
    incbin "../grapx/world9/level9-02b/halloffame.SC5",$0000+7,10240
halloffamepaletteaddress:    
    incbin "../grapx/world9/level9-02b/halloffame.SC5",$7687,32
        
    include "specialinthandler.asm"
       
    
        ds                $8000-$,$ff
dephase



;WARNING: THIS BLOCK MUST ALWAYS BE AT THE END OF THE LIBRARY!!!!
;
; block $E4
;
SCCreplayerblock:               equ $E4
phase   $4000



  include "sccreplayer/SCCreplayer.asm"
  
  ;player defines its own space
 ; ds                $a000-$,$ff
dephase


;E6 is the absolute limit





; forg $100000
totallenght:	equ	$-SupermarioWorld

    ds $100000 - totallenght ;fill er up to the end


;definition of our music files
;	ds		$80000-totallenght

;  incbin "../music/overworl.tmf"
;  incbin "../music/athletic.tmf"
;  incbin "../music/boss.tmf"
;  incbin "../music/vanilla.tmf"
;  incbin "../music/begin.tmf"
;  incbin "../music/levelcle.tmf"
;  incbin "../music/mariodie.tmf"
;  incbin "../music/bowserva.tmf"
;  incbin "../music/worldmap.tmf"
;  incbin "../music/cave.tmf"
;  incbin "../music/illusion.tmf"
;  incbin "../music/title.tmf"
;  incbin "../music/yoisland.tmf"
;  incbin "../music/gradius.tmf"
;  incbin "../music/invincib.tmf"
;  incbin "../music/pswitch.tmf"
