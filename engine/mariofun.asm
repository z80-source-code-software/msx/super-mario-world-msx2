mariofun:

  ld hl,(idlecounter)
  ld de,0
  xor a
  sbc hl,de
  ret nz

  ld a,(standingonsprite)
  and a
  ret nz

  ld a,(waterlevel)
  and a
  ret nz

  ld a,(marioyoshi)
  and a
  ret nz ;not allowed on yoshi
 
  ;give mario the shell status
  ld a,1
  ld (marioshell),a
  ld (funloop),a ;mario is having fun
 
  ld a,(funloop)
  and a
  ret z
 
 
  ;reset the idlecounter
  ld hl,1000
  ld (idlecounter),hl

  ld a,r
  bit 0,a
  jp nz,.spawnballoon


 
  ;spawn a mushroom in marios position and let this object handle the fun part
 

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec l
;  dec l
;  dec l
  inc hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de

;create the mushroom

  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back if possible
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),14               ;set object number (red mushroom)
  ld    (iy+6),0               ;set object number (red mushroom)

  call  activateobjectfrompagenul

  ld (ix+movepat),62
  ld (ix+deadly),0
  ld (ix+clipping),0
  ld (ix+deathtimer),0

  ld a,(marioleftrightspr)
  and a
  jp z,.makeleft
  
  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld (ix+v1),5

  pop ix

  ret 


.makeleft:

  ld hl,(mariox)
  ld de,11
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld (ix+v1),-5

  pop ix
  
  ret  

.spawnballoon:

  ;spawn a balloon in marios position and let this object handle the fun part
 

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec l
;  dec l
;  dec l
  inc hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
;  inc e
;  inc e

;create the balloon

  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back if possible
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),52               ;set object number (red mushroom)
  ld    (iy+6),0               ;set object number (red mushroom)

  call  activateobjectfrompagenul

;  ld (ix+movepat),62
;  ld (ix+deadly),0
;  ld (ix+clipping),0
;  ld (ix+deathtimer),0

  ld a,(marioleftrightspr)
  and a
  jp z,.makeleftballoon
  
  ld hl,(mariox)
  ld de,11
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  pop ix

  ret 


.makeleftballoon:

  ld hl,(mariox)
  ld de,4
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  pop ix
  
  ret  
