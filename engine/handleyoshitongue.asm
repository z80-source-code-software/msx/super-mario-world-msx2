;this routine handles the yoshi tongue. It runs from the engine and checks the position of the "air flow" that yoshi creates when sucking up opponents.
;It works in the following steps.
;1. it checks if mario is on yoshi and if the marioshot timer > 0 it returns is any of the two values are 0
;2. It creates a search point based on the position of the yoshihead. it keeps adding values according to a table that uses marioshot as the pointer
;3. It compares its position using objectinteract
;4. If a interact occurs the objects movementpattern is set to 0 and the routine adjust the objects coordinates until the marioshot timer reaches 0
;5. after step 4 the object is being destroyed and certain values are read and compared to determine what yoshi does next.
;6. write any values required for yoshi to do the right things.

forceswallow: ;force yoshi to swallow the object holding in his mouth

  ld hl,(swallowcounter)
  ld de,0
  xor a
  sbc hl,de
  jp z,.noswallowcount
  dec hl
  ld (swallowcounter),hl
.noswallowcount:

  ld hl,(swallowcounter)
  ld de,0
  xor a
  sbc hl,de
  ret nz

  ld a,(marioshot)
  and a
  ret nz

  ld a,(yoshifilled)
  and a
  ret z


  ld a,1
  ld (marioshot),a


  ret



handleyoshitongue:


;do not handle anything if mario is not on yoshi
  ld a,(marioyoshi)
  and a
  ret z
 
  call forceswallow
  
  ld a,(marioshot) ;do not handle anything if no shot is given
  and a
  ret z
  cp 1 ;2 not really sure how this works out on a darky but we shall see
  call z,Playsfx.yoshitongue
  
  
  ld a,(marioturnyoshi)
  and a
  call nz,.resettonguehold


  ld a,(yoshifilled)
  cp 1
  jp z,handlefilled
  cp 2
  jp z,handlefilled2
  cp 3
  jp z,handlefilled3

;from here get the yoshiix always push the ix byte before getting these values

    push ix
    ld ix,(yoshiix)
    
;fetch the data from the table
   ld hl,yoshitongueposition ;set the pointer
   ld a,(marioshot) ;get the shottimervalue
   ld e,a
   ld d,0
   add hl,de ;set pointer correctly
   ld a,(hl)
   ld c,a
   ld b,0  ;put value in bc
 
 
  ld a,(marioleftrightspr)
  dec a
  jp z,.right
      

.left:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,bc ;position of the tongue
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)

  jp .donelr
    
;the tongue is on a different position so fetch these values and write them into a virtual ix value. This step is VERY IMPORTANT!  

.right:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  add hl,bc ;position of the tongue
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)


.donelr:  


;write these values back into the fake objects list
  ld ix,fakeobjectslist  
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  push de
  pop hl
  ld de,20
  
  ld a,(marioduck)
  and a
  call nz,.settonguelower
  
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

;IMPORTANT!: from here current ix value is that of the fakeix list

;do not allow the tongue to pass through solid objects
  push bc
  push hl
  push de
  ld bc,0
  ld    de,0                  ;horizontal increase to tilecheck
  call  yoshitonguetilecheck
  cp 191 
  pop de
  pop hl
  pop bc
  pop hl ;this is the ix value which remains on the stack
  jp nc,.resettonguehold

  push hl

  ld a,(tonguehold)
  dec a
  jp z,grabobject.hold


  call yoshiobjectinteract
  jp c,grabobject


  pop ix
  
  ret


.resettonguehold:

  xor a
  ld (tonguehold),a
  ret

.settonguelower:
  
   ld de,12
   ret



grabobject:

;the cycle has been finished time to eat it!
  ld a,(marioshot)
  cp 14
  jp z,absorbobject

  ld a,1
  ld (tonguehold),a ;block any other objects to be taken
  ld (tongueiy),iy
  
.hold:
 
  ld iy,(tongueiy)
 
 ;the cycle has been finished time to eat it!
  ld a,(marioshot)
  cp 14
  jp z,absorbobject
    


;let the object follow the tongue
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ld a,(iy+movepat)
  cp 135
  jp z,.treatspecial


  pop ix
  
  ret

.treatspecial:

  ld (iy+v7),1

  pop ix
  
  ret

destroyobjects:

  ld a,(iy+edible)
  cp 4
  jp z,destroyobject.objectreturns ;pbox returns on map

    jp destroyobject



absorbobject:

  xor a
  ld (tonguehold),a

;write the objectstat
  ld a,(iy+objectrelease)
  ld (objectstat),a
;  ld b,(iy+objectrelease)
  ld b,a      
  
  ;we only do shells and koopas so check for edible byte
  ld a,(iy+edible)
  cp 5
  jp nz,.noset

.set:


  ld a,b
  
  cp 4
  call z,.setflying
  cp 5
  call z,.setall
  cp 2
  call z,.setstomp
  ld a,(iy+movepat)
  cp 195 ;exception is a rainbow shell
  call z,.setall
  
 ;check for permayoshi byte then directly set
  ld a,(yoshitype)
  cp 2
  call z,.setstomp
  cp 3
  call z,.setflying
  cp 4
  call z,.setflying  
  
.noset:
  
  push iy
  pop ix ;temp write the objects value into ix
  
  push bc
  
  call destroyobjects ;remove the object from screen

  pop bc

  push ix
  pop iy

;do all the amazing stuff from here

  ld a,(iy+movepat) ;read the object parameters and jump to the correct calls
  cp 20
  jp z,absorbextralife
  ld a,(iy+edible)
  cp 5
  jp z,absorbshell



  ld a,(iy+edible)
  cp 3
  jp z,absorbpowerup
  cp 4
  jp z,absorbpbox


  pop ix

  ld a,1
  ld (yoshiswallow),a ;set this value to be handled by the handleobjectmovement for yoshi head
  
  ret


.setstomp:

  ld a,(iy+movepat)
  cp 1
  jp z,.setstompdo
  cp 73
  jp z,.setstompdo

  ret

.setstompdo:

  ld a,1
  ld (yoshistomp),a
  ret

.setflying:

  ld a,(iy+movepat)
  cp 1
  jp z,.setflyingdo
  cp 73
  jp z,.setflyingdo

  ret

.setflyingdo:

  ld a,1
  ld (yoshiflying),a
  ret


.setall:

  ld a,1
  ld (yoshiflying),a
  ld (yoshistomp),a
  ld (objectstat),a ;force firespit type
  ret 
  

absorbpbox:

  ld hl,swallowobjecttime
  ld (swallowcounter),hl


  ld a,2
  ld (yoshifilled),a

  ld a,(objectstat)
  dec a
  call z,.set3
  
  
  pop ix

  
  ret

.set3:

  ld a,3
  ld (yoshifilled),a

  ret


absorbshell:
  
  ld hl,swallowobjecttime
  ld (swallowcounter),hl
  
  
  
  ld a,1
  ld (yoshifilled),a


  
  pop ix

;  ld a,1
;  ld (yoshiswallow),a ;set this value to be handled by the handleobjectmovement for yoshi head
  
  ret



absorbpowerup:


  call settransform

  pop ix

  ld a,1
  ld (yoshiswallow),a ;set this value to be handled by the handleobjectmovement for yoshi head
  
  ret



absorbextralife:


  call setextralife
  
  pop ix

  ld a,1
  ld (yoshiswallow),a ;set this value to be handled by the handleobjectmovement for yoshi head
  
  ret


handlefilled:


  ;no spit out during turning around
  ld a,(marioturnyoshi)
  and a
  ret nz

  xor a
  ld (yoshifilled),a
  ld (yoshiflying),a
  ld (yoshistomp),a

;disable marioshot 

  ld a,15
  ld (marioshot),a

;do not do anything else if the timer was exceeded  
  ld hl,(swallowcounter)
  ld de,0
  xor a
  sbc hl,de
  jp z,.swallowobject

  ld a,(yoshitype) ;if type is red yoshi
  dec a
  jp z,handleyoshifirespit

  ld a,(objectstat)
  cp 2 ;0 or 1 
  jp c,handleyoshifirespit
  cp 5
  jp z,handleyoshifirespit

  call Playsfx.yoshispit


  ld a,(marioleftrightspr)
  dec a
  jp z,.right
  

.left:  

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  dec hl
  dec hl
  dec hl
  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),01               ;set object number (01 = pink shell)
  ld    (iy+6),0               ;set object number (01 = pink shell)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),2
  ld (ix+movepat),7
  ld (ix+v1),-horshellspeed ;trow the shell out at the default speed
  ld a,(objectstat)
  ld (ix+objectrelease),a
  ld (ix+deadly),1
  ld (ix+edible),0

  call animateshell

  pop ix


  ret


.swallowobject:
  
  ld a,1
  ld (yoshiswallow),a
  
;  playsfx.yoshiswallow

  ret

  
.right:  

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  inc hl
  inc hl
  inc hl
  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),01               ;set object number (01 = pink shell)
  ld    (iy+6),0               ;set object number (01 = pink shell)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),2
  ld (ix+movepat),7
  ld (ix+v1),horshellspeed ;trow the shell out at the default speed
  ld a,(objectstat)
  ld (ix+objectrelease),a
  ld (ix+deadly),1
  ld (ix+edible),0

  call animateshell

  pop ix


  ret



handleyoshifirespit:

  call Playsfx.yoshifireball


  ld a,(marioleftrightspr)
  dec a
  jp z,.right
  

.left:  

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  dec hl
  dec hl
  dec hl

  ld (hlvar),hl
  ld (bcvar),de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject


  ld (ix+v1),-horflamespeed ;trow the shell out at the default speed


  pop ix

;make object 2

  ld hl,(hlvar)
  ld de,(bcvar)


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject
  

  ld (ix+v1),-horflamespeed ;trow the shell out at the default speed
  ld (ix+v2),1 ;trow the shell out at the default speed


  pop ix

;make flame 3  

  ld hl,(hlvar)
  ld de,(bcvar)


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject
  

  ld (ix+v1),-horflamespeed ;trow the shell out at the default speed
  ld (ix+v2),-1 ;trow the shell out at the default speed


  pop ix


  ret

  


  
.right:  

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  inc hl
  inc hl
  inc hl
 
 
  ld (hlvar),hl
  ld (bcvar),de
  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject
  

  ld (ix+v1),horflamespeed ;trow the shell out at the default speed


  pop ix

;make object 2

  ld hl,(hlvar)
  ld de,(bcvar)


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject
  

  ld (ix+v1),horflamespeed ;trow the shell out at the default speed
  ld (ix+v2),1 ;trow the shell out at the default speed


  pop ix

;make flame 3  

  ld hl,(hlvar)
  ld de,(bcvar)


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),82               ;set object number
  ld    (iy+6),0               ;set object number

  call  activateobject
  

  ld (ix+v1),horflamespeed ;trow the shell out at the default speed
  ld (ix+v2),-1 ;trow the shell out at the default speed


  pop ix

  ret

handlefilled3:

  ld c,77
  push bc
  jp handlefilled2.ready

handlefilled2:

  ld c,76
  push bc
;  jp handlefilled2.ready


.ready:

  call Playsfx.yoshispit


  ;no spit out during turning around
  ld a,(marioturnyoshi)
  and a
  pop bc ;bug
  ret nz
  
  push bc ;bug
  
  
  xor a
  ld (yoshifilled),a
  ld (yoshiflying),a

;disable marioshot 

  ld a,15
  ld (marioshot),a


  ld a,(marioleftrightspr)
  dec a
  jp z,.right
  

.left:  

  pop bc

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  dec hl
  dec hl
  dec hl
  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),c;76               ;set object number (01 = pink shell)
  ld    (iy+6),0               ;set object number (01 = pink shell)

  call  activateobject

  ld (ix+movepat),99
  ld (ix+deathtimer),0
  ld (ix+v1),-horshellspeed
  ld (ix+objectfp),0
  ld (ix+v4),0
    ld (ix+v3),0


  pop ix


  ret


  
.right:  

  pop bc

;prepare and spawn the object that needs to be trown out. green shell only right now. Use the filled counter as reference later

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;dec e
  inc hl
  inc hl
  inc hl
  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),c;76               ;set object number (01 = pink shell)
  ld    (iy+6),0               ;set object number (01 = pink shell)

  call  activateobject

  ld (ix+movepat),99
  ld (ix+deathtimer),0
  ld (ix+v1),horshellspeed
  ld (ix+objectfp),0
  ld (ix+v4),0
    ld (ix+v3),1


  pop ix


  ret


  





yoshitongueposition: db 46,46,46,46,46,46,46,46,42,38,34,30,26,22,16

horflamespeed: equ 6


;wanneer twee objecten elkaar raken moeten ze van alle andere actieve objecten checken of er iets moet gebeuren
;in = de ix van het object dat wilt weten waar we zijn
;out = carry flag als er colission is en A = 1 als dat het enige is en A = 2 als het object erdoor sterft
;OUT in B = de snelheid van het object dat is geraakt
yoshiobjectinteract:

  ld (currentobjectvalue),ix

;alle XY waarden die het huidige object bezitten saven voordat we de loop in gaan
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld (currentx),hl
  ld (currenty),de

;zodra we een object vinden handelen we hem af en returnen we met een carry flag
  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,handleyoshiobject : jp c,.end ;objectnr 20

;no object found return this
  xor a
  ld ix,(currentobjectvalue)

  ret


.end:

  
  ld a,(ix+clipping) ;gaan we echt dood?
  ld b,(ix+v1)
  
  
  push ix
  pop iy ;save the ix value for interaction reference by the routine that called this
;adress terugschrijven
  ld ix,(currentobjectvalue)
  scf

  ret



handleyoshiobject:

;see if the object can be eaten
  ld a,(ix+edible)
  and a
  ret z

;ben ik het zelf
  ld hl,(currentobjectvalue)
;  ld (ixtransfer),ix
;  ld de,(ixtransfer)
  push ix
  pop de
  xor a
  sbc hl,de
  jp z,.endcheck


  ;check if other object is within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-16;-20
  add hl,de
  
  ld de,(currentx)
  or  a  
  sbc hl,de
  
;left border not found, no collision
  jp nc,.endcheck

  ld  a,(ix+sizeX)
  add a,15;22
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;right border not found, no collision
   jp nc,.endcheck
  ;/check if other object is within x range of object

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-22;-28
  add hl,de
 
  ld de,(currenty)
  or  a  
  sbc hl,de
  
;top border not found, no collision
  jp nc,.endcheck

  ld  a,(ix+sizeY)
  add a,32;;15
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;bottom border not found, no collision
   jp nc,.endcheck
  ;/check if other object is  within y range of object
  
.endcheck:

  ret

swallowobjecttime:  equ 300
