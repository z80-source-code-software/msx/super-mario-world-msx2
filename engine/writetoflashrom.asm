
;V write data to block
writesramuserdata:
 ; di

  
  ;new problem. We don't have the space nor luxury to write to such extremes on the flashrom thus we MUST use F8, F9 and FA. The rest is cleared
  ;so before we call a erase we must save the data of the other blocks first and write them back after erase. We will have to change the erase code
    ld a,$F8;lowest possible block we can erase
    call flashblock
    ;save all data first
    ld de,Objectlist
    ld hl,$4000
    ld bc,endglobalvars+1
    ldir
    ld (hlvar),de ;save adress for block 2
    ld a,$F9
    call flashblock ;next block to save
    ld hl,$4000
    ld bc,endglobalvars+1
    ldir
    ld (devar),de ;save adress for block 3
    ld a,$FA
    call flashblock ;next block to save
    ld hl,$4000
    ld bc,endglobalvars+1
    ldir ;done saving all data    
      
    ld a,$F8
    call flashblock
    call eraseflashrom  ;erase flashrom first

  
  ld a,(srampointer)
  call flashblock

  
  ld a,5
  ld c,a
  ld hl,$4000
  call writetoflashrom ;set first byte
  
  
  ld hl,globalvars
  ld de,$4001
  ld bc,endglobalvars
  call ldirtoflashrom

  
;get other data back and write to flashrom
    ld a,(srampointer) ;which ones to fix?
    cp $F8
    jp z,.fixblock23
    cp $F9
    jp z,.fixblock13
    cp $FA
    jp z,.fixblock12
  
  ;code below not needed but in case of some stupid bug it will prevent a complete system hang catastrophy
  
  ld a,savemenublock
  call block12 ;restore rom block
  
  ret

 
.fixblock23:

    ld a,$F9
    call flashblock
    ld de,$4000
    ld hl,(hlvar)
    ld bc,endglobalvars+1
    call ldirtoflashrom
    
    ld a,$FA
    call flashblock
    ld de,$4000
    ld hl,(devar)
    ld bc,endglobalvars+1    
    call ldirtoflashrom    

  ld a,savemenublock
  call block12 ;restore rom block

  ret


.fixblock13:   

    ld a,$F8
    call flashblock
    ld de,$4000
    ld hl,Objectlist
    ld bc,endglobalvars+1
    call ldirtoflashrom
    
    ld a,$FA
    call flashblock
    ld de,$4000
    ld hl,(devar)
    ld bc,endglobalvars+1    
    call ldirtoflashrom    

  ld a,savemenublock
  call block12 ;restore rom block

  ret

.fixblock12: 

    ld a,$F8
    call flashblock
    ld de,$4000
    ld hl,Objectlist
    ld bc,endglobalvars+1
    call ldirtoflashrom
    
    ld a,$F9
    call flashblock
    ld de,$4000
    ld hl,(hlvar)
    ld bc,endglobalvars+1    
    call ldirtoflashrom    

  ld a,savemenublock
  call block12 ;restore rom block

  ret

  
  
  
;V reads data from block
readsramuserdata:

  di
  
  ld a,(srampointer)
  call flashblock
  
 
  
  ld a,($4000)
  cp 5
  jp nz,.skipread
  
  
  ld hl,$4001
  ld de,globalvars
  ld bc,endglobalvars
  ldir

.skipread:


  ei

  ret

;V write 0 to first part of block
erasesramdata:

  di
    
  ld a,(srampointer)
  call flashblock
  
  xor a
  ld c,a
  ld hl,$4000
  call writetoflashrom ;no block erase is needed because you can always write in a zero

  ld a,demodatablock
  call block12 ;restore call  
  
;  ei


  ret

;V read this info from the flashrom
readlevelprogress:

  di

  ld a,$F8 ;pointer 1
  call flashblock
  
  ld hl,$4000
  ld a,(hl)
  cp 5
  ld a,0
  jp nz,.next ;no save data in here
  
  inc hl
;  ld hl,$4000+1
  ld de,levelavailabilitylistposition
  add hl,de

  call .countlevels
  sub 3
;  dec a
;  dec a ;-2
.next:
  ld (levelprogress),a
  
  ld a,$F9 ;pointer 2
  call flashblock
  
  ld hl,$4000
  ld a,(hl)
  cp 5
  ld a,0
  jp nz,.next2 ;no save data in here
  
  inc hl
;  ld de,endglobalvars+1
;  add hl,de
  ld de,levelavailabilitylistposition
  add hl,de
  
  call .countlevels
  sub 3
.next2:
  ld (levelprogress+1),a ;position b
  
  ld a,$FA ;pointer 3
  call flashblock
  
  ld hl,$4000
  ld a,(hl)
  cp 5
  ld a,0
  jp nz,.next3 ;no save data in here
  
  inc hl
;  ld de,endglobalvars+1
;  add hl,de
;  add hl,de
  ld de,levelavailabilitylistposition
  add hl,de
  
  call .countlevels
  sub 3
.next3:
  ld (levelprogress+2),a  ;position c
  
  ld a,demodatablock
  call block12 ;restore call
  
  ei

  ret

  
.countlevels:

  ld b,10 ;bytes to read
  ld c,0 ;levels

.countlevelsloop:

  ld a,(hl)
  push bc
  call .countbits
  pop bc
  
  add a,c
  ld c,a
  
  inc hl ;next byte
  
  djnz .countlevelsloop
  
  ret
  
  
  
  
.countbits:

  ld b,8
  ld c,0
  
.countbitsloop:
  
  rra ;data in carry
  
  jp nc,.nobit
  
  inc c

.nobit:
  
  djnz .countbitsloop
  
  ld a,c
  
  ret






  

    
flashblock:    
    
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a  
	
	ret
