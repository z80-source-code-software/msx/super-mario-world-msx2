;trows the picture of sad mario on the screen. uses MSX1 BIOS functions
trowerrortext:
;switch otherbank
  ld		($5000),a

  xor a ;init with black color directly disable screen, this will make the rom start with a perfect black screen
	ld		($f3e9),a	  ;foreground color 
	ld		($f3ea),a	  ;background color 
	ld		($f3eb),a	  ;border color

  ld a,2
  call $5f ;switch to screen2, clears the screen

;write the character and color tables 4x to Vram
  ;1st character pattern table at ($0000)
	xor		a
	ld		hl,$0000                                          ;character pattern table address in Vram
	call	.SetVdp_Write
	ld		hl,$4000	                                    ;character pattern table in Rom
;	ld		bc,$0098
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;MSX1 is too slow for this otir operation so we need to do it with a loop...
  ld de,$1800
.charloop:
  ld a,(hl)
  out ($98),a
  inc hl
  dec de
  ld a,d
  or e
  jp nz,.charloop


  ;1st color table at ($2000)
	xor		a
	ld		hl,$2000                                          ;color table address in Vram
	call	.SetVdp_Write
	ld		hl,$6000	                                    ;color table table in Rom
;	ld		bc,$0098
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;MSX1 is too slow for this otir operation so we need to do it with a loop...
  ld de,$1800
.colorloop:
  ld a,(hl)
  out ($98),a
  inc hl
  dec de
  ld a,d
  or e
  jp nz,.colorloop


.hang:
  jp .hang


.SetVdp_Write:

;first set register 14 (actually this only needs to be done once
	rlc     h
	rla
	rlc     h
	rla
	srl     h
	srl     h
	di
	out     ($99),a       ;set bits 15-17
	ld      a,14+128
	out     ($99),a
;/first set register 14 (actually this only needs to be done once

	ld      a,l           ;set bits 0-7
	nop
	out     ($99),a
	ld      a,h           ;set bits 8-14
	or      64            ; + write access
	ei
	out     ($99),a       
	ret