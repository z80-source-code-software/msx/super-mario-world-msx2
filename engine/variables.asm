variablespage0:

;alle snelheidstabellen en waarden
staticvars: ;all the preset static variables

mariodspeedtable:     db 1,1,1,1,1,2,2,3 ;oplopende snelheid vanaf 1 wanneer mario nog stilstaat
mariospeedtabler:     db 4,4,4,4,4,4,4,4,4,4,5,4,5,4,5,5,5,5,5,5,5,5,5,5,6,5,6,5,6,5,6,6,7,8 ;van een ramp afglijden
mariospeedtable:      db 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,6 ;Versnelling van mario zodra je begint te rennen
marioslowdownff:      db 5,4,4,4,4,4,4,4,4,3,4,3,4,3,3,3,3,2,3,2,2,2,1,1,1,1,1,0 ;wanneer mario de grond raakt na ver vliegen deelt de pointer met bukken
marioslowdownr:       db 5,4,4,4,4,4,4,3,4,3,4,3,2,3,2,2,1,1,1,0 ;slowdown tabel wanneer mario op volle vaart rent
marioslowdownfr:      db 5,5,5,5,5,4,5,4,5,4,4,4,4,4,4,3 ;wanneer mario van rennen naar lopen gaat
marioslowdown:        db 3,2,1,0 ;vertraging na stoppen met lopen

marioswimtable:       db 1,1,1,1,1,2,1,2,1,2 ;oplopende snelheid vanaf 1 wanneer mario nog niet zwemt

fallspeedtablesp:     db 1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,3,3,3,3,3,4,4,4,4,5,4,5,4,5,4,5,4,5,4,5,5,5,5,5,6 ;speciale falltable voor het vliegen
fallspeedtablespd:    db 3,3,4,3,4,4,4,4,5,5,5,5,6,6,7 ;speciale falltable voor het droppen 
fallspeedtable:       db 0,1,2,4,5,6,7,8,9,10
marioswimdown:        db 0,1,0,1,0,1,1,1,2,1,2,3,2,3,2,3,4,5 ;waneer mario naar beneden zakt in het water 5 is overflow bescherming


mariojumpup:          db 12,10,9,8,7,6,4,3,2,1,1,1,0,0,0,0 ;Jump up normal
                      db 10,8,7,6,6,5,4,3,2,1,1,1,0,0,0,0 ;spinjump ;15
                      
mariorjumpup:         db 20,12,9,8,7,6,5,3,2,2,2,1,0,0,0,0 ;wanneer mario rent
                      db 12,9,8,7,6,5,4,3,2,1,1,1,0,0,0,0 ;spinjump ;15

mariorrjumpup:        db 20,12,10,8,7,6,5,4,3,2,2,2,2,1,1,0 ;wanneer mario rent at full speed!
                      db 15,10,9,7,6,5,4,3,2,2,2,2,1,1,0,0 ;spinjump
                      
mariojumpupre:        db 6,5,5,4,4,2,2,2,1,1,1,1,1,0,0,0 ;mario normal jump keyrelease
                      db 6,5,4,4,3,2,1,2,1,1,1,1,1,0,0,0 ;spinjump
                      
mariorjumpupre:       db 7,6,5,4,2,1,1,1,0,0,0,0,0,0,0,0 ;mario running jump keyrelease
                      db 6,5,4,3,2,1,1,1,0,0,0,0,0,0,0,0 ;spinjump
                      
mariorrjumpupre:      db 8,6,5,4,2,1,1,1,0,0,0,0,0,0,0,0 ;mario maximum running jump keyrelease
                      db 7,6,5,3,2,1,1,1,0,0,0,0,0,0,0,0 ;spinjump

mariojumpupra:        db 4,2,2,1,1,0;2,1,0 ;rampjump wanneer mario een enemy op een ramp raakt in een slide

mariorrjumpupfs:      db 1,1,2,2,2,3,4,5,4,3,3,2,2,2,2,1,1,0 ;mario jumpup in flying special 
mariojumpupdrop:      db 1,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1,1,0 ;vanuit een fly drop

marioswimup:          db 5,4,2,2,1,1,1,0 ;tabel wanneer mario omhoog zwemt

sheerselecttable:     dw 0,sheertableslightleft,sheertableslightright,0,0,sheertablemediumleft,sheertablemediumright,sheertablemaxleft,sheertablemaxright,sheertablenormalleft,sheertablenormalright


;local variables these one get cleared at every map load
localvars:

;variables from the eventhandler
Objectlist:
;active?,lobjlad,v5,   y,x,sprlstadd,movepat,amountspr,updatespr?,coloraddress,spataddrss,v1,v2,v3,v4,   offsets (these are the y,x offsets per objectsprite)                                                        ,spritecolROM,spritecharROM,sizeX,sizeY,deadly,clipping,hitpoints,objectrelease,spritesblock,edible,deathtimer,objectfp,sheertile,killshell,objectreturn,counter, random, updateoddoreven?, clearvram, standingonspr, lockdata 
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00
lenghtobjectlist: equ $-Objectlist
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00 
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00 
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00 
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
dummyobjectlist: db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 00,   00,    00,   00,       00,       00,            00,       00,       00,       00,      00,       00,         00    : dw 00 :   db 00,        00,           00,           00  : dw    00  
  db 0  ;this list ends with a 0, which is used in overflowcheck in "search empty space in objectlst" routine
  ;note: the last entry is a bogus entry to which a call writes in case of a list overflow
fulllenghtobjectlist: equ $-Objectlist


levelObjectList:                ;available? (0=no, 1=yes), x object in tiles (16 bit), y object/8 (8 bit), object number
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     
lenghtlevelObjectList: equ $-levelObjectList
  db  00       :  dw  000 : dw 000,000 ;reserved empty space in ram to which the objectlist is being copied to
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;10
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;20
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;30
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;40
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;50
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;60
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;70
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;80
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000
  db  00       :  dw  000 : dw 000,000 ;90 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;91 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;92 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;93 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;94 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;95 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;96 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;97 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;98 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;99 ;yes there is a map with 90 objects....
  db  00       :  dw  000 : dw 000,000 ;100 ;yes there is a map with 100 objects....
fulllenghtlevelObjectList: equ $-levelObjectList


;levelbackgroundtileslist
levelbackgroundtileslist:        
;bkgadress ,tilenumber  1,   2, 3,  4   
    dw 000  :   db      00, 00, 00, 00
lenghtlevelbackgroundtileslist: equ $-levelbackgroundtileslist
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00
    dw 000  :   db      00, 00, 00, 00            
    dw 000  :   db      00, 00, 00, 00 ;20   
fulllenghtlevelbackgroundtileslist: equ $-levelbackgroundtileslist

maxbkgbuffer:   equ fulllenghtlevelbackgroundtileslist 
bkgbufferrows:  equ fulllenghtlevelbackgroundtileslist/lenghtlevelbackgroundtileslist
bkgbuffercollumns:  equ fulllenghtlevelbackgroundtileslist/bkgbufferrows

;all pointers
mariospeedtablerp:    db 0 ;voor onderstaande tabel
mariodspeedtablep:    db 0
marioslowdownp:       db 0 ;pointer voor het slow down verhaal
marioswimtablep:      db 0
fallspeed:            db 0 ;Belangrijk. Deze variabele word gebruikt door de camera routine!
fallspeedtablep:      db 0
mariospeed:           db 0 ;snelheid van mario X is altijd 3 en max 5 bij rennen
marioswimupp:         db 0 ;pointer voor de jumpup routine zowel voor running als normal
jumpspeed:            db 0 ;snelheid waarmee mario jumpt deze waarde word doorgegeven aan de camera
mariojumpupp:         db 0 ;pointer voor de jumpup routine zowel voor running als normal
mariojumpuppd:        db 0 ;pointer voor de jumpupfromflydrop
marioswimdownp:       db 0
marioslowdownrdp:     db 0
marioslowdownrp:      db 0 
marioslowdownrpp:     db 0 ;tweede slowdown pointer om complicaties te vermijden
mariospeedtablep:     db 0 ;de pointer voor de running table
marioslowdowndp:      db 0 ;pointer voor het slow down verhaal


;alle sprite graphics gerelateerde variabelen
mariospinjumptable: db 0 ;Spinjump tellertje voor de sprite
mariowalktable:     db 0 ;oplopende waarde totdat een piek is bereikt en word dan weer gereset
marioruntable:      db 0 ;zelfde als walk maar dan voor rennen om te voorkomen dat van lopen naar rennen er bug uitziet
marioflickrtable:   db 0 ;pointer voor de knipperende sprite
marioleftrightspr:  db 0 ;1 = rechts; 0 = links ;voor de sprite engine de engine kopieert zijn waarde uit marioleftright
marioleftrightsprw: db 0 ;1 = rechts; 0 = links ;voor de sprite engine de engine kopieert zijn waarde uit marioleftright in water
mariosheerhold:     db 0 ;mario schoof van een tile af en dat word onthouden tot een bepaalde checkout.
holdspritedir:      db 0 ;de sprite engine negeert de richting van mario
marioturnshell:     db 0 ;het is een timer die loopt zodra mario zich omdraait met een schelpje de mariosprite routine handeld het verder af
marioturnyoshi:     db 0 ;het is een timer die loopt zodra mario zich omdraait op yoshi
turnshellactive:    db 0 ;hulpvariabele om te weten of de turnshell geactiveerd was
turnyoshiactive:    db 0 ;hulpvariabele om te weten of de turnyoshi geactiveerd was
mariocloud:         db 0 ;wanneer mario in een slide gaat komt er een wolkje 0 = niet actief overige waarden = wel actief



;alle statussen van mario
marionotdown:         db 0 ;vanaf hoogste punt naar beneden
mariospritenumber:    dw 0 ;Gaat in stappen van 192 dat houd in 128 voor de sprite zelf en 64 voor de kleur.
mariospritenumber2:   dw 0 ;Extra stukje mario word herkent in de putmariosprite en dan aan de sprite geplakt
mariospritenumber3:   dw 0 ;extra wolkje
mariorunning:         db 0 ;renne wij?? 0 = wel; 1 = niet ;ook op te merken dat dit alleen voor de sprite geld!
marioslide:           db 0 ;slide 0 = Niet; 1 = Wel 
mariolook:            db 0 ;kijken we naar boven
marioslowingdown:     db 0 ;Mario is nog bezig met afremmen vanuit de running zodra max speed is bereikt; 0 = niet; 1 = wel
;slowdownfromduck:     db 0 ;mario was aan het bukken maar en in een slowdown
mariospinjump:        db 0 ;Spinjump?; 0 = Nee; 1 = Ja
mariofloating:        db 0 ;is mario in de lucht?
mariofloatingpf:      db 0 ;was mario in de lucht in de vorige frame?
marioswim:            db 0 ;mario is in het water
mariojumpself:        db 0 ;mario is zijn jump aan het afmaken na keyrelease
mariopickupshell:     db 0 ;mario is een schelpje aan het oppakken
mariokickshell:       db 0 ;mario is een schelpje aan het wegtrappen
marioflying:          db 0 ;mario is aan het vliegen
marioclimbing:        db 0 ;mario is aan het klimmen
mariokillshell:       db 0 ;mario killt een shell of object met zijn capeje
mariostar:            db 0 ;mario heeft een sterretje niet gelijk aan nul is dodelijk
marioslowdownpointer: db 0 ;the new slowdown pointer which is used for all the slowdown routines
mariopeace:           db 0 ;mario is giving us the peace sign
marioduckinair:       db 0 ;this tells us if mario is in the air and still in duck
interactcounter:      db 0 ;counts after mario kicks someones ass


;alle speciale vars voor vliegende mario
marioflyspecial:    db 0 ;mario is aan het vliegen met jump toets vast
marioflyup:         db 0 ;mario zweeft omhoog na een successvolle keypress
marioflyhold:       db 0 ;mario zweeft omhoog na een successvolle keypress en is nog bezig met de flyup
marioflyslide:      db 0 ;mario komt neer op de grond met parachuutje en glijd helemaal door
marioflydrop:       db 0 ;mario is naar beneden aan het droppen
marioflyupfromdrop: db 0 ;mario maakt een jump vanuit een drop
jumpheightcounter:  dw 0 ;hoe hoog heeft mario al gejumpt? in 16 bits want we praten over 300+
keypresscounter:    db 0 ;hoe lang hebben we geen toetsen ingedrukt bij het droppen?

;alle speciale gebeurtenissen
mariospecialtile:   db 0 ; speciale mario tile is geraakt van onder denk aan de boxen met uitroeptekentjes
mariotransforming:  db 0 ;is mario aan het transformeren? 10 = sterven
mariotransformp:    db 0 ;mariotranformatiepointer
mariojumpkey:       db 0 ;een van beide jumpkeys ingedrukt?
mariojumprelease:   db 0 ;1 = toen de jump begon had je de jumpkey vast 0 = jumpkey is losgelaten ten tijde van de jump
tripscreen:         db 0 ;beeld shaked op en neer
mariosecondslide:   db 0 ;wanneer mario neerkomt vanuit een slide na vliegen moeten we weten of hij nog een tweede slide gaat doen
mariosheertile:     db 0 ;Schuine tile? 0 = nee, 1 = naar links, 2 = naar rechts
mariosheerslide:    db 0 ;mario schuift van een schuine tile af
mariosheerduck:     db 0 ;mario schuift van een schuine tile af terwijl hij bukt. mag pas resetten bij stilstand
slowdownfromrampend:  db 0 ;mario schuift op zijn kont van een ramp af
mariogod:           db 0 ;mario heeft godmode
rampjump:           db 0 ;mario heeft een beestje gesloopt en doet een heel kort jumpje 1=jumpje in zetten 2=jumpje word gemaakt. mariospeed ten alle tijden behouden
tempspeed:          db 0 ;tijdelijke opslag van mariospeed
movedownsheertile:  db 0 ;mario schuift naar beneden van een sheertile, handig voor enemy detects
funloop:            db 0 ;mario is having fun
marioduckonground:  db 0 ;mario is ducking on the ground not just in the air
watercurrent:       db 0 ;if set to 1 there is current in the water
marioforcewalk:     db 0 ;mario is forced to walk to the right
yoshiflying:        db 0 ;yoshi is flying. Blue shell
yoshistomp:         db 0 ;yoshi can stomp the ground. yellow shell
marioballoon:       dw 0 ;mario has changed into a balloon used as timer



;sprite related mario versus object handling bytes
standingonplatform: db 0 ;tells that mario is on a platform.
standingonsprite:   db 0 ;mario staat op een sprite, bijv een box. Dit betekend eigenlijk een universele collision. when bit 7 is set it means we are standing on a platform
jumpingupsprite:    db 0 ;mario jumpt tegen een sprite op. Dat word dus een bounce terug
touchside:          db 0 ;mario raakt de zijkant aan van een sprite 1=links, 2=rechts 
spriteixvalue:      dw 0 ;de ix value van de sprite waar mario op staat
specialsheertile:   db 0 ;we are standing on a special sheertile


;variabelen om de freaking hoekjes te checken en te securen
tiletypebelow:      db 0 ;sheertiletype 1
tiletypebelowtwo:   db 0 ;sheertiletype 2
tiletypeignore:     db 0 ;speciaal hoekje waar we doorheen mogen vallen
tilecorner:         db 0 ;1 = mario staat op een hoekje 0 = mario staat niet op een hoekje
tilecornertype:     db 0 ;1 = links 2 = rechts


;alle coordinaten gerelateerde zaken
mariocoordinates:   db 0,0 ;dit is nu gewoon de waarde van de mario spat. y x
;de coordinaten van mario op de map in 16bits!!

mariox:             dw 0 ;minimaal 128!! anders spawn je buiten de map!
marioy:             dw 0 ;ook minimaal 128 anders "" ;300
;positie van de camera in tiles van 8x8
camerax:            dw 0
cameray:            dw 0

;memory gelazer voor de putmariosprite et al.
blocknumber:        db 0 ;blocknummer waaruit de mario sprite word gelezen
jumpkeyhold:        db 0 ;een van de jumptoetsen word vastgehouden
sheerfalltimer:     db 0 ;wanneer mario van een sheerslide afkomt moeten we heel even wachten voordat we de
;lowerspat weer resetten
tilelocked:         db 0 ;is mario gelockt op een tile
;hlvar:              dw 0 ;hl eigen stackje
objectxy:           dw 0  ;objectxy temp stack used by lock object
nosprite:           db 0 ;mario sprite display inhibit
dieatonce:          db 0 ;mario will die at once. He will not shrink down
controlbuffer:      db 0 ;buffer Controls


;memory gelul
romprogblocknumber: db 0 ; het nummer waar we naartoe schakelen om de handleobjectmovement te lezen
tubecoloractive:    db 0 ;in welke kleur is een buis omgezet?
changetubecolor:    db 0 ;kleur werkelijk wegschrijven
idlecounter:        dw 0 ;count the time that mario is doing nothing, this will be fun ;)
vineactive:         db 0 ;a climbing plant has been released tile 252,253,254,255 should now be ignored by the sheertilechecker
bluepboxactive:     db 0 ;the blue p-box is active
pboxcounter:        dw 0 ;the pboxcounter which counts to 1:30 secondes?
previousmusicblock: db 0 ;temp musicblock stack
previousmusicblockp: db 0 ;temp musicblock stack for other purposes to prevent mixup with star etc..
switchmusicpforbox: db 0 ;switch the music for pbox from rom
shrinkingboxcounter:db 0 ;counter for the shrinkingsmiley boxes
shrinkingboxv4:     db 0 ;global v4 for all shrinking boxes


;pauze routine variablen
pauze:              db 0 ;spel op pauze??
setpauze:           db 0 ;p toets ingedrukt??


;schieten van mario
marioshoothold:     db 0 ;je houd de c toets ingedrukt je moet hem eerst loslaten voordat het volgende schot kan
marioshot:          db 0 ;op 1 betekend dat er geschoten is en dat moet worden afgehandeld
bulletsingame:      db 0 ;bullets in game. 2 = max


foregroundfoundleft:     db 0 ;er is een FG tile gevonden links van je terwijl je op de sheertile staat.
foregroundfoundright:    db 0 ;er is een FG tile gevonden rechts van je terwijl je op de sheertile staat.

uppersheercornerfound:  db 0 ;We found a upper sheercorner meaning we must start falling until we are on the ground


;geluiden en muziek
sfxplaying:         db 0 ;word er een geluidje afgespeeld??


;test en debug variabelen
cameratype:         db 0 ;save camera type
pauzetime:          db 0 ;save time for pauze reset
animatespecial:     db 0 ;is there a yellow pillar or similar object to be animated?

;engine control
levelend?:          db 0 ;if set to 1 the whole level will end
mapheight:          dw 0 ;the height of the map in tiles
mapheightx8:        dw 0 ;the height of the map in pixels
maplenghtx8:        dw 0 ;the lenght of the map in pixels
endlevelbywin:      db 0 ;mario has completed the map in the normal way
camreachedlimit:    db 0 ; 1 = utmost right of the map (camerascrollx) has been reached
playwinmusic:       db 0 ;name says enough
randomnumber:       db 0
handlecomplete:     db 0 ;set to 1 when an object movementpattern is finished
moonpoweruptaken:   db 0 ;if set to 1 it means that the moonpowerup has allready been taken
completed:          db 0 ;if set to 1 no powerups will spawn on this map
middlereached:      db 0 ;if set to 1 mario will start on alternate starting coordinates on the map
maxwaterlevel:      db 0 ;set to 1 by eventhandler
multiblockactive:   db 0 ;if set to 1 blocks do not turn brown at that frame
holdscreen:         db 0 ;set the timer to wait out the screenbuild
oldcamerascroll:    db 0 ;hold camerax static and do not allow mario to scroll the camera
ditileanimations:   db 0 ;disables the tile animations when set to 1
notileanimations:   db 0 ;same as above but more exclusive
demoend?:           db 0 ;demoscreen ends and exits to worldmap
switchon:           db 0 ;a special switch has been triggered
waterlevel:         db 0 ;is the map a watermap?
waterheight:        dw 0 ;height of the water
iceactive:          db 0 ;map has ice
forcetile:          db 0 ;force the exsistance of a tile below mario's feet
inhibitsprite:      db 0 ;inhibit the rendering of the mariosprite
yoshicoinscolected: db 0 ;all yoshi coins collected inhibit collecting again for this map
;yoshicointemp:      db 0 ;gets written to the usermapdata
maxtile:            db 0 ;mario is standing on a maxsheertile
switchmusicforstar: db 0 ;switch to star music
mainid:             db 0 ;main map id
springactive:       db 0 ;A spring is allready trowing mario up in the air and no other spring may continue with its operation
magikoopaactive:    db 0 ;used to keep track of the magikoopa object can be used for other things too
forcesetbackground: db 0 ;allow the eventhandler to force a background rebuild after a handle
staticcamy:         db 0 ;block movement of the Y camera
doublepboxactive:   db 0 ;grey and blue p-box have been activated
walktimer:          db 0 ;time marioexit
endcoins:           db 0 ;amount of coins you get at levelend
spawn1up:           db 0 ;spawn a oneup
;rebuildscreen:      db 0 ;if set to 1 the eventhandler will force a screenrebuild (use with EXTREME! care)
cdevice:            dw 0 ;temp store for previous controls. Used in the intromenu as cheap hack!


;yoshi intercontrol
yoshiheadix:        dw 0 ;the ix object adress of the head
yoshiix:            dw 0 ;the ix value of yoshi itself
yoshileftright:     db 0 ;0 = yoshi is facing the left 1 = yoshi is facing the right 1 = default on spawn
yoshiwalktable:     db 0 ;same as mariowalktable. This is only used when yoshi goes beserk.
yoshiswallow:       db 0 ;yoshi is swallowing a target. This variable is used as a timer
yoshifilled:        db 0 ;yoshi got something in his mouth is also timed
objectstat:         db 0 ;objectrelease number of the object in yoshi's mouth
swallowcounter:     dw 0 ;how long does yoshi have the object in his mouth?
tonguehold:         db 0 ;block the yoshitongue from rampaging the pokey/cactus thing
tongueiy:           dw 0 ;iy of object that is grabbed by the yoshitongue

;cutscene control
endcutscene:        db 0 ; 1 = end the cutscene engine
cutscenenr:         db 0 ;number of the cutscene

;spritehiderobject tempdata
spatx:              db 0
spritehiderx:       db 0
spritehidersize:    db 0

;tubecoverup related
holdingix:          dw 0 ;ix adress of any object which is in mario's hands.


;darky support
ixcopy:             dw 0 ; x coordinates of object

;variables for SeeSfxreplayer2
tempo2:  DB    0,0
PATADD2: DB    0,0               ;Current pattern address
_HALT2:  DB    0               ;Halt counter (0-15)
loopnr2: DB    0               ;Current loop number (0-3)
loopbf2: DS    4*3
clpadr2: DW    $00             ;"Continue" loop address

;       reg.  0 1 2 3 4 5 6 7 8 9 A B C D
PsgReg2: DB    0,0,0,0,0,0,0,0,0,0,0,0,0,0 ;   - PSG port store buffer -

;second psg
Sfxnr2:                db  0
ForceSfx_PR2:          db  0
SfxpreviousPriority2:  db  0
SfxPriority2:          db  0
SetSfx2?:              db  0
Sfxon2?:               db  0
ForceSfx2?:            db  0



endlocalvars: equ $ - localvars ;size of the list

variablespage0lenght: equ $ - variablespage0

engineend:



