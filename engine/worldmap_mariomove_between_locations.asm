   
WM_mariostart:  ;movement table, amount of frames, spriteanimation
;start mario 30 frames dont move mario front to camera dont animate, 66 frames walking up 1 step per 3 frames mario walk 
;animation back to camera, 20 frames dont move mario walk animation back to camera
  dw  WM_movementtable0001 : db 30  : db 0 ;dont move,  30 frames, 0=mario front to camera dont animate
  dw  WM_movementtable0002 : db 66  : db 2 ;move up,    66 frames, 2=mario walk animation back to camera
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0 
  
WM_marioWalkinginplacefronttoCamera:  ;movement table, amount of frames, spriteanimation
  dw  0

WM_marioFromYosHoutoYoIsl1:  ;movement table, amount of frames, spriteanimation
  ;left speed 2, down speed 1   (16 pixels left, 08 pixels down)
  dw  WM_movementtable0037 : db 32  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, down speed 1   (16 pixels left, 08 pixels down)
  dw  WM_movementtable0037 : db 32  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0
  
WM_marioFromYoIsl1toYosHou:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (16 pixels right, 08 pixels up)
  dw  WM_movementtable0031 : db 32  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (16 pixels right, 08 pixels up)
  dw  WM_movementtable0031 : db 32  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromYosHoutoYoIsl2:
  ;right speed 2, down speed 1   (16 pixels right, 08 pixels down)
  dw  WM_movementtable0033 : db 32  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (16 pixels right, 08 pixels down)
  dw  WM_movementtable0033 : db 32  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   
  
WM_marioFromYoIsl2toYosHou:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (16 pixels left, 08 pixels up)
  dw  WM_movementtable0039 : db 32  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (16 pixels left, 08 pixels up)
  dw  WM_movementtable0039 : db 32  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0  

WM_marioFromYoIsl1toYelSwP:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (08 pixels right, 16 pixels up)
  dw  WM_movementtable0030 : db 32  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (08 pixels right, 16 pixels up)
  dw  WM_movementtable0030 : db 32  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 11 ;11=mario climbing
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing

  db  1,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa

WM_marioFromYelSwPtoYoIsl1WhileEnteringScreen:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 2, left speed 1 (08 pixels left, 16 pixels down)
  dw  WM_movementtable0036 : db 32  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, left speed 1 (08 pixels left, 16 pixels down)
  dw  WM_movementtable0036 : db 32  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0

WM_marioFromYelSwPtoYoIsl1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;move down, 64 frames, 11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;move down, 64 frames, 11=mario climbing

  db  2,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa

WM_marioFromYoIsl1toYelSwPWhileEnteringScreen:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromForIl3toForIl2:
WM_marioFromYoIsl3toYoIsl4:
;Movement Package 32 pixels right, 32 pixels up
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 6 ;6=mario in water animation back to camera
  dw  0 

WM_marioFromYoIsl4toYoIsl3:
WM_marioFromForIl2toForIl3:
;Movement Package 32 pixels down, 32 pixels left
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels down, 32 pixels left

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromIggyCatoYoIsl4:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0 

WM_marioFromYoIsl4toIggyCa:  
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromIggyCatoDoPla1:
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up

  db  3,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa

WM_marioFromDoPla1toIggyCa:
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down

  db  4,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa

WM_marioFromIggyCatoDoPla1WhileEnteringScreen:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromDoPla1toIggyCaWhileEnteringScreen:
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 16 pixels down

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;dont move, 20 frames, 1=mario walk animation front to camera
  dw  0

WM_marioFromDoPla3toTubeH1:
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera

;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033                 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032                 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0

WM_marioFromTubeH1toDoPla3: ;in total move 32 pixels left and 64 pixels up
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029               : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0
  
WM_marioFromDoPla1toDoPla2: ;in total move 32 pixels left and 64 pixels up
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up

;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0

WM_marioFromWendCatoTubeL1:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up

;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0

WM_marioFromDoPla2toDoPla1:
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down

;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;  dw  WM_movementtable0034 | db 21  | db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0

WM_marioFromTubeL1toWendCa:
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down

;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;  dw  WM_movementtable0034 | db 21  | db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0

WM_marioFromDoPla1toDoSec1:
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                08 pixels up)
  dw  WM_movementtable0029               : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 6 ;6=mario in water animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001               : db 20  : db 6 ;6=mario in water animation back to camera
  dw  0

WM_marioFromDoSec1toDoPla1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;dont move, 20 frames, 1=mario walk animation front to camera
  dw  0

WM_marioFromDoPla2toGrSwPa:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   

WM_marioFromGrSwPatoDoPla2:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromDoPla2toDoGhos:
WM_marioFromForIl4toFoGhos:
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up


;Movement Package 32 pixels right, 32 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0    

WM_marioFromDoGhostoDoPla2:
;Movement Package 32 pixels left, 32 pixels down
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 32 pixels left, 32 pixels up

;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0    

WM_marioFromYoIsl2toYoIsl3:
WM_marioFromDoGhostoTopSec:
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up
;Movement Package 16 pixels up
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 16 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromYoIsl3toYoIsl2:
WM_marioFromTopSectoDoGhos:
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down
;Movement Package 16 pixels down
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;Movement Package 16 pixels down

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0

WM_marioFromDoGhostoDoPla3:
  ;right speed 3               (64 pixels right)
  dw  WM_movementtable0032 : db 85  : db 4 ;4=mario walk animation right to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromDoPla3toDoGhos:
  ;left speed 3               (64 pixels left)
  dw  WM_movementtable0038 : db 85  : db 3 ;3=mario walk animation left to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0    

WM_marioFromDoPla4toMortCa:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromMortCatoDoPla4:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   

WM_marioFromMortCatoVaDom1:
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

  db  5,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1

WM_marioFromMortCatoVaDom1WhileEnteringScreen:    
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0   

WM_marioFromVaDom1toMortCa:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  
  db  6,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa

WM_marioFromVaDom1toMortCaWhileEnteringScreen:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

WM_marioFromLudwCatoForIl1WhileEnteringScreen:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
;  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

WM_marioFromForIl1toLudwCaWhileEnteringScreen: 
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing

;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
;  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

  
  
WM_marioFromDoGhostoDoSec1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0

WM_marioFromDoSec1toDoGhos: 
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029               : db 21  : db 11 ;11=mario climbing
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromDoSec1toDoSeGh:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 8 ;8=mario in water animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033ScrollScreenDown : db 21  : db 8 ;8=mario in water animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromDoSeGhtoDoSec1:
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 6 ;6=mario in water animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 7 ;7=mario in water animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 7 ;7=mario in water animation left to camera
  dw  0

WM_marioFromDoPla3toDoPla4: 
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033ScrollScreenDown : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromDoPla4toDoPla3:  
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0


WM_marioFromDoSeGhtoTubeG1:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromTubeG1toDoSeGh:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   
   
WM_marioFromForIl4toForSec:
WM_marioFromDoSeGhtoStarA1:
  ;down speed 3               (16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3               (16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

WM_marioFromForSectoForIl4:
WM_marioFromStarA1toDoSeGh:
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0   

WM_marioFromDoSec2toTubeG2:
  ;down speed 3               (16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3               (16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

WM_marioFromTubeG2toDoSec2:
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0   
  
WM_marioFromDoSec2toTubeH2:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromTubeH2toDoSec2:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   
  
WM_marioFromVaDom1toVaDom2:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 32  : db 4 ;4=mario walk animation right to camera

;Movement Package 24 pixels right, 32 pixels up
  ;right speed 3               (08 pixels right)
  dw  WM_movementtable0032 : db 10  : db 8 ;8=mario in water animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 8 ;8=mario in water animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
;/Movement Package 24 pixels right, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 6 ;6=mario in water animation back to camera
  dw  0   
  
WM_marioFromVaDom2toVaDom1:
;Movement Package 32 pixels down, 24 pixels left
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 5 ;5=mario in water animation front to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 7 ;7=mario in water animation left to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 7 ;7=mario in water animation left to camera
;/Movement Package 32 pixels down, 24 pixels left

  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 32  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0     
  
WM_marioFromVaDom1toVaSec1:  ;in total move 32 pixels left and 64 pixels up
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
;/Movement Package 32 pixels left, 32 pixels up

  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0

WM_marioFromVaSec1toVaDom1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing

;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 10  : db 11 ;11=mario climbing
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;  dw  WM_movementtable0034 | db 21  | db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0

WM_marioFromVaDom2toVaGhos:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 11 ;11=mario climbing
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromVaGhostoVaDom2:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 1 ;1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0

WM_marioFromVaDom2toReSwPa:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 8 ;8=mario in water animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 8 ;8=mario in water animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera
  dw  0 

WM_marioFromReSwPatoVaDom2:
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 6 ;6=mario in water animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 6 ;6=mario in water animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 7 ;7=mario in water animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 7 ;7=mario in water animation left to camera
  dw  0

WM_marioFromVaSec1toTubeI2:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromTubeI2toVaSec1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 1 ;1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera
  dw  0




WM_marioFromVaSec2toVaSec3:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (08 pixels right)
  dw  WM_movementtable0032 : db 10  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (08 pixels right)
  dw  WM_movementtable0032 : db 10  : db 8 ;8=mario in water animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 8 ;8=mario in water animation right to camera
  dw  0

WM_marioFromVaSec3toVaSec2:
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 7 ;7=mario in water animation left to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0
  
WM_marioFromVaSec3toVaFort:
  ;right speed 3               (08 pixels right)
  dw  WM_movementtable0032 : db 10  : db 8 ;8=mario in water animation right to camera
  ;right speed 3               (08 pixels right)
  dw  WM_movementtable0032 : db 10  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0
  
WM_marioFromVaForttoVaSec3:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 7 ;7=mario in water animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 7 ;7=mario in water animation left to camera
  dw  0

WM_marioFromChoSectoTubeK2:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
WM_marioFromButBr2toLudwCa:
WM_marioFromVaForttoButBr1:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
;  ;right speed 3               (16 pixels right)
;  dw  WM_movementtable0032 | db 21  | db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   



WM_marioFromTubeK2toChoSec:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
WM_marioFromForSectoForForWhileEnteringScreen:
WM_marioFromLudwCatoButBr2:
WM_marioFromButBr1toVaFort:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   

WM_marioFromLudwCatoForIl1:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera
;/Movement Package 32 pixels down, 32 pixels right

;  ;down speed 3             (                16 pixels down)
;  dw  WM_movementtable0035 : db 21  : db 1 ;1=mario walk animation front to camera

  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
;  dw  WM_movementtable0035 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing

  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera

  db  7,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa
 

WM_marioFromForIl1toLudwCa:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  db  8,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa

WM_marioFromVaGhostoVaDom3:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;1=mario walk animation front to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera
  dw  0 

WM_marioFromVaDom3toVaGhos:
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0



;package 48 down
WM_marioFromStarF2toStarW5:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;package 32 down
WM_marioFromFrontDtoLarrCa:
WM_marioFromStarD1toValBo4:
WM_marioFromValBo4toValBo3:
WM_marioFromLarrCatoValGho:
WM_marioFromBackDotoValFor:
WM_marioFromValFortoValBow:
WM_marioFromVaDom4toLemmCa:
WM_marioFromVaDom3toVaDom4:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera
  dw  0

;package 48 up
WM_marioFromStarW5toStarF2:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
;package 32 up
WM_marioFromLarrCatoFrontD:
WM_marioFromValBo4toStarD1:
WM_marioFromValBo3toValBo4:
WM_marioFromValGhotoLarrCa:
WM_marioFromValFortoBackDo:
WM_marioFromValBo2toValFor:
WM_marioFromLemmCatoVaDom4:
WM_marioFromVaDom4toVaDom3:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromChoIs5toWendCa:
WM_marioFromChoIs4toChoFor:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromWendCatoChoIs5:
WM_marioFromChoFortoChoIs4:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera
  dw  0

WM_marioFromLemmCatoTubeJ2:
;Movement Package 32 pixels down, 32 pixels left
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels down, 32 pixels left

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromTubeJ2toLemmCa:
;Movement Package 32 pixels right, 32 pixels up
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromTubeJ1toCheese:
;Movement Package 32 pixels up, 32 pixels right
  ;up speed 2, right speed 1   (10 pixels up, 06 pixels right)
  dw  WM_movementtable0030ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (10 pixels up, 06 pixels right)
  dw  WM_movementtable0030               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 1, right speed 2   (06 pixels up, 10 pixels right)
  dw  WM_movementtable0031ScrollScreenUp : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 1, right speed 2   (06 pixels up, 10 pixels right)
  dw  WM_movementtable0031               : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032               : db 21  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0 

WM_marioFromCheesetoTubeJ1:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

;Movement Package 32 pixels left, 32 pixels down
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037                 : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21         : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromCheesetoSodaLa:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 10  : db 5 ;5=mario in water animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0 
  
WM_marioFromSodaLatoCheese:
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 06 ;6=mario in water animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029               : db 10  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 11 ;11=mario climbing

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromCookietoLudwCa:
;Movement Package 24 pixels down, 32 pixels right
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035               : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034               : db 21  : db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033               : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032               : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 24 pixels down, 32 pixels right

  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032               : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029               : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029               : db 21  : db 11 ;11=mario climbing
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromLudwCatoCookie:
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035                 : db 21  : db 11 ;11=mario climbing
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera

;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029               : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

;package 64 left
WM_marioFromChoIs3toChoFor:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
;package 48 left
WM_marioFromStarF1toSpeci1:
WM_marioFromChoSectoTubeL2:
WM_marioFromStarD2toStarW3:
WM_marioFromFrontDtoStarD1:
WM_marioFromLarrCatoValBo4:
WM_marioFromValGhotoValBo3:
WM_marioFromCookietoCheese:
WM_marioFromVaSec1toStarB1:
WM_marioFromChoIs1toChoGho:
WM_marioFromStarW2toStarB2:
WM_marioFromChoIs4toChoIs5:
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
;package 32 left
WM_marioFromSpeci1toSpeci2:
WM_marioFromValBo2toValGho:
WM_marioFromValBo1toValBo2:
WM_marioFromVaSec2toTubeI1:
WM_marioFromForFortoStarD1:
WM_marioFromBlSWPatoForIl2:
WM_marioFromForIl1toFoGhos:
WM_marioFromButBr2toButBr1:
WM_marioFromChoIs2toChoIs3:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   

;package 64 right
WM_marioFromChoFortoChoIs3:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032               : db 21  : db 4 ;4=mario walk animation right to camera
;package 48 right
WM_marioFromSpeci1toStarF1:
WM_marioFromTubeL2toChoSec:
WM_marioFromStarW3toStarD2:
WM_marioFromStarD1toFrontD:
WM_marioFromValBo4toLarrCa:
WM_marioFromValBo3toValGho:
WM_marioFromCheesetoCookie:
WM_marioFromStarB1toVaSec1:
WM_marioFromChoGhotoChoIs1:
WM_marioFromStarB2toStarW2:
WM_marioFromChoIs5toChoIs4:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032               : db 21  : db 4 ;4=mario walk animation right to camera
;package 32 right
WM_marioFromSpeci2toSpeci1:
WM_marioFromValGhotoValBo2:
WM_marioFromValBo2toValBo1:
WM_marioFromTubeI1toVaSec2:
WM_marioFromStarD1toForFor:
WM_marioFromForIl2toBlSWPa:
WM_marioFromFoGhostoForIl1:
WM_marioFromButBr1toButBr2:
WM_marioFromChoIs3toChoIs2:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromFoGhostoForIl4:
;Movement Package 32 pixels left, 32 pixels down
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;down speed 3               (16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0   

  dw  0  

WM_marioFromForIl1toForIl2:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
   ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera 
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0 

WM_marioFromForIl2toForIl1:
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera

;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromForIl3toFoGhos:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029               : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;dont move,  20 frames, 2=mario walk animation back to camera
  dw  0

WM_marioFromFoGhostoForIl3:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
   ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
   ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
 
 ;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033                 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032                 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0  

WM_marioFromForIl4toForIl2:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 4 ;4=mario walk animation right to camera
  dw  0   

WM_marioFromForIl2toForIl4:
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0   

WM_marioFromForSectoForFor:
;Movement Package 32 pixels down, 32 pixels left
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels down, 32 pixels left

  ;left speed 3               (32 pixels left)
  dw  WM_movementtable0038 : db 42  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

  db  9,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor

WM_marioFromForFortoForSec:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera

  db  10,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor, 10=FromForFortoForSec

WM_marioFromForFortoForSecWhileEnteringScreen:
  ;right speed 3               (32 pixels right)
  dw  WM_movementtable0032 : db 42  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;Movement Package 32 pixels right, 32 pixels up
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromForIl3toRoysCa:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

  db  11,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor, 10=FromForFortoForSec, 11=FromForIl3toRoysCa

WM_marioFromRoysCatoForIl3:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;Movement Package 32 pixels right, 32 pixels up
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels down, 32 pixels right

  db  12,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor, 10=FromForFortoForSec, 11=FromForIl3toRoysCa, 12=FromRoysCatoForIl3


WM_marioFromForIl3toRoysCaWhileEnteringScreen:
;Movement Package 32 pixels down, 32 pixels left
  ;down speed 3             (                08 pixels down)
;  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;1=mario walk animation front to camera
  ;down speed 3             (                08 pixels down)
;  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
;  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
;  dw  WM_movementtable0037 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
;  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels down, 32 pixels left


  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera


;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromRoysCatoForIl3WhileEnteringScreen:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromRoysCatoChoIs1:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0  
  
WM_marioFromChoIs1toRoysCa:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromChoGhotoChoIs2:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0  

WM_marioFromChoIs2toChoGho:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0

WM_marioFromChoIs2toTubeK1:
;Movement Package 32 pixels down, 32 pixels right
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033                 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032                 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera

;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0 

WM_marioFromTubeK1toChoIs2:
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
   ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 10  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera 
 
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038               : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels up
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0


WM_marioFromChoIs3toCircle:
;Movement Package 32 pixels down, 32 pixels left
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 2, left speed 1 (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels down, 32 pixels left

  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (16 pixels up)
;  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;Movement Package 32 pixels right, 32 pixels up
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 2 ;2=mario walk animation back to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035 : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                16 pixels down)
;  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
;

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera 
  dw  0
  
WM_marioFromWendCatoSunkGh:
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029 : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 3               (16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 6 ;6=mario in water animation back to camera

;Movement Package 32 pixels right, 32 pixels up
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 10  : db 6 ;6=mario in water animation back to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 6 ;6=mario in water animation back to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 8 ;8=mario in water animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 8 ;8=mario in water animation right to camera
;/Movement Package 32 pixels down, 32 pixels right
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0  
 
WM_marioFromSunkGhtoWendCa:
;Movement Package 32 pixels left, 32 pixels down
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 7 ;7=mario in water animation left to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037 : db 21  : db 7 ;7=mario in water animation left to camera
  ;down xzczxcspeed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 5 ;5=mario in water animation front to camera 
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera 
  dw  0   

WM_marioFromSunkGhtoValBo1:
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 6 ;6=mario in water animation back to camera
  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

  db  13,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor, 10=FromForFortoForSec, 11=FromForIl3toRoysCa, 12=FromRoysCatoForIl3, 13=FromSunkGhtoValBo1

WM_marioFromSunkGhtoValBo1WhileEnteringScreen:
;Movement Package 32 pixels up, 32 pixels left
  ;up speed 3                 (                08 pixels up)
;  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 3                 (                08 pixels up)
  dw  WM_movementtable0029 : db 10  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039 : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;left speed 3               (16 pixels left)
  dw  WM_movementtable0038 : db 21  : db 3 ;3=mario walk animation left to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 3 ;3=mario walk animation left to camera
  dw  0

WM_marioFromValBo1toSunkGh:
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera

;Movement Package 32 pixels right, 32 pixels down
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  ;down speed 3             (                08 pixels down)
  dw  WM_movementtable0035 : db 10  : db 5 ;5=mario in water animation front to camera
;/Movement Package 32 pixels down, 32 pixels right

  db  14,0     ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor, 10=FromForFortoForSec, 11=FromForIl3toRoysCa, 12=FromRoysCatoForIl3, 13=FromSunkGhtoValBo1, 14=FromValBo1toSunkGh

WM_marioFromValBo1toSunkGhWhileEnteringScreen:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 5 ;5=mario in water animation front to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;5=mario in water animation front to camera
  dw  0

WM_marioFromStarA2toStarW1:
;Movement Package 32 pixels up, 32 pixels right
  ;up speed 2, right speed 1   (10 pixels up, 06 pixels right)
  dw  WM_movementtable0030ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, right speed 1   (10 pixels up, 06 pixels right)
  dw  WM_movementtable0030               : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 1, right speed 2   (06 pixels up, 10 pixels right)
  dw  WM_movementtable0031ScrollScreenUp : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 1, right speed 2   (06 pixels up, 10 pixels right)
  dw  WM_movementtable0031               : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels left, 32 pixels up

  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0
  
WM_marioFromStarW1toStarA2:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera

;Movement Package 32 pixels left, 32 pixels down
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036 : db 21  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 10  : db 1 ;move down, 64 frames, 1=mario walk animation front to camera
  dw  0    

WM_marioFromStarW1toStarB2:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera

;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 10  : db 2 ;2=mario walk animation back to camera
  dw  0   

WM_marioFromStarB2toStarW1:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 1 ;1=mario walk animation front to camera  
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 1 ;1=mario walk animation front to camera  
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromStarW2toStarC2:
WM_marioFromStarW4toStarD2:
;Movement Package 32 pixels right, 32 pixels up
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, up speed 1   (10 pixels right, 06 pixels up)
  dw  WM_movementtable0031 : db 21  : db 4 ;4=mario walk animation right to camera
  ;up speed 2, right speed 1   (06 pixels right, 10 pixels up)
  dw  WM_movementtable0030 : db 21  : db 4 ;4=mario walk animation right to camera
;/Movement Package 32 pixels down, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromStarC2toStarW2:
WM_marioFromStarD2toStarW4:
;Movement Package 32 pixels left, 32 pixels down
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037                 : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera  
  ;left speed 2, down speed 1   (10 pixels left, 06 pixels down)
  dw  WM_movementtable0037                 : db 21  : db 3 ;3=mario walk animation left to camera
  ;down speed 2, left speed 1   (06 pixels left, 10 pixels down)
  dw  WM_movementtable0036ScrollScreenDown : db 21  : db 3 ;3=mario walk animation left to camera 
;/Movement Package 32 pixels left, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromStarW3toStarC2:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels right

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromStarC2toStarW3:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 4 ;4=mario walk animation right to camera 
;/Movement Package 32 pixels right, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromStarW4toStarE2:
  ;down speed 3             (                16 pixels down)
  dw  WM_movementtable0035ScrollScreenDown : db 21  : db 1 ;1=mario walk animation front to camera

;Movement Package 32 pixels right, 32 pixels down
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;down speed 2, right speed 1   (06 pixels right, 10 pixels down)
  dw  WM_movementtable0034 : db 21  : db 4 ;4=mario walk animation right to camera 
;/Movement Package 32 pixels right, 32 pixels up

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromStarE2toStarW4:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;up speed 2, left speed 1   (06 pixels left, 10 pixels up)
  dw  WM_movementtable0040               : db 21  : db 2 ;2=mario walk animation back to camera
;/Movement Package 32 pixels left, 32 pixels right

  ;up speed 3                 (                16 pixels up)
  dw  WM_movementtable0029ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
 
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 


WM_marioFromStarE2toStarW5:
;Movement Package 32 pixels left, 32 pixels up
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 10  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 3               (08 pixels left)
  dw  WM_movementtable0038 : db 07  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera
  ;left speed 2, up speed 1   (10 pixels left, 06 pixels up)
  dw  WM_movementtable0039ScrollScreenup : db 21  : db 2 ;2=mario walk animation back to camera

;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 2 ;2=mario walk animation back to camera
  dw  0 

WM_marioFromStarW5toStarE2:
;Movement Package 32 pixels right, 32 pixels down
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 08  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 10  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 10  : db 4 ;4=mario walk animation right to camera
  ;right speed 2, down speed 1   (10 pixels right, 06 pixels down)
  dw  WM_movementtable0033 : db 21  : db 4 ;4=mario walk animation right to camera
  ;right speed 3               (16 pixels right)
  dw  WM_movementtable0032 : db 10  : db 4 ;4=mario walk animation right to camera
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 


WM_marioFromSodaLatoStarC1: ;daemos Starworld3 path

  dw  MV_scrolldown : db 48  : db 5
  dw  MV_schuin : db 24  : db 5  
  dw  MV_scrolldown : db 24  : db 5  
  dw  MV_schuin : db 22  : db 5 
  dw  MV_links : db 16  : db 3    
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 1 ;1=mario walk animation front to camera  
  dw  0 

WM_marioFromStarC1toSodaLa:  
 
 
  dw  MV_rechts : db 16  : db 4 
  dw  MV_schuin2 : db 22  : db 6 
  dw  MV_up : db 24  : db 6 
  dw  MV_schuin2 : db 24  : db 6 
  dw  MV_scrollup : db 48  : db 6  
;walk in place before ending the movement
  dw  WM_movementtable0001 : db 20  : db 5 ;1=mario walk animation front to camera  
  dw  0  
 
MV_scrollup:
    db 100,100,100,100 
MV_up:
    db -1,00, 128 

MV_scrolldown:
    db 101, 101
MV_down:
    db 1,00, 128 

MV_schuin:
    db 1,-1, 128
MV_schuin2:
    db -1,1, 128
    
MV_links:
    db 0,-1, 128
    
MV_rechts:
    db 0,1, 128
    
;package 32 down  
;package 32 up  
;package 64 left
;package 48 left
;package 32 left
;package 64 right
;package 48 right
;package 32 right

   
lenghtMariomovetable: equ 4

WM_movementtable0001: ;y movement, x movement, 128=loop
  db  00,00,  128
WM_movementtable0002: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  00,00,  128
WM_movementtable0003: ;y movement, x movement, 128=loop
  db  01,-1,  00,-1,  00,00,  01,-1,  00,-1,  00,00,  01,-1,  00,-1,  00,00,  128
WM_movementtable0004: ;y movement, x movement, 128=loop
  db  01,-1,  00,-1,  00,00,  00,-1,  00,-1,  00,00,  00,-1,  00,-1,  00,00,  128
WM_movementtable0005: ;y movement, x movement, 128=loop
  db  -1,01,  00,01,  00,00,  00,01,  00,01,  00,00,  00,01,  00,01,  00,00,  128
WM_movementtable0006: ;y movement, x movement, 128=loop
  db  -1,01,  00,01,  00,00,  -1,01,  00,01,  00,00,  -1,01,  00,01,  00,00,  128
WM_movementtable0007: ;y movement, x movement, 128=loop
  db  01,01,  00,01,  00,00,  01,01,  00,01,  00,00,  01,01,  00,01,  00,00,  128
WM_movementtable0008: ;y movement, x movement, 128=loop
  db  01,01,  00,01,  00,00,  00,01,  00,01,  00,00,  00,01,  00,01,  00,00,  128
WM_movementtable0009: ;y movement, x movement, 128=loop
  db  -1,-1,  00,-1,  00,00,  00,-1,  00,-1,  00,00,  00,-1,  00,-1,  00,00,  128
WM_movementtable0010: ;y movement, x movement, 128=loop
  db  -1,-1,  00,-1,  00,00,  -1,-1,  00,-1,  00,00,  -1,-1,  00,-1,  00,00,  128
WM_movementtable0011: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  128
WM_movementtable0012: ;y movement, x movement, 128=loop
  db  01,00,  00,00,  128
WM_movementtable0013: ;y movement, x movement, 128=loop
  db  00,01,  00,00,  00,01,  00,00,  00,01,  00,00,  00,01,  -1,00,  128
WM_movementtable0014: ;y movement, x movement, 128=loop
  db  -1,01,  00,00,  -1,00,  -1,01,  00,00,  -1,00,  00,00,  128
WM_movementtable0015: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  -1,00,  00,00,  -1,00,  00,01,  128
WM_movementtable0016: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  -1,00,  00,00,  -1,00,  00,00,  128
WM_movementtable0017: ;y movement, x movement, 128=loop
  db  01,00,  00,00,  01,00,  00,00,  01,00,  00,00,  128
WM_movementtable0018: ;y movement, x movement, 128=loop
  db  01,00,  00,00,  01,00,  00,00,  01,00,  00,-1,  128
WM_movementtable0019: ;y movement, x movement, 128=loop
  db  01,-1,  00,00,  01,00,  01,-1,  00,00,  01,00,  00,00,  128
WM_movementtable0020: ;y movement, x movement, 128=loop
  db  00,-1,  00,00,  00,-1,  00,00,  00,-1,  00,00,  00,-1,  01,00,  128
WM_movementtable0021: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  -1,00,  00,00,  -1,00,  00,00,  128
WM_movementtable0022: ;y movement, x movement, 128=loop
  db  -1,00,  00,00,  -1,00,  00,00,  -1,00,  00,-1,  128
WM_movementtable0023: ;y movement, x movement, 128=loop
  db  -1,-1,  00,00,  -1,00,  -1,-1,  00,00,  -1,00,  00,00,  128
WM_movementtable0024: ;y movement, x movement, 128=loop
  db  00,-1,  00,00,  00,-1,  00,00,  00,-1,  00,00,  00,-1,  -1,00,  128
WM_movementtable0025: ;y movement, x movement, 128=loop
  db  00,01,  00,00,  00,01,  00,00,  00,01,  00,00,  00,01,  01,00,  128
WM_movementtable0026: ;y movement, x movement, 128=loop
  db  01,01,  00,00,  01,00,  01,01,  00,00,  01,00,  00,00,  128
WM_movementtable0027: ;y movement, x movement, 128=loop
  db  01,00,  00,00,  01,00,  00,00,  01,00,  00,01,  128
WM_movementtable0028: ;y movement, x movement, 128=loop
  db  01,00,  00,00,  01,00,  00,00,  01,00,  00,00,  128

;up speed 3
WM_movementtable0029ScrollScreenup: db 100 ;100 = scroll screen up 16 pixels
WM_movementtable0029:
  db  -1,00,  -1,00,  -1,00,  00,00,  128
;up speed 2, right speed 1
WM_movementtable0030ScrollScreenup: db 100 ;100 = scroll screen up 16 pixels
WM_movementtable0030: ;y movement, x movement, 128=loop
  db  00,01,  -1,00,  00,00,  -1,00,  128
;up speed 1, right speed 2
WM_movementtable0031ScrollScreenUp: db 100 ;100 = scroll screen up 16 pixels
WM_movementtable0031: ;y movement, x movement, 128=loop
  db  -1,00,  00,01,  00,00,  00,01,  128
;right speed 2
WM_movementtable0032: ;y movement, x movement, 128=loop
  db  00,01,  00,01,  00,01,  00,00,  128
;right speed 2, down speed 1
WM_movementtable0033ScrollScreenDown:  db 101 ;101 = scroll screen down 16 pixels
WM_movementtable0033: ;y movement, x movement, 128=loop
  db  01,00,  00,01,  00,00,  00,01,  128
;down speed 2, right speed 1
WM_movementtable0034ScrollScreenDown:  db 101 ;101 = scroll screen down 16 pixels
WM_movementtable0034: ;y movement, x movement, 128=loop
  db  00,01,  01,00,  00,00,  01,00,  128
;down speed 3
WM_movementtable0035ScrollScreenDown:  db 101 ;101 = scroll screen down 16 pixels
WM_movementtable0035:  
  db  01,00,  01,00,  01,00,  00,00,  128
;down speed 2, left speed 1
WM_movementtable0036ScrollScreenDown:  db 101 ;101 = scroll screendown 16 pixels
WM_movementtable0036: ;y movement, x movement, 128=loop
  db  00,-1,  01,00,  00,00,  01,00,  128
;left speed 2, down speed 1
WM_movementtable0037ScrollScreenDown:  db 101 ;101 = scroll screendown 16 pixels
WM_movementtable0037: ;y movement, x movement, 128=loop
  db  01,00,  00,-1,  00,00,  00,-1,  128
;left speed 3
WM_movementtable0038: ;y movement, x movement, 128=loop
  db  00,-1,  00,-1,  00,-1,  00,00,  128
;left speed 2, up speed 1
WM_movementtable0039ScrollScreenup: db 100 ;100 = scroll screen up 16 pixels
WM_movementtable0039: ;y movement, x movement, 128=loop
  db  -1,00,  00,-1,  00,00,  00,-1,  128
;up speed 2, left speed 1
WM_movementtable0040ScrollScreenup: db 100 ;100 = scroll screen up 16 pixels
WM_movementtable0040: ;y movement, x movement, 128=loop
  db  00,-1,  -1,00,  00,00,  -1,00,  128

