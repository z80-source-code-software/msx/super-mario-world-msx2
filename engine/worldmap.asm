worldmap:
;  ld    a,1
;  ld    (sfxactive),a
 ; ld    (musicactive),a ;problem, this byte is used for many other things so turning the music on or off doesn't stick
  call disableecho ;specific darky command
  call  worldmapinitiation            ;initiate all the worldmap variables and settings
  call  handlestartmusic              ;initiate the music to be played in this world (routine in enginepage3.asm)
  call  copyworldmaptilestopage3vram  ;set all the current world tiles in Vram page 3
  call  copymaptoram                  ;copy the map from $8000 in rom to $4000 in ram
  call  worldmapsetpalette            ;set the palette for the world where mario currently is
  call  copyinfobartopscreen          ;copy the info bar in the top of the screen
  call  worldmapsetcamera             ;sets camera and verticalscreenoffset
  call  buildupmapSetavailablepaths   ;set paths of all available levels in the world where mario is currently present
  call  checkmakelevelavailable       ;check if a level has to be made available (this happens after you finish a level)
  call  buildupmap                    ;build up the map in page 0 and page 1
  call  enableworldmapinterrupt       ;set up the int handler for worldmap
  call  setmarioanimation             ;animate mario to walk (if in water use the mario in water sprites for animation)
  call  checkautomoveatstartworldmap  ;should mario already move somewhere when the worldmap becomes visible ?
  call  infobarlives                  ;set lives in the infobar
  call  infobarlocation.withoutcheck  ;set the location in the infobar
  call  initiatebordermaskingsprites  ;load the bordermaskingsprites into Vram, setting the ready to use

 ld a,0
 ld (swaptablesonvblank?),a 
 
 
WorldMapengine:
  call  infobaranimatemario           ;animate mario in the infobar
  call	ReadControls                  ;readout keyboard or joystick/joymega (routine in enginepage3.asm)
  jp	  readexit                      ;check if esc is pressed. if so go to sound/chip optionsmenu
.return:
  call	HandleControls                ;handle the output from ReadControls/ user interaction
  call  movemario                     ;move mario when a new location is requested
  call  Worldmapputsprites            ;out color table to vram, character table to vram, spat to ram
  call  putworldmapspattovram         ;out spat to vram
  call  animatewater                  ;animate water using palettechanges
  call  animateyellowandredleveldots  ;animate level dots using palattechanges
  call  tileanimation                 ;after completing a level there is some kind of animation on the map
  call  fadeoutenterlevel
  call  infobarlocation               ;put the new location in the infobar when mario arrives at a level  
  
;this flag sais we can now swap horizontal& vertical offset 
;we can swap page and we can swap sprite tables
  ld    a,1
  ld    (swaptablesonvblank?),a
 
  ld    hl,Worldmapvblankintflag
  xor   a
.loop:
  cp    (hl)
  jr    z,.loop
  ld    (hl),a

  ld    a,(framecounter)
  inc   a
  ld    (framecounter),a

  ld    a,(switchworld?)
  or    a
  jp    nz,switchworld

  ld    a,(endworldmap) ;while this eats up interrupt time which may cause tiny timing bugs it is much safer than exiting a call with a stack manupilation
  dec   a
  jp    z,exitworldmap
  
  ld a,(forcesavemenu)
  and a
  call nz,exittosavemenu ;loader can force a savemenu to appear  
  
  
  jp    WorldMapengine

  ;darky specific code
disableecho:  
  
  ld a,(darky?)
  and a
  ret z
  
    ;effects off
    ld hl,$3207 ;set line in + bypass on ;4 for line in
    jp senddarkycommand
  
  
 ; ret
  
  
  
;a-z (sx)
fontoffsets:  db  0,6,12,18,24,30,36,42,48,52,59,66,73,80
              db  87,94,101,108,114,121,127,134,141,148,155,161
;0-9 (location numbers) (sx)
fontoffsetslocationnumbers:
              db  171,178,184,191,198,205,212,219,226,233,240
;0-9 (lives) (sx)
fontoffsetslives:
              db  176,184,192,200,208,216,224,232,240,248

infobarlives:
  ld    a,64
  ld    (worldmaplives+dx),a 

  ld    b,0           ;ten folds

  ld    a,(lives)
  cp    100
  jr    c,.loop
  ld    a,99          ;max lives in display is 99
  
.loop:
  sub   10            ;find the amount of tenfolds
  jr    c,.tenfoldsset
  inc   b             ;+1 tenfold every time nocarry is found
  jr    .loop
.tenfoldsset:

  push  af
  ld    a,b
  or    a
  call  nz,.setnumber ;this is to set the tenfold
  pop   af
  
  add   10            ;now set the remaining

.setnumber:
  ld    d,0
  ld    e,a
  ld    hl,fontoffsetslives
  add   hl,de
  ld    a,(hl)
  ld    (worldmaplives+sx),a
  
  ld    hl,worldmaplives
  call  DoCopy
  
  ld    a,73          ;dx for the remaining
  ld    (worldmaplives+dx),a
  ret

removelocation:
  db    052,000,020,002   ;sx,--,sy,spage
  db    086,000,027,002   ;dx,--,dy,dpage
  db    161,000,007,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
  
infobarlocation:
  ld    a,(WM_setlocation?)
  or    a
  ret   z
  xor   a
  ld    (WM_setlocation?),a

;beep sound when arriving at a location that is not a tube
  ld    ix,(CurrentLocation)
  ld    de,19
  add   ix,de
  ld    a,(ix)
  or    a
  call  nz,Playsfx.mapmovetospot

.withoutcheck:
;first remove the old location text
  ld    hl,removelocation
  call  DoCopy

;location text starts at x=87
  ld    a,87
  ld    (worldmaplocationtext+dx),a
  
;get the new text in ix  
  ld    ix,(CurrentLocation)
  ld    de,19
  add   ix,de
  
.loop:
;per letter set the sx and the nx
  ld    a,(ix)
  or    a
  ret   z  
      
  ;check special symbols
  cp    $27               ;' symbol
  jr    nz,.next1
  ld    a,168
  ld    (worldmaplocationtext+sx),a
  ld    a,3
  ld    (worldmaplocationtext+nx),a
  jp    .copytheletter
  
.next1:
  cp    $20               ;space symbol
  jr    nz,.next2
  ld    a,245
  ld    (worldmaplocationtext+sx),a
  ld    a,4
  ld    (worldmaplocationtext+nx),a
  jp    .copytheletter
  
.next2:  
  cp    $23               ;# symbol
  jr    nz,.next3
  ld    a,240
  ld    (worldmaplocationtext+sx),a
  ld    a,5
  ld    (worldmaplocationtext+nx),a
  jp    .copytheletter
  
.next3:  
  ;/check special symbols
  
  sub   a,$41             ;are we using numbers or letters ?
  jr    nc,.endchecknumbers  
  add   a,43              ;at this point we are using letters
  
.endchecknumbers:
  ld    hl,fontoffsets
  ld    d,0
  ld    e,a
  add   hl,de
  ld    a,(hl)            ;sx
  ld    b,a
  ld    (worldmaplocationtext+sx),a
  inc   hl
  ld    a,(hl)
  sub   b
  ld    (worldmaplocationtext+nx),a

;copy the letter
.copytheletter:
  ld    hl,worldmaplocationtext
  call  DoCopy  

;next letter will be 8 pixels further
  ld    a,(worldmaplocationtext+nx)
  add   a,2
  ld    b,a

  ld    a,(worldmaplocationtext+dx)
  add   a,b
  ld    (worldmaplocationtext+dx),a
  
  inc   ix
  jp    .loop
  ret

copymariosmall1:
  db    000,000,050,002   ;sx,--,sy,spage
  db    036,000,013,002   ;dx,--,dy,dpage
  db    016,000,021,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymariosmall2:
  db    016,000,050,002   ;sx,--,sy,spage
  db    036,000,013,002   ;dx,--,dy,dpage
  db    016,000,021,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

copymariobig1:
  db    032,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymariobig2:
  db    048,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymariobig3:
  db    064,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

copymarioshooting1:
  db    080,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymarioshooting2:
  db    096,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymarioshooting3:
  db    112,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

copymarioflying1:
  db    128,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymarioflying2:
  db    144,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy
copymarioflying3:
  db    160,000,042,002   ;sx,--,sy,spage
  db    036,000,005,002   ;dx,--,dy,dpage
  db    16,000,029,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

infobaranimatemario:
  ld    a,(mariostate)    ;0=small, 1=big, 2=marioshooting, 3=mario flying
  or    a
  jp    z,.mariosmall
  dec   a
  jp    z,.mariobig
  dec   a
  jp    z,.marioshooting
  
.marioflying:   
  ld    a,(marioanimationinfobarstep)
  inc   a
  ld    (marioanimationinfobarstep),a
  cp    8
  ld    hl,copymarioflying1
  jp    c,DoCopy
  cp    16
  ld    hl,copymarioflying2
  jp    c,DoCopy
  cp    24
  ld    hl,copymarioflying3
  jp    c,DoCopy
  ld    hl,copymarioflying2
  xor   a
  ld    (marioanimationinfobarstep),a
  jp    DoCopy

.marioshooting:
  ld    a,(marioanimationinfobarstep)
  inc   a
  ld    (marioanimationinfobarstep),a
  cp    8
  ld    hl,copymarioshooting1
  jp    c,DoCopy
  cp    16
  ld    hl,copymarioshooting2
  jp    c,DoCopy
  cp    24
  ld    hl,copymarioshooting3
  jp    c,DoCopy
  ld    hl,copymarioshooting2
  xor   a
  ld    (marioanimationinfobarstep),a
  jp    DoCopy

.mariobig:
  ld    a,(marioanimationinfobarstep)
  inc   a
  ld    (marioanimationinfobarstep),a
  cp    8
  ld    hl,copymariobig1
  jp    c,DoCopy
  cp    16
  ld    hl,copymariobig2
  jp    c,DoCopy
  cp    24
  ld    hl,copymariobig3
  jp    c,DoCopy
  ld    hl,copymariobig2
  xor   a
  ld    (marioanimationinfobarstep),a
  jp    DoCopy

.mariosmall:
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,copymariosmall1
  jp    c,DoCopy
  ld    hl,copymariosmall2  
  jp    DoCopy

switchworld:  
  call  Settempisr                        ;disable line int, and set a temp isr
;  ld a,(slot.bios) ;reset slot config and call bios back
;  out ($a8),a
  call disablescreen
  call stopmusic                          ;stop the music  
  jp   worldmap                           ;restart the worldmap    


setmarioanimation:              ;animate mario to walk (if in water use the mario in water sprites for animation)
;set mariospriteanimation when worldmap starts
;(112,164=start game)(112,142=YoHou)(64,158=YoIsl1)(160,158=YoIsl2)(160,126=YoIsl3)(192,94=YoIsl3)
  ld    hl,WM_marioWalkinginplacefronttoCamera  ;mariospriteanimation: mario walking in place face to camera
  call  movemario.setmovementtableframesandanimation
;/set mariospriteanimation when worldmap starts

  ld    a,(WMmariospriteanimation)
  cp    9                     ;9=mario raise hand dont animate
  jr    nz,.endcheckmariowasonland
  ld    a,1                   ;1=mario walk animation front to camera
  ld    (WMmariospriteanimation),a
  jr    .endcheckmariowasonwater
.endcheckmariowasonland:
  cp    10                    ;10=mario in water raise hand dont animate
  jr    nz,.endcheckmariowasonwater
  ld    a,5                   ;5=mario in water animation front to camera
  ld    (WMmariospriteanimation),a
.endcheckmariowasonwater:
  ret

AutoMoveTable:
dw WM_mariostart                                    ;1= to YosHou
dw WM_marioFromYoIsl1toYelSwPWhileEnteringScreen    ;2= to YelSwP
dw WM_marioFromYelSwPtoYoIsl1WhileEnteringScreen    ;3= to YoIsl1
dw WM_marioFromIggyCatoDoPla1WhileEnteringScreen    ;4= to DoPla1
dw WM_marioFromDoPla1toIggyCaWhileEnteringScreen    ;5= to IggyCa
dw WM_marioFromMortCatoVaDom1WhileEnteringScreen    ;6= to VaDom1
dw WM_marioFromVaDom1toMortCaWhileEnteringScreen    ;7= to MortCa
dw WM_marioFromLudwCatoForIl1WhileEnteringScreen    ;8= to ForIl1
dw WM_marioFromForIl1toLudwCaWhileEnteringScreen    ;9= to LudwCa
dw WM_marioFromForSectoForForWhileEnteringScreen    ;10= to ForFor
dw WM_marioFromForFortoForSecWhileEnteringScreen    ;11= to ForSec
dw WM_marioFromForIl3toRoysCaWhileEnteringScreen    ;12= to RoysCa
dw WM_marioFromRoysCatoForIl3WhileEnteringScreen    ;13= to ForIl3
dw WM_marioFromSunkGhtoValBo1WhileEnteringScreen    ;14= to ValBo1
dw WM_marioFromValBo1toSunkGhWhileEnteringScreen    ;15= to SunkGh
checkautomoveatstartworldmap:
  ld    a,(automoveatstartworldmap?)
  or    a                     ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa
  ret   z

  add   a,a
  ld    d,0
  ld    e,a
  ld    ix,AutoMoveTable-2
  add   ix,de
  ld    l,(ix)
  ld    h,(ix+1)
  call  movemario.setmovementtableframesandanimation

  xor   a 
  ld    (automoveatstartworldmap?),a
  ld    (WM_setlocation?),a  

    ret
  
worldmapbuildup2rowswhengoingup:
  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  ret   nz

  ld    hl,(WMmarioy)  
  ld    de,mariocentrey+16       ;if marioy<128 then cameray=0, dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  ret   c

  ld    hl,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  ld    de,-128*2
  add   hl,de
  ld    (worldmapcameraposition),hl ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile

  ld    a,(yoffsetbuildupscreen)
  sub   a,16
  ld    (yoffsetbuildupscreen),a

;  ld    a,(verticalscreenoffset)
;  sub   a,16
;  ld    (verticalscreenoffset),a

;  ld    a,(slot.page2rom)   ;all RAM except page 2
;  out   ($a8),a 
  call  buildup2newrowsgoingup
  call  buildup2newrowsgoingupinvispage
;  ld    a,(slot.page12rom)   ;all RAM except page 12
;  out   ($a8),a 
  ret

buildup2newrowsgoingupinvispage:  
  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,1
  jp    z,.builduppage
  ld    a,0
.builduppage:
  ld    (copytile+dpage),a  

  ld    ix,worldmapStartAddress - 4
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  
  ld    de,128*0      ;camera position 128* y + 
  add   ix,de
  
  ld    a,(yoffsetbuildupscreen)
  jp    buildup2newrows       


buildup2newrowsgoingup:  
  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,0
  jp    z,.builduppage
  ld    a,1
.builduppage:
  ld    (copytile+dpage),a  

  ld    ix,worldmapStartAddress
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  
  ld    de,128*0      ;camera position 128* y + 
  add   ix,de
  
  ld    a,(yoffsetbuildupscreen)
  jp    buildup2newrows     
    
worldmapbuildup2rowswhengoingdown:
  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  ret   nz

  ld    hl,(WMmarioy)  
  ld    de,mariocentrey     ;if marioy<128 then cameray=0, dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  ret   c

  ld    hl,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  ld    de,128*2
  add   hl,de
  ld    (worldmapcameraposition),hl ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile

  ld    a,(yoffsetbuildupscreen)
  add   a,16
  ld    (yoffsetbuildupscreen),a

;  ld    a,(verticalscreenoffset)
;  add   a,16
;  ld    (verticalscreenoffset),a

;  ld    a,(slot.page2rom)   ;all RAM except page 2
;  out   ($a8),a 
  call  buildup2newrowsgoingdown
  call  buildup2newrowsgoingdowninvispage
;  ld    a,(slot.page12rom)   ;all RAM except page 12
;  out   ($a8),a 
  ret

buildup2newrowsgoingdowninvispage:  
  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,1
  jp    z,.builduppage
  ld    a,0  
.builduppage:
  ld    (copytile+dpage),a  
  ld    ix,128*30 -4 + worldmapStartAddress ;30 tiles lower
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  
  ld    a,(yoffsetbuildupscreen)
  sub   16
  jp    buildup2newrows
  
buildup2newrowsgoingdown:  
  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,0
  jp    z,.builduppage
  ld    a,1  
.builduppage:
  ld    (copytile+dpage),a  
  ld    ix,128*30 + worldmapStartAddress ;30 tiles lower
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  
  ld    a,(yoffsetbuildupscreen)
  sub   16
  jp    buildup2newrows    
    
buildup2newrows:    
  ld    (copytile+dy),a
  xor   a
  ld    (copytile+dx),a   ;start copying at (0,36) on the map (right below the status bar)

  ld    b,32              ;32 tiles wide
  ld    c,2               ;2 tiles high

.loop:
  push  bc
  call  setsxandsy16bit        ;ix->tilenumber
    
  ld    hl,copytile
  call  DoCopy

  ld    a,(copytile+dx)
  add   a,8
  ld    (copytile+dx),a

  inc   ix
  inc   ix
  pop   bc
  djnz  .loop

  ld    de,64           ;next line
  add   ix,de

  ld    a,(copytile+dy)
  add   a,8
  ld    (copytile+dy),a

  ld    b,32
  dec   c
  jr    nz,.loop
  ret
  
worldmapscrollleft:
  ld    hl,(WMmariox)  
  ld    de,mariocentrex       ;if mariox<14*8  then camerax=0, dont move camera
  or    a                     ;reset carry
  sbc   hl,de
  ret   c

  ld    hl,(WMmariox)  
  ld    de,mariocentrexRight+1;if mariox>50*8 then camerax=furthest right, dont move camera
  or    a                     ;reset carr0y
  sbc   hl,de
  ret   nc

;R#18 (register 18) uses bits 3 - 0 for horizontal screen adjust
;MSB  7   6   5   4   3   2   1   0
;R#18 v3  v2  v1  v0  h3  h2  h1  h0
;h can have a value from 0 - 15 and the screen adjusts horizontally according to the table below
;     7   6   5   4   3   2   1   0   15  14  13  12  11  10  9   8
;H  left                       centre                           right  
;at the start of the scrollroutine (left or right) horizontalscreenoffset = 8
  ld    a,(horizontalscreenoffset)        ;set horizontal screen adjust
  dec   a
  and   15
  ld    (horizontalscreenoffset),a

  ld    a,(horizontalscrollstep)
  dec   a                                 ;we basically scroll backwards compared to scrolling right, so decrease the scrollstepp
  and   15                                ;there are 16 steps when scrolling left or right
  ld    (horizontalscrollstep),a
  cp    15                                ;check for the first step
  call  z,.swappageAndAdjustCamera

  ld    a,0
  ld    (copybackground+sx),a
  ld    a,16
  ld    (copybackground+dx),a

  call  handlescrollstepleft
  ret

.swappageAndAdjustCamera:
;at start of scrolling left switch page and adjust camera position
  ld    hl,(worldmapcameraposition)           ;camera position 128* y + 2*x, camera points to a tile
  ld    de,-4                                 ;2 tiles back
  add   hl,de
  ld    (worldmapcameraposition),hl

  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,1*32+31         ;set page
  jr    z,.Doswitchpage
  ld    a,0*32+31         ;set page
.Doswitchpage:
  ld    (currentpage),a
.endcheck16stepsdone:  
;/at start of scrolling left switch page and adjust camera position
  ret

handlescrollstepleft:
;set source and destination page
  ld    a,(currentpage)
  cp    0*32+31
  ld    a,1
  jr    z,.setpage
  xor   a
.setpage:
  ld    (copybackground+dpage),a
  ld    (copytile+dpage),a
  xor   1
  ld    (copybackground+spage),a
;/set source and destination page

;set source y and destination y
  ld    a,(horizontalscrollstep)
  
  add   a,a                   ;*2
  add   a,a                   ;*4
  add   a,a                   ;*8
  add   a,a                   ;*16

  ld    b,a
  ld    a,(yoffsetbuildupscreen)
  add   a,b

  ld    (copybackground+sy),a
  ld    (copybackground+dy),a
  ld    (copytile+dy),a
;/set source y and destination y

  call  putthefournewtilesleft
  ld    hl,copybackground
  call  DoCopy  
  ret

putthefournewtilesleft:
;in page 1 ram the map is found
;  ld    a,(slot.page2rom)   ;all RAM except page 2
;  out   ($a8),a 
  
  ld    ix,worldmapStartAddress
  ld    de,(worldmapcameraposition)
  add   ix,de 

  ld    de,-4
  add   ix,de

  ld    a,(horizontalscrollstep)
  add   a,a                   ;*2
  add   a,a                   ;*4
  add   a,a                   ;*8
  add   a,a                   ;*16
  ld    l,a
  ld    h,0
  add   hl,hl                 ;*32
  add   hl,hl                 ;*64
  add   hl,hl                 ;*128
  add   hl,hl                 ;*256
  ex    de,hl
  add   ix,de 
    
  call  setsxandsy16bit

  ld    a,0
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  

  inc   ix
  inc   ix
  call  setsxandsy16bit
  ld    a,8
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  

  ld    de,128           ;next line
  add   ix,de
  call  setsxandsy16bit  
  ld    a,(copytile+dy)
  add   a,8
  ld    (copytile+dy),a
  ld    hl,copytile
  call  DoCopy  

  dec   ix
  dec   ix
  call  setsxandsy16bit  
  ld    a,0
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  
  
;  ld    a,(slot.page12rom)   ;all RAM except page 12
;  out   ($a8),a   
  ret  
  
worldmapscrollright:
  ld    hl,(WMmariox)  
  ld    de,mariocentrex+1     ;if mariox<14*8 then camerax=0, dont move camera
  or    a                     ;reset carry
  sbc   hl,de
  ret   c

  ld    hl,(WMmariox)  
  ld    de,mariocentrexRight+1;if mariox>50*8 then camerax=furthest right, dont move camera
  or    a                     ;reset carr0y
  sbc   hl,de
  ret   nc

;R#18 (register 18) uses bits 3 - 0 for horizontal screen adjust
;MSB  7   6   5   4   3   2   1   0
;R#18 v3  v2  v1  v0  h3  h2  h1  h0
;h can have a value from 0 - 15 and the screen adjusts horizontally according to the table below
;     7   6   5   4   3   2   1   0   15  14  13  12  11  10  9   8
;H  left                       centre                           right
  ld    a,(horizontalscreenoffset)             ;set horizontal screen adjust
  inc   a
  and   15
  ld    (horizontalscreenoffset),a             ;set horizontal screen adjust

  ld    a,16
  ld    (copybackground+sx),a
  ld    a,0
  ld    (copybackground+dx),a

  call  handlescrollstepright

  ld    a,(horizontalscrollstep)
  inc   a
  and   15
  ld    (horizontalscrollstep),a
  ret   nz

;16 steps have been scrolled, now switch page and adjust camera position
  ld    hl,(worldmapcameraposition)           ;camera position 128* y + 2*x, camera points to a tile
  ld    de,4                                  ;2 tiles further
  add   hl,de
  ld    (worldmapcameraposition),hl

  ld    a,(currentpage)
  cp    0*32+31           ;set page
  ld    a,1*32+31         ;set page
  jr    z,.Doswitchpage
  ld    a,0*32+31         ;set page
.Doswitchpage:
  ld    (currentpage),a
.endcheck16stepsdone:  
;/16 steps have been scrolled, now switch page and adjust camera position  
  ret

handlescrollstepright:
;set source and destination page
  ld    a,(currentpage)
  cp    0*32+31
  ld    a,1
  jr    z,.setpage
  xor   a
.setpage:
  ld    (copybackground+dpage),a
  ld    (copytile+dpage),a
  xor   1
  ld    (copybackground+spage),a
;/set source and destination page

;set source y and destination y
  ld    a,(horizontalscrollstep)
  add   a,a                   ;*2
  add   a,a                   ;*4
  add   a,a                   ;*8
  add   a,a                   ;*16

  ld    b,a
  ld    a,(yoffsetbuildupscreen)
  add   a,b
  
  ld    (copybackground+sy),a
  ld    (copybackground+dy),a
  ld    (copytile+dy),a
;/set source y and destination y

  call  putthefournewtilesright
  ld    hl,copybackground
  call  DoCopy  
  ret
  
putthefournewtilesright:
;in page 1 ram the map is found
;  ld    a,(slot.page2rom)   ;all RAM except page 2
;  out   ($a8),a 
  
  ld    ix,worldmapStartAddress
  ld    de,(worldmapcameraposition)
  add   ix,de 

  ld    de,64
  add   ix,de

  ld    a,(horizontalscrollstep)
  add   a,a                   ;*2
  add   a,a                   ;*4
  add   a,a                   ;*8
  add   a,a                   ;*16
  ld    l,a
  ld    h,0
  add   hl,hl                 ;*32
  add   hl,hl                 ;*64
  add   hl,hl                 ;*128
  add   hl,hl                 ;*256
  ex    de,hl
  add   ix,de 
    
  call  setsxandsy16bit

  ld    a,256-16
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  

  inc   ix
  inc   ix
  call  setsxandsy16bit
  ld    a,(copytile+dx)
  add   a,8
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  

  ld    de,128           ;next line
  add   ix,de
  call  setsxandsy16bit  
  ld    a,(copytile+dy)
  add   a,8
  ld    (copytile+dy),a
  ld    hl,copytile
  call  DoCopy  

  dec   ix
  dec   ix
  call  setsxandsy16bit  
  ld    a,(copytile+dx)
  sub   a,8
  ld    (copytile+dx),a
  ld    hl,copytile
  call  DoCopy  
  
;  ld    a,(slot.page12rom)   ;all RAM except page 12
;  out   ($a8),a   
  ret


fadeoutenterlevel:
  ld    a,(WMmariospriteanimation)
  cp    9                     ;9=mario raise hand dont animate
  jr    z,.enterlevel
  cp    10                    ;10=mario in water raise hand dont animate 
  ret   nz

.enterlevel:

  ld    a,(WMenterlevelwaittimer)
  dec   a
  ld    (WMenterlevelwaittimer),a  
  ret   nz

  ld    a,1
  ld    (endworldmap),a
  
  ld    ix,(CurrentLocation)
  ld    a,(ix+18)
  
  ex    af,af' ;store value in alternate register 
  ret
  
;  pop   hl                    ;pop the call HandleControls in the worldmap engine ;NO NO NO NO NO this is SOOOO Dangerous. Always jump with a preset variable 
;I have hunted down enough of these wierd stack, interrupt and mem bugs!
;  jp    exitworldmap

	include	"worldmap_all_locations.asm"	
 
HandleControls:

  ld    a,(WMmariomove?)
  or    a
  ret   nz ;if mario is moving we never read the controls otherwise wierd bugs may occur

  ;skip Controls if mario is entering a level
  ld    a,(WMmariospriteanimation)
  cp    9                     ;9=mario raise hand dont animate
  ret   z
  cp    10                    ;10=mario in water raise hand dont animate 
  ret   z

  ;if mario just arrived on a new level, then Controls become active
  ld    hl,(WM_movementtable)
  ld    de,WM_movementtable0001
  or    a
  sbc   hl,de
  jr    z,.controlsareactive

  ;skip Controls if mario is already moving
  ld    a,(WMmariomove?)
  or    a
  ret   nz                    ;no control interaction when mario is moving

.controlsareactive:

  ;skip Controls if you are in world2 and a tile/path-animation is happening
  ld    a,(worldmapworld)     ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2
  jp    nz,.notworld2
  ld    a,(tileanimation?)
  or    a
  ret   nz
.notworld2:

;Standard keyboard mapping/ this is the same for joystick
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		     	Z 	C 		  X   	  right	  left	down	up	(keyboard)
;
;  ld    a,(NewPrControls)
  ld    a,(Controls)
  bit   0,a
  ld    de,2                  ;check if up is presssable, and where does it lead to
  jp    nz,.CheckMarioMovement
  bit   1,a
  ld    de,6                  ;check if down is presssable, and where does it lead to
  jp    nz,.CheckMarioMovement
  bit   2,a
  ld    de,10                 ;check if left is presssable, and where does it lead to
  jp    nz,.CheckMarioMovement
  bit   3,a
  ld    de,14                 ;check if right is presssable, and where does it lead to
  jp    nz,.CheckMarioMovement
  bit   4,a
  jp    nz,.ActionButtonPressed
  bit   5,a
  jp    nz,.ActionButtonPressed
  bit   6,a
  jp    nz,.ActionButtonPressed
  ret
 
.LocationJumpTableTubesAndStars:
;   current location, x,  y,        new location,     world
dw  LocationTubeG1,16 -8, 80,       LocationTubeG2  : db 7
dw  LocationTubeG2,136-8,248 + 24,  LocationTubeG1  : db 2
dw  LocationTubeH1,48 -8,48      ,  LocationTubeH2  : db 7
dw  LocationTubeH2,168-8,216 + 24,  LocationTubeH1  : db 2
dw  LocationTubeI1,9*8-8,2*8 + 24,  LocationTubeI2  : db 3
dw  LocationTubeI2,152-8,40 + 24,   LocationTubeI1  : db 2
dw  LocationTubeJ1,23*8-8,20*8 + 24,LocationTubeJ2  : db 3
dw  LocationTubeJ2,33*8-8,16*8,     LocationTubeJ1  : db 2
dw  LocationTubeK1,26*8-8,22*8,     LocationTubeK2  : db 7
dw  LocationTubeK2,35*8-8,48*8,     LocationTubeK1  : db 2
dw  LocationTubeL2,19*8-8,47*8 + 24,LocationTubeL1  : db 2
dw  LocationTubeL1,10*8-8,19*8 + 24,LocationTubeL2  : db 7
dw  WendCatoTubeL1,10*8-8,19*8 + 24,LocationTubeL2  : db 7
dw  LocationStarA1,40-8,136 + 24,   LocationStarA2  : db 8
dw  LocationStarA2,104-8,280 + 24,  LocationStarA1  : db 2
dw  LocationStarB1,40-8,56 + 24,    LocationStarB2  : db 8
dw  LocationStarB2,24-8,64 + 24,    LocationStarB1  : db 3
dw  StarW2toStarB2,24-8,64 + 24,    LocationStarB1  : db 3
dw  StarW1toStarB2,24-8,64 + 24,    LocationStarB1  : db 3
dw  LocationStarC1,15*8-8,03*8 + 24,LocationStarC2  : db 8
dw  LocationStarC2,31*8-8,29*8 + 24,LocationStarC1  : db 2
dw  StarW2toStarC2,31*8-8,29*8 + 24,LocationStarC1  : db 2
dw  LocationStarD1,25*8-8,07*8 + 24,LocationStarD2  : db 8
dw  ForFortoStarD1,25*8-8,07*8 + 24,LocationStarD2  : db 8
dw  LocationStarD2,39*8-8,31*8 + 24,LocationStarD1  : db 2
dw  StarW3toStarD2,39*8-8,31*8 + 24,LocationStarD1  : db 2
dw  StarW4toStarE2,10*8-8,07*8 + 24,LocationStarE1  : db 7
dw  LocationStarE1,25*8-8,17*8 + 24,StarW4toStarE2  : db 8
dw  LocationStarF2,21*8-8,08*8 + 24,LocationStarF1  : db 9
dw  LocationStarF1,15*8-8,07*8 + 24,LocationStarF2  : db 8
dw  0 ;end

.LocationJumpTableCastlesFortressesAndSwitchPalaces:
dw  LocationIggyCa, levelavailabilitylist+0 : db %0000'0001
dw  LocationMortCa, levelavailabilitylist+2 : db %0000'0100
dw  LocationLemmCa, levelavailabilitylist+4 : db %0000'1000
dw  LocationVaFort, levelavailabilitylist+5 : db %1000'0000
dw  CookietoLudwCa, levelavailabilitylist+5 : db %0000'0010
dw  ButBr2toLudwCa, levelavailabilitylist+5 : db %0000'0010
dw  ForSectoForFor, levelavailabilitylist+7 : db %1000'0000
dw  ForIl3toRoysCa, levelavailabilitylist+7 : db %0010'0000
dw  LocationWendCa, levelavailabilitylist+8 : db %0000'1000
dw  FrontDtoLarrCa, levelavailabilitylist+9 : db %0000'0010
dw  ValBo4toLarrCa, levelavailabilitylist+9 : db %0000'0010
dw  LocationLarrCa, levelavailabilitylist+9 : db %0000'0010
dw  LocationChoFor, levelavailabilitylist+8 : db %0100'0000
dw  LocationValFor, levelavailabilitylist+9 : db %1000'0000
dw  LocationYelSwP, levelavailabilitylist+2 : db %0000'0010
dw  LocationGrSwPa, levelavailabilitylist+2 : db %0000'0001
dw  LocationReSwPa, levelavailabilitylist+3 : db %1000'0000
dw  LocationBlSwPa, levelavailabilitylist+3 : db %0100'0000
dw  0 ;end

;actionbutton is pressed. Now we check if mario is standing on a normal level.
;in this case we jp .Enterlevel.
;if mario is standing on a star or tube, then switch map/world to the appropriate exit
;if mario is standing on a fortress, caste or switch palace, then check if this is
;cleared. If so, do nothing. If not then enter this level.
.ActionButtonPressed:       
  ld    ix,.LocationJumpTableTubesAndStars

.loopStarsAndTubes:
  ld    a,(ix+0)        ;check for end of .LocationJumpTableTubesAndStars
  ld    b,(ix+1)
  or    b
  jr    z,.NowCheckCastlesFortressesAndSwitchPalaces   ;we are entering a 'normal' level

  ld    e,(ix+0)
  ld    d,(ix+1)
  or    a
.kker:  
  ld    hl,(CurrentLocation)
  sbc   hl,de
  jr    z,.StarorTubeFound
  
  ld    de,9
  add   ix,de           ;next location in .LocationJumpTableTubesAndStars
  jr    .loopStarsAndTubes

.NowCheckCastlesFortressesAndSwitchPalaces:
  ld    ix,.LocationJumpTableCastlesFortressesAndSwitchPalaces

.loopCastlesFortressesAndSwitchPalaces:
  ld    a,(ix+0)        ;check for end of .LocationJumpTableTubesAndStars
  ld    b,(ix+1)
  or    b
  jr    z,.EnterLevel   ;we are entering a 'normal' level

  ld    e,(ix+0)
  ld    d,(ix+1)
  or    a
 
  ld    hl,(CurrentLocation)
  sbc   hl,de
  jr    z,.CastleFortressOrSwitchPalaceFound
  
  ld    de,5
  add   ix,de           ;next location in .LocationJumpTableTubesAndStars
  jr    .loopCastlesFortressesAndSwitchPalaces

.CastleFortressOrSwitchPalaceFound:
  ld    l,(ix+2)        ;levelavailabilitylist address
  ld    h,(ix+3)        ;levelavailabilitylist address
  ld    a,(hl)
  ld    b,(ix+4)
  and   b
  ret   nz
  jr    .EnterLevel

;stars and tubes change mario's coordinates and change the world he teleports to
.StarorTubeFound:
  ld    l,(ix+2)        ;mario x
  ld    h,(ix+3)
  ld    (WMmariox),hl
  ld    l,(ix+4)        ;mario y
  ld    h,(ix+5)
  ld    (WMmarioy),hl
  ld    l,(ix+6)        ;new location
  ld    h,(ix+7)
  ld    (CurrentLocation),hl

  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a  

  ld    a,(ix+8)        ;in which world is the new location ?
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  ld    a,1
  ld    (switchworld?),a
  ret  

.EnterLevel:
  xor   a
  ld    (WMmariomove?),a

  ;raise mario's hand on land or in water
  ld    a,(WMmariospriteanimation)
  cp    5
  ld    a,9                   ;9=mario raise hand dont animate
  jr    c,.setsprite
  inc   a                     ;10=mario in water raise hand dont animate 
.setsprite:
  ld    (WMmariospriteanimation),a
  ;/raise mario's hand on land or in water
  
  ld    a,50
  ld    (WMenterlevelwaittimer),a  
  ret  
 
.CheckMarioMovement:
;is it possible to move in the requested direction ?
  ld    hl,(CurrentLocation)
  add   hl,de
  ld    a,(hl)
  ld    e,a
  inc   hl
  or    (hl)
  ret   z
;/is it possible to move in the requested direction ?

;is the new location available ?
  ld    a,(hl)
  ld    d,a

  inc   hl                  ;what is your new location after the movement ?
  ld    c,(hl)
  inc   hl
  ld    b,(hl)

  ld    hl,levelavailabilitylist
  ld    a,(bc)              ;check if this new location is available (bytenumber of levelavailabilitylist)
  or    a
  jr    z,.bytefound
.searchbyte:
  inc   hl
  dec   a
  jr    nz,.searchbyte
.bytefound:
  push  bc                  ;store new location

  inc   bc
  ld    a,(bc)              ;check if this new location is available (bitnumber of levelavailabilitylist)
  inc   a
  ld    b,a
  ld    a,(hl)

.searchbit:
  rrca                      ;rotate a 1 bit to the right bit 0 is copied to the carry flag
  djnz  .searchbit

  pop   bc                  ;recall new location
  ret   nc
;/is the new location available ?

  ld    (CurrentLocation),bc  ;new location becomes current location

  ld    a,1
  ld    (WMmariomove?),a
  ex    de,hl
  call  movemario.setmovementtableframesandanimation
  ret

maxCameray: equ 53*8
movemario:
  ld    a,(WMmariomove?)
  or    a
  ret   z
  
  ;when mario moves mapinfo has to be retrieved from page 1 in ram
  ld    a,(slot.page2rom)   ;all RAM except page 2
  out   ($a8),a 
  
  ld    hl,(WM_movementtable)
  ld    a,(WM_movementtablepointer)
  ld    e,a
  ld    d,0
  add   hl,de   

;check 16 pixels going up   
  ld    a,(hl)
  cp    100
  jr    nz,.endcheckscrollingup

  ld    hl,(WM_movementtable)
  inc   hl
  ld    (WM_movementtable),hl

;  ld    a,13
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

  
  call  worldmapbuildup2rowswhengoingup
;    ret
;  ld    a,15
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

    
  jr    movemario  
.endcheckscrollingup:
;check 16 pixels going up   

;check 16 pixels going down  
  cp    101
  jr    nz,.endcheckscrollingdown

  ld    hl,(WM_movementtable)
  inc   hl
  ld    (WM_movementtable),hl

;  ld    a,13
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

  call  worldmapbuildup2rowswhengoingdown
;ret
;  ld    a,15
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a


  jr    movemario
.endcheckscrollingdown:  
;/check 16 pixels going down  

  ld    a,(WM_movementtablepointer)
  add   a,2
  ld    (WM_movementtablepointer),a
  
  ld    b,(hl)                ;y movement
  inc   hl
  ld    c,(hl)                ;x movement
  inc   hl
  ld    a,(hl)
  cp    128
  jr    nz,.notendtable
  xor   a
  ld    (WM_movementtablepointer),a
.notendtable:

  ;move mario y 
  ld    a,b
  cp    -1
  ld    de,-1
  jr    z,.ymovement16bitready
  or    a
  ld    de,0
  jr    z,.ymovement16bitready
  ld    de,1
.ymovement16bitready:
  ld    hl,(WMmarioy)
  add   hl,de
  ld    (WMmarioy),hl
  ;/move mario y

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  jr    nz,.endhandleverticalscroll

  ;handle vertical scroll (using vertical screen adjust)
  ld    hl,verticalscreenoffset
  ld    a,b
  cp    1
  jr    nz,.endcheckmovingdown
    ;mario moving down ?
  push  hl
  ld    hl,(WMmarioy)  
  ld    de,mariocentrey       ;if marioy<128 then cameray=0, dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  pop   hl
  jr    c,.endhandleverticalscroll

;Max cameryY Check
  push  hl
  ld    hl,(WMmarioy)  
  ld    de,maxCameray         ;if marioy>maxCameray then dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  pop   hl
  jr    nc,.endhandleverticalscroll
;/Max cameryY Check

  inc   (hl)                  ;set vertical screen adjust
  ld    hl,camerayinpixels
  inc   (hl)
  
  jr    .endhandleverticalscroll
.endcheckmovingdown:
    ;/mario moving down ?

  cp    -1
  jr    nz,.endhandleverticalscroll  
  
    ;mario moving up ?
  push  hl
  ld    hl,(WMmarioy)  
  ld    de,mariocentrey-1     ;if marioy<128 then cameray=0, dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  pop   hl
  jr    c,.endhandleverticalscroll

;Max cameryY Check
  push  hl
  ld    hl,(WMmarioy)  
  ld    de,maxCameray-1       ;if marioy>maxCameray then dont scroll
  or    a                     ;reset carry
  sbc   hl,de
  pop   hl
  jr    nc,.endhandleverticalscroll
;/Max cameryY Check

  dec   (hl)                                    ;set vertical screen adjust
  ld    hl,camerayinpixels
  dec   (hl)
    ;/mario moving up ?

.endhandleverticalscroll:
  ;/handle vertical scroll (using vertical screen adjust)
  
  ;move mario x
  ld    a,c
  cp    -1
  ld    de,-1
  jr    z,.xmovement16bitready
  or    a
  ld    de,0
  jr    z,.xmovement16bitready
  ld    de,1
.xmovement16bitready:
  ;/move mario x

  ld    hl,(WMmariox)
  add   hl,de
  ld    (WMmariox),hl

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  jr    nz,.endscrollmap

  push  bc
  ld    a,c
  cp    1
  call  z,worldmapscrollright
  pop   bc

  ld    a,c
  cp    -1
  call  z,worldmapscrollleft
.endscrollmap:

  ld    a,(WMmariomoveamountofframes)
  dec   a
  ld    (WMmariomoveamountofframes),a
  ret   nz

  ld    hl,(WMmariomovetable)
  ld    de,lenghtMariomovetable
  add   hl,de
.setmovementtableframesandanimation:
  ld    (WMmariomovetable),hl

  ;set new movement table, amount of frames to repeat this move, sprite animation for this move
  ld    a,(hl)
  ld    (WM_movementtable),a
  inc   hl
  ld    a,(hl)
  or    a
  jr    z,.endmove
  ld    (WM_movementtable+1),a
  inc   hl
  ld    a,(hl)
  ld    (WMmariomoveamountofframes),a
  xor   a
  ld    (WM_movementtablepointer),a
  inc   hl
  ld    a,(hl)
  ld    (WMmariospriteanimation),a
  ld    a,1
  ld    (WMmariomove?),a
  
  ld    hl,(WM_movementtable)
  ld    de,WM_movementtable0001
  or    a
  sbc   hl,de
  ret   nz
  ld    a,1
  ld    (WM_setlocation?),a  
  ret
  ;/set new movement table, amount of frames to repeat this move, sprite animation for this move
  
.endmove:    
  xor   a
  ld    (WMmariomove?),a
  
  dec   hl
  ld    a,(hl)
  or    a
  jr    nz,.mariospecialaction
  
  ld    a,(WMmariospriteanimation)
  cp    5                                 ;mario was on land ?
  jr    c,.SetmarioSpriteOnland
  cp    9                                 ;9=mario raise hand dont animate
  jr    z,.SetmarioSpriteOnland
.SetmarioSpriteinwater:
  ld    a,5                               ;5=mario in water animation front to camera
  ld    (WMmariospriteanimation),a
  ret

.SetmarioSpriteOnland:
  ld    a,1                               ;1=mario walk animation front to camera
  ld    (WMmariospriteanimation),a
  ret
  
.mariospecialaction:
  dec   a                   ;1=FromYoIsl1toYelSwP, 2=FromYelSwPtoYoIsl1, 3=FromIggyCatoDoPla1, 4=FromDoPla1toIggyCa, 5=FromMortCatoVaDom1, 6=FromVaDom1toMortCa, 7=FromLudwCatoForIl1, 8=FromForIl1toLudwCa, 9=FromForSectoForFor

  jp    z,.FromYoIsl1toYelSwP
  dec   a
  jp    z,.FromYelSwPtoYoIsl1
  dec   a
  jp    z,.FromIggyCatoDoPla1
  dec   a
  jp    z,.FromDoPla1toIggyCa
  dec   a
  jp    z,.FromMortCatoVaDom1
  dec   a
  jp    z,.FromVaDom1toMortCa
  dec   a
  jp    z,.FromLudwCatoForIl1
  dec   a
  jp    z,.FromForIl1toLudwCa
  dec   a
  jp    z,.FromForSectoForFor
  dec   a
  jp    z,.FromForFortoForSec
  dec   a
  jp    z,.FromForIl3toRoysCa
  dec   a
  jp    z,.FromRoysCatoForIl3
  dec   a
  jp    z,.FromSunkGhtoValBo1
  dec   a
  jp    z,.FromValBo1toSunkGh
        
.FromYoIsl1toYelSwP:
  ;from yoshis island 1 to yellow switch palace
  ld    hl,24-8
  ld    (WMmariox),hl
  ld    hl,312  + 24
  ld    (WMmarioy),hl
  ld    a,2                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationYelSwP
  ld    (CurrentLocation),hl
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap
  
.FromYelSwPtoYoIsl1:
  ;from yellow switch palace to yoshis island 1
  ld    hl,80 -8
  ld    (WMmariox),hl
  ld    hl,16
  ld    (WMmarioy),hl
  ld    a,3                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a  
  ld    hl,LocationYoIsl1
  ld    (CurrentLocation),hl
  ld    a,1
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromIggyCatoDoPla1:
  ;from iggy castle to donut plains 1
  ld    hl,72-8
  ld    (WMmariox),hl
  ld    hl,264 + 24 + 64
  ld    (WMmarioy),hl
  ld    a,4                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationDoPla1
  ld    (CurrentLocation),hl
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3              
  jp    .switchmap

.FromDoPla1toIggyCa:
  ;from donut plains 1 to iggy castle
  ld    hl,160 -8
  ld    (WMmariox),hl
  ld    hl,16
  ld    (WMmarioy),hl
  ld    a,5                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationIggyCa
  ld    (CurrentLocation),hl
  ld    a,1
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromMortCatoVaDom1:
  ;from mortons castle to vanilla dome1
  ld    hl,104 -8
  ld    (WMmariox),hl
  ld    hl,160 + 24
  ld    (WMmarioy),hl
  ld    a,6                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationVaDom1
  ld    (CurrentLocation),hl
  ld    a,3
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromVaDom1toMortCa:
  ;from vanilla dome1 to mortons castle
  ld    hl,200-8
  ld    (WMmariox),hl
  ld    hl,136 + 24
  ld    (WMmarioy),hl
  ld    a,7                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationMortCa
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromLudwCatoForIl1:
  ;from ludwig castle to forest of illusion 1
  ld    hl,18*8 -8
  ld    (WMmariox),hl
  ld    hl,00*8 + 24
  ld    (WMmarioy),hl
  ld    a,8                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LudwCatoForIl1
  ld    (CurrentLocation),hl
  ld    a,5
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion
  jp    .switchmap

.FromForIl1toLudwCa:
  ;from forest of illusion 1 to ludwig castle
  ld    hl,55*8-8
  ld    (WMmariox),hl
  ld    hl,20*8 + 24
  ld    (WMmarioy),hl
  ld    a,9                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,CookietoLudwCa
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromForSectoForFor:
  ;from forest secret area to forest fortress
  ld    hl,51*8-8
  ld    (WMmariox),hl
  ld    hl,31*8 + 24
  ld    (WMmarioy),hl
  ld    a,10                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor
  ld    (automoveatstartworldmap?),a    
  ld    hl,ForSectoForFor
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromForFortoForSec:
  ;from forest fortress to forest secret area
  ld    hl,00*8-8
  ld    (WMmariox),hl
  ld    hl,18*8 + 24
  ld    (WMmarioy),hl
  ld    a,11                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor, 11=to ForSec
  ld    (automoveatstartworldmap?),a    
  ld    hl,ForFortoForSec
  ld    (CurrentLocation),hl  
  ld    a,5
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromForIl3toRoysCa:
  ;from forest of illusion 3 to roys castle
  ld    hl,51*8-8
  ld    (WMmariox),hl
  ld    hl,35*8 + 24
  ld    (WMmarioy),hl
  ld    a,12                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor, 12=to RoysCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,ForIl3toRoysCa
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromRoysCatoForIl3:
  ;from roys castle to forest of illusion 3
  ld    hl,18*8-8
  ld    (WMmariox),hl
  ld    hl,20*8 + 24
  ld    (WMmarioy),hl
  ld    a,13                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor, 12=to RoysCa, 13=to ForIl3
  ld    (automoveatstartworldmap?),a    
  ld    hl,ForIl2toForIl3
  ld    (CurrentLocation),hl  
  ld    a,5
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromSunkGhtoValBo1:
  ;from sunken ghost to valley bo 1
  ld    hl,30*8-8
  ld    (WMmariox),hl
  ld    hl,18*8 + 24
  ld    (WMmarioy),hl
  ld    a,14                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor, 12=to RoysCa, 13=to ForIl3
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationValBo1
  ld    (CurrentLocation),hl  
  ld    a,7
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap

.FromValBo1toSunkGh:
  ;from valley bo 1 to sunken ghost
  ld    hl,27*8-8
  ld    (WMmariox),hl
  ld    hl,41*8 + 24
  ld    (WMmarioy),hl
  ld    a,15                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1, 7=to MortCa, 10=to ForFor, 12=to RoysCa, 13=to ForIl3
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationSunkGh
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  jp    .switchmap
  
.switchmap:
  ld    a,1
  ld    (switchworld?),a
  ret

	include	"worldmap_mariomove_between_locations.asm"	
 
Worldmapswap_spat_col_and_char_table:
	ld		a,(VDP_0+6)     ;check current sprite character table
  cp    %0010'1111      ;spr chr table at $17800 now ?
  ld    hl,$6c00        ;spr color table $16c00
  ld    de,$7400        ;spr color table buffer $17400
	ld		a,%1101'1111    ;spr att table to $16e00    
	ld		b,%0010'1110    ;spr chr table to $17000
  jp    z,.setspritetables
  ld    hl,$7400        ;spr color table $17400
  ld    de,$6c00        ;spr color table buffer $16c00
	ld		a,%1110'1111    ;spr att table to $17600
	ld		b,%0010'1111    ;spr chr table to $17800

.setspritetables:
	di
	ld		(VDP_0+5),a
	out		($99),a		;spr att table to $17600
	ld		a,5+128
	out		($99),a
	ld		a,$02     ;%0000'0010
	ld		(VDP_8+3),a
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,b
	ld		(VDP_0+6),a
	out		($99),a		;spr chr table to $17800
	ld		a,6+128
	ei
	out		($99),a

  ld    bc,$200
  ld    (sprcoltableaddress),hl
  add   hl,bc
  ld    (spratttableaddress),hl
  add   hl,bc
  ld    (sprchatableaddress),hl
  ex    de,hl
  ld    (invissprcoltableaddress),hl
  add   hl,bc
  ld    (invisspratttableaddress),hl
  add   hl,bc
  ld    (invissprchatableaddress),hl
  ret

initiatebordermaskingsprites:
  ld    a,(slot.page12rom)            ;all RAM except page 12
  out   ($a8),a 

	ld		a,worldmapspritesblock
	call	block12               ;set blocks in page 1
	ld		c,$98                 ;out port

	ld		de,(sprchatableaddress)		      ;sprite character table in VRAM ($17800)
  call  .bordermaskspritecharacter
	ld		de,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
  call  .bordermaskspritecharacter
	ld		de,(sprcoltableaddress)		      ;sprite color table in VRAM ($17400)
  call  .bordermaskspritecolor
	ld		de,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
  call  .bordermaskspritecolor
  ret

.bordermaskspritecharacter:
;put border masking sprites character
  ld    hl,0 * 32             ;border mask at sprite position 0
  add   hl,de
	ld		a,1
	call	SetVdp_Write
  
  ld    b,22                  ;22 sprites

.loop1:
  push  bc
  ld    hl,WM_sprite_bordermasksprite
	call	outix32               ;1 sprites (1 * 32 = 32 bytes)
  pop   bc
  djnz  .loop1
;/put border masking sprites character
  ret
  
.bordermaskspritecolor:
;put border masking sprites color
  ld    hl,0 * 16             ;border mask at sprite position 0
  add   hl,de
	ld		a,1
	call	SetVdp_Write

  ld    b,11                  ;first 11 sprites

.loop2:
  push  bc
  ld    hl,WM_sprite_bordermasksprite+32+16
	call	outix16               ;1 sprites (1 * 16 = 16 bytes)
  pop   bc
  djnz  .loop2

  ld    b,11                  ;next 11 sprites

.loop3:
  push  bc
  ld    hl,WM_sprite_bordermasksprite+32
	call	outix16               ;1 sprites (1 * 16 = 16 bytes)
  pop   bc
  djnz  .loop3
;/put border masking sprites color
  ret

mariospriteposition:  equ 26
Worldmapputsprites:
  ;spritedata is retrieved by putting the spriteblock on page 1 in rom
  ld    a,(slot.page12rom)            ;all RAM except page 12
  out   ($a8),a   
  
  call  WM_putmariosprite
  call  WM_putbordermasksprites
  call  WM_puteventsprites
  ret

WM_puteventsprites:
  ld    a,(worldmapworld)   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3       
  cp    1
  jp    z,WM_world1_handleeventsprites
  cp    2
  jp    z,WM_world2_handleeventsprites
  cp    3
  jp    z,WM_world3_handleeventsprites
  cp    7
  jp    z,WM_world7_handleeventsprites
  ret

WM_spriteoffsety:           equ 0
WM_spriteoffsetx:           equ 2
WM_spriteoffsetamount:      equ 4
WM_spriteoffsetoffsets:     equ 5
WM_spriteoffsetmovepat:     equ 7
WM_spriteoffsetv1:          equ 8
WM_spriteoffsetv2:          equ 9
WM_spriteoffsetv3:          equ 10
WM_spriteoffsetspritenr:    equ 11
WM_spriteoffsetspritedata:  equ 12

WM_world1_sprites:
;   y   ,x  ,amount,offsets,movepat,v1,v2,v3,spritenr ,  spritedata
dw  050 ,140 : db 1,00,00  ,0      ,00,00,00,22       : dw WM_sprite_cloud16wide
dw  058 ,120 : db 2,00,16  ,0      ,04,00,00,23       : dw WM_sprite_cloud26wide
dw  066 ,042 : db 1,00,00  ,0      ,04,00,00,25       : dw WM_sprite_cloud16wide
dw  092 ,174 : db 1,00,00  ,0      ,04,00,00,30       : dw WM_sprite_cloud16wide
dw  135 ,140 : db 1,00,00  ,0      ,00,00,00,31       : dw WM_sprite_cloud16wide
WM_world1_sprites_end:

WM_world2_sprites:
;   y   ,x  ,amount,offsets,movepat,v1,v2,v3,spritenr ,  spritedata
lenghtlabel:
dw  320 ,040 : db 1,00,00  ,0      ,04,00,00,22       : dw WM_sprite_cloud16wide
lenght_WM_spritetable:  equ $-lenghtlabel
dw  340 ,020 : db 1,00,00  ,0      ,00,00,00,23       : dw WM_sprite_cloud16wide
dw  274 ,088 : db 2,00,00  ,1      ,00,00,00,24       : dw WM_sprite_ghostleft
dw  178 ,073 : db 2,00,00  ,1      ,32,00,00,30       : dw WM_sprite_ghostleft
WM_world2_sprites_end:

WM_world3_sprites:
;   y   ,x  ,amount,offsets,movepat,v1,v2,v3,spritenr ,  spritedata
dw  100 ,170 : db 2,00,00  ,2      ,18,00,00,22       : dw WM_sprite_star1
dw  130 ,060 : db 2,00,00  ,2      ,00,00,00,24       : dw WM_sprite_star1
dw  058 ,136 : db 2,00,00  ,1      ,00,00,00,30       : dw WM_sprite_ghostleft
WM_world3_sprites_end:

WM_world7_sprites:
;   y   ,x  ,amount,offsets,movepat,v1,v2,v3,spritenr ,  spritedata
dw  100 ,210 : db 2,00,00  ,2      ,18,00,00,22       : dw WM_sprite_star1
dw  120 ,060 : db 2,00,00  ,2      ,00,00,00,24       : dw WM_sprite_star1
dw  146 ,112 : db 2,00,00  ,1      ,00,00,00,30       : dw WM_sprite_ghostleft
WM_world7_sprites_end:

WM_world1_handleeventsprites:
  ld    ix,Objectlist + (lenght_WM_spritetable * 0)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 1)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 2)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 3)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 4)
  call  WM_putsprite
  ret

WM_world2_handleeventsprites:
  ld    ix,Objectlist + (lenght_WM_spritetable * 0)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 1)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 2)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 3)
  call  WM_putsprite
  ret

WM_world3_handleeventsprites:
  ld    ix,Objectlist + (lenght_WM_spritetable * 0)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 1)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 2)
  call  WM_putsprite
  ret

WM_world7_handleeventsprites:
  ld    ix,Objectlist + (lenght_WM_spritetable * 0)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 1)
  call  WM_putsprite
  ld    ix,Objectlist + (lenght_WM_spritetable * 2)
  call  WM_putsprite
  ret

WM_putsprite:
  call  WM_spritehandlemovementpattern
  call  WM_putyxandcharactercolorsprite
  ret

WM_spritehandlemovementpattern:
  ld    a,(ix+WM_spriteoffsetmovepat)
  or    a
  jp    z,WMmovementpattern0
  dec   a
  jp    z,WMmovementpattern1
  dec   a
  jp    z,WMmovementpattern2
    ret ;fallthrough
  
startable:    dw WM_sprite_star1,WM_sprite_star2,WM_sprite_star3,WM_sprite_star4
              dw WM_sprite_star5,WM_sprite_star6,WM_sprite_star8,WM_sprite_star9
              dw WM_sprite_star6,WM_sprite_star5,WM_sprite_star4,WM_sprite_star3
              dw WM_sprite_star2,WM_sprite_star1,WM_sprite_star0
WMmovementpattern2:
  ld    a,(framecounter)
  and   3
  ret   nz
  
	ld    a,(ix+WM_spriteoffsetv1)
	add   a,2
  cp    30
  jr    nz,.not30
	xor   a
.not30:
	ld    (ix+WM_spriteoffsetv1),a
  
  ld    d,0
  ld    e,a
  ld    hl,startable
  add   hl,de
  ld    c,(hl)
  inc   hl
  ld    b,(hl)

.setspritecharacter:
  ;set sprite character
	ld    (ix+WM_spriteoffsetspritedata),c
	ld    (ix+WM_spriteoffsetspritedata+1),b     
  ;/set sprite character
  ret

WMGhostmovementtable: ;dy,dx
  db  +00,+00,+00,+01,+00,+00,+01,+00, +00,+01,+00,+00,+01,+00,+01,+00
  db  +01,+00,+01,+01,+00,+01,+00,+01, +01,+00,+01,+01,+01,+00,+01,+01
  db  +01,+01,+00,+01,+01,+01,+00,+01, +01,+00,+01,+00,+01,+01,+00,+01
  db  +00,+01,+00,+01,+00,+00,+01,+00, +00,+01,+00,+00,+01,+00,+00,+00

  db  -00,-00,-00,-01,-00,-00,-01,-00, -00,-01,-00,-00,-01,-00,-01,-00
  db  -01,-00,-01,-01,-00,-01,-00,-01, -01,-00,-01,-01,-01,-00,-01,-01
  db  -01,-01,-00,-01,-01,-01,-00,-01, -01,-00,-01,-00,-01,-01,-00,-01
  db  -00,-01,-00,-01,-00,-00,-01,-00, -00,-01,-00,-00,-01,-00,-00,-00

WMmovementpattern1:
  ld    a,(framecounter)
  and   7
  jr    nz,.endverticalmove

  ;vertical movement
	ld    l,(ix+WM_spriteoffsety)     ;hl=y sprite
	ld    h,(ix+WM_spriteoffsety+1)

	ld    a,(ix+WM_spriteoffsetv2)
	inc   a
	and   7                           ;8 movement steps defined in v1
	ld    (ix+WM_spriteoffsetv2),a

  dec   hl                          ;already move 1 step left
  cp    4                           ;movement steps 0,1,2 and 3 are going left
  jr    c,.sety
  inc   hl                          ;move 2 steps right in case
  inc   hl
.sety:
	ld    (ix+WM_spriteoffsety),l
	ld    (ix+WM_spriteoffsety+1),h
.endverticalmove:    
  ;/vertical movement
  
  ld    a,(framecounter)
  and   1
  ret   z

  ;horizontal movement
	ld    a,(ix+WM_spriteoffsetv1)
  inc   a
	and   127                           ;64 movement steps defined in v1
	ld    (ix+WM_spriteoffsetv1),a

  ld    hl,WMGhostmovementtable
  ld    d,0
  ld    e,a
  add   hl,de                       ;y
  
  ld    a,(hl)                      ;dy
  or    a
  ld    de,0
  jr    z,.setspritex
  cp    1
  ld    bc,WM_sprite_ghostright
  ld    de,1
  jr    z,.setspritecharacter
  cp    1
  ld    bc,WM_sprite_ghostleft
  ld    de,-1

.setspritecharacter:
  ;set sprite character
	ld    (ix+WM_spriteoffsetspritedata),c
	ld    (ix+WM_spriteoffsetspritedata+1),b     
  ;/set sprite character

.setspritex:
	ld    l,(ix+WM_spriteoffsetx)     ;hl=x sprite
	ld    h,(ix+WM_spriteoffsetx+1)
  add   hl,de
	ld    (ix+WM_spriteoffsetx),l
	ld    (ix+WM_spriteoffsetx+1),h  
  ;/horizontal movement
  ret
  
WMmovementpattern0:
  ld    a,(framecounter)
  and   15
  ret   nz

	ld    l,(ix+WM_spriteoffsetx)     ;hl=x sprite
	ld    h,(ix+WM_spriteoffsetx+1)

	ld    a,(ix+WM_spriteoffsetv1)
	inc   a
	and   7                           ;8 movement steps defined in v1
	ld    (ix+WM_spriteoffsetv1),a

  dec   hl                          ;already move 1 step left
  cp    4                           ;movement steps 0,1,2 and 3 are going left
  jr    c,.setx
  inc   hl                          ;move 2 steps right in case
  inc   hl
.setx:
	ld    (ix+WM_spriteoffsetx),l
	ld    (ix+WM_spriteoffsetx+1),h
  ret

WM_putyxandcharactercolorsprite:
;put y  
  ld    a,(ix+WM_spriteoffsetspritenr)
  add   a,a                   ;*2
  add   a,a                   ;*4 (spritenumber * 4, used in spat)
  ld    hl,Worldmapspat
  ld    b,0
  ld    c,a
  add   hl,bc                 ;hl=y offset sprite in spat

  push  hl

  ;check if sprite is within camera range
	ld    de,(camerayinpixels)
	ld    l,(ix+WM_spriteoffsety)
	ld    h,(ix+WM_spriteoffsety+1)
  or    a
  sbc   hl,de
  
  jp    c,.outofscreeny       ;camery > spitey

  ld    de,210
  sbc   hl,de
  jp    nc,.outofscreeny      ;out of screen below vision
  add   hl,de
  ;/check if sprite is within camera range
  
  ld    a,(verticalscreenoffset)
  add   a,l  

  cp    216
  jr    nz,.not216
  inc   a  
.not216:

  ld    b,a                   ;store y in b
  pop   hl    

  ld    (hl),a
;/put y  

;put x
  push  hl
  
  ;check if sprite is within camera range
  ld    a,(worldmapcameraposition)           ;camera position 128* y + 2*x, camera points to a tile
  and   %0111'1111            ;bit 7 is used for the y of camera
  add   a,a                   ;*2

  ld    h,0
  ld    l,a
  add   hl,hl                 ;*4
  ex    de,hl

	ld    l,(ix+WM_spriteoffsetx)
	ld    h,(ix+WM_spriteoffsetx+1)
  or    a
  sbc   hl,de

  jp    c,.outofscreenx       ;camerx > spitex

  ld    de,240
  sbc   hl,de
  jp    nc,.outofscreeny      ;out of screen below vision
  add   hl,de
  ;check if sprite is within camera range

  ld    c,l                   ;store x in c

  pop   hl
  inc   hl    
  ld    (hl),c
;/put x

  ld    a,(ix+WM_spriteoffsetamount)
  cp    1
	jr    z,.endput2ndsprite

;put 2nd sprite if there is one
  inc   hl
  inc   hl
  inc   hl                    ;y next sprite
  ld    a,(ix+WM_spriteoffsetoffsets+0)
  add   a,b

  ld    (hl),a
  inc   hl                    ;x next sprite
  ld    a,(ix+WM_spriteoffsetoffsets+1)
  add   a,c
  ld    (hl),a
.endput2ndsprite:
;/put 2nd sprite if there is one

	ld		a,worldmapspritesblock
	call	block12               ;set blocks in page 1
	ld		c,$98                 ;out port for the outi's

;put sprite character
	ld		de,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
  ld    l,(ix+WM_spriteoffsetspritenr)
  ld    h,0
  add   hl,hl                 ;*2
  add   hl,hl                 ;*4
  add   hl,hl                 ;*8
  add   hl,hl                 ;*16
  push  hl                    ;*16 character data is spritenr * 16 (we use this later for the sprite color)
  add   hl,hl                 ;*32 character data is spritenr * 32
  add   hl,de
	ld		a,1
	call	SetVdp_Write
  
  ld    l,(ix+WM_spriteoffsetspritedata)
  ld    h,(ix+WM_spriteoffsetspritedata+1)
	call	outix32               ;1 sprite (1 * 32 = 32 bytes)

  ld    a,(ix+WM_spriteoffsetamount)
  cp    2                     ;if 2 sprites are used then another 32 outi's is required
	call	z,outix32             ;1 sprite (1 * 32 = 32 bytes)
;/put sprite character

;put sprite color
  exx                         ;store hl (hl points to sprite color data in rom)
  pop   hl                    ;*16 character data is spritenr * 16
	ld		de,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
  add   hl,de
	ld		a,1
	call	SetVdp_Write
  exx                         ;recall hl (hl points to sprite color data in rom)
    
	call	outix16               ;1 sprite (1 * 16 = 16 bytes)
  ld    a,(ix+WM_spriteoffsetamount)
  cp    2                     ;if 2 sprites are used then another 16 outi's is required
	call	z,outix16             ;1 more sprite (1 * 16 = 16 bytes)
;/put sprite color  
  ret

.outofscreenx:
.outofscreeny:
  pop   hl
  ld    a,(ix+WM_spriteoffsetamount)
  cp    1

  ld    a,(verticalscreenoffset)
  or    1                     ;so that spritey will never be 216
  ld    (hl),a
  
	ret   z

  inc   hl
  inc   hl
  inc   hl
  inc   hl                    ;y next sprite
  ld    (hl),a
  ret

WM_putbordermasksprites:
;set y border masking sprites
  ld    a,(verticalscreenoffset)
  add   a,35
  or    %0000'0001            ;this way sprite y will never be 216
  ld    hl,Worldmapspat       ;y sprite 0 in spat
  ld    de,4
  ld    b,11                  ;first 11 sprites
  
.loop1:
  ld    (hl),a                ;set y
  add   hl,de                 ;next sprite
  add   a,16                  ;next bordermasksprite is 16 pixels lower
  djnz  .loop1

  ld    b,11                  ;next 11 sprites

.loop2:
  sub   a,16                  ;build up next 11 sprites from bottom of screen to top    
  ld    (hl),a
  add   hl,de                 ;next sprite
  djnz  .loop2
;/set y border masking sprites  

;set x border masking sprites
  ld    a,(horizontalscreenoffset)
  and   15
  ld    d,0
  ld    e,a
  ld    hl,horizontalscreenoffsettranslationtable
  add   hl,de
  ld    a,(hl)

  add   a,32
  
  ld    hl,Worldmapspat+1     ;x sprite 0 in spat
  ld    de,4
  ld    b,11                  ;first 11 sprites

.loop3:
  ld    (hl),a
  add   hl,de                 ;next sprite
  djnz  .loop3
  
  ld    b,11                  ;first 11 sprites
  add   a,224

.loop4:
  ld    (hl),a
  add   hl,de                 ;next sprite
  djnz  .loop4
;/set x border masking sprites  
  ret

WM_putmariosprite:
  ld    a,(marioyoshi)
  or    a
  jp    z,WmNotonYoshi

WMMariositsonYoshi:
;animate mario & yoshi sprite
  ld    a,(WMmariospriteanimation)
  or    a
  jp    z,.position00         ;0=mario front to camera dont animate
  dec   a
  jp    z,.position01         ;1=mario walk animation front to camera
  dec   a
  jp    z,.position02         ;2=mario walk animation back to camera
  dec   a
  jp    z,.position03         ;3=mario walk animation left to camera
  dec   a
  jp    z,.position04         ;4=mario walk animation right to camera
  dec   a
  jp    z,.position05         ;5=mario in water animation front to camera
  dec   a
  jp    z,.position06         ;6=mario in water animation back to camera
  dec   a
  jp    z,.position07         ;7=mario in water animation left to camera
  dec   a
  jp    z,.position08         ;8=mario in water animation right to camera
  dec   a
  jp    z,.position09         ;9=mario raise hand dont animate
  dec   a
  jp    z,.position10         ;10=mario in water raise hand dont animate
  dec   a
  jp    z,.position11         ;11=mario climbing
  
.position00:                ;0=mario front to camera dont animate  
  ld    hl,WM_sprite_marioandyoshifront1
  jp    WmNotonYoshi.setspriteposition

.position01:                ;1=mario walk animation front to camera
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-6 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshifront1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-7 * 256 + 1       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshifront2
  jp    WmNotonYoshi.setspriteposition

.position02:                ;2=mario walk animation back
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-5 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiback1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-5 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiback2
  jp    WmNotonYoshi.setspriteposition

.position03:                ;3=mario walk animation left
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-4 * 256 + 4       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshileft1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-5 * 256 + 4       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshileft2
  jp    WmNotonYoshi.setspriteposition

.position04:                ;4=mario walk animation right
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-3 * 256 - 4       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiright1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-4 * 256 - 4       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiright2
  jp    WmNotonYoshi.setspriteposition

.position05:                ;5=mario in water animation front
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-2 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshifrontwater1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-3 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshifrontwater2
  jp    WmNotonYoshi.setspriteposition

.position06:                ;6=mario in water animation back
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-1 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshibackwater1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-2 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshibackwater2
  jp    WmNotonYoshi.setspriteposition

.position07:                ;7=mario in water animation left
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-1 * 256 +4        ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshileftwater1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,0 * 256 + 4        ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshileftwater2
  jp    WmNotonYoshi.setspriteposition

.position08:                ;8=mario in water animation right
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,0 * 256 -4        ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshirightwater1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,1 * 256 - 4        ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshirightwater2
  jp    WmNotonYoshi.setspriteposition

.position09:                ;9=mario raise hand dont animate
  ld    de,-6 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiraisehand
  jp    WmNotonYoshi.setspriteposition

.position10:                ;10=mario in water raise hand dont animate
  ld    hl,WM_sprite_marioandyoshiraisehandwater
  ld    de,-2 * 256 + 0       ;mario yoshie (y,x) offsets
  jp    WmNotonYoshi.setspriteposition

.position11:                ;11=mario climbing
  ld    a,(framecounter)
  and   15
  cp    8
  ld    de,-5 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiback1
  jp    c,WmNotonYoshi.setspriteposition
  ld    de,-5 * 256 + 0       ;mario yoshie (y,x) offsets
  ld    hl,WM_sprite_marioandyoshiback2
  jp    WmNotonYoshi.setspriteposition

WmNotonYoshi:
;animate mario sprite
  ld    a,(WMmariospriteanimation)
  or    a
  jp    z,.position00         ;0=mario front to camera dont animate
  dec   a
  jp    z,.position01         ;1=mario walk animation front to camera
  dec   a
  jp    z,.position02         ;2=mario walk animation back to camera
  dec   a
  jp    z,.position03         ;3=mario walk animation left to camera
  dec   a
  jp    z,.position04         ;4=mario walk animation right to camera
  dec   a
  jp    z,.position05         ;5=mario in water animation front to camera
  dec   a
  jp    z,.position06         ;6=mario in water animation back to camera
  dec   a
  jp    z,.position07         ;7=mario in water animation left to camera
  dec   a
  jp    z,.position08         ;8=mario in water animation right to camera
  dec   a
  jp    z,.position09         ;9=mario raise hand dont animate
  dec   a
  jp    z,.position10         ;10=mario in water raise hand dont animate
  dec   a
  jp    z,.position11         ;11=mario climbing
  
.position00:                ;0=mario front to camera dont animate
  ld    hl,WM_sprite_mariofrontstraight
  jp    .setspriteposition

.position01:                ;1=mario walk animation front
  ld    a,(framecounter)
  and   31
  cp    8
  ld    hl,WM_sprite_mariofrontstraight
  jp    c,.setspriteposition
  cp    16
  ld    hl,WM_sprite_mariofrontleft
  jp    c,.setspriteposition
  cp    24
  ld    hl,WM_sprite_mariofrontstraight
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariofrontright
  jp    .setspriteposition

.position02:                ;2=mario walk animation back
  ld    a,(framecounter)
  and   31
  cp    8
  ld    hl,WM_sprite_mariobackstraight
  jp    c,.setspriteposition
  cp    16
  ld    hl,WM_sprite_mariobackleft
  jp    c,.setspriteposition
  cp    24
  ld    hl,WM_sprite_mariobackstraight
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariobackright
  jp    .setspriteposition

.position03:                ;3=mario walk animation left
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_marioleft1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_marioleft2
  jp    .setspriteposition

.position04:                ;4=mario walk animation right
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_marioright1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_marioright2
  jp    .setspriteposition

.position05:                ;5=mario in water animation front
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_mariowaterfront1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariowaterfront2
  jp    .setspriteposition

.position06:                ;6=mario in water animation back
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_mariowaterback1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariowaterback2
  jp    .setspriteposition
  
.position07:                ;7=mario in water animation left
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_mariowaterleft1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariowaterleft2
  jp    .setspriteposition

.position08:                ;8=mario in water animation right
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_mariowaterright1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_mariowaterright2
  jp    .setspriteposition   

.position09:                ;9=mario raise hand dont animate
  ld    hl,WM_sprite_marioraisehand
  jp    .setspriteposition

.position10:                ;10=mario in water raise hand dont animate
  ld    hl,WM_sprite_marioraisehandinwater
  jp    .setspriteposition

.position11:                ;11=mario climbing
  ld    a,(framecounter)
  and   15
  cp    8
  ld    hl,WM_sprite_marioclimbing1
  jp    c,.setspriteposition
  ld    hl,WM_sprite_marioclimbing2
  jp    .setspriteposition
  
.setspriteposition:
  push  de                    ;mario yoshie (y,x) offsets
  push  hl                    ;sprite character in ROM
;/animate mario sprite

;put mario sprite character
	ld		de,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
  ld    hl,mariospriteposition * 32
  add   hl,de
	ld		a,1
	call	SetVdp_Write

	ld		a,worldmapspritesblock
	call	block12               ;set blocks in page 1
  
	pop   hl                    ;sprite character in ROM
	ld		c,$98                 ;copy sprite character from ROM
	call	outix64               ;2 sprites (2 * 32 = 64 bytes)

  ld    a,(marioyoshi)
  or    a
  call  nz,outix64            ;2 sprites (2 * 32 = 64 bytes)
;/put mario sprite character

;put mario sprite color
	push  hl                    ;sprite color in ROM
	ld		de,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
  ld    hl,mariospriteposition * 16
  add   hl,de
	ld		a,1
	call	SetVdp_Write

	pop   hl                    ;sprite color in ROM
	call	outix32               ;2 sprites (2 * 16 = 32 bytes)

  ld    a,(marioyoshi)
  or    a
  call  nz,outix32            ;2 sprites (2 * 16 = 32 bytes)
;/put hero sprite color
;
;put mario y
  ld    hl,(WMmarioy)

  ld    a,(verticalscreenoffset)
  ld    b,a

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  jr    nz,.getmarioy
  
  ;correction in case marioy<mariocentrey (then camera is locked)
  ld    de,mariocentrey       ;if marioy<mariocentrey then camera is at y=0
  or    a                     ;reset carry
  sbc   hl,de
  ld    a,mariocentrey
  jr    nc,.notcarry
.getmarioy:
  ld    a,(WMmarioy)          ;cameray=0
  
  jr    .skipnextcheck
  
.notcarry:
  ;/correction in case marioy<mariocentrey (then camera is locked)


  ;correction in case marioy>maxCameray (then camera is locked)
   ld    hl,(WMmarioy)
  ld    de,maxCameray       ;if marioy>maxCameray then camera locked
  or    a                     ;reset carry
  sbc   hl,de
  ld    a,mariocentrey
  jr    c,.notcarry3
.getmarioy2:
  ld    a,(WMmarioy)          ;cameray=0
  sub   64
.notcarry3:
  ;/correction in case marioy>maxCameray (then camera is locked)

.skipnextcheck:




  
  add   a,b
  add   a,4                  ;small correction to marioy

  cp    216
  jr    nz,.not216
  inc   a  
.not216:
  
  ld    (Worldmapspat+0+((mariospriteposition+0)*4)),a
  ld    (Worldmapspat+0+((mariospriteposition+1)*4)),a

  pop   de                    ;mario yoshie (y,x) offsets
  add   a,d
  push  de

  cp    216
  jr    nz,.not216b
  inc   a  
.not216b:

  ld    (Worldmapspat+0+((mariospriteposition+2)*4)),a
  ld    (Worldmapspat+0+((mariospriteposition+3)*4)),a

  ;is yoshi present ?
  ld    a,(marioyoshi)
  or    a
  jp    nz,.endcheckyoshipresent

  ld    a,(verticalscreenoffset)
  add   a,216
  or    1
  ld    (Worldmapspat+0+((mariospriteposition+2)*4)),a
  ld    (Worldmapspat+0+((mariospriteposition+3)*4)),a
.endcheckyoshipresent:
  ;/is yoshi present ?

;/put mario y

;put mario x
  ld    hl,(WMmariox)  

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  jr    nz,.getmariox

  ;correction in case mariox>mariocentrexright (then camera is locked)
  ld    de,mariocentrexRight  ;if mariox>mariocentrexright then camerax=locked
  or    a                     ;reset carry
  sbc   hl,de
  ld    a,mariocentrex
  jr    c,.carry
  ld    a,(WMmariox)          ;camerax=0
  sub   32
  jr    .notcarry2
.carry:
  ld    hl,(WMmariox)  
  ;/correction in case mariox>mariocentrexright (then camera is locked)

  ;correction in case mariox<mariocentrex (then camera is locked)
  ld    de,mariocentrex       ;if mariox<mariocentrex then camerax=0
  or    a                     ;reset carry
  sbc   hl,de
  ld    a,mariocentrex
  jr    nc,.notcarry2
.getmariox:
  ld    a,(WMmariox)          ;camerax=0
.notcarry2:
  ;/correction in case mariox<mariocentrex (then camera is locked)

  add   a,23                  ;small correction to mariox

  ld    b,a
  ld    a,(horizontalscreenoffset)
  and   15
  ld    d,0
  ld    e,a
  ld    hl,horizontalscreenoffsettranslationtable
  add   hl,de
  ld    a,(hl)
  add   a,b
  
  ld    (Worldmapspat+1+((mariospriteposition+0)*4)),a
  ld    (Worldmapspat+1+((mariospriteposition+1)*4)),a

  pop   de                    ;mario yoshie (y,x) offsets
  add   a,e

  ld    (Worldmapspat+1+((mariospriteposition+2)*4)),a
  ld    (Worldmapspat+1+((mariospriteposition+3)*4)),a

;/put mario x
  ret

horizontalscreenoffsettranslationtable:
;h can have a value from 0 - 15 and the screen adjusts horizontally according to the table below
;     7   6   5   4   3   2   1   0   15  14  13  12  11  10  9   8
;H  left                       centre                           right
  db    -07,-06,-05,-04,-03,-02,-01,-00,-15,-14,-13,-12,-11,-10,-09,-08
;  db    08,09,10,11,12,13,14,15,00,01,02,03,04,05,06,07

putworldmapspattovram:
	ld		hl,(invisspratttableaddress)		;sprite attribute table in VRAM ($17600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,Worldmapspat       ;sprite attribute table
  ld    c,$98
	call	outix128
  ret

animateyellowandredleveldots:
  ld    a,04                    ;color yellow
  di
  out   ($99),a
  ld    a,16+128
  ei
  out   ($99),a
  
  ld    a,(framecounter)
  and   4
  ret   nz

  ld    a,(leveldotsanimationstep)
  inc   a
  and   15
  ld    (leveldotsanimationstep),a

  add   a,a
  ld    e,a
  ld    d,0
  
  ld    hl,yellowdotpalleteanimation
  add   hl,de
  ld    a,(hl)
  inc   hl
  di
  out   ($9a),a               ;red + blue
  ld    a,(hl)
  ei
  out   ($9a),a               ;green
  ret

yellowdotpalleteanimation:    ;red + blue, green
  db    7*16 + 0, 6
  db    7*16 + 1, 7
  db    7*16 + 2, 7
  db    7*16 + 3, 7

  db    7*16 + 4, 7
  db    7*16 + 5, 7
  db    7*16 + 6, 7
  db    7*16 + 7, 7

  db    7*16 + 7, 7
  db    7*16 + 6, 6
  db    7*16 + 5, 6
  db    7*16 + 4, 6

  db    7*16 + 3, 6
  db    7*16 + 2, 6
  db    7*16 + 1, 6
  db    7*16 + 0, 6

;reddotpalleteanimation:       ;red + blue, green
;  db    6*16 + 0, 0
;  db    7*16 + 0, 1
;  db    7*16 + 0, 2
;  db    7*16 + 0, 3

;  db    7*16 + 0, 3
;  db    7*16 + 0, 4
;  db    7*16 + 0, 4
;  db    7*16 + 0, 5

;  db    7*16 + 0, 4
;  db    7*16 + 0, 4
;  db    7*16 + 0, 3
;  db    7*16 + 0, 3

;  db    7*16 + 0, 2
;  db    7*16 + 0, 2
;  db    7*16 + 0, 1
;  db    6*16 + 0, 0

checkmakelevelavailable:
;check if a level has to be made available (this happens after you finish a level)
;if so then check the levelavailabilitylist (in variables.asm), and set the byte
;that this level belongs to
  ld    a,(makelevelavailable?)
  or    a
  ret   z

  ld    ix,makelevelavailableInfoList
  ld    de,lenghtlevelavailableInfoListobject
.listsearchloop:
  dec   a
  jr    z,.listfound
  add   ix,de
  jp    .listsearchloop  
.listfound:
  
  ld    h,(ix+1)
  ld    l,(ix)
  ld    a,(ix+4)
  
  ld    b,(hl)
  or    b
  cp    b
  ret   z                         ;this level is already available
  ld    (hl),a
  ;if a new level becomes available then lets activate its animation
  ld    a,1
  ld    (tileanimation?),a
  xor   a
  ld    (tileanimationstep),a
  ld    h,(ix+3)
  ld    l,(ix+2)
  ld    (tileanimationpointer),hl  
  ret

buildupmapSetavailablepaths:
  ld    a,(slot.page2rom)             ;all RAM except page 2
  out   ($a8),a 

  xor   a
  ld    (tileanimation?),a

  ld    a,(worldmapworld)
  cp    1
  ld    iy,.world1Paths  
  jr    z,.loopPaths

  cp    2
  ld    iy,.world2Paths  
  jr    z,.loopPaths

  cp    3
  ld    iy,.world3Paths  
  jr    z,.loopPaths

  cp    5
  ld    iy,.world5Paths  
  jr    z,.loopPaths
  
  cp    7
  ld    iy,.world7Paths  
  jr    z,.loopPaths

  cp    8
  ld    iy,.world8Paths  
  jr    z,.loopPaths
  ret
  
.loopPaths:
  ld    a,(iy+0)        ;check in levelavailabilitylist if this level is available
  ld    b,(iy+1)
  or    b
  ret   z

  ld    l,(iy+2)
  ld    h,(iy+3)
  push  hl
  pop   ix

  ld    l,(iy+0)
  ld    h,(iy+1)
  ld    a,(iy+4)
  and   (hl)
  call  nz,setpathinmapinram

  ld    de,5
  add   iy,de           ;next location in .LocationJumpTableTubesAndStars
  jr    .loopPaths

.world1Paths:
dw  levelavailabilitylist+00,WMAnimYoIsl1toYeSwPa : db %0001'0000
dw  levelavailabilitylist+00,WMAnimYoIsl2toYoIsl3 : db %0000'1000
dw  levelavailabilitylist+00,WMAnimYoIsl3toYoIsl4 : db %0000'0100
dw  levelavailabilitylist+00,WMAnimYoIsl4toIggyCa : db %0000'0010
dw  levelavailabilitylist+00,WMAnimIggyCatoDoPla1 : db %0000'0001
dw  0

.world2Paths:
dw  levelavailabilitylist+00,WMAnimIggyCatoDoPla1InWorld2 : db %0000'0001

dw  levelavailabilitylist+01,WMAnimDoPla1toDoPla2 : db %1000'0000
dw  levelavailabilitylist+01,WMAnimDoPla1toDoSec1 : db %0100'0000
dw  levelavailabilitylist+01,WMAnimDoPla2toGrSwPa : db %0010'0000
dw  levelavailabilitylist+01,WMAnimDoPla2toDoGhos : db %0001'0000
dw  levelavailabilitylist+01,WMAnimDoSec1toDoGhos : db %0000'1000
dw  levelavailabilitylist+01,WMAnimDoSec1toDoSeGh : db %0000'0100
dw  levelavailabilitylist+01,WMAnimDoSeGhtoStarA1 : db %0000'0010
dw  levelavailabilitylist+01,WMAnimDoSeGhtoTubeG1 : db %0000'0001

dw  levelavailabilitylist+02,WMAnimDoGhostoTopSec : db %1000'0000
dw  levelavailabilitylist+02,WMAnimDoGhostoDoPla3 : db %0100'0000
dw  levelavailabilitylist+02,WMAnimTubeH1toDoPla3 : db %0010'0000
dw  levelavailabilitylist+02,WMAnimDoPla3toDoPla4 : db %0001'0000
dw  levelavailabilitylist+02,WMAnimDoPla4toMortCa : db %0000'1000
dw  levelavailabilitylist+02,WMAnimMortCatoVaDom1 : db %0000'0100
dw  levelavailabilitylist+02,WMAnimYeSwPaFinished : db %0000'0010
dw  levelavailabilitylist+02,WMAnimGrSwPaFinished : db %0000'0001

dw  levelavailabilitylist+04,WMAnimVaSec2toVaSec3 : db %0000'0010
dw  levelavailabilitylist+04,WMAnimTubeJ1toCheese : db %0000'1000

dw  levelavailabilitylist+05,WMAnimVaForttoButBr1 : db %1000'0000
dw  levelavailabilitylist+05,WMAnimButBr1toButBr2 : db %0100'0000
dw  levelavailabilitylist+05,WMAnimButBr2toLudwCa : db %0010'0000
dw  levelavailabilitylist+05,WMAnimCheesetoSodaLa : db %0001'0000
dw  levelavailabilitylist+05,WMAnimCheesetoCookie : db %0000'1000
dw  levelavailabilitylist+05,WMAnimCookietoLudwCa : db %0000'0100
dw  levelavailabilitylist+05,WMAnimLudwCatoForIl1 : db %0000'0010

dw  levelavailabilitylist+07,WMAnimForFortoStarC1 : db %1000'0000
dw  levelavailabilitylist+07,WMAnimForIl3toRoysCa_InWorld2 : db %0100'0000
dw  levelavailabilitylist+07,WMAnimRoysCatoChoIs1 : db %0010'0000
dw  levelavailabilitylist+07,WMAnimChoIs1toChoGho : db %0001'0000
dw  levelavailabilitylist+07,WMAnimChoGhotoChoIs2 : db %0000'1000
dw  levelavailabilitylist+07,WMAnimChoIs2toTubeK1 : db %0000'0100
dw  levelavailabilitylist+07,WMAnimChoIs2toChoIs3 : db %0000'0010
dw  levelavailabilitylist+07,WMAnimChoIs3toCircle : db %0000'0001

dw  levelavailabilitylist+08,WMAnimChoIs3toChoFor : db %1000'0000
dw  levelavailabilitylist+08,WMAnimChoFortoChoIs4 : db %0100'0000
dw  levelavailabilitylist+08,WMAnimChoIs4toChoIs5 : db %0010'0000
dw  levelavailabilitylist+08,WMAnimChoIs5toWendCa : db %0001'0000
dw  levelavailabilitylist+08,WMAnimWendCatoSunkGh : db %0000'1000
dw  levelavailabilitylist+08,WMAnimSunkGhtoValBo1 : db %0000'0100

dw  levelavailabilitylist+10,WMAnimStarW3toStarD2_InWorld2 : db %0010'0000
dw  0

.world3Paths:
dw  levelavailabilitylist+04,WMAnimVaDom2toVaGhos : db %1000'0000
dw  levelavailabilitylist+04,WMAnimVaGhostoVaDom3 : db %0100'0000
dw  levelavailabilitylist+04,WMAnimVaDom3toVaDom4 : db %0010'0000
dw  levelavailabilitylist+04,WMAnimVaDom4toLemmCa : db %0001'0000
dw  levelavailabilitylist+04,WMAnimLemmCatoTubeJ2 : db %0000'1000
dw  levelavailabilitylist+04,WMAnimLocationStarB1 : db %0000'0100

dw  levelavailabilitylist+03,WMAnimVaDom1toVaDom2 : db %0001'0000
dw  levelavailabilitylist+03,WMAnimVaDom1toVaSec1 : db %0000'1000
dw  levelavailabilitylist+03,WMAnimVaDom2toReSwPa : db %0000'0100
dw  levelavailabilitylist+03,WMAnimReSwPaFinished : db %1000'0000
dw  levelavailabilitylist+03,WMAnimVaSec1toStarB1 : db %0000'0010
dw  levelavailabilitylist+03,WMAnimVaSec1toTubeI2 : db %0000'0001
dw  0

.world5Paths:
dw  levelavailabilitylist+05,WMAnimForIl1toFoGhos : db %0000'0001

dw  levelavailabilitylist+06,WMAnimForIl1toForIl2 : db %1000'0000
dw  levelavailabilitylist+06,WMAnimForIl2toForIl3 : db %0100'0000
dw  levelavailabilitylist+06,WMAnimForIl2toBlSwPa : db %0010'0000
dw  levelavailabilitylist+06,WMAnimForIl3toFoGhos : db %0001'0000
dw  levelavailabilitylist+06,WMAnimFoGhostoForIl4 : db %0000'1000
dw  levelavailabilitylist+06,WMAnimForIl4toForIl2 : db %0000'0100
dw  levelavailabilitylist+06,WMAnimForIl4toForSec : db %0000'0010
dw  levelavailabilitylist+06,WMAnimForSectoForFor : db %0000'0001

dw  levelavailabilitylist+07,WMAnimForIl3toRoysCa : db %0100'0000

dw  levelavailabilitylist+03,WMAnimBlSwPaFinished : db %0100'0000
dw  0

.world7Paths:
dw  levelavailabilitylist+01,WMAnimTubeG2toDoSec2 : db %0000'0001

dw  levelavailabilitylist+02,WMAnimDoSec2toTubeH2 : db %0010'0000

dw  levelavailabilitylist+08,WMAnimSunkGhtoValBo1_InWorld7 : db %0000'0100
dw  levelavailabilitylist+08,WMAnimValBo1toValBo2 : db %0000'0010
dw  levelavailabilitylist+08,WMAnimValBo2toValFor : db %0000'0001

dw  levelavailabilitylist+09,WMAnimValFortoBackDo : db %1000'0000
dw  levelavailabilitylist+09,WMAnimValBo2toValGho : db %0100'0000
dw  levelavailabilitylist+09,WMAnimValGhotoLarrCa : db %0010'0000
dw  levelavailabilitylist+09,WMAnimValGhotoValBo3 : db %0001'0000
dw  levelavailabilitylist+09,WMAnimValBo3toValBo4 : db %0000'1000
dw  levelavailabilitylist+09,WMAnimValBo4toStarD1 : db %0000'0100
dw  levelavailabilitylist+09,WMAnimLarrCatoFrontD : db %0000'0010
dw  levelavailabilitylist+09,WMAnimValBo4toLarrCa : db %0000'0001

dw  levelavailabilitylist+10,WMAnimStarW4toStarE2_InWorld7 : db %0001'0000
dw  0

.world8Paths:
dw  levelavailabilitylist+10,WMAnimStarW1toStarB2 : db %1000'0000
dw  levelavailabilitylist+10,WMAnimStarW2toStarC2 : db %0100'0000
dw  levelavailabilitylist+10,WMAnimStarW3toStarD2 : db %0010'0000
dw  levelavailabilitylist+10,WMAnimStarW4toStarE2 : db %0001'0000

dw  levelavailabilitylist+10,WMAnimStarW5toStarF2 : db %0000'0100
dw  0


makelevelavailableInfoList: 
  dw  levelavailabilitylist+00, WMAnimYoIsl1toYeSwPa : db %0001'0000 ;byte0: GameSttoYosHou,YosHoutoYoIsl1,YosHoutoYoIsl2,YoIsl1toYeSwPa,  YoIsl2toYoIsl3,YoIsl3toYoIsl4,YoIsl4toIggyCa,IggyCatoDoPla1
lenghtlevelavailableInfoListobject: equ $-makelevelavailableInfoList
  dw  levelavailabilitylist+00, WMAnimYoIsl2toYoIsl3 : db %0000'1000 ;byte0: GameSttoYosHou,YosHoutoYoIsl1,YosHoutoYoIsl2,YoIsl1toYeSwPa,  YoIsl2toYoIsl3,YoIsl3toYoIsl4,YoIsl4toIggyCa,IggyCatoDoPla1
  dw  levelavailabilitylist+00, WMAnimYoIsl3toYoIsl4 : db %0000'0100 ;byte0: GameSttoYosHou,YosHoutoYoIsl1,YosHoutoYoIsl2,YoIsl1toYeSwPa,  YoIsl2toYoIsl3,YoIsl3toYoIsl4,YoIsl4toIggyCa,IggyCatoDoPla1
  dw  levelavailabilitylist+00, WMAnimYoIsl4toIggyCa : db %0000'0010 ;byte0: GameSttoYosHou,YosHoutoYoIsl1,YosHoutoYoIsl2,YoIsl1toYeSwPa,  YoIsl2toYoIsl3,YoIsl3toYoIsl4,YoIsl4toIggyCa,IggyCatoDoPla1
  dw  levelavailabilitylist+00, WMAnimIggyCatoDoPla1 : db %0000'0001 ;byte0: GameSttoYosHou,YosHoutoYoIsl1,YosHoutoYoIsl2,YoIsl1toYeSwPa,  YoIsl2toYoIsl3,YoIsl3toYoIsl4,YoIsl4toIggyCa,IggyCatoDoPla1

  dw  levelavailabilitylist+01, WMAnimDoPla1toDoPla2 : db %1000'0000 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1
  dw  levelavailabilitylist+01, WMAnimDoPla1toDoSec1 : db %0100'0000 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1
  dw  levelavailabilitylist+01, WMAnimDoPla2toGrSwPa : db %0010'0000 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1
  dw  levelavailabilitylist+01, WMAnimDoPla2toDoGhos : db %0001'0000 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1
  dw  levelavailabilitylist+01, WMAnimDoSec1toDoGhos : db %0000'1000 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1

  dw  levelavailabilitylist+01, WMAnimDoSec1toDoSeGh : db %0000'0100 ;byte1: DoPla1toDoPla2,DoPla1toDoSec1,DoPla2toGrSwPa,DoPla2toDoGhos,  DoGhostoDoPla2,DoSec1toDoSeGh,DoSec1toDoGhos,DoGhostoDoSec1
  dw  levelavailabilitylist+01, WMAnimDoSeGhtoStarA1 : db %0000'0010 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4
  dw  levelavailabilitylist+01, WMAnimDoSeGhtoTubeG1 : db %0000'0001 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4
  dw  levelavailabilitylist+02, WMAnimDoGhostoTopSec : db %1000'0000 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4
  dw  levelavailabilitylist+02, WMAnimDoGhostoDoPla3 : db %0100'0000 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4

  dw  levelavailabilitylist+02, WMAnimTubeH1toDoPla3 : db %0010'0000 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4
  dw  levelavailabilitylist+02, WMAnimDoPla3toDoPla4 : db %0001'0000 ;byte2: DoGhostoTopSec,DoSeGhtoStarA1,DoSeGhtoTubeG1,DoGhostoDoPla3,  DoPla3toDoGhos,TubeH1toDoPla3,DoPla3toTubeH1,DoPla3toDoPla4
  dw  levelavailabilitylist+02, WMAnimDoPla4toMortCa : db %0000'1000 ;byte3: 
  dw  levelavailabilitylist+02, WMAnimMortCatoVaDom1 : db %0000'0100 ;byte3: 
  dw  levelavailabilitylist+02, WMAnimYeSwPaFinished : db %0000'0010 ;byte3: 

  dw  levelavailabilitylist+02, WMAnimGrSwPaFinished : db %0000'0001 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimReSwPaFinished : db %1000'0000 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimBlSwPaFinished : db %0100'0000 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimVaDom1toVaDom2 : db %0001'0000 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimVaDom1toVaSec1 : db %0000'1000 ;byte3: 

  dw  levelavailabilitylist+03, WMAnimVaDom2toReSwPa : db %0000'0100 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimVaSec1toStarB1 : db %0000'0010 ;byte3: 
  dw  levelavailabilitylist+03, WMAnimVaSec1toTubeI2 : db %0000'0001 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimVaDom2toVaGhos : db %1000'0000 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimVaGhostoVaDom3 : db %0100'0000 ;byte3: 

  dw  levelavailabilitylist+04, WMAnimVaDom3toVaDom4 : db %0010'0000 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimVaDom4toLemmCa : db %0001'0000 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimLemmCatoTubeJ2 : db %0000'1000 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimLocationStarB1 : db %0000'0100 ;byte3: 
  dw  levelavailabilitylist+04, WMAnimVaSec2toVaSec3 : db %0000'0010 ;byte3: 

  dw  levelavailabilitylist+05, WMAnimVaForttoButBr1 : db %1000'0000 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimButBr1toButBr2 : db %0100'0000 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimButBr2toLudwCa : db %0010'0000 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimCheesetoSodaLa : db %0001'0000 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimCheesetoCookie : db %0000'1000 ;byte3: 

  dw  levelavailabilitylist+05, WMAnimCookietoLudwCa : db %0000'0100 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimLudwCatoForIl1 : db %0000'0010 ;byte3: 
  dw  levelavailabilitylist+05, WMAnimForIl1toFoGhos : db %0000'0001 ;byte3: 
  dw  levelavailabilitylist+06, WMAnimForIl1toForIl2 : db %1000'0000
  dw  levelavailabilitylist+06, WMAnimForIl2toForIl3 : db %0100'0000

  dw  levelavailabilitylist+06, WMAnimForIl2toBlSwPa : db %0010'0000
  dw  levelavailabilitylist+06, WMAnimForIl3toFoGhos : db %0001'0000
  dw  levelavailabilitylist+06, WMAnimFoGhostoForIl4 : db %0000'1000
  dw  levelavailabilitylist+06, WMAnimForIl4toForIl2 : db %0000'0100
  dw  levelavailabilitylist+06, WMAnimForIl4toForSec : db %0000'0010

  dw  levelavailabilitylist+06, WMAnimForSectoForFor : db %0000'0001
  dw  levelavailabilitylist+07, WMAnimForFortoStarC1 : db %1000'0000
  dw  levelavailabilitylist+07, WMAnimForIl3toRoysCa : db %0100'0000
  dw  levelavailabilitylist+07, WMAnimRoysCatoChoIs1 : db %0010'0000
  dw  levelavailabilitylist+07, WMAnimChoIs1toChoGho : db %0001'0000

  dw  levelavailabilitylist+07, WMAnimChoGhotoChoIs2 : db %0000'1000
  dw  levelavailabilitylist+07, WMAnimChoIs2toTubeK1 : db %0000'0100
  dw  levelavailabilitylist+07, WMAnimChoIs2toChoIs3 : db %0000'0010
  dw  levelavailabilitylist+07, WMAnimChoIs3toCircle : db %0000'0001
  dw  levelavailabilitylist+08, WMAnimChoIs3toChoFor : db %1000'0000

  dw  levelavailabilitylist+08, WMAnimChoFortoChoIs4 : db %0100'0000
  dw  levelavailabilitylist+08, WMAnimChoIs4toChoIs5 : db %0010'0000
  dw  levelavailabilitylist+08, WMAnimChoIs5toWendCa : db %0001'0000
  dw  levelavailabilitylist+08, WMAnimWendCatoSunkGh : db %0000'1000
  dw  levelavailabilitylist+08, WMAnimSunkGhtoValBo1 : db %0000'0100
  
  dw  levelavailabilitylist+08, WMAnimValBo1toValBo2 : db %0000'0010
  dw  levelavailabilitylist+08, WMAnimValBo2toValFor : db %0000'0001
  dw  levelavailabilitylist+09, WMAnimValFortoBackDo : db %1000'0000
  dw  levelavailabilitylist+09, WMAnimValBo2toValGho : db %0100'0000
  dw  levelavailabilitylist+09, WMAnimValGhotoLarrCa : db %0010'0000

  dw  levelavailabilitylist+09, WMAnimValGhotoValBo3 : db %0001'0000
  dw  levelavailabilitylist+09, WMAnimValBo3toValBo4 : db %0000'1000
  dw  levelavailabilitylist+09, WMAnimValBo4toStarD1 : db %0000'0100
  dw  levelavailabilitylist+09, WMAnimLarrCatoFrontD : db %0000'0010
  dw  levelavailabilitylist+09, WMAnimValBo4toLarrCa : db %0000'0001

  dw  levelavailabilitylist+10, WMAnimStarW1toStarB2 : db %1000'0000
  dw  levelavailabilitylist+10, WMAnimStarW2toStarC2 : db %0100'0000
  dw  levelavailabilitylist+10, WMAnimStarW3toStarD2 : db %0010'0000
  dw  levelavailabilitylist+10, WMAnimStarW4toStarE2 : db %0001'0000
  dw  levelavailabilitylist+10, WMAnimStarW5toStarF2 : db %0000'0100

;	include	"worldmap_tileanimations.asm"	
setpathinmapinram:
;we have to read out our worlmap tileanimations from a Rom block (WorldmapTileAnimationsRomBlock)
  ld    a,(slot.page12rom)             ;all RAM except page 1+2
  out   ($a8),a 
	ld		a,WorldMapTileAnimationsBlock
	call	block12		          ;set blocks in page 1

  ld    a,(ix)          ;amount of tiles to put

  and   %0011'1111      ;bit 7 and 6 are used for sfx
  ld    b,a             ;amount of tiles to put
  or    a

;we have to write our tilenumbers to ram in page 1
  ld    a,(slot.page2rom)             ;all RAM except page 2
  out   ($a8),a 
;  ei  


  ret   z

  inc   ix              ;tilenumber (16 bit)

.loop:
  push  bc
  call  .settile
  pop   bc
  djnz  .loop      
  jp    setpathinmapinram

.settile:
;we have to read out our worlmap tileanimations from a Rom block (WorldmapTileAnimationsRomBlock)
  ld    a,(slot.page12rom)             ;all RAM except page 1+2
  out   ($a8),a 

  ;prepare tilenumber in bc to be put in ram map later
  ld    c,(ix)
  ld    b,(ix+1)          ;tilenumber in bc  
  inc   bc   
  ;/prepare tilenumber in bc to be put in ram map later

  inc   ix
  inc   ix                ;dy

  ;prepare y value of tile in ram map
  ld    h,0
  ld    l,(ix)            ;dy in hl

  add   hl,hl             ;*2
  add   hl,hl             ;*4
  add   hl,hl             ;*8
  add   hl,hl             ;*16
  add   hl,hl             ;*32
  add   hl,hl             ;*64
  ld    a,(worldmapworld) ;world 2 is 2x wider than the other worlds
  cp    2
  jr    nz,.notworld2
  add   hl,hl             ;*128  
.notworld2:
  ld    de,$4000
  add   hl,de
  ;/prepare y value of tile in ram map

  inc   ix                ;dx
  ld    a,(ix)            ;x of tile
  add   a,a               ;*2 on the x-axis a tile uses 2 bytes

  ;prepare x,y value of tile in ram map
  ld    d,0
  ld    e,a
  add   hl,de             ;hl=(y,x) of tile in map in ram

;we have to write our tilenumbers to ram in page 1
  ld    a,(slot.page2rom)             ;all RAM except page 2
  out   ($a8),a 
  
  ld    (hl),c            ;put tilenumber in map in ram
  inc   hl
  ld    (hl),b
  ;/prepare x,y value of tile in ram map
  inc   ix
  ret

tileanimation:
;we have to read out our worlmap tileanimations from a Rom block (WorldmapTileAnimationsRomBlock)
  ld    a,(slot.page12rom)             ;all RAM except page 1+2
  out   ($a8),a 
	ld		a,WorldMapTileAnimationsBlock
	call	block12		          ;set blocks in page 1

  ld    a,(tileanimation?)
  or    a
  ret   z

  ;play sfx when animating the new map pieces
  ld    a,(tileanimationstep)
  cp    2
  jr    nz,.endplaysfx

  ld    ix,(tileanimationpointer)
  ld    a,(ix)          ;amount of tiles to put (bit 7=0 slow animation speed, bit 7=1 fast animation speed)
  and   %1100'0000      ;bit 7 and 6 are used for sfx

  push  af
  ;cp    %0000'0000
  call  z,Playsfx.mapcreatepath
  pop   af
  cp    %1000'0000
  push af
  call  z,Playsfx.mapexplosion
  pop af
  push af
  call z,exittosavemenu
  pop   af
  cp    %1100'0000
;  call  z,playsfx.xxxxxxxxxxxxx
  
.endplaysfx:
  ;/play sfx when animating the new map pieces
 
  ld    ix,(tileanimationpointer)
  ld    a,(ix)          ;amount of tiles to put (bit 7=0 slow animation speed, bit 7=1 fast animation speed)
  and   %0011'1111      ;bit 7 and 6 are used for sfx
  jr    z,.end          ;if no more tiles need to be put, then end this routine

  ld    b,a             ;amount of tiles to put
  inc   ix              ;tilenumber (16 bit)

.loop:
  push  bc
  call  .settile
  pop   bc
  djnz  .loop
        
  ld    a,(tileanimationstep)
  inc   a
  and   15              ;animation speed
  ld    (tileanimationstep),a
  ret   nz
  
  ld    (tileanimationpointer),ix
  ret

.end:
  xor   a
  ld    (tileanimation?),a
  
  ;when the tileanimation is done a new path is completely put in screen
  ;all we need to do now is put this new path also in the map in ram
;  ld    a,(slot.page2rom)             ;all RAM except page 2
;  out   ($a8),a 
  call  buildupmapSetavailablepaths
;  ld    a,(slot.page12rom)            ;all RAM except page 12
;  out   ($a8),a   
  ret

.settile:
  call  setsxandxy16bit   ;ix->tilenumber

  inc   ix
  inc   ix                ;dy
  ld    a,(ix)

  ld    a,(camerayintiles)
  ld    d,a
  ld    a,(ix)    
  sub   a,d

  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8
  
  ld    (copytile+dy),a
  inc   ix                ;dx
  ld    a,(ix)

  ld    a,(cameraxintiles)
  ld    d,a
  ld    a,(ix)    
  sub   a,d
 
  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8
  
  ld    (copytile+dx),a

  ld    a,(currentpage)
  cp    0*32+31     ;set page
  ld    a,0
  jr    z,.Setdpage
  ld    a,1
.Setdpage:
  ld    (copytile+dpage),a  
  inc   ix
  
  ld    hl,copytile
  call  DoCopy

  ld    a,(copytile+dpage)
  xor   1
  ld    (copytile+dpage),a

  ld    a,(copytile+dx)
  add   a,16
  ld    (copytile+dx),a

  ld    hl,copytile
  call  DoCopy  
  ret

setsxandxy16bit:          ;ix->tilenumber
  ld    l,(ix)
  ld    h,(ix+1)
  ld    de,32
  or    a                 ;reset carry flag
  ld    b,0               ;sy
  
.yloop:
  sbc   hl,de
  jr    c,.yfound
  inc   b                 ;sy (/8)
  jr    .yloop

.yfound: 
  add   hl,de
  ld    a,l
  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8
  
  ld    (copytile+sx),a
  
  ld    a,b

  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8

  ld    (copytile+sy),a
  ret

animatewater:
  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    3                 ;only world 1 and 2 have water animation
  ret   nc

  ld    a,0             ;write color 0 + 1
  di
  out   ($99),a
  ld    a,16+128
  ei
  out   ($99),a
  
  ld    a,(framecounter)
  and   63
  ret   nz
  
  ld    a,(framecounter)
  and   127
  cp    64
  jp    c,.pos2

.pos1:
  ld    a,5*16 + 6
  di
  out   ($9a),a         ;red + blue
  ld    a,5
  ei
  out   ($9a),a         ;green

  ld    a,0*16 + 7
  di
  out   ($9a),a         ;red + blue
  ld    a,2
  ei
  out   ($9a),a         ;green
  ret

.pos2:
  ld    a,0*16 + 7
  di
  out   ($9a),a         ;red + blue
  ld    a,2
  ei
  out   ($9a),a         ;green

  ld    a,5*16 + 6
  di
  out   ($9a),a         ;red + blue
  ld    a,5
  ei
  out   ($9a),a         ;green
  ret

v9958centerscreen:
;set horizontal scroll offset v9958
  ld    a,0
	ld		(VDP_8+21),a ;Hey gast dit klopt allemaal niet zo
  di
	out		($99),a
	ld		a,27+128
	ei
	out		($99),a
;/set horizontal scroll offset v9958

;set horizontal scroll offset v9938  
  ld    a,4
	ld		(VDP_8+10),a
  di
  out   ($99),a
  ld    a,18+128
  ei
  out   ($99),a
;/set horizontal scroll offset v9938
  ret

v9938centerscreen:
;set horizontal scroll offset v9958
;  ld    a,0
;	ld		(VDP_8+21),a
;  di
;	out		($99),a
;	ld		a,27+128
;	ei
;	out		($99),a
;/set horizontal scroll offset v9958

;set horizontal scroll offset v9938  
  ld    a,0
	ld		(VDP_8+10),a
  di
  out   ($99),a
  ld    a,18+128
  ei
  out   ($99),a
;/set horizontal scroll offset v9938
  ret

buildupmap:
;build up page 0 at cameras position  
  xor   a
  ld    (copytile+dpage),a  
  ld    ix,worldmapStartAddress
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  call  .dobuildup

;build up page 1 2 tiles left of cameras position  
  ld    a,1
  ld    (copytile+dpage),a  
  ld    ix,worldmapStartAddress
  ld    de,(worldmapcameraposition) ;128*5 + 2*5      ;camera position 128* y + 2*x, camera points to a tile
  add   ix,de
  ld    de,-4
  add   ix,de
  call  .dobuildup

  xor   a
  ld    (copytile+dpage),a  
  ret

.dobuildup:
  ld    a,(yoffsetbuildupscreen)
  ld    (copytile+dy),a
  xor   a
  ld    (copytile+dx),a   ;start copying at (0,36) on the map (right below the status bar)
    
  ld    b,32              ;32 tiles wide
  ld    c,32              ;32 tiles high

.loop:
  push  bc
  call  setsxandsy16bit   ;ix->tilenumber
    
  ld    hl,copytile
  call  DoCopy

  ld    a,(copytile+dx)
  add   a,8
  ld    (copytile+dx),a

  inc   ix
  inc   ix
  pop   bc
  djnz  .loop

  ld    de,64             ;next line

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has a map size of 64 tiles
  jr    nz,.notzero       ;all the other worlds are 32 tiles in size
  add   ix,de             ;so only add 64 bytes if this is world 2
.notzero:

  ld    a,(copytile+dy)
  add   a,8
  ld    (copytile+dy),a

  ld    b,32
  dec   c
  jr    nz,.loop
  ret

setsxandsy16bit:          ;ix->tilenumber
  ld    l,(ix)
  ld    h,(ix+1)

  dec   hl ;small correction here
    
  ld    de,32
  or    a                 ;reset carry flag
  ld    b,0               ;sy
  
.yloop:
  sbc   hl,de
  jr    c,.yfound
  inc   b                 ;sy (/8)
  jr    .yloop

.yfound: 
  add   hl,de
  ld    a,l
  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8
  ld    (copytile+sx),a
  
  ld    a,b
  add   a,a               ;*2
  add   a,a               ;*4
  add   a,a               ;*8

  ld    (copytile+sy),a
  ret

;Waitvdpready:
;  ld    a,2
;  di
;  out   ($99),a
;  ld    a,15+128
;  out   ($99),a
;  in    a,($99)
;  rra
;  ld    a,0
;  out   ($99),a
;  ld    a,15+128
;  ei
;  out   ($99),a
;  jr    c,Waitvdpready
;  ret

DoCopy:
  ld    a,32
  di
  out   ($99),a
  ld    a,17+128
  ei
  out   ($99),a
  ld    c,$9b
.vdpready:
  ld    a,2
  di
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)
  rra
  ld    a,0
  out   ($99),a
  ld    a,15+128
  ei
  out   ($99),a
  jr    c,.vdpready
	dw    $a3ed,$a3ed,$a3ed,$a3ed
	dw    $a3ed,$a3ed,$a3ed,$a3ed
	dw    $a3ed,$a3ed,$a3ed,$a3ed
	dw    $a3ed,$a3ed,$a3ed
  ret

;check for the exit button and act to it if found
readexit:
  ld    a,(WMmariomove?)
  or    a
  jp    nz,WorldMapengine.return                    ;no control interaction when mario is moving

	ld    b,7
  
	in	  a,($AA)
	and   $F0		;only change bits 0-3
	or    b		;take row number from B
	out   ($AA),a
	in    a,($A9)		;read row into A
	
	bit   2,a
	jp    z,.exittooptionsmenu
	;hiero
	;TEMP: this is just for testing remove after finished
 ; bit 3,a;;;;;;;;;
 ; call z,exittosavemenu;;;;;;;;;;
  ;//////////////remove above after finished!!

  jp    WorldMapengine.return

.exittooptionsmenu:

;  di

  call  Settempisr                                     ;disable line int, and set a temp isr
  
;  ld a,(slot.bios) ;reset slot config and call bios back
;  out ($a8),a
;  
;  call $41 ;disable screen
 
  call disablescreen
  
  call stopmusic ;stop the music
  
  call openmenuoptions ;call the menu from page 3

  jp worldmap ;restart the worldmap    
  
  
exitworldmap:
  call  Settempisr                                     ;disable line int, and set a temp isr 

  call disablescreen
  call stopmusic
  call clearallvram
 
 
  ld    a,(VDP_8)         ;sprites on
  and   %11111101
  ld    (VDP_8),a
  out   ($99),a
  ld    a,8+128
  out   ($99),a 
 
  xor a
  ld (endworldmap),a
 
;switch to screen 4
  ld    a,(VDP_0)
  and   %1111'1101		      ;m3=0
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 4

;border masking on (Mask 8 pixels at edges)
	ld		a,(VDP_8+19)
  or    %0000'0010
;  and   %1111'1101
	ld		(VDP_8+19),a
	di
	out		($99),a
	ld		a,25+128
	ei
	out		($99),a

;backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 
  ld    a,%0000'1111          ;color 15= black, backdrop black
;  ld    a,%0000'1111
  di
  out   ($99),a
  ld    a,7+128
  ei
  out   ($99),a
;/backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 

  ld    a,(VDP_0+1)       ;screen off
  and   %1011'1111
  di
  out   ($99),a
  ld    a,1+128
  ei
  out   ($99),a 

 ; call  Settempisr                                     ;disable line int, and set a temp isr  
  ret  

worldmapinitiation:
;switch to screen 5, center screen, set transparancy mode off, backdrop color
;border masking off, screen width one page, set sprite table addresses, setpage
;set horizontalscreenoffset, set horizontalscrollstep, reset yoffsetbuildupscreen
;reset Worldmaplineintflag, reset framecounter, reset WMenterlevelwaittimer
;reset switchworld?, copy object/eventspritelist, clear sprites from spat
;set worldmapsong

;switch to screen 5
  ld    a,(VDP_0)
  or    %0000'0010		      ;m3=1
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 5  

;center screen
  ld    a,(v9958found?)
  or    a
  call  nz,v9958centerscreen
  call  z,v9938centerscreen
;/center screen

;screenmode transparancy
	ld		a,(VDP_8)
	or		%0010'0000          ;transparant mode off
	ld		(VDP_8),a
	di
	out		($99),a
	ld		a,8+128
	ei
	out		($99),a
;/screenmode transparancy

;backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 
  ld    a,7          ;color 7= black, backdrop black
;  ld    a,%0000'1111
  di
  out   ($99),a
  ld    a,7+128
  ei
  out   ($99),a
;/backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 

; 26   25     0   CMD  VDS  YAE  YJK  WTE  MSK  SP2
;CMD     - Command mode (0: normal; 1: screen 2-4 as screen 8)
;VDS     - ???
;YAE     - Colour palette RGB output
;YJK     - YJK system
;WTE     - Send a wait-signal to the CPU
;MSK     - Mask 8 pixels at edges
;SP2     - Screen width for hor. scroll (0: one page; 1: two pages)
;26   25     0   CMD  VDS  YAE  YJK  WTE  MSK  SP2
;border masking off and set Screen width one page
	ld		a,(VDP_8+19)
;  or    %0000'0010
  and   %1111'1101
	ld		(VDP_8+19),a
	di
	out		($99),a
	ld		a,25+128
	ei
	out		($99),a
;/border masking off and set Screen width one page

;  ld    hl,ClearVram
;  call  DoCopy
;  call  Waitvdpready

;set sprite table addresses
;  ld    hl,$6c00        ;spr color table $16c00
;  ld    de,$7400        ;spr color table buffer $17400
;	ld		a,%1101'1111    ;spr att table to $16e00    
;	ld		b,%0010'1110    ;spr chr table to $17000
;  jp    z,.setspritetables
  ld    hl,$7400        ;spr color table $17400
  ld    de,$6c00        ;spr color table buffer $16c00
	ld    a,%1110'1111    ;spr att table to $17600
	ld    b,%0010'1111    ;spr chr table to $17800

  ;sprcoltableaddress:       $17400  at end of page 2
  ;spratttableaddress:       $17600
  ;sprchatableaddress:       $17800
  ;invissprcoltableaddress:  $16c00
  ;invisspratttableaddress:  $16e00
  ;invissprchatableaddress:  $17000

.setspritetables:
	di
	ld		(VDP_0+5),a
	out		($99),a		;spr att table to $17600 or $16e00
	ld		a,5+128
	out		($99),a
	ld		a,$02     ;%0000'0010
	ld		(VDP_8+3),a
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,b
	ld		(VDP_0+6),a
	out		($99),a		;spr chr table to $17800 or $17000
	ld		a,6+128
	ei
	out		($99),a

  ld    bc,$200
  ld    (sprcoltableaddress),hl
  add   hl,bc
  ld    (spratttableaddress),hl
  add   hl,bc
  ld    (sprchatableaddress),hl
  ex    de,hl
  ld    (invissprcoltableaddress),hl
  add   hl,bc
  ld    (invisspratttableaddress),hl
  add   hl,bc
  ld    (invissprchatableaddress),hl
;/set sprite table addresses

  ld    a,0*32+31         ;set page
  ld    (currentpage),a
  
;R#18 (register 18) uses bits 3 - 0 for horizontal screen adjust
;MSB  7   6   5   4   3   2   1   0
;R#18 v3  v2  v1  v0  h3  h2  h1  h0
;h can have a value from 0 - 15 and the screen adjusts horizontally according to the table below
;     7   6   5   4   3   2   1   0   15  14  13  12  11  10  9   8
;H  left                       centre                           right
  ld    a,8
  ld    (horizontalscreenoffset),a
  ld    a,-18
  ld    (verticalscreenoffset),a
  ld    a,0
  ld    (horizontalscrollstep),a  
 
.reloadobjectlist: 
 
  ld    a,(worldmapworld)   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3       
  cp    1
  ld    hl,WM_world1_sprites
  ld    bc,WM_world1_sprites_end-WM_world1_sprites
  jp    z,.setspritelist
  cp    2
  ld    hl,WM_world2_sprites
  ld    bc,WM_world2_sprites_end-WM_world2_sprites
  jp    z,.setspritelist 
  cp    3
  ld    hl,WM_world3_sprites
  ld    bc,WM_world3_sprites_end-WM_world3_sprites
  jp    z,.setspritelist 
  cp    7
  ld    hl,WM_world7_sprites
  ld    bc,WM_world7_sprites_end-WM_world7_sprites
  jp    z,.setspritelist 
.setspritelist:  
  ld    de,Objectlist
  ldir  

  ld    a,(worldmapworld)   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3       
  cp    1
  ld    b,worldmapmusic_YOISLANDblock
  jp    z,.setmusic
  cp    2
  ld    b,worldmapmusic_WORLDMAPblock
  jp    z,.setmusic
  cp    3
  ld    b,worldmapmusic_VANILLAblock
  jp    z,.setmusic
  cp    5
  ld    b,worldmapmusic_FORILLUblock
  jp    z,.setmusic
  cp    7
  ld    b,worldmapmusic_BOWSERVAblock
  jp    z,.setmusic
  cp    8 ;starworld
  ld    b,starworldmusicblock
  jp    z,.setmusic
  cp    9 ;msx world
  ld    b,msxsongblock
  jp    z,.setmusic  
.setmusic:
  ld    a,b
  ld    (currentmusicblock),a;set music replayer in page 2, block3+4, at $8000

  ;clear all sprites from the spat by putting their x value on 255
  ld    hl,Worldmapspat+1       ;x sprite 0 in spat
  ld    b,32
  ld    de,4
  ld    a,255
.loop:
  ld    (hl),a
  add   hl,de
  djnz  .loop
  ;/clear all sprites from the spat by putting their x value on 255
  
  xor   a
  ld    (yoffsetbuildupscreen),a     ;y offset start buildup screen  
  ld    (Worldmapvblankintflag),a
  ld    (framecounter),a        ;this is used so that the water will animate this frame
  ld    (switchworld?),a
  ld    (WMenterlevelwaittimer),a
  ret

copyworldmaptilestopage3vram: 
  ld    a,(slot.page12rom)            ;all RAM except page 12
  out   ($a8),a          

  ld    a,(worldmapworld)
  cp    3
  jr    z,.world3and7
  cp    7
  jr    z,.world3and7
  cp    5
  jp    z,.forestofillusion
  cp    8
  jp    z,.starworld
  cp    9
  jp    z,.starworld

.world1and2and4:
  ;copy world1and2and4 tiles to Vram in page 3
  ld    a,world1and2and4tilesblock
  call  block12
  
  ld    hl,$8000
	ld		a,1
	call	SetVdp_Write	
	ld		hl,world1and2and4tiles
  ld    c,$98
  ld    b,128                      ;128 lines high
    
.loop2:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop2

  ld    a,world1and2and4tilesblock+2
  call  block12
	ld		hl,world1and2and4tiles
  ld    c,$98
  ld    b,192-128                  ;192 lines high
    
.loop3:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop3
  ret

.world3and7:
  ;copy world3 tiles to Vram in page 3
  ld    a,world3and7tilesblock
  call  block12
  
  ld    hl,$8000
	ld		a,1
	call	SetVdp_Write	
	ld		hl,world3and7tiles
  ld    c,$98
  ld    b,128                      ;128 lines high
    
.loop1:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop1
  ret
  
.starworld:
  ;copy starworld tiles to Vram in page 3
  ld    a,starandspecialworldtilesblock
  call  block12
  
  ld    hl,$8000
	ld		a,1
	call	SetVdp_Write	
	ld		hl,starandspecialworldtiles
  ld    c,$98
  ld    b,128                      ;128 lines high
    
.loop4:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop4
  ret

.forestofillusion:
  ;copy starworld tiles to Vram in page 3
  ld    a,world5tilesblock
  call  block12
  
  ld    hl,$8000
	ld		a,1
	call	SetVdp_Write	
	ld		hl,world5tiles
  ld    c,$98
  ld    b,128                      ;128 lines high
    
.loop5:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop5
  ret

copymaptoram:
  ;copy map from page 2 ($8000) in rom to page 1 ($4000) in ram
  ld    a,(slot.page2rom)   ;all RAM except page 2
  out   ($a8),a 

  ld    a,(worldmapworld)
  cp    1
  ld    b,worldmapblockWorld1
  jr    z,.worldmapfound
  cp    2
  ld    b,worldmapblockWorld2
  jr    z,.worldmapfound
  cp    3
  ld    b,worldmapblockWorld3
  jr    z,.worldmapfound
  cp    5
  ld    b,worldmapblockWorld5
  jr    z,.worldmapfound
  cp    7
  ld    b,worldmapblockWorld7
  jr    z,.worldmapfound
  cp    8
  ld    b,starworldmapblock
  jr    z,.worldmapfound
  cp    9
  ld    b,specialworldmapblock
  jr    z,.worldmapfound  
.worldmapfound:

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  call  z,.world2

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                 ;only world 2 has backgroundscoll
  call  nz,.notworld2    
;/make a copy of the map to page 1 in RAM
  ret

.notworld2:
;make a copy of the map to page 1 in RAM
  ld    a,b                 ;worldmapblockWorldx

  ld    hl,$8000
  ld    de,128*1 + $4000
  ld    bc,$600
  call  copymaptopage1inram

  ld    hl,128*1 + $4000
  ld    de,$4000
  ld    bc,128*1
  ldir

  ld    hl,128*1 + $4000
  ld    de,128*1 + $4000 + $600
  ld    bc,$600
  ldir
  ret  
  
.world2:  ;world 2 is slightly different since its 64 tiles wide
;make a copy of the map to page 1 in RAM
  ld    a,b                 ;worldmapblockWorldx
  ld    hl,$8000
  ld    de,128*2 + $4000
  ld    bc,$2000
  call  copymaptopage1inram

  ld    hl,128*2 + $4000
  ld    de,$4000
  ld    bc,128*2
  ldir

  ld    hl,$4000
  ld    de,128*2 + $6000
  ld    bc,$1000
  ldir
  ret

worldmapsetpalette:
  ld    a,(slot.page12rom)   ;all RAM except page 12
  out   ($a8),a 

  ld    a,(worldmapworld) ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    3
  jr    z,.world3and7
  cp    5
  jr    z,.world5
  cp    7
  jr    z,.world3and7
  cp    8
  jr    z,.starandspecialworld
  cp    9
  jr    z,.starandspecialworld

.world1and2and4:
  ld    a,worldmapblockWorld2
  call  block12
  ld    hl,world1and2and4palette
  call  SetPalette
  ret

.world3and7:
  ld    a,worldmapblockWorld3
  call  block12
  ld    hl,world3and7palette
  call  SetPalette
  ret

.world5:
  ld    a,worldmapblockWorld5
  call  block12
  ld    hl,world5palette
  call  SetPalette
  ret

.starandspecialworld:
  ld    a,starworldmapblock
  call  block12
  ld    hl,starandspecialworldpalette
  call  SetPalette
  ret

copyinfobartopscreen:
  ld    a,(slot.page12rom)   ;all RAM except page 12
  out   ($a8),a 

;copy top (mario x5 Yoshi's house) of the screen to page 2
  ld    a,worldmapinfobarblock
  call  block12
  
  ld    hl,$0000
	ld		a,1
	call	SetVdp_Write	
	ld		hl,worldmapinfobar
  ld    c,$98
  ld    b,71
.loop2:
  push  bc
	call	outix128
  pop   bc
  djnz  .loop2
  ret
  
worldmapsetcamera:
  ld    hl,(WMmarioy)
  ld    de,maxCameray    ;mariocentrey       ;if marioy<mariocentrey then cameray=0
  or    a                     ;reset carry
  sbc   hl,de
  jr    c,.carry2
  ld    hl,40*8                  ;cameray=0
  jr    .notcarry
.carry2:





;set y cameraposition (per 8 tiles offset) and set y vertical screen offset (per tile offset)
  ld    hl,(WMmarioy)

  ;the camera starts mariocentrey pixels above mario's position, unless marioy<mariocentrey
  ld    de,mariocentrey    ;mariocentrey       ;if marioy<mariocentrey then cameray=0
  or    a                     ;reset carry
  sbc   hl,de
  
  jr    nc,.notcarry
  ld    hl,0                  ;cameray=0
.notcarry:

  ;set cameray in pixels
	ld    (camerayinpixels),hl
  ;/set cameray in pixels

  ;get cameray in tiles instead of in pixels
	srl   h
	rr    l                     ;/2
	srl   h
	rr    l                     ;/4
	srl   h
	rr    l                     ;/8
	
	ld    a,l
	ld    (camerayintiles),a
  ;/get cameray in tiles instead of in pixels
  ;cameray * 128
  add   hl,hl                 ;*2
  add   hl,hl                 ;*4
  add   hl,hl                 ;*8
  add   hl,hl                 ;*16
  add   hl,hl                 ;*32
  add   hl,hl                 ;*64
  add   hl,hl                 ;*128
  ;/cameray * 128

  push  hl

;set x cameraposition (per 8 tiles offset)
  ld    hl,(WMmariox)  
  ld    de,mariocentrexRight  ;if mariox>50*8 then camerax=utmost right position
  or    a                     ;reset carry
  sbc   hl,de
  jr    c,.carry
  ld    hl,36*8               ;camerax=0
  jp    .notcarry2
.carry:





  ld    hl,(WMmariox)  
  ld    de,mariocentrex       ;if mariox<128 then camerax=0
  or    a                     ;reset carry
  sbc   hl,de
  jr    nc,.notcarry2
  ld    hl,0                  ;camerax=0
.notcarry2:
  ;get camerax in tiles instead of in pixels
	srl   h
	rr    l                     ;/2
	srl   h
	rr    l                     ;/4
	srl   h
	rr    l                     ;/8
	
	ld    a,l
	ld    (cameraxintiles),a	
  ;/get camerax in tiles instead of in pixels
  add   hl,hl                 ;*2
;/set x cameraposition (per 8 tiles offset) and set y vertical screen offset (per tile offset)

  pop   de
  add   hl,de  
  
  ld    a,(worldmapworld)     ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3
  cp    2                     ;only in world 2 we have an active camera
  jr    z,.setcamera          ;in all the other worlds camera position = 0
  ld    hl,0
  xor   a
  ld    (cameraxintiles),a
  ld    (camerayintiles),a  
  ld    (camerayinpixels),a
  
.setcamera:
  ld    (worldmapcameraposition),hl ;camera position 128* y + 2*x, camera points to a tile
  ret  

exittosavemenu:
  
  ld a,(flashrom?)
  and a
  ret z ;no sram!
  
  
  ld a,savemenublock
  call block12
    
  call savemenu ;call the menu from page 3
  
  call worldmapinitiation.reloadobjectlist
  
  ret




;worldmaplineintheight: equ 32
enableworldmapinterrupt:
  di
  ld    hl,WorldmapInterruptHandler 
  ld    ($38+1),hl          ;set new normal interrupt
  ld    a,$c3               ;jump command
  ld    ($38),a
 
  ld    a,(VDP_0)           ;set ei1
  or    16                  ;ei1 checks for lineint and vblankint
  ld    (VDP_0),a           ;ei0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,128
  out   ($99),a

  ld    a,worldmaplineintheight
  out   ($99),a
  ld    a,19+128            ;set lineinterrupt height
  ei
  out   ($99),a
  ret

WorldmapInterruptHandler:
  push  af
  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix
  
  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,worldmaplineint ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,worldmapvblank  ;vblank detected, so jp to that routine
 
  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc
  pop   af 
  ei
  ret

;on vblank we set page 2
;we set horizontal and vertical screen adjust to 0
;we set status register 0
worldmapvblank:   
  ld    a,2*32+31         ;set page 2
  out   ($99),a
  ld    a,2+128
  out   ($99),a

;R#18 (register 18) uses bits 3 - 0 for horizontal screen adjust
;MSB  7   6   5   4   3   2   1   0
;R#18 v3  v2  v1  v0  h3  h2  h1  h0
;h can have a value from 0 - 15 and the screen adjusts horizontally according to the table below
;     7   6   5   4   3   2   1   0   15  14  13  12  11  10  9   8
;H  left                       centre                           right
  ld    a,0               ;set horizontal screen adjust
  out   ($99),a
  ld    a,18+128
  out   ($99),a

  ld    a,0               ;set vertical screen adjust
  out   ($99),a
  ld    a,23+128
  out   ($99),a

;If you have a split and are changing the vertical scrollregister (r#23) on it,
;then you should always re-set the splitline (r#19). This because the splitline
;is calculated from line 0 in the VRAM, and not from line 0 of the screen. 
;In order to set the splitline to the �screenline� it�s easiest to simply add 
;the value of r#23 to it.
  ld    a,worldmaplineintheight
  out   ($99),a
  ld    a,19+128            ;set lineinterrupt height
  out   ($99),a

  ld    a,(VDP_8)         ;sprites off
  or    %00000010
  ld    (VDP_8),a
  out   ($99),a
  ld    a,8+128
  out   ($99),a

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a

;  ld    a,1               ;lineint flag gets set
;  ld    (Worldmapvblankintflag),a  

;this flag sais we can now swap horizontal& vertical offset 
;we can swap page and we can swap sprite tables
  ld    a,(swaptablesonvblank?)
  or    a
  jr    z,.end
  call  Worldmapswap_spat_col_and_char_table

  ld    a,(currentpage)
  ld    (newlineintpage),a

  ld    a,(horizontalscreenoffset)              ;prepare horizontal screen adjust for next frame
  ld    (newlineinthoroff),a

  ld    a,(verticalscreenoffset)               ;set vertical screen adjust
  ld    (newlineintveroff),a
  
  ld    a,1               ;lineint flag gets set
  ld    (Worldmapvblankintflag),a  

  xor   a
  ld    (swaptablesonvblank?),a


.end:

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc
  pop   af 
  ei
  ret

;on the lineint we turn the screen off at the end of the line using polling for HR
;then we switch between page 0+1
;we set horizontal and vertical adjust
;and we turn screen on again at the end of the line
;we play music and set s#0 again
worldmaplineint:  
  ld    a,2               ;Set Status register #2
  out   ($99),a
  ld    a,15+128          ;we are about to check for HR
  out   ($99),a

  ld    hl,linesplitvariables

;PREPARE ALL THESE INSTRUCTION TO BE EXECUTED WITH OUTI'S AFTER THE POLLING
  ld    a,(newlineintpage)
  ld    (hl),a
  ld    a,2+128
  inc   hl  
  ld    (hl),a
  inc   hl  

  ld    a,(newlineinthoroff)              ;prepare horizontal screen adjust for next frame
  ld    (hl),a
  ld    a,18+128
  inc   hl  
  ld    (hl),a
  inc   hl  

  ld    a,(newlineintveroff)               ;set vertical screen adjust
  ld    d,a
  ld    (hl),a
  ld    a,23+128
  inc   hl  
  ld    (hl),a
  inc   hl  

  ld    a,d
  add   a,worldmaplineintheight
  ld    (hl),a
  ld    a,19+128            ;set lineinterrupt height  
  inc   hl  
  ld    (hl),a
  inc   hl  

  ld    a,(VDP_0+1)       ;screen on
  or    %0100'0000
  ld    (hl),a
  ld    a,1+128
  inc   hl  
  ld    (hl),a
  inc   hl
;/PREPARE ALL THESE INSTRUCTION TO BE EXECUTED WITH OUTI'S AFTER THE POLLING

  ld    hl,linesplitvariables
  ld    c,$99

  ld    b,%0010'0000      ;bit to check for HBlank detection

.Waitline1:
  in    a,($99)           ;Read Status register #2
  and   b                 ;wait until start of HBLANK
  jr    nz,.Waitline1

.Waitline2:
  in    a,($99)           ;Read Status register #2
  and   b                 ;wait until end of HBLANK
  jr    z,.Waitline2 

  ;screen always gets turned on/off at the END of the line
  ld    a,(VDP_0+1)       ;screen off
  and   %1011'1111
  out   ($99),a
  ld    a,1+128
  out   ($99),a
  ;so after turning off the screen wait till the end of HBLANK, then perform actions

  ld    a,(VDP_8)         ;sprites on
  and   %11111101
  ld    (VDP_8),a
  out   ($99),a
  ld    a,8+128
  out   ($99),a
  
.Waitline3:
  in    a,($99)           ;Read Status register #2
  and   b                 ;wait until end of HBLANK
  jr    z,.Waitline3

  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a

;  ld    a,3
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

  call  handlemusicint     ;handle this from page3 or else!!

;  ld    a,4
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings
  ld    a,(slot.page12rom)        ;all RAM except page 1+2
  out   ($a8),a         

  call  handlesfxint

;  ld    a,7
;  out   ($99),a
;  ld    a,7+128 ;backdrop
;  out   ($99),a

  ld a,(lineintflag)
  inc a
  ld (lineintflag),a

  pop   af                  ;recall RAM/ROM page setting
  out   ($a8),a         



  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc
  pop   af 
  ei
  ret  
  
worldmapend:
