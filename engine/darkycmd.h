;darky system commands. Call from engine32

;detect presence of darky. Send 170 to $40 receive cpl
;return 1 or 0 in a
detectdarky:

    ld a,170
    di
    out ($40),a ;send 
    ei
    in a,($40) ;return
    cp $55 ;compare to inverse value
    ld a,0
    ret nz
    ld a,1
    
    ret

;send a command to the darky
;in h, register port l, data
;out none
;remember that darky needs to be detected first for this command to work
senddarkycommand:

    ld a,(turbor?)
    dec a
    jp nz,.nowaitstates ;do not bog the z80 further down

    di
    ld a,h
    or $80 ;set bit 7
    out ($42),a
    ex (sp),ix
    ex (sp),ix ;turbo wait
    ld a,l ;get data
    and $7F ;reset bit 7 (not needed but you may never know)
    out ($42),a
    ex (sp),ix
    ex (sp),ix
    ex (sp),ix
    ex (sp),ix ;some more wait
    ei
    ret
    
.nowaitstates:

    di
    ld a,h
    or $80 ;set bit 7
    out ($42),a
    ld a,l ;get data
    and $7F ;reset bit 7 (not needed but you may never know)
    out ($42),a
    ei
    ret    

    
