variables: org $c000+enginepage3length

variablespage3:

;mario related stuff

deathwaitsecure:  rb 1 ;in case mario gets stuck on a deathscene. Prevent a loop hang by counting to 255 on this counter


slot:						
.ram:		                    rb 1
.page1rom:	                rb 1
.page2rom:	                rb 1
.page12rom:	                rb 1
.bios:      	              rb 1
memblocks:
.n1:			                    rb 1
.n2:			                    rb 1
.n3:			                    rb 1
.n4:			                    rb 1	
VDP_0b:		                  equ   $F3DF ;All of the systemvars above this adress can safely be overwritten
;CASPRV:                     equ   $FCB1 ;Dummy label. This ONE may NEVER be overwritten on TURBOR!!
VDP_8:		                  equ   $FFE7
;VDP_0:                      rb    25
;VDP_8:                      rb    25
chput:                      equ   $a2
engaddr:	                  equ	  $03e
loader.address:             equ   $8000
enginepage3addr:            equ   $c000
bgrmapaddr:	                equ   $4000   ;unpack routine requires fgrmap to be in page 2
pattaddre:	                equ	  $4000
coloraddre:	                equ	  $4000+2048
paletaddre:	                equ	  $4000+2048+2048
cpusp:                      equ   $FFFD ;save the cpu stackpointer


lineinterruptheight:        equ   0;31
demolineinterruptheight:    equ   31


sx:                         equ   0
sy:                         equ   2
spage:                      equ   3
dx:                         equ   4
dy:                         equ   6
dpage:                      equ   7 
nx:                         equ   8
ny:                         equ   10
copytype:                   equ   14

;make a copy of the vdp mirrors into RAM on a place where WE like to use it.
VDP_0:		                  equ   $FFDE ;All of the systemvars above this adress can safely be overwritten


biostype:                   rb 1

maplenght:                  rb    2
maplenghtmin32:             rb    2
;mapheightmin23:             rw		1

framecounter:               rb    1
currentpalette:             rb    16*2
currentpaletteONEtintdarker:rb    16*2
editablepalette:            rb    16*2
v9958found?:                rb    1


;player info
yoshicoin:		              rb    1
currentlevel:               rb    1


;SPAT and VDP related
invissprcoltableaddress:    rw    1
invisspratttableaddress:    rw    1
invissprchatableaddress:    rw    1
sprcoltableaddress:         rw    1
spratttableaddress:         rw    1
sprchatableaddress:         rw    1
nametableaddress:           rw    1
invispagenametable:         rw    1
Controls:	                  rb		1
controls2:                  rb    1
NewPrControls:	            rb		1
upperspatattadress:		rb 1
invisupperspatattadress:	rw 1

presearchytable: equ $F664; rw 80 ;rw 160; rw 256 rw 512
;engine2adress:   equ $F664+512
endenginepage3variablesspecial: equ $F664+1024 ;work area for basic. we can use that


temphl:     rw  1 ;hl storage register
tempde:     rw  1 ;de storage register

;turbo-R CPU control

turbor?:  rb 1
flashrom?:  rb 1
;cpusp:    rw 1 ;save the cpu stackpointer
tempstack:    rw 1 ;save the cpu stackpointer

;darky support
darky?:             rb 1 ;detect darky


;write to register 6
;% 7 6 5 4 3 2 1 0
;  ? 1 1 ? ? ? ? ?
; 5, 1 = Z80
;    0 = R800    


;variables for joymega
;status1:        rb 1
;status2:        rb 1
;status3:        rb 1
;status4:        rb 1
joymega?:       rb 1 ;joymega is set?
;/variables for joymega



worldmapStartAddress:       equ $4000
mariocentrex:               equ 112 ; 102-24
mariocentrexRight:          equ 50*8 
mariocentrey:               equ 104 ; 102-24 
worldmaplineintheight:      equ 30

;worldmap vars
marioanimationinfobarstep:  rb 1
switchworld?: 	            rb 1
worldmapworld:	            rb 1 ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3 
endworldmap:		            rb 1 ;sets the worldmap to end
automoveatstartworldmap?:   rb 1
verticalscreenoffset:       rb 1
yoffsetbuildupscreen:       rb 1
linesplitvariables:         rb 10
horizontalscrollstep:       rb 1
horizontalscreenoffset:     rb 1
worldmapcameraposition:     rb 2
camerayinpixels:            rb 2
cameraxinpixels:            rb 2
camerayintiles:             rb 1
cameraxintiles:             rb 1
currentpage:                rb 1
tileanimation?:             rb 1
tileanimationstep:          rb 1
tileanimationpointer:       rw 1
;Worldmaplineintflag:        rb 1
Worldmapvblankintflag:      rb 1
leveldotsanimationstep:     rb 1
WMmariospriteanimation:     rb 1
WMenterlevelwaittimer:      rb 1
WMmariomove?:               rb 1
WMmariomovetable:           rw 1
WMmariomoveamountofframes:  rb 1
WM_movementtable:           rw 1
WM_movementtablepointer:    rb 1
CurrentLocation:            rw 1
WMmarioy:                   rb 2
WMmariox:                   rb 2
WM_setlocation?:            rb 1
swaptablesonvblank?:        rb 1

newlineintpage:             rb 1
newlineinthoroff:           rb 1
newlineintveroff:           rb 1











vblankflag:                 rb 1
lineintflag:		            rb 1

pillarheight:               rb 0 ;used in iggys castle

                            rb 16 ; belongs to pillarbkgbuffer
pillarbkgbuffer:            rb 16*20

;extra variables concerning platform control
noplatformrotation:         rb 1



;random stuff
staticpointer:              rb 1
randomtablepointer:         rb 1

;optionsmenu
optionsmenuheight:		      rb 1
optionsmenuposition:		    rb 1
optionsecondary:		        rb 5


;options like sfx and music
sfxactive:                  rb 1
musicactive:                rb 1; equ $3FFF; rb 1
musicactiveoption:          rb 1; equ $3FFF; rb 1
blockcpusetting:	          rb 1

;nosheer byte
nosheer:                    rb 1
noblock:                    rb 1  ;0 = no yellow,green,red or blue blocks on map
                                  ;1 = only yellow blocks
                                  ;2 = only green blocks
                                  ;3 = only redblocks
                                  ;4 = only blue blocks
                                  ;5 = yellow and green blocks
                                  ;6 = yellow and red blocks
                                  ;7 = yellow and blue block
                                  ;8 = green and red blocks
                                  ;9 = green and blue block
                                  ;10 = all blocks


;scorebar
scorebaractive:             rb 1
droppowerup:                rb 1 ;drop the powerreserve powerup
lineintheight:              rb 1
removebar:                  rb 1 ;remove the scorebar
showpowerup:                rb 1
showx:                      rb 1
showcoin:                   rb 1
showtime:                   rb 1
showyoshi:                  rb 1
numbertable:                rb 3


;the in game timer
time:			                  rb 1
timecounter:		            rb 1

;mapdata
noyoshi:                    rw 1 ;yoshi buffer

;demostuff
demopointer:                rw 1 ;demo pointer
enddemotimer:               rb 1

;intromenu related
intromenuposition:          rb 1 ;position of the intromenu arrow
introdemoactive:            rb 1 ;see if we are on the intro screen
levelprogress:              rb 3 ;level progress read in each of the slots


;load save pointer
srampointer:                rw 1;which part of the sram is written?

;giant yellow pillar must be globalized so that all animations are exact
pillaranimationstep:        rb 1 ;animation step of the yellow pillar
pillarmovementstep:         rb 1 ;animation step of the yellow pillar
pillarspeed:                rb 1 ;speed of the pillar taken from the slowdown table
pillary:                    rw 1 ;y coordinate of the pillar

;object table. This one is in the active RAM and is being copied in on mapload by the levelloader
;we have to link this to the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    0      |     db 0
spritehidertable:           rb 60

;spritehider related
spritehideractive:          rb 1

;yoshispecial
yoshispecial:               rb 1 ;special byte that tells yoshi to update his sprite anyway and ignore all no control bytes

;vram control
sc5page:                    rb 1 ;which page is set?

;temporary sprite data
tempspritedata:             rb 64 ;Was 32 but simply need more

;force a save on the worldmap
forcesavemenu:              rb 1 ;if set to 1 the worldmap will give us the savemenu
;completedfirsttime:         rb 1 ;check if this map is completed for the first time

;marioshelltype
marioshelltype:             rb 1 ; global storage of objectrelease byte when mario exits through a tube

;---------------------------------------------------------
;SCC sample player Variables
;---------------------------------------------------------
SccSfxOn:           rb	1

SamplePos:          rw	1
NumBlocksToPlay:    rb	2
SamplePage:         rb	1


variablespage3end:
                

variablespage3lenght: equ $ - variablespage3
fullpage3lenght:  equ $ - enginepage3
endenginepage3variables:  equ $+enginepage3length
org variables
