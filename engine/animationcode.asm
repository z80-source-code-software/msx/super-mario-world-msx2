
animate0:
  ret	


levelanimationadresses:
level00animationadress:
;write the animation tiles to each corresponding character and color table
;  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
;  out   ($a8),a        
;
;  ld    a,animationtilesblock
;  call  block1                                        ;set block in page 1 ($4000 - $5fff)
;  ld    c,$98

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescharacteraddr+(8*10)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescharacteraddr+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescharacteraddr+(8*30)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescoloraddr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescoloraddr+(8*10)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescoloraddr+(8*20)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+186*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilescoloraddr+(8*30)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  
  
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  
  ret

level130animationadress:

  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock2
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608a+(8*04)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608a+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608a+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608aclr+(8*04)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608aclr+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+175*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0608aclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  ld    a,animationtilesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98
  
;  ld    a,(slot.page2rom)                             ;all RAM except page 2
;  out   ($a8),a    
  
  jp level01animationadress
  
  ;ret     
  
  
level119animationadress:

  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock2
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604a+(8*04)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604a+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604a+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604aclr+(8*04)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604aclr+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0604aclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  ld    a,animationtilesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98
  
;  ld    a,(slot.page2rom)                             ;all RAM except page 2
;  out   ($a8),a    
  
  jp level01animationadress
  
  ;ret    
  
  
level88animationadress:

  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock2
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507a+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507a+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507a+(8*18)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507aclr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507aclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0507aclr+(8*18)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  
  
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  
  ret  


level87animationadress:

  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock2
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406d+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406d+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406d+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406d+(8*18)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406dclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406dclr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406dclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+180*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0406dclr+(8*18)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  
  
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  
  ret  


level69animationadress:

  call level34animationadress
  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98
  
  jp levelanimationadresses

level62animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0307a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0307a+(8*04)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0307a+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0307a+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0307aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0307aclr+(8*04)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0307aclr+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+124*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0307aclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  
  
  jp levelanimationadresses ;jump to the default animations



level46animationadress:


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303b+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303b+(8*11)         ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303b+(8*22)         ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303b+(8*33)         ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303bclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303bclr+(8*11)             ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303bclr+(8*22)             ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303bclr+(8*33)             ;rotating tile animation 1 character address in rom
  ld    b,8*11
  otir
 
    jp level01animationadress
  
;  ld    a,(slot.page2rom)                             ;all RAM except page 2
;  out   ($a8),a    
;  
;  ret


level45animationadress:


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303a+(8*10)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303a+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303a+(8*30)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303aclr+(8*10)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303aclr+(8*20)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+96*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0303aclr+(8*30)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
 
    jp level01animationadress
  
;  ld    a,(slot.page2rom)                             ;all RAM except page 2
;  out   ($a8),a    
;  
;  ret


level44animationadress:


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301c+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301c+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301c+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301c+(8*18)         ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301cclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301cclr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301cclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+177*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0301cclr+(8*18)             ;rotating tile animation 1 character address in rom
  ld    b,8*6
  otir
 
     jp level00animationadress
  
;  ld    a,(slot.page2rom)                             ;all RAM except page 2
;  out   ($a8),a    
;  
;  ret



level37animationadress:


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211d+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211d+(8*04)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211d+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211d+(8*12)         ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211dclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211dclr+(8*04)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211dclr+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+182*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211dclr+(8*12)             ;rotating tile animation 1 character address in rom
  ld    b,8*4
  otir
  
  
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  
  ret



level34animationadress:

;
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211a+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211a+(8*15)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211a+(8*30)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211a+(8*45)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211aclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211aclr+(8*15)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211aclr+(8*30)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+171*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0211aclr+(8*45)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
 
    ld a,(currentlevel)
    cp 166
    jp z,.skiplevel00 ;cheap level specific hack
 
     jp level00animationadress 
 
.skiplevel00: 
 
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  

  
  ret



level0207animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0207+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0207+(8*03)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0207+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtiles0207+(8*09)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0207clr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0207clr+(8*03)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0207clr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0207clr+(8*09)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  
  
  jp levelanimationadresses ;jump to the default animations


level01animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtilesCavecharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtilesCavecharacteraddr+(8*03)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtilesCavecharacteraddr+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 183
  ld    hl,animationtilesCavecharacteraddr+(8*09)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilesCavecoloraddr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilesCavecoloraddr+(8*03)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilesCavecoloraddr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtilesCavecoloraddr+(8*09)             ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  
  
  jp levelanimationadresses ;jump to the default animations
 
 
level04animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,blackholeanimationgfx1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,blackholeanimationgfx2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,blackholeanimationgfx3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,blackholeanimationgfx4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,blackholeanimationgfxclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,blackholeanimationgfxclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,blackholeanimationgfxclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+148*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,blackholeanimationgfxclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*12
  otir
  
  
  jp levelanimationadresses ;jump to the default animations


level05animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,LavaanimationtilesCave+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,LavaanimationtilesCave+(8*02)         ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,LavaanimationtilesCave+(8*04)         ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,LavaanimationtilesCave+(8*06)         ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,LavaanimationtilesCaveclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,LavaanimationtilesCaveclr+(8*02)             ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,LavaanimationtilesCaveclr+(8*04)             ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+181*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,LavaanimationtilesCaveclr+(8*06)             ;rotating tile animation 1 character address in rom
  ld    b,8*2
  otir
  
  
  jp level01animationadress ;jump to the default animations +cave animations


level06animationadress:

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 128
  ld    hl,wateranimation1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 128
  ld    hl,wateranimation2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 128
  ld    hl,wateranimation3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 128
  ld    hl,wateranimation4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,wateranimationclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,wateranimationclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,wateranimationclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,wateranimationclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*23
  otir
  
  
  jp level00animationadress
  



level09animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  
  
  jp levelanimationadresses ;jump to the default animations


level10banimationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  
  
  jp levelanimationadresses ;jump to the default animations

level10animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,IggyCastleanimation4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+168*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,IggyCastleanimationclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*05
  otir
  
  
  jp levelanimationadresses ;jump to the default animations

;hiero
level135animationadress:
;ghostship
;write the cave animation tiles to each corresponding character and color table


  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock2
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,animationtiles0701aa+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,animationtiles0701aa+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,animationtiles0701aa+(8*16)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,animationtiles0701aa+(8*24)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0701aclra+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0701aclra+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0701aclra+(8*16)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0701aclra+(8*24)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  
  
  ld    a,animationtilesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98
  
  jp levelanimationadresses ;jump to the default animations    


level26animationadress:

;write the cave animation tiles to each corresponding character and color table

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level26animation+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level26animation+(8*08)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level26animation+(8*16)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level26animation+(8*24)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level26animationclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level26animationclr+(8*08)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level26animationclr+(8*16)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+178*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level26animationclr+(8*24)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  
  
  jp levelanimationadresses ;jump to the default animations  
  
  
  
level41animationadress:

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level41animation+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level41animation+(8*10)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level41animation+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,level41animation+(8*30)         ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level41animationclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*08
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level41animationclr+(8*10)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level41animationclr+(8*20)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+176*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,level41animationclr+(8*30)             ;rotating tile animation 1 character address in rom
  ld    b,8*10
  otir

  jp levelanimationadresses ;jump to the default animations



level55animationadress:

  ld a,animationtilesblock2 ;its on another block hurraaaaay
  call block12


  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*15)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*30)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*45)         ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*63)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*66)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304b+(8*69)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*15)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*30)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+114*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*45)             ;rotating tile animation 1 character address in rom
  ld    b,8*15
  otir

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*63)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*66)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+183*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,animationtiles0304bclr+(8*69)         ;rotating tile animation 1 character address in rom
  ld    b,8*3
  otir

 
 ;   jp level01animationadress
  
  ld    a,(slot.page2rom)                             ;all RAM except page 2
  out   ($a8),a    
  
  ret
 
