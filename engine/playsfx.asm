;routine die alle geluiden afhandeld. Zodra de engine een request doet naar een subroutine van deze routine dan word er een geluidje afgespeeld


    




 ; ret



Playsfx:


.blargh:

  ld a,sccblargh
  jp ReplayerInit  

.magicbeam:

  ld a,sccmagikoopabeam
  jp ReplayerInit 
  
  

.error:
  
  ld a,sccwrongtictac
  jp ReplayerInit   
  

.bossdies:

  ld a,sccbossdies
  jp ReplayerInit   
  

.yoshimount:
  
  ld a,sccyoshimount
  jp ReplayerInit 
    


.fire:
  
  ld a,sccFireball
  jp ReplayerInit  
    

.swim:
  
  ld a,sccmarioswimup
  jp ReplayerInit  
    
  
;       ld    de,00*256 + sfxswim
;       jp .Setsfx  

.spring:
  
     ; ld a,(sfxplaying)
     ; dec a
     ; ret z
  
  ld a,sccspring
  jp ReplayerInit  
  
 ;      ld    de,01*256 + sfxspringjump
 ;      jp .Setsfx  


.bonestomp:
  
     ; ld a,(sfxplaying)
     ; dec a
     ; ret z
  
       ld    de,01*256 + sfxbonestomp
       jp .Setsfx    
  

.yoshispit:
  
     ; ld a,(sfxplaying)
     ; dec a
     ; ret z
  
    ld a,sccyoshispit
    jp ReplayerInit  
  
   ;    ld    de,01*256 + sfxyoshispit
   ;    jp .Setsfx  


.yoshitongue:
  
     ; ld a,(marioshot)
     ; cp 2
     ; ret nz
  
       ld    de,01*256 + sfxyoshitongue
       jp .Setsfx  


.yoshifireball:
  
      ;ld a,(sfxplaying)
      ;dec a
      ;ret z
  
  ld a,sccfireballshoot
  jp ReplayerInit  
  
   ;    ld    de,01*256 + sfxyoshifireball
   ;    jp .Setsfx  
  

.yoshihit:
    
      ;ld a,(sfxplaying)
      ;dec a
      ;ret z
  
       ld    de,01*256 + sfxyoshihit
        jp .Setsfx


.yoshicoin:
    
      ;ld a,(sfxplaying)
      ;dec a
      ;ret z
  
       ld    de,01*256 + sfxyoshicoin
        jp .Setsfx


.door:
  
    
      ld a,(sfxplaying)
      dec a
      ret z
      
      
      ld a,1
      ld (sfxplaying),a  
  
    ld a,sccdoor
    jp ReplayerInit
  
    ;   ld    de,02*256 + sfxdoor
    ;    jp .Setsfx
    
.secretdoor:
  
      ld a,(sfxplaying)
      dec a
      ret z
      
      
      ld a,1
      ld (sfxplaying),a  
  
    ld a,sccsecretexit
    jp ReplayerInit
  
    ;   ld    de,02*256 + sfxdoor
    ;    jp .Setsfx
  


.Setsfx:

      ld a,1
      ld (sfxplaying),a

      jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

  
.bat:
  
   ;   ld a,(sfxplaying)
   ;   dec a
   ;   ret z
  
  ld a,sccbat
  jp ReplayerInit  
  
 ;      ld    de,02*256 + 44
 ;       jp .Setsfx
 
  
.jump:
  
      ld a,(sfxplaying)
      dec a
      ret z
  
       ld    de,01*256 + sfxjump
       jp .Setsfx
;       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
;    
;      ld a,1
;      ld (sfxplaying),a
;    
;      ret

.stomp:
  
  ;    ld a,(sfxplaying)
  ;    dec a
  ;    ret z
  
    ld a,(marionotdown)
    and a
    jp nz,.setcarry
 
.bump:
  
       ld    de,02*256 + sfxbumphead
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
    
  ;    ld a,1
  ;    ld (sfxplaying),a
        scf
      ret

.spinjump:
  
     ; ld a,(sfxplaying)
     ; dec a
     ; ret z
  
       ld    de,01*256 + sfxspinjump
       jp .Setsfx
;       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
;    
;      ld a,1
;      ld (sfxplaying),a
;    
;      ret

.kick:
  


      ld a,(mariokickshellsfx)
      and a
      ret nz  
  
        
       ld    de,02*256 + sfxkick
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
    
    
      ;ret





.fireball:
    
  ld a,sccfireballshoot
  jp ReplayerInit   
    
.bullet: 
    
       ld    de,00*256 + sfxfireball
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
    
      ;ret
   
.oneup:
    
       ld    de,02*256 + sfxoneup
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
      ;ret
   
.breakblock:
   
  ld a,sccbreakblock
  call ReplayerInit 
   
   
       ld    de,02*256 + sfxbreakblock
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
      ;ret

.getfeather:
 
    push af
    push bc
   
    ld a,sccpickupfeather 
    call ReplayerInit
   
 ;      ld    de,03*256 + sfxgetfeather
 ;      call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
    pop bc 
    pop af 
     
      ret
      
.mapmovetospot:
  ld    de,03*256 + sfxmapmovetospot
  jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

.mapcreatepath: 
  ld    de,03*256 + sfxmapcreatepath
  jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
   
.checkpoint:
 

       ld    de,03*256 + sfxcheckpoint
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     

     ; ret
 
.pauze:
    
    
       ld    de,20*256 + sfxpause
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     

      ;ret
 
  
.pipe:
    
    
        push af
    
       ld    de,04*256 + sfxpipe
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
        pop af

      ret
  

.powerup:
    
      push af
    
       ld    de,04*256 + sfxpowerupappears
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
     pop af

      ret
 
.grow:
    
      push af
      
    
       ld    de,05*256 + sfxpowerup
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
      pop af

      ret
 
   
.kickenemy:
   
      push af
   
   
   ;reset the stompcounter
       ld a,30
       ld (stompcounter),a
   
       ld a,(stompcount)
       cp 1
       jp z,.stomp2
       cp 2
       jp z,.stomp3
       cp 3
       jp z,.stomp4
;       cp 4
;       jp z,.stomp5
       cp 3
       jp nc,.oneups

   
   
       ld    de,04*256 + sfxstomp
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
              ld a,(stompcount)
              inc a
              ld (stompcount),a
        pop af
        ret
      
      
.stomp2:     
             ld    de,02*256 + sfxstomp2
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
              ld a,(stompcount)
              inc a
              ld (stompcount),a
        pop af
        ret
              
              
.stomp3:     
       ld    de,02*256 + sfxstomp3
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
              ld a,(stompcount)
              inc a
              ld (stompcount),a
        pop af
        ret


.stomp4:                
       ld    de,02*256 + sfxstomp4
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
              ld a,(stompcount)
              inc a
              ld (stompcount),a
        pop af
        ret 
 
              
              
.stomp5:                              
       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
              ld a,(stompcount)
              inc a
              ld (stompcount),a     

      pop af

      ret

      
.oneups:

  ld a,(lives)
  inc a
  ld (lives),a

    
       ld    de,02*256 + sfxoneup
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
     
     
           pop af
     
      ret      
      

.powerdown:
  
       ld    de,06*256 + sfxpowerdown
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

    ;ret
    
    
.spinjumpstomp:
  
       ld    de,03*256 + sfxspinjumpstomp
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

    ;ret


.explosion:
    
    ld a,sccexplosion
    jp ReplayerInit
  
.mapexplosion:
  
  
    ld a,sccmapexplosion
    call ReplayerInit  

  push bc
  ;wait some time
    ld b,60
.wait:
    halt : halt : 
    djnz .wait
  pop bc
  

    ret   
    
    
.marioflyrise:
  
      push af
  
    ld a,sccmarioflyup
    call ReplayerInit
  
    ;   ld    de,03*256 + sfxflyingmariorise
    ;   call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

      pop af

    ret     
    
    
.springjump:
  
  ;    push af
  
  ld a,sccspring
  jp ReplayerInit  
  
   ;    ld    de,03*256 + sfxspringjump
   ;    jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

   ;   pop af

    ;ret   
    
    
.chuckclap:
  
  ;    push af
  
       ld    de,03*256 + sfxchuckclap
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

   ;   pop af

   ; ret   
    
;count a timer for the sound  
.chuckrun:
  
     
  
      ld a,(chuckruncount)
      inc a
      ld (chuckruncount),a
      cp 25
      ret c 
       
      xor a
      ld (chuckruncount),a 
       
  
       ld    de,03*256 + sfxchuckrun
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number    

    ;ret  
    
    
.chuckauw:

    ld a,sccchuckauw
    jp ReplayerInit
  
  
   ;    ld    de,04*256 + sfxchuckauw
   ;    jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number    

    ;ret  
    
    
.chuckstomp:

      push af
  
    ld a,sccchuckhit
    call ReplayerInit
  
  ;     ld    de,04*256 + sfxchuckstomp
  ;     call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number    

      pop af

    ret  
  
.coin:

;      push af
  
       ld    de,04*256 + sfxcoin
       jp  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number    

 ;     pop af

 ;   ret 
  
    

.setcarry:

  scf
  ret


mariokickshellsfx:   db 0
stompcounter:        db 0 ;gets set at each stomp and counts down to 0
stompcount:          db 0 ;increases with 1 and gets set to 0 if stompcounter reaches 0
chuckruncount:       db 0 ;keep count of the chuckrunning time

sfxjump:             equ 00
sfxstomp:            equ 01
sfxspinjump:         equ 02
sfxkick:             equ 03
sfxfireball:         equ 04
sfxoneup:            equ 05
sfxbreakblock:       equ 06
sfxcoin:             equ 07
sfxgetfeather:       equ 08
sfxmapmovetospot:    equ 09
sfxcheckpoint:       equ 10
sfxpause:            equ 11
sfxpipe:             equ 12
sfxpowerup:          equ 13
sfxpowerupappears:   equ 14   
sfxstomp2:           equ 15   
sfxstomp3:           equ 16   
sfxstomp4:           equ 17   
sfxstomp5:           equ 18   
sfxbumphead:         equ 19
sfxpowerdown:        equ 20
sfxspinjumpstomp:    equ 21
sfxbanzaibill:       equ 22
sfxflyingmariorise:  equ 23
sfxspringjump:       equ 24
sfxchuckclap:        equ 25
sfxchuckrun:         equ 26
sfxchuckauw:         equ 27
sfxchuckstomp:       equ 28
sfxswim:             equ 29
sfxyoshispit:        equ 30
sfxyoshimount:       equ 31
sfxyoshifireball:    equ 32
sfxdoor:             equ 33
sfxyoshicoin:        equ 34
sfxyoshitongue:      equ 35
sfxegghatch:         equ 36
sfxspringjump2:      equ 37
sfxbubblepop:        equ 38
sfxincorrect:        equ 39
sfxsave:             equ 40
sfxbonestomp:        equ 41
sfxbat:              equ 42
sfxyoshihit:         equ 43
sfxblargh:           equ 44
sfxmapcreatepath:    equ 45

;SCC sfx definitions
sccblargh:           equ 0
sccFireball:         equ 1
sccbreakblock:       equ 2
sccbubblesplash:     equ 3
sccmarioflyup:       equ 4
sccdoor:             equ 5
sccpickupfeather:    equ 6
sccsecretexit:       equ 7
sccfireball2:        equ 8
sccwrongtictac:      equ 9
sccbossdies:         equ 10
sccmagikoopabeam:    equ 11
sccmarioshrink:      equ 12
sccpowerreserve:     equ 13
sccyoshimount:       equ 14
sccshellbounce:      equ 15
sccspring:           equ 16
sccchuckauw:         equ 17
sccchuckhit:         equ 18
sccmarioswimup:      equ 19
sccbat:              equ 20
sccexplosion:        equ 21
sccthunder:          equ 22
sccfireballshoot:    equ 23
sccyoshispit:        equ 24
sccmapexplosion:     equ 25



