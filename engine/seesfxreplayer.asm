

; File:SEE3EDPL.asm             Project:SEE v3.00 release A    Update:31/10/94
; Sound Effect Driver v3.00A    coding:SHADOW                  (c) FUZZY LOGIC
; Replay a SEE file (v3.xx only!)

; Song structure OFFSETS
;SEEID: equ   $00             	;"SEE3EDIT"
;HISPT: equ   $08             	;Highest used Start_pattern
;HIPTA: equ   $0A             	;Higest used pattern+1_address
;HISfx: equ   $0C             	;Highest used Sfx (not used)

SEEadrPatts: equ  $8000+$0210 ;Base address + Pattern data (max 1024 patts)
SEEadrpostb: equ  $8000+$0010 ;Base address + Table with Sfx start pattern
;+-----------------------------------------------------------------------------+

DoForceSfx:
  ld    (Sfxnr),a
  xor   a
  ld    (ForceSfx?),a
  ld    a,(ForceSfx_PR)
  ld    (SfxPriority),a
  jp    SetSfx.DoSetSfx		

SetSfx:
  ld a,(darky?) ;branch to special player that is in the darky
  and a
  jp nz,SetSfxd

  ld    a,(SetSfx?)           ;should a Sfx be played?
  or    a
  ret   z
  xor   a
  ld    (SetSfx?),a

  ld    a,(ForceSfx?)
  or    a
  jr    nz,DoForceSfx		

  ld    a,(Sfxon?)            ;check if a Sfx is currently
  or    a
  jp    z,.DoSetSfx           ;Sfx is not being played, so do set Sfx

  ld    a,(SfxpreviousPriority)
  ld    b,a                   ;check priority of previous Sfx
  ld    a,(SfxPriority)       ;check priority of new Sfx
  cp    b
  ret   c                     ;new priority is lower, dont play Sfx

.DoSetSfx:
  ld    a,(SfxPriority)
  ld    (SfxpreviousPriority),a

  ld    a,(Sfxnr)
  add   a,a                   ;Sfxnr*2
  ld    h,0
  ld    l,a
  ld    de,SEEadrpostb
  add   hl,de                 ;Sfx base address + Table with Sfx start pattern

  ld    c,(hl)                ;Get start pattern
  inc   hl
  ld    b,(hl)

  ; Calculate pattern address
  ; In:  bc=Pat_nr, pattern nr (0-$3FF)
  ld l,c
  ld h,b
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl
  sbc hl,bc
  ld bc,SEEadrPatts
  add hl,bc
  ld    (PATADD),hl

  ld    hl,0
  ld    (tempo),hl
  ld    a,1
  ld    (Sfxon?),a            ;Sfx on
  ret

MainSfxIntRoutine:


  ld a,(sfxactive) ;sfx is active
  and a
  ret z
  
  
  ld    a,(Sfxon?)
  or    a
  ret   z



  ld    a,(tempo+1)
  sub   a,1
  ld    (tempo+1),a
  ret   nc
  ld    a,(tempo+0)
  ld    (tempo+1),a
  ld    hl,(PATADD)

  ld    a,(_HALT)             ;Halt counter
  and   a
  jr    z,.Main

  dec   A
  ld    (_HALT),a
  ret   nz
  jp    .Main2                ;previously a HALT Event was done, now do PSG

.Main:
  call  Event                 ;Do operation command

.Main2: 
  call  SetPsg                ;Set PSG reg.
  ld    (PATADD),hl
  ret

;+----- Event Commands -----+
Event: 
  ld    a,(hl)                ;Get Event
  ld    c,a
  and   $70
  ret   z                     ;No command

  cp    $10
  jr    z,.EV_hlT             ;Halt
  cp    $20
  jp    z,.EV_For             ;Loop start
  cp    $30
  jp    z,.EV_Nxt             ;Loop next
  cp    $40
  jr    z,.EV_Str             ;Continue Loop start
  cp    $50
  jr    z,.EV_Rer             ;Continue Loop next
  cp    $60
  jr    z,.EV_Tem             ;tempo

  ;- Effect end
.EV_End: 
  xor   a
  ld    (Sfxon?),a
  pop   af                    ;restore stack
  ret

  ;- Halt "hlt:x"
.EV_hlT: 
  ld    a,c
  and   $0f
  ld    (_HALT),a
  pop   af                    ;restore stack
  ret

  ;- Loop start "for:x"
.EV_For: 
  ld    a,(loopnr)            ;inc loop nr
  inc   a
  and   $03
  ld    (loopnr),a

  ex		de,hl

  ld    hl,loopbf

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc			
      
  ld		a,(de)
  and   $0f
  ld    (hl),a                ;[Counter]
  inc   hl
  ld    (hl),e                ;Set address
  inc   hl
  ld    (hl),d

  ex		de,hl
  ret

  ;- Loop next "next"
.EV_Nxt: 
  ld    a,(loopnr)
  and   $03

  ex    de,hl
  ld    hl,loopbf

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc				

  dec   (hl)                  ;dec. counter
  jr    z,.Ev_Nxt1            ;Loop end
      
  inc   hl
  ld    c,(hl)                ;get address
  inc   hl
  ld    h,(hl)
  ld    l,c
  ret

  ; Loop done, now remove this loop
.Ev_Nxt1: 
  ld    a,(loopnr)            ;dec loopnr
  dec   a
  and   3
  ld    (loopnr),a

  ex		de,hl
  ret

  ;- Continue loop start "start"
.EV_Str: 
  ld    (clpadr),hl           ;Store Patt adr.
  ret

  ;- Continue loop next "rerun"
.EV_Rer: 
  ld    hl,(clpadr)
  ret

  ;- tempo "tmp:x"
.EV_Tem: 
  ld    a,c
  and   $0f
  ld    (tempo+0),a
  ld    (tempo+1),a
  ret

;+----- Set all PSG regs.-----+
SetPsg:                       
  ld	  a,(SfxpreviousPriority)
  cp	  254			              ;Sfx being played that
  jp    Z,SetPsg3		          ;requires 3 channels ??

  ld    de,PsgReg+4

  ld    bc,5          
  add   hl,bc                 ;start with reg.4 (PSG Channel 3)

  ;reg 0 - 5. [sound frequency]       
  ld    c,(hl)                ;reg.0, 2 or 4
  inc   hl
  ld    b,(hl)                ;reg.1, 3 or 5
  
;add turbo support, sigh even more clockcycles wasted!
  ld a,(turbor?)
  cp 2
  call nc,setturbofreq15 ;branch in here we want minimal clockcycles wasted on this one

  bit   7,(hl)                ;check and handle Tuning
  jp    Z,.Dont_TUNWUP

  ;TUNWUP: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  add   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl
.Dont_TUNWUP:

  bit   6,(hl)
  jp    z,.Dont_TUNWDW     
  ;TUNWDW: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  xor   a
  sbc   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl

.Dont_TUNWDW:
  inc   hl

  ld    a,4                   ;reg.4   
  out   ($a0),a
  ld    a,c
  out   ($a1),a
  ld    (de),a
  inc   de        

  ld    a,5                   ;reg.5   
  out   ($a0),a                
  ld    a,b
  and   $0f
  out   ($a1),a
  ld    (de),a
  inc   de         

;Use reg.6 ONLY when a soundeffect needs specific noise	
;		ld    a,6             ;reg.6   
;		out   ($A0),a
		
;		ld    a,(hl)          ;reg.6 [Rustle frequency]
;		bit   7,a
;		jp    Z,.DontTuneUp
		
;		ex    de,hl
;		add   a,(hl)          ; Up
;		ex    de,hl
;.DontTuneUp:
		
;		bit   6,a
;		jp    Z,.DontTuneDown
;		ex    de,hl
;		sub   (hl)            ; Down
;		ex    de,hl
;		NEG
;.DontTuneDown:
		
;		and   $1F
;		out   ($A1),a
;		ld    (de),a

  inc   hl             
  inc   de

  ld    a,7             ;reg.7 [set channel ON/OFF]
  out   ($a0),a         ;read r#7, which channels are used
  in    a,($a2)         ;by SCC-blaffer at this moment?
  ;       B A  Noise C B A  Tone C B A
  and   %11'011'011    ;0=sound output  
  ld    c,a
  ld    a,(hl)
  and   %00'100'100
  or    c
  out   ($a1),a       ;write value to reg.7

  inc   hl
  inc   hl
  inc   hl
  inc   de
  inc   de
  inc   de

  ;reg 8-10 [Volume]
  ld    a,10             ;reg.10   
  out   ($a0),a
				
  ld    a,(hl)
  inc   hl
  ld    c,a
  and   $0f
  bit   7,c
  jp    z,.No_TUN_UP
  ;TUN_UP:
  ex    de,hl
  add   a,(hl)
  ex    de,hl
.No_TUN_UP:
  bit   6,c
  jp    z,.No_VOL_DW
  ;VOL_DW: 
  and   $0f
  ld    b,a
  ld    a,(de)
  sub   a,b
  jp    nc,.No_VOL_DW
  xor   a
.No_VOL_DW:
  ;FIXVOL: 
  and   $0f
  ld    (de),a

;Set Maximum PSG Volume
;.SetMaxPSGVolume:	Equ	$+1
;  sub   a,1
;  jp    nc,.SetPsgvolume
;  xor   a
;.SetPsgvolume:
;/Set Maximum PSG Volume

  out   ($A1),a               ;write volume value
 
;reg 11-13 [volume effect]
  ld    c,$a1                 ;out port

  ld    a,11                  ;reg.11   
  out   ($a0),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.12   
  out   ($a0),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.13   
  out   ($a0),a
  outi                        ;out  (c),(hl)  and  inc hl
  ret

; Shut up PSG!
;PSGOFF: 
;Only mute Channel 3
;        ld      a,7             ;reg.7 [set channel ON/OFF]
;        out     ($A0),a         ;read r#7, which channels are used
;        IN      a,($A2)         ;by SCC-blaffer at this moment?
;       I/O       B A  Noise C B A  Tone C B A
;        and     %00'111'111    ;0=sound output  
;        or      %00'100'100    ;shut up channel C
;        out     ($A1),a

;        ld    a,7               ;reg.7 [set channel ON/OFF]  
;        out   ($A0),a           ;I/O B A Noise C B A Tone C B A
;        ld    a,                %00'111'111  ;0=sound output  
;        out   ($A1),a
;        ret


; Write PSG (not ROM-BIOS compatible!)
; In:   a, data
;       E, port
;      IX, PSG store table
; out:  E+1
;      IX+1
;WRTPSG: ld    (IX),a
;        inc   IX
;        push  af
;        ld    a,E
;        out   ($A0),a
;        inc   E
;        pop   af
;        out   ($A1),a
;        ret

;+----- Set all 3 PSG regs.-----+
SetPsg3:                       
        LD    IX,PsgReg
        LD    E,0             ;Start reg.0
        INC   hl              ;[skip Event byte]

        LD    B,3             ;First 6 reg. [sound frequency]
STPS.n0: PUSH  bc
        LD    C,(hl)          ;reg.0, 2 or 4
        INC   hl
        LD    B,(hl)          ;reg.1, 3 or 5
        BIT   7,(hl)          ;check and handle Tuning
        call  NZ,TUNWUP
        BIT   6,(hl)
        call  NZ,TUNWDW
        INC   hl
        LD    a,C
        call  WRTPSG
        LD    a,B
        AND   $0F
        call  WRTPSG
        pop   bc
        DJNZ  STPS.n0

        LD    a,(hl)          ;reg.6 [Rustle frequency]
        INC   hl
        BIT   7,a
        call  NZ,TUN_UP
        BIT   6,a
        call  NZ,TUN_DW
        AND   $1F
        call  WRTPSG

        LD    a,(hl)          ;reg.7 [set channel ON/OFF]
        AND   $3F
        OR    $80             ;set bit 7 is necessary to write the PSG!!
        INC   hl
        call  WRTPSG

        LD    B,3             ;reg 8-10 [Volume]
STPS.n1: PUSH  bc
        LD    a,(hl)
        INC   hl
;        BIT   5,a
;        jp    NZ,STPS.2       ;Volume pattern is set.
        LD    C,a
        AND   $0F
        BIT   7,C
        call  NZ,TUN_UP
        BIT   6,C
        call  NZ,VOL_DW
;        call  FIXVOL


; Make relative volume (depending on "SEEVOL")
; In:  a, volume
; Out: a, new volume [seevol - (15-volume)]
FIXVOL: AND   $0F

		LD    (IX),a


;Set Maximum PSG Volume
.SetMaxPSGVolume:	equ	$+1
		SUB		a,1
		jp		NC,.SetPsgvolume
		XOR		A
.SetPsgvolume:
;/Set Maximum PSG Volume


STPS.n2: 
		AND   $1F
;		call  WRTPSG

        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($A0),a
        INC   E
        pop   AF
        OUT   ($A1),a

		pop   bc
		DJNZ  STPS.n1

        LD    B,3             ; reg 11-13 [volume effect]
STPS_2: LD    a,(hl)
        INC   hl
        call  WRTPSG
        DJNZ  STPS_2
        ret


; Tuning
;- byte
TUN_UP: ADD   a,(IX)          ; Up
        ret
TUN_DW: SUB   (IX)            ; Down
        NEG
        ret
;- word
TUNWUP: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        ADD   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret
TUNWDW: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        XOR   A
        sbc   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret

; Volume Down
VOL_DW: AND   $0F
        LD    B,a
        LD    a,(IX+0)
        SUB   B
        ret   NC
        XOR   A
        ret


; Write PSG (not ROM-BIOS compatible!)
; In:   a, data
;       E, port
;      IX, PSG store table
; Out:  E+1
;      IX+1
WRTPSG: LD    (IX),a
        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($A0),a
        INC   E
        pop   AF
        OUT   ($A1),a
        ret


setturbofreq15:
;frequency divide by 2 lets see what happens
;so the story is lowering the value makes the freq higher
;turning bc up lowers the frequency
;we need to multiply by 1,5 :S
  cp 3
  jp z,.setx2

   push hl
   push de
   
   push bc
   pop de
   srl d
   rr e ;/2
   
   push bc
   pop hl
   add hl,de 
   push hl 
   pop bc

   pop de
   pop hl
   
   ret
   
.setx2:

   sla c
   rl b ;*2

  ret


