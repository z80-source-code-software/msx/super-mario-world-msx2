;routine die mario transformeert zodra deze word gecalled treed er een transformatie op van x naar y
;er word een event nummer doorgegeven die word herkent door de transform routine en dan word afgehandeld.
;het blijkt dat mario zijn transformaties allemaal hetzelfde zijn ongeacht de state
;dus van klein naar groot is een kant op alle grote marios naar klein hebben hetzelfde filmpje
;klein naar vuur etc allemaal hetzelfe filmpje


mariotransform:

  ld a,(pauze)
  dec a
  ret z


;was mario niet toevallig neergekomen vanuit vliegen?
;TODO: moet deze tripscreen code niet apart worden gemaakt??
  ld a,(tripscreen)
  and a
  call nz,tripscreenupdown

  ld a,(mariotransforming)
  cp 10
  jp z,.skipshellchecks ;Prevent heavy conflict during dying!

 
 ;mario pakt schelpje op 
  ld a,(mariopickupshell)
  dec a
  jp z,pickupshell

 ;mario kickt hem weg
  ld a,(mariokickshell)
  dec a
  jp z,dropshell

.skipshellchecks:


  ;de state waarnaar mario transformeert
  ld a,(mariotransforming)
  cp 1
  jp z,shrinktonormal
  cp 2
  jp z,growtosupermario
  cp 3
  jp z,growtofiremario
  cp 4
  jp z,growtoflyingmario
  cp 5
  jp z,growtostar
  cp 10 ;mario sterft
  jp z,mariodie
 

  ret


shrinktonormal:

;mario stilzetten zodat ook de voetjes stoppen met bewegen etc
  xor a
  ld (mariospeed),a
;  ld (powerreserve),a

  ld a,1
  ld (mariotransforming),a
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a
  
;ja we willen echt supernes style transformatie zonder gelul :)

    cp 2
  jp z,marionormal
    cp 4
  jp z,mariolarge
    cp 6
  jp z,marionormal
    cp 8
  jp z,mariolarge
    cp 10
  jp z,marionormal
    cp 12
  jp z,mariolarge
    cp 14
  jp z,marionormal
    cp 16
  jp z,mariolarge
    cp 18
  jp z,marionormal
    cp 20
  jp z,mariolarge
    cp 22
  jp z,marionormal
 
 ;beetje pauze vind ik echt mooier
  cp 30
  jp z,.endtransformation
  
  ret
  
.endtransformation:
    
    xor a
    ld (mariostate),a
    ld (mariotransforming),a
    ld a,(powerreserve)
    and a
    ret z ;nothing in powerreserve to drop
    ld a,1
      ld (droppowerup),a
    ret


growtosupermario: ;dit vind ik iets veiliger

 ;mario stilzetten zodat ook de voetjes stoppen met bewegen etc
  xor a
  ld (mariospeed),a
 
 
  ld a,2
  ld (mariotransforming),a
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a
  
;ja we willen echt supernes style transformatie zonder gelul :)

    cp 2
  jp z,marionormal
    cp 4
  jp z,mariolarge
    cp 6
  jp z,marionormal
    cp 8
  jp z,mariolarge
    cp 10
  jp z,marionormal
    cp 12
  jp z,mariolarge
    cp 14
  jp z,marionormal
    cp 16
  jp z,mariolarge
    cp 18
  jp z,marionormal
    cp 20
  jp z,mariolarge
 
  cp 30
  jp z,.endtransformation
  
  ret
  
.endtransformation:
    ld a,1
    ld (mariostate),a
  
  
    xor a
    ld (mariotransforming),a
    ret

growtofiremario: ;dit vind ik iets veiliger

 ;mario stilzetten zodat ook de voetjes stoppen met bewegen etc
  xor a
  ld (mariospeed),a
 
 
  ld a,3
  ld (mariotransforming),a
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a
  
;ja we willen echt supernes style transformatie zonder gelul :)

    cp 2
  jp z,mariolarge
    cp 4
  jp z,mariofire
    cp 6
  jp z,mariolarge
    cp 8
  jp z,mariofire
    cp 10
  jp z,mariolarge
    cp 12
  jp z,mariofire
    cp 14
  jp z,mariolarge
    cp 16
  jp z,mariofire
    cp 18
  jp z,mariolarge
    cp 20
  jp z,mariofire

;korte stop is mooier 
  cp 30
  jp z,.endtransformation
  
  ret
  
.endtransformation:
    ld a,2
    ld (mariostate),a
  
  
    xor a
    ld (mariotransforming),a
    ret
 
growtoflyingmario:

  ;mario stilzetten zodat ook de voetjes stoppen met bewegen etc
  xor a
  ld (mariospeed),a
 
 
  ld a,4
  ld (mariotransforming),a
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a
  
;ja we willen echt supernes style transformatie zonder gelul :)
;TODO: ik vind deze transformatie nog niet echt mooi. Misschien moeten we een aparte sprite hiervoor
;maken net zoals op de supernes.

    cp 2
  jp z,mariofire
    cp 4
  jp z,mariofeather
    cp 6
  jp z,mariofire
    cp 8
  jp z,mariofeather
    cp 10
  jp z,mariofire
    cp 12
  jp z,mariofeather
    cp 14
  jp z,mariofire
    cp 16
  jp z,mariofeather
    cp 18
  jp z,mariofire
    cp 20
  jp z,mariofeather

;korte stop is mooier 
  cp 30
  jp z,.endtransformation
  
  ret
  
.endtransformation:
    ld a,3
    ld (mariostate),a
 
  
    xor a
    ld (mariotransforming),a
    ret

;hier word alleen een waarde gezet die door de objecthandler word herkend als mario = clipping 2
;en de spriteengine varieert de color gebeuren van mario
growtostar:

  ld a,1
  ld (mariostar),a
  ld (switchmusicforstar),a
  xor a
  ld (mariotransforming),a
  
  ld hl,(mariox) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8  

  ld de,(marioy) ;16 bits

  srl d ;nu moet je hl delen door 8
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8  

  ld    iy,CustomObject
  ld    (iy),available 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),38               ;set object number
  ld    (iy+6),0               ;set object number



  jp transformintostar


 
;elke mariostate heeft zijn eigen pickupshell daarom handelen we de sprite af in de sprite engine zelf
;pickupshell blockt ook niet de phsysics! het speelt alleen maar een sprite filmpje af.

pickupshell:
 


  ;tijdens zemmen is er geen pickup filmpje
  ld a,(marioswim)
  dec a
  jp z,.endtransformation

  ld a,1
  ld (mariopickupshell),a
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a

  cp 3
  jp z,.endtransformation

  ret
  
  
.endtransformation:
    ld a,1
    ld (marioshell),a
  
  
    xor a
    ld (mariopickupshell),a
    ret  


dropshell:



  ;tijdens zemmen is er geen drop filmpje
  ld a,(marioswim)
  dec a
  jp z,.endtransformation

  call Playsfx.kick
  
;  ld a,1
;  ld (mariokickshell),a
  ld a,1
  ld (mariokickshellsfx),a
  
  
  ld a,(mariotransformp)
  inc a
  ld (mariotransformp),a

  cp 3
  jp z,.endtransformation

  ret
  
  
.endtransformation:
    xor a
    ld (marioshell),a
    ld (marioblueblock),a
  
  
    xor a
    ld (mariokickshell),a
      ld (mariokickshellsfx),a
    ret 

mariodie:

  ld a,(dieatonce)
  and a
  jp nz,.setdeathpointermax

  ld a,(mariostate)
  and a
  jp nz,.setshrink


;we zetten hier al de blocknumber omdat we in principe alles vanaf hier afhandelen
  ld a,$19
  ld (blocknumber),a



  ;reset the player then set the new music block
  ld b,romprogblock ;save the state of this block
  di
  ld a,$30
  ld (currentmusicblock),a
  ld a,(mariotransformp)
  cp 1 
  push af
  call c,switchmusic
  pop af
  call c,.resetwait
  ei


;we pauzeren ongeveer 20 frames en gaan dan mario laten sterven
  ld a,(mariotransformp)
  cp 20
  jp nc,.deathstart
  inc a
  ld (mariotransformp),a
  
  ;jumppointer resetten
  xor a
  ld (mariojumpupp),a
  ld (marionotdown),a
  ld (mariospeed),a ;staat netter
  
  jp mariodead.mariospritewalkleft1

.resetwait:

  xor a
  ld (deathwaitsecure),a

  ret


.setdeathpointermax:
  ;we will make this routine so that the jumppointer can dynamicly be adapted

  ld a,(dieatonce)
  inc a
  ld (dieatonce),a
  cp 60
  ret c

  ld a,21
  ld (mariotransformp),a
  xor a
  ld (dieatonce),a
  
  ;reset the player then set the new music block
  ld b,romprogblock ;save the state of this block
  di
  ld a,$30
  ld (currentmusicblock),a
  call switchmusic
  ei
  ret

.setfall:

  ld a,1
  ld (marionotdown),a
  ret

;nu komt de deathscene
.deathstart:    

  ld a,(deathwaitsecure)
  inc a
  ld (deathwaitsecure),a
  cp 255
  jp z,.deathend ;waitsecure in case this routine fails


  call mariodead
  
  ;niet uit het beeld vallen
  ld a,(mariocoordinates)
  cp 5
  call c,.setfall
  
  ;mario was buiten het beeld dus direct vallen 
  ld a,(marionotdown)
  dec a
  jp z,.fall
  
  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorjumpup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.fall
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e


  jp .setjumpcoordinates
  
  ret

  ;hier worden de uiteindelijke mario coordinaten geset
.setjumpcoordinates:
  

  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a
  
  
  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
  
  
  ret


.fall:

  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtable
  add hl,de
  ld a,(hl)
  cp 7
  ld b,a
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e

.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 
  ld a,(mariocoordinates)
  cp 216
  jp nc,.restart
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
  ld b,0
  add hl,bc
  ld (marioy),hl
 
  ret

;mario is nu officieel de pijp uit we wachten even en dan resetten we zijn coordinaten
.restart:

  ld a,(mariotransformp)
  cp 110
  jp nc,.deathend
  inc a
  ld (mariotransformp),a
  ret

;mario is nog groot eerst klein worden

.setshrink:


  ld a,1
  ld (mariotransforming),a
  ld (mariogod),a
  
  xor a
    ld (mariotransformp),a


  ret

.deathend:

  ld a,2
  ld (nocontrol),a
  ld a,1
  ld (levelend?),a
  xor a
  ld (marioyoshi),a ;mario must be removed from yoshi. Sorry man you just died so this is your punishment
  ld (marioshell),a
  ld (yoshicoin),a ;no more yoshicoins

  ret

  ;temp routine later resetten we de map gewoon.

;;we resetten de coordinaten
;  ld hl,128
;  ld (mariox),hl
;  ld hl,344-16
;  ld (marioy),hl
; 
;;alle spat waarden resetten en de mariocoordinates
;
;  ld a,100
;  ld (mariocoordinates),a
; 
;  call resetspat ;temp kan later weg
;
;;camera een harde reset geven
;
;  ld hl,0
;  ld (camerax),hl
;
;
;;zetten de state terug op 0
;
;
;   xor a
;   ld (mariotransforming),a
;
;  ret


resetspat:

  xor a
  ld (spat+1),a
  ld (spat+5),a
  ld (spat+9),a
  ld (spat+13),a
  ld (spat+17),a
  ld (spat+21),a
  ld (spat+25),a
  ld (spat+29),a
  ld (spat+33),a
  ld (spat+37),a
  ld (spat+41),a
  ld (spat+45),a
  ld (spat+49),a
  ld (spat+53),a
  ld (spat+57),a
  ld (spat+61),a
  ld (spat+65),a
  ld (spat+69),a
  ld (spat+73),a
  ld (spat+77),a
  ld (spat+81),a
  ld (spat+85),a
  ld (spat+89),a
  ld (spat+93),a
  ld (spat+97),a
  ld (spat+101),a
  ld (spat+105),a
  ld (spat+109),a
  ld (spat+113),a
  ld (spat+117),a
  ld (spat+121),a
  ld (spat+125),a
  
  ld a,226
  ld (spat+0),a
  ld (spat+4),a
  ld (spat+8),a
  ld (spat+12),a
  ld (spat+16),a
  ld (spat+20),a
  ld (spat+24),a
  ld (spat+28),a
  ld (spat+32),a
  ld (spat+36),a
  ld (spat+40),a
  ld (spat+44),a
  ld (spat+48),a
  ld (spat+52),a
  ld (spat+56),a
  ld (spat+60),a
  ld (spat+64),a
  ld (spat+68),a
  ld (spat+72),a
  ld (spat+76),a
  ld (spat+80),a
  ld (spat+84),a
  ld (spat+88),a
  ld (spat+92),a
  ld (spat+96),a
  ld (spat+100),a
  ld (spat+104),a
  ld (spat+108),a
  ld (spat+112),a
  ld (spat+116),a
  ld (spat+120),a
  ld (spat+124),a
  
  ret
