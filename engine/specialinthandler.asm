specialinthandler:

 
  push  af


  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,.Lineint        ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,.vblank          ;vblank detected, so jp to that routine

  

  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret


.screen4: equ InterruptHandler + ( $ - specialinthandler)  
  
    ld a,80-16-16
    ld (lineintheight),a
   
  ;split pallette
    ld hl,currentpalette
	xor		a
	out		($99),a
	ld		a,16+128
	out		($99),a
	ld		bc,$209A
	otir     
   
;switch to screen 4
  ld    a,(VDP_0)
  and   %1111'1101		      ;m3=0
  ld    (VDP_0),a
  out   ($99),a
  ld    a,128
  out   ($99),a
;/switch to screen 4  
	ld		a,(VDP_0+2)  
	out		($99),a		
	ld		a,2+128
	out		($99),a 

  ld a,(lineintheight)
  ld b,a
  ld                a,(VDP_8+15)      ;lineinterrupt has to adapt to the vertical screen offset
  add a,b
  out   ($99),a
  ld    a,19+128
  out   ($99),a 


  
  
  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 



  ei
  ret 
  
.screen5: equ InterruptHandler + ( $ - specialinthandler)

    push af

    ld a,80+80-16
    ld (lineintheight),a
    
  ;switch to screen 5
  ld    a,(VDP_0)
  or    %0000'0010		      ;m3=1
  ld    (VDP_0),a
  out   ($99),a
  ld    a,128
  out   ($99),a
;/switch to screen 5 
  ld a,$5f
  out ($99),a
  ld a,128 + 2
  out ($99),a ;page 2
  
  
  ld a,(lineintheight)
  ld b,a
  ld                a,(VDP_8+15)      ;lineinterrupt has to adapt to the vertical screen offset
  add a,b
  out   ($99),a
  ld    a,19+128
  out   ($99),a  

  ;split pallette
    ld hl,editablepalette
	xor		a
	out		($99),a
	ld		a,16+128
	out		($99),a
	ld		bc,$209A
	otir  
  
  pop af
  
  ret

  
  
.Lineint: equ InterruptHandler + ( $ - specialinthandler)
 

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix

  ld a,(lineintheight)
  cp 80-16-16
  call z,.screen5
  cp 80+80-16
  jp z,.screen4
  

  
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
      ld                a,(slot.page12rom)        ;all RAM except page 1+2
        out                ($a8),a         

  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a

  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.


  ;handle sfx
        ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc               a
        ld                ($7000),a
        ld                a,Sfxblock
        ld                ($9000),a
        inc               a
        ld                ($b000),a
  call  MainSfxIntRoutine       ;keep running sfx every int
  call  SetSfx                                     ;if sfx is on, then set to play

   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a
        
        

  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        

  ld a,(lineintflag)
  inc a
  ld (lineintflag),a

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 



  ei
  ret

.vblank: equ InterruptHandler + ( $ - specialinthandler)



;scc replayer this is the most reliable interval
  in a,($a8)
  push af

  ld a,(slot.page12rom)
  out ($a8),a

        ld                a,SCCreplayerblock+1   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
;        ld                ($5000),a
;        inc               a
        ld                ($7000),a
        ld a,$3f ;get bank ready for SCC access
        ld                ($9000),a
;        inc               a
;        ld                ($b000),a
        
        push hl
        push de
        push bc
        
  call  SccReplayerUpdate       ;keep running sfx every int
  
        pop bc
        pop de
        pop hl
   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
;        ld                a,(memblocks.4)
;        ld                ($b000),a
    pop af
    out ($a8),a

  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a
  
  pop   af 
  ei
  ret
  
lenghtspecialinterrupthandler: equ $ - specialinthandler

  
