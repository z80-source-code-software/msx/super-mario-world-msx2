;hidemariospriteright:
;
;  ld e,a ;store in e
;  push hl
;  exx
;  pop hl
;  
;  
;  call readspritefromvram
;  push de
;  call workupspriterightleft
;  pop hl
;  call writespritebacktovram
;  
;  ret


;objectunhider. This one handles the from down to top sprite removal of any object
;call this from the eventhandler. move it into the generalcode in rom if ROMspace permits
;in = A ; line that needs renewal. 0 removes entire sprite works from bottom to top
;in = BC ; number of the sprite to read from ROM *32
;in = HL ; sprite character address on VRAM
;in = IY ; IX adress of object
;it works different than hidemariosprite since it returns the sprite instead of removing it
unhideobjectsprite:

  ld e,a ;store in e
  push hl
  push bc
  exx
  pop bc
  pop hl
  
  push hl
  call copyspritetoram
  call workupspritedownup
  pop hl
  call writespritebacktovram
  
  ret
  

;ret

;the other way around
unhideobjectspriteupdown:

  ld e,a ;store in e
  push hl
  push bc
  exx
  pop bc
  pop hl
  
  push hl
  call copyspritetoram
  call workupspriteupdown
  pop hl
  call writespritebacktovram
  
  ret


;mariohider. This one handles the from down to top sprite removal of any object or mario
;call this from the eventhandler. move it into the generalcode in rom if ROMspace permits
;in = A ; line that needs removal. 0 removes entire sprite works from bottom to top
;in = HL ; sprite character address on VRAM
;step1: read mariosprite into RAM
;step2: apply timerstep as input by the objectmovement handler
;step3: write sprite back into VRAM
hidemariosprite:

  ld e,a ;store in e
  push hl
  exx
  pop hl
  
  
  call readspritefromvram
  push de
  call workupspritedownup
  pop hl
  call writespritebacktovram
  
  ret

;same as hidemariosprite. however 0 in A shows the entire sprite. and 16 removes it  
hidemariospritedown:

  ld e,a ;store in e
  push hl
  exx
  pop hl
  
  call readspritefromvram
  push de
  call workupspriteupdown
  pop hl
  call writespritebacktovram
  
  ret



readspritefromvram:
  
  ld a,1
  push hl
  call SetVdp_read
  pop de ;trow address in DE
  
 ld b,32
 ld hl,tempspritedata 
 ld c,$98
 call inix32 ;sprite data is now in RAM ready to get worked up :)
 call inix32 ;sprite data is now in RAM ready to get worked up :)

  ;push de

  ret


;in E = line to be worked up
workupspriteupdown: ;nieuwe routine

  exx

  ld d,0
  ld c,4
  ld a,e
  and a
  call z,.setmax
;  bit 7,a
;  call nz,.setmax ;watch out for negatives!
  ld b,a
  
  ld hl,tempspritedata
  
.workupsprites:

  push bc
  
.sprite1:

  xor a
  ld (hl),a
  inc hl

  djnz .sprite1 ;repeat for all lines
  
  pop bc
  push bc
  
.setback: ;set lines back to 0
  
  dec hl
  
  djnz .setback
  
  pop bc
  
  ld de,16
  add hl,de

  dec c
  
  jp nz,.workupsprites

  ret

.setmax:

  ld a,16
  ret

workupspritedownup: ;this one works DON'T TOUCH


;increment by 16 4 times


  exx

  ld a,16
  sub e
;  call c,.setmax ;never allow negatives!
  ret c
  and a
;  call z,.setmax
  ret z
  ld b,a ;amount of lines to work up
  ld d,0
  ld c,4 ;4 sprites per operation


.workupsprites:

   ld hl,tempspritedata
   add hl,de

  push bc
  
.sprite1:

  xor a
  ld (hl),a
  inc hl

  djnz .sprite1 ;repeat for all lines
  
  pop bc
  

  ex de,hl
  ld de,16
  add hl,de
  ex de,hl

  dec c
  
  jp nz,.workupsprites
 

  ret

;.setmax:
;
;  ld a,16
;  ret
  


writespritebacktovram:

;I think this can be a call.  
 ; pop hl
  
  
	ld		a,1
	call	SetVdp_Write

  ld hl,tempspritedata
  ld c,$98
  call outix64




  ret

;predefine spritepositions for mario
mariosprite1: equ 448
mariosprite2: equ 512

;get the VRAM spriteadress of the selected object. In HL = IX of the object
;in BC = spritenumber 
;out = Spriteadress in HL
getobjectspriteadress:

  push hl
  pop iy


  ld hl,spriteclrpositiontable
  ld a,c
;  sub e
  add a,a ;*2 the table consists of WORDS!!

  ld e,a
  ld d,0
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) ;sprite position in VRAM almost obtained  

  ld    l,(iy+coloraddress)     ;address of spritenumber in color table of VRAM (16 bits)
  ld    h,(iy+coloraddress+1)   ;address of spritenumber in color table of VRAM (16 bits)
 
  add hl,de

  ;set vdp address to write at character table and write spritecharacter
  add   hl,hl                   ;(color address*2) = character address (character address is 2x higher than color)
	ld		de,(invissprchatableaddress)
	add   hl,de

  ret


spriteclrpositiontable: dw 0,32/2,64/2,96/2,128/2,160/2,192/2,224/2,256/2,288/2,320/2,352/2,384/2,416/2,448/2,480/2,512/2,544/2,576/2,608/2,640/2,672/2,704/2,736/2,768/2,800/2,832/2,864/2,896/2,928/2,960/2,992/2

