objectlistlv048:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
    
  
  db available  :  dw  032 : dw 026,295  ;wiggler
 
  db available  :  dw  050 : dw 010,028  ;questionmark animation red flower

  db available :  dw  058 : dw 026-2,060  ;brown coopa
  db available :  dw  079 : dw 026-2,060  ;brown coopa
  db available :  dw  089 : dw 026-2,060  ;brown coopa

  db available :  dw  106 : dw 016,058  ;questionmark animation (green yoshi egg)


  db available  :  dw  114 : dw 022,295  ;wiggler

     db available :  dw  132 : dw 022,191  ;goomba
     db available :  dw  136 : dw 022,191  ;goomba

     db available :  dw  152 : dw 022,195  ;goomba (winged)

    db available  :  dw  159 : dw 022,205  ;spring 

  db available  :  dw  189 : dw 026,295  ;wiggler

   db available  :  dw  194 : dw 016,251  ;music note block
   db available  :  dw  196 : dw 016,251  ;music note block
   db available  :  dw  198 : dw 016,251  ;music note block
   db available  :  dw  200 : dw 016,251  ;music note block

  db available  :  dw  264 : dw 022,295  ;wiggler

  db force18   :  dw  301 : dw 015-3,004  ;checkpoint top
  db available :  dw  301+2 : dw 015,005  ;checkpoint bar
  db force04   :  dw  301+5 : dw 015-1,031  ;checkpoint pole right

      db available :  dw  317 : dw 020,196  ;changing powerup

;  db available |  dw  321 | dw 026-2,060  ;brown coopa

  db available  :  dw  354 : dw 014,296  ;exploding smileybox (all types)
  db available  :  dw  360 : dw 016,296  ;exploding smileybox (all types)
  db available  :  dw  387 : dw 016,296  ;exploding smileybox (all types)
  db available  :  dw  391 : dw 015,296  ;exploding smileybox (all types)


  db available  :  dw  392 : dw 026,295  ;wiggler

  db available  :  dw  397 : dw 017,296  ;exploding smileybox (all types)

  db available  :  dw  427 : dw 014,296  ;exploding smileybox (all types)


   db available :  dw  444 : dw 026,178  ;keyhole  
  
  
  db available  :  dw  453 : dw 012,295  ;wiggler

  db available  :  dw  471 : dw 008,296  ;exploding smileybox (all types)
  db available  :  dw  484 : dw 011,296  ;exploding smileybox (all types)
  db available  :  dw  486 : dw 013,296  ;exploding smileybox (all types)


  db available   :  dw  499   : dw 020,204  ;p-balloon

  db available  :  dw  531 : dw 026,295  ;wiggler

     db available :  dw  560+18 : dw 013-2,192  ;hammer brother 


  db force00   :  dw  601 : dw 008,007  ;endflag bar
  db force28   :  dw  601+1 : dw 008-1,006  ;endflag top

 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv048: equ $-objectlistlv048


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    20*8      :     db 48 ;-2
dw    66*8      :     db 32 ;-2
dw    76*8      :     db 32 ;-2
dw    86*8      :     db 32 ;-2
dw    144*8     :     db 48 ;-2
dw    184*8     :     db 32 ;-2
dw    222*8     :     db 48 ;-2
dw    228*8     :     db 48 ;-2
dw    234*8     :     db 48 ;-2
dw    240*8     :     db 48 ;-2
dw    246*8     :     db 48 ;-2
dw    252*8     :     db 48 ;-2
dw    324*8     :     db 32 ;-2
dw    328*8     :     db 32 ;-2
dw    332*8     :     db 32 ;-2
dw    364*8     :     db 48 ;-2
dw    525*8     :     db 48 ;-2
dw    537*8     :     db 32 ;-2
dw    000         ;defines the end of the list COMPULSORY!! 
