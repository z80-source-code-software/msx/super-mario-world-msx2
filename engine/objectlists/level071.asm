objectlistlv071:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 015	:	dw 043,093	;green flying koopa
	db available	:	dw 019	:	dw 043,093	;green flying koopa
	db available	:	dw 030	:	dw 034,029	;feather box
	db available	:	dw 042	:	dw 043,093	;green flying koopa
	db available	:	dw 068	:	dw 043,093	;green flying koopa
	db available	:	dw 081	:	dw 041,339	;countplateau 1
	db available	:	dw 095	:	dw 043,009	;Red flying koopa (up down)
	db available	:	dw 096	:	dw 031,055	;yoshi coin
	db available	:	dw 104	:	dw 045,339	;countplateau 1
	db available	:	dw 105	:	dw 034,340	;countplateau 4
	db available	:	dw 117	:	dw 048,009	;Red flying koopa (up down)
	db available	:	dw 125	:	dw 042,339	;countplateau 1
	db available	:	dw 137	:	dw 037,339	;countplateau 1
	db available	:	dw 144	:	dw 038,009	;Red flying koopa (up down)
	db available	:	dw 153	:	dw 045,339	;countplateau 1
	db available	:	dw 163	:	dw 042,009	;Red flying koopa (up down)
	db available	:	dw 164	:	dw 034,340	;countplateau 4
	db available	:	dw 177	:	dw 043,339	;countplateau 1
	db available	:	dw 181	:	dw 033,055	;yoshi coin
	db available	:	dw 185	:	dw 038,339	;countplateau 1
	db available	:	dw 222	:	dw 037,036	;Mario exit (up down)
	db available	:	dw 230	:	dw 030,055	;yoshi coin
	db available	:	dw 234	:	dw 038,339	;countplateau 1
	db available	:	dw 248	:	dw 040,009	;Red flying koopa (up down)
	db available	:	dw 252	:	dw 032,339	;countplateau 1
	db available	:	dw 284	:	dw 046,205	;spring
	db available	:	dw 291	:	dw 038,004	;Checkpointtop
	db available	:	dw 293	:	dw 041,005	;Checkpointbar
	db available	:	dw 296	:	dw 040,031	;checkpointright
	db available	:	dw 298	:	dw 030,028	;flower box
	db available	:	dw 334	:	dw 041,093	;green flying koopa
	db available	:	dw 340	:	dw 039,093	;green flying koopa
	db available	:	dw 345	:	dw 030,093	;green flying koopa
	db available	:	dw 349	:	dw 040,339	;countplateau 1
	db available	:	dw 355	:	dw 040,093	;green flying koopa
	db available	:	dw 370	:	dw 043,315	;Sheerplatform (right)
	db available	:	dw 380	:	dw 038,009	;Red flying koopa (up down)
	db available	:	dw 387	:	dw 044,340	;countplateau 4
	db available	:	dw 397	:	dw 031,339	;countplateau 1
	db available	:	dw 409	:	dw 040,339	;countplateau 1
	db available	:	dw 425	:	dw 038,009	;Red flying koopa (up down)
	db available	:	dw 429	:	dw 043,339	;countplateau 1
	db available	:	dw 446	:	dw 038,339	;countplateau 1
	db available	:	dw 449	:	dw 028,055	;yoshi coin
	db available	:	dw 454	:	dw 034,048	;banzai bill
	db available	:	dw 458	:	dw 027,314	;Sheerplatform (left)
	db available	:	dw 465	:	dw 034,009	;Red flying koopa (up down)
	db available	:	dw 469	:	dw 041,339	;countplateau 1
	db available	:	dw 484	:	dw 039,339	;countplateau 1
	db available	:	dw 490	:	dw 039,048	;banzai bill
	db available	:	dw 496	:	dw 035,340	;countplateau 4
	db available	:	dw 509	:	dw 037,339	;countplateau 1
	db available	:	dw 518	:	dw 033,055	;yoshi coin
	db available	:	dw 527	:	dw 031,339	;countplateau 1
	db available	:	dw 535	:	dw 034,048	;banzai bill
	db available	:	dw 542	:	dw 032,289	;greenmushroom (static)
	db available	:	dw 572	:	dw 031,007	;Endflagbar
	db available	:	dw 573	:	dw 030,006	;Endflagtop





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv071: equ $-objectlistlv071

