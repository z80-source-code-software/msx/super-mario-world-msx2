objectlistlv033:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
;  db available  :  dw  001 : dw 043+1,104  ;water (mario swimming) 

 
  db available  :  dw  028-1 : dw 049-4,149  ;mediumsheertile (left)
  db available  :  dw  036-1 : dw 045-4,149  ;mediumsheertile (left)

  db available  :  dw  040 : dw 042+1,121  ;fish (ADHD)

;    db available |  dw  045 | dw 038,237  ;mini charging chuck 

  db available :  dw  066 : dw 030-3,169  ;cavekoopa 
 
  db available  :  dw  072 : dw 041-1,171  ;mediumsheertile (right)
  
  db available  :  dw  074 : dw 032,028  ;questionmark animation red flower  
  db available  :  dw  076 : dw 032,057  ;questionmark animation (coin)  
  
  db available  :  dw  077 : dw 045,127  ;blockwall (16x16) 
  
  db available  :  dw  078 : dw 032,057  ;questionmark animation (coin)  
        
  db available  :  dw  080 : dw 045-1,171  ;mediumsheertile (right) 

    db available  :  dw  100 : dw 046,120  ;fish (dumb) (left and right)


  db available :  dw  118 : dw 032,074  ;smiley box (climbing plant)

  db available  :  dw  119 : dw 019,182  ;vinehelper ghost house (helper object)

    db available  :  dw  120 : dw 037,202  ;fish (dumb) (up and down)

   db available  :  dw  130 : dw 016-1,172  ;mediumsheertile (right) (short segment) 1/2 

    db available  :  dw  141 : dw 045,120  ;fish (dumb) (left and right)

    db available :  dw  145 : dw 018-2,169  ;cavekoopa

    db available  :  dw  158 : dw 046,120  ;fish (dumb) (left and right)


  db available :  dw  160 : dw 028,055  ;yoshicoin


   db available :  dw  162 : dw 008-2,169  ;cavekoopa

    db available  :  dw  163 : dw 043,120  ;fish (dumb) (left and right)

  db available  :  dw  166-1 : dw 033-4,150  ;mediumsheertile (left) (short segment) 1/2 

    db available :  dw  168 : dw 020-2,169  ;cavekoopa

   db available :  dw  169 : dw 047,178  ;keyhole  

    db available  :  dw  170 : dw 040,120  ;fish (dumb) (left and right)

    db available  :  dw  186 : dw 039,202  ;fish (dumb) (up and down)

  db available :  dw  197 : dw 020,055  ;yoshicoin

    db available  :  dw  214 : dw 041,120  ;fish (dumb) (left and right)
    db available  :  dw  218 : dw 047,120  ;fish (dumb) (left and right)

      db available :  dw  228+1 : dw 029,225  ;secret box (1 up)

  db available  :  dw  230 : dw 007,028  ;questionmark animation red flower 

    db available  :  dw  231 : dw 038,202  ;fish (dumb) (up and down)

  db available  :  dw  236 : dw 029,057  ;questionmark animation (coin)
  db available  :  dw  238 : dw 029,057  ;questionmark animation (coin)

        db available :  dw  240 : dw 029,029  ;questionmark animation (feather)
          db available  :  dw  241 : dw 041,120  ;fish (dumb) (left and right)  
        
db available  :  dw  242 : dw 011,076  ;p-box (blue)

    db available :  dw  253 : dw 009-2,169  ;cavekoopa

   db available  :  dw  255 : dw 013-1,172  ;mediumsheertile (right) (short segment) 1/2

    db available  :  dw  265 : dw 038,202  ;fish (dumb) (up and down)

    db available :  dw  269 : dw 014-2,169  ;cavekoopa

    db available  :  dw  271 : dw 038,202  ;fish (dumb) (up and down)

  db available  :  dw  272 : dw 028,028  ;questionmark animation red flower 

  db available :  dw  275 : dw 008,055  ;yoshicoin

  db available  :  dw  291-1 : dw 016-4,149  ;mediumsheertile (left)

    db available  :  dw  292 : dw 038,202  ;fish (dumb) (up and down)
  
  db available  :  dw  298-1 : dw 048-4,149  ;mediumsheertile (left)
  
  db available  :  dw  299-1 : dw 012-4,149  ;mediumsheertile (left) 
  
    db available  :  dw  302 : dw 040,120  ;fish (dumb) (left and right)  
  
  db available  :  dw  303   : dw 044+1,127  ;blockwall (16x16) ;cheap hack to fix a bug i cannot resolve. It basicly resides there and fixes the bug for no good reason...
  db available  :  dw  306-1 : dw 044-4,149  ;mediumsheertile (left)
  db available  :  dw  314-1 : dw 040-4,149  ;mediumsheertile (left) 
  
    db available :  dw  319 : dw 005,237  ;mini charging chuck
  
  db available  :  dw  322-1 : dw 036-4,149  ;mediumsheertile (left)

  db available  :  dw  323 : dw 009-1,171  ;mediumsheertile (right)

    db available :  dw  334 : dw 011-2,169  ;cavekoopa

    db available :  dw  337 : dw 027,237  ;mini charging chuck
    
  db force18   :  dw  353-2 : dw 004-2,004  ;checkpoint top
  db available :  dw  353+2-2 : dw 004+3-2,005  ;checkpoint bar
  db force04   :  dw  353+5-2 : dw 004+2-2,031  ;checkpoint pole right    

    db available :  dw  360 : dw 007,237  ;mini charging chuck
    
      db available :  dw  366 : dw 006,196  ;changing powerup      

    db available :  dw  368 : dw 010-2,169  ;cavekoopa

  db available :  dw  380 : dw 019,055  ;yoshicoin

    db available :  dw  383 : dw 045-2,169  ;cavekoopa

  db available  :  dw  391 : dw 025,076  ;p-box (blue)

    db available :  dw  394 : dw 045-2,169  ;cavekoopa

  db available  :  dw  411 : dw 031,028  ;questionmark animation red flower 

    db available :  dw  415 : dw 034,237  ;mini charging chuck

    db available :  dw  423 : dw 045-2,169  ;cavekoopa

  db force00   :  dw  431+1 : dw 041-1,036  ;tube cover up exit sprite

    db available :  dw  442 : dw 044-2,169  ;cavekoopa

  db available :  dw  445 : dw 030,055  ;yoshicoin

    db available :  dw  454 : dw 032-2,169  ;cavekoopa
  
  db available  :  dw  458 : dw 035-1,171  ;mediumsheertile (right)

    db available :  dw  484 : dw 032,170  ;bat (flying turd)

    db available :  dw  486 : dw 040,237  ;mini charging chuck
 ;   db available |  dw  486 | dw 041,238  ;mini charging chuck ;debug chuck
    db available :  dw  491 : dw 032,170  ;bat (flying turd)    
    db available :  dw  494 : dw 045-2,169  ;cavekoopa
    db available :  dw  495 : dw 032,170  ;bat (flying turd)    
    db available :  dw  504 : dw 032,170  ;bat (flying turd)    
    db available :  dw  511 : dw 032,170  ;bat (flying turd)    
    db available :  dw  519 : dw 032,170  ;bat (flying turd)    
    db available :  dw  523 : dw 032,170  ;bat (flying turd)    
    db available :  dw  530 : dw 032,170  ;bat (flying turd)    

    db available  :  dw  534 : dw 039,119  ;multicoin box
 
    db available :  dw  542 : dw 037,238  ;replicating chuck 
 
  db available   :  dw  553 : dw 039,174  ;spiketop 



  db force00   :  dw  576 : dw 029-3+4,007  ;endflag bar
  db available :  dw  576+1 : dw 029-3+1+1,006  ;endflag top
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv033: equ $-objectlistlv033

