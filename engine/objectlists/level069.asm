objectlistlv069:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist

  
    db available	:	dw 032	:	dw 043,318	;Megamole
	db available	:	dw 061	:	dw 033,237	;mini chargin chuck
	db available	:	dw 115	:	dw 024,318	;Megamole
	db available	:	dw 130	:	dw 037,055	;yoshi coin
	db available	:	dw 134	:	dw 014,318	;Megamole
	db available	:	dw 166	:	dw 007,318	;Megamole
	db available	:	dw 169	:	dw 007,055	;yoshi coin
	db available	:	dw 170	:	dw 017,318	;Megamole
	db available	:	dw 208	:	dw 040,237	;mini chargin chuck
	db available	:	dw 216	:	dw 042,237	;mini chargin chuck
	db available	:	dw 247	:	dw 040,032	;yellow !
	db available	:	dw 258	:	dw 045,318	;Megamole
	db available	:	dw 264	:	dw 017,318	;Megamole
	db available	:	dw 275	:	dw 035,318	;Megamole
	db available	:	dw 313	:	dw 017,209	;muncher
	db available	:	dw 319	:	dw 017,209	;muncher
	db available	:	dw 325	:	dw 047,237	;mini chargin chuck
	db available	:	dw 326	:	dw 017,209	;muncher
	db available	:	dw 333	:	dw 010,318	;Megamole
	db available	:	dw 356	:	dw 033,318	;Megamole
	db available	:	dw 364	:	dw 009,024	;moonpowerup
	db available	:	dw 405	:	dw 028,318	;Megamole
	db available	:	dw 427	:	dw 033,241	;secret box (coin)
	db available	:	dw 428	:	dw 021,032	;yellow !
	db available	:	dw 429	:	dw 033,225	;secret box (1up)
	db available	:	dw 431	:	dw 033,241	;secret box (coin)
	db available	:	dw 435	:	dw 009,237	;mini chargin chuck
	db available	:	dw 436	:	dw 018,318	;Megamole
	db available	:	dw 439	:	dw 009,055	;yoshi coin
	db available	:	dw 450	:	dw 038,055	;yoshi coin
	db available	:	dw 468	:	dw 026,237	;mini chargin chuck
	db available	:	dw 483	:	dw 038,074	;Smileybox (climbing plant)
	db available	:	dw 500	:	dw 043,318	;Megamole
	db available	:	dw 513	:	dw 004,036	;Mario exit (up down)
	db available	:	dw 519	:	dw 019,318	;Megamole
	db available	:	dw 530	:	dw 045,237	;mini chargin chuck
	db available	:	dw 540	:	dw 043,036	;Mario exit (up down)
	db available	:	dw 550	:	dw 018,237	;mini chargin chuck
	db available	:	dw 571	:	dw 042,055	;yoshi coin




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv069: equ $-objectlistlv069



level69bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 22371+bgrmapaddr :   db      005, 069, 005, 069 ;calculate adress = (y * mapwidth) + x
    dw 23173+bgrmapaddr :   db      000, 000, 009, 000 ;calculate adress = (y * mapwidth) + x
    dw 23175+bgrmapaddr :   db      041, 069, 041, 069 ;calculate adress = (y * mapwidth) + x
    dw 000             :   db      000, 000, 000, 000
lenghtlevel69bkgtiles: equ $-level69bkgtiles 
