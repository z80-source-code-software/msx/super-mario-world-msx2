objectlistlv006:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  

 ; db available  |  dw  001 | dw 068+1,104  ;water (mario swimming)
 ; db force00    |  dw  002 | dw 068,105  ;water coverupsprite
  
  
  db available  :  dw  008 : dw 068,106  ;fish (jumping up from water)
  db available  :  dw  016 : dw 068,107  ;fish (jumping up from water 2 tiles lower)
  db available  :  dw  024 : dw 068,106  ;fish (jumping up from water)

  db available  :  dw  026 : dw 048+3,032  ;exclamationmark (yellow)

 ;   db available |  dw  033 | dw 068,113  ;waterplatform
  db available :  dw  035 : dw 057,055  ;yoshicoin

    db available :  dw  056 : dw 068,113  ;waterplatform

  db available :  dw  057 : dw 060,055  ;yoshicoin

  db available  :  dw  062 : dw 056,028  ;questionmark animation red flower
  
 ;     db available |  dw  076 | dw 068,113  ;waterplatform
      db available :  dw  084 : dw 068,113  ;waterplatform
  ;    db available |  dw  090 | dw 068,113  ;waterplatform
  
  db available  :  dw  097-1 : dw 068,108  ;blockwall for mario (right)
  
      db force00   :  dw  098+1 : dw 064-1,036  ;tube cover up exit sprite
  
  db available  :  dw  100 : dw 068,109  ;blockwall for mario (left)
  
    db available :  dw  117 : dw 067,112  ;giant watermine
  
        db available :  dw  127 : dw 068,113  ;waterplatform
  
      db available :  dw  136 : dw 067,112  ;giant watermine
  
  db available  :  dw  142 : dw 043,076  ;p-box (blue)

  db available :  dw  148 : dw 060,001  ;pink shell
  
  db available  :  dw  156 : dw 044,110  ;brown box destroyer


  db available :  dw  159 : dw 040,060  ;brown/pink coopa
      db available  :  dw  161-1 : dw 068,108  ;blockwall for mario (right)
;  db available |  dw  161 | dw 058,078  ;coin (static)

  db available :  dw  163 : dw 058,102  ;ultracoin (static)
    db available  :  dw  164 : dw 068,109  ;blockwall for mario (left)
  db available :  dw  172 : dw 040,060  ;brown/pink coopa
  
    db available  :  dw  176 : dw 069,120  ;fish (dumb)
    db available :  dw  177 : dw 053,055  ;yoshicoin
  
  db available :  dw  183 : dw 058,083  ;supercoin (static)
  db available :  dw  184 : dw 040,060  ;brown/pink coopa

          db available :  dw  221 : dw 068,113  ;waterplatform
      db available :  dw  224 : dw 067,112  ;giant watermine
      db available :  dw  233 : dw 067,112  ;giant watermine
          db available :  dw  239 : dw 026,055  ;yoshicoin
      
      db available :  dw  263 : dw 067,112  ;giant watermine

          db available :  dw  269 : dw 068,113  ;waterplatform

      db available :  dw  277 : dw 067,112  ;giant watermine
      db available :  dw  291 : dw 067,112  ;giant watermine

  db available :  dw  297 : dw 054,055  ;yoshicoin

      db available :  dw  311 : dw 067,112  ;giant watermine

          db available :  dw  318 : dw 068,113  ;waterplatform

      db available :  dw  324 : dw 067,112  ;giant watermine

  db available  :  dw  340 : dw 068,121  ;fish (ADHD)

  db available :  dw  343-1 : dw 061-1,054  ;tubeexit to the right 


  db available  :  dw  353 : dw 068,111  ;blockwallmap


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv006: equ $-objectlistlv006


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    341*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!

  
  