objectlistlv028c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db force25  :  dw  004 : dw 028,129  ;door 

  db available  :  dw  011 : dw 016,185  ;small ghost (following mario)
  db available  :  dw  016 : dw 026,185  ;small ghost (following mario)
  db available  :  dw  031 : dw 023,185  ;small ghost (following mario)
  db available  :  dw  039 : dw 017,185  ;small ghost (following mario)


  db available  :  dw  048 : dw 022,183  ;questionmark box (coin tail)
 
  db available  :  dw  059 : dw 019,185  ;small ghost (following mario)  



  db available  :  dw  073 : dw 022,214  ;eerie (left and right)
  db available  :  dw  082 : dw 029,214  ;eerie (left and right)
  db available  :  dw  084 : dw 017,214  ;eerie (left and right)


  
  db force25  :  dw  088 : dw 028,129  ;door  
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv028c: equ $-objectlistlv028c