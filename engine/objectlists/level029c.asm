objectlistlv029c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  017 : dw 012,034  ;exclamationmark  (green)

  db force26 :  dw  027 : dw 014,226  ;huge door (special spawn)

  db available :  dw  019 : dw 016,224  ;dry bones (special spawn)

  db available :  dw  014 : dw 035,034  ;exclamationmark (green)

  db force11 :  dw  009    : dw 73,057  ;questionmark animation (coin)
  db force28 :  dw  016    : dw 73,057  ;questionmark animation (coin)  
  db force29 :  dw  023    : dw 73,057  ;questionmark animation (coin)  

  db available :  dw  025 : dw 077,224  ;dry bones (special spawn)
;  db available |  dw  017 | dw 077,224  ;dry bones (special spawn)  
;  db available |  dw  026 | dw 087,224  ;dry bones (special spawn)

      db available :  dw  007 : dw 099,225  ;secret box (1 up) 

  db available :  dw  004 : dw 103,224  ;dry bones (special spawn)

 
  db available :  dw  018 : dw 133,224  ;dry bones (special spawn)

  db force26 :  dw  010 : dw 137,074  ;smiley box (climbing plant) 

  db available :  dw  015 : dw 167,224  ;dry bones (special spawn) 

    db force30  :  dw  008 : dw 169,205  ;spring  

  db force08 :  dw  020    : dw 207,057  ;questionmark animation (coin)  
  db force09 :  dw  022    : dw 207,029  ;questionmark animation (feather)  
  db force10 :  dw  024    : dw 207,057  ;questionmark animation (coin)  

  db available :  dw  003 : dw 220,223  ;Horizontal object mover (helper object)


 
  db  00        :  dw  999 : dw 999,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv029c: equ $-objectlistlv029c

