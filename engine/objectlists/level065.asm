objectlistlv065:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 004 : dw 043,327	;Special yellow pillar
	db available : dw 014 : dw 047,205	;spring
	db available : dw 016 : dw 051,240	;blargh
	db available : dw 050 : dw 043,149	;mediumsheerile (left)
	db available : dw 057 : dw 039,149	;mediumsheerile (left)
	db available : dw 064 : dw 038,169	;cavekoopa
	db available : dw 069 : dw 038,169	;cavekoopa
	db available : dw 102 : dw 034,175	;football chuck
	db available : dw 104 : dw 047,169	;cavekoopa
	db available : dw 109 : dw 047,169	;cavekoopa
	db available : dw 124 : dw 032,231	;maxsheertile (right) (14 high)
	db available : dw 137 : dw 047,169	;cavekoopa
	db available : dw 151 : dw 037,027	;red shroom box
	db available : dw 172 : dw 031,175	;football chuck
	db available : dw 175 : dw 027,162	;exit (upwards)
	db available : dw 176 : dw 026,127	;blockwall (16x16)
	db available : dw 186 : dw 033,231	;maxsheertile (right) (14 high)
	db available : dw 211 : dw 031,127	;blockwall (16x16)
	db available : dw 225 : dw 030,175	;football chuck
	db available : dw 234 : dw 028,004	;Checkpointtop
	db available : dw 236 : dw 031,005	;Checkpointbar
	db available : dw 239 : dw 030,031	;checkpointright
	db available : dw 247 : dw 045,054	;mario exit (right)





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv065: equ $-objectlistlv065


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    246*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 
