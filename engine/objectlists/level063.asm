objectlistlv063:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  

	db available : dw 024 : dw 038,319	;Spikey (static)
	db available : dw 037 : dw 044,319	;Spikey (static)
	db available : dw 038 : dw 030,319	;Spikey (static)
	db available : dw 048 : dw 006,077	;p-box (grey)
	db available : dw 049 : dw 037,058	;yoshi egg ? box
	db available : dw 055 : dw 045,076	;p-box (blue)
	db available : dw 061 : dw 044,319	;Spikey (static)
	db available : dw 062 : dw 030,319	;Spikey (static)
	db available : dw 075 : dw 040,036	;Mario exit (up down)
	db available : dw 078 : dw 017,055	;yoshi coin
	db available : dw 085 : dw 044,319	;Spikey (static)
	db available : dw 095 : dw 047,083	;supercoin (static)
	db available : dw 099 : dw 039,272	;red mushroom (static)
	db available : dw 105 : dw 047,078	;coin (static)
	db available : dw 111 : dw 044,319	;Spikey (static)
	db available : dw 112 : dw 030,319	;Spikey (static)
	db available : dw 125 : dw 038,319	;Spikey (static)
	db available : dw 141 : dw 045,272	;red mushroom (static)
	db available : dw 151 : dw 039,250	;spikey
	db available : dw 153 : dw 037,055	;yoshi coin
	db available : dw 166 : dw 044,250	;spikey
	db available : dw 189 : dw 034,250	;spikey
	db available : dw 190 : dw 044,250	;spikey
	db available : dw 198 : dw 040,036	;Mario exit (up down)
	db available : dw 238 : dw 012,255	;paraboomb
	db available : dw 247 : dw 038,055	;yoshi coin
	db available : dw 249 : dw 012,197	;paragoomba
	db available : dw 261 : dw 012,197	;paragoomba
	db available : dw 271 : dw 010,255	;paraboomb
	db available : dw 291 : dw 037,085	;green shell
	db available : dw 300 : dw 039,102	;ultracoin (static)
	db available : dw 303 : dw 022,177	;blue shell
	db available : dw 313 : dw 022,001	;Pink Shell
	db available : dw 320 : dw 039,083	;supercoin (static)
	db available : dw 322 : dw 022,190	;yellow shell
	db available : dw 347 : dw 022,004	;Checkpointtop
	db available : dw 349 : dw 025,005	;Checkpointbar
	db available : dw 352 : dw 024,031	;checkpointright
	db available : dw 409 : dw 034,101	;expanding smileybox platform (vertical)
	db available : dw 419 : dw 025,100	;expanding smileybox expander
	db available : dw 421 : dw 029,101	;expanding smileybox platform (vertical)
	db available : dw 432 : dw 036,101	;expanding smileybox platform (vertical)
	db available : dw 501 : dw 045,237	;mini chargin chuck
	db available : dw 510 : dw 045,237	;mini chargin chuck
	db available : dw 521 : dw 043,026	;snapping chuck
	db available : dw 535 : dw 029,007	;Endflagbar
	db available : dw 536 : dw 028,006	;Endflagtop



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv063: equ $-objectlistlv063
