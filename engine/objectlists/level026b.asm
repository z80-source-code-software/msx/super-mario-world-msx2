objectlistlv026b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available   :  dw  009 : dw 006,055  ;yoshi coin 
 
  db available   :  dw  012   : dw 078,204  ;p-balloon
  
  db  available :  dw  021 : dw 122,091  ;red flying koopa  
  db  available :  dw  022 : dw 050,091  ;red flying koopa  
  
  db available   :  dw  031   : dw 148,204  ;p-balloon
  
  db available :  dw  038+1    : dw 016,042    ;smileybox (extra life)  
  
  db  available :  dw  039+1 : dw 086,091  ;red flying koopa  
  
  db available :  dw  053 : dw 078,028  ;questionmark animation red flower  
 
  db available   :  dw  055 : dw 006,055  ;yoshi coin 
  
  db  available :  dw  059 : dw 133,091  ;red flying koopa  
  db  available :  dw  060 : dw 049,091  ;red flying koopa  
  db  available :  dw  062 : dw 095,091  ;red flying koopa  
  
  db available   :  dw  061+2 : dw 151-1,036  ;tube cover up exit sprite
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv026b: equ $-objectlistlv026b


  
 