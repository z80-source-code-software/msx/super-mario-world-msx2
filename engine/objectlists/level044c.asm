objectlistlv044c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db  available :  dw  048 : dw 044,097  ;square flattened platform (vertical)
  db  available :  dw  058 : dw 039,097  ;square flattened platform (vertical)
  db  available :  dw  070 : dw 047,097  ;square flattened platform (vertical)


  db available  :  dw  078 : dw 049,248  ;bill (superspawner)

  db  available :  dw  082 : dw 038,097  ;square flattened platform (vertical)
  db  available :  dw  102 : dw 046,097  ;square flattened platform (vertical)

  db available :  dw  121    : dw 037,162  ;tube cover up exit (upwards)  

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv044c: equ $-objectlistlv044c

