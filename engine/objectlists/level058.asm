objectlistlv058:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
   
  db available  :  dw  005 : dw 001,138  ;parralax scroller
  
	db available	:	dw 016	:	dw 017,102	;ultracoin (static)
	db available	:	dw 028	:	dw 017,102	;ultracoin (static)
	db available	:	dw 033	:	dw 022,062	;yellow koopa
	db available	:	dw 036	:	dw 017,102	;ultracoin (static)
	db available	:	dw 044	:	dw 017,102	;ultracoin (static)
	db available	:	dw 057	:	dw 022,064	;blue koopa
	db available	:	dw 060	:	dw 016,102	;ultracoin (static)
	db available	:	dw 076	:	dw 016,102	;ultracoin (static)
	db available	:	dw 081	:	dw 019,102	;ultracoin (static)





  db force00    :  dw  086 : dw 020,054  ;tubeexit to the right 


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv058: equ $-objectlistlv058
  

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    85*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
 
