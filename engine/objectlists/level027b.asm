objectlistlv027b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available   :  dw  012   : dw 038,204  ;p-balloon
  
  db available :  dw  039 : dw 34,078  ;coin (static)     
  
  db available   :  dw  041   : dw 047,210  ;falling platform
  
  db available :  dw  043 : dw 34,078  ;coin (static)   

  db available   :  dw  054 : dw 045,008  ;Jumpin Piranha Plant 

; db available |  dw  064 | dw 31,078  ;coin (static) 

  db available   :  dw  066   : dw 045,210  ;falling platform  
  
;  db available |  dw  069 | dw 31,078  ;coin (static) 
  
  
  db available   :  dw  078 : dw 044,008  ;Jumpin Piranha Plant 
  
  db available   :  dw  092   : dw 046,210  ;falling platform   
  
;  db available |  dw  094 | dw 33,078  ;coin (static) 
  db available :  dw  100 : dw 33,078  ;coin (static) 
  
  

  db available   :  dw  117+1 : dw 041-1,036  ;tube cover up exit sprite


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv027b: equ $-objectlistlv027b


  
 