objectlistlv060d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 004 : dw 039,167	;Diagonal block (helper object)
	db available : dw 011 : dw 049,164	;feather bonus (static)
	db available : dw 015 : dw 049,165	;diagonal block (left)
	db available : dw 029 : dw 009,078	;coin (static)
	db available : dw 031 : dw 009,078	;coin (static)
	db available : dw 033 : dw 009,078	;coin (static)
	db available : dw 041 : dw 011,078	;coin (static)
	db available : dw 043 : dw 011,078	;coin (static)
	db available : dw 045 : dw 011,078	;coin (static)
	db available : dw 055 : dw 013,078	;coin (static)
	db available : dw 057 : dw 013,078	;coin (static)
	db available : dw 059 : dw 013,078	;coin (static)
	db available : dw 078 : dw 012,055	;yoshi coin
	db available : dw 095 : dw 015,078	;coin (static)
	db available : dw 097 : dw 015,078	;coin (static)
	db available : dw 099 : dw 015,078	;coin (static)
	db available : dw 120 : dw 018,078	;coin (static)
	db available : dw 122 : dw 018,078	;coin (static)
	db available : dw 124 : dw 018,078	;coin (static)
	db available : dw 145 : dw 021,078	;coin (static)
	db available : dw 147 : dw 021,078	;coin (static)
	db available : dw 149 : dw 021,078	;coin (static)
	db available : dw 176 : dw 024,078	;coin (static)
	db available : dw 178 : dw 024,078	;coin (static)
	db available : dw 180 : dw 024,078	;coin (static)
	db available : dw 217 : dw 044,036	;Mario exit (up down)


  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060d: equ $-objectlistlv060d
