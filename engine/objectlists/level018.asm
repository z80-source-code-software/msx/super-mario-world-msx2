objectlistlv018:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db force04  :  dw  007 : dw 006,147  ;mario 
  
  db force18  :  dw  008 : dw 008,148  ;world
  
  
  db force00  :  dw  013 : dw 003,146  ;super


  db available :  dw  017 : dw 015,061  ;pink coopa
 
 
 
    db available :  dw  020 : dw 018,043  ;powerpopup (red mushroom)
  
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv018: equ $-objectlistlv018
  
 