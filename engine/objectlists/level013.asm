objectlistlv013:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
   
  db available  :  dw  005 : dw 001,138  ;parralax scroller
  
  db available  :  dw  010 : dw 015,139  ;coin special spawn
  db available  :  dw  011 : dw 010,139  ;coin special spawn
  
    db available  :  dw  020 : dw 022,076  ;p-box (blue-static)
  
  db available  :  dw  040 : dw 010,139  ;coin special spawn
  db available  :  dw  041 : dw 015,139  ;coin special spawn
  db available  :  dw  050 : dw 010,139  ;coin special spawn
  db available  :  dw  051 : dw 015,139  ;coin special spawn
  db available  :  dw  060 : dw 010,139  ;coin special spawn
  db available  :  dw  061 : dw 015,139  ;coin special spawn
  db available  :  dw  070 : dw 010,139  ;coin special spawn
  db available  :  dw  071 : dw 015,139  ;coin special spawn
  db available  :  dw  080 : dw 010,139  ;coin special spawn
  db available  :  dw  081 : dw 015,139  ;coin special spawn
    
  db force00    :  dw  086 : dw 020,054  ;tubeexit to the right 


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv013: equ $-objectlistlv013
  

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    85*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
 