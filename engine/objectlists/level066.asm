objectlistlv066:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


	db available : dw 007 : dw 015,327	;Special yellow pillar
	db available : dw 037 : dw 022,330	;Wooden pillar (down up)
	db available : dw 038 : dw 010,331	;Wooden pillar (up down)
	db available : dw 048 : dw 022,222	;dry bones
	db available : dw 057 : dw 022,330	;Wooden pillar (down up)
	db available : dw 061 : dw 019,329	;Castleflame
	db available : dw 063 : dw 026,033	;red !
	db available : dw 065 : dw 026,033	;red !
	db available : dw 067 : dw 026,033	;red !
	db available : dw 069 : dw 026,033	;red !
	db available : dw 071 : dw 026,033	;red !
	db available : dw 080 : dw 010,331	;Wooden pillar (up down)
	db available : dw 099 : dw 007,331	;Wooden pillar (up down)
	db available : dw 101 : dw 018,329	;Castleflame
	db available : dw 109 : dw 018,029	;feather box
	db available : dw 112 : dw 021,222	;dry bones
	db available : dw 138 : dw 018,330	;Wooden pillar (down up)
	db available : dw 151 : dw 022,329	;Castleflame
	db available : dw 166 : dw 011,331	;Wooden pillar (up down)
	db available : dw 177 : dw 016,329	;Castleflame
	db available : dw 185 : dw 021,222	;dry bones
	db available : dw 193 : dw 024,330	;Wooden pillar (down up)
	db available : dw 200 : dw 023,329	;Castleflame
	db available : dw 206 : dw 022,330	;Wooden pillar (down up)
	db available : dw 207 : dw 011,331	;Wooden pillar (up down)
	db available : dw 218 : dw 016,329	;Castleflame
	db available : dw 233 : dw 024,329	;Castleflame
	db available : dw 242 : dw 016,004	;Checkpointtop
	db available : dw 244 : dw 019,005	;Checkpointbar
	db available : dw 247 : dw 018,031	;checkpointright
	db available : dw 256 : dw 022,129	;door




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv066: equ $-objectlistlv066
