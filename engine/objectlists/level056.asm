objectlistlv056:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
   
  db available  :  dw  005 : dw 001,138  ;parralax scroller
  
	db available : dw 002 : dw 012,102	;ultracoin (static)
	db available : dw 010 : dw 006,250	;spikey
	db available : dw 022 : dw 012,102	;ultracoin (static)
	db available : dw 024 : dw 024,076	;p-box (blue)
	db available : dw 030 : dw 006,250	;spikey
	db available : dw 032 : dw 024,077	;p-box (grey)
	db available : dw 042 : dw 012,102	;ultracoin (static)
	db available : dw 048 : dw 006,250	;spikey
	db available : dw 062 : dw 012,102	;ultracoin (static)
	db available : dw 072 : dw 006,250	;spikey
	db available : dw 082 : dw 012,083	;supercoin (static)
	db available : dw 086 : dw 020,054	;mario exit (right)

    
;  db force00    |  dw  086 | dw 020,054  ;tubeexit to the right 


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv056: equ $-objectlistlv056
  

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    85*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
 
