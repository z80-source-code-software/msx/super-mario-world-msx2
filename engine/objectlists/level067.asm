objectlistlv067:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
	db available : dw 006 : dw 024,332	;Wendy superspike
	db available : dw 041 : dw 029,205	;spring
	db available : dw 043 : dw 029,205	;spring
	db available : dw 090 : dw 025,308	;Circlesaw
	db available : dw 116 : dw 025,308	;Circlesaw
	db available : dw 135 : dw 030,128	;flame
	db available : dw 140 : dw 021,033	;red !
	db available : dw 142 : dw 021,033	;red !
	db available : dw 149 : dw 030,128	;flame
	db available : dw 176 : dw 022,308	;Circlesaw
	db available : dw 210 : dw 025,308	;Circlesaw
	db available : dw 228 : dw 020,308	;Circlesaw
	db available : dw 268 : dw 018,308	;Circlesaw
	db available : dw 278 : dw 012,308	;Circlesaw
	db available : dw 366 : dw 019,004	;Checkpointtop
	db available : dw 368 : dw 022,005	;Checkpointbar
	db available : dw 371 : dw 021,031	;checkpointright
	db available : dw 376 : dw 025,129	;door



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv067: equ $-objectlistlv067
