objectlistlv037b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
   db available :  dw  043    : dw 031,175    ;football chuck 
 
 
  db force00   :  dw  058 : dw 030-3+4,007  ;endflag bar
  db available :  dw  058+1 : dw 030-3+1+1,006  ;endflag top  
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv037b: equ $-objectlistlv037b

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    010*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!