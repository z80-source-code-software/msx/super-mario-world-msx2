objectlistlv039:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  001 : dw 048+1,104  ;water (mario swimming)
  db force00    :  dw  002 : dw 048,105  ;water coverupsprite
  

  db available    :  dw  035+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  045+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  055+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  065+10 : dw 048,258  ;dolphin (left right)
  
          db available :  dw  078 : dw 026,029  ;questionmark animation (feather)

  db available   :  dw  091 : dw 026,055  ;yoshi coin

  db available    :  dw  100+10 : dw 048,259  ;dolphin (left only)
  db available    :  dw  110+10 : dw 048,259  ;dolphin (left only)
  db available    :  dw  120+10 : dw 048,259  ;dolphin (left only)
  db available    :  dw  140+10 : dw 048,259  ;dolphin (left only)
  db available    :  dw  150+10 : dw 048,259  ;dolphin (left only)
  db available    :  dw  160+10 : dw 048,259  ;dolphin (left only)

  db available   :  dw  174 : dw 035,055  ;yoshi coin

  db available    :  dw  198+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  205+10 : dw 048,258  ;dolphin (left right)
  
  db available    :  dw  219 : dw 046,257  ;porcupuffer  
  
          db available :  dw  220 : dw 028,029  ;questionmark animation (feather)  
  
  db available    :  dw  215+10 : dw 048-3,261  ;dolphin (up down)

  db available    :  dw  230+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  245+10 : dw 048,258  ;dolphin (left right)
  
  
  db available    :  dw  275+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  293+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  311+10 : dw 048,258  ;dolphin (left right)
  db available    :  dw  327+10 : dw 048,258  ;dolphin (left right)
  
  db available    :  dw  340 : dw 048-3,261  ;dolphin (up down)  
  db available    :  dw  348 : dw 048-3,261  ;dolphin (up down)  

  db available   :  dw  357 : dw 026,055  ;yoshi coin

  db available    :  dw  360 : dw 048,260  ;dolphin (right only)
  db available    :  dw  365 : dw 048,260  ;dolphin (right only)
;  db available    |  dw  375 | dw 048,260  ;dolphin (right only)
 ; db available    |  dw  380 | dw 048,260  ;dolphin (right only)
  db available    :  dw  390 : dw 048,260  ;dolphin (right only)
;  db available    |  dw  410 | dw 048,260  ;dolphin (right only)
  db available    :  dw  415 : dw 048,260  ;dolphin (right only)
  db available    :  dw  435 : dw 048,260  ;dolphin (right only)
;  db available    |  dw  445 | dw 048,260  ;dolphin (right only)
  db available    :  dw  455 : dw 048,260  ;dolphin (right only)
;  db available    |  dw  475 | dw 048,260  ;dolphin (right only)


  db available   :  dw  487 : dw 035,055  ;yoshi coin

  db force00   :  dw  500-1 : dw 040,054  ;tubeexit to the right 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv039: equ $-objectlistlv039

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    498*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!


