objectlistlv028b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db force25  :  dw  004 : dw 044,129  ;door 

   db available  :  dw  014 : dw 036,076  ;p-box 
 
  db available  :  dw  030 : dw 034,185  ;small ghost (following mario) 
  
  db available  :  dw  058 : dw 040,215  ;questionmark animation (pbox)
;  db available  |  dw  060 | dw 040,215  ;questionmark animation (pbox)

  db available :  dw  060 : dw 032,074  ;smiley box (climbing plant)  
  
  db force25  :  dw  061 : dw 036,129  ;door  
  
;  db available  |  dw  062 | dw 040,215  ;questionmark animation (pbox)  
  
  db available  :  dw  075 : dw 034,185  ;small ghost (following mario) 
 
   db force22  :  dw  080+1 : dw 039-1,181  ;door (only appearing with pbox)  

  db available  :  dw  094+1 : dw 034-2,213  ;one way door

  db available  :  dw  099 : dw 030,211  ;small ghostring

   db available  :  dw  100 : dw 040,119  ;multicoin box

   db force22  :  dw  101 : dw 014,181  ;door (only appearing with pbox)  

  db available  :  dw  120 : dw 034,185  ;small ghost (following mario)

  db available  :  dw  142 : dw 038,028  ;questionmark animation (red flower)

 

 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv028b: equ $-objectlistlv028b


  
level28bbkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 5210+bgrmapaddr :   db      059, 092, 058, 060 ;calculate adress = (y * mapwidth) + x
    dw 5212+bgrmapaddr :   db      093, 062, 061, 062 ;calculate adress = (y * mapwidth) + x
    dw 5530+bgrmapaddr :   db      080, 081, 081, 049 ;calculate adress = (y * mapwidth) + x
    dw 5538+bgrmapaddr :   db      059, 092, 027, 028 ;calculate adress = (y * mapwidth) + x
    dw 6494+bgrmapaddr :   db      114, 115, 146, 147 ;calculate adress = (y * mapwidth) + x
    dw 6496+bgrmapaddr :   db      114, 115, 146, 147 ;calculate adress = (y * mapwidth) + x
    dw 6504+bgrmapaddr :   db      114, 115, 146, 147 ;calculate adress = (y * mapwidth) + x
    dw 6506+bgrmapaddr :   db      114, 115, 146, 147 ;calculate adress = (y * mapwidth) + x
    dw 6260+bgrmapaddr :   db      128, 128, 160, 160 ;calculate adress = (y * mapwidth) + x
    dw 6540+bgrmapaddr :   db      114, 115, 146, 147 ;calculate adress = (y * mapwidth) + x
    dw 000             :   db      000, 000, 000, 000
lenghtlevel28bbkgtiles: equ $-level28bbkgtiles  