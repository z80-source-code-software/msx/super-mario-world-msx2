objectlistlv078g:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


  
        db available    :       dw 026  :       dw 037,334      ;wendyfireball
        db available    :       dw 038  :       dw 026,289      ;greenmushroom (static)
        db available    :       dw 046  :       dw 045,210      ;falling platform
        db available    :       dw 061  :       dw 035,334      ;wendyfireball
        db available    :       dw 071  :       dw 032,334      ;wendyfireball
        db available    :       dw 090  :       dw 032,335      ;wendyfireballxl

  
  db force24	:	dw 124	:	dw 043,129	;door




  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078g: equ $-objectlistlv078g
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
