objectliststarworld3:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 

	db available	:	dw 011	:	dw 030,304	;trowing lakitu
	db available	:	dw 014	:	dw 047,087	;blue rotating block
	db available	:	dw 016	:	dw 006,027	;red shroom box
	db available	:	dw 018	:	dw 047,087	;blue rotating block
	db available	:	dw 020	:	dw 047,087	;blue rotating block
	db available	:	dw 024	:	dw 046,348	;Permayoshi
	db available	:	dw 029	:	dw 046,077	;p-box (grey)
	db available	:	dw 034	:	dw 047,087	;blue rotating block
	db available	:	dw 036	:	dw 047,087	;blue rotating block
	db available	:	dw 038	:	dw 047,087	;blue rotating block
	db available	:	dw 047	:	dw 010,178	;keyhole
	db available	:	dw 059	:	dw 031,007	;Endflagbar
	db available	:	dw 060	:	dw 030,006	;Endflagtop




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectliststarworld3: equ $-objectliststarworld3


