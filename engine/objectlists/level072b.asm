objectlistlv072b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 003	:	dw 026,162	;exit (upwards)
	db available	:	dw 049	:	dw 021,174	;spiketop
	db available	:	dw 053	:	dw 036,120	;fish (dumb)
	db available	:	dw 064	:	dw 026,174	;spiketop
	db available	:	dw 068	:	dw 036,120	;fish (dumb)
	db available	:	dw 080	:	dw 028,058	;yoshi egg ? box
	db available	:	dw 088	:	dw 036,120	;fish (dumb)
	db available	:	dw 091	:	dw 022,174	;spiketop
	db available	:	dw 099	:	dw 036,120	;fish (dumb)
	db available	:	dw 104	:	dw 027,174	;spiketop
	db available	:	dw 111	:	dw 037,104	;water (marioswim)
	db available	:	dw 116	:	dw 030,208	;Ice



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv072b: equ $-objectlistlv072b

