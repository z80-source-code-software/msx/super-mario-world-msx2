objectlistlv001:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     

  db force18   :  dw  045 : dw 025,055  ;yoshi coin
  db force00   :  dw  056 : dw 025,054  ;tubeexit to the right 

  db  00       :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv001: equ $-objectlistlv001
  

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    055*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!  