objectlistlv062b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 006 : dw 026,316	;Special exit (mapleve6-04b)
	db available : dw 008 : dw 252,205	;spring
	db available : dw 012 : dw 018,076	;p-box (blue)
	db available : dw 037 : dw 062,164	;feather bonus (static)
	db available : dw 039 : dw 158,317	;Flower (static)
	db available : dw 042 : dw 126,289	;greenmushroom (static)
	db available : dw 044 : dw 030,102	;ultracoin (static)
	db available : dw 045 : dw 190,272	;red mushroom (static)
	db available : dw 046 : dw 030,102	;ultracoin (static)
	db available : dw 047 : dw 222,289	;greenmushroom (static)
	db available : dw 048 : dw 030,102	;ultracoin (static)
	db available : dw 049 : dw 222,289	;greenmushroom (static)
	db available : dw 051 : dw 222,289	;greenmushroom (static)
	db available : dw 054 : dw 092,055	;yoshi coin

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv062b: equ $-objectlistlv062b

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    56*8      :     db 48 ;-2
dw    000         ;defines the end of the list COMPULSORY!!
