objectlistlv050b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

   db available :  dw  018 : dw 024,178  ;keyhole

  db force00   :  dw  026-1 : dw 016,054  ;tubeexit to the right 
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv050b: equ $-objectlistlv050b


level50bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4
  
    dw 654+bgrmapaddr :   db      068, 019, 068, 051 ;calculate adress = (y * mapwidth) + x
    dw 656+bgrmapaddr :   db      020, 005, 016, 005 ;calculate adress = (y * mapwidth) + x
    dw 658+bgrmapaddr :   db      005, 005, 005, 005 ;calculate adress = (y * mapwidth) + x
    dw 662+bgrmapaddr :   db      015, 050, 015, 050 ;calculate adress = (y * mapwidth) + x
    dw 000            :   db      000, 000, 000, 000
lenghtlevel50bkgtiles: equ $-level50bkgtiles


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    024*8      :     db 48
dw    000         ;defines the end of the list COMPULSORY!!  


;BUG: SOME WIERD SHIT IS GOING ON in here. BLocks are magically turned into blue blocks!
