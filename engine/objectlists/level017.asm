objectlistlv017:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  008 : dw 010,163  ;tic tac toe
  db available :  dw  008 : dw 018,163  ;tic tac toe
  db available :  dw  008 : dw 026,163  ;tic tac toe
  db available :  dw  008 : dw 034,163  ;tic tac toe
  db available :  dw  008 : dw 042,163  ;tic tac toe


  
  db  available :  dw  024-1 : dw 008-2,054  ;tubeexit to the right 

  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv017: equ $-objectlistlv017
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    001      :     db 32-2
dw    022*8    :     db 48
dw    000         ;defines the end of the list COMPULSORY!! 

 