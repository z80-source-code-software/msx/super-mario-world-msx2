objectlistlv041d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
  db available  :  dw  012 : dw 010,273  ;moving flame (bouncing)
   
  db available  :  dw  018 : dw 014,274  ;lemmy (boss) 

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv041d: equ $-objectlistlv041d

