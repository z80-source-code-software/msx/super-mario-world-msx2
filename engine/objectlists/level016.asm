objectlistlv016:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  005 : dw 049,167  ;diagonal block (helper)


  db available :  dw  011 : dw 049,164  ;feather
  
  db available :  dw  024 : dw 049,165  ;diagonal block (left) 
  db available :  dw  050 : dw 049,166  ;diagonal block (right)

  db available :  dw  060 : dw 018,102  ;ultracoin (static)


  db available :  dw  075 : dw 049,078  ;coin (static)

  db available :  dw  085 : dw 018+6,102  ;ultracoin (static)

  db available :  dw  100 : dw 049,078  ;coin (static)
  db available :  dw  100+2 : dw 049,078  ;coin (static)

  db available :  dw  115 : dw 018-6,102  ;ultracoin (static)

  db available :  dw  125 : dw 049,078  ;coin (static)
  db available :  dw  125+2 : dw 049,078  ;coin (static)
  db available :  dw  125+2+2 : dw 049,078  ;coin (static)


  db available :  dw  135 : dw 018,102  ;ultracoin (static)

  db available :  dw  150 : dw 049,078  ;coin (static)
  db available :  dw  150+2 : dw 049-2,078  ;coin (static)
  db available :  dw  150+2+2 : dw 049,078  ;coin (static)
  db available :  dw  150+2+2+2 : dw 049-2,078  ;coin (static)

  db available :  dw  160 : dw 018+6,102  ;ultracoin (static)

  db available :  dw  175 : dw 049,078  ;coin (static)
  db available :  dw  175+2 : dw 049-2,078  ;coin (static)
  db available :  dw  175+2+2 : dw 049,078  ;coin (static)
  db available :  dw  175+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  175+2+2+2+2 : dw 049,078  ;coin (static)

  db available :  dw  190 : dw 018,102  ;ultracoin (static)


  db available :  dw  200 : dw 049,078  ;coin (static)
  db available :  dw  200+2 : dw 049-2,078  ;coin (static)
  db available :  dw  200+2+2 : dw 049,078  ;coin (static)
  db available :  dw  200+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  200+2+2+2+2 : dw 049,078  ;coin (static)
  db available :  dw  200+2+2+2+2+2 : dw 049-2,078  ;coin (static)


  db available :  dw  220 : dw 018-6,102  ;ultracoin (static)

  db available :  dw  225 : dw 049,078  ;coin (static)
  db available :  dw  225+2 : dw 049-2,078  ;coin (static)
  db available :  dw  225+2+1 : dw 049-2-2,078  ;coin (static)
  db available :  dw  225+2+2 : dw 049,078  ;coin (static)
  db available :  dw  225+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  225+2+2+2+2 : dw 049,078  ;coin (static)
  db available :  dw  225+2+2+2+2+2 : dw 049-2,078  ;coin (static)

  db available :  dw  240 : dw 018+6,102  ;ultracoin (static)


  db available :  dw  250 : dw 049,078  ;coin (static)
  db available :  dw  250+2 : dw 049-2,078  ;coin (static)
  db available :  dw  250+2+1 : dw 049-2-2,078  ;coin (static)
  db available :  dw  250+2+2 : dw 049,078  ;coin (static)
  db available :  dw  250+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  250+2+2+2+1 : dw 049-2-2,078  ;coin (static)
  db available :  dw  250+2+2+2+2 : dw 049,078  ;coin (static)
  db available :  dw  250+2+2+2+2+2 : dw 049-2,078  ;coin (static)

  db available :  dw  300 : dw 018,102  ;ultracoin (static)

  db available :  dw  350 : dw 049,078  ;coin (static)
  db available :  dw  350+2 : dw 049-2,078  ;coin (static)
  db available :  dw  350+2+1 : dw 049-2-2,078  ;coin (static)
  db available :  dw  350+2+2 : dw 049,078  ;coin (static)
  db available :  dw  350+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  350+2+2+2+1 : dw 049-2-2,078  ;coin (static)
  db available :  dw  350+2+2+2+2 : dw 049,078  ;coin (static)
  db available :  dw  350+2+2+2+2+2 : dw 049-2,078  ;coin (static)
  db available :  dw  350+2+2+2+2+2+1 : dw 049-2-2,078  ;coin (static)

  db available :  dw  380 : dw 018-6,102  ;ultracoin (static)
 
  db available :  dw  390 : dw 049,165  ;diagonal block (left) 
  db available :  dw  416 : dw 049,166  ;diagonal block (right) 
 
  db available :  dw  443+1 : dw 044-2,036  ;tube cover up exit sprite 
 
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv016: equ $-objectlistlv016
  
 