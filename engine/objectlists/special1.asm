objectlistspecial1:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

        db available    :       dw 022  :       dw 044,057      ;coin ? box
        db available    :       dw 024  :       dw 044,057      ;coin ? box
        db available    :       dw 028  :       dw 038,057      ;coin ? box
        db available    :       dw 030  :       dw 038,029      ;feather box
        db available    :       dw 034  :       dw 050,191      ;goomba
        db available    :       dw 052  :       dw 040,057      ;coin ? box
        db available    :       dw 058  :       dw 050,191      ;goomba
        db available    :       dw 072  :       dw 044,061      ;pink koopa
        db available    :       dw 083  :       dw 048,029      ;feather box
        db available    :       dw 089  :       dw 044,057      ;coin ? box
        db available    :       dw 107  :       dw 048,191      ;goomba
        db available    :       dw 117  :       dw 048,191      ;goomba
        db available    :       dw 128  :       dw 048,195      ;goomba (winged)
        db available    :       dw 171  :       dw 035,093      ;green flying koopa
        db available    :       dw 177  :       dw 035,093      ;green flying koopa
        db available    :       dw 180  :       dw 048,063      ;green koopa
        db available    :       dw 185  :       dw 046,029      ;feather box
        db available    :       dw 196  :       dw 020,289      ;greenmushroom (static)
        db available    :       dw 200  :       dw 020,289      ;greenmushroom (static)
        db available    :       dw 204  :       dw 020,289      ;greenmushroom (static)
        db available    :       dw 234  :       dw 046,290      ;piranjaplant (fire version)
        db available    :       dw 273  :       dw 048,061      ;pink koopa
        db available    :       dw 285  :       dw 012,036      ;Mario exit (up down)
        db available    :       dw 334  :       dw 045,371      ;CHanging powerup box
        
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial1: equ $-objectlistspecial1
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
