objectlistlv030:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

   db available  :  dw  005 : dw 001,138  ;parralax scroller

    db available :  dw  012 : dw 008,001  ;pink shell
   db available  :  dw  016 : dw 016,069  ;blue naked koopa


   db available  :  dw  020 : dw 008,076  ;p-box 
  
 
  db available :  dw  28 : dw 022,061  ;pink coopa 
  db available :  dw  44 : dw 022,061  ;pink coopa 
  db available :  dw  66 : dw 022,061  ;pink coopa 
  db available :  dw  83 : dw 022,061  ;pink coopa 
 
  db available :  dw  090-1 : dw 006,054  ;tubeexit to the right ;force06 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv030: equ $-objectlistlv030


level30bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 982+bgrmapaddr  :   db      000, 001, 002, 003 ;calculate adress = (y * mapwidth) + x
    dw 000             :   db      000, 000, 000, 000
lenghtlevel30bkgtiles: equ $-level30bkgtiles 

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    088*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!   

  
  
