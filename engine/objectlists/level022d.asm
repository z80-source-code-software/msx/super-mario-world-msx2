objectlistlv022d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
    db available  :  dw  015 : dw 047,183  ;questionmark box (coin tail)
    db force26    :  dw  025 : dw 051,129  ;door
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv022d: equ $-objectlistlv022d
  

 