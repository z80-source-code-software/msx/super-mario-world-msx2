objectlistlv078c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  	db force24	:	dw 028	:	dw 021,129	;door
	db force24	:	dw 076	:	dw 021,129	;door
	db force24	:	dw 124	:	dw 021,129	;door
	db force24	:	dw 172	:	dw 021,129	;door




  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078c: equ $-objectlistlv078c
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
