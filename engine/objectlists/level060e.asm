objectlistlv060e:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
  
	db available : dw 018 : dw 047,305	;Color switcher ;use this to save code space on the ROM
	db available : dw 031 : dw 047,160	;chuck (trowing)
	db available : dw 053 : dw 048,087	;blue rotating block
	db available : dw 055 : dw 046,087	;blue rotating block
	db available : dw 057 : dw 048,087	;blue rotating block
	db available : dw 066 : dw 040,160	;chuck (trowing)
	db available : dw 075 : dw 048,087	;blue rotating block
	db available : dw 077 : dw 046,087	;blue rotating block
	db available : dw 079 : dw 048,087	;blue rotating block
	db available : dw 085 : dw 040,160	;chuck (trowing)
	db available : dw 120 : dw 036,178	;keyhole

  
  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060e: equ $-objectlistlv060e


level60ebkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4
  
    dw 6325+bgrmapaddr :   db      053, 051, 051, 051 ;calculate adress = (y * mapwidth) + x
    dw 6071+bgrmapaddr :   db      057, 092, 089, 059 ;calculate adress = (y * mapwidth) + x
    dw 6327+bgrmapaddr :   db      051, 084, 051, 084 ;calculate adress = (y * mapwidth) + x
    dw 6329+bgrmapaddr :   db      026, 092, 058, 059 ;calculate adress = (y * mapwidth) + x
    dw 6347+bgrmapaddr :   db      083, 084, 084, 026 ;calculate adress = (y * mapwidth) + x
    dw 6093+bgrmapaddr :   db      021, 058, 084, 084 ;calculate adress = (y * mapwidth) + x
    dw 6349+bgrmapaddr :   db      026, 092, 092, 092 ;calculate adress = (y * mapwidth) + x
    dw 6351+bgrmapaddr :   db      092, 051, 092, 093 ;calculate adress = (y * mapwidth) + x
    dw 000              :   db      000, 000, 000, 000
lenghtlevel60ebkgtiles: equ $-level60ebkgtiles
