objectlistlv074:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist

  
	db available	:	dw 012	:	dw 015,332	;Wendy superspike
	db available	:	dw 073	:	dw 022,034	;green !
	db available	:	dw 130	:	dw 016,263	;Falling Spike
	db available	:	dw 142	:	dw 028,046	;Dry bones (trowing)
	db available	:	dw 158	:	dw 030,264	;bonybeetle
	db available	:	dw 162	:	dw 016,263	;Falling Spike
	db available	:	dw 170	:	dw 016,263	;Falling Spike
	db available	:	dw 308	:	dw 032,128	;flame
	db available	:	dw 316	:	dw 015,034	;green !
	db available	:	dw 318	:	dw 018,222	;dry bones
	db available	:	dw 328	:	dw 032,128	;flame
	db available	:	dw 336	:	dw 015,222	;dry bones
	db available	:	dw 346	:	dw 032,128	;flame
	db available	:	dw 356	:	dw 025,264	;bonybeetle
	db available	:	dw 389	:	dw 032,128	;flame
	db available	:	dw 466	:	dw 032,128	;flame
	db force00     	:	dw 511	:	dw 024,134	;giant door




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv074: equ $-objectlistlv074

