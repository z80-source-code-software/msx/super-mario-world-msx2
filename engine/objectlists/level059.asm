objectlistlv059:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available : dw 029 : dw 041,312	;Dino Rhino
	db available : dw 054 : dw 027,313	;Dino Rhino (small)
	db available : dw 085 : dw 036,116	;slightsheertile (right)
	db available : dw 095 : dw 038,116	;slightsheertile (right)
	db available : dw 100 : dw 036,312	;Dino Rhino
	db available : dw 105 : dw 040,116	;slightsheertile (right)
	db available : dw 135 : dw 039,313	;Dino Rhino (small)
	db available : dw 145 : dw 041,313	;Dino Rhino (small)
	db available : dw 146 : dw 031,041	;Flying box (flower)
	db available : dw 166 : dw 039,313	;Dino Rhino (small)
	db available : dw 176 : dw 043,313	;Dino Rhino (small)
	db available : dw 187 : dw 039,008	;Piranjaplant
	db available : dw 194 : dw 028,313	;Dino Rhino (small)
	db available : dw 214 : dw 032,036	;Mario exit (up down)
	db available : dw 252 : dw 031,205	;spring
	db available : dw 260 : dw 032,116	;slightsheertile (right)
	db available : dw 270 : dw 034,116	;slightsheertile (right)
	db available : dw 274 : dw 032,312	;Dino Rhino
	db available : dw 280 : dw 036,116	;slightsheertile (right)
	db available : dw 290 : dw 038,116	;slightsheertile (right)
	db available : dw 302 : dw 026,004	;Checkpointtop
	db available : dw 304 : dw 029,005	;Checkpointbar
	db available : dw 307 : dw 028,031	;checkpointright
	db available : dw 331 : dw 041,076	;p-box (blue)
	db available : dw 337 : dw 045,209	;muncher
	db available : dw 341 : dw 029,026	;snapping chuck
	db available : dw 342 : dw 045,209	;muncher
	db available : dw 346 : dw 045,209	;muncher
	db available : dw 350 : dw 045,209	;muncher
	db available : dw 383 : dw 036,116	;slightsheertile (right)
	db available : dw 403 : dw 037,313	;Dino Rhino (small)
	db available : dw 405 : dw 036,115	;slightsheertile (left)
	db available : dw 417 : dw 027,041	;Flying box (flower)
	db available : dw 424 : dw 029,313	;Dino Rhino (small)
	db available : dw 449 : dw 031,058	;yoshi egg ? box
	db available : dw 458 : dw 039,008	;Piranjaplant
	db available : dw 464 : dw 041,313	;Dino Rhino (small)
	db available : dw 478 : dw 037,034	;green !
	db available : dw 483 : dw 041,008	;Piranjaplant
	db available : dw 488 : dw 043,313	;Dino Rhino (small)
	db available : dw 494 : dw 038,036	;Mario exit (up down)
	db available : dw 496 : dw 042,033	;red !
	db available : dw 498 : dw 042,033	;red !
	db available : dw 500 : dw 042,033	;red !
	db available : dw 502 : dw 042,033	;red !
	db available : dw 507 : dw 034,008	;Piranjaplant
	db available : dw 512 : dw 037,036	;Mario exit (up down)
	db available : dw 516 : dw 040,313	;Dino Rhino (small)
	db available : dw 582 : dw 043,205	;spring
	db available : dw 583 : dw 033,090	;Brown flying koopa
	db available : dw 600 : dw 027,007	;Endflagbar
	db available : dw 601 : dw 026,006	;Endflagtop



  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv059: equ $-objectlistlv059
