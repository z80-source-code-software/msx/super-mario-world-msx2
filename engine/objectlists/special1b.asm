objectlistspecial1b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


        db available    :       dw 010  :       dw 018,289      ;greenmushroom (static)
        db available    :       dw 012  :       dw 018,289      ;greenmushroom (static)
        db available    :       dw 014  :       dw 018,289      ;greenmushroom (static)
        db available    :       dw 016  :       dw 018,289      ;greenmushroom (static)
        db available    :       dw 037  :       dw 004,162      ;exit (upwards)
  


  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial1b: equ $-objectlistspecial1b
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
