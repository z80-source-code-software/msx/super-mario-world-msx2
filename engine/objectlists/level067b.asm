objectlistlv067b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available : dw 005 : dw 013,270	;Lemmy's block mover (helper object)
	db available : dw 045 : dw 023,334	;wendyfireball
	db available : dw 077 : dw 022,335	;wendyfireballxl
	db available : dw 124 : dw 022,334	;wendyfireball
	db available : dw 136 : dw 022,335	;wendyfireballxl
;	db available : dw 173 : dw 020,334	;wendyfireball
	db available : dw 184 : dw 031,333	;wendy moving block
	db available : dw 193 : dw 024,335	;wendyfireballxl
	db available : dw 211 : dw 016,222	;dry bones
	db available : dw 224 : dw 012,334	;wendyfireball
	db available : dw 225 : dw 020,334	;wendyfireball
	db available : dw 237 : dw 032,034	;green !
	db available : dw 246 : dw 034,134	;giant door



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv067b: equ $-objectlistlv067b
