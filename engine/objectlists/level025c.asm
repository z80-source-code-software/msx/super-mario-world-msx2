objectlistlv025c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

   db available :  dw  016 : dw 026,009  ;red flying koopa (up and down)
   db available :  dw  017 : dw 021,009  ;red flying koopa (up and down)
   
   db available :  dw  031 : dw 026,009  ;red flying koopa (up and down)
   db available :  dw  032 : dw 021,009  ;red flying koopa (up and down)
  
   
   db available :  dw  044 : dw 023,009  ;red flying koopa (up and down)
   
  db available   :  dw  048 : dw 014,055  ;yoshi coin      
   
   db available :  dw  051 : dw 023,009  ;red flying koopa (up and down)
   

  db force00    :  dw  057 : dw 025,054  ;tubeexit to the right 
  db available  :  dw  058 : dw 027,108  ;blockwall for mario (right)

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv025c: equ $-objectlistlv025c


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    056*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 
  
 