objectlistlv020:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
 
   db available :  dw  009    : dw 038,032    ;exclamationmark (yellow)


  db available  :  dw  018 : dw 046,109  ;blockwall for mario (left)
  db available  :  dw  024 : dw 046,108  ;blockwall for mario (right)
   
   db available :  dw  041    : dw 043,175    ;football chuck
   
  db available  :  dw  048 : dw 040,108  ;blockwall for mario (right)   
  db available  :  dw  052 : dw 034,108  ;blockwall for mario (right)   
  db available  :  dw  058 : dw 034,109  ;blockwall for mario (left)
  
   db available :  dw  077 : dw 008,178  ;keyhole  
    
  db available  :  dw  085 : dw 010,127  ;blockwall (16x16) 
   
  db available  :  dw  101 : dw 044,177  ;blueshell
  
  db available  :  dw  117 : dw 042,108  ;blockwall for mario (right)   
  
  db available    :  dw  120 : dw 038,054  ;tubeexit to the right

  db available  :  dw  121 : dw 040,108  ;blockwall for mario (right)  
 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv020: equ $-objectlistlv020


  
level20bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4
  
    dw 5084+bgrmapaddr :   db      082, 021, 082, 068 ;calculate adress = (y * mapwidth) + x
    dw 4562+bgrmapaddr :   db      005, 009, 005, 009  
;    dw 4040+bgrmapaddr |   db      000, 000, 000, 000   ;niet nodig is 0 0 0 0
    dw 3518+bgrmapaddr :   db      000, 000, 038, 000
    dw 5379+bgrmapaddr :   db      015, 050, 015, 050
    dw 5381+bgrmapaddr :   db      069, 068, 069, 068
    dw 5383+bgrmapaddr :   db      009, 014, 019, 020
    dw 000             :   db      000, 000, 000, 000
lenghtlevel20bkgtiles: equ $-level20bkgtiles

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    119*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
