objectlistlv022b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


;  db available  |  dw  005 | dw 019,182  ;vinehelper ghost house (helper object)
  db force22  :  dw  010 : dw 019,129  ;door
  
  db available  :  dw  011 : dw 038,185  ;small ghost (following mario)
  db available  :  dw  018 : dw 055,185  ;small ghost (following mario)

  db available :  dw  035 : dw 044,074  ;smiley box (climbing plant)
  db available  :  dw  036 : dw 019,182  ;vinehelper ghost house (helper object)  
  
  db available  :  dw  045 : dw 033,185  ;small ghost (following mario)  
  
  db force26  :  dw  055 : dw 040,129  ;door




  db available  :  dw  056 : dw 049,185  ;small ghost (following mario)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv022b: equ $-objectlistlv022b
  
 