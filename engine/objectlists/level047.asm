objectlistlv047:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db  available :  dw  067 : dw 048,264  ;bony beetle
    
  db  available :  dw  072-1 : dw 040-7,220  ;ball n chain    

  db  available :  dw  091 : dw 040,264  ;bony beetle

  db  available :  dw  100-1 : dw 033-7,220  ;ball n chain  
  db  available :  dw  116-1 : dw 033-7,220  ;ball n chain  

  db force26  :  dw  157 : dw 038,129  ;door 


  db available :  dw  160 : dw 040,272  ;red mushroom (static) 

  db available :  dw  186 : dw 012-3,036  ;tube cover up exit sprite  

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv047: equ $-objectlistlv047

