objectlistlv066b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


	db available : dw 013 : dw 005,327	;Special yellow pillar
	db available : dw 026 : dw 002,221	;thwomp
	db available : dw 052 : dw 006,221	;thwomp
	db available : dw 072 : dw 006,221	;thwomp
	db available : dw 077 : dw 023,218	;thwimp
	db available : dw 100 : dw 019,029	;feather box
	db available : dw 102 : dw 008,221	;thwomp
	db available : dw 130 : dw 006,221	;thwomp
	db available : dw 145 : dw 023,033	;red !
	db available : dw 147 : dw 021,033	;red !
	db available : dw 149 : dw 006,221	;thwomp
	db available : dw 155 : dw 023,218	;thwimp
	db available : dw 168 : dw 006,221	;thwomp
	db available : dw 189 : dw 021,218	;thwimp
	db available : dw 204 : dw 021,218	;thwimp
	db available : dw 221 : dw 006,221	;thwomp
	db available : dw 251 : dw 006,221	;thwomp
	db available : dw 272 : dw 004,221	;thwomp
	db available : dw 300 : dw 004,221	;thwomp
	db available : dw 313 : dw 021,218	;thwimp
	db available : dw 336 : dw 017,134	;giant door

  
  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv066b: equ $-objectlistlv066b
