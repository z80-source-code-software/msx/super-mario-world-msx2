objectlistlv060i:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 016 : dw 040,180	;smileybox (pbox)
	db available : dw 038 : dw 044,102	;ultracoin (static)
	db available : dw 065 : dw 044,102	;ultracoin (static)
	db available : dw 075 : dw 035,033	;red !
	db available : dw 077 : dw 035,033	;red !
	db available : dw 082 : dw 026,055	;yoshi coin
	db available : dw 103 : dw 033,007	;Endflagbar
	db available : dw 104 : dw 032,006	;Endflagtop


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060i: equ $-objectlistlv060i
