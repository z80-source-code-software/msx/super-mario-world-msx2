objectlistlv070:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist

    db available  :  dw  003 : dw 030,130  ;static camerascroller
    db available  :  dw  004 : dw 040,152  ;blocktileanimator
    db available  :  dw  005 : dw 040,168  ;mariocrusher  
  
  
	db available	:	dw 050	:	dw 035,151	;Yellow pillar
	db available	:	dw 052	:	dw 004,170	;bat
	db available	:	dw 066	:	dw 039,151	;Yellow pillar
	db available	:	dw 071	:	dw 004,170	;bat
	db available	:	dw 086	:	dw 026,061	;pink koopa
	db available	:	dw 090	:	dw 034,151	;Yellow pillar
	db available	:	dw 108	:	dw 026,169	;cavekoopa
	db available	:	dw 113	:	dw 040,151	;Yellow pillar
	db available	:	dw 121	:	dw 004,170	;bat
	db available	:	dw 126	:	dw 040,029	;feather box
	db available	:	dw 128	:	dw 045,169	;cavekoopa
	db available	:	dw 130	:	dw 033,151	;Yellow pillar
	db available	:	dw 141	:	dw 029,190	;yellow shell
	db available	:	dw 146	:	dw 029,067	;yellow naked koopa
	db available	:	dw 149	:	dw 040,151	;Yellow pillar
	db available	:	dw 167	:	dw 004,170	;bat
	db available	:	dw 169	:	dw 028,169	;cavekoopa
	db available	:	dw 173	:	dw 043,151	;Yellow pillar
	db available	:	dw 194	:	dw 029,169	;cavekoopa
	db available	:	dw 199	:	dw 039,151	;Yellow pillar
	db available	:	dw 210	:	dw 004,170	;bat
	db available	:	dw 214	:	dw 032,169	;cavekoopa
	db available	:	dw 218	:	dw 043,151	;Yellow pillar
	db available	:	dw 239	:	dw 028,169	;cavekoopa
	db available	:	dw 260	:	dw 040,032	;yellow !
	db available	:	dw 263	:	dw 004,170	;bat
	db available	:	dw 267	:	dw 042,061	;pink koopa
	db available	:	dw 270	:	dw 034,151	;Yellow pillar
	db available	:	dw 273	:	dw 018,055	;yoshi coin
	db available	:	dw 278	:	dw 004,170	;bat
	db available	:	dw 288	:	dw 034,151	;Yellow pillar
	
        db available    :       dw 298  :       dw 028,004      ;Checkpointtop
        db available    :       dw 299  :       dw 004,170      ;bat
        db available    :       dw 300  :       dw 031,005      ;Checkpointbar
        db available    :       dw 303  :       dw 030,031      ;checkpointright

	
	
	db available	:	dw 309	:	dw 035,054	;mario exit (right)





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv070: equ $-objectlistlv070

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    308*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
