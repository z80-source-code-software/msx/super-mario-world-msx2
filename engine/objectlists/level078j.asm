objectlistlv078j:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

        db available    :       dw 007  :       dw 024,127      ;blockwall (16x16)
        db available    :       dw 028  :       dw 024,046      ;Dry bones (trowing)
        db available    :       dw 050  :       dw 012,262      ;deadfish
        db available    :       dw 059  :       dw 011,220      ;ball n chain
        db available    :       dw 070  :       dw 025,262      ;deadfish
        db available    :       dw 105  :       dw 016,262      ;deadfish
        db available    :       dw 108  :       dw 010,028      ;flower box
        db available    :       dw 127  :       dw 024,046      ;Dry bones (trowing)
        db available    :       dw 145  :       dw 008,262      ;deadfish
        db available    :       dw 148  :       dw 026,054      ;mario exit (right)
        db available    :       dw 149  :       dw 026,127      ;blockwall (16x16)


  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078j: equ $-objectlistlv078j
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    001      :     db 64+6
dw    147*8    :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 

 
