objectlistlv073c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 013	:	dw 045,076	;p-box (blue)
	db available	:	dw 021	:	dw 047,102	;ultracoin (static)
	db available	:	dw 034	:	dw 043,055	;yoshi coin
	db available	:	dw 068	:	dw 041,078	;coin (static)
	db available	:	dw 069	:	dw 045,078	;coin (static)
	db available	:	dw 070	:	dw 043,078	;coin (static)
	db available	:	dw 081	:	dw 035,055	;yoshi coin
	db available	:	dw 095	:	dw 039,183	;? box (cointail)
	db available	:	dw 103	:	dw 042,185	;small ghost (following mario)
	db available	:	dw 119	:	dw 043,269	;door (spriteless)
	db available	:	dw 129	:	dw 040,185	;small ghost (following mario)
	db available	:	dw 132	:	dw 010,178	;keyhole



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv073c: equ $-objectlistlv073c

