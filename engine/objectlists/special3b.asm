objectlistspecial3b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


    ;    db available    :       dw 013  :       dw 018,186      ;rotating platform (permanent)
        db available    :       dw 027  :       dw 012,366      ;bart


  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial3b: equ $-objectlistspecial3b
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
