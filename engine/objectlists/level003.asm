objectlistlv003:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     

  db force19   :  dw  014 : dw 009,089  ;flying box (alternate movement) (coin)  
  
  db force25   :  dw  021 : dw 027-1,087  ;blue rotating block
  
  db force19   :  dw  022 : dw 007,089  ;flying box (alternate movement) (coin) 
  
  db force25   :  dw  023 : dw 025-1,087  ;blue rotating block
;  db force26   |  dw  023 | dw 027-1,087  ;blue rotating block
  db force26   :  dw  025 : dw 027-1,087  ;blue rotating block



  db force00   :  dw  040 : dw 008,088  ;flying box (alternate movement) (1up) 
  db force00   :  dw  047 : dw 009,089  ;flying box (alternate movement) (coin)   
;;  db available |  dw  051 | dw 012,090  ;flying box (alternate movement) (coin)   


  db available :  dw  056 : dw 025,054  ;tubeexit to the right ;force06

  db  00       :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv003: equ $-objectlistlv003
  

  
level03bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 1673+bgrmapaddr :   db      069, 082, 069, 082 ;calculate adress = (y * mapwidth) + x
    dw 1803+bgrmapaddr :   db      069, 051, 069, 051  
    dw 1805+bgrmapaddr :   db      069, 082, 069, 082   
    dw 1807+bgrmapaddr :   db      068, 050, 068, 050
    dw 000             :   db      000, 000, 000, 000
lenghtlevel03bkgtiles: equ $-level03bkgtiles    

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    055*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 
   
