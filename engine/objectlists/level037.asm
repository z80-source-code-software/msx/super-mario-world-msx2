;Y based map
objectlistlv037:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db force00   :  dw  044-1 : dw 022,054  ;tubeexit to the right 

  db available  :  dw  013-3 : dw 031-1,245  ;springboard (left) 

  db available  :  dw  002 : dw 036,246  ;springboard (right) 

  db available   :  dw  031 : dw 042,055  ;yoshi coin
  
  db available  :  dw  013-3 : dw 045-1,245  ;springboard (left)  

  db available  :  dw  002 : dw 054,246  ;springboard (right)  
  
  db available   :  dw  056 : dw 055,055  ;yoshi coin

    db available  :  dw  052 : dw 088,205  ;spring 

  db  available :  dw  033 : dw 092,091  ;red flying koopa
  db  available :  dw  036 : dw 094,091  ;red flying koopa

  db  available :  dw  039 : dw 097,091  ;red flying koopa  

  db force00   :  dw  005-2 : dw 098,249  ;tubecoverup for mario exit (left)
  
    db available  :  dw  035 : dw 104,205  ;spring 

      db available :  dw  050 : dw 106,057  ;questionmark animation (coin)
      db available :  dw  052 : dw 106,057  ;questionmark animation (coin)
              db available :  dw  054 : dw 106,029  ;questionmark animation (feather)


  db available  :  dw  002 : dw 136,246  ;springboard (right)

  db available :  dw  048 : dw 140,074  ;smiley box (climbing plant)

  db available  :  dw  011-3 : dw 141,245  ;springboard (left)
  
        db available  :  dw  035 : dw 146,119  ;multicoin box  

  db available  :  dw  002 : dw 150,246  ;springboard (right)

  db available   :  dw  008 : dw 154,055  ;yoshi coin
 
  db available :  dw  054 : dw 164,074  ;smiley box (climbing plant)

  db available  :  dw  059 : dw 165,061  ;pink coopa

  db available   :  dw  041 : dw 176,055  ;yoshi coin

  db available  :  dw  029-3 : dw 178-1,245  ;springboard (left)

      db available :  dw  006 : dw 178,057  ;questionmark animation (coin)
              db available :  dw  008 : dw 178,029  ;questionmark animation (feather)
  
  db  available :  dw  027 : dw 183,091  ;red flying koopa  
  
  db available  :  dw  029-3 : dw 188-1,245  ;springboard (left)

  db available   :  dw  059 : dw 195,055  ;yoshi coin  

  db available  :  dw  043 : dw 200,061  ;pink coopa

  db  available :  dw  036 : dw 210,093  ;green flying koopa   
 

  
 
  db  00        :  dw  999 : dw 999,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv037: equ $-objectlistlv037

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    002*8      :     db 32
dw    042*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!