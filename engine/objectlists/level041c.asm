objectlistlv041c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
 
  db available  :  dw  003 : dw 019,270  ;Lemmy's block mover (helper object)  
  
  db available  :  dw  069 : dw 018,222  ;Dry bones 
  
  db available :  dw  099 : dw 029,128  ;flame   
    
  db available  :  dw  108 : dw 022,222  ;Dry bones     
    
  db available  :  dw  138 : dw 019,271  ;single grey block sprite  

  db available :  dw  149 : dw 029,128  ;flame 
    
  db available  :  dw  158 : dw 018,222  ;Dry bones  

  db available :  dw  193 : dw 029,128  ;flame  
  db available :  dw  211 : dw 030,272  ;red mushroom (static)  
  db available :  dw  280 : dw 029,128  ;flame  
     
  db available  :  dw  288 : dw 022,222  ;Dry bones     
  db available  :  dw  313 : dw 011,222  ;Dry bones     
  db available  :  dw  335 : dw 022,222  ;Dry bones     

  db force18    :  dw  374 : dw 25,134  ;giant door

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv041c: equ $-objectlistlv041c

