objectlistlv034b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available  :  dw  003 : dw 032,103  ;lava (mario killer)

  db force26  :  dw  025 : dw 048-1,239  ;floating skull
 
  db available  :  dw  062 : dw 048+2,240  ;blargh 
  db available  :  dw  099 : dw 048+2,240  ;blargh 
  db available  :  dw  145 : dw 048+2,240  ;blargh 
  
  db available   :  dw  155 : dw 027,055  ;yoshi coin   
  
  db available  :  dw  168 : dw 048+2,240  ;blargh 

  db available   :  dw  207 : dw 030,174  ;spiketop
  db available   :  dw  210 : dw 046,174  ;spiketop
  db available   :  dw  227 : dw 030,174  ;spiketop
  db available   :  dw  233 : dw 046,174  ;spiketop

    db available :  dw  250 : dw 046-2,169  ;cavekoopa  
    db available :  dw  263 : dw 042-2,169  ;cavekoopa  

  db available   :  dw  283 : dw 046,055  ;yoshi coin 

         db available :  dw  297 : dw 040,029  ;questionmark animation (feather)
    db available :  dw  298 : dw 046-2,169  ;cavekoopa

    db available :  dw  321 : dw 046-2,169  ;cavekoopa  
    db available :  dw  331 : dw 046-2,169  ;cavekoopa

   db available  :  dw  342 : dw 012,209  ;muncher

  db available  :  dw  347 : dw 048-1,239  ;floating skull

 ;   db available :  dw  364 : dw 039-3,242  ;piranjaplant2

   db available  :  dw  365 : dw 012,209  ;muncher    
    
;    db available :  dw  372 : dw 039-3,242  ;piranjaplant2
  db available   :  dw  373 : dw 009,055  ;yoshi coin     
    db available :  dw  378 : dw 039-3,242  ;piranjaplant2
;    db available :  dw  386 : dw 039-3,242  ;piranjaplant2

   db available  :  dw  394 : dw 012,209  ;muncher
   db available  :  dw  407 : dw 012,209  ;muncher

db available  :  dw  417 : dw 012,076  ;p-box (blue)

db available  :  dw  421 : dw 018-1,243  ;secret box (pbox)
db available  :  dw  431 : dw 026-1,243  ;secret box (pbox)
 
  db force00   :  dw  439 : dw 030-3+4,007  ;endflag bar
  db available :  dw  439+1 : dw 030-3+1+1,006  ;endflag top 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv034b: equ $-objectlistlv034b

