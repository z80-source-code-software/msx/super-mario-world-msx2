objectlistlv054:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  
	db available : dw 003 : dw 034,130	;static camerascroller
	db available : dw 006 : dw 033,309	;BLocktileanimator2
	db force00      : dw 033 : dw 028,133	;Iggy block coverup
	db force28	    : dw 034 : dw 030,131	;Giant iggy castle rock
	db force02	    : dw 049 : dw 028,133	;Iggy block coverup
	db available : dw 060 : dw 044,308	;Circlesaw
	db available : dw 094 : dw 043,308	;Circlesaw
	db force00	    : dw 113 : dw 028,133	;Iggy block coverup
	db force28	    : dw 114 : dw 030,131	;Giant iggy castle rock
	db force02	    : dw 120 : dw 043,308	;Circlesaw
	db available : dw 129 : dw 028,133	;Iggy block coverup
	db force00	    : dw 145 : dw 028,133	;Iggy block coverup
	db force28	    : dw 146 : dw 030,131	;Giant iggy castle rock
	db force02	    : dw 154 : dw 043,308	;Circlesaw
	db available : dw 161 : dw 028,133	;Iggy block coverup
	db available : dw 166 : dw 041,308	;Circlesaw
	db force28	    : dw 184 : dw 044,129	;door

  
  

;	db force00	    |	dw 065	|	dw 028,133	;Iggy block coverup
;	db force28	    |	dw 066	|	dw 030,131	;Giant iggy castle rock
;	db force02	    |	dw 081	|	dw 028,133	;Iggy block coverup




;  	db force28  	|	dw 184	|	dw 044,129	;door

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv054: equ $-objectlistlv054

