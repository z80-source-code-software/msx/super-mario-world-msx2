objectlistlv054b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 004 : dw 031,167	;Diagonal block (helper object)
	db available : dw 032 : dw 028,308	;Circlesaw
	db available : dw 042 : dw 048,128	;flame
	db available : dw 075 : dw 044,308	;Circlesaw
	db available : dw 078 : dw 046,165	;diagonal block (left)
	db available : dw 079 : dw 030,127	;blockwall (16x16)
	db available : dw 086 : dw 028,032	;yellow !
	db available : dw 102 : dw 048,128	;flame
	db available : dw 110 : dw 048,128	;flame
	db available : dw 136 : dw 048,128	;flame
	db available : dw 144 : dw 048,128	;flame
	db available : dw 146 : dw 042,308	;Circlesaw
	db available : dw 155 : dw 048,128	;flame
	db available : dw 169 : dw 048,128	;flame
	db available : dw 176 : dw 042,308	;Circlesaw
	db available : dw 193 : dw 048,128	;flame
	db available : dw 209 : dw 036,308	;Circlesaw
	db available : dw 226 : dw 036,308	;Circlesaw
	db available : dw 231 : dw 045,308	;Circlesaw
	db available : dw 234 : dw 026,029	;feather box
	db available : dw 242 : dw 036,308	;Circlesaw
	db available : dw 250 : dw 046,165	;diagonal block (left)
	db available : dw 272 : dw 034,134	;giant door
	db available : dw 314 : dw 048,128	;flame
	db available : dw 322 : dw 048,128	;flame
	db available : dw 336 : dw 048,128	;flame
	db available : dw 344 : dw 048,128	;flame
	db available : dw 366 : dw 048,128	;flame
	db available : dw 374 : dw 048,128	;flame
	db available : dw 391 : dw 048,128	;flame
	db available : dw 399 : dw 048,128	;flame
	db available : dw 424 : dw 048,128	;flame
	db available : dw 432 : dw 048,128	;flame
	db available : dw 453 : dw 048,128	;flame
	db available : dw 461 : dw 048,128	;flame
	db available : dw 475 : dw 040,225	;secret box (1up)
	db available : dw 483 : dw 032,225	;secret box (1up)
	db available : dw 489 : dw 038,225	;secret box (1up)
	db available : dw 495 : dw 030,225	;secret box (1up)
	db available : dw 501 : dw 038,225	;secret box (1up)
	db available : dw 502 : dw 042,134	;giant door
	db available : dw 507 : dw 032,225	;secret box (1up)





 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv054b: equ $-objectlistlv054b

