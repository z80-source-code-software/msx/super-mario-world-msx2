objectlistlv075b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist

  db  available :  dw  003 : dw 015,217  ;sheer stairs (helper object)
  db  available :  dw  050 : dw 033,219  ;blockwall (standingon)
  

	db available	:	dw 033	:	dw 023,046	;Dry bones (trowing)
	db available	:	dw 065	:	dw 018,331	;Wooden pillar (up down)
	db available	:	dw 080	:	dw 029,267	;magikoopa
	db available	:	dw 097	:	dw 033,128	;flame
	db available	:	dw 099	:	dw 020,032	;yellow !
	db available	:	dw 107	:	dw 029,330	;Wooden pillar (down up)
	db available	:	dw 117	:	dw 029,330	;Wooden pillar (down up)
	db available	:	dw 133	:	dw 029,267	;magikoopa
	db available	:	dw 139	:	dw 033,128	;flame
	db available	:	dw 161	:	dw 009,331	;Wooden pillar (up down)
	db available	:	dw 170	:	dw 009,331	;Wooden pillar (up down)
	db available	:	dw 179	:	dw 022,046	;Dry bones (trowing)
	db available	:	dw 189	:	dw 032,128	;flame
	db available	:	dw 204	:	dw 027,046	;Dry bones (trowing)
	db available	:	dw 219	:	dw 010,331	;Wooden pillar (up down)
	db available	:	dw 230	:	dw 032,128	;flame
	db available	:	dw 235	:	dw 013,331	;Wooden pillar (up down)
	db available	:	dw 242	:	dw 020,267	;magikoopa
	db available	:	dw 248	:	dw 032,128	;flame
	db available	:	dw 252	:	dw 013,331	;Wooden pillar (up down)
	db available	:	dw 289	:	dw 027,046	;Dry bones (trowing)
;	db available	:	dw 310	:	dw 025,134	;giant door

	db force00     	:	dw 310	:	dw 025,134	;giant door



  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv075b: equ $-objectlistlv075b

