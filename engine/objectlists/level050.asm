objectlistlv050:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


   db available  :  dw  035 : dw 018,301  ;exploding bubble (boomb)

 ; db available   :  dw  048 : dw 023,055  ;yoshi coin

   db available  :  dw  065 : dw 016,300  ;exploding bubble (goomba)

   db available  :  dw  080 : dw 020,301  ;exploding bubble (boomb)

   db available  :  dw  090 : dw 018,300  ;exploding bubble (goomba)

    db available  :  dw  096 : dw 026,205  ;spring 

  db available  :  dw  106 : dw 019,058  ;questionmark animation (green yoshi egg)

   db available  :  dw  112 : dw 014,301  ;exploding bubble (boomb)
   
     db force00   :  dw  113+1 : dw 020-1,036  ;tube cover up exit sprite
   
   db available  :  dw  123 : dw 020,301  ;exploding bubble (boomb)



   db available  :  dw  144 : dw 019,301  ;exploding bubble (boomb)

   db available  :  dw  148 : dw 020,300  ;exploding bubble (goomba)

   db available  :  dw  162 : dw 019,301  ;exploding bubble (boomb)

   db available  :  dw  166 : dw 005,302  ;exploding bubble (mushroom)


    db available :  dw  191 : dw 016,001  ;pink shell

    db available  :  dw  192 : dw 008,205  ;spring 

   db available  :  dw  224 : dw 016,300  ;exploding bubble (goomba)

 ; db available   :  dw  225 : dw 012,055  ;yoshi coin

   db available  :  dw  227 : dw 014,301  ;exploding bubble (boomb)

   db available  :  dw  233 : dw 018,300  ;exploding bubble (goomba)
   db available  :  dw  257 : dw 010,300  ;exploding bubble (goomba)
   db available  :  dw  272 : dw 017,300  ;exploding bubble (goomba)
   db available  :  dw  277 : dw 018,300  ;exploding bubble (goomba)
   db available  :  dw  282 : dw 015,300  ;exploding bubble (goomba)

  db force18   :  dw  300 : dw 021-3,004  ;checkpoint top
  db available :  dw  300+2 : dw 021,005  ;checkpoint bar
  db force04   :  dw  300+5 : dw 021-1,031  ;checkpoint pole right

   db available  :  dw  311 : dw 011,300  ;exploding bubble (goomba)
   db available  :  dw  319 : dw 018,300  ;exploding bubble (goomba)

   db available  :  dw  326 : dw 012,302  ;exploding bubble (mushroom)

;  db available   :  dw  340 : dw 012,055  ;yoshi coin

   db available  :  dw  351 : dw 015,301  ;exploding bubble (boomb)

   db available  :  dw  359 : dw 011,300  ;exploding bubble (goomba)

   db available  :  dw  387 : dw 012,301  ;exploding bubble (boomb)
   db available  :  dw  399 : dw 008,300  ;exploding bubble (goomba)
   db available  :  dw  411 : dw 008,301  ;exploding bubble (boomb)
   
  db available :  dw  425 : dw 020,057  ;questionmark animation (coin)   
  db available :  dw  427 : dw 020,057  ;questionmark animation (coin)   
  db available :  dw  428 : dw 012,057  ;questionmark animation (coin)   
  db available :  dw  429 : dw 020,057  ;questionmark animation (coin)   
   

  db available :  dw  449 : dw 018,057  ;questionmark animation (coin) 
  db available :  dw  451 : dw 014,057  ;questionmark animation (coin) 
  db available :  dw  455 : dw 020,057  ;questionmark animation (coin) 
  db available :  dw  459 : dw 012,027  ;questionmark animation red mushroom 
  db available :  dw  463 : dw 016,057  ;questionmark animation (coin) 
;    db available   :  dw  468 : dw 010,055  ;yoshi coin 
  db available :  dw  469 : dw 019,057  ;questionmark animation (coin) 
  db available :  dw  471 : dw 010,057  ;questionmark animation (coin) 
  db available :  dw  475 : dw 014,057  ;questionmark animation (coin) 
  db available :  dw  483 : dw 016,057  ;questionmark animation (coin) 
  db available :  dw  487 : dw 020,057  ;questionmark animation (coin) 
  db available :  dw  489 : dw 012,057  ;questionmark animation (coin) 
  db available :  dw  491 : dw 016,057  ;questionmark animation (coin) 
  db available :  dw  498 : dw 020,057  ;questionmark animation (coin) 
  db available :  dw  499 : dw 014,027  ;questionmark animation red mushroom


   db available  :  dw  539 : dw 017,300  ;exploding bubble (goomba)
   
    db available :  dw  548 : dw 026,238  ;replicating chuck    
   
   
   db available  :  dw  549 : dw 016,300  ;exploding bubble (goomba)
   db available  :  dw  560 : dw 007,300  ;exploding bubble (goomba)

  db force00   :  dw  564+1 : dw 012-1,036  ;tube cover up exit sprite
 ;   db available   :  dw  568 : dw 024,055  ;yoshi coin 
 
  db available :  dw  576 : dw 020,057  ;questionmark animation (coin) 
  db available :  dw  578 : dw 018,057  ;questionmark animation (coin) 
  db available :  dw  580 : dw 016,057  ;questionmark animation (coin) 

   db available  :  dw  582 : dw 022,301  ;exploding bubble (boomb)   
   
   db available  :  dw  585 : dw 010,300  ;exploding bubble (goomba)   
;   db available  |  dw  589 | dw 014,300  ;exploding bubble (goomba)   


  db force00   :  dw  595 : dw 008,007  ;endflag bar
  db force28   :  dw  595+1 : dw 008-1,006  ;endflag top

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv050: equ $-objectlistlv050

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    117*8      :     db 32 ;-2
dw    125*8      :     db 32 ;-2
dw    243*8      :     db 32 ;-2
dw    257*8      :     db 32 ;-2
dw    349*8      :     db 32 ;-2
dw    551*8      :     db 32 ;-2
dw    000         ;defines the end of the list COMPULSORY!! 



