objectlistlv040b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

 ; db available  |  dw  001 | dw 026+1,153  ;water (mario swimming) 
   
  db available  :  dw  006 : dw 008,265  ;msx powerup 
;  db available  |  dw  022 | dw 044,265  ;msx powerup 
  
  db available  :  dw  019 : dw 040-1,127  ;blockwall (16 x 16) 
  db available  :  dw  021 : dw 040-1,127  ;blockwall (16 x 16) 

  db  available :  dw  036 : dw 045,262  ;deadfish 
  
  db available :  dw  045 : dw 030,221  ;twhomp    
    
  db  available :  dw  046 : dw 043,262  ;deadfish
  
 
     
  db  available :  dw  063 : dw 042,262  ;deadfish   
  
  db available :  dw  067 : dw 038,029  ;questionmark animation (feather)   
  
  
  db  available :  dw  070 : dw 046,264  ;bony beetle  
  db  available :  dw  074 : dw 046,264  ;bony beetle  

;  db  available |  dw  075 | dw 015,262  ;deadfish
;  db  available |  dw  079 | dw 018,262  ;deadfish
 
  db available :  dw  081 : dw 030,221  ;twhomp 
 
  
  db  available :  dw  099 : dw 034,046  ;dry bones (trowing variant)  

  db  available :  dw  125 : dw 012,262  ;deadfish


  db available :  dw  126 : dw 029,221  ;twhomp

  db  available :  dw  128 : dw 018,262  ;deadfish 

  db  available :  dw  134 : dw 034,262  ;deadfish 

  
  db available  :  dw  145 : dw 040-1,127  ;blockwall (16 x 16) 
  db available  :  dw  147 : dw 040-1,127  ;blockwall (16 x 16) 

  db  available :  dw  164 : dw 044,046  ;dry bones (trowing variant) 

;  db  available |  dw  167 | dw 035,262  ;deadfish 
  db  available :  dw  168 : dw 041,262  ;deadfish 
  db available :  dw  169 : dw 029,221  ;twhomp

 ; db  available |  dw  177 | dw 012,262  ;deadfish 
 ; db  available |  dw  180 | dw 018,262  ;deadfish 


  db  available :  dw  192 : dw 034,262  ;deadfish 

;  db  available |  dw  198-1 | dw 033-7,220  ;ball n chain
;  db  available |  dw  208-1 | dw 044-7,220  ;ball n chain
  db  available :  dw  208-1 : dw 039-7,220  ;ball n chain

;  db  available |  dw  223 | dw 041,262  ;deadfish
  
 
  
  
 ; db  available |  dw  228 | dw 036,262  ;deadfish

  db  available :  dw  239 : dw 038,264  ;bony beetle 
  db  available :  dw  242 : dw 038,264  ;bony beetle   
  
  
 ; db  available |  dw  243 | dw 034,262  ;deadfish

  db force18    :  dw  245 : dw 34,134  ;giant door
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv040b: equ $-objectlistlv040b



