objectlistlv061b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
	db available : dw 015 : dw 016,210	;falling platform
	db available : dw 022 : dw 018,083	;supercoin (static)
	db available : dw 023 : dw 034,083	;supercoin (static)
	db available : dw 026 : dw 040,083	;supercoin (static)
	db available : dw 028 : dw 026,083	;supercoin (static)
	db available : dw 032 : dw 019,098	;rotating platform
	db available : dw 039 : dw 022,083	;supercoin (static)
	db available : dw 041 : dw 038,083	;supercoin (static)
	db available : dw 044 : dw 027,083	;supercoin (static)
	db available : dw 045 : dw 036,083	;supercoin (static)
	db available : dw 051 : dw 024,055	;yoshi coin
	db available : dw 066 : dw 030,036	;Mario exit (up down)


  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv061b: equ $-objectlistlv061b
