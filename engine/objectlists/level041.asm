objectlistlv041:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

db available  :  dw  001 : dw 048+2,104  ;water (mario swimming)

  db available  :  dw  008 : dw 028,267  ;magikoopa

  db available :  dw  033    : dw 032,042    ;smileybox (extra life)
  
    db available :  dw  039 : dw 048-1,128  ;flame  
  
  db available  :  dw  053 : dw 040,267  ;magikoopa

    db available :  dw  061 : dw 048-1,128  ;flame  

  db available :  dw  063    : dw 032,042    ;smileybox (extra life)
  
    db available :  dw  085 : dw 048-1,128  ;flame  
  
  db available  :  dw  101 : dw 040,267  ;magikoopa
  
        db available :  dw  115 : dw 038,029  ;questionmark animation (feather)  
  
  db available  :  dw  146 : dw 047,268  ;waterplatform (type2)
  
  
  db available  :  dw  148-1 : dw 031,267  ;magikoopa
  
db available  :  dw  163 : dw 030,076  ;p-box (blue)  
  
  db available  :  dw  170 : dw 047,268  ;waterplatform (type2)  
  
  db available  :  dw  180-1 : dw 038,267  ;magikoopa
  
  db available  :  dw  193 : dw 047,268  ;waterplatform (type2)  

  db available  :  dw  196 : dw 040,083  ;supercoin (static)
  db available  :  dw  200 : dw 036,269  ;door (spriteless)
  
  db available  :  dw  221 : dw 042,267  ;magikoopa
  
    db available :  dw  236 : dw 048-1,128  ;flame   
    db available :  dw  247 : dw 048-1,128  ;flame   
  
  db available  :  dw  255 : dw 028,267  ;magikoopa

    db available :  dw  266 : dw 048-1,128  ;flame
    
  db force26  :  dw  304 : dw 030,129  ;door    
    
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv041: equ $-objectlistlv041

