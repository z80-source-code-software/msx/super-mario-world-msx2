objectlistlv014:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
   
   db available  :  dw  005 : dw 001,138  ;parralax scroller
   db available  :  dw  079 : dw 056,140  ;yellow switch

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv014: equ $-objectlistlv014
  

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    001      :     db 32-2
dw    000         ;defines the end of the list COMPULSORY!!
 