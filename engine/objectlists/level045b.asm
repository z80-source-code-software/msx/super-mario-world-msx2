objectlistlv045b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  007    : dw 037,162  ;tube cover up exit (upwards) 


  db available    :  dw  100 : dw 046,257  ;porcupuffer

  db available  :  dw  138 : dw 048+1,104  ;water (mario swimming)
  db force00    :  dw  139 : dw 048,105  ;water coverupsprite


 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv045b: equ $-objectlistlv045b

