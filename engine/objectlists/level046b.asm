objectlistlv046b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

db available  :  dw  001 : dw 048+2,104  ;water (mario swimming)

  db force00   :  dw  028 : dw 031-3+4,007  ;endflag bar
  db available :  dw  028+1 : dw 031-3+1+1,006  ;endflag top   
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv046b: equ $-objectlistlv046b

