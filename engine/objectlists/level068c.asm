objectlistlv068c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 002	:	dw 252,289	;greenmushroom (static)
	db available	:	dw 008	:	dw 239,104	;water (marioswim)
	db available	:	dw 017	:	dw 083,091	;red flying koopa
	db available	:	dw 029	:	dw 110,028	;flower box
	db available	:	dw 044	:	dw 231,178	;keyhole
	db available	:	dw 047	:	dw 136,091	;red flying koopa
	db available	:	dw 055	:	dw 252,289	;greenmushroom (static)
	db available	:	dw 058	:	dw 252,289	;greenmushroom (static)
	
	
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv068c: equ $-objectlistlv068c
