objectlistlv047c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
 ; db force18    :  dw  020 : dw 012,134  ;giant door

 ;   db available :  dw  006 : dw 023,124  ;climbing koopa (green vertical)

    db available :  dw  018 : dw 029,124  ;climbing koopa (green vertical)


 db available :  dw  007 : dw 049,125  ;climbing koopa (red horizontal)
 db available :  dw  006 : dw 058,125  ;climbing koopa (red horizontal)

 ;   db available :  dw  024 : dw 059,124  ;climbing koopa (green vertical)

    db available :  dw  052 : dw 065,124  ;climbing koopa (green vertical)
    db available :  dw  040 : dw 067,124  ;climbing koopa (green vertical)

 db available :  dw  015 : dw 078,125  ;climbing koopa (red horizontal)

    db available :  dw  007 : dw 090,124  ;climbing koopa (green vertical)
    db available :  dw  013 : dw 091,124  ;climbing koopa (green vertical)

 db available :  dw  014 : dw 102,125  ;climbing koopa (red horizontal)

  db available  :  dw  054 : dw 111,029  ;questionmark animation (feather) 

   db available :  dw  020 : dw 121,123  ;climbing koopa (green horizontal)

  db available  :  dw  017 : dw 140,122  ;fence support object for marioclimbing
  db available  :  dw  017 : dw 141,103  ;lava (object that kills mario when he hits the lava)

  db available    :  dw  020 : dw 142,368  ;giant door (super special hack) ;yes this is very bad coding practice but do I care?? 
  
 
  db  00        :  dw  999 : dw 999,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv047c: equ $-objectlistlv047c

