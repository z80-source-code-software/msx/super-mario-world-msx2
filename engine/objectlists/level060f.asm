objectlistlv060f:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 035 : dw 044,312	;Dino Rhino
;	db available	|	dw 043	|	dw 043,313	;Dino Rhino (small)
	db available : dw 051 : dw 046,313	;Dino Rhino (small)
	db available : dw 061 : dw 036,055	;yoshi coin
	db available : dw 062 : dw 048,043	;powerpopup (shroom)
	db available : dw 075 : dw 045,313	;Dino Rhino (small)
	db available : dw 081 : dw 042,313	;Dino Rhino (small)
	db available : dw 089 : dw 045,313	;Dino Rhino (small)
	db available : dw 100 : dw 047,312	;Dino Rhino
	db available : dw 120 : dw 044,036	;Mario exit (up down)

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060f: equ $-objectlistlv060f
