objectlistlv029:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db  available :  dw  003 : dw 015,217  ;sheer stairs (helper object)
  
  db  available :  dw  019 : dw 022,218  ;twhimp
  db  available :  dw  034 : dw 022,218  ;twhimp
  
  db  available :  dw  044 : dw 048,219  ;blockwall (standingon)
  
  db  available :  dw  045 : dw 042-13,220  ;ball n chain
  
  db force00   :  dw  048-1 : dw 008,054  ;tubeexit to the right   
  
  db  available :  dw  057 : dw 017,220  ;ball n chain
  db  available :  dw  069 : dw 005,220  ;ball n chain

  db force25  :  dw  088 : dw 008,129  ;door
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv029: equ $-objectlistlv029

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    046*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!   