objectlistlv060c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 023 : dw 042,057	;coin ? box
	db available : dw 044 : dw 041,002	;Rex
	db available : dw 048 : dw 036,002	;Rex
	db available : dw 052 : dw 030,002	;Rex
	db available : dw 055 : dw 041,002	;Rex
	db available : dw 089 : dw 049,205	;spring
	db available : dw 114 : dw 047,002	;Rex
	db available : dw 123 : dw 040,002	;Rex
	db available : dw 131 : dw 036,002	;Rex
	db available : dw 142 : dw 036,002	;Rex
	db available : dw 155 : dw 051,008	;Piranjaplant
	db available : dw 170 : dw 033,055	;yoshi coin
	db available : dw 192 : dw 037,057	;coin ? box
	db available : dw 194 : dw 043,002	;Rex
	db available : dw 195 : dw 034,057	;coin ? box
	db available : dw 200 : dw 039,002	;Rex
	db available : dw 203 : dw 032,027	;red shroom box
	db available : dw 206 : dw 047,002	;Rex
	db available : dw 210 : dw 047,002	;Rex
	db available : dw 217 : dw 043,036	;Mario exit (up down)
  
  
  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060c: equ $-objectlistlv060c
