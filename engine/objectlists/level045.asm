objectlistlv045:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
  db available :  dw  024 : dw 040,071  ;montey mole (jumping from ground)

  db available   :  dw  032 : dw 029,019  ;sliding guy


  db available :  dw  037 : dw 040,071  ;montey mole (jumping from ground)

  db available :  dw  060 : dw 032,057  ;questionmark animation (coin)
  db available :  dw  062 : dw 032,057  ;questionmark animation (coin)
  db available :  dw  064 : dw 032,057  ;questionmark animation (coin)
  db available :  dw  065 : dw 024,029  ;questionmark animation (feather)
  db available :  dw  066 : dw 032,057  ;questionmark animation (coin)
  db available :  dw  067 : dw 029,288  ;sumokoopa  
  db available :  dw  068 : dw 032,057  ;questionmark animation (coin)
  db available :  dw  070 : dw 032,057  ;questionmark animation (coin)

  db available :  dw  080 : dw 040,071  ;montey mole (jumping from ground)

  db available :  dw  123 : dw 022,055  ;yoshicoin 

  db available   :  dw  124 : dw 027,019  ;sliding guy
  db available   :  dw  130 : dw 029,019  ;sliding guy
  db available   :  dw  136 : dw 031,019  ;sliding guy

  db available :  dw  140 : dw 040,001  ;pink shell

  db available :  dw  156 : dw 028,072  ;montey mole (follow)
  db available :  dw  157 : dw 037,072  ;montey mole (follow)
  db available :  dw  162 : dw 027,072  ;montey mole (follow)
  db available :  dw  163 : dw 039,072  ;montey mole (follow)

  db available :  dw  166 : dw 040,071  ;montey mole (jumping from ground)
;  db available |  dw  167 | dw 032,071  ;montey mole (jumping from ground)
  db available :  dw  168 : dw 024,071  ;montey mole (jumping from ground)
;  db available |  dw  173 | dw 035,072  ;montey mole (follow)

  db available :  dw  204 : dw 042,033  ;exclamationmark (red)
  db available :  dw  206 : dw 042,033  ;exclamationmark (red)

  db available :  dw  217 : dw 040,071  ;montey mole (jumping from ground)

  db available :  dw  222 : dw 033,070  ;montey mole (jumping)
  db available :  dw  225 : dw 038,070  ;montey mole (jumping)
  db available :  dw  229 : dw 038,070  ;montey mole (jumping)
  db available :  dw  230 : dw 033,072  ;montey mole (follow)

;  db available |  dw  234 | dw 040,071  ;montey mole (jumping from ground)

  db available   :  dw  237 : dw 036,008  ;Jumpin Piranha Plant

   db available  :  dw  260 : dw 026,251  ;music note block
   db available  :  dw  262 : dw 026,251  ;music note block
   db available  :  dw  264 : dw 026,251  ;music note block

  db available :  dw  265 : dw 028,055  ;yoshicoin    
   
   db available  :  dw  266 : dw 026,251  ;music note block

  db available :  dw  267 : dw 023,288  ;sumokoopa    
   
   db available  :  dw  268 : dw 026,251  ;music note block
   db available  :  dw  270 : dw 026,251  ;music note block
   db available  :  dw  272 : dw 026,251  ;music note block
   db available  :  dw  274 : dw 026,251  ;music note block

  db available :  dw  275 : dw 023,288  ;sumokoopa   
   
   db available  :  dw  276 : dw 026,251  ;music note block
   db available  :  dw  278 : dw 026,251  ;music note block

  db available :  dw  279 : dw 044,072  ;montey mole (follow)
   
   db available  :  dw  280 : dw 026,251  ;music note block
   db available  :  dw  282 : dw 026,251  ;music note block

  db available   :  dw  307 : dw 038,008  ;Jumpin Piranha Plant

  db available :  dw  321 : dw 018,072  ;montey mole (follow) 
  db available :  dw  322 : dw 044,070  ;montey mole (jumping) 
  db available :  dw  326 : dw 027,072  ;montey mole (follow) 
  db available :  dw  328    : dw 008,042    ;smileybox (extra life)
  db available :  dw  338 : dw 017,070  ;montey mole (jumping)
  db available :  dw  339 : dw 036,072  ;montey mole (follow) 

  db force18   :  dw  347-2 : dw 022-2,004  ;checkpoint top
  db available :  dw  347+2-2 : dw 022+3-2,005  ;checkpoint bar
  db force04   :  dw  347+5-2 : dw 022+2-2,031  ;checkpoint pole right 
  
  db available :  dw  356 : dw 034,074  ;smiley box (climbing plant)  
  db available :  dw  359 : dw 005,055  ;yoshicoin  

  db force00   :  dw  375+1 : dw 036-1,036  ;tube cover up exit sprite

  db available :  dw  387 : dw 040,071  ;montey mole (jumping from ground)

   db available  :  dw  390 : dw 034,251  ;music note block
     db available :  dw  391 : dw 042,033  ;exclamationmark (red)
  db available :  dw  392 : dw 034,058  ;questionmark animation (green yoshi egg)
       db available :  dw  393 : dw 042,033  ;exclamationmark (red)   
   db available  :  dw  394 : dw 034,251  ;music note block


  db available :  dw  401 : dw 040,071  ;montey mole (jumping from ground)

  db available :  dw  411 : dw 038,072  ;montey mole (follow) 
  db available :  dw  422 : dw 030,072  ;montey mole (follow) 
  db available :  dw  423 : dw 021,055  ;yoshicoin 
  db available :  dw  424 : dw 036,072  ;montey mole (follow) 
  db available :  dw  429 : dw 040,071  ;montey mole (jumping from ground)

  db available :  dw  437 : dw 038,072  ;montey mole (follow) 

      db available :  dw  493 : dw 040,196  ;changing powerup 

  db available :  dw  500 : dw 034,055  ;yoshicoin

  db available   :  dw  525 : dw 038,290  ;JumpinPiranhaPlant (fireversion)

  db available :  dw  546 : dw 042,034  ;exclamationmark (green)
  db available :  dw  548 : dw 042,034  ;exclamationmark (green)

     db available :  dw  558 : dw 042,033  ;exclamationmark (red)
     db available :  dw  560 : dw 042,033  ;exclamationmark (red)

  db available :  dw  574    : dw 034,280    ;smileybox (feather) 

  db available :  dw  578 : dw 031,288  ;sumokoopa
  db available :  dw  585 : dw 023,288  ;sumokoopa


  db available :  dw  614 : dw 038,026  ;snapping chuck

  db force00   :  dw  634 : dw 024-3+4,007  ;endflag bar
  db available :  dw  634+1 : dw 024-3+1+1,006  ;endflag top 

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv045: equ $-objectlistlv045

