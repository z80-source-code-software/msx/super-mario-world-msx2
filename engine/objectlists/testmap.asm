objectlisttestmap:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
        db available    :       dw 003  :       dw 040,129      ;door
        db available    :       dw 009  :       dw 035,272      ;red mushroom (static)
        db available    :       dw 011  :       dw 042,164      ;feather bonus (static)
        db available    :       dw 012  :       dw 028,058      ;yoshi egg ? box
        db available    :       dw 015  :       dw 042,351      ;Flower (static)
        db available    :       dw 020  :       dw 042,046      ;Flower (static)



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlisttestmap: equ $-objectlisttestmap


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    01*8      :     db 32
dw    000         ;defines the end of the list COMPULSORY!!  


