objectlistlv027:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

    db available  :  dw  010 : dw 047,208  ;ice (helper object)
    
    db available  :  dw  022 : dw 047,183  ;questionmark box (coin tail)
 
    db available  :  dw  038 : dw 034-2,063  ;green koopa 

  db available :  dw  043 : dw 032,055  ;yoshicoin
    
    db available  :  dw  064 : dw 040,205  ;spring 
    
  db available :  dw  065 : dw 034,074  ;smiley box (climbing plant)    

    db available  :  dw  070 : dw 013,207  ;vine separator (martio helper object)
    
    db available  :  dw  073 : dw 013,206  ;smiley box (star)

  db available :  dw  083 : dw 017,055  ;yoshicoin

  db available :  dw  086 : dw 022,078  ;coin (static) 
  db available :  dw  087 : dw 024,078  ;coin (static) 

  db available   :  dw  104 : dw 47,174  ;spiketop
  db available   :  dw  115 : dw 47,174  ;spiketop

  db available :  dw  116 : dw 039,055  ;yoshicoin 

  db available   :  dw  128 : dw 35,174  ;spiketop

  db available :  dw  155 : dw 039,055  ;yoshicoin

  db available :  dw  177 : dw 040,009  ;red flying koopa (up and down)   

  db available   :  dw  215 : dw 47,174  ;spiketop
  db available   :  dw  237 : dw 47,174  ;spiketop

    db available  :  dw  270 : dw 047,209  ;muncher
  db available   :  dw  273 : dw 041,008  ;Jumpin Piranha Plant    
    
    db available  :  dw  278 : dw 047,209  ;muncher
  db available   :  dw  283 : dw 043,008  ;Jumpin Piranha Plant     
    db available  :  dw  288 : dw 047,209  ;muncher
    db available  :  dw  291 : dw 047,209  ;muncher

 db available :  dw  300+1    : dw 030,162  ;tube cover up exit (upwards)

  db available   :  dw  319 : dw 47,174  ;spiketop
  db available   :  dw  321 : dw 29,174  ;spiketop


  db available :  dw  326 : dw 024,055  ;yoshicoin 
  
  db available   :  dw  331 : dw 29,174  ;spiketop

  db available :  dw  343 : dw 040,009  ;red flying koopa (up and down) 

    db available  :  dw  360 : dw 047,208  ;ice (helper object)

    db available  :  dw  381 : dw 045,063  ;green koopa 

  db available   :  dw  390 : dw 47,174  ;spiketop
  db available   :  dw  397 : dw 47,174  ;spiketop

  db available :  dw  413 : dw 030,078  ;coin (static)
  db available :  dw  417 : dw 030,078  ;coin (static)

  db available :  dw  433 : dw 028,009  ;red flying koopa (up and down)
  db available :  dw  434 : dw 040,009  ;red flying koopa (up and down)

  db force00   :  dw  460 : dw 031,007  ;endflag bar
  db force28   :  dw  460+1 : dw 031-1,006  ;endflag top
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv027: equ $-objectlistlv027


  
 