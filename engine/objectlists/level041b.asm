objectlistlv041b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
   
  db force18   :  dw  020 : dw 021-3,004  ;checkpoint top
  db available :  dw  020+2 : dw 021,005  ;checkpoint bar
  db force04   :  dw  020+5 : dw 021-1,031  ;checkpoint pole right
  
  
    db available :  dw  043    : dw 019,042    ;smileybox (extra life)
    
    db force26  :  dw  056 : dw 024,129  ;door   
    
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv041b: equ $-objectlistlv041b

