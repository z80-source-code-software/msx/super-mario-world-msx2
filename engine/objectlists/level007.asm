objectlistlv007:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  


  db available   :  dw  024 : dw 007,007  ;endflag bar
  db available   :  dw  024+1 : dw 007-2,006  ;endflag top


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv007: equ $-objectlistlv007
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    1*8      :     db 32
dw    000         ;defines the end of the list COMPULSORY!!  