objectlistlv064:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
  
	db available : dw 033 : dw 048,322	;Ghosthole
	db available : dw 044 : dw 041,214	;eerie (left right)
	db available : dw 048 : dw 045,214	;eerie (left right)
	db available : dw 110 : dw 048,322	;Ghosthole
	db available : dw 119 : dw 041,324	;Eerie (high angle)
	db available : dw 122 : dw 044,324	;Eerie (high angle)
	db available : dw 139 : dw 048,322	;Ghosthole
	db available : dw 150 : dw 043,214	;eerie (left right)
	db available : dw 168 : dw 048,322	;Ghosthole
	db force21      : dw 179 : dw 036,323	;Fishin boo
	db available : dw 190 : dw 043,324	;Eerie (high angle)
	db available : dw 192 : dw 034,324	;Eerie (high angle)
	db available : dw 223 : dw 048,324	;Eerie (high angle)
	db available : dw 266 : dw 040,324	;Eerie (high angle)
	db available : dw 276 : dw 043,027	;red shroom box
	db available : dw 295 : dw 039,324	;Eerie (high angle)
	db available : dw 298 : dw 042,324	;Eerie (high angle)
	db available : dw 331 : dw 048,322	;Ghosthole
	db available : dw 346 : dw 043,324	;Eerie (high angle)
	db available : dw 350 : dw 046,324	;Eerie (high angle)
	db available : dw 360 : dw 048,322	;Ghosthole
	db available : dw 389 : dw 048,322	;Ghosthole
	db available : dw 399 : dw 043,324	;Eerie (high angle)
	db available : dw 402 : dw 046,324	;Eerie (high angle)
	db available : dw 414 : dw 044,129	;door
	db available : dw 418 : dw 048,322	;Ghosthole





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv064: equ $-objectlistlv064
