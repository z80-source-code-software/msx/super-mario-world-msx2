objectlistlv061:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
	db available : dw 033 : dw 029,186	;rotating platform (permanent)
	db available : dw 035 : dw 033,189	;fuzzy
	db available : dw 069 : dw 026,186	;rotating platform (permanent)
	db available : dw 085 : dw 029,055	;yoshi coin
	db available : dw 111 : dw 034,186	;rotating platform (permanent)
	db available : dw 121 : dw 028,186	;rotating platform (permanent)
	db available : dw 123 : dw 032,189	;fuzzy
	db available : dw 134 : dw 024,186	;rotating platform (permanent)
	db available : dw 136 : dw 028,189	;fuzzy
	db available : dw 163 : dw 036,029	;feather box
	db available : dw 169 : dw 039,061	;pink koopa
	db available : dw 195 : dw 034,186	;rotating platform (permanent)
	db available : dw 197 : dw 038,189	;fuzzy
	db available : dw 211 : dw 031,186	;rotating platform (permanent)
	db available : dw 212 : dw 043,055	;yoshi coin
	db available : dw 213 : dw 035,189	;fuzzy
	db available : dw 227 : dw 032,186	;rotating platform (permanent)
	db available : dw 229 : dw 036,189	;fuzzy
	db available : dw 234 : dw 026,162	;exit (upwards)
	db available : dw 248 : dw 033,186	;rotating platform (permanent)
	db available : dw 250 : dw 037,189	;fuzzy
	db available : dw 270 : dw 033,186	;rotating platform (permanent)
	db available : dw 272 : dw 037,189	;fuzzy
	db available : dw 306 : dw 040,064	;blue koopa
	db available : dw 313 : dw 034,029	;feather box
	db available : dw 320 : dw 034,004	;Checkpointtop
	db available : dw 322 : dw 037,005	;Checkpointbar
	db available : dw 325 : dw 036,031	;checkpointright
	db available : dw 337 : dw 033,186	;rotating platform (permanent)
	db available : dw 351 : dw 031,186	;rotating platform (permanent)
	db available : dw 363 : dw 030,186	;rotating platform (permanent)
	db available : dw 365 : dw 034,189	;fuzzy
	db available : dw 384 : dw 023,102	;ultracoin (static)
	db available : dw 386 : dw 040,064	;blue koopa
	db available : dw 419 : dw 032,186	;rotating platform (permanent)
	db available : dw 421 : dw 036,189	;fuzzy
	db available : dw 431 : dw 033,091	;red flying koopa
	db available : dw 434 : dw 037,091	;red flying koopa
	db available : dw 439 : dw 032,186	;rotating platform (permanent)
	db available : dw 441 : dw 036,189	;fuzzy
	db available : dw 455 : dw 034,034	;green !
	db available : dw 461 : dw 039,055	;yoshi coin
	db available : dw 478 : dw 032,186	;rotating platform (permanent)
	db available : dw 480 : dw 036,189	;fuzzy
	db available : dw 493 : dw 031,055	;yoshi coin
	db available : dw 494 : dw 035,096	;Special green star box
	db available : dw 505 : dw 035,074	;Smileybox (climbing plant)
	db available : dw 511 : dw 026,091	;red flying koopa
	db available : dw 519 : dw 002,007	;Endflagbar
	db available : dw 520 : dw 001,006	;Endflagtop
	db available : dw 521 : dw 039,064	;blue koopa
	db available : dw 586 : dw 036,042	;smileybox (extra life)
	db available : dw 593 : dw 036,042	;smileybox (extra life)
	db available : dw 600 : dw 036,042	;smileybox (extra life)
	db available : dw 604 : dw 026,007	;Endflagbar
	db available : dw 605 : dw 025,006	;Endflagtop




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv061: equ $-objectlistlv061
