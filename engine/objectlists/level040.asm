objectlistlv040:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

 ; db available  |  dw  001 | dw 026+1,153  ;water (mario swimming)  
 
  
  db  available :  dw  041-1 : dw 014-7,220  ;ball n chain
  
  db  available :  dw  054 : dw 018,262  ;deadfish  
  db  available :  dw  063 : dw 010,262  ;deadfish  
  
  
  db  available :  dw  072-1 : dw 010-7,220  ;ball n chain
  
  db  available :  dw  091 : dw 006,262  ;deadfish   
  
  db  available :  dw  097-1 : dw 001,220  ;ball n chain


;  db  available |  dw  107 | dw 022,262  ;deadfish ;no more sprites

  db force00   :  dw  109-1 : dw 020,054  ;tubeexit to the right 


  
  db  available :  dw  138 : dw 008,262  ;deadfish 
  
        db available :  dw  153 : dw 014,029  ;questionmark animation (feather)  
  
  
  db  available :  dw  172 : dw 017,262  ;deadfish 
  db  available :  dw  184 : dw 014,262  ;deadfish 

  db  available :  dw  188 : dw 009,263  ;spikedown  

  db  available :  dw  191 : dw 022,264  ;bony beetle
  
  db  available :  dw  202 : dw 009,263  ;spikedown
  
  db  available :  dw  203 : dw 017,262  ;deadfish   
  
  db  available :  dw  204 : dw 009,263  ;spikedown
  db  available :  dw  214 : dw 009,263  ;spikedown

  db  available :  dw  216 : dw 022,264  ;bony beetle  
  
  db  available :  dw  220 : dw 009,263  ;spikedown

  db  available :  dw  221 : dw 020,046  ;dry bones (trowing variant)
  
  db  available :  dw  227 : dw 018,262  ;deadfish   
  
  db  available :  dw  230 : dw 009,263  ;spikedown


  db  available :  dw  237 : dw 014,262  ;deadfish 

  db force00   :  dw  245-1 : dw 011,054  ;tubeexit to the right
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv040: equ $-objectlistlv040

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    009*8      :     db 32
dw    107*8      :     db 64
dw    243*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!


