objectlistlv043b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  002    : dw 028,282    ;square flattened platform (horizontal one way)
  db available :  dw  015    : dw 026,283    ;ridable cloud


  db available :  dw  011 : dw 026,078  ;coin (static)
  db available :  dw  013 : dw 026,078  ;coin (static)
  db available :  dw  027 : dw 017,078  ;coin (static)
  db available :  dw  030 : dw 017,078  ;coin (static)
  db available :  dw  043 : dw 026,078  ;coin (static)
  db available :  dw  045 : dw 026,078  ;coin (static)
  db available :  dw  051 : dw 017,078  ;coin (static)
  db available :  dw  053 : dw 017,078  ;coin (static)
  db available :  dw  056 : dw 017,078  ;coin (static)
  db available :  dw  057 : dw 026,078  ;coin (static)
  db available :  dw  060 : dw 021,078  ;coin (static)
  db available :  dw  067 : dw 026,078  ;coin (static)
  db available :  dw  068 : dw 017,078  ;coin (static)

  db available :  dw  080 : dw 010,055  ;yoshicoin

  db available :  dw  095 : dw 017,078  ;coin (static)
  db available :  dw  096 : dw 026,078  ;coin (static)
  db available :  dw  097 : dw 017,078  ;coin (static)
  db available :  dw  098 : dw 026,078  ;coin (static)
  db available :  dw  103 : dw 021,078  ;coin (static)
  db available :  dw  109 : dw 026,078  ;coin (static)


  db available :  dw  123    : dw 017,162  ;tube cover up exit (upwards)


 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv043b: equ $-objectlistlv043b

