objectlistlv065c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 015 : dw 040,327	;Special yellow pillar
	db available : dw 023 : dw 044,174	;spiketop
	db available : dw 027 : dw 040,174	;spiketop
	db available : dw 031 : dw 036,174	;spiketop
	db available : dw 037 : dw 032,174	;spiketop
	db available : dw 050 : dw 035,328	;Special yellow pillar (helper platform)
	db available : dw 070 : dw 032,174	;spiketop
	db available : dw 081 : dw 040,174	;spiketop
	db available : dw 093 : dw 032,174	;spiketop
	db available : dw 098 : dw 032,174	;spiketop
	db available : dw 105 : dw 040,174	;spiketop
	db available : dw 112 : dw 032,174	;spiketop
	db available : dw 119 : dw 040,174	;spiketop
	db available : dw 135 : dw 032,174	;spiketop
	db available : dw 142 : dw 038,328	;Special yellow pillar (helper platform)
	db available : dw 152 : dw 038,328	;Special yellow pillar (helper platform)
	db available : dw 166 : dw 036,328	;Special yellow pillar (helper platform)
	db available : dw 180 : dw 034,328	;Special yellow pillar (helper platform)
	db available : dw 196 : dw 032,328	;Special yellow pillar (helper platform)
	db available : dw 208 : dw 030,328	;Special yellow pillar (helper platform)
	db available : dw 226 : dw 026,328	;Special yellow pillar (helper platform)
	db available : dw 260 : dw 040,028	;flower box
	db available : dw 263 : dw 045,237	;mini chargin chuck
	db available : dw 282 : dw 037,237	;mini chargin chuck
	db available : dw 302 : dw 029,237	;mini chargin chuck
	db available : dw 320 : dw 036,237	;mini chargin chuck
	db available : dw 346 : dw 036,328	;Special yellow pillar (helper platform)
	db available : dw 360 : dw 034,328	;Special yellow pillar (helper platform)
	db available : dw 374 : dw 036,328	;Special yellow pillar (helper platform)
	db available : dw 388 : dw 034,328	;Special yellow pillar (helper platform)
	db available : dw 406 : dw 026,328	;Special yellow pillar (helper platform)
	db available : dw 424 : dw 030,328	;Special yellow pillar (helper platform)
	db available : dw 461 : dw 033,054	;mario exit (right)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv065c: equ $-objectlistlv065c

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    460*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 
