objectlistlv012:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  
;=========
;  db available |  dw  020 | dw 042,001  ;tube cover up exit sprite ;test
;  db available |  dw  022 | dw 042,076  ;tube cover up exit sprite ;test
;  db available |  dw  025 | dw 044,036  ;tube cover up exit sprite ;test
;  db available |  dw  030 | dw 035,162  ;tube cover up exit (upwards)
;=========
  db available :  dw  055    : dw 044-1,154    ;superkoopa (walking)

  db available :  dw  070    : dw 040-1,156    ;superkoopa (walking no feather)
  db available :  dw  081    : dw 036-1,156    ;superkoopa (walking no feather)

  db available :  dw  92     : dw 044-1,043  ;powerpopup (red mushroom)

  db available :  dw  110    : dw 023,157    ;superkoopa (flying in curve)

  db available :  dw  120    : dw 043-1,160    ;chuck (baseball)

  db available :  dw  150    : dw 038-1,156    ;superkoopa (walking no feather)
  db available :  dw  161    : dw 044-1,154    ;superkoopa (walking)

  db available :  dw  177    : dw 036-1,158    ;spuugplant

  db available :  dw  206    : dw 044-4,093  ;green flying koopa
  db available :  dw  211    : dw 034-1,156    ;superkoopa (walking no feather)

  db available :  dw  217    : dw 044-4,093  ;green flying koopa

  db available :  dw  240    : dw 045,032    ;exclamationmark (yellow)
  
  db available :  dw  242    : dw 037,162  ;tube cover up exit (upwards)
  
  db available :  dw  242    : dw 045,032    ;exclamationmark (yellow)
  db available :  dw  244    : dw 045,032    ;exclamationmark (yellow)
  db available :  dw  246    : dw 045,032    ;exclamationmark (yellow)

  db available :  dw  255    : dw 044-1,154    ;superkoopa (walking)

;  db force21   |  dw  257   | dw 042-3,009  ;tube cover up (goes in front of Jumpin Piranha Plant)
;  db force27   |  dw  257+1 | dw 042-3,008  ;Jumpin Piranha Plant

  db available :  dw  264    : dw 043-1,160    ;chuck (baseball)
  
  ;add the rest including the midpole
  db force18   :  dw  315 : dw 038-3,004  ;checkpoint top
  db available :  dw  317 : dw 041-3,005  ;checkpoint bar
  db force04   :  dw  320 : dw 040-3,031  ;checkpoint pole right
  
  db available :  dw  338    : dw 018,157    ;superkoopa (flying in curve) 

  db available :  dw  343     : dw 044-1,043  ;powerpopup (red mushroom)

  db available :  dw  357    : dw 044-1,156    ;superkoopa (walking no feather)
  db available :  dw  363    : dw 044-1,156    ;superkoopa (walking no feather)
  db available :  dw  369    : dw 038-1,156    ;superkoopa (walking no feather)

  db available :  dw  376+1 : dw 039-2,036  ;tube cover up exit sprite 
  
  db available :  dw  405    : dw 023,157    ;superkoopa (flying in curve)

  db available :  dw  415    : dw 044-1,154    ;superkoopa (walking)


  db available :  dw  422    : dw 038-1,158    ;spuugplant
  
  db available :  dw  436    : dw 027,057  ;questionmark animation (coin)
  
  db available :  dw  447    : dw 032-1,156    ;superkoopa (walking no feather)

  db available :  dw  463    : dw 043-1,160    ;chuck (baseball)  

  db available :  dw  488    : dw 038-1,158    ;spuugplant

  db available :  dw  509    : dw 037-1,160    ;chuck (baseball)

  db available :  dw  528    : dw 044-1,154    ;superkoopa (walking)  
  db available :  dw  532    : dw 040-1,156    ;superkoopa (walking no feather)
  db available :  dw  535    : dw 036-1,156    ;superkoopa (walking no feather)
  db available :  dw  542    : dw 033-1,156    ;superkoopa (walking no feather)

  db available :  dw  561 : dw 035,034  ;exclamationmark (green)

   db available :  dw  577 : dw 010,178  ;keyhole
  
  db available :  dw  582    : dw 033,032    ;exclamationmark (yellow)
  
  db available :  dw  583    : dw 032-1,158    ;spuugplant  
  
  db available :  dw  589    : dw 036,032    ;exclamationmark (yellow)
  db available :  dw  591    : dw 036,032    ;exclamationmark (yellow)


  db available :  dw  600    : dw 043-1,160    ;chuck (baseball)

  db force00   :  dw  603 : dw 033-3,007  ;endflag bar
  db available :  dw  604 : dw 029-3,006  ;endflag top


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv012: equ $-objectlistlv012
  
level12bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4
  
    dw 23576+bgrmapaddr :   db      049, 050, 062, 062 ;calculate adress = (y * mapwidth) + x
    dw 24203+bgrmapaddr :   db      067, 000, 067, 000 
    dw 25547+bgrmapaddr :   db      000, 000, 000, 047 
    dw 25549+bgrmapaddr :   db      000, 047, 048, 062 
    dw 000             :   db      000, 000, 000, 000
lenghtlevel12bkgtiles: equ $-level12bkgtiles 