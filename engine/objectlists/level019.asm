objectlistlv019:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
    db available  :  dw  003 : dw 030,130  ;static camerascroller
    db available  :  dw  004 : dw 040,152  ;blocktileanimator
    db available  :  dw  005 : dw 040,168  ;mariocrusher
  
  
  db available  :  dw  019-1 : dw 046-4,149  ;mediumsheertile (left)

  
      db available :  dw  027 : dw 034,057  ;questionmark animation (coin)
      
      db available :  dw  029 : dw 034,119  ;multicoin box
      db available :  dw  031 : dw 034,057  ;questionmark animation (coin)

    db available :  dw  032 : dw 028,170  ;bat (flying turd)      
      
  db available :  dw  33    : dw 034,032    ;exclamationmark (yellow)
  
    db available :  dw  034 : dw 041-2,169  ;cavekoopa
  
  
  db available  :  dw  035-1 : dw 042-4,150  ;mediumsheertile (left) (short segment) 1/2
  
    db available :  dw  048 : dw 038-8,170  ;bat (flying turd)

    db available :  dw  055 : dw 035-2,169  ;cavekoopa

    db available :  dw  061 : dw 030,170  ;bat (flying turd)   
  
  db available  :  dw  064 : dw 036-2,151  ;yellowpillar (down)

  db available   :  dw  071 : dw 021,055  ;yoshi coin

    db available :  dw  072+2 : dw 030,169  ;cavekoopa
  
  
  db available  :  dw  088 : dw 036-2-1,115  ;slightsheertile (left)
  
      db available :  dw  093 : dw 025-6,170  ;bat (flying turd)
  
  
  db available  :  dw  096 : dw 034-2-1,115  ;slightsheertile (left)
  
        db available :  dw  104 : dw 024,029  ;questionmark animation (feather)
  
    db available  :  dw  108 : dw 030-2,151  ;yellowpillar (up)
        db available  :  dw  119 : dw 022,119  ;multicoin box
    
    db available  :  dw  132 : dw 032-2,151  ;yellowpillar (down)
    
         db available :  dw  144 : dw 012,170  ;bat (flying turd)
         db available  :  dw  149 : dw 028-2,169  ;cavekoopa
         db available :  dw  155 : dw 012,170  ;bat (flying turd)
    
    db available  :  dw  156 : dw 035-2,151  ;yellowpillar (down)
         db available :  dw  164 : dw 012,170  ;bat (flying turd) 
  db available :  dw  173 : dw 027,034  ;exclamationmark (green)            
         db available :  dw  174 : dw 012,170  ;bat (flying turd)
         db available :  dw  178 : dw 012,170  ;bat (flying turd)          
    
    db available  :  dw  181 : dw 035-2,151  ;yellowpillar (down)
         db available :  dw  182 : dw 012,170  ;bat (flying turd)  
         
  db available :  dw  187    : dw 015,162  ;tube cover up exit (upwards)         
           
         db available :  dw  201 : dw 012,170  ;bat (flying turd)

  db available   :  dw  203 : dw 026,055  ;yoshi coin         
             
    db available  :  dw  205 : dw 034-2,151  ;yellowpillar (down)
  
    db available :  dw  225 : dw 022,170  ;bat (flying turd)   
  
  db available   :  dw  232 : dw 027,055  ;yoshi coin  
  
     db available  :  dw  233 : dw 033-2,169  ;cavekoopa 
 
    db available :  dw  240 : dw 022,170  ;bat (flying turd)  
    db available :  dw  245 : dw 022,170  ;bat (flying turd)  
 
     db available  :  dw  247 : dw 033-2,169  ;cavekoopa  

    db available :  dw  255 : dw 022,170  ;bat (flying turd) 
  
   db available  :  dw  258-1 : dw 034-4,150  ;mediumsheertile (left) (short segment) 1/2 
   db available  :  dw  258+3 : dw 034-3,172  ;mediumsheertile (right) (short segment) 1/2 
   
       db available  :  dw  266 : dw 026-2,151  ;yellowpillar (up)
   
   db available  :  dw  273-1 : dw 034-4,150  ;mediumsheertile (left) (short segment) 1/2
   db available  :  dw  273+3 : dw 034-3,172  ;mediumsheertile (right) (short segment) 1/2     


    db available :  dw  277 : dw 027,173  ;flying box (feather)
   
          db available  :  dw  278 : dw 026-2,151  ;yellowpillar (up)
           

     db available  :  dw  282 : dw 033-2,169  ;cavekoopa
     
 ;   db available |  dw  286+2 | dw 033-2,174  ;debug object         
     
     
     db available  :  dw  290 : dw 033-2,169  ;cavekoopa

   
   db available  :  dw  292-1 : dw 034-4,150  ;mediumsheertile (left) (short segment) 1/2
   db available  :  dw  292+3 : dw 034-3,172  ;mediumsheertile (right) (short segment) 1/2     
 
 
      db available  :  dw  302 : dw 033-2,169  ;cavekoopa
  
  db available   :  dw  322 : dw 025,055  ;yoshi coin  
  
  
      db available  :  dw  326 : dw 038-2,151  ;yellowpillar (down)


  db available   :  dw  343 : dw 019,055  ;yoshi coin
  
  db available   :  dw  366 : dw 037-1,174  ;spiketop
  db available   :  dw  371 : dw 037-1,174  ;spiketop


  db available    :  dw  372 : dw 034,054  ;tubeexit to the right -1

  db available  :  dw  373 : dw 036,108  ;blockwall for mario (right)


  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv019: equ $-objectlistlv019


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    371*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
  
 
