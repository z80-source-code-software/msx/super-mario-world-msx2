objectliststarworld2:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
	db available	:	dw 006	:	dw 016,351	;Star (static)
	db available	:	dw 013	:	dw 023,153	;water (marioswim)
	db available	:	dw 025	:	dw 046,348	;Permayoshi
	db available	:	dw 033	:	dw 036,203	;bigfish (dumb) (left or right)
	db available	:	dw 041	:	dw 030,203	;bigfish (dumb) (left or right)
	db available	:	dw 044	:	dw 044,203	;bigfish (dumb) (left or right)
	db available	:	dw 066	:	dw 017,199	;rip van fish
	db available	:	dw 079	:	dw 036,199	;rip van fish
	db available	:	dw 094	:	dw 017,199	;rip van fish
	db available	:	dw 100	:	dw 038,199	;rip van fish
	db available	:	dw 120	:	dw 026,203	;bigfish (dumb) (left or right)
	db available	:	dw 128	:	dw 035,203	;bigfish (dumb) (left or right)
	db available	:	dw 137	:	dw 045,203	;bigfish (dumb) (left or right)
	db available	:	dw 150	:	dw 032,028	;flower box
	db available	:	dw 172	:	dw 018,199	;rip van fish
	db available	:	dw 178	:	dw 039,199	;rip van fish
	db available	:	dw 187	:	dw 024,199	;rip van fish
	db available	:	dw 195	:	dw 032,199	;rip van fish
	db available	:	dw 218	:	dw 034,203	;bigfish (dumb) (left or right)
	db available	:	dw 225	:	dw 043,203	;bigfish (dumb) (left or right)
	db available	:	dw 253	:	dw 027,203	;bigfish (dumb) (left or right)
	db available	:	dw 261	:	dw 037,203	;bigfish (dumb) (left or right)
	db available	:	dw 284	:	dw 036,199	;rip van fish
	db available	:	dw 290	:	dw 019,199	;rip van fish
	db available	:	dw 294	:	dw 043,199	;rip van fish
	db available	:	dw 300	:	dw 027,199	;rip van fish
	db available	:	dw 313	:	dw 034,054	;mario exit (right)
	db available	:	dw 379	:	dw 046,178	;keyhole





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectliststarworld2: equ $-objectliststarworld2

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    312*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!! 
