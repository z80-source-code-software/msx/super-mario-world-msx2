objectlistlv026:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

    ;db available  |  dw  023 | dw 026-2,120  ;fish (dumb) (left and right)

  ;db available   |  dw  045 | dw 025,055  ;yoshi coin
 
 ; db available  |  dw  001 | dw 026+1,153  ;water (mario swimming)
 
    db available  :  dw  022 : dw 029,199  ;rip van fish

    
  db available   :  dw  037 : dw 016,078  ;coin (static)    
  db available   :  dw  040 : dw 016,078  ;coin (static)    
    
    db available  :  dw  048 : dw 015,202  ;fish (dumb) (up and down)
     db available  :  dw  055 : dw 029,203  ;bigfish (dumb) (left or right) 

  db available   :  dw  060 : dw 016,078  ;coin (static)    
  db available   :  dw  062 : dw 016,078  ;coin (static) 

    db available  :  dw  080 : dw 027,199  ;rip van fish

  db available :  dw  085 : dw 020,057  ;questionmark animation (coin)
  db available :  dw  087 : dw 020,057  ;questionmark animation (coin)

  db available   :  dw  088 : dw 012,055  ;yoshi coin

  db available  :  dw  089 : dw 020,028  ;questionmark animation (red flower)

    db available  :  dw  090 : dw 018,199  ;rip van fish 
  db available :  dw  091 : dw 020,057  ;questionmark animation (coin)

     db available  :  dw  097 : dw 026,203  ;bigfish (dumb) (left or right)
  db available  :  dw  105 : dw 026,120  ;fish (dumb) (left and right)

  db available :  dw  107    : dw 015,162  ;tube cover up exit (upwards)

    db available  :  dw  121 : dw 023,199  ;rip van fish
     db available  :  dw  133 : dw 022,203  ;bigfish (dumb) (left or right)
     db available  :  dw  140 : dw 026,203  ;bigfish (dumb) (left or right)

    db available  :  dw  153 : dw 014,202  ;fish (dumb) (up and down)
  db available   :  dw  154 : dw 026,078  ;coin (static) 

 ; db available |  dw  157    | dw 011,162  ;tube cover up exit (upwards)

  db available   :  dw  161 : dw 027,055  ;yoshi coin
    db available  :  dw  162 : dw 014,202  ;fish (dumb) (up and down)

    db available  :  dw  172 : dw 023,199  ;rip van fish
  db available  :  dw  174 : dw 025,028  ;questionmark animation (red flower)

    db available  :  dw  184 : dw 025,202  ;fish (dumb) (up and down)
    db available  :  dw  189 : dw 025,202  ;fish (dumb) (up and down)

    db available  :  dw  200 : dw 022-3,199  ;rip van fish

  db available   :  dw  201 : dw 022-1,087  ;blue rotating block

  db available   :  dw  204 : dw 022-2,055  ;yoshi coin  
  
  db available   :  dw  206 : dw 022-1,087  ;blue rotating block

    db available  :  dw  222 : dw 031-2,199  ;rip van fish  

    db available  :  dw  225 : dw 018,120  ;fish (dumb) (left and right)
    db available  :  dw  232 : dw 023,120  ;fish (dumb) (left and right)
 
   db available  :  dw  237 : dw 026,076  ;p-box 
   
     db available  :  dw  246 : dw 024,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  261 : dw 019,203  ;bigfish (dumb) (left or right)   
   
  db available   :  dw  273 : dw 008,055  ;yoshi coin    
  
   db available :  dw  298 : dw 030,178  ;keyhole    
 
  db available :  dw  303 : dw 024,057  ;questionmark animation (coin) 

     db available  :  dw  308 : dw 019,203  ;bigfish (dumb) (left or right) 
 
    db available  :  dw  328 : dw 021,120  ;fish (dumb) (left and right) 
    db available  :  dw  329 : dw 031,120  ;fish (dumb) (left and right) 

  db available   :  dw  336 : dw 007,055  ;yoshi coin    


    db available  :  dw  340 : dw 030,202  ;fish (dumb) (up and down)
 
  db available   :  dw  342 : dw 015,078  ;coin (static)  
  db available   :  dw  346 : dw 015,078  ;coin (static)  
    
    db available  :  dw  347 : dw 029,120  ;fish (dumb) (left and right)  

    db available  :  dw  369 : dw 019,199  ;rip van fish 
    
  db force00   :  dw  380 : dw 013,007  ;endflag bar
  db force28   :  dw  380+1 : dw 013-1,006  ;endflag top    
    
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv026: equ $-objectlistlv026


  
 