objectlistlv052b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


	db available : dw 021 : dw 041,179	;small ghost (many)
	db available : dw 031 : dw 038,179	;small ghost (many)
	db available : dw 039 : dw 040,179	;small ghost (many)
	db available : dw 046 : dw 038,179	;small ghost (many)
	db available : dw 051 : dw 039,041	;Flying box (flower)
	db available : dw 060 : dw 039,179	;small ghost (many)
	db available : dw 069 : dw 039,179	;small ghost (many)
	db available : dw 079 : dw 036,179	;small ghost (many)
	db available : dw 089 : dw 037,179	;small ghost (many)
	db available : dw 094 : dw 037,179	;small ghost (many)
	db available : dw 102 : dw 037,179	;small ghost (many)
	db available : dw 110 : dw 040,029	;feather box
	db available : dw 120 : dw 039,055	;yoshi coin
	db available : dw 126 : dw 038,179	;small ghost (many)
	db available : dw 132 : dw 037,179	;small ghost (many)
	db available : dw 140 : dw 036,179	;small ghost (many)
	db available : dw 145 : dw 035,179	;small ghost (many)
	db available : dw 156 : dw 034,055	;yoshi coin
	db available : dw 166 : dw 029,179	;small ghost (many)
	db available : dw 175 : dw 038,179	;small ghost (many)
	db available : dw 181 : dw 028,179	;small ghost (many)
	db available : dw 188 : dw 028,179	;small ghost (many)
	db available : dw 193 : dw 038,055	;yoshi coin
	db available : dw 200 : dw 044,042	;smileybox (extra life)
	db available : dw 202 : dw 029,179	;small ghost (many)
	db available : dw 207 : dw 030,179	;small ghost (many)
	db available : dw 211 : dw 035,076	;p-box (blue)
	db available : dw 216 : dw 032,179	;small ghost (many)
	db force24      : dw 220 : dw 044,129	;door
	db available : dw 222 : dw 032,179	;small ghost (many)
	db available : dw 226 : dw 041,102	;ultracoin (static)
	db available : dw 231 : dw 033,179	;small ghost (many)
	db available : dw 236 : dw 041,102	;ultracoin (static)
	db available : dw 239 : dw 031,179	;small ghost (many)
	db available : dw 243 : dw 032,179	;small ghost (many)
	db available : dw 246 : dw 041,083	;supercoin (static)
	db force28	    : dw 250 : dw 044,181	;door (pbox appearing)


  
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv052b: equ $-objectlistlv052b

