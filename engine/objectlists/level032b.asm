objectlistlv032b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 



  db force18   :  dw  045 : dw 028-3,004  ;checkpoint top
  db available :  dw  045+2 : dw 028,005  ;checkpoint bar
  db force04   :  dw  045+5 : dw 028-1,031  ;checkpoint pole right

  db available :  dw  055 : dw 047-3,169  ;cavekoopa 

  db available :  dw  060 : dw 032,078  ;coin (static)  
  db available :  dw  062 : dw 034,078  ;coin (static)  
  db available :  dw  063 : dw 036,078  ;coin (static)  
   
  db available :  dw  077 : dw 047-3,169  ;cavekoopa 
  db available :  dw  098 : dw 047-3,169  ;cavekoopa 

  db available :  dw  103 : dw 045,078  ;coin (static)
  db available :  dw  105 : dw 045,078  ;coin (static)
  db available :  dw  107 : dw 045,078  ;coin (static)

  db available :  dw  124 : dw 033-1,087  ;blue rotating block

  db available   :  dw  126 : dw 039,174  ;spiketop
  db available   :  dw  130 : dw 039,174  ;spiketop

  db available :  dw  139 : dw 030,055  ;yoshicoin

  db available   :  dw  145 : dw 041,174  ;spiketop
  db available   :  dw  150 : dw 041,174  ;spiketop
  db available   :  dw  155 : dw 041,174  ;spiketop

  db available :  dw  156 : dw 035-3,169  ;cavekoopa 

  db available :  dw  170 : dw 041,057  ;questionmark animation (coin) 

  db available :  dw  201 : dw 041-3,169  ;cavekoopa 

  db available :  dw  210 : dw 043,008  ;Jumpin Piranha Plant

  db available :  dw  213 : dw 049,034  ;exclamationmark (green) 
  db available :  dw  215 : dw 049,034  ;exclamationmark (green) 
  
  db available :  dw  217 : dw 043,009  ;Red flying koopa (up and down)

  db available :  dw  219 : dw 049,034  ;exclamationmark (green)  
  db available :  dw  221 : dw 049,034  ;exclamationmark (green)  
 
  db available :  dw  244 : dw 047-3,169  ;cavekoopa  

  db available :  dw  247 : dw 050-18,230  ;maxsheertile (left)

  db available :  dw  263 : dw 031-3,169  ;cavekoopa 

  db available :  dw  267 : dw 045,078  ;coin (static)
  db available :  dw  269 : dw 045,078  ;coin (static)
  
  db available :  dw  284 : dw 031,078  ;coin (static)
  db available :  dw  286 : dw 031,078  ;coin (static)
  db available :  dw  288 : dw 031,078  ;coin (static)

  db available :  dw  291 : dw 029-1,087  ;blue rotating block
   
  db available :  dw  292 : dw 047,085  ;green shell
   
  db available :  dw  303 : dw 047-1,087  ;blue rotating block 
  
  db available  :  dw  318 : dw 035,177  ;blueshell  

  db available :  dw  328 : dw 046,055  ;yoshicoin

  db available :  dw  348 : dw 043-3,169  ;cavekoopa 

  db available  :  dw  351 : dw 045-1,171  ;mediumsheertile (right)
 
   db available :  dw  364    : dw 046,175    ;football chuck 

  db force00   :  dw  370   : dw 031+2,007  ;endflag bar
  db available :  dw  370+1 : dw 031-1,006  ;endflag top  
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv032b: equ $-objectlistlv032b

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    005*8+1      :     db 32-2
;dw    184*8    |     db 48
dw    000         ;defines the end of the list COMPULSORY!! 
