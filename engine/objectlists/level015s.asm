objectlistlv015s:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available  :  dw  001 : dw 007,358  ;final cutscene helper object
 
 
;  db available  :  dw  002 : dw 007,142  ;treeline
 
  
 
  db available  :  dw  010 : dw 005,141  ;redbird at yoshi's house
  db available  :  dw  012 : dw 005,141  ;redbird at yoshi's house
  

  
;  db available  :  dw  016 : dw 005,141  ;redbird at yoshi's house

  
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv015s: equ $-objectlistlv015s
  
 
