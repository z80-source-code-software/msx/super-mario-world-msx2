objectlistlv025b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

      db available :  dw  028 : dw 043,198  ;tube up and down 
;      db available |  dw  037 | dw 038,198  ;tube up and down 
      db available :  dw  054 : dw 042,198  ;tube up and down 
;      db available |  dw  063 | dw 040,198  ;tube up and down 
      
      db available :  dw  059 : dw 030,009  ;red flying koopa (up and down)      
      db available :  dw  074 : dw 031,009  ;red flying koopa (up and down)      
      
      db available :  dw  086 : dw 039,198  ;tube up and down

 
  db force00   :  dw  122+1 : dw 042-1,036  ;tube cover up exit sprite 

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv025b: equ $-objectlistlv025b

  
 