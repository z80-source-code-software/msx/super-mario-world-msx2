objectlistlv052:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
	db force28      : dw 021 : dw 004,129	;door
	db available : dw 048 : dw 028,185	;small ghost (following mario)
	db available : dw 053 : dw 043,185	;small ghost (following mario)
	db available : dw 061 : dw 034,214	;eerie (left right)
	db available : dw 067 : dw 026,055	;yoshi coin
	db available : dw 087 : dw 032,212	;big boo
	db available : dw 105 : dw 024,185	;small ghost (following mario)
	db force28      : dw 110 : dw 025,129	;door
	db available : dw 120 : dw 041,185	;small ghost (following mario)
	db available : dw 133 : dw 030,185	;small ghost (following mario)
	db available : dw 149 : dw 031,185	;small ghost (following mario)
	db available : dw 167 : dw 026,185	;small ghost (following mario)
	db available : dw 175 : dw 040,185	;small ghost (following mario)
	db available : dw 190 : dw 025,214	;eerie (left right)
	db available : dw 193 : dw 018,214	;eerie (left right)
	db available : dw 196 : dw 024,214	;eerie (left right)
	db available : dw 200 : dw 018,214	;eerie (left right)
	db available : dw 256 : dw 034,214	;eerie (left right)
	db available : dw 265 : dw 034,214	;eerie (left right)
	db available : dw 278 : dw 027,057	;coin ? box
	db available : dw 282 : dw 021,055	;yoshi coin
	db available : dw 286 : dw 027,029	;feather box
	db available : dw 291 : dw 028,214	;eerie (left right)
	db available : dw 293 : dw 023,214	;eerie (left right)
	db available : dw 305 : dw 028,214	;eerie (left right)
	db available : dw 313 : dw 031,214	;eerie (left right)
	db force28      : dw 327 : dw 037,129	;door


 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv052: equ $-objectlistlv052

