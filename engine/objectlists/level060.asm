objectlistlv060:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  
	db available : dw 020 : dw 037,205	;spring
	db available : dw 021 : dw 020,119	;multicoinbox
	db available : dw 023 : dw 025,241	;secret box (coin)
	db available : dw 029 : dw 037,313	;Dino Rhino (small)
	db available : dw 040 : dw 041,312	;Dino Rhino
	db available : dw 043 : dw 017,313	;Dino Rhino (small)
	db available : dw 050 : dw 036,313	;Dino Rhino (small)
	db available : dw 055 : dw 014,055	;yoshi coin
	db available : dw 058 : dw 019,313	;Dino Rhino (small)
	db available : dw 064 : dw 010,058	;yoshi egg ? box
	db available : dw 070 : dw 019,313	;Dino Rhino (small)
	db available : dw 083 : dw 029,313	;Dino Rhino (small)
	db available : dw 091 : dw 020,313	;Dino Rhino (small)
	db available : dw 105 : dw 017,055	;yoshi coin
	db available : dw 121 : dw 036,036	;Mario exit (up down)


  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060: equ $-objectlistlv060
