objectlistlv023b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
 
  	db force04 : dw 021 : dw 029,024	;moonpowerup
 
 
  db force00   :  dw  30 : dw 013+2,007  ;endflag bar
  db available :  dw  31 : dw 013-1,006  ;endflag top 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv023b: equ $-objectlistlv023b
  
 
