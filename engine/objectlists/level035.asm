objectlistlv035:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  031 : dw 026,247  ;bill



  db available   :  dw  032 : dw 042,055  ;yoshi coin

 ; db available  |  dw  035 | dw 049,248  ;bill (superspawner)


  db available  :  dw  041-3 : dw 037-1,245  ;springboard (left)
  
  db available :  dw  060 : dw 028,061  ;pink koopa  

  db available  :  dw  064 : dw 039,247  ;bill  
  
  db available  :  dw  072 : dw 042-2,246  ;springboard (right)
  
  db  available :  dw  077 : dw 033,009  ;red flying koopa  (up and down)

  db available  :  dw  091 : dw 032,247  ;bill


  db available  :  dw  129 : dw 033,247  ;bill   
  
  db available  :  dw  152 : dw 044,246  ;springboard (right)
  db available  :  dw  161-3 : dw 046-1,245  ;springboard (left)
  db available  :  dw  166 : dw 044,246  ;springboard (right) 

  db  available :  dw  169 : dw 037,009  ;red flying koopa  (up and down)

  db available   :  dw  170 : dw 042,055  ;yoshi coin
  db available  :  dw  171 : dw 023,247  ;bill   
  
  
  db available  :  dw  177-3 : dw 046,245  ;springboard (left)
  db available  :  dw  182 : dw 048,246  ;springboard (right) 
  db available  :  dw  193-3 : dw 044-1,245  ;springboard (left)

        db available :  dw  210 : dw 029,029  ;questionmark animation (feather)  

  db available  :  dw  217 : dw 035,247  ;bill
 
  db available :  dw  229 : dw 041,061  ;pink koopa
  
  
  db available  :  dw  239-3 : dw 037-1,245  ;springboard (left)
  
  db available  :  dw  254 : dw 028,247  ;bill   
  
  db  available :  dw  272 : dw 036,009  ;red flying koopa  (up and down)  
  
  db available  :  dw  279-3 : dw 039-1,245  ;springboard (left)

  db  available :  dw  307 : dw 026,009  ;red flying koopa  (up and down)  

  db force18   :  dw  313-2 : dw 027-2,004  ;checkpoint top
  db available :  dw  313+2-2 : dw 027+3-2,005  ;checkpoint bar
  db force04   :  dw  313+5-2 : dw 027+2-2,031  ;checkpoint pole right 

  db available   :  dw  336+1 : dw 047,008  ;Jumpin Piranha Plant

  
  db available  :  dw  343-3 : dw 043,245  ;springboard (left)
  db available  :  dw  348 : dw 046,246  ;springboard (right) 

  db available  :  dw  352 : dw 049,248  ;bill (superspawner)

  db available   :  dw  354+1 : dw 045,008  ;Jumpin Piranha Plant
  db available :  dw  356 : dw 045-3,036  ;tube cover up exit sprite

  db available  :  dw  363-3 : dw 044,245  ;springboard (left)

  db available  :  dw  387 : dw 049,248  ;bill (superspawner)  
    db available  :  dw  388 : dw 025,063  ;green koopa   
  
  db available  :  dw  399-3 : dw 037,245  ;springboard (left)
  
  db available :  dw  407 : dw 039,028  ;questionmark animation red flower  

  db available  :  dw  424 : dw 049,248  ;bill (superspawner) 
  
  db available  :  dw  431-3 : dw 038,245  ;springboard (left)
  db available  :  dw  436 : dw 046,246  ;springboard (right) 

  db available :  dw  454    : dw 033,042    ;smileybox (extra life)

  db available :  dw  480 : dw 031,061  ;pink koopa  
  
  db available  :  dw  517 : dw 049,248  ;bill (superspawner)   
  
  db available :  dw  520 : dw 033,061  ;pink koopa  

  
  db available  :  dw  530 : dw 037,246  ;springboard (right) 
  
  db available   :  dw  534 : dw 043,055  ;yoshi coin  
  
  db available  :  dw  552 : dw 045,246  ;springboard (right) 

  db  available :  dw  558 : dw 038,009  ;red flying koopa  (up and down) 

  db available  :  dw  565-3 : dw 040,245  ;springboard (left)
  db available  :  dw  570 : dw 043,246  ;springboard (right)

  db  available :  dw  577 : dw 033,009  ;red flying koopa  (up and down)

  db available   :  dw  578 : dw 027,055  ;yoshi coin  

  db available  :  dw  585-3 : dw 041,245  ;springboard (left)
  db available  :  dw  590 : dw 034,246  ;springboard (right)


  db force00   :  dw  602 : dw 028-3+4,007  ;endflag bar
  db available :  dw  602+1 : dw 028-3+1+1,006  ;endflag top 

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv035: equ $-objectlistlv035

