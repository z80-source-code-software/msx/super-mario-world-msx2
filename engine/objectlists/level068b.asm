objectlistlv068b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 047	:	dw 026,214	;eerie (left right)
	db available	:	dw 061	:	dw 012,338	;Ghost many
	db available	:	dw 101	:	dw 021,324	;Eerie (high angle)
	db available	:	dw 177	:	dw 026,324	;Eerie (high angle)
	db available	:	dw 180	:	dw 017,324	;Eerie (high angle)
	db available	:	dw 201	:	dw 023,185	;small ghost (following mario)
	db available	:	dw 217	:	dw 011,211	;small ghostring
	db available	:	dw 250	:	dw 028,036	;Mario exit (up down)




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv068b: equ $-objectlistlv068b
