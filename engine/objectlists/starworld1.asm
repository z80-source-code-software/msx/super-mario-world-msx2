objectliststarworld1:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 

	db available	:	dw 035	:	dw 038,351	;Star (static)
	db available	:	dw 053	:	dw 029,280	;smileybox (feather)
	db available	:	dw 058	:	dw 032,064	;blue koopa
	db available	:	dw 076	:	dw 034,272	;red mushroom (static)
	db available	:	dw 091	:	dw 058,055	;yoshi coin
	db available	:	dw 098	:	dw 048,061	;pink koopa
	db available	:	dw 109	:	dw 011,055	;yoshi coin
	db available	:	dw 116	:	dw 035,055	;yoshi coin
	db available	:	dw 123	:	dw 042,061	;pink koopa
	db available	:	dw 138	:	dw 027,061	;pink koopa
	db available	:	dw 161	:	dw 031,064	;blue koopa
	db available	:	dw 176	:	dw 031,061	;pink koopa
	db available	:	dw 180	:	dw 039,317	;Flower (static)
	db available	:	dw 192	:	dw 025,055	;yoshi coin
	db available	:	dw 199	:	dw 051,289	;greenmushroom (static)
	db available	:	dw 204	:	dw 039,164	;feather bonus (static)
	db available	:	dw 209	:	dw 060,348	;Permayoshi
	db available	:	dw 212	:	dw 031,064	;blue koopa
	db available	:	dw 215	:	dw 058,062	;yellow koopa
	db available	:	dw 225	:	dw 055,094	;green flying koopa (to left only)
	db available	:	dw 237	:	dw 055,094	;green flying koopa (to left only)
	db available	:	dw 245	:	dw 025,055	;yoshi coin
	db available	:	dw 254	:	dw 056,091	;red flying koopa
	db available	:	dw 267	:	dw 004,178	;keyhole
	db available	:	dw 274	:	dw 044,007	;Endflagbar
	db available	:	dw 275	:	dw 043,006	;Endflagtop







  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectliststarworld1: equ $-objectliststarworld1


