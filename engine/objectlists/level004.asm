objectlistlv004:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  
 
;  db available  |  dw  006-4 | dw 019-1,099  ;expanding smileybox platform
  
  db available  :  dw  045 : dw 017,098  ;rotating platform
  
  db available  :  dw  071 : dw 020,032  ;exclamationmark (yellow)  
  

  db available  :  dw  083-4 : dw 022-1,099  ;expanding smileybox platform
  
  db available  :  dw  084 : dw 044,032  ;exclamationmark (yellow)  
  
    db available  :  dw  085 : dw 012,100  ;expanding smileybox expander
  db available  :  dw  086 : dw 044,032  ;exclamationmark (yellow)     
  db available  :  dw  088 : dw 044,032  ;exclamationmark (yellow)     
  db available  :  dw  090 : dw 044,032  ;exclamationmark (yellow)     
    
  db available  :  dw  095-4 : dw 016-1,099  ;expanding smileybox platform
  
  db available  :  dw  096 : dw 010,055  ;yoshicoin
  
  db available  :  dw  123 : dw 018,098  ;rotating platform
  
  db available  :  dw  145 : dw 016,057  ;questionmark animation (coin)
  db available  :  dw  147 : dw 016,058  ;questionmark animation (green yoshi egg)
     
  db  available :  dw  156 : dw 007,091  ;red flying koopa
  
  db  available :  dw  167 : dw 022,095  ;square flattened platform 
    
  db  available :  dw  168 : dw 015,091  ;red flying koopa  

  db available  :  dw  174 : dw 030,032  ;exclamationmark (yellow) 
  db available  :  dw  176 : dw 030,032  ;exclamationmark (yellow) 
  db available  :  dw  184 : dw 030,032  ;exclamationmark (yellow) 


  db  available :  dw  185 : dw 018,095  ;square flattened platform 

  db available  :  dw  186 : dw 030,032  ;exclamationmark (yellow) 

  db available  :  dw  208 : dw 012,060  ;brown coopa

  db force00    :  dw  216 : dw 039,036  ;tube cover up exit sprite

  db available  :  dw  222-2 : dw 023-11,098  ;rotating platform
  db available  :  dw  239-2 : dw 032-11,098  ;rotating platform
  
  db force18   :  dw  300 : dw 038-3,004  ;checkpoint top
  db available :  dw  302 : dw 041-3,005  ;checkpoint bar
  db force04   :  dw  305 : dw 040-3,031  ;checkpoint pole right
  
  
  
  db available  :  dw  315 : dw 035,055  ;yoshicoin
   
  db  available :  dw  317 : dw 041,093  ;green flying koopa   
 
  db available  :  dw  347 : dw 033,060  ;brown coopa


  db available  :  dw  355 : dw 021,028  ;questionmark animation (red flower)

  db  available :  dw  367 : dw 029,097  ;square flattened platform (vertical)

  db available  :  dw  393-2 : dw 029-11,098  ;rotating platform

  db available  :  dw  407-4 : dw 020-1,101  ;expanding smileybox platform (+vertical)
  db available  :  dw  417-4 : dw 024-1,099  ;expanding smileybox platform
  
  db available  :  dw  427-4 : dw 020-1,101  ;expanding smileybox platform (+vertical)
  
  db available  :  dw  431 : dw 009,100  ;expanding smileybox expander
  
  db available  :  dw  437-4 : dw 016-1,099  ;expanding smileybox platform
  db available  :  dw  447-4 : dw 010-1,101  ;expanding smileybox platform (+vertical)
  db available  :  dw  459-4 : dw 010-1,099  ;expanding smileybox platform



  db available  :  dw  461 : dw 004,055  ;yoshicoin

  db available  :  dw  465 : dw 020,032  ;exclamationmark (yellow)
  db available  :  dw  467 : dw 020,032  ;exclamationmark (yellow)
  db available  :  dw  469 : dw 020,032  ;exclamationmark (yellow)

  
  db  available :  dw  497 : dw 027,097  ;square flattened platform (vertical)

  db  available :  dw  508 : dw 013,091  ;red flying koopa
  db  available :  dw  509 : dw 029,091  ;red flying koopa


  db available  :  dw  512-2 : dw 028-11,098  ;rotating platform

  db  available :  dw  528 : dw 012,096  ;green special box 


  db available  :  dw  541-2 : dw 029-11,098  ;rotating platform
  db available  :  dw  560-2 : dw 033-11,098  ;rotating platform


  db available  :  dw  574 : dw 024,055  ;yoshicoin

  db force00    :  dw  622 : dw 033-4,007  ;endflag bar
  db force28    :  dw  623 : dw 029-4,006  ;endflag top


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv004: equ $-objectlistlv004
  
  