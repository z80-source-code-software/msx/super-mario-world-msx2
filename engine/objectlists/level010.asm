objectlistlv010:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  

  db available  :  dw  003 : dw 030,130  ;static camerascroller

 ; db force04    |  dw  005 | dw 019,078  ;coin (static)  
 ; db force06    |  dw  007 | dw 017,078  ;coin (static)  
 ; db force08    |  dw  009 | dw 017,078  ;coin (static) 
  
   
;  db force10    |  dw  022 | dw 026,078  ;coin (static)  
;  db force18    |  dw  024 | dw 026,078  ;coin (static)  

  db force00    :  dw  026-2 : dw 010-2,133  ;giant iggy block coverup
  db force28    :  dw  026 : dw 010,131  ;huge iggy block
  db force02    :  dw  026+16 : dw 010-2,133  ;giant iggy block coverup


  db force04    :  dw  050-2 : dw 010-2,133  ;giant iggy block coverup
  db force24    :  dw  050 : dw 010,131  ;huge iggy block
 
  
 ; db force08    |  dw  054 | dw 019,078  ;coin (static)  
 ; db force10    |  dw  056 | dw 017,078  ;coin (static)  
 ; db force18    |  dw  058 | dw 017,078  ;coin (static)
 ; db force20    |  dw  060 | dw 019,078  ;coin (static) 
 
  
  
      db force18 :  dw  051 : dw 023-6,041  ;flying box (red flower) 
 
     db force06    :  dw  050+16 : dw 010-2,133  ;giant iggy block coverup 
  
  db force00    :  dw  082-2 : dw 010-2,133  ;giant iggy block coverup
  db force28    :  dw  082 : dw 010,131  ;huge iggy block
  
  
    db available   :  dw  084 : dw 27,132  ;falling block
  db available   :  dw  086 : dw 27,132  ;falling block
;    db force04  |  dw  087 | dw 026,078  ;coin (static)
;      db force06  |  dw  089 | dw 026,078  ;coin (static) 
  db available   :  dw  090 : dw 27,132  ;falling block
;    db force08  |  dw  091 | dw 026,078  ;coin (static)
  db available   :  dw  092 : dw 27,132  ;falling block
  
  
  db force02    :  dw  082+16 : dw 010-2,133  ;giant iggy block coverup  


  db force18    :  dw  109 : dw 22,134  ;giant door



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv010: equ $-objectlistlv010
  
 

level10bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 3624-236+bgrmapaddr :   db      039, 035, 039, 035 ;calculate adress = (y * mapwidth) + x
    dw 3626-236+bgrmapaddr :   db      036, 037, 036, 037 ;calculate adress = (y * mapwidth) + x
    dw 3628-236+bgrmapaddr :   db      039, 064, 039, 032 ;calculate adress = (y * mapwidth) + x
    dw 3630-236+bgrmapaddr :   db      065, 066, 033, 034 ;calculate adress = (y * mapwidth) + x
    dw 3632-236+bgrmapaddr :   db      039, 035, 039, 035 ;calculate adress = (y * mapwidth) + x
    dw 000                 :   db      000, 000, 000, 000
lenghtlevel10bkgtiles: equ $-level10bkgtiles   