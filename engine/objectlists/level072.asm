objectlistlv072:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  

	db available	:	dw 020	:	dw 031,342	;scooping chuck
	db available	:	dw 043	:	dw 048,341	;lavaplateau
	db available	:	dw 046	:	dw 043,009	;Red flying koopa (up down)
	db available	:	dw 062	:	dw 038,342	;scooping chuck
	db available	:	dw 065	:	dw 032,032	;yellow !
	db available	:	dw 090	:	dw 048,341	;lavaplateau
	db available	:	dw 095	:	dw 044,009	;Red flying koopa (up down)
	db available	:	dw 101	:	dw 048,341	;lavaplateau
	db available	:	dw 112	:	dw 048,341	;lavaplateau
	db available	:	dw 119	:	dw 044,091	;red flying koopa
	db available	:	dw 132	:	dw 037,036	;Mario exit (up down)
	db available	:	dw 147	:	dw 031,074	;Smileybox (climbing plant)
	db available	:	dw 153	:	dw 005,029	;feather box
	db available	:	dw 183	:	dw 048,341	;lavaplateau
	db available	:	dw 206	:	dw 019,042	;smileybox (extra life)
	db available	:	dw 220	:	dw 048,341	;lavaplateau
	db available	:	dw 224	:	dw 040,009	;Red flying koopa (up down)
	db available	:	dw 229	:	dw 048,341	;lavaplateau
	db available	:	dw 254	:	dw 035,342	;scooping chuck
	db available	:	dw 268	:	dw 048,341	;lavaplateau
	db available	:	dw 277	:	dw 048,341	;lavaplateau
	db available	:	dw 298	:	dw 040,207	;vinehelper
	db available	:	dw 310	:	dw 038,342	;scooping chuck
	db available	:	dw 321	:	dw 048,341	;lavaplateau
	db available	:	dw 324	:	dw 043,032	;yellow !
	db available	:	dw 326	:	dw 033,028	;flower box
	db available	:	dw 329	:	dw 048,341	;lavaplateau
	db available	:	dw 337	:	dw 048,341	;lavaplateau
	db available	:	dw 373	:	dw 030,342	;scooping chuck
	db available	:	dw 416	:	dw 048,341	;lavaplateau
	db available	:	dw 447	:	dw 031,342	;scooping chuck
	db available	:	dw 470	:	dw 048,341	;lavaplateau
	db available	:	dw 474	:	dw 041,009	;Red flying koopa (up down)
	db available	:	dw 518	:	dw 048,341	;lavaplateau
	db available	:	dw 527	:	dw 042,205	;spring
	db available	:	dw 535	:	dw 043,035	;blue !
	db available	:	dw 543	:	dw 044,035	;blue !
	db available	:	dw 549	:	dw 024,342	;scooping chuck
	db available	:	dw 550	:	dw 045,035	;blue !
	db available	:	dw 573	:	dw 047,178	;keyhole
	db available	:	dw 587	:	dw 031,007	;Endflagbar
	db available	:	dw 588	:	dw 030,006	;Endflagtop





  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv072: equ $-objectlistlv072

