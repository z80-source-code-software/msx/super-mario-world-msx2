objectlistlv047b:
;note that on this map mario goes from right to left
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  

  db force26  :  dw  008 : dw 028,129  ;door   
  

        db available :  dw  152 : dw 032,128  ;flame 
        db available :  dw  164 : dw 032,128  ;flame 
        db available :  dw  176 : dw 032,128  ;flame 
        db available :  dw  188 : dw 032,128  ;flame 


  db  available :  dw  199 : dw 030,264  ;bony beetle
  db  available :  dw  222 : dw 030,264  ;bony beetle

  
  db  available :  dw  230 : dw 022,293  ;switchbox for maplevel04-06b
  
  
        db available :  dw  244 : dw 032,128  ;flame  
        db available :  dw  262 : dw 032,128  ;flame  
        
        db available :  dw  278 : dw 032,127  ;blockwall (16x16)
  
  db  available :  dw  299 : dw 015,292  ;maplevel04-06b (helper object)
  db  available :  dw  300 : dw 015,217  ;sheer stairs (helper object)
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv047b: equ $-objectlistlv047b

