cutscenefortress:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
    db available   :  dw  023 : dw 24,345  ;mario cutscene helper

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtcutscenefortress: equ $-cutscenefortress


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    27*8      :     db 32
dw    000         ;defines the end of the list COMPULSORY!!  


