objectlistlv062:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 013 : dw 010,327	;Special yellow pillar  
	db available : dw 061 : dw 027,314	;Sheerplatform (left)
	db available : dw 105 : dw 030,314	;Sheerplatform (left)
	db available : dw 112 : dw 009,055	;yoshi coin
	db available : dw 156 : dw 027,314	;Sheerplatform (left)
	db available : dw 167 : dw 034,314	;Sheerplatform (left)
	db available : dw 172 : dw 020,314	;Sheerplatform (left)
	db available : dw 181 : dw 046,315	;Sheerplatform (right)
	db available : dw 192 : dw 045,210	;falling platform
	db available : dw 201 : dw 010,055	;yoshi coin
	db available : dw 219 : dw 041,033	;red !
	db available : dw 220 : dw 039,289	;greenmushroom (static)
	db available : dw 221 : dw 041,033	;red !
	db available : dw 222 : dw 039,289	;greenmushroom (static)
	db available : dw 223 : dw 041,033	;red !
	db available : dw 224 : dw 039,289	;greenmushroom (static)
	db available : dw 233 : dw 037,314	;Sheerplatform (left)
	db available : dw 246 : dw 037,314	;Sheerplatform (left)
	db available : dw 260 : dw 037,314	;Sheerplatform (left)
	db available : dw 277 : dw 037,055	;yoshi coin
	db available : dw 286 : dw 048,036	;Mario exit (up down)
	db available : dw 294 : dw 021,180	;smileybox (pbox)
	db available : dw 297 : dw 018,055	;yoshi coin
	db available : dw 360 : dw 007,318	;Megamole
	db available : dw 365 : dw 006,027	;red shroom box
	db available : dw 381 : dw 044,318	;Megamole
	db available : dw 441 : dw 041,026	;snapping chuck
	db available : dw 446 : dw 031,007	;Endflagbar
	db available : dw 447 : dw 030,006	;Endflagtop




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv062: equ $-objectlistlv062
