objectlistlv015:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available  :  dw  002 : dw 007,142  ;treeline
 
  db available  :  dw  005 : dw 008,144  ;apple
;  db available 	|  dw  006 | dw 012,144  ;apple
  
 
  db available  :  dw  010 : dw 005,141  ;redbird at yoshi's house
  db available  :  dw  012 : dw 005,141  ;redbird at yoshi's house
  
;  db available 	|  dw  014 | dw 008,144  ;apple
  db available  :  dw  015 : dw 012,144  ;apple
  
  db available  :  dw  016 : dw 005,141  ;redbird at yoshi's house
  db available  :  dw  018 : dw 005,141  ;redbird at yoshi's house 
  
  
  db available  :  dw  022 : dw 022,143  ;yoshis fireplace
  
  db available  :  dw  023 : dw 008,144  ;apple
;  db available 	|  dw  024 | dw 012,144  ;apple 

 ; db available  :  dw  024 : dw 020,367  ;test object

  db available  :  dw  025 : dw 003,145  ;yoshi house exit
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv015: equ $-objectlistlv015
  
 
