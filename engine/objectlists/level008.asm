objectlistlv008:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  


;db available |  dw  012 | dw 053,055  ;yoshicoin


    db available  :  dw  016 : dw 066-2-1,115  ;slightsheertile (left)
    
    db available  :  dw  034 : dw 062,117  ;cactuscat
    db available  :  dw  070 : dw 064,117  ;cactuscat
    
    
    db available  :  dw  081 : dw 058,119  ;multicoin box
    
    db available  :  dw  102 : dw 057,117  ;cactuscat
    

    
    
        db force00   :  dw  122+1 : dw 060-1,036  ;tube cover up exit sprite
    



  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv008: equ $-objectlistlv008
  

level08bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 7737+bgrmapaddr :   db      083, 084, 084, 026 ;calculate adress = (y * mapwidth) + x
    dw 000             :   db      000, 000, 000, 000
lenghtlevel08bkgtiles: equ $-level08bkgtiles  
  