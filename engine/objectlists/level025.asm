objectlistlv025:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
 

     db available :  dw  015 : dw 028,055  ;yoshicoin 

   
   db available :  dw  027 : dw 042,190  ;yellow shell
   db available :  dw  038 : dw 038,067  ;yellow naked koopa
    

     db available :  dw  57 : dw 026,055  ;yoshicoin 
     
     db available :  dw  64 : dw 042,191  ;goomba 
   
  db available :  dw  074 : dw 042-4,061  ;pink coopa  
  
;  db force19   |  dw  078 | dw 039,008  ;Jumpin Piranha Plant   

  db available  :  dw  092 : dw 042-4,064  ;blue koopa
  
      db available :  dw  095 : dw 039,198  ;tube up and down  
  
  db available  :  dw  106 : dw 042-4,064  ;blue koopa

  db force25   :  dw  110 : dw 039,008  ;Jumpin Piranha Plant  
  db force00   :  dw  111 : dw 039-1,036  ;tube cover up exit sprite  
  
      db available :  dw  120 : dw 041,198  ;tube up and down   
  
     db available :  dw  162-6 : dw 038-10-2,192  ;hammer brother 
 
  db available :  dw  148 : dw 038,167  ;diagonal block (helper)  
 
  db available :  dw  170-1 : dw 044-1,165  ;diagonal block (left)  

 ;  db available  |  dw  173 | dw 029,193  ;blockwall (objects only)
     
   db available  :  dw  183 : dw 032-1,171  ;mediumsheertile (right) 

  db available :  dw  184 : dw 032-5,061  ;pink coopa 

   db available  :  dw  187 : dw 040-2,193  ;blockwall (objects only)  
   
   db available  :  dw  188 : dw 034+3,150  ;mediumsheertile (left) (short segment) 1/2   
   db available  :  dw  192 : dw 034+1,149  ;mediumsheertile (left)
   
   db available  :  dw  193 : dw 035-2-2,193  ;blockwall (objects only)
   
   db available  :  dw  200 : dw 033-2,194  ;truwall (objects only)
   
   
     db available :  dw  202 : dw 042,191  ;goomba    
   
  db available  :  dw  203 : dw 031-4,064  ;blue koopa   

  db available  :  dw  207 : dw 035,057  ;questionmark animation (coin)

     db available :  dw  208 : dw 042,191  ;goomba      
  
  db available  :  dw  209 : dw 035,028  ;questionmark animation red flower
  db available  :  dw  211 : dw 035,057  ;questionmark animation (coin)
 
     db available :  dw  214 : dw 042,195  ;goomba (winged)     

  db force25   :  dw  234 : dw 039,008  ;Jumpin Piranha Plant 
  db force00   :  dw  235 : dw 039-1,036  ;tube cover up exit sprite    

  db available :  dw  245 : dw 002,042  ;smileybox extra life
   
  db available  :  dw  261 : dw 043,127  ;blockwall (16x16)   
  db available  :  dw  262 : dw 033,127  ;blockwall (16x16)   

  db available :  dw  282 : dw 036-2-5-3,192  ;hammer brother 

     db available :  dw  326 : dw 044,195  ;goomba (winged) 

  db available :  dw  328 : dw 039,058  ;questionmark animation (green yoshi egg)

 ; db available |  dw  300 | dw 032-4,061  ;pink coopa
 
     db available :  dw  343 : dw 44,195  ;goomba (winged)  
 
      db available :  dw  344 : dw 041,055  ;yoshicoin 

     db available :  dw  360 : dw 42,195  ;goomba (winged)   

      db available :  dw  372 : dw 032,196  ;changing powerup

      db available :  dw  392 : dw 028,197  ;paragoomba

  db available :  dw  404 : dw 036-2-5-3,192  ;hammer brother

      db available :  dw  416 : dw 018,197  ;paragoomba

     db available :  dw  430 : dw 38,195  ;goomba (winged) 

      db available :  dw  440 : dw 033,055  ;yoshicoin
      db available :  dw  446 : dw 006,197  ;paragoomba      
        db available :  dw  453 : dw 011,024  ;moon powerup 
     db available :  dw  454 : dw 040,195  ;goomba (winged)
     db available :  dw  479 : dw 038,195  ;goomba (winged)
     db available :  dw  490 : dw 034,195  ;goomba (winged)
     db available :  dw  504 : dw 042,195  ;goomba (winged)
     db available :  dw  517 : dw 042,195  ;goomba (winged)

      db available :  dw  518 : dw 015,197  ;paragoomba
      db available :  dw  534 : dw 015,197  ;paragoomba


  db available :  dw  557 : dw 036-2-5-3,192  ;hammer brother

  db force00   :  dw  567 : dw 027,007  ;endflag bar
  db force28   :  dw  567+1 : dw 027-1,006  ;endflag top 


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv025: equ $-objectlistlv025

  
 
