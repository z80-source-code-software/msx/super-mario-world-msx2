objectlistlv065d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 041 : dw 083,210	;falling platform
	db available : dw 056 : dw 084,210	;falling platform
	db available : dw 063 : dw 067,192	;hammer brother
	db available : dw 070 : dw 083,210	;falling platform
	db available : dw 105 : dw 079,036	;Mario exit (up down)

  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv065d: equ $-objectlistlv065d
