objectlistlv060g:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
	db available : dw 030 : dw 050,104	;water (marioswim)
	db available : dw 041 : dw 034,302	;exploding bubble (mushroom)
	db available : dw 061 : dw 038,302	;exploding bubble (mushroom)
	db available : dw 074 : dw 045,055	;yoshi coin
	db available : dw 077 : dw 032,302	;exploding bubble (mushroom)
	db available : dw 087 : dw 039,302	;exploding bubble (mushroom)
	db available : dw 107 : dw 034,302	;exploding bubble (mushroom)
	db available : dw 120 : dw 044,036	;Mario exit (up down)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060g: equ $-objectlistlv060g
