objectliststarworld4:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
	db available	:	dw 026	:	dw 040,348	;Permayoshi
	db available	:	dw 034	:	dw 030,186	;rotating platform (permanent)
	db available	:	dw 050	:	dw 027,061	;pink koopa
	db available	:	dw 061	:	dw 032,061	;pink koopa
	db available	:	dw 071	:	dw 027,186	;rotating platform (permanent)
	db available	:	dw 089	:	dw 028,064	;blue koopa
	db available	:	dw 111	:	dw 043,064	;blue koopa
	db available	:	dw 114	:	dw 035,064	;blue koopa
	db available	:	dw 115	:	dw 025,061	;pink koopa
	db available	:	dw 132	:	dw 027,028	;flower box
	db available	:	dw 155	:	dw 027,061	;pink koopa
	db available	:	dw 167	:	dw 024,186	;rotating platform (permanent)
	db available	:	dw 174	:	dw 029,091	;red flying koopa
	db available	:	dw 186	:	dw 033,091	;red flying koopa
	db available	:	dw 197	:	dw 040,064	;blue koopa
	db available	:	dw 210	:	dw 035,186	;rotating platform (permanent)
	db available	:	dw 254	:	dw 040,061	;pink koopa
	db available	:	dw 279	:	dw 042,009	;Red flying koopa (up down)
	db available	:	dw 321	:	dw 013,186	;rotating platform (permanent)
	db available	:	dw 331	:	dw 042,034	;green !
	db available	:	dw 333	:	dw 042,034	;green !
	db available	:	dw 335	:	dw 042,034	;green !
	db available	:	dw 337	:	dw 042,034	;green !
	db available	:	dw 339	:	dw 042,034	;green !
	db available	:	dw 351	:	dw 028,001	;Pink Shell
	db available	:	dw 361	:	dw 028,085	;green shell
	db available	:	dw 363	:	dw 028,069	;blue naked koopa
	db available	:	dw 377	:	dw 027,093	;Green flying koopa (jumping)
	db available	:	dw 383	:	dw 045,178	;keyhole
	db available	:	dw 384	:	dw 027,093	;Green flying koopa (jumping)
	db available	:	dw 412	:	dw 019,061	;pink koopa
	db available	:	dw 426	:	dw 015,186	;rotating platform (permanent)
	db available	:	dw 435	:	dw 022,009	;Red flying koopa (up down)
	db available	:	dw 443	:	dw 018,064	;blue koopa
	db available	:	dw 472	:	dw 017,186	;rotating platform (permanent)
	db available	:	dw 489	:	dw 021,186	;rotating platform (permanent)
	db available	:	dw 508	:	dw 021,098	;rotating platform
	db available	:	dw 567	:	dw 031,007	;Endflagbar
	db available	:	dw 568	:	dw 030,006	;Endflagtop




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectliststarworld4: equ $-objectliststarworld4


