objectliststarworld5:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 

	db available	:	dw 022	:	dw 046,210	;falling platform
	db available	:	dw 031	:	dw 045,210	;falling platform
	db available	:	dw 039	:	dw 043,210	;falling platform
	db available	:	dw 046	:	dw 042,210	;falling platform
	db available	:	dw 049	:	dw 044,094	;green flying koopa (to left only)
	db available	:	dw 053	:	dw 043,210	;falling platform
	db available	:	dw 061	:	dw 044,210	;falling platform
	db available	:	dw 065	:	dw 039,094	;green flying koopa (to left only)
	db available	:	dw 069	:	dw 043,210	;falling platform
	db available	:	dw 073	:	dw 040,094	;green flying koopa (to left only)
	db available	:	dw 077	:	dw 041,210	;falling platform
	db available	:	dw 086	:	dw 042,210	;falling platform
	db available	:	dw 095	:	dw 043,210	;falling platform
	db available	:	dw 100	:	dw 040,091	;red flying koopa
	db available	:	dw 105	:	dw 044,210	;falling platform
	db available	:	dw 113	:	dw 043,210	;falling platform
	db available	:	dw 122	:	dw 045,210	;falling platform
	db available	:	dw 126	:	dw 041,091	;red flying koopa
	db available	:	dw 130	:	dw 046,210	;falling platform
	db available	:	dw 140	:	dw 038,076	;p-box (blue)
	db available	:	dw 149	:	dw 040,029	;feather box
	db available	:	dw 157	:	dw 047,102	;ultracoin (static)
	db available	:	dw 171	:	dw 043,009	;Red flying koopa (up down)
	db available	:	dw 185	:	dw 046,102	;ultracoin (static)
	db available	:	dw 214	:	dw 037,009	;Red flying koopa (up down)
	db available	:	dw 229	:	dw 039,009	;Red flying koopa (up down)
	db available	:	dw 262	:	dw 038,009	;Red flying koopa (up down)
	db available	:	dw 285	:	dw 040,009	;Red flying koopa (up down)
	db available	:	dw 298	:	dw 045,009	;Red flying koopa (up down)
	db available	:	dw 310	:	dw 045,348	;Permayoshi
	db available	:	dw 343	:	dw 044,250	;spikey
	db available	:	dw 350	:	dw 040,290	;piranjaplant (fire version)
	db available	:	dw 360	:	dw 044,250	;spikey
	db available	:	dw 380	:	dw 047,210	;falling platform
	db available	:	dw 389	:	dw 046,210	;falling platform
	db available	:	dw 397	:	dw 044,210	;falling platform
	db available	:	dw 404	:	dw 043,210	;falling platform
	db available	:	dw 407	:	dw 045,094	;green flying koopa (to left only)
	db available	:	dw 411	:	dw 044,210	;falling platform
	db available	:	dw 419	:	dw 045,210	;falling platform
	db available	:	dw 423	:	dw 040,094	;green flying koopa (to left only)
	db available	:	dw 427	:	dw 044,210	;falling platform
	db available	:	dw 431	:	dw 041,094	;green flying koopa (to left only)
	db available	:	dw 435	:	dw 042,210	;falling platform
	db available	:	dw 444	:	dw 043,210	;falling platform
	db available	:	dw 452	:	dw 016,034	;green !
	db available	:	dw 454	:	dw 016,034	;green !
	db available	:	dw 455	:	dw 044,210	;falling platform
	db available	:	dw 469	:	dw 040,061	;pink koopa
	db available	:	dw 474	:	dw 008,178	;keyhole
	db available	:	dw 480	:	dw 041,250	;spikey
	db available	:	dw 494	:	dw 034,061	;pink koopa
	db available	:	dw 498	:	dw 035,250	;spikey
	db available	:	dw 515	:	dw 031,007	;Endflagbar
	db available	:	dw 516	:	dw 030,006	;Endflagtop







  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectliststarworld5: equ $-objectliststarworld5


