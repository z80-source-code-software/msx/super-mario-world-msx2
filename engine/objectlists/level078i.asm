objectlistlv078i:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  
  
    db available :  dw  002 : dw 018,122  ;fence support object for marioclimbing  
  db available  :  dw  003 : dw 032,103  ;lava (mario killer)  
  
  ;tmxdata
  
  
        db available    :       dw 027  :       dw 024,125      ;climbing koopa (red horizontal)
        db available    :       dw 040  :       dw 014,273      ;moving flame (bouncing)
        db available    :       dw 052  :       dw 023,273      ;moving flame (bouncing)
        db available    :       dw 055  :       dw 013,125      ;climbing koopa (red horizontal)
        db available    :       dw 061  :       dw 022,125      ;climbing koopa (red horizontal)
        db available    :       dw 065  :       dw 017,125      ;climbing koopa (red horizontal)
        db available    :       dw 071  :       dw 013,273      ;moving flame (bouncing)
        db available    :       dw 081  :       dw 025,273      ;moving flame (bouncing)
        db available    :       dw 083  :       dw 017,125      ;climbing koopa (red horizontal)

  
  ;/tmxdata
  	db force24	:	dw 088	:	dw 024,129	;door




  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078i: equ $-objectlistlv078i
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
