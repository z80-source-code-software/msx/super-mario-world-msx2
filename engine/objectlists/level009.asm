objectlistlv009:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  


 ; db available |  dw  002 | dw 018,122  ;fence support object for marioclimbing

  db available  :  dw  003 : dw 032,103  ;lava (mario killer)

    db force00 :  dw  026 : dw 030,128  ;flame
  
  ;hek 1
    db force28 :  dw  031 : dw 017,123  ;climbing koopa (green horizontal)
    db force24 :  dw  041 : dw 015,123  ;climbing koopa (green horizontal)
    db force20 :  dw  048 : dw 013,125  ;climbing koopa (red horizontal)
    db force08 :  dw  057 : dw 021,124  ;climbing koopa (green vertical)
 
  ;hek 2
     db force04 :  dw  077 : dw 013,123  ;climbing koopa (green horizontal)
  db available  :  dw  82 : dw 012,110  ;brown box destroyer
  db available :  dw  83 : dw 010,127  ;blockwall (16x16)
  db force28 :  dw  84 : dw 012,057  ;questionmark animation (coin)
  db force29 :  dw  86 : dw 012,057  ;questionmark animation (coin)
  db force30  :  dw  88 : dw 012,028  ;questionmark animation (red flower)
      db force24 :  dw  089 : dw 019,126  ;climbing koopa (green vertical change on edge)
  db force31 :  dw  90 : dw 012,057  ;questionmark animation (coin)
  db force08 :  dw  92 : dw 012,057  ;questionmark animation (coin)
  db available :  dw  93 : dw 010,127  ;blockwall (16x16)

  db force18  :  dw  94 : dw 026,076  ;p-box (blue-static)

    db force20 :  dw  101 : dw 018,125  ;climbing koopa (red horizontal)
    db force00 :  dw  106 : dw 018,124  ;climbing koopa (green vertical)
    
        db available :  dw  127 : dw 030,128  ;flame
    
    db force28 :  dw  130 : dw 024,125  ;climbing koopa (red horizontal)

      db force24 :  dw  154 : dw 014,124  ;climbing koopa (green vertical)
      db force08 :  dw  175 : dw 014,124  ;climbing koopa (green vertical)
      db force04 :  dw  183 : dw 014,124  ;climbing koopa (green vertical)
      db force20 :  dw  190 : dw 023,125  ;climbing koopa (red horizontal)
      db force28 :  dw  200 : dw 020,126  ;climbing koopa (green vertical edge)

  db force18   :  dw  229   : dw 020,004  ;checkpoint top
  db available :  dw  229+2 : dw 020+3,005  ;checkpoint bar
  db force08   :  dw  229+5 : dw 020+2,031  ;checkpoint pole right


  db available  :  dw  241 : dw 026,129  ;door

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv009: equ $-objectlistlv009
  
 
  