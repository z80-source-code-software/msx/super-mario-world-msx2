objectlistlv022:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available :  dw  25 : dw 039,179  ;ghost small
  db available :  dw  30 : dw 039,179  ;ghost small
  db available :  dw  35 : dw 039,179  ;ghost small
  db available :  dw  40 : dw 039,179  ;ghost small
  db available :  dw  45 : dw 039,179  ;ghost small
  db available :  dw  50 : dw 039,179  ;ghost small
  db available :  dw  55 : dw 039,179  ;ghost small
  db available :  dw  60 : dw 039,179  ;ghost small
  db available :  dw  65 : dw 039,179  ;ghost small
  db available :  dw  70 : dw 039,179  ;ghost small
  db available :  dw  75 : dw 039,179  ;ghost small
  db available :  dw  80 : dw 039,179  ;ghost small
  db available :  dw  85 : dw 039,179  ;ghost small
  db available :  dw  90 : dw 039,179  ;ghost small
  db available :  dw  95 : dw 039,179  ;ghost small
  db available :  dw  100 : dw 039,179  ;ghost small
  db available :  dw  105 : dw 039,179  ;ghost small
  db available :  dw  110 : dw 039,179  ;ghost small
  db available :  dw  115 : dw 039,179  ;ghost small
  db available :  dw  120 : dw 039,179  ;ghost small
  db available :  dw  125 : dw 039,179  ;ghost small
  db available :  dw  130 : dw 039,179  ;ghost small
  db available :  dw  135 : dw 039,179  ;ghost small
  db available :  dw  140 : dw 039,179  ;ghost small
  db available :  dw  145 : dw 039,179  ;ghost small
  db available :  dw  150 : dw 039,179  ;ghost small
  
  db force28  :  dw  154 : dw 051,129  ;door   
  
  db available :  dw  155 : dw 039,179  ;ghost small
  db available :  dw  160 : dw 039,179  ;ghost small
 
 
  db force28  :  dw  216 : dw 051,129  ;door
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv022: equ $-objectlistlv022
  
 