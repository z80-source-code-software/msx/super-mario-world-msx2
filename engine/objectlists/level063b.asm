objectlistlv063b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  

	db available : dw 020 : dw 035,055	;yoshi coin
	db available : dw 025 : dw 032,320	;Exploding bubble (fish)
	db available : dw 030 : dw 049,104	;water (marioswim)
	db available : dw 044 : dw 032,320	;Exploding bubble (fish)
	db available : dw 059 : dw 033,320	;Exploding bubble (fish)
	db available : dw 069 : dw 031,302	;exploding bubble (mushroom)
	db available : dw 086 : dw 032,320	;Exploding bubble (fish)
	db available : dw 097 : dw 037,055	;yoshi coin
	db available : dw 104 : dw 032,320	;Exploding bubble (fish)
	db available : dw 111 : dw 037,036	;Mario exit (up down)

  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv063b: equ $-objectlistlv063b
