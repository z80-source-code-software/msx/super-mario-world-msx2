objectlistlv051:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

     db force02  :  dw  001 : dw 025,303  ;fishin lakitu
 ;    db force02  |  dw  002 | dw 025,303  ;fishin lakitu

  db  available :  dw  038 : dw 043-2,093  ;green flying koopa    

  db available :  dw  063 : dw 044,001  ;pink shell
  db available :  dw  067 : dw 044,066  ;pink naked koopa


   
  db available :  dw  074 : dw 030,027  ;questionmark animation red mushroom

     db available :  dw  080 : dw 023,055  ;yoshicoin 
     
     db available  :  dw  107 : dw 040-1,253  ;lakitu (pipe)

      db available :  dw  140 : dw 038,196  ;changing powerup

  db available  :  dw  168 : dw 038,057  ;questionmark animation (coin)

     db available  :  dw  179 : dw 036-1,253  ;lakitu (pipe)

  db available  :  dw  190 : dw 038,057  ;questionmark animation (coin)
  db available  :  dw  192 : dw 038,057  ;questionmark animation (coin)

     db available :  dw  194 : dw 042,055  ;yoshicoin   
  
  
  db available  :  dw  196 : dw 038,057  ;questionmark animation (coin)
  db available  :  dw  198 : dw 038,057  ;questionmark animation (coin)

     db available  :  dw  209 : dw 038-1,253  ;lakitu (pipe)
     db available  :  dw  239 : dw 040-1,253  ;lakitu (pipe)

;      db available |  dw  253 | dw 040,198  ;tube up and down ;no fucking way that I am implementing this shit!

     db available :  dw  258 : dw 027,055  ;yoshicoin

     db available  :  dw  281 : dw 038-1,253  ;lakitu (pipe)

  db force18   :  dw  313-2 : dw 024-3+1,004  ;checkpoint top
  db available :  dw  313+2-2 : dw 024+1,005  ;checkpoint bar
  db force04   :  dw  313+5-2 : dw 024-1+1,031  ;checkpoint pole right

     db available  :  dw  333 : dw 038-1,253  ;lakitu (pipe)

  db available  :  dw  362 : dw 038,139  ;coin special spawn
  db available  :  dw  365 : dw 038,139  ;coin special spawn



     db available  :  dw  373 : dw 034-1,253  ;lakitu (pipe)

  db force00   :  dw  373+1 : dw 034-1,036  ;tube cover up exit sprite

    db available  :  dw  375 : dw 044,076  ;p-box (blue-static)

  db available  :  dw  381 : dw 038,139  ;coin special spawn
  db available  :  dw  385 : dw 038,139  ;coin special spawn

  db  available :  dw  411 : dw 030-2,093  ;green flying koopa

  db force00   :  dw  433+1 : dw 038-1,036  ;tube cover up exit sprite

     db force02  :  dw  440 : dw 025,304  ;trowing lakitu

  db available  :  dw  454 : dw 038,057  ;questionmark animation (coin)
  db available  :  dw  456 : dw 038,057  ;questionmark animation (coin)

  db available :  dw  467 : dw 044,177  ;blue shell
     db available :  dw  468 : dw 012,055  ;yoshicoin

  db available  :  dw  478 : dw 038,057  ;questionmark animation (coin)
  db available  :  dw  480 : dw 038,057  ;questionmark animation (coin)

  db available :  dw  493 : dw 044,001  ;pink shell
  db available :  dw  497 : dw 044,066  ;pink naked koopa


;TODO: change block colors on this position

  db available   :  dw  499 : dw 040,305  ;color switcher (maplevel5-04a)


   db available   :  dw  518 : dw 044-1,087  ;blue rotating block
   db available   :  dw  520 : dw 042-1,087  ;blue rotating block
   db available   :  dw  522 : dw 040-1,087  ;blue rotating block
   db available   :  dw  524 : dw 042-1,087  ;blue rotating block
   db available   :  dw  526 : dw 044-1,087  ;blue rotating block
   
   
   db available   :  dw  538 : dw 044-1,087  ;blue rotating block
   db available   :  dw  540 : dw 042-1,087  ;blue rotating block
   db available   :  dw  542 : dw 040-1,087  ;blue rotating block
   db available   :  dw  544 : dw 038-1,087  ;blue rotating block
   db available   :  dw  546 : dw 040-1,087  ;blue rotating block
   db available   :  dw  548 : dw 042-1,087  ;blue rotating block
   db available   :  dw  550 : dw 044-1,087  ;blue rotating block






  db available   :  dw  567 : dw 040,008  ;Jumpin Piranha Plant


  db available   :  dw  570 : dw 040,306  ;color switcher (maplevel5-04a) (to left)

    db available :  dw  577 : dw 043,237  ;mini charging chuck 
 
    db available :  dw  587 : dw 030,055  ;yoshicoin   

  db force00   :  dw  601 : dw 028,007  ;endflag bar
  db force28   :  dw  601+1 : dw 028-1,006  ;endflag top    
    
    
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv051: equ $-objectlistlv051

