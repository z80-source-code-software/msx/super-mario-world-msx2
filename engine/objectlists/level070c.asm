objectlistlv070c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
	db available	:	dw 019	:	dw 037,178	;keyhole
	db available	:	dw 050	:	dw 033,127	;blockwall (16x16)
	db available	:	dw 052	:	dw 033,127	;blockwall (16x16)
	db available	:	dw 082	:	dw 035,174	;spiketop
	db available	:	dw 089	:	dw 030,174	;spiketop
	db available	:	dw 096	:	dw 025,174	;spiketop
	db available	:	dw 112	:	dw 022,250	;spikey
	db available	:	dw 138	:	dw 027,250	;spikey
	db available	:	dw 141	:	dw 029,174	;spiketop
	db available	:	dw 158	:	dw 034,061	;pink koopa
	db available	:	dw 185	:	dw 034,061	;pink koopa
	db available	:	dw 194	:	dw 028,174	;spiketop
	db available	:	dw 207	:	dw 025,250	;spikey
	db available	:	dw 258	:	dw 047,250	;spikey
	db available	:	dw 263	:	dw 009,289	;greenmushroom (static)
	db available	:	dw 265	:	dw 009,289	;greenmushroom (static)
	db available	:	dw 267	:	dw 009,289	;greenmushroom (static)
	db available	:	dw 274	:	dw 047,054	;mario exit (right)




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv070c: equ $-objectlistlv070c

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    046*8      :     db 64
dw    273*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
