objectlistlv029b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  007 : dw 008,222  ;drybones

  db force25  :  dw  008 : dw 008,129  ;door
 
  db available :  dw  038 : dw 006,221  ;twhomp 
 
  db available :  dw  046 : dw 041,128  ;flame 
 
  db available :  dw  058 : dw 022,221  ;twhomp
  db available :  dw  072 : dw 028,221  ;twhomp
  

 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv029b: equ $-objectlistlv029b

