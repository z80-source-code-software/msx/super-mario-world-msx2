objectlistlv031:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
  db available  :  dw  006 : dw 017,028  ;questionmark animation (red flower) 
  db available  :  dw  008 : dw 017,028  ;questionmark animation (red flower) 
 
 
  db available :  dw  016 : dw 017,058  ;questionmark animation (green yoshi egg) 

  db available :  dw  024 : dw 017,029  ;questionmark animation (feather)
 
  db available  :  dw  025 : dw 003,145  ;yoshi house exit 
 
  db available :  dw  026 : dw 017,029  ;questionmark animation (feather) 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv031: equ $-objectlistlv031

