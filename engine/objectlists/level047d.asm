objectlistlv047d:
;note that on this map mario goes from right to left
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
    db available :  dw  040 : dw 015,294  ;ludwig (boss)

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv047d: equ $-objectlistlv047d

