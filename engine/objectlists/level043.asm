objectlistlv043:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  032    : dw 030,157    ;superkoopa (flying in curve)
  db available :  dw  033 : dw 032,078  ;coin (static)
  db available :  dw  035 : dw 032,078  ;coin (static)


  db available :  dw  048    : dw 042,154    ;superkoopa (walking)

  db available :  dw  060 : dw 030,078  ;coin (static)

  db available :  dw  073 : dw 040,065  ;brown naked koopa

  db available :  dw  078 : dw 032,078  ;coin (static)
  db available :  dw  080 : dw 032,078  ;coin (static)

  db available :  dw  090 : dw 042,085  ;green shell

  db available :  dw  098 : dw 027,029  ;questionmark animation (feather)

  db available :  dw  104    : dw 030,157    ;superkoopa (flying in curve)
  db available :  dw  108    : dw 028,157    ;superkoopa (flying in curve)


  db available :  dw  123 : dw 008,055  ;yoshicoin 


  db available :  dw  130    : dw 030,157    ;superkoopa (flying in curve)
  db available :  dw  148    : dw 029,157    ;superkoopa (flying in curve)
  db available :  dw  160    : dw 031,157    ;superkoopa (flying in curve)
  db available :  dw  161 : dw 030,055  ;yoshicoin

  db available :  dw  164    : dw 027,157    ;superkoopa (flying in curve)
  db available :  dw  170    : dw 029,157    ;superkoopa (flying in curve)

  db available :  dw  199 : dw 027,001  ;pink shell
  db available  :  dw  202 : dw 027,069  ;blue naked koopa 

  db available :  dw  211    : dw 025,157    ;superkoopa (flying in curve)
 
  db available :  dw  217 : dw 027,001  ;pink shell
  db available  :  dw  221 : dw 027,069  ;blue naked koopa 

  db available :  dw  237    : dw 021,157    ;superkoopa (flying in curve)

  db available :  dw  238    : dw 042,156    ;superkoopa (walking no feather)

  db available :  dw  244 : dw 024,177  ;blue shell
  db available :  dw  246    : dw 021,157    ;superkoopa (flying in curve)  
  db available  :  dw  248 : dw 021,069  ;blue naked koopa 

  db available :  dw  256    : dw 022,157    ;superkoopa (flying in curve)  

  db available :  dw  259    : dw 042,154    ;superkoopa (walking)

  db available :  dw  262 : dw 031,177  ;blue shell
  db available  :  dw  264 : dw 031,069  ;blue naked koopa 

  db available :  dw  265 : dw 027,177  ;blue shell
  db available  :  dw  269 : dw 027,069  ;blue naked koopa 

  db available :  dw  277 : dw 024,055  ;yoshicoin

  db force18   :  dw  301 : dw 030-3,004  ;checkpoint top
  db available :  dw  301+2 : dw 030,005  ;checkpoint bar
  db force04   :  dw  301+5 : dw 030-1,031  ;checkpoint pole right

  db available :  dw  323 : dw 037,068  ;green naked koopa
 
  db available :  dw  343 : dw 031,034  ;exclamationmark (green)  

  db available  :  dw  347 : dw 037,069  ;blue naked koopa

  db available :  dw  353 : dw 025,078  ;coin (static)
  db available :  dw  355 : dw 025,078  ;coin (static)

  db available  :  dw  367 : dw 037,069  ;blue naked koopa

  db available :  dw  371 : dw 025,078  ;coin (static)
  db available :  dw  375 : dw 044-3,036  ;tube cover up exit sprite  
  db available :  dw  377 : dw 025,078  ;coin (static)

  db available :  dw  397    : dw 026,157    ;superkoopa (flying in curve) 
  db available :  dw  398    : dw 024,157    ;superkoopa (flying in curve) 
  db available :  dw  399    : dw 021,157    ;superkoopa (flying in curve) 
  db available :  dw  401    : dw 018,157    ;superkoopa (flying in curve) 
  
  db available :  dw  432    : dw 008,157    ;superkoopa (flying in curve) 
  
  
  db available :  dw  460    : dw 027,157    ;superkoopa (flying in curve) 
  db available :  dw  465    : dw 019,157    ;superkoopa (flying in curve) 
  db available :  dw  482    : dw 028,157    ;superkoopa (flying in curve) 
  db available :  dw  485    : dw 020,157    ;superkoopa (flying in curve) 
  db available :  dw  492    : dw 026,157    ;superkoopa (flying in curve) 
  db available :  dw  495    : dw 020,157    ;superkoopa (flying in curve) 

  db available :  dw  517 : dw 027,065  ;brown naked koopa

  db available :  dw  531    : dw 030,157    ;superkoopa (flying in curve) 

  db available :  dw  543 : dw 035,065  ;brown naked koopa

  db available :  dw  548    : dw 029,157    ;superkoopa (flying in curve) 
  db available :  dw  556    : dw 032,157    ;superkoopa (flying in curve) 

  db available :  dw  560 : dw 037,055  ;yoshicoin

  db available :  dw  576    : dw 030,157    ;superkoopa (flying in curve) 
  
  
  db available :  dw  594    : dw 025,157    ;superkoopa (flying in curve) 
  db available :  dw  596    : dw 022,157    ;superkoopa (flying in curve) 
  db available :  dw  599    : dw 018,157    ;superkoopa (flying in curve) 

  db force00   :  dw  600 : dw 025-3+4,007  ;endflag bar
  db available :  dw  600+1 : dw 025-3+1+1,006  ;endflag top 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv043: equ $-objectlistlv043

