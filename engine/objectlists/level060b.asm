objectlistlv060b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 042 : dw 035,091	;red flying koopa
	db available : dw 064 : dw 030,091	;red flying koopa
	db available : dw 093 : dw 032,091	;red flying koopa
	db available : dw 108 : dw 029,091	;red flying koopa
	db available : dw 121 : dw 034,091	;red flying koopa
	db available : dw 147 : dw 032,091	;red flying koopa
	db available : dw 170 : dw 033,091	;red flying koopa
	db available : dw 185 : dw 032,091	;red flying koopa
	db available : dw 199 : dw 036,091	;red flying koopa
	db available : dw 217 : dw 044,036	;Mario exit (up down)


  
  
  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060b: equ $-objectlistlv060b
