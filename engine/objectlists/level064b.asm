objectlistlv064b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 033 : dw 047,325	;Blockboo
	db available : dw 035 : dw 040,027	;red shroom box
	db available : dw 037 : dw 047,325	;Blockboo
	db available : dw 063 : dw 037,326	;Snakeboo
	db available : dw 074 : dw 028,129	;door
	db available : dw 098 : dw 045,326	;Snakeboo
	db available : dw 128 : dw 032,326	;Snakeboo
	db available : dw 159 : dw 028,326	;Snakeboo
	db available : dw 161 : dw 043,325	;Blockboo
	db available : dw 182 : dw 032,325	;Blockboo
	db available : dw 186 : dw 040,042	;smileybox (extra life)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv064b: equ $-objectlistlv064b
