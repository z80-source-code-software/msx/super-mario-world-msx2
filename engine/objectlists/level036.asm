objectlistlv036:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
 
 
    db available  :  dw  033 : dw 035,214  ;eerie (left and right)
    db available  :  dw  034 : dw 042,214  ;eerie (left and right)
 

  db available  :  dw  064 : dw 032,028  ;questionmark animation (red flower)
  db available  :  dw  065 : dw 046-20,211  ;small ghostring 
  db available   :  dw  066 : dw 045,055  ;yoshi coin

  db available  :  dw  099 : dw 044,185  ;small ghost (following mario) 
  db available  :  dw  120 : dw 047,185  ;small ghost (following mario) 
  db available  :  dw  127 : dw 023,185  ;small ghost (following mario) 
  db available  :  dw  144 : dw 045,185  ;small ghost (following mario) 
  db available  :  dw  159 : dw 040,185  ;small ghost (following mario) 
  
  db available   :  dw  174 : dw 031,055  ;yoshi coin  
  
  db available  :  dw  179 : dw 032,185  ;small ghost (following mario) 
  db available  :  dw  191 : dw 046,185  ;small ghost (following mario) 
  db available  :  dw  198 : dw 041,185  ;small ghost (following mario) 

  db available  :  dw  235 : dw 039,212  ;bigboo  

  db available   :  dw  258 : dw 011,055  ;yoshi coin

        db available :  dw  270 : dw 040,029  ;questionmark animation (feather)

  db available  :  dw  286 : dw 041,212  ;bigboo 
 
    db available  :  dw  316 : dw 011,214  ;eerie (left and right)
    
    
  db available  :  dw  322 : dw 040-10,211  ;small ghostring    
        db available  :  dw  323 : dw 040,119  ;multicoin box    

  db available   :  dw  324 : dw 032,055  ;yoshi coin    


  db available  :  dw  336 : dw 032,185  ;small ghost (following mario) 
    
    
    db available  :  dw  343 : dw 011,214  ;eerie (left and right) 

  db force25  :  dw  346 : dw 044,129  ;door 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv036: equ $-objectlistlv036

