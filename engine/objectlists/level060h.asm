objectlistlv060h:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 035 : dw 051,205	;spring
	db available : dw 067 : dw 035,002	;Rex
	db available : dw 071 : dw 031,002	;Rex
	db available : dw 073 : dw 039,002	;Rex
	db available : dw 075 : dw 043,002	;Rex
	db available : dw 078 : dw 047,002	;Rex
	db available : dw 103 : dw 033,007	;Endflagbar
	db available : dw 104 : dw 032,006	;Endflagtop
  
  
  
  

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv060h: equ $-objectlistlv060h
