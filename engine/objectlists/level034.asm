objectlistlv034:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  003 : dw 032,103  ;lava (mario killer)

  db force26  :  dw  020 : dw 048-1,239  ;floating skull
  
  
  
  db available  :  dw  051 : dw 048+2,240  ;blargh
  
      db available :  dw  070 : dw 038-6,041  ;flying box (red flower)   
  
  db available  :  dw  074 : dw 048+2,240  ;blargh

  db force30  :  dw  090 : dw 034,028  ;questionmark animation (red flower)

  db available   :  dw  094 : dw 044,008  ;Jumpin Piranha Plant
  db available   :  dw  102 : dw 036,008  ;Jumpin Piranha Plant
   
  
   db available  :  dw  112 : dw 032-1,231  ;maxsheertile (right)

    db available :  dw  120 : dw 028-2,169  ;cavekoopa 

  db available   :  dw  122 : dw 044,055  ;yoshi coin  
  
  db available  :  dw  127 : dw 048-1,239  ;floating skull
 
db available  :  dw  145 : dw 048+2,240  ;blargh 


  db available :  dw  188 : dw 010,024  ;moon powerup

db available  :  dw  207 : dw 036+2,240  ;blargh 
db available  :  dw  216 : dw 036+2,240  ;blargh 

db available  :  dw  257 : dw 038,241  ;secret box (coin) 
 
         db available :  dw  262 : dw 038,029  ;questionmark animation (feather)

  db force26  :  dw  293 : dw 048-1,239  ;floating skull
  db available  :  dw  308 : dw 048+2,240  ;blargh 


  db available  :  dw  325 : dw 018-1,171  ;mediumsheertile (right) 
  db available  :  dw  346-1 : dw 021-4,149  ;mediumsheertile (left) 

  db available   :  dw  347 : dw 035,055  ;yoshi coin

    db available :  dw  352 : dw 038-2,169  ;cavekoopa

    db available :  dw  376 : dw 041-3,242  ;piranjaplant2 
    
    db available :  dw  371 : dw 008-2,169  ;cavekoopa 
    
    
    db available :  dw  377 : dw 008-2,169  ;cavekoopa 

  db available   :  dw  380-1 : dw 007-1,054  ;tubeexit to the right

    db available :  dw  382 : dw 043-3,242  ;piranjaplant2 
    db available :  dw  394 : dw 043-3,242  ;piranjaplant2 

  db available  :  dw  409 : dw 048-1,239  ;floating skull ;no blarghs in the area so we can use available sprite spots


  db available  :  dw  416 : dw 046,108  ;blockwall for mario (right)

        db available  :  dw  436 : dw 036,119  ;multicoin box

    db available :  dw  439 : dw 042-2,169  ;cavekoopa 

    db available :  dw  443 : dw 039-3,242  ;piranjaplant2

  db available  :  dw  446 : dw 046,109  ;blockwall for mario (left)

  db available :  dw  477 : dw 044-3,036  ;tube cover up exit sprite

  db available :  dw  480 : dw 038,167  ;diagonal block (helper) 
  
  db available :  dw  520-1 : dw 047-1,165  ;diagonal block (left)  
 ; db available |  dw  529 | dw 047-1,166  ;diagonal block (right)   ;not enough horizontal lines left!!!

  db available :  dw  533 : dw 032,033  ;red block
  db available :  dw  535 : dw 032,058  ;questionmark animation (green yoshi egg)
  db available :  dw  537 : dw 032,033  ;red block
  
  db available :  dw  541 : dw 047-1,165  ;diagonal block (left) 
  
    db available :  dw  543 : dw 032,001  ;pink shell
   db available  :  dw  545 : dw 032,069  ;blue naked koopa  
  
  
  db available :  dw  547 : dw 047-1,166  ;diagonal block (right)  




    db available   :  dw  570+1 : dw 044-1,036  ;tube cover up exit sprite
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv034: equ $-objectlistlv034

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    378*8      :     db 64
dw    497*8      :     db 32
dw    000         ;defines the end of the list COMPULSORY!!

