objectlistlv075:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist


  db available  :  dw  004 : dw 020,343  ;spike lava killer

  
  
	db available	:	dw 010	:	dw 039,055	;yoshi coin
	db available	:	dw 042	:	dw 047,344	;special platform
	db available	:	dw 048	:	dw 010,055	;yoshi coin
	db available	:	dw 067	:	dw 010,220	;ball n chain
	db available	:	dw 078	:	dw 006,032	;yellow !
	db available	:	dw 095	:	dw 023,220	;ball n chain
	db available	:	dw 121	:	dw 010,220	;ball n chain
	db available	:	dw 146	:	dw 019,220	;ball n chain
	db available	:	dw 157	:	dw 048,128	;flame
	db available	:	dw 167	:	dw 008,220	;ball n chain
	db available	:	dw 192	:	dw 010,055	;yoshi coin
	db available	:	dw 222	:	dw 007,269	;door (spriteless)
	db available	:	dw 224	:	dw 038,004	;Checkpointtop
	db available	:	dw 226	:	dw 041,005	;Checkpointbar
	db available	:	dw 229	:	dw 040,031	;checkpointright
	db available	:	dw 236	:	dw 045,055	;yoshi coin
	db available	:	dw 240	:	dw 045,055	;yoshi coin
	db available	:	dw 249	:	dw 044,269	;door (spriteless)


  
  
  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv075: equ $-objectlistlv075

