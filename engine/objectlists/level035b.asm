objectlistlv035b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  003 : dw 032,103  ;lava (mario killer)


  db available  :  dw  020 : dw 048-1,239  ;floating skull 


 ; db available |  dw  022 | dw 043-2,078  ;coin (static) 
  db available :  dw  030 : dw 043-2,102  ;ultracoin (static)

  db available :  dw  043 : dw 042-2,078  ;coin (static) 
  db available :  dw  050 : dw 042-2,102  ;ultracoin (static)

  db available :  dw  055 : dw 038-2,078  ;coin (static) 
  db available :  dw  068 : dw 037-2,102  ;ultracoin (static)

  db available :  dw  083 : dw 039-2,078  ;coin (static) 
  db available :  dw  090 : dw 042-2,102  ;ultracoin (static)

  db available :  dw  104 : dw 040-2,078  ;coin (static) 
  db available :  dw  113 : dw 041-2,102  ;ultracoin (static)

  db available :  dw  124 : dw 039-2,078  ;coin (static) 
  db available :  dw  133 : dw 032-2,102  ;ultracoin (static)

  db available :  dw  145 : dw 024-2,078  ;coin (static) 
  db available :  dw  159 : dw 036-2,102  ;ultracoin (static)

  db available :  dw  166 : dw 043-2,078  ;coin (static) 


  db available   :  dw  177 : dw 041,055  ;yoshi coin
  db available   :  dw  182-1 : dw 044-1,054  ;tubeexit to the right 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv035b: equ $-objectlistlv035b

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    180*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!  