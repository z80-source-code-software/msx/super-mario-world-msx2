objectlistlv000:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     
  
    ;db available   |  dw  028 | dw 39,132  ;falling block 
    ;db available   |  dw  028 | dw 41,088  ;blue rotating block 
    ;db available   |  dw  024 | dw 33,090  ;flying box (alternate movement) 
  
  db force28   :  dw  030 : dw 040-3,003  ;diagonal background sprites donut plains     
  db force26   :  dw  034 : dw 034-3,019  ;sliding guy 
  db force00   :  dw  055 : dw 035-1,048  ;banzai bill                  
  db available :  dw  071 : dw 043-3,002  ;rex 
  db available :  dw  079 : dw 036-3,041  ;flying box (red flower) 
  db available :  dw  085 : dw 043-3,002  ;rex 
  db available :  dw  110 : dw 035-3,002  ;rex       
  db available :  dw  136 : dw 035-3,002  ;rex
  db force20   :  dw  144 : dw 040-3,003  ;diagonal background sprites donut plains
  db force24   :  dw  152 : dw 040-3,003  ;diagonal background sprites donut plains
  db available :  dw  158 : dw 043-3,002  ;rex
  db force28   :  dw  160 : dw 040-3,003  ;diagonal background sprites donut plains
  db available :  dw  173 : dw 043-3,002  ;rex
  db available :  dw  190 : dw 047-5,043  ;powerpopup (red mushroom)
  db available :  dw  222 : dw 043-3,002  ;rex
;  db force21   |  dw  223 | dw 041-3,009  ;tube cover up (goes in front of Jumpin Piranha Plant)
  db available   :  dw  224 : dw 041-3,008  ;Jumpin Piranha Plant
  db available :  dw  240 : dw 042-3,036  ;tube cover up exit sprite

  db available :  dw  275 : dw 043-3,002  ;rex
 ; db force21   |  dw  276 | dw 039-3,009  ;tube cover up (goes in front of Jumpin Piranha Plant)
  db available   :  dw  277 : dw 039-3,008  ;Jumpin Piranha Plant

  db force18   :  dw  291+34 : dw 037-3,004  ;checkpoint top
  db available :  dw  293+34 : dw 040-3,005  ;checkpoint bar
  db force04   :  dw  296+34 : dw 039-3,031  ;checkpoint pole right
;  db available |  dw  355    | dw 044-1,032    ;exclamationmark
  db available :  dw  360    : dw 036-1,032    ;exclamationmark (yellow)


  db available :  dw  365    : dw 043-3,002  ;rex
  db available :  dw  338+69 : dw 043-3,002  ;rex
  db force23   :  dw  339+69 : dw 030-3,037  ;diagonal background sprites 2
  db available :  dw  344+69 : dw 043-3,002  ;rex
  db available :  dw  349+69 : dw 043-3,002  ;rex
  db available :  dw  354+69 : dw 039-3,002  ;rex


  db available :  dw  381+69 : dw 007-3,024  ;moon powerup
  db force00   :  dw  460    : dw 037-1,048    ;banzai bill 
  
  db available :  dw  399+69 : dw 043-3,002  ;rex
  db available :  dw  472    : dw 027-1,042    ;smileybox (extra life)
  db available :  dw  404+69 : dw 045-3,001  ;pink shell

  db available :  dw  409+69 : dw 047-3,032  ;exclamationmark
  db available :  dw  411+69 : dw 047-3,032  ;exclamationmark
  db available :  dw  439+69 : dw 047-3,032  ;exclamationmark
  db force00   :  dw  512    : dw 037-1,048    ;banzai bill 
  db available :  dw  441+69 : dw 047-3,032  ;exclamationmark

  db available :  dw  462+69 : dw 043-3,002  ;rex
  db available :  dw  464+69 : dw 033-3,028  ;questionmark animation red flower
  db available :  dw  500+69 : dw 043-3,002  ;rex
  db available :  dw  512+69 : dw 043-3,002  ;rex
  db force00   :  dw  599    : dw 037-1,048    ;banzai bill 
;  db force26   |  dw  553+58 | dw 039-3,009  ;tube cover up (goes in front of Jumpin Piranha Plant)
  db available   :  dw  554+58 : dw 039-3,008  ;Jumpin Piranha Plant
  db available :  dw  568+57 : dw 036-3,002  ;rex
  db available :  dw  583+57 : dw 044-3,026  ;snapping chuck
  db force00   :  dw  590+59 : dw 032-3,007  ;endflag bar
  db available :  dw  591+59 : dw 028-3,006  ;endflag top
  
 
  
  db  00       :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist
lenghtobjectlistlv000: equ $-objectlistlv000
  
  