objectlistlv002:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     

  db available :  dw  033 : dw 045,001  ;pink shell
  db available :  dw  040 : dw 038,060  ;brown coopa
  db available :  dw  046 : dw 038,060  ;brown coopa
  db available :  dw  050 : dw 038,060  ;brown coopa
  
  db available :  dw  054 : dw 038,060  ;brown coopa
;  db available |  dw  054 | dw 038,060  ;brown coopa
;  db available |  dw  058 | dw 038,060  ;brown coopa
  
  db available :  dw  074 : dw 040,057  ;questionmark animation (coin)
  db available :  dw  076 : dw 040,032  ;exclamationmark
  db available :  dw  078 : dw 040,057  ;questionmark animation (coin)
  
  db available :  dw  104 : dw 042,057  ;questionmark animation (coin)
  db available :  dw  106 : dw 042,058  ;questionmark animation (green yoshi egg)

  db available :  dw  146 : dw 045,001  ;pink shell
  
  db available :  dw  149 : dw 040,055  ;yoshicoin 
  
  db available :  dw  150 : dw 045,065  ;brown naked koopa

  
  db available :  dw  185 : dw 045,085  ;green shell
  db available :  dw  189 : dw 045,068  ;green naked koopa
  
  
  db available :  dw  190 : dw 040,055  ;yoshicoin

  db available :  dw  247 : dw 035,025  ;charging chuck

  db available :  dw  256 : dw 032,055  ;yoshicoin

  db force18   :  dw  283 : dw 038,004  ;checkpoint top
  db available :  dw  285 : dw 041,005  ;checkpoint bar
  db force04   :  dw  288 : dw 040,031  ;checkpoint pole right

  
  db available :  dw  310 : dw 042,057  ;questionmark animation (coin)
  db available :  dw  312 : dw 042,027  ;questionmark animation red mushroom
  db available :  dw  314 : dw 042,057  ;questionmark animation (coin)

  db available :  dw  338 : dw 038,070  ;montey mole (jumping)
  db available :  dw  347 : dw 044,072  ;montey mole (follow)
  db available :  dw  352 : dw 032,074  ;smiley box (climbing plant)

;upper secret objects 
  db available :  dw  356 : dw 007,078  ;coin (static)
  db available :  dw  360 : dw 007,078  ;coin (static)
  db available :  dw  364 : dw 007,055  ;yoshicoin
  db available :  dw  370 : dw 007,078  ;coin (static)
  db available :  dw  374 : dw 007,078  ;coin (static)


  db available :  dw  379 : dw 041,072  ;montey mole (follow)

  db available :  dw  384 : dw 032,078  ;coin (static)
  db available :  dw  388 : dw 032,078  ;coin (static)
  db available :  dw  392 : dw 032,055  ;yoshicoin


  
  db available :  dw  397 : dw 036,032  ;exclamationmark
  db available :  dw  407 : dw 048,032  ;exclamationmark
  db available :  dw  409 : dw 048,032  ;exclamationmark
 
  db available :  dw  421 : dw 046,071  ;montey mole (jumping from ground)
  
  db force00   :  dw  432 : dw 043,036  ;tube cover up exit sprite
  
  db available :  dw  441 : dw 046,071  ;montey mole (jumping from ground)

 ; db force20   |  dw  491 | dw 042,086  ;tube cover up green (goes in front of Jumpin Piranha Plant)
 ; db force25   |  dw  492 | dw 042,008  ;Jumpin Piranha Plant

  db available :  dw  508 : dw 034,084  ;p-box (blue-static)

  db available :  dw  515 : dw 040,078  ;coin (static)
  db available :  dw  517 : dw 038,083  ;supercoin (static)
  
  db available :  dw  518 : dw 044,025  ;charging chuck

  db available :  dw  527 : dw 038,078  ;coin (static)

  db force00   :  dw  533 : dw 033,007  ;endflag bar
  db force28   :  dw  534 : dw 029,006  ;endflag top

  db  00       :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv002: equ $-objectlistlv002
  
  