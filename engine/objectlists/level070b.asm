objectlistlv070b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  

 ;   db available  :  dw  003 : dw 030,130  ;static camerascroller
    db available  :  dw  004 : dw 040,152  ;blocktileanimator
    db available  :  dw  005 : dw 040,168  ;mariocrusher    
  
  
	db available	:	dw 004	:	dw 024,225	;secret box (1up)
;	db available	:	dw 016	:	dw 044,127	;blockwall (16x16)
	db available	:	dw 030	:	dw 037,029	;feather box
	db available	:	dw 032	:	dw 037,029	;feather box
	db available	:	dw 044	:	dw 043,169	;cavekoopa
	db available	:	dw 067	:	dw 025,170	;bat
	db available	:	dw 075	:	dw 037,061	;pink koopa
	db available	:	dw 086	:	dw 029,170	;bat
	db available	:	dw 108	:	dw 032,169	;cavekoopa
	db available	:	dw 115	:	dw 042,055	;yoshi coin
	db available	:	dw 118	:	dw 027,318	;Megamole
	db available	:	dw 138	:	dw 033,170	;bat
	db available	:	dw 154	:	dw 031,318	;Megamole
	db available	:	dw 158	:	dw 008,055	;yoshi coin
	db available	:	dw 179	:	dw 036,169	;cavekoopa
	db available	:	dw 207	:	dw 024,318	;Megamole
	db available	:	dw 227	:	dw 031,169	;cavekoopa
	db available	:	dw 237	:	dw 020,170	;bat
	db available	:	dw 245	:	dw 032,169	;cavekoopa
	db available	:	dw 265	:	dw 020,170	;bat
	db available	:	dw 294	:	dw 041,318	;Megamole
	db available	:	dw 306	:	dw 044,289	;greenmushroom (static)
	db available	:	dw 308	:	dw 032,169	;cavekoopa
	db available	:	dw 328	:	dw 039,169	;cavekoopa
	db available	:	dw 339	:	dw 022,169	;cavekoopa
	db available	:	dw 358	:	dw 016,318	;Megamole
	db available	:	dw 368	:	dw 027,170	;bat
	db available	:	dw 389	:	dw 032,170	;bat
	db available	:	dw 400	:	dw 013,318	;Megamole
	db available	:	dw 403	:	dw 033,029	;feather box
	db available	:	dw 404	:	dw 017,289	;greenmushroom (static)
	db available	:	dw 405	:	dw 033,029	;feather box
	db available	:	dw 407	:	dw 033,029	;feather box
	db available	:	dw 418	:	dw 040,169	;cavekoopa
	db available	:	dw 448	:	dw 027,054	;mario exit (right)

  


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv070b: equ $-objectlistlv070b

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    009*8      :     db 64
dw    447*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
