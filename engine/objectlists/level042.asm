objectlistlv042:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

    db available  :  dw  003 : dw 030,130  ;static camerascroller


  db available  :  dw  049 : dw 044-1,275  ;special platform (maplevel4-01a)
  
  db available   :  dw  054 : dw 040,055  ;yoshi coin   
  
  
  db available  :  dw  069 : dw 040-1,276  ;special platform (maplevel4-01a) (alternate tileset)

  db  available :  dw  074 : dw 038,009  ;red flying koopa  (up and down)

  db available    :  dw  075 : dw 030,078  ;coin (static)

  db  available :  dw  086 : dw 037,279    ;square flattened platform (dropping)
  db available  :  dw  096 : dw 036-1,277  ;special platform (maplevel4-01a) (wide)

  db  available :  dw  103 : dw 034,279    ;square flattened platform (dropping)

  db  available :  dw  120 : dw 031,009  ;red flying koopa  (up and down)

  db available   :  dw  126 : dw 024,055  ;yoshi coin

  db available  :  dw  142 : dw 026-1,276  ;special platform (maplevel4-01a) (alternate tileset)

  db available   :  dw  147 : dw 026,055  ;yoshi coin
  
  db available :  dw  157 : dw 028,033  ;exclamationmark (red)  
  
  db available  :  dw  159 : dw 020,094  ;green flying koopa (to left only)  
  
  db available :  dw  161 : dw 028,033  ;exclamationmark (red)  
  db available :  dw  163 : dw 028,033  ;exclamationmark (red)  
  db available :  dw  167 : dw 028,033  ;exclamationmark (red)  

  db available  :  dw  168 : dw 021,094  ;green flying koopa (to left only)    
  
  db available :  dw  169 : dw 028,033  ;exclamationmark (red)  

  db available  :  dw  170 : dw 019,094  ;green flying koopa (to left only)  
  db available  :  dw  175 : dw 019,094  ;green flying koopa (to left only)  
  db available  :  dw  180 : dw 019,094  ;green flying koopa (to left only)  
  db available  :  dw  188 : dw 019,094  ;green flying koopa (to left only)  
  db available  :  dw  190 : dw 019,094  ;green flying koopa (to left only)  
  db available  :  dw  199 : dw 019,094  ;green flying koopa (to left only)  
  
  
  db available  :  dw  207 : dw 026-1,275  ;special platform (maplevel4-01a)
  
  db available    :  dw  213 : dw 023,078  ;coin (static)  
;  db available    |  dw  221 | dw 023,078  ;coin (static)  
  
  db available  :  dw  223 : dw 019-1,276  ;special platform (maplevel4-01a) (alternate tileset)

  db available    :  dw  227 : dw 016,078  ;coin (static)

  db available   :  dw  228 : dw 029,055  ;yoshi coin

  db available    :  dw  230 : dw 024,078  ;coin (static)
 ; db available    |  dw  237 | dw 015,078  ;coin (static)

  db available  :  dw  239 : dw 011-1,275  ;special platform (maplevel4-01a)
  
  db available    :  dw  243 : dw 007,078  ;coin (static)  
;  db available    |  dw  245 | dw 007,078  ;coin (static)  
  
  db available  :  dw  260 : dw 038-1,276  ;special platform (maplevel4-01a) (alternate tileset)

  db available   :  dw  265 : dw 026,055  ;yoshi coin


  db available  :  dw  268 : dw 018,094  ;green flying koopa (to left only)
  db available  :  dw  273 : dw 026,094  ;green flying koopa (to left only)
  db available  :  dw  277 : dw 031,094  ;green flying koopa (to left only)

  db available :  dw  283    : dw 010,042    ;smileybox (extra life)
  db available :  dw  287    : dw 018,281    ;smileybox (multicoin) 

  db available  :  dw  293 : dw 023,278  ;special tileanimator (maplevel04-01a)  
  
  db available :  dw  297    : dw 026,280    ;smileybox (feather) 
  db available :  dw  299    : dw 018,281    ;smileybox (multicoin) 
  db available :  dw  303    : dw 010,281    ;smileybox (multicoin) 

  db available  :  dw  314 : dw 014-1,277  ;special platform (maplevel4-01a) (wide)

  db  available :  dw  323 : dw 015,009  ;red flying koopa  (up and down)

  db available   :  dw  337 : dw 017,055  ;yoshi coin


  db  available :  dw  339 : dw 017,279    ;square flattened platform (dropping)

  db  available :  dw  379 : dw 031,091  ;red flying koopa 
  db  available :  dw  385 : dw 039,091  ;red flying koopa 
  db  available :  dw  389 : dw 033,091  ;red flying koopa 


  db available :  dw  396+1 : dw 040-3,036  ;tube cover up exit sprite

  db  available :  dw  403 : dw 038,096  ;green special box   
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv042: equ $-objectlistlv042

