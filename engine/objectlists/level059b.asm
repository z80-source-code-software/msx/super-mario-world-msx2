objectlistlv059b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 
  
	db available : dw 012 : dw 036,042	;smileybox (extra life)
	db available : dw 016 : dw 047,104	;water (marioswim)
	db available : dw 017 : dw 046,105	;Water (coverupsprite)
	db available : dw 042 : dw 044,259	;Dolphin (left only)
	db available : dw 052 : dw 044,257	;porcupuffer
	db available : dw 062 : dw 044,259	;Dolphin (left only)
	db available : dw 072 : dw 044,259	;Dolphin (left only)
	db available : dw 085 : dw 044,259	;Dolphin (left only)
	db available : dw 098 : dw 044,259	;Dolphin (left only)
	db available : dw 120 : dw 037,162	;exit (upwards)

	
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv059b: equ $-objectlistlv059b
