objectlistlv032:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available   :  dw  041 : dw 048,174  ;spiketop

  db available :  dw  047 : dw 026,170  ;bat (flying turd) 
  db available :  dw  052 : dw 026,170  ;bat (flying turd) 

  db available  :  dw  069 : dw 041-2,169  ;cavekoopa
 

  db available :  dw  072 : dw 026,170  ;bat (flying turd)
  
      db available :  dw  076 : dw 035-6,041  ;flying box (red flower)   
   
  db available :  dw  088 : dw 026,170  ;bat (flying turd) 
  db available :  dw  094 : dw 026,170  ;bat (flying turd) 

  db available :  dw  114 : dw 028,001  ;pink shell
  db available :  dw  116 : dw 036,177  ;blue shell

  db available  :  dw  124 : dw 048-3,169  ;cavekoopa
  
  db available :  dw  130 : dw 038,057  ;questionmark animation (coin)  
  
  db available  :  dw  138 : dw 048-3,169  ;cavekoopa

  db available :  dw  144 : dw 034,055  ;yoshicoin   
  
  db available  :  dw  150 : dw 036-3,169  ;cavekoopa
  db available  :  dw  160 : dw 048-3,169  ;cavekoopa
  db available  :  dw  162 : dw 036-3,169  ;cavekoopa
  
  db available :  dw  164 : dw 048,043  ;powerpopup (red mushroom)  
  
  db available  :  dw  182 : dw 036-3,169  ;cavekoopa

  db available :  dw  200 : dw 044,033  ;exclamationmark (red)


  db available :  dw  202 : dw 050,057  ;questionmark animation (coin) 

  db available :  dw  204 : dw 040,033  ;exclamationmark (red)
  db available :  dw  206 : dw 038,033  ;exclamationmark (red)
  db available :  dw  208 : dw 036,033  ;exclamationmark (red)

  db available :  dw  210 : dw 030,074  ;smiley box (climbing plant) 

  db available :  dw  212 : dw 036,033  ;exclamationmark (red)
 
  db available  :  dw  215 : dw 048-3,169  ;cavekoopa 
 
   db available :  dw  232 : dw 015,178  ;keyhole  
 
  db available :  dw  237 : dw 048,009  ;red flying koopa (up and down)  
 
  db force00   :  dw  271-1 : dw 048,054  ;tubeexit to the right  
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv032: equ $-objectlistlv032


;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    269*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!  


