objectlistlv068:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  
;	db available	:	dw 009	:	dw 008,104	;water (marioswim)


	db available	:	dw 020	:	dw 022,337	;Watercannon
	db available	:	dw 021	:	dw 016,029	;feather box
	db available	:	dw 030	:	dw 025,337	;Watercannon
	db available	:	dw 072	:	dw 028,337	;Watercannon
	db available	:	dw 088	:	dw 023,036	;Mario exit (up down)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv068: equ $-objectlistlv068
