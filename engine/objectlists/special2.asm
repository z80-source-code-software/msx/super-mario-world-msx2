objectlistspecial2:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

        db available    :       dw 026  :       dw 045,177      ;blue shell
        db available    :       dw 030  :       dw 045,069      ;blue naked koopa
        db available    :       dw 052  :       dw 044,064      ;blue koopa
        db available    :       dw 054  :       dw 019,363      ;MSX logo
        db available    :       dw 076  :       dw 036,029      ;feather box
        db available    :       dw 081  :       dw 039,064      ;blue koopa
        db available    :       dw 094  :       dw 020,255      ;paraboomb
        db available    :       dw 110  :       dw 013,255      ;paraboomb
        db available    :       dw 125  :       dw 041,093      ;green flying koopa
        db available    :       dw 139  :       dw 044,190      ;yellow shell
        db available    :       dw 146  :       dw 044,067      ;yellow naked koopa
        db available    :       dw 172  :       dw 032,177      ;blue shell
        db available    :       dw 176  :       dw 032,069      ;blue naked koopa
        db available    :       dw 195  :       dw 032,238      ;replicating chuck
        db available    :       dw 213  :       dw 022,177      ;blue shell
        db available    :       dw 217  :       dw 022,069      ;blue naked koopa
        db available    :       dw 260  :       dw 020,094      ;green flying koopa (to left only)
        db available    :       dw 283  :       dw 036,362      ;MSX logo

  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial2: equ $-objectlistspecial2
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
