objectlistlv053:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

	db available : dw 025 : dw 027,307	;illusion secret 1 platform
	db available : dw 049 : dw 040,091	;red flying koopa
	db available : dw 050 : dw 030,055	;yoshi coin
	db available : dw 064 : dw 028,094	;green flying koopa (to left only)
	db available : dw 076 : dw 024,091	;red flying koopa
	db available : dw 103 : dw 020,057	;coin ? box
	db available : dw 109 : dw 010,055	;yoshi coin
	db available : dw 121 : dw 027,091	;red flying koopa
	db available : dw 126 : dw 032,094	;green flying koopa (to left only)
	db available : dw 131 : dw 042,094	;green flying koopa (to left only)
	db available : dw 152 : dw 014,028	;flower box
	db available : dw 162 : dw 021,091	;red flying koopa
	db available : dw 183 : dw 036,094	;green flying koopa (to left only)
	db available : dw 193 : dw 006,055	;yoshi coin
	db available : dw 213 : dw 022,042	;smileybox (extra life)
	db available : dw 215 : dw 030,035	;blue !
	db available : dw 217 : dw 030,035	;blue !
	db available : dw 219 : dw 030,035	;blue !
	db available : dw 220 : dw 009,055	;yoshi coin
	db available : dw 224 : dw 040,094	;green flying koopa (to left only)
	db available : dw 233 : dw 025,094	;green flying koopa (to left only)
	db available : dw 245 : dw 038,094	;green flying koopa (to left only)
	db available : dw 258 : dw 017,055	;yoshi coin
	db available : dw 283 : dw 018,007	;Endflagbar
	db available : dw 284 : dw 017,006	;Endflagtop
	db available : dw 301 : dw 034,289	;greenmushroom (static)
	db available : dw 303 : dw 034,289	;greenmushroom (static)
	db available : dw 305 : dw 034,289	;greenmushroom (static)




  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv053: equ $-objectlistlv053

