objectlistlv055:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist

	db available : dw 065 : dw 046,310	;Roys castle special platform
	db available : dw 074 : dw 049,128	;flame
	db available : dw 078 : dw 040,055	;yoshi coin
	db available : dw 112 : dw 041,273	;moving flame (bouncing)
	db available : dw 129 : dw 048,273	;moving flame (bouncing)
	db available : dw 147 : dw 036,273	;moving flame (bouncing)
	db available : dw 155 : dw 047,273	;moving flame (bouncing)
	db available : dw 160 : dw 039,273	;moving flame (bouncing)
	db available : dw 168 : dw 035,273	;moving flame (bouncing)
	db available : dw 173 : dw 049,273	;moving flame (bouncing)
	db available : dw 179 : dw 046,273	;moving flame (bouncing)
	db available : dw 233 : dw 038,055	;yoshi coin
;	db available : dw 240 : dw 046,273	;moving flame (bouncing)
	db available : dw 254 : dw 038,055	;yoshi coin
	db available : dw 258 : dw 028,263	;Falling Spike
	db available : dw 267 : dw 045,273	;moving flame (bouncing)
	db available : dw 278 : dw 028,263	;Falling Spike
	db available : dw 284 : dw 035,055	;yoshi coin
	db available : dw 286 : dw 046,273	;moving flame (bouncing)
;	db available	|	dw 335	|	dw 042,076	;p-box (blue)
	db available : dw 336 : dw 034,263	;Falling Spike
	db available : dw 360 : dw 047,128	;flame
	db available : dw 371 : dw 047,128	;flame
	db available : dw 378 : dw 040,311	;Stonekoopa
	db available : dw 404 : dw 043,132	;falling block
	db available : dw 406 : dw 043,132	;falling block
	db available : dw 408 : dw 043,132	;falling block
	db available : dw 410 : dw 043,132	;falling block
	db available : dw 412 : dw 043,132	;falling block
	db available : dw 416 : dw 035,273	;moving flame (bouncing)
	db available : dw 418 : dw 043,132	;falling block
	db available : dw 420 : dw 043,132	;falling block
	db available : dw 422 : dw 043,132	;falling block
	db available : dw 424 : dw 043,132	;falling block
	db available : dw 426 : dw 043,132	;falling block
	db available : dw 430 : dw 041,311	;Stonekoopa
	db available : dw 432 : dw 043,132	;falling block
	db available : dw 434 : dw 043,132	;falling block
	db available : dw 436 : dw 043,132	;falling block
	db available : dw 438 : dw 043,132	;falling block
	db available : dw 440 : dw 043,132	;falling block
	db available : dw 445 : dw 034,273	;moving flame (bouncing)
	db available : dw 462 : dw 041,311	;Stonekoopa
	db available : dw 472 : dw 038,134	;giant door





 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv055: equ $-objectlistlv055

