objectlistlv078e:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

        db available    :       dw 067  :       dw 024,355      ;qeustionmark (special light)
        db available    :       dw 087  :       dw 020,356      ;ninja
        db available    :       dw 092  :       dw 035,128      ;flame
        db available    :       dw 119  :       dw 032,356      ;ninja
        db available    :       dw 128  :       dw 026,352      ;Mechakoopa
        db available    :       dw 146  :       dw 032,356      ;ninja
        db available    :       dw 161  :       dw 030,352      ;Mechakoopa
        db available    :       dw 175  :       dw 032,356      ;ninja
        db available    :       dw 181  :       dw 035,128      ;flame
     ;   db available    :       dw 243  :       dw 020,134      ;giant door

        db available    :       dw 229  :       dw 027,005      ;Checkpointbar
        
        db available    :       dw 235  :       dw 028,164      ;feather bonus (static)
        db available    :       dw 237  :       dw 026,272      ;redshroom (static)
        db available    :       dw 243  :       dw 020,134      ;giant door
        db available    :       dw 250  :       dw 024,289      ;greenmushroom (static)
        
        

  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078e: equ $-objectlistlv078e
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
