objectlistlv033b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  001 : dw 047+2,104  ;water (mario swimming) 
  db available  :  dw  010 : dw 047,208  ;ice (helper object)
    
  db available  :  dw  022-1 : dw 048-4,149  ;mediumsheertile (left)
  db available :  dw  026 : dw 042-3,169  ;cavekoopa   
  db available  :  dw  030-1 : dw 044-4,149  ;mediumsheertile (left)

  db available :  dw  039 : dw 040-3,169  ;cavekoopa  

  db available :  dw  049 : dw 037,102  ;ultracoin (static)

    db available  :  dw  053 : dw 050,120  ;fish (dumb) (left and right)

  db available :  dw  067 : dw 040,102  ;ultracoin (static) 
 
    db available  :  dw  080 : dw 038,119  ;multicoin box 

  db available :  dw  087 : dw 044-3,169  ;cavekoopa  
  db available :  dw  091 : dw 044-3,169  ;cavekoopa  

  db available   :  dw  101 : dw 040,174  ;spiketop 

    db available  :  dw  105 : dw 050,120  ;fish (dumb) (left and right)

  db available :  dw  106 : dw 039,102  ;ultracoin (static)
  db available :  dw  108 : dw 039,102  ;ultracoin (static)

  db available :  dw  121    : dw 039,162  ;tube cover up exit (upwards)
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv033b: equ $-objectlistlv033b

