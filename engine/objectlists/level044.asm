objectlistlv044:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  036    : dw 037,284    ;square flattened platform (pathfollower)
  db available :  dw  052    : dw 031,284    ;square flattened platform (pathfollower)
  db available :  dw  076    : dw 025,284    ;square flattened platform (pathfollower)

  db available   :  dw  140 : dw 031,055  ;yoshi coin 
  db available   :  dw  185 : dw 023,055  ;yoshi coin


  db available :  dw  242    : dw 031-2,157    ;superkoopa (flying in curve)
  db available :  dw  250    : dw 031-2,157    ;superkoopa (flying in curve)
  db available :  dw  251    : dw 031-2-2,157    ;superkoopa (flying in curve)
  
  db available   :  dw  255 : dw 036,055  ;yoshi coin  
  
  
  db available :  dw  266    : dw 032-2,157    ;superkoopa (flying in curve)
  db available :  dw  284    : dw 031,157    ;superkoopa (flying in curve)
  db available :  dw  292    : dw 031,157    ;superkoopa (flying in curve)


  db force18   :  dw  304-2 : dw 019-2,004  ;checkpoint top
  db available :  dw  304+2-2 : dw 019+3-2,005  ;checkpoint bar
  db force04   :  dw  304+5-2 : dw 019+2-2,031  ;checkpoint pole right

  db available :  dw  313 : dw 031,285  ;Flywing box

  db available :  dw  319    : dw 036,283    ;ridable cloud

  db available :  dw  358 : dw 033,029  ;questionmark animation (feather)

  db available :  dw  363    : dw 038,283    ;ridable cloud

  db available   :  dw  369 : dw 027,055  ;yoshi coin   
  
    db available :  dw  371+1 : dw 039-3,036  ;tube cover up exit sprite
  
  db available :  dw  401    : dw 038,283    ;ridable cloud
  db available :  dw  474    : dw 038,283    ;ridable cloud

;  db available  |  dw  486 | dw 016-4,189  ;fuzzy 
  db available  :  dw  491 : dw 027-4,189  ;fuzzy 
  db available  :  dw  492 : dw 037-4,189  ;fuzzy 
  db available  :  dw  496 : dw 016-4,189  ;fuzzy 
  db available  :  dw  503 : dw 033-4,189  ;fuzzy 
  db available  :  dw  512 : dw 027-4,189  ;fuzzy 
  db available  :  dw  520 : dw 027-4,189  ;fuzzy
  
  db available   :  dw  527 : dw 025,055  ;yoshi coin  
   
  db available  :  dw  530 : dw 040-4,189  ;fuzzy 
  db available  :  dw  537 : dw 025-4,189  ;fuzzy 
  db available  :  dw  538 : dw 037-4,189  ;fuzzy 
  db available  :  dw  547 : dw 029-4,189  ;fuzzy 
  db available  :  dw  548 : dw 043-4,189  ;fuzzy 
  db available  :  dw  556 : dw 030-4,189  ;fuzzy 
  db available  :  dw  557 : dw 042-4,189  ;fuzzy 
  db available  :  dw  564 : dw 030-4,189  ;fuzzy 
  db available  :  dw  565 : dw 042-4,189  ;fuzzy 

  db force00   :  dw  596 : dw 018-3+4,007  ;endflag bar
  db available :  dw  596+1 : dw 018-3+1+1,006  ;endflag top 

  db available :  dw  648 : dw 028,024  ;moon powerup  

  db force00   :  dw  660 : dw 022-3+4,007  ;endflag bar
  db available :  dw  660+1 : dw 022-3+1+1,006  ;endflag top 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv044: equ $-objectlistlv044

