objectlistlv034c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  001 : dw 048+1,104  ;water (mario swimming)   
    db available  :  dw  010 : dw 047,208  ;ice (helper object)

    db available :  dw  029 : dw 038-3,169  ;cavekoopa 
    db available :  dw  033 : dw 038-3,169  ;cavekoopa 
    db available :  dw  052 : dw 044-3,169  ;cavekoopa 

db available  :  dw  056 : dw 039,078  ;coin (static) 
 
    db available :  dw  070 : dw 036-3,169  ;cavekoopa 

db available  :  dw  074 : dw 032,078  ;coin (static) 
db available  :  dw  076 : dw 032,078  ;coin (static) 
 
  db available  :  dw  082 : dw 050,120  ;fish (dumb) (left and right) 

      db available :  dw  101 : dw 041,009  ;red flying koopa (up and down)    
  
  db available  :  dw  106 : dw 050,120  ;fish (dumb) (left and right) 
 
 
  db available :  dw  107 : dw 031,055  ;yoshicoin 

  db available :  dw  117    : dw 039,162  ;tube cover up exit (upwards)

    db available :  dw  120 : dw 044-3,169  ;cavekoopa 

   db available :  dw  122 : dw 038,029  ;questionmark animation (feather) 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv034c: equ $-objectlistlv034c

