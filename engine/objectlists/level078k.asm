objectlistlv078k:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  
  
        db available    :       dw 003  :       dw 016,289      ;greenmushroom (static)
        db available    :       dw 035  :       dw 022,352      ;Mechakoopa
        db available    :       dw 037  :       dw 038,352      ;Mechakoopa
        db available    :       dw 078  :       dw 014,352      ;Mechakoopa
        db available    :       dw 089  :       dw 030,352      ;Mechakoopa
        db available    :       dw 112  :       dw 022,352      ;Mechakoopa
        db available    :       dw 122  :       dw 030,164      ;feather bonus (static)

  
  
  	db force24	:	dw 152	:	dw 038,129	;door




  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078k: equ $-objectlistlv078k
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    001      :     db 48-2
dw    022*8    :     db 64-1
dw    046*8    :     db 64-1
dw    070*8    :     db 64-1
dw    094*8    :     db 64-1
dw    118*8    :     db 64-1
dw    000         ;defines the end of the list COMPULSORY!! 

 
