objectlistlv028:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


;  db available  |  dw  015 | dw 042,212  ;bigboo 
;  db available  |  dw  015 | dw 042,214  ;eerie (left and right) 

 
  db available  :  dw  053 : dw 032,211  ;small ghostring
  
  db available  :  dw  069 : dw 041,213  ;one way door
 
    db available  :  dw  082 : dw 031,205  ;spring  

  db available  :  dw  088 : dw 045,214  ;eerie (left and right)

   db available  :  dw  091 : dw 046,076  ;p-box
 
  db available  :  dw  097 : dw 042,212  ;bigboo 
 
  db available :  dw  157    : dw 041,042    ;smileybox (extra life) 
 
  db force25  :  dw  166 : dw 029,129  ;door 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv028: equ $-objectlistlv028


  
level28bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4

    dw 5890+bgrmapaddr :   db      042, 043, 044, 043 ;calculate adress = (y * mapwidth) + x
    dw 000             :   db      000, 000, 000, 000
lenghtlevel28bkgtiles: equ $-level28bkgtiles  