objectlistlv044b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


    db available  :  dw  003 : dw 030,130  ;static camerascroller
    db available  :  dw  004 : dw 030,287  ;maplevel04-03b helper object 


    db available  :  dw  048 : dw 012-4,189  ;fuzzy 
    
  db available   :  dw  049 : dw 024,055  ;yoshi coin     
    
    db available  :  dw  052 : dw 022-4,189  ;fuzzy 
    
  db available  :  dw  061 : dw 012,083  ;supercoin (static)    
    
    
    db available  :  dw  064 : dw 014-4,189  ;fuzzy 
    
  db available   :  dw  071 : dw 029,055  ;yoshi coin    
    
    db available  :  dw  074 : dw 020-4,189  ;fuzzy 
    db available  :  dw  086 : dw 032-4,189  ;fuzzy 
    
  db available  :  dw  090 : dw 032,083  ;supercoin (static)      
  db available  :  dw  091 : dw 043,083  ;supercoin (static)      
    
    db available  :  dw  094 : dw 032-4,189  ;fuzzy 
    db available  :  dw  102 : dw 024-4,189  ;fuzzy 
    db available  :  dw  106 : dw 032-4,189  ;fuzzy

  db available   :  dw  109 : dw 014,055  ;yoshi coin     
     
    db available  :  dw  110 : dw 010-4,189  ;fuzzy 
    db available  :  dw  114 : dw 014-4,189  ;fuzzy 
    db available  :  dw  122 : dw 024-4,189  ;fuzzy 
    
  db available   :  dw  126 : dw 020,055  ;yoshi coin     
  db available   :  dw  134 : dw 020,055  ;yoshi coin     
    
    
  db available :  dw  137    : dw 035,157    ;superkoopa (flying in curve)    
    
    db available  :  dw  138 : dw 018-4,189  ;fuzzy 
    db available  :  dw  142 : dw 032-4,189  ;fuzzy 
    db available  :  dw  153 : dw 024-4,189  ;fuzzy 
    db available  :  dw  155 : dw 036-4,189  ;fuzzy 

  db available :  dw  168    : dw 010,157    ;superkoopa (flying in curve)  

 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv044b: equ $-objectlistlv044b

