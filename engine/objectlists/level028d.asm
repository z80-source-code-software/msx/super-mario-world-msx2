objectlistlv028d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

   db force25   :  dw  002 : dw 022-1,087  ;blue rotating block
  db available  :  dw  003 : dw 008,185  ;small ghost (following mario)    
   db force25   :  dw  004 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  006 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  008 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  010 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  012 : dw 022-1,087  ;blue rotating block
   
  db available  :  dw  013 : dw 004,216  ;the bigboo    
   
   db force25   :  dw  014 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  016 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  018 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  020 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  022 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  024 : dw 022-1,087  ;blue rotating block
   db force25   :  dw  026 : dw 022-1,087  ;blue rotating block
  db available  :  dw  027 : dw 008,185  ;small ghost (following mario)    
   db force25   :  dw  028 : dw 022-1,087  ;blue rotating block
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv028d: equ $-objectlistlv028d