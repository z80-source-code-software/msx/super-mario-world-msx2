objectlistlv022c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

 
  db force22  :  dw  007 : dw 043,181  ;door (only appearing with pbox) 

  db available  :  dw  010 : dw 038,185  ;small ghost (following mario)
  db available  :  dw  018 : dw 055,185  ;small ghost (following mario) 
 
  db available :  dw  035 : dw 044,180 ;object 180: smileybox (pbox)
  
  db available  :  dw  045 : dw 033,185  ;small ghost (following mario)  
  db force26  :  dw  055 : dw 040,129  ;door

  db available  :  dw  056 : dw 049,185  ;small ghost (following mario)
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv022c: equ $-objectlistlv022c


;level22cbkgtiles:
;
;  ;bkgadress,          tilenumber   1,  2,  3,    4
;  
;    dw 5098+bgrmapaddr |   db      238, 235, 007, 007 ;calculate adress = (y * mapwidth) + x
;    dw 000             |   db      000, 000, 000, 000
;lenghtlevel22cbkgtiles: equ $-level22cbkgtiles  
 