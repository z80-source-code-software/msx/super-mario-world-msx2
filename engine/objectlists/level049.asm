objectlistlv049:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

; db available  |  dw  001 | dw 026+1,153  ;water (mario swimming)
 
   db available :  dw  014    : dw 038,032    ;exclamationmark (yellow)
 
 
 db available  :  dw  036 : dw 036,297  ;seaurchin (up down)
 
     db available  :  dw  037 : dw 037,203  ;bigfish (dumb) (left or right)
     db available  :  dw  043 : dw 034,203  ;bigfish (dumb) (left or right)
     db available  :  dw  047 : dw 038,203  ;bigfish (dumb) (left or right)

 db available  :  dw  048 : dw 032,297  ;seaurchin (up down)


     db available  :  dw  062 : dw 044,203  ;bigfish (dumb) (left or right)

  db available   :  dw  063 : dw 020,055  ;yoshi coin

 db available  :  dw  065 : dw 020,298  ;seaurchin (side to side) 


 db available  :  dw  070 : dw 036,298  ;seaurchin (side to side)
  
   db available   :  dw  078 : dw 022-1,087  ;blue rotating block
 
      db available  :  dw  080 : dw 036,203  ;bigfish (dumb) (left or right)
;      db available  |  dw  081 | dw 039,203  ;bigfish (dumb) (left or right)
;      db available  |  dw  082 | dw 034,203  ;bigfish (dumb) (left or right)
 
 db available  :  dw  084 : dw 008,297  ;seaurchin (up down) 
 
    db available  :  dw  114 : dw 026,199  ;rip van fish 

   db available   :  dw  120 : dw 044-1,087  ;blue rotating block

     db available  :  dw  142 : dw 039,203  ;bigfish (dumb) (left or right)

  db available  :  dw  144 : dw 008,028  ;questionmark animation (red flower)

  db available  :  dw  146 : dw 017,120  ;fish (dumb) (left and right)
  db available  :  dw  147 : dw 023,120  ;fish (dumb) (left and right)
  db available  :  dw  148 : dw 013,120  ;fish (dumb) (left and right)

    db available  :  dw  157 : dw 043,199  ;rip van fish

; db available  |  dw  164 | dw 023,297  ;seaurchin (up down)
 db available  :  dw  171 : dw 035,297  ;seaurchin (up down)

     db available  :  dw  174 : dw 006,203  ;bigfish (dumb) (left or right)
     db available  :  dw  176 : dw 009,203  ;bigfish (dumb) (left or right)

 ;db available  |  dw  186 | dw 006,297  ;seaurchin (up down) 

 ; db available  :  dw  192 : dw 036,241  ;secret box (coin) 
  db available  :  dw  194 : dw 036,225  ;secret box (1 up)
  db available  :  dw  196 : dw 036,241  ;secret box (coin) 
  db available  :  dw  198 : dw 036,241  ;secret box (coin) 



  db available   :  dw  199 : dw 007,055  ;yoshi coin
    db available  :  dw  200 : dw 010,199  ;rip van fish

     db available  :  dw  207 : dw 043,203  ;bigfish (dumb) (left or right)
    db available  :  dw  209 : dw 033,199  ;rip van fish 
  db available   :  dw  214 : dw 043,055  ;yoshi coin

    db available  :  dw  219 : dw 045,199  ;rip van fish
    db available  :  dw  241 : dw 039,199  ;rip van fish
    
    db available   :  dw  250 : dw 022-1,087  ;blue rotating block
 
 
 
 db available  :  dw  251 : dw 008,299  ;seaurchin (going round) 
 
   db available :  dw  262 : dw 044,178  ;keyhole  

    db available  :  dw  269 : dw 015,199  ;rip van fish 
 
 
  db available   :  dw  273 : dw 020,055  ;yoshi coin 
 
    db available  :  dw  283 : dw 047,199  ;rip van fish 
    
  db available :  dw  298    : dw 044,032    ;exclamationmark (yellow)    
    
    db available  :  dw  309 : dw 021,199  ;rip van fish 
 
 db available  :  dw  313 : dw 030,299  ;seaurchin (going round) 
 
    db available  :  dw  339 : dw 023,199  ;rip van fish 
    db available  :  dw  349 : dw 025,199  ;rip van fish
    db available  :  dw  359 : dw 045,199  ;rip van fish
     
   db available   :  dw  365 : dw 025,055  ;yoshi coin 

     db available  :  dw  366 : dw 033,203  ;bigfish (dumb) (left or right)
     db available  :  dw  367 : dw 038,203  ;bigfish (dumb) (left or right)


    db available :  dw  374 : dw 046,237  ;mini charging chuck 

    db available  :  dw  395 : dw 025,199  ;rip van fish 
    db available  :  dw  396 : dw 045,199  ;rip van fish 
    db available  :  dw  405 : dw 045,199  ;rip van fish 
 
  db force00   :  dw  410 : dw 030-3+4,007  ;endflag bar
  db available :  dw  410+1 : dw 030-3+1+1,006  ;endflag top 
 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv049: equ $-objectlistlv049

