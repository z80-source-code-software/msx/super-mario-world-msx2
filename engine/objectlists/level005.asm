objectlistlv005:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist  

  db available  :  dw  003 : dw 027,103  ;lava (mario killer)
  
  db available  :  dw  012 : dw 023,076  ;p-box (blue-static)
  
  db available :  dw  012 : dw 018,078  ;coin (static)
  db available :  dw  014 : dw 018,078  ;coin (static)
  db available :  dw  016 : dw 016,078  ;coin (static)
  db available :  dw  018 : dw 014,078  ;coin (static)
  db available :  dw  020 : dw 012,078  ;coin (static)
  
  
  db available :  dw  022 : dw 012,102  ;supercoin (static)
  db available :  dw  032 : dw 012,083  ;supercoin (static)

  db available :  dw  044 : dw 014,055  ;yoshicoin

  db available :  dw  056 : dw 021,054  ;tubeexit to the right 

  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv005: equ $-objectlistlv005
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    55*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!  