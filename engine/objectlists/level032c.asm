objectlistlv032c:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  003 : dw 050,103  ;lava (mario killer)
 
  db available  :  dw  014-1 : dw 042-4,149  ;mediumsheertile (left)
 
  db available  :  dw  022-1 : dw 038-4,150  ;mediumsheertile (left) (short segment) 1/2  
 
 ; db available :  dw  030 : dw 028,057  ;questionmark animation (coin)  
  db available :  dw  030 : dw 028,030  ;questionmark (star)
  
  
  db available :  dw  042 : dw 029,236  ;maplevel03-01c special object 
 
 
  db available :  dw  062 : dw 028-2,169  ;cavekoopa 
  db available :  dw  067 : dw 030-2,169  ;cavekoopa 
  db available :  dw  085 : dw 030-2,169  ;cavekoopa 
  db available :  dw  095 : dw 030-2,169  ;cavekoopa 
  db available :  dw  110 : dw 035-2,169  ;cavekoopa 
  db available :  dw  120 : dw 035-2,169  ;cavekoopa 
  db available :  dw  130 : dw 035-2,169  ;cavekoopa 

  db  available :  dw  148 : dw 035,093  ;green flying koopa  

 
  db available :  dw  160 : dw 039,055  ;yoshicoin 

  db force00   :  dw  186-1 : dw 040,054  ;tubeexit to the right 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv032c: equ $-objectlistlv032c

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    001      :     db 32-2
dw    184*8    :     db 48
dw    000         ;defines the end of the list COMPULSORY!! 
