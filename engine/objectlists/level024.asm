objectlistlv024:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available :  dw  37 : dw 033,055  ;yoshicoin 

  
  db available  :  dw  040-2 : dw 043-12+5,186  ;rotating platform (permanent)
   
 
  db available  :  dw  078-4 : dw 039,097  ;square flattened platform (vertical) 
  
  
  db available  :  dw  088-2 : dw 039-12+5,186  ;rotating platform (permanent) 

  db available  :  dw  117 : dw 032-2,064  ;blue koopa


  db  available :  dw  134 : dw 032,093  ;green flying koopa

  db available  :  dw  137 : dw 028,057  ;questionmark animation (coin)
  db available  :  dw  144 : dw 028,057  ;questionmark animation (coin)



  db available  :  dw  148 : dw 032-2,094  ;green flying koopa (to left only)
  db available  :  dw  156 : dw 032-2,094  ;green flying koopa (to left only)

  db  available :  dw  173 : dw 032,093  ;green flying koopa

  db available  :  dw  185-2 : dw 040-12+5,186  ;rotating platform (permanent)

  db available :  dw  198 : dw 010,078  ;coin (static)
  
  db available  :  dw  203 : dw 039-12+5,187  ;moving platform (path dependant)  
  
   db available :  dw  220 : dw 013,055  ;yoshicoin 
  
 
  db available  :  dw  225-2 : dw 040-12+5,186  ;rotating platform (permanent) 
  
  db available :  dw  245 : dw 020,078  ;coin (static)  
  
  db available :  dw  265 : dw 043,060  ;brown/pink coopa   
  
  db available  :  dw  272 : dw 031,188  ;switchbox  
  
  db available  :  dw  273 : dw 046-5,187  ;moving platform (path dependant)    

  db available  :  dw  298 : dw 043-1,094  ;green flying koopa (to left only)

  db available  :  dw  302 : dw 027,188  ;switchbox 

  db available  :  dw  315 : dw 028-4,189  ;fuzzy   
  
   db available :  dw  318 : dw 022,055  ;yoshicoin 

  db available  :  dw  326 : dw 040-4,189  ;fuzzy 
  db available  :  dw  327 : dw 040-4+12,189  ;fuzzy 

  db available  :  dw  331 : dw 027-1,094  ;green flying koopa (to left only)

  db force18   :  dw  350-2 : dw 039-2,004  ;checkpoint top
  db available :  dw  350+2-2 : dw 039+3-2,005  ;checkpoint bar
  db force04   :  dw  350+5-2 : dw 039+2-2,031  ;checkpoint pole right

;  db available  |  dw  362-3 | dw 047,097  ;square flattened platform (vertical) 
  db available  :  dw  362 : dw 042,189  ;fuzzy -4+12
  
  db available  :  dw  372-2 : dw 040-12+5,186  ;rotating platform (permanent) 
  db available :  dw  372 : dw 040,029  ;questionmark animation feather

  
  db available  :  dw  384 : dw 036-2,187  ;moving platform (path dependant) 
  
  db available  :  dw  403 : dw 035-6,094  ;green flying koopa (to left only)



;  db  available |  dw  408 | dw 047,095  ;square flattened platform 
  
  db available  :  dw  425 : dw 035-2,094  ;green flying koopa (to left only)  
    
  db  available :  dw  430 : dw 046,095  ;square flattened platform   
 
  db available  :  dw  442 : dw 038-2,094  ;green flying koopa (to left only) 
  
    db force00   :  dw  454 : dw 030,036  ;tube cover up exit sprite

   db available :  dw  464 : dw 027,055  ;yoshicoin 

  
  db available  :  dw  480-2 : dw 040-12+5,186  ;rotating platform (permanent) 
  db available  :  dw  504-2 : dw 040-12+5,186  ;rotating platform (permanent) 
  db available  :  dw  545-2 : dw 039-12+5,186  ;rotating platform (permanent) 
  db available  :  dw  545 : dw 040-4-4,189  ;fuzzy  
  
 
  db  available :  dw  561 : dw 038,096  ;green special box 
 
  db force00   :  dw  579 : dw 029,007  ;endflag bar
  db force28   :  dw  579+1 : dw 029-1,006  ;endflag top 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv024: equ $-objectlistlv024
  
level24bkgtiles:

  ;bkgadress,          tilenumber   1,  2,  3,    4
  
    dw 22943+bgrmapaddr :   db      000, 049, 000, 000 ;calculate adress = (y * mapwidth) + x
    dw 22945+bgrmapaddr :   db      050, 000, 000, 000 ;calculate adress = (y * mapwidth) + x
    dw 000              :   db      000, 000, 000, 000
lenghtlevel24bkgtiles: equ $-level24bkgtiles  