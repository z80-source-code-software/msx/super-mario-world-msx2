objectlistlv078d:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  
        db available    :       dw 024  :       dw 022,353      ;Stonekoopa (special)
        db available    :       dw 032  :       dw 008,353      ;Stonekoopa (special)
        db available    :       dw 082  :       dw 030,128      ;flame
        db available    :       dw 090  :       dw 008,353      ;Stonekoopa (special)
        db available    :       dw 106  :       dw 026,354      ;Stonekoopa (jumping)
  
  
  db force24	:	dw 123	:	dw 015,129	;door




  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv078d: equ $-objectlistlv078d
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
