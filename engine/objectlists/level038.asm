objectlistlv038:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 


  
  db  available :  dw  032 : dw 041,093  ;green flying koopa

  db available :  dw  046 : dw 036,058  ;questionmark animation (green yoshi egg)

  db available  :  dw  053 : dw 042-1,171  ;mediumsheertile (right) 

  db  available :  dw  065 : dw 042,093  ;green flying koopa

  db  available :  dw  072 : dw 040,093  ;green flying koopa
  db  available :  dw  081 : dw 038,093  ;green flying koopa

  db available   :  dw  089 : dw 012,055  ;yoshi coin 

  db available :  dw  094 : dw 044,034  ;exclamationmark (green) 

  db available  :  dw  098-1 : dw 046-4,150  ;mediumsheertile (left) (short segment) 1/2
  
  db  available :  dw  103 : dw 038,093  ;green flying koopa  
  
  
  db available  :  dw  106-1 : dw 044-4,150  ;mediumsheertile (left) (short segment) 1/2
  db available  :  dw  114-1 : dw 042-4,150  ;mediumsheertile (left) (short segment) 1/2

  db  available :  dw  121 : dw 034,093  ;green flying koopa

  db available  :  dw  125 : dw 040-1,171  ;mediumsheertile (right) 

  db available   :  dw  127 : dw 032,055  ;yoshi coin  

  db  available :  dw  136 : dw 038,093  ;green flying koopa  
  
  db available  :  dw  139 : dw 044-1,171  ;mediumsheertile (right) 
  
  db  available :  dw  152 : dw 042,093  ;green flying koopa  
  db  available :  dw  164 : dw 038,093  ;green flying koopa  
  db  available :  dw  183 : dw 036,093  ;green flying koopa  
  db  available :  dw  202 : dw 034,093  ;green flying koopa  
  
  db available :  dw  204 : dw 032,028  ;questionmark animation red flower  

  db  available :  dw  229 : dw 034,093  ;green flying koopa
  db  available :  dw  242 : dw 040,093  ;green flying koopa

  db available   :  dw  263 : dw 042,008  ;Jumpin Piranha Plant  

  db available   :  dw  272 : dw 039,055  ;yoshi coin 

  db force18   :  dw  302 : dw 039-3,004  ;checkpoint top
  db available :  dw  302+2 : dw 039,005  ;checkpoint bar
  db force04   :  dw  302+5 : dw 039-1,031  ;checkpoint pole right

  db  available :  dw  338 : dw 027,250  ;spikey

  db  available :  dw  358 : dw 015,255  ;paraboomb


   db available  :  dw  362 : dw 030,251  ;music note block
   db available  :  dw  364 : dw 030,251  ;music note block
   db available  :  dw  365 : dw 040-1,253  ;lakitu (pipe)
   db available  :  dw  366 : dw 030,252  ;music note block (feather release)
   db available  :  dw  368 : dw 030,251  ;music note block

  db  available :  dw  372 : dw 015,255  ;paraboomb

  db  available :  dw  400 : dw 015,255  ;paraboomb
  
  db available  :  dw  402 : dw 034-1,171  ;mediumsheertile (right) 
  
 
  db  available :  dw  413 : dw 035,250  ;spikey  
  db  available :  dw  435 : dw 042,250  ;spikey
  
  db available   :  dw  439 : dw 037,055  ;yoshi coin 


        db available  :  dw  440 : dw 034,119  ;multicoin box  
         
        db available  :  dw  462 : dw 034,256  ;smileybox (pbox grey)  
    
  db  available :  dw  464 : dw 042,250  ;spikey  
  db  available :  dw  471 : dw 042,250  ;spikey  
  db  available :  dw  478 : dw 042,250  ;spikey  




 
  db  available :  dw  498 : dw 015,255  ;paraboomb 
        
   db available  :  dw  549 : dw 040-1,253  ;lakitu (pipe)   
  db available :  dw  550 : dw 040-3,036  ;tube cover up exit sprite   
        
    db available :  dw  566 : dw 034,238  ;replicating chuck         
        
  db  available :  dw  595 : dw 033,009  ;red flying koopa  (up and down)        
        
  db force00   :  dw  601 : dw 028-3+4,007  ;endflag bar
  db available :  dw  601+1 : dw 028-3+1+1,006  ;endflag top 
  
  
;green koopa's seem to walk the mediumsheertiles very well. Use their tiledetection as codebase
  
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv038: equ $-objectlistlv038



