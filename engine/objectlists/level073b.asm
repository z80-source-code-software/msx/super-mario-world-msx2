objectlistlv073b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist
  

	db available	:	dw 004	:	dw 037,269	;door (spriteless)
	db available	:	dw 016	:	dw 041,102	;ultracoin (static)
	db available	:	dw 023	:	dw 045,185	;small ghost (following mario)
	db available	:	dw 033	:	dw 031,241	;secret box (coin)
	db available	:	dw 034	:	dw 022,055	;yoshi coin
	db available	:	dw 040	:	dw 041,102	;ultracoin (static)
	db available	:	dw 047	:	dw 044,185	;small ghost (following mario)
	db available	:	dw 060	:	dw 041,102	;ultracoin (static)
	db available	:	dw 084	:	dw 041,102	;ultracoin (static)
	db available	:	dw 091	:	dw 046,185	;small ghost (following mario)
	db available	:	dw 114	:	dw 029,076	;p-box (blue)
	db available	:	dw 115	:	dw 005,076	;p-box (blue)
	db available	:	dw 125	:	dw 041,102	;ultracoin (static)
	db available	:	dw 138	:	dw 041,185	;small ghost (following mario)
	db available	:	dw 146	:	dw 036,083	;supercoin (static)
	db available	:	dw 153	:	dw 040,324	;Eerie (high angle)
	db available	:	dw 157	:	dw 028,055	;yoshi coin
	db available	:	dw 164	:	dw 038,102	;ultracoin (static)
	db available	:	dw 174	:	dw 030,029	;feather box
	db available	:	dw 180	:	dw 037,324	;Eerie (high angle)
	db available	:	dw 187	:	dw 045,102	;ultracoin (static)
	db available	:	dw 201	:	dw 042,324	;Eerie (high angle)
	db available	:	dw 210	:	dw 041,269	;door (spriteless)
	db available	:	dw 236	:	dw 041,269	;door (spriteless)
	db available	:	dw 262	:	dw 041,269	;door (spriteless)
	db available	:	dw 288	:	dw 041,269	;door (spriteless)
	db available	:	dw 310	:	dw 041,269	;door (spriteless)
	db available	:	dw 314	:	dw 043,289	;greenmushroom (static)


  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv073b: equ $-objectlistlv073b

