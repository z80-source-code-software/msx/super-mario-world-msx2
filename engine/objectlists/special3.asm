objectlistspecial3:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

        db available    :       dw 028  :       dw 032,029      ;feather box
        db available    :       dw 042  :       dw 030,177      ;blue shell
        db available    :       dw 045  :       dw 030,069      ;blue naked koopa
        db available    :       dw 055  :       dw 023,170      ;bat
        db available    :       dw 073  :       dw 034,064      ;blue koopa
        db available    :       dw 079  :       dw 030,170      ;bat
        db available    :       dw 095  :       dw 026,177      ;blue shell
        db available    :       dw 098  :       dw 026,069      ;blue naked koopa
        db available    :       dw 111  :       dw 017,170      ;bat
        db available    :       dw 123  :       dw 024,042      ;smileybox (extra life)
        db available    :       dw 129  :       dw 025,093      ;green flying koopa
        db available    :       dw 140  :       dw 020,177      ;blue shell
        db available    :       dw 143  :       dw 020,069      ;blue naked koopa
        db available    :       dw 147  :       dw 011,170      ;bat
        db available    :       dw 160  :       dw 011,170      ;bat
        db available    :       dw 185  :       dw 034,290      ;piranjaplant (fire version)
        db available    :       dw 209  :       dw 032,238      ;replicating chuck
        db available    :       dw 227  :       dw 024,177      ;blue shell
        db available    :       dw 230  :       dw 024,069      ;blue naked koopa
        db available    :       dw 249  :       dw 021,064      ;blue koopa
        db available    :       dw 277  :       dw 022,064      ;blue koopa
        db available    :       dw 289  :       dw 019,170      ;bat
        db available    :       dw 294  :       dw 019,170      ;bat
        db available    :       dw 298  :       dw 024,365      ;rightexit

  


  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial3: equ $-objectlistspecial3
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
