objectlistlv046:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  001 : dw 026+1,153  ;water (mario swimming)
  
  db available  :  dw  035-1 : dw 039-1,291  ;torpedoted

     db available  :  dw  038 : dw 034,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  043 : dw 030,203  ;bigfish (dumb) (left or right)  
  
  db available  :  dw  064 : dw 034,028  ;questionmark animation red flower  

     db available  :  dw  070 : dw 039,203  ;bigfish (dumb) (left or right) 
     db available  :  dw  071 : dw 045,203  ;bigfish (dumb) (left or right) 

  
  db available  :  dw  077-1 : dw 033-1,291  ;torpedoted
  db available  :  dw  081-1 : dw 035-1,291  ;torpedoted

     db available  :  dw  084 : dw 046,203  ;bigfish (dumb) (left or right) 
     db available  :  dw  089 : dw 043,203  ;bigfish (dumb) (left or right) 
     db available  :  dw  097 : dw 046,203  ;bigfish (dumb) (left or right)   
  
  
  db available  :  dw  111-1 : dw 045-1,291  ;torpedoted
  db available  :  dw  115-1 : dw 035-1,291  ;torpedoted
  
     db available  :  dw  142 : dw 041,203  ;bigfish (dumb) (left or right)
     db available  :  dw  143 : dw 033,203  ;bigfish (dumb) (left or right)
     db available  :  dw  148 : dw 037,203  ;bigfish (dumb) (left or right)
     db available  :  dw  149 : dw 045,203  ;bigfish (dumb) (left or right)
  
  
  db available  :  dw  155-1 : dw 031-1,291  ;torpedoted
  db available  :  dw  165-1 : dw 035-1,291  ;torpedoted
  db available  :  dw  175-1 : dw 041-1,291  ;torpedoted
  
     db available  :  dw  197 : dw 050,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  198 : dw 043,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  202 : dw 033,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  204 : dw 039,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  210 : dw 030,203  ;bigfish (dumb) (left or right)  
     db available  :  dw  217 : dw 033,203  ;bigfish (dumb) (left or right)  
  
  db available  :  dw  227-1 : dw 043-1,291  ;torpedoted
  db available  :  dw  237-1 : dw 031-1,291  ;torpedoted
  db available  :  dw  247-1 : dw 035-1,291  ;torpedoted
  db available  :  dw  248-1 : dw 045-1,291  ;torpedoted

     db available  :  dw  268 : dw 045,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  269 : dw 035,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  276 : dw 041,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  282 : dw 038,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  289 : dw 038,203  ;bigfish (dumb) (left or right)   
  
  
  
  db available  :  dw  281-1 : dw 048-1,291  ;torpedoted
  db available  :  dw  295-1 : dw 037-1,291  ;torpedoted
  db available  :  dw  307-1 : dw 045-1,291  ;torpedoted
  db available  :  dw  317-1 : dw 035-1,291  ;torpedoted
  
     db available  :  dw  321 : dw 038,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  322 : dw 041,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  323 : dw 047,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  324 : dw 050,203  ;bigfish (dumb) (left or right)   
     db available  :  dw  336 : dw 042,203  ;bigfish (dumb) (left or right)     
  
  db available  :  dw  339-1 : dw 037-1,291  ;torpedoted
  
     db available  :  dw  339 : dw 045,203  ;bigfish (dumb) (left or right)   
  
  db available  :  dw  347-1 : dw 039-1,291  ;torpedoted
  db available  :  dw  355-1 : dw 041-1,291  ;torpedoted

     db available  :  dw  373 : dw 046,203  ;bigfish (dumb) (left or right) 
     db available  :  dw  375 : dw 042,203  ;bigfish (dumb) (left or right) 


  db available :  dw  377-1 : dw 040,054  ;tubeexit to the right 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv046: equ $-objectlistlv046

;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
dw    375*8      :     db 64
dw    000         ;defines the end of the list COMPULSORY!!
