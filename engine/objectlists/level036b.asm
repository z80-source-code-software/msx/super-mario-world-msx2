objectlistlv036b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

    db available  :  dw  040 : dw 030,244  ;ghostblob 
    db available  :  dw  063 : dw 039,244  ;ghostblob 
    db available  :  dw  100 : dw 036,244  ;ghostblob 
    db available  :  dw  139 : dw 036,244  ;ghostblob 
    db available  :  dw  161 : dw 032,244  ;ghostblob 
    db available  :  dw  188 : dw 036,180  ;smileybox (pbox) 
    db available  :  dw  195 : dw 034,244  ;ghostblob 


  db force25  :  dw  221 : dw 044,129  ;door

  db available   :  dw  231 : dw 039,055  ;yoshi coin 

   db force22  :  dw  245 : dw 044,181  ;door (only appearing with pbox) 
 
 
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistlv036b: equ $-objectlistlv036b

