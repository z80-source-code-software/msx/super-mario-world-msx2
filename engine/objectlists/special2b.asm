objectlistspecial2b:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00        :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist 

  db available  :  dw  010 : dw 003,364  ;halloffame
  
  
  
        db available    :       dw 011  :       dw 024,289      ;greenmushroom (static)
        db available    :       dw 013  :       dw 022,289      ;greenmushroom (static)
        db available    :       dw 020  :       dw 024,289      ;greenmushroom (static)
        db available    :       dw 022  :       dw 022,289      ;greenmushroom (static)

  
  
  
  db available  :  dw  025 : dw 003,145  ;yoshi house exit

  
  db  00        :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist

lenghtobjectlistspecial2b: equ $-objectlistspecial2b
  
;spritehiderlist! important must always be placed BEHIND the levelobjectslist.
;spritehiderX,    spritehiderXsize
;spritehidertable:
;dw    001      :     db 32-2
;dw    022*8    :     db 48
;dw    000         ;defines the end of the list COMPULSORY!! 

 
