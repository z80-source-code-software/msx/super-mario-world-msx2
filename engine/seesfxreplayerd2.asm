
MainSfxIntRoutined2: 

  ld a,(turbor?)
  and a
  ret z ;only on turbo machines because those are fast enough

  
  ld    a,(Sfxon2?)
  or    a
  ret   z

 

  ld    a,(tempo2+1)
  sub   a,1
  ld    (tempo2+1),a
  ret   nc
  ld    a,(tempo2+0)
  ld    (tempo2+1),a
  ld    hl,(PATADD2)

  ld    a,(_HALT2)             ;Halt counter
  and   a
  jr    z,.Main

  dec   A
  ld    (_HALT2),a
  ret   nz
  jp    .Main2                ;previously a HALT Event was done, now do PSG

.Main:
  call  Eventd2                 ;Do operation command

.Main2: 
  call  SetPsgd2                ;Set PSG reg.
  ld    (PATADD2),hl
  ret

;+----- Event Commands -----+
Eventd2: 
  ld    a,(hl)                ;Get Event
  ld    c,a
  and   $70
  ret   z                     ;No command

  cp    $10
  jr    z,.EV_hlT             ;Halt
  cp    $20
  jp    z,.EV_For             ;Loop start
  cp    $30
  jp    z,.EV_Nxt             ;Loop next
  cp    $40
  jr    z,.EV_Str             ;Continue Loop start
  cp    $50
  jr    z,.EV_Rer             ;Continue Loop next
  cp    $60
  jr    z,.EV_Tem             ;tempo

  ;- Effect end
.EV_End2: 
  xor   a
  ld    (Sfxon2?),a
  pop   af                    ;restore stack
  ret

  ;- Halt "hltd2:x"
.EV_hlT: 
  ld    a,c
  and   $0f
  ld    (_HALT2),a
  pop   af                    ;restore stack
  ret

  ;- Loop start "ford2:x"
.EV_For: 
  ld    a,(loopnr2)            ;inc loop nr
  inc   a
  and   $03
  ld    (loopnr2),a

  ex		de,hl

  ld    hl,loopbf2

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc			
      
  ld		a,(de)
  and   $0f
  ld    (hl),a                ;[Counter]
  inc   hl
  ld    (hl),e                ;Set address
  inc   hl
  ld    (hl),d

  ex		de,hl
  ret

  ;- Loop next "next"
.EV_Nxt: 
  ld    a,(loopnr2)
  and   $03

  ex    de,hl
  ld    hl,loopbf2

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc				

  dec   (hl)                  ;dec. counter
  jr    z,.Ev_Nxt1            ;Loop end
      
  inc   hl
  ld    c,(hl)                ;get address
  inc   hl
  ld    h,(hl)
  ld    l,c
  ret

  ; Loop done, now remove this loop
.Ev_Nxt1: 
  ld    a,(loopnr2)            ;dec loopnr
  dec   a
  and   3
  ld    (loopnr2),a

  ex		de,hl
  ret

  ;- Continue loop start "start"
.EV_Str: 
  ld    (clpadr2),hl           ;Store Patt adr.
  ret

  ;- Continue loop next "rerun"
.EV_Rer: 
  ld    hl,(clpadr2)
  ret

  ;- tempo "tmpd2:x"
.EV_Tem: 
  ld    a,c
  and   $0f
  ld    (tempo2+0),a
  ld    (tempo2+1),a
  ret

;+----- Set all PSG regs.-----+
SetPsgd2:                       
  ld	  a,(SfxpreviousPriority2)
  cp	  254			              ;Sfx being played that
  jp    Z,SetPsg3d2		          ;requires 3 channels ??

  ld    de,PsgReg2+4

  ld    bc,5          
  add   hl,bc                 ;start with reg.4 (PSG Channel 3)

  ;reg 0 - 5. [sound frequency]       
  ld    c,(hl)                ;reg.0, 2 or 4
  inc   hl
  ld    b,(hl)                ;reg.1, 3 or 5
  
;add turbo support, sigh even more clockcycles wasted!
 ; ld a,(turbor?)
 ; cp 2
 ; call nc,setturbofreq15d2 ;branch in here we want minimal clockcycles wasted on this one

  bit   7,(hl)                ;check and handle Tuning
  jp    Z,.Dont_TUNWUP

  ;TUNWUPd2: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  add   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl
.Dont_TUNWUP:

  bit   6,(hl)
  jp    z,.Dont_TUNWDW     
  ;TUNWDWd2: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  xor   a
  sbc   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl

.Dont_TUNWDW:
  inc   hl

  ld    a,4                   ;reg.4   
  out   ($4C),a
  ld    a,c
  out   ($4D),a
  ld    (de),a
  inc   de        

  ld    a,5                   ;reg.5   
  out   ($4C),a                
  ld    a,b
  and   $0f
  out   ($4D),a
  ld    (de),a
  inc   de         

;Use reg.6 ONLY when a soundeffect needs specific noise	
;		ld    a,6             ;reg.6   
;		out   ($4C),a
		
;		ld    a,(hl)          ;reg.6 [Rustle frequency]
;		bit   7,a
;		jp    Z,.DontTuneUp
		
;		ex    de,hl
;		add   a,(hl)          ; Up
;		ex    de,hl
;.DontTuneUpd2:
		
;		bit   6,a
;		jp    Z,.DontTuneDown
;		ex    de,hl
;		sub   (hl)            ; Down
;		ex    de,hl
;		NEG
;.DontTuneDownd2:
		
;		and   $1F
;		out   ($4D),a
;		ld    (de),a

  inc   hl             
  inc   de

  ld    a,7             ;reg.7 [set channel ON/OFF]
  out   ($4C),a         ;read r#7, which channels are used
  in    a,($4E)         ;by SCC-blaffer at this moment?
  ;       B A  Noise C B A  Tone C B A
  and   %11'011'011    ;0=sound output  
  ld    c,a
  ld    a,(hl)
  and   %00'100'100
  or    c
  out   ($4D),a       ;write value to reg.7

  inc   hl
  inc   hl
  inc   hl
  inc   de
  inc   de
  inc   de

  ;reg 8-10 [Volume]
  ld    a,10             ;reg.10   
  out   ($4C),a
				
  ld    a,(hl)
  inc   hl
  ld    c,a
  and   $0f
  bit   7,c
  jp    z,.No_TUN_UP
  ;TUN_UPd2:
  ex    de,hl
  add   a,(hl)
  ex    de,hl
.No_TUN_UP:
  bit   6,c
  jp    z,.No_VOL_DW
  ;VOL_DWd2: 
  and   $0f
  ld    b,a
  ld    a,(de)
  sub   a,b
  jp    nc,.No_VOL_DW
  xor   a
.No_VOL_DW:
  ;FIXVOLd2: 
  and   $0f
  ld    (de),a

;Set Maximum PSG Volume
;.SetMaxPSGVolumed2:	Equ	$+1
;  sub   a,1
;  jp    nc,.SetPsgvolume
;  xor   a
;.SetPsgvolumed2:
;/Set Maximum PSG Volume

  out   ($4D),a               ;write volume value
 
;reg 11-13 [volume effect]
  ld    c,$a1                 ;out port

  ld    a,11                  ;reg.11   
  out   ($4C),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.12   
  out   ($4C),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.13   
  out   ($4C),a
  outi                        ;out  (c),(hl)  and  inc hl
  ret

; Shut up PSG!
;PSGOFFd2: 
;Only mute Channel 3
;        ld      a,7             ;reg.7 [set channel ON/OFF]
;        out     ($4C),a         ;read r#7, which channels are used
;        IN      a,($4E)         ;by SCC-blaffer at this moment?
;       I/O       B A  Noise C B A  Tone C B A
;        and     %00'111'111    ;0=sound output  
;        or      %00'100'100    ;shut up channel C
;        out     ($4D),a

;        ld    a,7               ;reg.7 [set channel ON/OFF]  
;        out   ($4C),a           ;I/O B A Noise C B A Tone C B A
;        ld    a,                %00'111'111  ;0=sound output  
;        out   ($4D),a
;        ret


; Write PSG (not ROM-BIOS compatible!)
; Ind2:   a, data
;       E, port
;      IX, PSG store table
; outd2:  E+1
;      IX+1
;WRTPSGd2: ld    (IX),a
;        inc   IX
;        push  af
;        ld    a,E
;        out   ($4C),a
;        inc   E
;        pop   af
;        out   ($4D),a
;        ret

;+----- Set all 3 PSG regs.-----+
SetPsg3d2:                       
        LD    IX,PsgReg2
        LD    E,0             ;Start reg.0
        INC   hl              ;[skip Event byte]

        LD    B,3             ;First 6 reg. [sound frequency]
STPS.n0d2: PUSH  bc
        LD    C,(hl)          ;reg.0, 2 or 4
        INC   hl
        LD    B,(hl)          ;reg.1, 3 or 5
        BIT   7,(hl)          ;check and handle Tuning
        call  NZ,TUNWUPd2
        BIT   6,(hl)
        call  NZ,TUNWDWd2
        INC   hl
        LD    a,C
        call  WRTPSGd2
        LD    a,B
        AND   $0F
        call  WRTPSGd2
        pop   bc
        DJNZ  STPS.n0d2

        LD    a,(hl)          ;reg.6 [Rustle frequency]
        INC   hl
        BIT   7,a
        call  NZ,TUN_UPd2
        BIT   6,a
        call  NZ,TUN_DWd2
        AND   $1F
        call  WRTPSGd2

        LD    a,(hl)          ;reg.7 [set channel ON/OFF]
        AND   $3F
        OR    $80             ;set bit 7 is necessary to write the PSG!!
        INC   hl
        call  WRTPSGd2

        LD    B,3             ;reg 8-10 [Volume]
STPS.n1d2: PUSH  bc
        LD    a,(hl)
        INC   hl
;        BIT   5,a
;        jp    NZ,STPS.2       ;Volume pattern is set.
        LD    C,a
        AND   $0F
        BIT   7,C
        call  NZ,TUN_UPd2
        BIT   6,C
        call  NZ,VOL_DWd2
;        call  FIXVOL


; Make relative volume (depending on "SEEVOL")
; Ind2:  a, volume
; Outd2: a, new volume [seevol - (15-volume)]
FIXVOLd2: AND   $0F

		LD    (IX),a


;Set Maximum PSG Volume
.SetMaxPSGVolume:	equ	$+1
		SUB		a,1
		jp		NC,.SetPsgvolume
		XOR		A
.SetPsgvolume:
;/Set Maximum PSG Volume


STPS.n2d2: 
		AND   $1F
;		call  WRTPSG

        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($4C),a
        INC   E
        pop   AF
        OUT   ($4D),a

		pop   bc
		DJNZ  STPS.n1d2

        LD    B,3             ; reg 11-13 [volume effect]
STPS_2d2: LD    a,(hl)
        INC   hl
        call  WRTPSGd2
        DJNZ  STPS_2d2
        ret


; Tuning
;- byte
TUN_UPd2: ADD   a,(IX)          ; Up
        ret
TUN_DWd2: SUB   (IX)            ; Down
        NEG
        ret
;- word
TUNWUPd2: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        ADD   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret
TUNWDWd2: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        XOR   A
        sbc   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret

; Volume Down
VOL_DWd2: AND   $0F
        LD    B,a
        LD    a,(IX+0)
        SUB   B
        ret   NC
        XOR   A
        ret


; Write PSG (not ROM-BIOS compatible!)
; Ind2:   a, data
;       E, port
;      IX, PSG store table
; Outd2:  E+1
;      IX+1
WRTPSGd2: LD    (IX),a
        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($4C),a
        INC   E
        pop   AF
        OUT   ($4D),a
        ret


setturbofreq15d2:
;frequency divide by 2 lets see what happens
;so the story is lowering the value makes the freq higher
;turning bc up lowers the frequency
;we need to multiply by 1,5 d2:S
  cp 3
  jp z,.setx2

   push hl
   push de
   
   push bc
   pop de
   srl d
   rr e ;/2
   
   push bc
   pop hl
   add hl,de 
   push hl 
   pop bc

   pop de
   pop hl
   
   ret
   
.setx2:

   sla c
   rl b ;*2

  ret
