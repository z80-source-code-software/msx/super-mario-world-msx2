cutscene:
 
;this is only for debugging can safely be removed after source merging
 ; ld a,1
 ; ld (musicactive),a
 ; ld (sfxactive),a
;/ this is only for debugging  


  ld a,beginmusicblock
  ld (currentmusicblock),a 
 
  call screen5
 
  xor a
  ld (sc5page),a ;why like this? because it will be easier to do VDP commands each frame
  ld a,$1f ; Page 0 vram  
  di  
  out ($99),a
  ld a,128 + 2
  ei
  out ($99),a

;clear spat
  ld hl,emptyspat
  ld de,spat
  ld bc,fulllengthemptyspat
  ldir

;TODO: make the cutscene code more generic thus allowing for different type of cutscenes to get called
  ld a,(cutscenenr)
  and a
  call z,setcutscenetype.n1 ;0
  dec a
  call z,setcutscenetype.n2 ;1
  dec a
  call z,setcutscenetype.n3 ;2 iggy
  dec a
  call z,setcutscenetype.n4 ;3 morton
  dec a
  call z,setcutscenetype.n5 ;4 vanilla
  dec a
  call z,setcutscenetype.n6 ;5 ludwig
  dec a
  call z,setcutscenetype.n7 ;6 roy
  dec a
  call z,setcutscenetype.n8 ;7 wendy 
  dec a
  call z,setcutscenetype.n9 ;8 larry
  dec a
  call z,setcutscenetype.n10 ;9 bowser branch to totally different code
  dec a
  call z,setcutscenetype.n10 ;10 The very last part  
  ;etc etc

  push bc
  push de
  

 ; ld hl,paletteadress
  ld de,currentpalette
  ld bc,32
  ldir
  ld hl,currentpalette
  xor a
  ld (hl),a ;make color 0 black
  call SetPalette

;copy the cutscene objects to ram
  ld hl,objectlistcs
  ld de,levelObjectList
  ld bc,lenghtobjectlistcs
  ldir


  call clearvrampage0and1cs ;clear the entire visible part of the VRAM

  pop hl
 ; ld hl,iggybkgadress
  call uploadgfxmaptovram ;upload the tilemap to VRAM page 2

  ld a,(cutscenenr)
  cp 2
  push af
  call z,setscenebkg1
  pop af
  cp 3
  push af
  call z,setscenebkg2
  pop af
  cp 4
  push af
  call z,setscenebkg2
  pop af
  cp 5
  push af
  call z,setscenebkg1
  pop af
  cp 6
  push af
  call z,setscenebkg1
  pop af
  cp 7 ;wendy
  push af
  call z,setscenebkg2
  pop af
  cp 8
  call z,setscenebkg2
  
  pop hl
  ;ld hl,text1
  ld de,$9016
  call writetexttovram 

  call copypage0to1vram ;make a copy into page 1 as well

;set the isr active
  call setcutsceneisr

;all gfx in place? enable screen and start music
  call enablescreen
  
  ld a,(cutscenenr)
  cp 2
  call nc,startcutscenemusic  ;TODO: make a small starttrack and play that one first before doing the main music part

  
  
;initiate a few values for the eventhandler to take care of
  ld a,1
  ld (setobjectscurrentscreen?),a
  ld hl,0
  ld (camerax),hl
  ld (cameray),hl
  ld hl,256
  ld (mapheightx8),hl

cutsceneloop:

  call flipvrampage
  call swap_spat_col_and_char_table

  ld    a,(framecounter)
  inc   a
  ld    (framecounter),a

  call  fadeoutscreen ;fade out the screen on level exit

  call calleventhandler ;use eventhandler :)

  call putspattovram ;trow the spat into vram 
  
  ld a,(tripscreen)
  and a
  call nz,tripscreenupdown
  
  ;tweede vblank check 

  ld    a,1
  ld    hl,vblankflag
.checkflag:
  cp    (hl)
  jp    nc,.checkflag

  ld    (hl),0

  ld a,(fadeoutscreen?)
  ld c,a
  ld a,(endcutscene)
  xor c
  jp z,cutsceneloop
  
  xor a
  ld (endcutscene),a ;not really needed but just to make sure
  
;exit code  
  call disablescreen
  call Settempisr
  call stopmusic
  call screen4

  ret

setscenebkg1:

  call setscenebkg ;set the background into page 0 and 1
  call setscenecastle ;place the castle in the background
  call drawscenefuse ;draw the fuse in the FG

  ret

setscenebkg2:

  call setscenebkg ;set the background into page 0 and 1
  call setscenecastle ;place the castle in the background
  
  ret


setcutscenetype:

;game over
.n1:

  ld a,cutscenedatablock
  call block34

  ld hl,gameoverpaletteadress
  ld de,gameovertimeupadress

  ret

;time up
.n2:

  ld a,cutscenedatablock
  call block34

  ld hl,gameoverpaletteadress
  ld de,gameovertimeupadress


  ret

;iggy
.n3:

  ld a,cutscenedatablock
  call block34

  ld hl,paletteadress
  ld de,iggybkgadress
  ld bc,text1

  ret


;morton
.n4:

  ld a,cutscenedatablock
  call block34

  ld hl,mortonpaletteadress
  ld de,iggybkgadress
  ld bc,text2

  ret

;lemmy
.n5:

  ld a,cutscenedatablock2
  call block34

  ld hl,lemmypaletteadress
  ld de,lemmybkgadress
  ld bc,text3

  ret

;ludwig
.n6:

  ld a,cutscenedatablock3
  call block34

  ld hl,ludwigpaletteadress
  ld de,ludwigbkgadress
  ld bc,text4

  ret

;roy
.n7:

  ld a,cutscenedatablock
  call block34

  ld hl,mortonpaletteadress
  ld de,iggybkgadress
  ld bc,text5

  ret

;wendy
.n8:

  ld a,cutscenedatablock4
  call block34

  ld hl,wendypaletteadress
  ld de,wendybkgadress
  ld bc,text6

  ret  

;larry
.n9:

  ld a,cutscenedatablock2
  call block34

  ld hl,lemmypaletteadress
  ld de,lemmybkgadress
  ld bc,text7

  ret


;the end
.n10:

  ld a,finalsongblock ;different music
  ld (currentmusicblock),a

  ld a,cutscenedatablock5
  call block34

  ld hl,bowserpaletteadress
  ld de,endbkgadress
  ld bc,textempty

  
  ret    
  
  
text1:
    db "mario has defeated the demented iggy koopa in castle one and rescued yoshi's f"
    db "riend who is still trapped in an egg. together, they now travel to donutland.",0
text2:
    db "morton koopa jr. of castle two is now just a memory. the next area is the undergr"
    db "ound vanilla dome. what traps await mario in this new world? what will become of "
    db "princess toadstool?",0
text3:
    db "mario has triumphed over lemmy koopa of castle three. mario's quest is starting t"
    db "o get much more difficult. have you found the red and green switches yet?",0
text4:
    db "ludwig his days of composing koopa symphonies in castle four are over. the forest"
    db " of illusion lies ahead. mario must use his brain to solve the puzzle of this per"
    db "plexing forest.",0
text5:
    db "mario found his way through the forest of illusion and has put an end to roy koop"
    db "a of castle five. onward to dangerous but tasty chocolate island",0
text6:
    db "wendy koopa in castle six has sung her last song. mario must meet the challenge "
    db "that is now before him. there is a sunken ship that appears to be a gateway to "
    db "the valley of bowser.",0
text7:
    db "Mario has defeated larry koopa in castle seven. all that is left is bowsers castle "
    db "where princess toadstool is being held. can mario rescue her and restore peace "
    db "to the dinasour land?",0    
textempty:
    db " ",0 ;basicly make sure that nothing gets written to page2. That will all be taken care of later
    
    
flipvrampage:


  ld a,(sc5page)
  and a
  ld a,0
  ld (sc5page),a
  ld a,$1f
  call z,.set1
  di  
  out ($99),a
  ld a,128 + 2
  ei
  out ($99),a

  ret

.set1:

  ld a,1
  ld (sc5page),a
  ld a,$3f

  ret

;trow the fuse in the VRAM page
drawscenefuse:

  ld c,104 ;startposition of fuse
  ld b,4 ;draw 4 fuses on the ground

.loop:
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,240;fuseline
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,32;fuseline
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,c
  out ($9b),a ;Xd
  add a,16
  ld c,a
  xor a
  out ($9b),a ;Xd
  
  ld a,80+16
  out ($9b),a ;Yd  
  xor a
  out ($9b),a ;Yd  

  ld a,16
  out ($9b),a ;Xw  
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000  ;LMMM + TIMP
  ei
  out ($9b),a ;command  

  halt

  djnz .loop


  ret


;place the castle in place. This is always fixed so no inputs
setscenecastle:

;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192;castle
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ;xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,16+32
  out ($9b),a ;Yd  
  xor a
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw  
  xor a
  out ($9b),a ;Xw

  ld a,65
  out ($9b),a ;Yw
  xor a
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM ;LMMM is too slow to get the castle copied in one interrupt.
  out ($9b),a ;command  

  ei
  halt ;wait manually  

  ret


copypage0to1vram:

;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a; Xs
  out ($9b),a; Xs

  out ($9b),a ;Ys
  out ($9b),a ;Ys
  
  out ($9b),a ;Xd
  out ($9b),a ;Xd
  
  out ($9b),a ;Yd  
  ld a,1 ;page 1
  out ($9b),a ;Yd  

  xor a
  out ($9b),a ;Xw
  ld a,1  
  out ($9b),a ;Xw

  xor a
  out ($9b),a ;Yw
  ld a,1  
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  out ($9b),a ;command  

  ei
  halt : halt : halt : halt : halt ;wait manually  
  halt : halt : halt : halt : halt ;wait manually  

  ret



clearvrampage0and1cs: ;clear page 0 and 1 ;added: clear page 2 and 3 as well.

  ;clear entire page with black
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  xor a
  out ($9b),a ;36
  out ($9b),a
  out ($9b),a
  out ($9b),a
  
  ;256
  out ($9b),a ;40
  ld a,1
  out ($9b),a
  
  ld hl,1024
  ld a,l
  out ($9b),a ;42
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  ;wait for it
  ret


clearvisiblevram12: ;clear page 0 and 1 ;added: clear page 2 and 3 as well.

  ;clear entire page with black
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  xor a
  out ($9b),a ;36
  out ($9b),a
  out ($9b),a
  out ($9b),a
  
  ;256
  out ($9b),a ;40
  ld a,1
  out ($9b),a
  
  ld hl,512
  ld a,l
  out ($9b),a ;42
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  ;wait for it
  ret  
  
  

;copy the background to page 0
setscenebkg:

  ld c,2

.loop:

;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a; Xs
  out ($9b),a; Xs

  out ($9b),a ;Ys
  ld a,2 ;page 2  
  out ($9b),a ;Ys
  
  ld a,32
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,16
  out ($9b),a ;Yd  
;  xor a
  ld a,c
  dec a
  out ($9b),a ;Yd  

  ld a,192
  out ($9b),a ;Xw
  xor a  
  out ($9b),a ;Xw

  ld a,112
  out ($9b),a ;Yw
  xor a  
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000
  out ($9b),a ;command  

  dec c
  ei
  halt : halt : halt : halt : halt ;wait manually  
  ret z
  
  jp .loop
  
  

  ret

;upload the gfx map to VRAM page 2 because we are going to use page 0 and 1 for the sprites etc.
; in HL = adress of the imagemap that need uploading to the VRAM
uploadendgfxtovram:

;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X
  out ($9b),a
  ld a,2
  out ($9b),a ;Y
  
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;X
  ld a,128
  out ($9b),a
  xor a
  out ($9b),a ;Y 
  
  ld a,(hl) ;fetch first color byte
  out ($9b),a ;first byte
  inc hl ;next byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!


  ld de,256/2*125
  dec de ;-1
  ld c,$99

.upload:
  outi
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload
  

  ei



  ret



;upload the gfx map to VRAM page 2 because we are going to use page 0 and 1 for the sprites etc.
; in HL = adress of the imagemap that need uploading to the VRAM
uploadgfxmaptovram:

  ld a,(cutscenenr)
  cp 10
  jp z,uploadendgfxtovram ;special gfx load. Will take a long time but if it works it works

;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X
  out ($9b),a
  ld a,2
  out ($9b),a ;Y
  
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;X
  ld a,128
  out ($9b),a
  xor a
  out ($9b),a ;Y 
  
  ld a,(hl) ;fetch first color byte
  out ($9b),a ;first byte
  inc hl ;next byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!


  ld de,256/2*112
  dec de ;-1
  ld c,$99

.upload:
  outi
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload

  ld de,2048
  ld hl,smokecloudadress

.upload2:
  outi
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload2  

  

  ei



  ret


;write any text from a predefined fontmap into the vram. needs conversion from a string first
;autoreads the converted string from RAM
;e X coordinate d Y coordinate
;in HL adress of textstring
writetexttovram:

  push de

  ld de,levelbackgroundtileslist
  call convertstringtonumber ;convert the strings into numbers

  pop de
  
;precall  
  
  ld hl,levelbackgroundtileslist
    
  push hl
  push de
  exx
  pop de
  pop hl
  ld a,(maxrowcount)
  ld c,a
;  ld c,.maxrowcount
  ld b,e
  exx
  
;preloop 

.puttext:
  
  exx
  ld a,(hl)
  cp $BF
  call z,.setspace
  exx
  
  add a,a
  add a,a
  ld e,a
  ld d,0
  ld hl,fontadress 
  add hl,de
  
;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  exx
  push de
  exx
  pop de
  
  ld a,e
  out ($9b),a
  xor a
  out ($9b),a ;X = 16
  ld a,d
  out ($9b),a
;  ld a,(sc5page)
;  xor 1
  ld a,2
  out ($9b),a ;Y = 16
  
  ld a,8
  out ($9b),a
  xor a
  out ($9b),a ;8 in size
  ld a,8
  out ($9b),a
  xor a
  out ($9b),a ;font = 8*8
  
  ld a,(hl) ;fetch first color byte
  out ($9b),a ;first byte
  inc hl ;next byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!  

  ld b,3
.upload:
  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  inc hl
  
  djnz .upload

  ld b,7
.loadfont:  

  push bc

  dec hl
  dec hl
  dec hl
  dec hl
  ld de,32*4
  add hl,de
  
  ld b,4
.upload2:
  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  inc hl
  
  djnz .upload2

  pop bc
  
  djnz .loadfont
  ei

  exx
  ld a,e
  add a,8
  ld e,a
 
  ld a,(hl)
  cp $BF
  jp z,.space
 
  push hl
  push de
  push bc
 
  ld c,e ;store shift value in c
 
  ld a,(hl)
  ld e,a
  ld d,0
  ld hl,fontsizemap
  add hl,de
  ld a,(hl)
  dec a
  ld e,a
  
  ld a,c
  sub e
 
;PROBLEM!!: the last bit is lost in the VDP! so it is all or nothing
  bit 0,a
  call nz,.inca
 
 
  pop bc
  pop de
  pop hl
 
  ld e,a ;store final result

.space:
 
  dec c
  call z,.nextrow
 
 
  inc hl
  ld a,(hl)
  exx
  cp 255
  ret z
  jp .puttext


 ; ret

.nextrow:

  ld c,1 ;keep c reset until a space apears
  ld a,(hl)
  cp $BF
  ret nz 
  
  ld a,(maxrowcount)
  ld c,a
  ;ld c,.maxrowcount ;reset rowcounter
  ld a,d
  add a,8 ;shift to next fontposition
  ld d,a

  ld e,b

  ret

.maxrowcount: equ 25 ;maximum amount of characters per row

.inca:

  inc a
  ret

.setspace:

  ld a,31
  ret


;convert a text string on any given adress in HL to a workable number. 
;Reads the entire string and stops when it detects 0 writes the results back in DE
convertstringtonumber:

  ld c,$61

.loop:

  ld a,(hl)
  and a
  jp z,.finishloop
  cp $21
  jp z,.convert!
  cp $3f
  jp z,.convert?
  cp $2e
  jp z,.convertp
  cp $2c
  jp z,.convertc
  cp $27
  jp z,.convertu
  sub c
  ld (de),a

  inc hl
  inc de
  
  jp .loop

.finishloop:

  ld a,255
  ld (de),a
  ret
  
  ;ret

.convert!:

  ld a,27-1
  ld (de),a
  inc hl
  inc de
  jp .loop

.convert?:

  ld a,28-1
  ld (de),a
  inc hl
  inc de  
  jp .loop
  
.convertp:

  ld a,29-1
  ld (de),a
  inc hl
  inc de
  jp .loop
  
.convertc:

  ld a,30-1
  ld (de),a
  inc hl
  inc de  
  jp .loop

.convertu:

  ld a,31-1
  ld (de),a
  inc hl
  inc de  
  jp .loop


setcutsceneisr:
  
  di
  ld    a,(VDP_0)
  or    %0001'0000          ;set ei1 (enable lineints)
  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,0+128
;  ei
  out   ($99),a
 
  ld hl,cutsceneisr
  ld de,InterruptHandler
  ld bc,lenghtcutsceneisr
  ldir
 
 
	ld		hl,.cutsceneisr
	ld		de,$38
	ld		bc,3;6
	ldir

  ei
	ret 

.cutsceneisr:

  jp InterruptHandler 




cutsceneisr:
 
  push  af


  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,.Lineint        ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,.vblank          ;vblank detected, so jp to that routine


  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret
  
.lenghttillhere:  equ $ - cutsceneisr
.Lineint: equ InterruptHandler + .lenghttillhere

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix
  
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
      ld                a,(slot.page12rom)        ;all RAM except page 1+2
      out                ($a8),a         

  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a


  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.


  ;handle sfx
        ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc                a
        ld                ($7000),a
        ld                a,Sfxblock
        ld                ($9000),a
        inc               a
        ld                ($b000),a
  call  MainSfxIntRoutine       ;keep running sfx every int
  call  SetSfx                                     ;if sfx is on, then set to play

   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a   
        

  pop   af                  ;recall RAM/ROM page setting
  out                ($a8),a        

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc

  pop   af 



  ei
  ret


.lenghttillhere2: equ $ - cutsceneisr
.vblank: equ InterruptHandler + .lenghttillhere2

  ;scc replayer this is the most reliable interval
  in a,($a8)
  push af

  ld a,(slot.page12rom)
  out ($a8),a

        ld                a,SCCreplayerblock+1   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
;        ld                ($5000),a
;        inc               a
        ld                ($7000),a
        ld a,$3f ;get bank ready for SCC access
        ld                ($9000),a
;        inc               a
;        ld                ($b000),a
        
        push hl
        push de
        push bc
        
  call  SccReplayerUpdate       ;keep running sfx every int
  
        pop bc
        pop de
        pop hl
   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
;        ld                a,(memblocks.4)
;        ld                ($b000),a
    pop af
    out ($a8),a


  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a
  
  pop   af 
  ei
  ret
  
lenghtcutsceneisr: equ $ - cutsceneisr




maxrowcount:  db 25

;data
;our actual fontmap for screen 5 :)
fontadress:
incbin "cutscenes/font.SC5",$0000+7,1024
paletteadress: ;new palette
incbin "cutscenes/iggybkg.SC5",$7687,32
fontsizemap:  db 2,3,2,2,3,3,2,2,5,5,3,4,1,2,1,3,1,3,3,3,2,2,1,2,3,4,6,2,6,6,6

mortonpaletteadress:
incbin "cutscenes/mortonbkg.SC5",$7687,32

lemmypaletteadress:
incbin "cutscenes/lemmybkg.SC5",$7687,32
ludwigpaletteadress:
incbin "cutscenes/ludwigbkg.SC5",$7687,32
wendypaletteadress:
incbin "cutscenes/wendybkg.SC5",$7687,32
bowserpaletteadress:
incbin "../grapx/world8/finalboss/grapx.SC5",$7687,32
endbkgpalettadress:
incbin "cutscenes/endbkg.SC5",$7687,32

smokecloudadress:
incbin "cutscenes/smokecloud.SC5",$0000+7,2048

gameovertimeupadress:
incbin "cutscenes/gameover.SC5",$0000+7,2048
gameoverpaletteadress: ;new palette
incbin "cutscenes/gameover.SC5",$7687,32

include "cutscenes/csobjects.asm"

