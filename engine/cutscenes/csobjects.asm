objectlistcs:
;important notice: this list HAS TO BE chronological, lowest x first
;                 Xobject, Yobject,Objectnumber
  db  00       :  dw  000 : dw 000,000  ;x=000 this defines the leftborder of the levelobjectlist     
  
  db force06 :  dw  019 : dw 010,228  ;cutscene objects (mario)

  db  00       :  dw  999 : dw 000,000  ;x=999 this defines the rightborder of the levelobjectlist
lenghtobjectlistcs: equ $-objectlistcs