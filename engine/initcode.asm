
;initcode for SMW. Here the MSX system is being initialized and all the all time available data is being copied to RAM

showversion:
;give error if MSX1 is found
  ld a,errorblock+1			
;	ld		($5000),a
;	inc		a
	ld		($7000),a
  dec a

  jp showsmwversion


error:
;give error if MSX1 is found
  ld a,errorblock+1			
;	ld		($5000),a
;	inc		a
	ld		($7000),a
  dec a

  jp trowerrortext


;see if there is a MSX2+ present. If so check if it is panasonic. Yes? switch clock to maximum power
init2plus:

;taken from map.grauw.nl
	LD	A,8
	OUT 	($40),A	;out the manufacturer code 8 (Panasonic) to I/O port 40h
	IN	A,($40)	;read the value you have just written
	CPL			;complement all bits of the value
	CP	8		;if it does not match the value you originally wrote,
	JP	NZ,init.noturbor	;it is not a WX/WSX/FX.
	XOR	A		;write 0 to I/O port 41h
	OUT	($41),A	;and the mode changes to high-speed clock

  ld a,2
  ld (turbor?),a

  jp init.noturbor

setz80turbo:

  ld a,3
  ld (turbor?),a
  
  jp init.noturbor

;screw bios up  
tempinthook:

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt

  inc sp
  inc sp

  pop ix
  pop iy
  pop af
  pop bc
  pop de
  pop hl
  ex af,af'
  exx
  pop af
  pop bc
  pop de
  pop hl
 
  ei

  ret   
  
;
init:		

;read if the 0 key is pressed. If yes then leave it all be and boot normally (for the save utility)
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	0		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

  bit 0,a
  ret z
  bit 5,a
  call z,showversion


	ld		a,($2d)        ;ok (suppress memory access warning)
	and a
	jp    z,error ;MSX1 found: give error

	call $41 ;disable screen twice which will supress the white flash from the initi screen switch

;  ld    a,15        ;is zwart in onze huidige pallet
  xor a ;init with black color directly disable screen, this will make the rom start with a perfect black screen
	ld		($f3e9),a	  ;foreground color 
	ld		($f3ea),a	  ;background color 
	ld		($f3eb),a	  ;border color


	ld 		a,4			    ;switch to screen 4
	call 	$5f         
	call  $41          ;disable screen	
	

	di
	ld    hl,tempinthook
	ld    ($FD9A+1),hl          ;set bios hook so it no longer meddles with our business
	ld    a,$c3               
	ld    ($FD9A),a

	;problem is that this aproach is complex and problematic only because we depend on the BIOS's CPU switching code
	;If for some stupid reason the BIOS writes to any part of page3 we get into deep trouble and thus we are using all kinds of tricks to avoid
    ;writing into the BIOS area. If we somehow are able to hijack both CPU's and be able to park them into our own code in page3
    ;we can ditch the entire BIOS after init making $C000-$FFFE available as RAM!

		;clear unused system area
  ld hl,$F664
  ld bc,$FCB0-$F664
  ld (hl),0
  ld de,$F664+1
  ldir

  ei

  ;relocate stack
 ; ld sp,$F380 ;right above the rdslt helper
  ld sp,$FFDD ;right above the vdp mirrors

	
;  ld a,1
;  ld (slotfm),a
 call initAll ;init kinrou5, check if FM is present

  ld a,(optionsecondary+2) 
  push af
  ld a,(musicactiveoption)
  push af
 
;init page3 variables, write zero's into reserved RAM space
  ld hl,variablespage3
  ld bc,variablespage3lenght-1
  ld (hl),0
  ld de,variablespage3+1
  ldir 
	
  pop af
  ld (musicactiveoption),a
  pop af
  ld (optionsecondary+2),a
  
  
;read if the 1 key is pressed. If yes do not init the turboR or MSX2+ turbo
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	0		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

  bit 1,a
  jp z,.noturbor
  bit 2,a ;set z80 to 5 mhz for msx2+ computers
  jp z,init2plus
  bit 3,a ;set z80 turbo bit
  jp z,setz80turbo
  
;get msx type from BIOS and init the CPU's accordingly
	ld		a,($2d)    ;ok (suppress memory access warning)
;copy msx type to ram
    ld (biostype),a
	cp    2
;	jp    z,init2plus 	;makes the msx2+ alot slower on the vdp calls so disbale this!	
	cp		3
	jp		c,.noturbor
	
;enable r800 ROM turbo mode by calling to the BIOS
  ld a,%00000001
  call $180 ;switch with bios
  ld a,%00000000
  call $180 ;switch with bios
  ld a,%00000001
  call $180 ;switch with bios  
  
  ;switch CPU twice to make sure both CPU's are on the BIOS parking spot so that we can hijack the Z80
  ;from now on we leave the turbo led permanently on. Nobody knows we switch CPU's just for the worldmap code to be able to execute
  ld a,%10000000
  out ($a7),a ;switch turbo led on disable pauze button
  
  
  ld a,1
  ld (turbor?),a ;write value to ram for future reference


;/end of turboR or Z80 turbo init
  
.noturbor:

;prepare the rominit magic and execute.
  ld hl,Rominitdata
  ld de,$c000
  ld bc,lenghtrominit
  ldir
  call rominit
  
  ld a,$3f
  ld ($9000),a
  call SccInit ;init the scc soundchip

  di
  ;relocate vdp mirrors
  ld hl,VDP_0b
  ld de,VDP_0
  ld bc,9
  ldir

  ei
;===========================================[BIOS is kicked out from this point] =====================================

;bios can be called back from any position in the ROM and most safely from page3. value = stored in slot.bios
;when slot.bios is set bios is on page0 rom is in page12 and page3 remains unchanged

;==================================[Init all VDP mode registers by hand]========================================================

;           bit7          bit6          bit5          bit4          bit3          bit2          bit1          bit0
;register 0 0             DG            IE2(lightpen) IE1(lineint)   M5            M4            M3            0
  ld a,(VDP_0+0)
  or %0000'0000
  and %1100'1111 ;leave mode registers untouched. The BIOS has allready done the job for us
;  ld    a,%0000'0100
  ld    (VDP_0+0),a
  di
  out   ($99),a
  ld    a,0+128
  ei
  out   ($99),a

;           bit7          bit6          bit5          bit4          bit3          bit2          bit1          bit0
;register 1 0             BLK (display) IE0 (Vblank)   M1            M2            0             SZ            MAG
  ld a,(VDP_0+1)
  or %0000'0010
  and %1111'1110
  ;ld    a,%0010'0010
  ld    (VDP_0+1),a
  di
  out   ($99),a         ;/SpriteInitialize:
  ld    a,1+128
  ei
  out   ($99),a

;           bit7          bit6          bit5          bit4          bit3          bit2          bit1          bit0
;register 8 MS mouse      LP (lightpen) TPtransparant CB (color bus)VR            0             SPD           BW
 ld a,(VDP_8+0)
 or %0010'0000
 and %1111'1100
; ld    a,%0010'1000 
 ld  (VDP_8+0),a
 di
 out  ($99),a
 ld  a,8+128
 ei
 out  ($99),a

;           bit7          bit6          bit5          bit4          bit3          bit2          bit1          bit0
;register 9 LN (192/212)  0             SYM1          SYM0          IL(interlace) E/O           NTSC(60/50Hz) DCD
 ld a,(VDP_8+1)
 or %1000'0000
 and %1111'1101
; ld  a,%1000'0000    ;screenheight 212, 60 hertz 
 ld  (VDP_8+1),a
   di
 out  ($99),a
 ld  a,9+128
 ei
 out  ($99),a


;           bit7          bit6          bit5          bit4          bit3          bit2          bit1          bit0
;register25 0             CMD           VDS           YAE           YJK           WTE           MSK           SP2
 ld a,(VDP_8+19) ;17 moet 19 zijn!!!
 or %0000'0010
 and %0111'1110
;  ld    a,%0000'0110
 ld  (VDP_8+19),a
   di
 out  ($99),a
 ld  a,25+128
 ei
 out  ($99),a

 
;centre screen horizontally for as much as possible in v9958 mode
  ld    a,4
	ld		(VDP_8+10),a
	di
	out		($99),a
	ld		a,18+128
	ei
	out		($99),a
;/centre screen horizontally for as much as possible in v9958 mode

  xor   a
	ld		(VDP_8+15),a
	di
	out		($99),a
	ld		a,23+128
	ei
	out		($99),a



;check s#1 to check if v9958 is active
  ld    a,1               ;Set Status register #1
  di
  out   ($99),a
  ld    a,15+128          ;we are about to check for HR
  out   ($99),a
  in    a,($99)           ;Read Status register #1

  and   %0000'0100        ;if bit set then v9958 found
  ld    (v9958found?),a

  xor   a                 ;Return to Status register #0
  out   ($99),a
  ld    a,15+128          
  ei
  out   ($99),a
;/check s#1 to check if v9958 is active

;/==================================[Init all VDP mode registers by hand]========================================================


;init blocks this way the SCC memory mapper becomes active and is recognized by the
;emulators and megaflashrom
	xor		a			
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
	inc		a
	ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a




;tabel 1 mirroren:   %1001'1111 in R#3 en %0000'0000 in R#4
;normaal is dan:     %1111'1111 in R#3 en %0000'0011 in R$4 DENK IK

;msb  7   6   5   4   3   2   1   0   lsb
;r#4  0   0  a16 a15 a14 a13  1   1   pattern generator table base address register

;r#3 a13  1   1   1   1   1   1   1   color table base address register
;r#10 0   0   0   0   0  a16 a15 a14  color table base address register

;a12=0 en a11=0 betekemt dus tabel2 = tabel 1 EN tabel3 = tabel 1

;normally:  character pattern table starts at 0 ($0000)
;           color table starts at 8192 ($2000)
  
;copy the engine to RAM

	ld		hl,engine
	ld		de,engaddr
	ld		bc,enlength	        ;load engine (page0)
	di
	ldir

	ld		hl,enginepage3
	ld		de,enginepage3addr
	ld		bc,enginepage3length	    ;load enginepage3
	ldir
  ei
  
;from this point all generic routines are loaded into RAM this means that the tempISR's and inthooks + blockcallers are now available. From this point page1 ROM segments can freely be changed
	ld a,engine32block
	call block34
	ld		hl,$8000
	ld		de,$d880;variablespage3end+1
	ld		bc,lenghtengine32block	    ;load enginepage3
	ldir

  ld a,engine2block
  call block3
  
  ld hl,$8000 ;copy tail part of engine
  ld de,engineend
  ld bc,lenghtengine2block
  ldir


;all done now hijack the Z80 if on turbor
;yes we hijack the cpu swithing code so that we can fully ditch the BIOS clearing adress $FFFE!
    ld a,(turbor?)
    dec a
    call z,hijackz80

    ;reset stack
    ld sp,$FFDD

;detect darky and inititate

    call detectdarky
 ;   ld a,1 ;DEBUG
    ld (darky?),a
    and a
    jp z,.nodarky ;if no darky is found the hell with all these commands
    ld hl,$4000 ;CPLD disable mirror and clock
    call senddarkycommand  
    ;enable echo on the sfx processor
    ld hl,$2404
    call senddarkycommand    
    ld hl,$200F
    call senddarkycommand
    ld hl,$2103
    call senddarkycommand    
    ld hl,$2209
    call senddarkycommand
    
;write correct code to demo inthandler
    ld hl,demointhandler.sfx
    ld de,MainSfxIntRoutined
    ld a,$CD
    ld (hl),a
    inc hl
    ld (hl),e
    inc hl
    ld (hl),d     
    
;read if the 4 key is pressed. If yes set the mixers. If not ignore the mixer settings and leave it to the end user
 ; in	a,($AA)
;	and	$F0		;only change bits 0-3
;	or	0		;take row number from B
;	out	($AA),a
;	in	a,($A9)		;read row into A
;
 ; bit 4,a
 ; jp nz,.nodarky 
    
    ld hl,$3207 ;set line in + bypass on ;4 for line in
    call senddarkycommand
    ld hl,$301F ;set volume max input left
    call senddarkycommand
    ld hl,$311A ;set volume high input right
    call senddarkycommand      
.nodarky:    
    
;get introscreen ready  
  ld a,logoblock
  call block34

if (fullintroenable = 1)
   call showlogo ;uncomment for full function
endif

;get loader ready
  ld	a,loaderblock
  call	block34			        ;at address $8000 / page 2

startgame:
 
 
;set lives 
  ld a,1
  ld (marioleftright),a ;naar rechts kijken on start
  ld    a,3
  ld    (lives),a

if (fullintroenable = 1)  
  jp startdemo ;and start the demoengine with introscreen ;uncomment for full function
endif 

  ld a,15     ;black bkg
  di
  out ($99),a
  ld a,7+128
  ei
  out ($99),a	
; 
; 
  jp    loader.address      ;set loader in page 2 and jp to it
  
hijackz80:

    di
    ld a,(slot.bios)
    out ($A8),a
    
    ld (tempstack),sp
    
    ld hl,switchcpucode
    push hl

    ld c,%01000000
    
	PUSH	HL
	PUSH	DE
	PUSH	BC
	PUSH	AF
	PUSH	IX
	PUSH	IY
	EXX
	EX	AF,AF'
	PUSH	HL
	PUSH	DE
	PUSH	BC
	PUSH	AF			; save all Z80 registers
	LD	A,I
	PUSH	AF			; save interrupttable pointer
	LD	(cpusp),SP		; save stackpointer

	ex af,af'
;now switch!	
	LD	A,6
	OUT	($E4),A		; S1990 register 6
    ld a,%01100000
    out ($E5),a

    nop
    
    ld a,(slot.page12rom)
    out ($A8),a
    
    ld sp,(tempstack) ;recall stack pointer
    
    ei
    ret ;we return here after the Z80 has parked into its new parking spot
  
