;A huge space of nothingness is found after page3 we are going to use that
;startadress = $D880 we end when page3 is full as well.

;screenfader. this fades the screen when mario exits the level
fadeoutscreen:
  ld    a,(fadeoutscreen?)
  or    a
  ret   z
    
  ld    a,(framecounter)
  and   7
  ret   nz
  
  ld    a,(fadeoutscreenstep)
  dec   a
  ld    (fadeoutscreenstep),a
  jr    nz,.dofade
  
  xor   a
  ld    (fadeoutscreen?),a
  ret
  
.dofade:
  ld    hl,currentpalette                 ;fade out the current palette
  call  .darkenpalette
  ld    hl,currentpaletteONEtintdarker    ;but also fade out the darker version of the palette (which is used for the scoreboard)
  call  .darkenpalette

  ld    hl,currentpalette
  call  SetPalette
  ret

.darkenpalette:
  ld    b,16                              ;amount of colors
.loop:
  call  .darkencolors
  djnz  .loop
  ret  
  
.darkencolors:
  ;first make blue darker
  ld    a,(hl)
  and   %0000'1111                        ;blue
  jr    z,.endDarkenBlue                  ;there is no blue at all in this palletecolor
  dec   a
.endDarkenBlue:  
  ld    d,a                               ;store the darker blue in d

  ;then make red darker
  ld    a,(hl)
  and   %1111'0000                        ;red
  jr    z,.endDarkenRed                   ;there is no red at all in this palletecolor
  sub   a,16
.endDarkenRed:  
  ;then add blue and red together again
  or    d
  ld    (hl),a
  
  inc   hl                                ;green
  ;and finally make green darker
  ld    a,(hl)
  and   %0000'1111                        ;green
  jr    z,.endDarkenGreen                 ;there is no green at all in this palletecolor
  dec   a
.endDarkenGreen:  
  ld    (hl),a
  
  inc   hl                                ;next color
  ret




lockmariotoobject:
;this code makles sure that the mariospat is correctly displayed when mario is standing on a moving platform

  ld a,(standingonsprite)
  dec a
  ret nz
  
  ld a,(specialsheertile)
  and a
  ret nz
 
  
  ld ix,(spriteixvalue)

  ld de,(marioy)
  push de

;get Y coordinates
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,24
  xor a
  sbc hl,de
  ld (marioy),hl

  ld hl,(mariox)
  push hl

;get x coordinates
  ld d,0
  ld a,(ix+v1)
  bit 7,a
  jp z,.nonegate
  neg 
  ld e,a
  ld hl,(mariox)
  xor a
  sbc hl,de
  ld (mariox),hl
  jp .nosubstract

.nonegate:
  ld e,a
  ld hl,(mariox)
 ; xor a
  add hl,de
  ld (mariox),hl
  
.nosubstract:

  call calculatemarioscreenposition
  
  pop hl
  ld (mariox),hl
  pop de
  ld (marioy),de  ;let the physics handle the rest
  
  
  ld		a,(VDP_8+15)
  ld    b,a
  
  ;add vertical scroll offset
  ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a
;/add vertical scroll offset

  ld a,(marioyoshi)
  and a
  call nz,.setmarioonyoshiy

  ld    a,(v9958found?)
  or    a

  ld    a,(mariocoordinates+1) 

  ;this code is for v9938
  jp    nz,.v9958found
  ld    c,a
	ld		a,(VDP_8+10)
  add   a,c
  sub   a,7
.v9958found:  
  ;/this code is for v9938
  
  
  sub 4
  ld    (spat+57),a
  ld    (spat+61),a
  ld    (spat+65),a
  ld    (spat+69),a
;/make the horizontal coordinates count  
  ld b,a
  
  ld a,(marioyoshi)
  and a
  call nz,.setmarioonyoshix
  

  ret


.setmarioonyoshix:
  
  ld a,(marioturnyoshi)
  and a
  ret nz ;HACK: let the eventhandler decide yoshis position during turn

;BUG: if spriteflickr is active problems ensue because the non active half gets forced into position as well...  
  
  ld a,b
  add a,4

  ld ix,(yoshiix) ;set yoshi too
  ld    l,(ix+spataddress)      ;spat address of sprite (16 bits)
  ld    h,(ix+spataddress+1)    ;spat address of sprite (16 bits)
  push hl
  pop iy

  ld b,a
  
  ld a,(ix+clearvram)
  and a
  ret nz
  
  ld a,b

  ld (iy+1),a
  ld (iy+5),a
  
  ld b,a
  ld a,(marioleftrightspr)
  and a
  call z,.left
  ld a,b
  
  ld ix,(yoshiheadix) ;set yoshi too
  ld    l,(ix+spataddress)      ;spat address of sprite (16 bits)
  ld    h,(ix+spataddress+1)    ;spat address of sprite (16 bits)
  push hl
  pop iy

  ld b,a

  ld a,(marioleftrightspr)
  and a
  jp z,.headleft

  ld a,b
  add a,9
  ld (iy+1),a
  ld (iy+5),a


  ret

.headleft:

  ld a,b
  sub 13
  ld (iy+1),a
  ld (iy+5),a


  ret

.left:

  ld a,b
  sub 4
  ld (iy+1),a
  ld (iy+5),a

  ret


.setmarioonyoshiy:
  
  ;add vertical scroll offset
  ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  sub 16-2
  
  
  ld c,a
  ld a,(marioduck)
  and a
  ld a,c
  call nz,.setduck
  
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a 
  
  
  
;/add vertical scroll offset
  ld ix,(yoshiix) ;set yoshi too
  ld    l,(ix+spataddress)      ;spat address of sprite (16 bits)
  ld    h,(ix+spataddress+1)    ;spat address of sprite (16 bits)
  push hl
  pop iy

  ld a,(ix+clearvram)
  and a
  ret nz


  ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  add a,16
  ld (iy),a
  ld (iy+4),a
  
  ld ix,(yoshiheadix) ;set yoshi too
  ld    l,(ix+spataddress)      ;spat address of sprite (16 bits)
  ld    h,(ix+spataddress+1)    ;spat address of sprite (16 bits)
  push hl
  pop iy

;TODO: when mario ducks yoshi's coordinates need to change along.

  ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  
  ld c,a
  ld a,(marioduck)
  and a
  ld a,c
  call nz,.setduck
  
  ld (iy),a
  ld (iy+4),a


  ret

.setduck:
  
  add a,4
  ret


  
  
initializepillarbkgbuffer:


    ld hl,0
    ld (pillarheight),hl ;reset pointer
    xor a
    ld (pillarbkgbuffer),a ;writ a zero
    ld hl,pillarbkgbuffer
    ld de,pillarbkgbuffer+1
    ld bc,320-1
    ldir

    ret


;scan the entire map for brown blocks find their X and Y tilecoordinates. Write background into place and spawn a special coin
turnbrownblocksintocoin:

    



  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call initializepillarbkgbuffer ;initialise the entire part of this memory
	
  ld hl,$4000 ;start of map
  ld de,(maplenght) ;lenght of map
  ld bc,0 ;count X
  exx 
  ld c,5 ;limit spawn to 5 coins maximum
  exx
  
  
  
  
.looprow:
    ld a,(hl)
    cp 204
    call z,.founblock  
    
    inc hl
    dec de
    
    ex de,hl ;check for 0
    push bc
    ld bc,0
    xor a
    sbc hl,bc
    pop bc
    ex de,hl
    jp z,.nextrow
    
    jp .looprow

.nextrow:


/*
    inc c
    ld de,(maplenght)
    
    ;we need to know the mapheight and work with that
    
;    push hl
    
    ld a,(mapheight) ;mapheight = a word btw so loading it like this is allready very risky
    
    sub c ;problem while this code works the map can only be 128 tiles high. This constrain makes it impossible to work in eg. maplevel6-04b
;    pop hl

*/

;new 16 bits code

    inc bc
    ld de,(maplenght)
    
    push hl
    ld hl,(mapheight)
    
    xor a
    sbc hl,bc
    
    ld a,l
    or h
    
    pop hl


    jp z,.donesearch

    jp .looprow


.donesearch:

  jp setpage12tohandleobjectmovement
  
  
 
;block found write into mapadress  
.founblock:
 
    push bc
    push de
    push hl ;do hl last

;find if there is predefined bkg in there 

  ex de,hl ;push bkg in de
    ld bc,levelbackgroundtileslist
  ld a,bkgbufferrows

.findbkgloop:
   
  ex af,af'
  
   ld a,(bc)
   ld l,a
   inc bc
   ld a,(bc)
   ld h,a
   dec bc ;put pointer back
   xor a
   sbc hl,de
   jp z,.found ;we have a match
    
    inc bc
    inc bc
    inc bc
    inc bc
    inc bc
    inc bc
   
  ex af,af'
    
    dec a
    jp nz,.findbkgloop

    jp .foundnothing

.found:
  
    push bc
    pop de
    
    pop hl ;get mapadress back
    push hl
     inc de ;get to table shift 2 bytes
     inc de 
     
  ;now write the block into the map
  ld a,(de)
  ld (hl),a
  inc hl
  inc de
  ld a,(de)
  ld (hl),a
  push de
  ld de,(maplenght)
  add hl,de
  pop de
  inc de
  dec hl
  ld a,(de)
  ld (hl),a
  inc hl
  inc de
  ld a,(de)
  ld (hl),a
   
    jp .makecoin

;write special bkg to these blocks
.fixspecial:    
   
     ;now write the block into the map
  xor a
  ld (hl),a
  inc hl
  inc a
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  inc a
  inc a
  ld (hl),a
  dec hl
  dec a
  ld (hl),a
   
   
   
   jp .makecoin 
    

.foundnothing:

  pop hl
  push hl

  ;exception for maplevel 102
  ld a,(currentlevel)
  cp 102
  jp z,.fixspecial
  
  ;now write the block into the map
  xor a
  ld (hl),a
  inc hl
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  ld (hl),a
  dec hl
  ld (hl),a

    
.makecoin:

    pop hl
    pop de
    pop bc
  
;    ld a,(currentlevel)
;    cp 53
;    ret z ;ignore this map
;    cp 102
;    ret z
    exx
    ld a,c
    exx
    and a
    jp z,.storehl ;coin spawning limit reached store all brown blocks into pillarbkgbuffer use the pillarheight as pointer
    exx
    dec c
    exx

;PROBLEM: if no coins are being spawn in place then how does the engine that the brown blocks need to be returned?    
    
    push bc
    push de
    push hl

  ld hl,(maplenght)
  xor a
  sbc hl,de


  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c
  ld    (iy+4),0
                  ;set y custom object
  ld    (iy+5),78                ;set object number (coin)
  ld    (iy+6),0                ;set object number (coin)

  push ix

  call  activateobject

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  ld (ix+v7),1
  ld (ix+objectrelease),10 ;turn into ultracoin since this is a special spawn
 
  pop ix
 
    pop hl
    pop de
    pop bc
 
    
    ret

;160 blocks can be written to this place max limit this!
;multiple p-box on one map? can it be?
.storehl:

    push hl

    exx

    
    ld hl,(pillarheight)
    ld de,310 ;limit to 160 blocks
    xor a
    sbc hl,de
    jp nc,.stopsearch    ;first check if code works
    
    
    ld de,(pillarheight) ;get pointer
    ld hl,pillarbkgbuffer
    inc de
    inc de ;word
    ld (pillarheight),de
    add hl,de
    
    pop de
    
    ld (hl),e ;and mapadress is stored for later in the pillarbkgbuffer. BUT REMEMBER!!!! This WILL colide with any code using this buffer  
    inc hl
    ld (hl),d
    
    exx
    
    ret

.stopsearch:

;limit reached? do not store the block!
    pop de
    exx
    ret
    

    
getbrownblocksback:


    ld de,(pillarheight) ;get pointer
    ld hl,pillarbkgbuffer
    add hl,de    
    ld a,e
    or d
    ret z ;pointer = @ 0    
    dec de
    dec de ;word
    ld (pillarheight),de


    ld e,(hl) ;get mapadress
    inc hl
    ld d,(hl)
    ex de,hl    
    
 
    ld a,204
    ld (hl),a
    inc hl
    inc a
    ld (hl),a
    ld de,(maplenght)
    add hl,de
    dec hl
    inc a
    ld (hl),a
    inc hl
    inc a
    ld (hl),a ;write a brown block to the current fetched mapadress

    
    
    jp getbrownblocksback 
    
;    ret
    
    
    

;this routine is called once from the loader and checks which color of blocks need to be placed on the map
Transformyellowblocks:

;no switch activated?
  ld a,(yellowswitchactive)
  and a
  ret z
  
  ld a,(noblock)
  and a
  ret z
  dec a
  call z,checkyellow
  ld a,(noblock)  
  cp 2
  call z,checkgreen
  ld a,(noblock)   
  cp 3
  call z,checkred
  ld a,(noblock) 
  cp 4
  call z,checkblue
  
;do a full map search
 
 ; ld hl,(mapheightmin23)
 ; add 23
  ld bc,(mapheight)
 ; ld b,a
  
  ld hl,0 ;set hl to zero before we start the multiplication
  
  ld de,(maplenght) ;we search the rows
  
;we first multiply the maplenght x mapheight    
.loop:
  add hl,de
  dec bc
  ld a,c
  or b
  jp nz,.loop
 
  
;  djnz .loop  
  
;now we have the total amount of tiles we need to search the map for
  ex de,hl ;do transfer of data

  ld hl,bgrmapaddr

.loop2:
  ld a,(hl)
  cp 1 ;block found on map??
  call z,.writeblock
  ld a,(hl)
  cp 3
  call z,.writeblock2
  inc hl
  dec de
  ld a,e
  cp 0
  jp z,.quitloop
  jp .loop2


.quitloop:
;are we really sure about that?

  ld a,d
  cp 0
  jp nz,.loop2

  ret

;write the new block in place
.writeblock:



  ld a,196
  ld (hl),a
  inc hl
  ld a,197
  ld (hl),a

  dec hl

  ret



.writeblock2:


  ld a,198
  ld (hl),a
  inc hl 
  ld a,199
  ld (hl),a
  
  dec hl


  ret

checkyellow:

  ld a,(yellowswitchactive)
  bit 0,a
  ret nz
  
  inc sp
  inc sp

  ret

checkgreen:

  ld a,(yellowswitchactive)
  bit 1,a
  ret nz
  
  inc sp
  inc sp

  ret

checkred:

  ld a,(yellowswitchactive)
  bit 2,a
  ret nz
  
  inc sp
  inc sp

  ret

checkblue:

  ld a,(yellowswitchactive)
  bit 3,a
  ret nz
  
  inc sp
  inc sp

  ret

;update the tilegfx on all pages. In a = tilenumber to change HL = Gfxadress DE = Gfx coloradress
updatetilegfx:
  
  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
  push hl
 
  call .doshit

  pop hl
  ld de,$4000
  add hl,de
  push hl

  call .doshit  

  pop hl
  ld de,$4000
  add hl,de
  
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

  ld c,$98
  ld   a,8
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


  call  SetVdp_Write                                  ;set vdp to write at character of tile

  exx
  push de
  exx
  pop hl

    ld c,$98
  ld   a,8
  call outix32

  ret

screen5:
  ;switch to screen 5
  ld    a,(VDP_0)
  or    %0000'0010		      ;m3=1
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 5 
  ret
  
screen4:
;switch to screen 4
  ld    a,(VDP_0)
  and   %1111'1101		      ;m3=0
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 4  
  ret

jumptocutscene:
  
  ld a,cutsceneblock
  call block12
  
  call cutscene

  ld a,loaderblock
  call block34

  ret

calleventhandler:

;  ld		a,(slot.ram)	;back to full RAM
;	out		($a8),a   
   
  call eventhandler

  ld		a,(slot.page12rom)	;back to full RAM
	out		($a8),a

  ld a,cutsceneblock
  call block12

  ret

startcutscenemusic: 


  xor a
  call  block12             ;set music replayer in page 2, at $8000

  ld         a,(currentmusicblock)
  call       block34                       ;set music in page 1, at $4000


;  - In: A = No. of Repeats (0 = infinite), HL = Address of BGM data
;  - Out: A = Playing flag (0 = not playing, 255 = playing)

  ld a,(cutscenenr)
  cp 9
  ld a,1
  call z,.setinfinite
  
;  ld    hl,$4000
  call  startfm             ;to initialise the music.
  
   
  ld a,cutsceneblock
  call block12

  ret

  
.setinfinite:

    xor a
    ret 

tripscreenupdown:

  ld a,(tripscreen)
  inc a
  ld (tripscreen),a
	cp 1
	jp z,.settripscreenup
	cp 2
  jp z,.settripscreendown
	cp 3
	jp z,.settripscreenup
	cp 4
  jp z,.settripscreendown
  cp 5
	jp z,.settripscreenup
	cp 6
  jp z,.settripscreendown	
	cp 7
	jp z,.settripscreenup
	cp 8
  jp z,.settripscreendown
  cp 9
  jp z,.settripscreenup
  cp 10
  ret nz
  
  xor a
  ld (tripscreen),a

  ret

.settripscreenup:

	ld		a,(VDP_8+15)
	inc a
	inc a
	ld c,a
	di
	out		($99),a
	ld		a,23+128
	ei
	out		($99),a	
	jp .correctlineint
	
 ; ret

.settripscreendown:


	ld		a,(VDP_8+15)
	ld c,a
	di
	out		($99),a
	ld		a,23+128
	ei
	out		($99),a
 ; ret

.correctlineint:
  ld    a,c
 ; xor a
  di
  out   ($99),a
  ld    a,19+128
  ei
  out   ($99),a
  ret 

pollvdpcmd:

.poll:
;keep polling until the command is finished
  ld    a,2               ;set s#2
  di
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  ei
  jp    c,.poll        ;lineint detected, so jp to that routine

  ret

;set scc sound to play
ReplayerInit:


;       ld      a,(sccsfxnr)
        ld l,a
        ld      h,0
        
        xor a
        ld (SamplePos),a
        ld (NumBlocksToPlay),a
        ld (SamplePage),a
  
        
  in a,($a8)
  push af

  ld a,(slot.page12rom)
  out ($a8),a

        ld                a,(memblocks.n1)     ;save blocks
        ld c,a
        ld                a,(memblocks.n2)
        ld b,a
  
        push bc

        ld                a,SCCreplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
                di
        ld                ($5000),a
        inc               a
        ld                ($7000),a
 ;       call block12

        ld      d,h
        ld      e,l

        add     hl,hl
        add     hl,hl
        add     hl,de

        ld      de,SfxTable
        add     hl,de

        ld      e,(hl)
        inc     hl
        ld      d,(hl)
        inc     hl
   ;     ld de,$4000
        ld      (SamplePos),de

        ld		  a,(hl)
        inc     hl
    ;    ld a,$93
        ld      (SamplePage),a

        ld      e,(hl)
        inc     hl
        ld      d,(hl)
        ld      (NumBlocksToPlay),de

        ld      a,0FFh
        ld      (SccSfxOn),a

   ;reset blocks
   pop bc
   
   ld a,c
;        ld                a,(memblocks.1)     ;reset blocks
        ld                ($5000),a
;        ld                a,(memblocks.2)
    ld a,b
        ld                ($7000),a

    ei        
        
    pop af
    out ($a8),a

    
        ret
		

droppowerreservedetected:

  ld b,a ;save a
  
  ld a,(powerreserve)
  and a
  ld a,b
  ret z
  
  ld a,sccpowerreserve
  call ReplayerInit
  
  ld a,1
  ld (droppowerup),a
  ld a,b
  ret




checkblockabovemario:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;om te voorkomen dat mario in zijn val toch de blok kan activeren
  ld a,(jumpspeed)
  and a
  jp z,.endblocktilescheck

  call checktileabovemarioram
  ld    a,191                   ;alle hardtiles
  cp    (hl)                    ;kijk of de huidige tile groter is dan 191
  jp    nc,.endblocktilescheck

  ld a,(currentlevel)
  cp 51
  jp z,.disablerotatingcheck
  cp 52
  jp z,.disablerotatingcheck  

  ld    a,203                   ;alle blocks (vraagteken, uitroepteken en roterend block)
  cp    (hl)                    ;kijk of de huidige tile een block is
  jp    c,.endblocktilescheck

.continueblocktilescheck:
  ld    a,1
  ld    (activatenewblock?),a   ;mario activates a new block
  ld    (blocksactive?),a       ;there is now at least 1 block active, so activate blockhandler
  ld    (blockaddress),hl       ;hl points to tile in map
.endblocktilescheck:

  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret

.disablerotatingcheck:

  ld    a,199                   ;alle blocks (vraagteken, uitroepteken en roterend block)
  cp    (hl)                    ;kijk of de huidige tile een block is
  jp    c,.endblocktilescheck


  jp .continueblocktilescheck

include "eventhandlery.asm"
;{
eventhandler:

  xor a
  ld (standingonsprite),a
  ld (standingonplatform),a
  ld (specialsheertile),a
  ld (touchside),a 

 ; call  setcameravalues         ;sets 16 bit values for cameray and camerax
  
  ld   hl,(cameray)           ;use this if oldcamerascroll is disabled (cameray is then 16 bits) do the same for camerax

  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  add   hl,hl                   ;*8
  ld    (cameraytimes8),hl      ;cameray is now rounded down to 8 pixels, which might give problems later, lets see

  ld    hl,(camerax)
  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  add   hl,hl                   ;*8
	ld		a,(VDP_8+10)            ;to obtain the exact 16 bits camerax value, add the horizontal scroll register
	ld    d,0
	ld    e,a
	add   hl,de
  ld    (cameraxprecise),hl  
  
  
  
  call  handleObjectlist        ;handle movement, write char-,color- and spatdata, and sets object inactive if out of camerarange
.calleventhandlery:  
  call  adjustlevObjListpointer ;set objectlistpointer (which object is nearest to camera ?)
  call  setnewobjects           ;check levelObjectList if new objects need to be set in play

  ret

  
  
;setcameravalues:
;
;  ld   hl,(cameray)           ;use this if oldcamerascroll is disabled (cameray is then 16 bits) do the same for camerax
;  add   hl,hl                   ;*2
;  add   hl,hl                   ;*4
;  add   hl,hl                   ;*8
;  ld    (cameraytimes8),hl      ;cameray is now rounded down to 8 pixels, which might give problems later, lets see
;
;  ld    hl,(camerax)
;  add   hl,hl                   ;*2
;  add   hl,hl                   ;*4
;  add   hl,hl                   ;*8
;	ld		a,(VDP_8+10)            ;to obtain the exact 16 bits camerax value, add the horizontal scroll register
;	ld    d,0
;	ld    e,a
;	add   hl,de
;  ld    (cameraxprecise),hl
;  ret


handleObjectlist:

 ;   ld a,3     ;this routine changes backdrop color, and can be used in between these engine routines, to see how fast the routines are
 ;   out ($99),a
 ;   ld a,7+128
 ;   out ($99),a	
 
  ld a,(pauze)
  dec a
  ret z

  ld    ix,Objectlist+(lenghtobjectlist*00) : bit 0,(ix) : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : bit 0,(ix) : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : bit 0,(ix) : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : bit 0,(ix) : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : bit 0,(ix) : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : bit 0,(ix) : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : bit 0,(ix) : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : bit 0,(ix) : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : bit 0,(ix) : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : bit 0,(ix) : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : bit 0,(ix) : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : bit 0,(ix) : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : bit 0,(ix) : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : bit 0,(ix) : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : bit 0,(ix) : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : bit 0,(ix) : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : bit 0,(ix) : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : bit 0,(ix) : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : bit 0,(ix) : call nz,.handleobject ;objectnr 20
  
  ;  ld a,15     ;this routine changes backdrop color, and can be used in between these engine routines, to see how fast the routines are
  ;  out ($99),a
  ;  ld a,7+128
  ;  out ($99),a	  
  
  
  ret

.handleobject:                ;handle movement, write char-,color- and spatdata, 


;mario sterft alles stop zetten
  ld a,(mariotransforming)
  and a
  jp nz,.donehandleobjectmovement ;stop everything but DO update the sprites


;framecounter per object!
    inc (ix+deathtimer)
    inc (ix+objectfp)
    

;when a sprite gets removed from the screen sometimes the sprite remains in the Vram of the other sprite
;by forcing the sprite to be updated this sprite gets removed from the vram and the flickr stops  
  ld a,(framecounter)
  and 63;127
  call z,.forceupdatesprite

  
;thank you once again grauw
    ld    a,(slot.page12rom)                            ;all RAM except page 12
    out   ($a8),a  
  
  	ld e,(ix+movepat) 
	ld d,(ix+movepat+1) 
	ld hl,eventhandlerjumptable ;10T
	add hl,de ;11T
	add hl,de ;11T
	
    ld a,eventhandlertablesblock ;7T
    di ;4T
;	ld		(memblocks.n1),a ;13T
	ld		($5000),a ;13T
	
	ld a,(hl) ;7T
	inc hl  ;6T
	ld h,(hl) ;7T
	ei ;4T
	ld l,a ;4T
	jp (hl) ;4T
  
  ;88T totaal
  

    
.forceupdatesprite:

    ld a,(ix+nosprupdate) ;do not update these type of sprites
    cp 2
    ret z    
 
    ld l,(ix+movepat)
    ld h,(ix+movepat+1)
    ld de,10
    and a
    sbc hl,de
    ret z
    inc hl
    ld a,l
    or h
    ret z 
 
 
;    ld l,(ix+movepat)
;    ld h,(ix+movepat+1)
;    ld de,10
;;    cp 10
;    xor a
;    push hl
;    sbc hl,de
;    pop hl
;    ret z ;do not update the waiting piranja plant
;;    cp 9
;    ld de,9
;;    cp 10
;    xor a
;    push hl
;    sbc hl,de
;    pop hl
;    ret z
    ld (ix+updatespr?),2
    ret

.handleromprogblock3:


;handleobjectmovement word vanuit de rom aangeroepen
  ld a,romprogblock3
  ld (romprogblocknumber),a

  call setpage12tohandleobjectmovementfast

  call HandleObjectMovement


  jp .donehandleobjectmovement


.handleromprogblock4:

  ld a,romprogblock4 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement2  

  jp .donehandleobjectmovement
  

.handleromprogblock5:

  ld a,romprogblock5 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement3  

  jp .donehandleobjectmovement
  

.handleromprogblock6:

  ld a,romprogblock6 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement4  

  jp .donehandleobjectmovement

.handleromprogblock7:

  ld a,romprogblock7 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement5  

  jp .donehandleobjectmovement  

.handleromprogblock8:

  ld a,romprogblock8 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement6  

  jp .donehandleobjectmovement 


.handleromprogblock9:

  ld a,romprogblock9 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement7

  jp .donehandleobjectmovement 


.handleromprogblock10:

  ld a,romprogblock10 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement8

  jp .donehandleobjectmovement 

.handleromprogblock11:

  ld a,romprogblock11 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement9

  jp .donehandleobjectmovement 

.handleromprogblock12:

  ld a,romprogblock12 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement10

  jp .donehandleobjectmovement  

 
.handleromprogblock13:

  ld a,romprogblock13 ;switch to next dynamic movement block
  ld (romprogblocknumber),a
;  call  block34
  call setpage12tohandleobjectmovementfast


  call HandleObjectMovement11

 ; jp .donehandleobjectmovement  
 

.donehandleobjectmovement:

;  xor a
;  ld (handlecomplete),a

  ld (ix+standingonspr),0

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;skip special rules during nocontrol phase; BUG: we need to get back on this
;  ld a,(nocontrol)
;  and a
;  jp nz,.endtest

  ld a,(ix+amountspr) ;only update the coordinates if the object has no sprites
  and a
  jp z,objectwithoutsprites

  ld b,a
;  jp .end ;uncomment/comment this line to activate or deactivate the spriteflicker routine

  ;check for double sprites
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
 ; ld    b,(ix+amountspr)        ;amount of sprites
  ld a,2
.checkfornrtwo:
  ;get the value
  cp (hl)
  jp z,.dospecialrule
  inc   hl
  djnz  .checkfornrtwo
;  jp .end
  ;/check for double sprites

.end:


  ld (ix+updateoddoreven?),0 ;always reset this byte if no sprlst 2 has been found
  ld (ix+clearvram),0 ;always reset this byte if no sprlst 2 has been found

.endtest:

;check if sprite out of screen and thus not visible instead of setting it inactive do not out the sprite to VRAM so that the slow poke MSX machines can handle the busy stuff

  ld    l,(ix+yobject)          ;y value of sprite (16 bits)
  ld    h,(ix+yobject+1)        ;y value of sprite (16 bits)
  ld de,16
  add   hl,de                   ;y value objects+offset

  ld    de,(cameraytimes8)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract y value of camera from y value of sprite
  jp    c,.endupdatesprite       ;sprite is out of screen (top side of camera)
  
  ld    c,l                     ;8 bits y value of sprite

  ld    de,212+16
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.endupdatesprite         ;sprite is out of screen (bottom side of camera)
;/check if sprite out of screen and thus not visible instead of setting it inactive do not out the sprite to VRAM so that the slow poke MSX machines can handle the busy stuff

  ld    a,(ix+updatespr?)       ;only update (write character and color) sprite if requested
  or    a                       ;important notice: if sprite is to be updated, then update 2 consecutive frames
  jr    z,.endupdatesprite      ;because we use 2 spritecharacter&color tables which alternate each frame
  dec   (ix+updatespr?)         ;therefore both tables should be updated if updatespr is requested
  
  
.updatesprite:
 ;branch here and merge when done
; include "updatesprite.asm"

  ;set ingamespritesblock in page 1+2
	ld		a,(slot.page12rom)	    ;all RAM except page 1+2
	out		($a8),a	
  ld    a,(ix+spritesblock)
	call	block1234               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 1+2

;TODO: copy all the spritedata into a position of the vram. We must prevent outing from the RAM to VRAM this part is taking 30% of our clockcycles!

  ;set vdp address to write at color table and write spritecolor
	ld		hl,(invissprcoltableaddress)
  ld    e,(ix+coloraddress)     ;address of spritenumber in color table of VRAM (16 bits)
  ld    d,(ix+coloraddress+1)   ;address of spritenumber in color table of VRAM (16 bits)
	add   hl,de
	ld		a,1
	call	SetVdp_Write

  ld    l,(ix+spritecolROM)     ;ROM address of spritecolor date in ingamespritesblock (16 bits)
  ld    h,(ix+spritecolROM+1)   ;ROM address of spritecolor date in ingamespritesblock (16 bits)
	ld		c,$98
  ld    a,(ix+amountspr)        ;amount of sprites
.colorloop:
	call	outix16                 ;out 16*a bytes (1 sprite=16 bytes) of sprite colordata, hl-> adress on block
  dec   a
  jp    nz,.colorloop
  ;/set vdp address to write at color table and write spritecolor

  ;set vdp address to write at character table and write spritecharacter
  ex    de,hl                   ;set color address in hl
  add   hl,hl                   ;(color address*2) = character address (character address is 2x higher than color)
	ld		de,(invissprchatableaddress)
	add   hl,de
	ld		a,1
	call	SetVdp_Write

  ld    l,(ix+spritecharROM)    ;ROM address of spritecharacter date in ingamespritesblock (16 bits)
  ld    h,(ix+spritecharROM+1)  ;ROM address of spritecharacter date in ingamespritesblock (16 bits)
	ld		c,$98
  ld    a,(ix+amountspr)        ;amount of sprites
.characterloop:
	call	outix32                 ;out 32*a bytes (1 sprite=32 bytes) of sprite characterdata, hl-> adress on block
  dec   a
  jp    nz,.characterloop


  ld		a,(slot.ram)	          ;back to full RAM
	out		($a8),a

  ;/set vdp address to write at character table and write spritecharacter
.endupdatesprite:


  ld    l,(ix+spataddress)      ;spat address of sprite (16 bits)
  ld    h,(ix+spataddress+1)    ;spat address of sprite (16 bits)
  push  hl
  pop   iy                      ;mirror spat address of sprite in iy

  ld    l,(ix+yobject)          ;y value of sprite (16 bits)
  ld    h,(ix+yobject+1)        ;y value of sprite (16 bits)
  ld    (yobjectmirror),hl      ;mirror y of sprite
  ld    l,(ix+xobject)          ;x value of sprite (16 bits)
  ld    h,(ix+xobject+1)        ;x value of sprite (16 bits)
    ;this code is for v9938     ;x value of sprite has to be shifted depending on v9938 or v9958
  ld    a,(v9958found?)
  or    a                       ;check v9958
  jp    nz,.v9958found

  ld    d,0
	ld		a,(VDP_8+10)
  ld    e,a
  add   hl,de
  ld    de,-7
  add   hl,de
.v9958found:
    ;/this code is for v9938
;  dec   hl
  ld    (xobjectmirror),hl      ;mirror x of sprite (with offset for v9938)


  ld    b,(ix+amountspr)        ;amount of sprites
  ld    a,b
  or    a
  jp    z,objectwithoutsprites
  
  push ix
  exx
  pop hl
  exx

.loopy:  
  db 0,0,0 ;why is this here??!! because this is where we branch the xy flip
  
.loop:
  ;set sprite y coordinate in spat
  ld    hl,(yobjectmirror)
  ld    e,(ix+offsets+0)        ;y offset of sprite (16 bits)
  ld    d,(ix+offsets+1)        ;y offset of sprite (16 bits)
  add   hl,de                   ;y value objects+offset

  ld    de,(cameraytimes8)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract y value of camera from y value of sprite
  jp    c,.outofscreentop       ;sprite is out of screen (top side of camera)
  
  ld    c,l                     ;8 bits y value of sprite

  ld    de,212
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.outofscreen         ;sprite is out of screen (bottom side of camera)

  ld    (iy+00),c               ;set sprite y in spat
  ;/set sprite y coordinate in spat
  jp .checkbottom

.checkforx:

  ;set sprite x coordinate in spat
  ld    hl,(xobjectmirror)
  ld    e,(ix+offsets+2)        ;x offset of sprite (16 bits)
  ld    d,(ix+offsets+3)        ;x offset of sprite (16 bits)
  add   hl,de                   ;x value objects+offset

  ld    de,(cameraxprecise)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract x value of camera from x value of sprite  
  jp    c,.outofscreenleft      ;sprite is out of screen (left side of camera)
  
  ld    c,l                     ;8 bits x value of sprite

  ld    de,256
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.outofscreenright    ;sprite is out of screen (right side of camera)  
  ld    (iy+01),c               ;set sprite x in spat
  ;/set sprite x coordinate in spat

.nextsprite:
  ld    de,4
  add   iy,de                   ;iy increased by 4 to write to next sprites spat values
  add   ix,de                   ;ix increased by 4 to read out next sprites offset values

  djnz  .loop                   ;next sprite
  ;/set sprite coordinates in spat

;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

  ret

  
.dospecialrule: ;double sprite found. check in which frame we are


  ld a,(framecounter)
  and 1         ; hiermee checken we of de framecounter even (=0) of oneven (=1) is
  ld  b,a

  ld a,(ix+updateoddoreven?)
  cp b
  jp nz,clearsprfromvram 

  jp .updatesprite  
  
.outofscreentop:

  ld    (iy+00),220             ;set sprite y in spat

;if the camera is fully on top we still want to check a little bit higher. So you can trow objects a little bit out of the upper map edge 
  push af
;  push hl
  ld    hl,(cameraytimes8)
;  ld de,0
;  xor a
;  sbc hl,de
;  pop hl
  ld a,l
  or h
  jp z,.checkboundary
  
  pop af
 
  ld    de,(yobjectmirror)
  ld hl,4
  xor a
  sbc hl,de
  jp c,.checkforx
 
 
  jp    .setspriteinactive


.checkboundary:
  pop af

  ld    de,120              ;if sprite is "de" pixels out of screen top, then set sprite inactive
  add   hl,de                   ;carry isnt set here
  jp    c,.nextsprite           ;if carry then sprite is still within active range

  jp    .setspriteinactive


.outofscreen:
  ld    (iy+00),220             ;set sprite y in spat

;make the bottom limitless in depth and compare the sprite coordinates to the absolute maximum

.checkbottom:
  ld    hl,(yobjectmirror)
  ;ld l,(ix+yobject)
  ;ld h,(ix+yobject+1)
  ld de,16
  add hl,de

  ld de,(mapheightx8)

  xor a
  sbc hl,de
  jp    c,.checkforx           ;if carry then sprite is still within active range Y then check if still in active x range

;  ld    de,40                   ;if sprite is "de" pixels out of screen bottom, then set sprite inactive
;  sbc   hl,de                   ;carry isnt set here
;  jp    c,.nextsprite           ;if carry then sprite is still within active range
  jp    .setspriteinactive

.outofscreenright:
  ld    (iy+00),220             ;set sprite y in spat

  ld    de,120                   ;if sprite is "de" pixels out of screen right, then set sprite inactive
  sbc   hl,de                   ;carry isnt set here
  jp    c,.nextsprite           ;if carry then sprite is still within active range
  jp    .setspriteinactive

.outofscreenleft:
  ld    (iy+00),220             ;set sprite y in spat

  ld    de,80                   ;if sprite is "de" pixels out of screen left, then set sprite inactive
  add   hl,de                   ;carry isnt set here
  jp    c,.nextsprite           ;if carry then sprite is still within active range
  jp    .setspriteinactive

.setspriteinactive:           ;if an enemy consists of (example) 4 sprites, then only check the first/primary sprite
 
  exx
  push hl
  exx
  pop ix


  ld    a,(ix+amountspr)        ;if we are dealing with the primary sprite (compare amountspr with b) then set inactive
  cp    b                       ;check if the sprite we are trying to make inactive is the primary sprite
  jp    nz,handleObjectlist.nextsprite

  
  ;test debug. do not use in final release but write it in here
  ld l,(ix+movepat)
  ld h,(ix+movepat+1)
  ld de,367
  xor a
  sbc hl,de
  ret z ;Final boss test code
  
;TODO: create possibility to execute offload script on object

  ld a,(ix+edible)
  cp 10
  call z,.resetactivebyte
  ;legacy 8bit code to keep this part as snappy as possible
  ld a,(ix+movepat+1)
  and a
  jp nz,.skipchecks
  ld a,(ix+movepat)
  cp 56
  push af
  call z,removebanzaibill
  pop af

;  ld a,(ix+movepat)
  cp 105
  push af
  call z,.removeyoshihead
  pop af
  cp 106
  push af
  call z,.removeyoshihead
  pop af

 ; cp 178
 ; ret z ;diagonal block helper object
;never destroy yoshi or shell if he is on mario
;  ld a,(ix+movepat)
  cp 103
  ret z
  cp 104
  ret z
  cp 11 ;-shell
  ret z
  cp 92 ;vine plant
  call z,.checkforstatus2 ;is only allowed to be destroyed if the plant decides for himself
  cp 94 ;p-box
  ret z
  cp 123
  ret z
  cp 100
  call z,.checkforstatus
   ; cp 147
   ; ret z ;test
  
  
.skipchecks:


  ld    (ix+active?),0          ;our primary sprite is out of active range, set sprite inactive
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v6),0
  ld (ix+v7),0
    ld a,(ix+movepat)
    cp 30
    call z,.removebullet

  ld (ix+objectrelease),0 ;must happen for the one time only objects
  ld (ix+movepat),0
  ld (ix+movepat+1),0
  ld (ix+deathtimer),0
  ld (ix+killshell),0
  ld (ix+updateoddoreven?),0
  ld (ix+clearvram),0


  call removeobjectspat

  ;remove sprite from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
  ld    b,(ix+amountspr)        ;amount of sprites
.cleanspritesfromspritelist:
  ;we no longer write 0 but decrease the total value to prevent open object positions
  ld a,(hl)
  and a
  jp z,.noremove ;do not make a -1
  dec a
  cp 1
  call z,clearvramonothersprite 
  ld (hl),a
.noremove:
  ;ld    (hl),0
  inc   hl
  djnz  .cleanspritesfromspritelist
  ;/remove sprite from spritelist

 
  ;do not return the object if it was destroyed by destroyobject
  ld a,(ix+objectreturn)
  and a
  ld a,0 ;always remove this from the list before we fully deactivate
  ld (ix+objectreturn),a
  ret nz
  
  ;sprite becomes avaiable again in the levelObjectList
  ld    l,(ix+levobjlistadd)    ;level object list address (16 bits) of this object
  ld    h,(ix+levobjlistadd+1)  ;level object list address (16 bits) of this object  
  set   7,(hl)                  ;sprite is now inactive, so make it available in levelObjectList again
  ;/sprite becomes avaiable again in the levelObjectList
  ret

.resetactivebyte:

  xor a
  ld (magikoopaactive),a

  ret

;check if the coin must persist
.checkforstatus:

  ld a,(ix+v7)
  and a
  ret z
  
  
  inc sp
  inc sp


  ret

.checkforstatus2:

  ld a,(ix+v5)
  and a
  ret nz
  
  
  inc sp
  inc sp


  ret


;stukje code om de bullet te verwijderen als die out of screen is
.removebullet:

  ld a,(ix+movepat+1)
  and a
  ret nz

  ld a,(bulletsingame)
  dec a
  ld (bulletsingame),a
  ret


.removeyoshihead:

  push ix
  ld ix,(yoshiheadix)
  call destroyobject
  pop ix

;reset any type of yoshi
  xor a
  ld (yoshiflying),a
  ld (yoshitype),a

  ret


objectwithoutsprites:



  ld    l,(ix+yobject)          ;y value of sprite (16 bits)
  ld    h,(ix+yobject+1)        ;y value of sprite (16 bits)
  ld    (yobjectmirror),hl      ;mirror y of sprite
  ld    l,(ix+xobject)          ;x value of sprite (16 bits)
  ld    h,(ix+xobject+1)        ;x value of sprite (16 bits)
    ;this code is for v9938     ;x value of sprite has to be shifted depending on v9938 or v9958
  ld    a,(v9958found?)
  or    a                       ;check v9958
  jp    nz,.v9958found

  ld    d,0
	ld		a,(VDP_8+10)
  ld    e,a
  add   hl,de
  ld    de,-7
  add   hl,de
.v9958found:
    ;/this code is for v9938
;  dec   hl
  ld    (xobjectmirror),hl      ;mirror x of sprite (with offset for v9938)


  ;set sprite y coordinate in spat
  ld    hl,(yobjectmirror)
  ld    e,(ix+offsets+0)        ;y offset of sprite (16 bits)
  ld    d,(ix+offsets+1)        ;y offset of sprite (16 bits)
  add   hl,de                   ;y value objects+offset

  ld    de,(cameraytimes8)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract y value of camera from y value of sprite
  jp    c,.outofscreentop       ;sprite is out of screen (top side of camera)
  
  ld    c,l                     ;8 bits y value of sprite

  jp .outofscreen
 ; ld    de,212
 ; sbc   hl,de                   ;carry isnt set here
 ; jp    nc,.outofscreen         ;sprite is out of screen (bottom side of camera)
;  ld    (iy+00),c               ;set sprite y in spat
  ;/set sprite y coordinate in spat

.checkforx:

  ;set sprite x coordinate in spat
  ld    hl,(xobjectmirror)
  ld    e,(ix+offsets+2)        ;x offset of sprite (16 bits)
  ld    d,(ix+offsets+3)        ;x offset of sprite (16 bits)
  add   hl,de                   ;x value objects+offset

  ld    de,(cameraxprecise)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract x value of camera from x value of sprite  
  jp    c,.outofscreenleft      ;sprite is out of screen (left side of camera)
  
  ld    c,l                     ;8 bits x value of sprite

  ld    de,256
  sbc   hl,de                   ;carry isnt set here
  jp    nc,.outofscreenright    ;sprite is out of screen (right side of camera)  
 ; ld    (iy+01),c               ;set sprite x in spat
  ;/set sprite x coordinate in spat


  ;/set sprite coordinates in spat

;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

  ret

.outofscreentop:

;  ld    (iy+00),220             ;set sprite y in spat
 
  ld    de,(yobjectmirror)
  ld hl,4
  xor a
  sbc hl,de
  jp c,.checkforx
 
 
  jp    .setspriteinactive


.outofscreen:
;  ld    (iy+00),220             ;set sprite y in spat

;make the bottom limitless in depth and compare the sprite coordinates to the absolute maximum

  ld    hl,(yobjectmirror)
  ;ld l,(ix+yobject)
  ;ld h,(ix+yobject+1)
  ld de,16
  add hl,de
  ld de,(mapheightx8)
  xor a
  sbc hl,de
  jp    c,.checkforx           ;if carry then sprite is still within active range Y then check if still in active x range

;  ld    de,40                   ;if sprite is "de" pixels out of screen bottom, then set sprite inactive
;  sbc   hl,de                   ;carry isnt set here
;  jp    c,.nextsprite           ;if carry then sprite is still within active range
  jp    .setspriteinactive

.outofscreenright:
;  ld    (iy+00),220             ;set sprite y in spat

  ld    de,120                   ;if sprite is "de" pixels out of screen right, then set sprite inactive
  sbc   hl,de                   ;carry isnt set here
;  jp    c,.nextsprite           ;if carry then sprite is still within active range
  ret c
  jp    .setspriteinactive

.outofscreenleft:
;  ld    (iy+00),220             ;set sprite y in spat

  ld    de,80                   ;if sprite is "de" pixels out of screen left, then set sprite inactive
  add   hl,de                   ;carry isnt set here
;  jp    c,.nextsprite           ;if carry then sprite is still within active range
  ret c
  jp    .setspriteinactive

.setspriteinactive:           ;if an enemy consists of (example) 4 sprites, then only check the first/primary sprite

  ld a,(ix+movepat+1)
  and a
  jp nz,.skip
  ld a,(ix+movepat)
  cp 230 ;horzizontal object mover
  ret z
  cp 122 ;water
  ret z

.skip:

  ld    (ix+active?),0          ;our primary sprite is out of active range, set sprite inactive
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v6),0
  ld (ix+v7),0  

  ld (ix+objectrelease),0 ;must happen for the one time only objects
  ld (ix+movepat),0
  ld (ix+movepat+1),0
  ld (ix+deathtimer),0
  ld (ix+killshell),0
  ld (ix+updateoddoreven?),0
  ld (ix+clearvram),0

  ;do not return the object if it was destroyed by destroyobject
  ld a,(ix+objectreturn)
  and a
  ld a,0 ;always remove this from the list before we fully deactivate
  ld (ix+objectreturn),a
  ret nz

  ;sprite becomes avaiable again in the levelObjectList
  ld    l,(ix+levobjlistadd)    ;level object list address (16 bits) of this object
  ld    h,(ix+levobjlistadd+1)  ;level object list address (16 bits) of this object  
  set   7,(hl)                  ;sprite is now inactive, so make it available in levelObjectList again
  ;/sprite becomes avaiable again in the levelObjectList
  ret


adjustlevObjListpointer:        ;set objectlistpointer left or right (alternate each frame) of nearest object
  ld    iy,(levelObjectListpointer)                                                            ;1130
  ld    l,(iy+1)                ;x object in tiles (16 bit)
  ld    h,(iy+2)                ;x object in tiles (16 bit)

  ;subtract 16 tiles from enemy
  ld    de,16
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;subtract 16 tiles from enemy
  jr    nc,.notcarry
  ld    hl,0
.notcarry:
  ex    de,hl
  ;subtract 16 tiles from enemy
  
  ld    hl,(camerax)            ;camera x in tiles (16 bit)
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de
  ld    de,-lenghtlevelObjectList
  jp    c,.setobjectlistpointer
  ld    de,lenghtlevelObjectList

.setobjectlistpointer:        ;move the objectlistpointer 1 object to the left or right (alternate each frame)
  add   iy,de

  ld    a,iyh
  ld    (levelObjectListpointer+1),a
  ld    a,iyl
  ld    (levelObjectListpointer),a
  ret


setnewobjects:                  ;we are going to check if an object is 8 tiles out of screen left or right
  call  setobjectrightofcamera
  call  setobjectlefttofcamera

  ;check if the level just started, and see if objects need to be placed in screen
  ld    a,(setobjectscurrentscreen?)
  and    a
  ret   z

.setcurrentscreenobjects:
  
  
  ld    hl,levelObjectList+lenghtlevelObjectList
  push  hl
  pop   iy
  
;find object nearest to left camera boundary and start from there

.findfirstobject:
    ld e,(iy+1)
    ld d,(iy+2)
    ld hl,(camerax)
    xor a
    sbc hl,de
    jp c,.foundfirstobject
    
    ld de,lenghtlevelObjectList
    add iy,de
    
    jp nz,.findfirstobject 
  

.foundfirstobject: 

  ld de,-lenghtlevelObjectList
  add iy,de

  ld    b,AmountofPossibleActiveObjects

.loop:
  call .checkforcoin
  push  bc
  call  activateobject
  pop   bc
.skip:
  ld    de,lenghtlevelObjectList
  add   iy,de                   ;check next object to the right
  djnz  .loop
  ;/check if the level just started, and see if objects need to be placed in screen  
  
  
  xor   a
  ld    (setobjectscurrentscreen?),a
  
  ret
  

;coin list
.checkforcoin:

  ld    a,(setobjectscurrentscreen?)
  cp 2
  ret nz

  ld a,(iy+4)
  cp 78
  ret z
  cp 83
  ret z
  
  inc sp
  inc sp
  jp .skip
 
  ;ret

  
  
setobjectlefttofcamera:
  ld    hl,(camerax)            ;camera x in tiles (for pixels we need to multiply by 8, but this is not needed here)
  or    a                       ;reset carry first for sbc hl,de
 ; ld    de,8
  ld    de,8
  sbc   hl,de                   ;check 8 tiles left of screen to see if there is an object
  ret   c
  ld    (cameraposition),hl

  ld    hl,(levelObjectListpointer)
 
  push  hl                      ;hl now points to object in objectlist to check
  pop   iy

  ld    bc,-lenghtlevelObjectList

.loopleft:
  ;check x=000 this defines the leftborder of the levelobjectlist
  ld    a,(iy+1)                ;x object (16 bit)
  ld    d,(iy+2)                ;x object (16 bit)
  or    d
  ret   z
  ;/check x=000 this defines the leftborder of the levelobjectlist

  call  checkobject
  jp    z,activateobject        ;if zero then object is exactly 8 tiles out of screen left, so activate
  ret   nc                      ;if not carry then we checked enough to the left

  add   iy,bc                   ;check next object to the left
  jp    .loopleft

;ret

setobjectrightofcamera:
  ld    hl,(camerax)            ;camera x in tiles (for pixels we need to multiply by 8, but this is not needed here)
;  ld    de,32+8
  ld    de,32+4
  add   hl,de                   ;check 8 tiles right of screen to see if there is an object
  ld    (cameraposition),hl

  ld    hl,(levelObjectListpointer)
 
  push  hl                      ;hl now points to object in objectlist to check
  pop   iy

  ld    bc,lenghtlevelObjectList

.loopright:
  call  checkobject
  jp    z,activateobject        ;if zero then object is exactly 8 tiles out of screen right, so activate
  ret   c                       ;if carry then we checked enough to the right                      

  add   iy,bc                   ;check next object to the right
  jp    .loopright


;ret

checkobject:                    ;we check if object in objectlist should be activated
  ld    hl,(cameraposition)     ;check 8 tiles left/right of screen

  ld    e,(iy+1)                ;x object in tiles (16 bit)
  ld    d,(iy+2)                ;x object in tiles (16 bit)
  
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de
  ret
;}
;end of leftright code
endeventhandlerx: equ $ - eventhandler

activateobject:

 ; ld hl,dummyobjectlist
 ; push hl
 ; pop ix ;In case the object does not spawn: echo a fake IX back to the caller otherwise the caller causes trouble to itself.
    ld ix,dummyobjectlist
 
 
  bit   7,(iy)                  ;is object available? (check bit 7: 0xxxxxxx=no, 1xxxxxxx=yes)
  ret   z                       ;this object is not avaiable (its either dead or already active)

  ;search empty space in objectlst
  ld    b,1                     ;object number in objectlist
  ld    hl,Objectlist
  ld    de,lenghtobjectlist
  
.searchemptyplace:
  ld    a,(hl)
  or    a
  jr    z,.emptyplacefound      ;there is an empty place in the Object list, put object here
  add   hl,de                   ;next object
  inc   b                       ;increase object number
  jp    .searchemptyplace

.emptyplacefound:
  ld    a,AmountofPossibleActiveObjects
  sub   a,b
  ret   c                       ;this is the overflow check, if carry then no more empty space

  push  hl
  pop   ix
  ;/search empty space in objectlst

  ;set x object
  ld    h,(iy+2)                ;x object in tiles (16 bit)
  ld    l,(iy+1)                ;x object in tiles (16 bit)

  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  add   hl,hl                   ;*8
  ld    de,7
  add   hl,de                   ;x correction; always put sprite 7 pixels to the right
  ld    (ix+xobject),l          ;x value of sprite (16 bits)
  ld    (ix+xobject+1),h        ;x value of sprite (16 bits)
  ;/set x object

  ;set y object
  ld    l,(iy+3)
  ld    h,(iy+4)
  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  add   hl,hl                   ;*8
  dec   hl                      ;y correction; always put sprite 1 pixels up

  ld    (ix+yobject),l          ;y value of sprite (16 bits)
  ld    (ix+yobject+1),h        ;y value of sprite (16 bits)
  ;/set y object

  ;set current level object list address
  push  iy
  pop   hl
;  ld    a,l
  ld    (ix+levobjlistadd),l    ;current level object list address (16 bits)  
;  ld    a,h
  ld    (ix+levobjlistadd+1),h  ;current level object list address (16 bits)  
  ;/set current level object list address


  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,infolistblock
  call  block1234 ;we may need to expand the infolist block!!

  
;  ;fetch and write object info from objectnumberinfolist
;  ld    b,(iy+4)                ;object number
;  ld    hl,objectnumberinfo-lenghtobjectnumberinfoList
;  ld    de,lenghtobjectnumberinfoList
;  
;  .fetchloop:
;  add   hl,de ;11
;  djnz  .fetchloop ;13
;  24T

  ;more efficient method of fetching by using a precalculated adress table
  ld l,(iy+5) ;0
  ld h,(iy+6) ;7
;  ld h,0 ;temp keep this one 8 bits for legacy
  dec hl ;4
  add hl,hl ;11
  ex de,hl ;4

  ld hl,infolistprefetchtable  ;0
  add hl,de ;11
  ld e,(hl) ;7
  inc hl ;6
  ld d,(hl) ;7
  ld hl,objectnumberinfo-lenghtobjectnumberinfoList ;0
  add hl,de ;11
  ;68T
  ;Faster if object number is greater than 4 

  ld    b,(hl)                  ;amount of sprites
  ld    (ix+amountspr),b        ;amount of sprites

  inc   hl                      ;movement pattern
  ld    a,(hl)
  ld    (ix+movepat),a          ;write movement pattern (16bit)
  inc   hl           
  ld    a,(hl)
  ld    (ix+movepat+1),a
 
  ld (ix+deathtimer),0 ;reset such values
  
  inc   hl                   ;v1

  ex    de,hl                   ;v1 in de
  push  ix
  pop   hl
  ld    bc,v1
  add   hl,bc                   ;jump to v1
  ex    de,hl
;  ld    bc,65;40 ;as many as the list is wide
;  ldir                          ;write v1,v2,v3,v4,offsets,spritecolROM,spritecharROM en nu ook sizeX en sizeY

  call ldix67 ;20% faster :)
  ;/fetch and write object info from objectnumberinfolist

;call to romprogblock since ths routine is usually called from there
  call setpage12tohandleobjectmovement

;  jp .endcheckforce ;DEBUG

  ld    a,(ix+amountspr)                     ;check if there are no sprites used for this object
  or    a                       ;if so, then dont set spritelistaddress
  jp    z,.objectwithoutsprites  


  ;force spritenumber ?  
  bit   6,(iy)                  ;force sprite number ?
.forcesprite:  
  jr    z,.endcheckforce        ;if zeroflag then dont force

  ld    a,(iy)                  ;which spritenumber ?
  and   %0011'1111              ;reset bit 7 and 6
  
  ld    hl,sprlst
  ld    d,0
  ld    e,a
  add   hl,de
  ld    b,(ix+amountspr)        ;amount of sprites

  ld a,(ix+movepat+1)
  and a
  jp nz,.skipchecks
  ld a,(ix+movepat)
;  cp 30
;  jp   z,.spotsfound ;bullet
;;  cp 14
;;  jp   z,.spotsfound ;questionmarkblock must always be in the correct position and always spawn
;  cp 43
;  jp   z,.spotsfound ;when mario exits the tube force the whole tubesprite to appear in place
;  cp 44
;  jp   z,.spotsfound ;tubecover pulling mario in
  cp 61
  jp   z,.spotsfound ;shell goes along into tube
;  cp 64
;  jp   z,.spotsfound ;tube when comming in
;  cp 65
;  jp   z,.spotsfound ;another exit tube
;  cp 66
;  jp   z,.spotsfound ;blockwall MUST function at all times

.skipchecks:

  push hl
  push bc
  call  .searchfreespots
  pop   bc
  pop   hl
  jp    z,.spotsfound

;see if we can push a sprite on the overlap
  push  hl
  push  bc                      ;amount of sprites are stored in b
  call  .searchfreeoverlapspots
  pop   bc
  pop   hl

  jp    nz,.spotsfound



.endcheckforce:
  ;/force spritenumber ?
 

    
  ;set spritelistaddress
  ld    hl,sprlst               ;spritelist: 32 bytes representing each sprite, 00=sprite is free to use, 01=sprite is in use
;
  ld    b,(ix+amountspr)        ;amount of sprites
;  
;  ld    a,b                     ;check if there are no sprites used for this object
;  or    a                       ;if so, then dont set spritelistaddress
;  jp    z,.objectwithoutsprites  
  
.loop:
  push  hl
  push  bc                      ;amount of sprites are stored in b
  call  .searchfreespots
  pop   bc
  pop   hl

  jp    z,.spotsfound
  
  inc   hl

                                ;overflow check
  ld    de,endsprlst            ;check if end of sprite list is reached
  or    a                       ;reset carry first for sbc hl,de
 ; push  hl    
  sbc   hl,de
  add hl,de
 ; pop   hl
  
  jp    c,.loop


 ; ret ;uncomment this to deactivate
;enter second loop if no spots are found in the first run

  ld    hl,sprlst               ;spritelist: 32 bytes representing each sprite, 00=sprite is free to use, 01=sprite is in use
;
  ld    b,(ix+amountspr)        ;amount of sprites
;  
;  ld    a,b                     ;check if there are no sprites used for this object
;  or    a                       ;if so, then dont set spritelistaddress
;  jp    z,.objectwithoutsprites  
  
.loop2:
  push  hl
  push  bc                      ;amount of sprites are stored in b
  call  .searchfreeoverlapspots
  pop   bc
  pop   hl

  jp    nz,.spotsfound
  
  inc   hl

                                ;overflow check
  ld    de,endsprlst            ;check if end of sprite list is reached
  or    a                       ;reset carry first for sbc hl,de
 ; push  hl    
  sbc   hl,de
  add hl,de
 ; pop   hl
  
  jp    c,.loop2



  ret                           ;end of sprite list is reached


.searchfreespots:
  ld    a,(hl)
  or    a
  ret nz
  inc   hl
  djnz  .searchfreespots
  ret


.searchfreeoverlapspots:
  ld    a,(hl)
  cp 2
  ret z
  cp 10
  ret z
  cp -1
  ret z
;old state before sprite flicker implementation
;  or    a
;  ret nz
  inc   hl
  djnz  .searchfreeoverlapspots
  ret



.seteven:

  ld (ix+updateoddoreven?),1
  ld (ix+clearvram),0 ;reset this byte to clear the vram of the garbage sprites
  call clearvramonothersprite

  ret
  
.spotsfound:
  ld    (ix+sprlstadd),l
  ld    (ix+sprlstadd+1),h      ;hl->address of sprite in spritelist
  ;/set spritelistaddress
  
  push  hl
.setspriteinuseinspritelist:
;add the value not set it
  ld a,(hl)
  inc a
  ld (hl),a
  cp 2
  call z,.seteven

;  ld    (hl),1
  inc   hl
  djnz  .setspriteinuseinspritelist  
  pop   hl

  ld    (ix+active?),1          ;object becomes active
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ;object no longer available in levelObjectlist
  res   7,(iy+0)                ;sprite is put in screen, so no longer available in levelObjectList
  ;/object no longer available in levelObjectlist
  
  ;/set coloraddress and spataddress
  ld    de,sprlst
  or    a                       ;reset carry first for sbc hl,de
  sbc   hl,de                   ;out: hl=spritenumber which we can use for coloraddress and spataddress
  
  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  push  hl
  add   hl,hl                   ;*8
  add   hl,hl                   ;*16
  ld    (ix+coloraddress),l
  ld    (ix+coloraddress+1),h   ;color address = sprite number * 16
  
  pop   hl
  ld    de,spat
  add   hl,de
  ld    (ix+spataddress),l
  ld    (ix+spataddress+1),h    ;spataddress = sprite number * 4 + spat
  ;/set coloraddress and spataddress
  
  
  ld a,(randomnumber)
  xor 2
  ld (randomnumber),a
  ld (ix+random),a
  ret

.objectwithoutsprites:
  
  
  ld    (ix+active?),1          ;object becomes active
  ld    (ix+updatespr?),0       ;dont update sprite

  ;object no longer available in levelObjectlist
  res   7,(iy+0)                ;sprite is put in screen, so no longer available in levelObjectList
  ;/object no longer available in levelObjectlist

;call to romprogblock since this routine is usually called from there
 ; call setpage12tohandleobjectmovement

  ld a,(randomnumber)
  xor 2
  ld (randomnumber),a
  ld (ix+random),a

  ret

;24 bits add ahl,bde
add_ahl_bde:

  add hl,de
  adc a,b

  ret


;24 bits sub ahl,bde
sub_ahl_bde:

  and a
  sbc hl,de
  sbc a,b

  ret


;taken from z80 heaven http://z80-heaven.wikidot.com/math#toc18
HL_Div_C:
;Inputs:
;     HL is the numerator
;     C is the denominator
;Outputs:
;     A is the remainder
;     B is 0
;     C is not changed
;     DE is not changed
;     HL is the quotient
;
       ld b,16
       xor a
         add hl,hl
         rla
         cp c
         jr c,$+4
           inc l
           sub c
         djnz $-7
       ret


;exactly like any tile but writes it in presetted bulks. in A= tilenumber B= amount of tiles to write c= autoincrement tilewrite 0 = yes 1 = no
writemanytile:

  push af
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a
  pop af
  
  dec c
  jp z,.otherline
  
.line:
  ld (hl),a
  inc a
  inc hl
  djnz .line

  jp setpage12tohandleobjectmovement


.otherline:

.line1:
  ld (hl),a
  inc hl
  djnz .line1

  jp setpage12tohandleobjectmovement


return:

  inc sp
  inc sp

  ret


;spawn some 291's for piranjaplant
spawnbullets:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,280
  ld    (iy+5),l               ;set object number (56 = coin)
  ld    (iy+6),h               ;set object number (56 = coin)

  call activateobject

  ld (ix+deadly),2
  ld hl,291
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v1),-1
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex42  ;color address
  ld    bc,spriteaddrex42c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  call  setcharcolandoffset

  pop ix

  pop de
  pop hl

  inc hl
  inc hl
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,280
  ld    (iy+5),l               ;set object number (56 = coin)
  ld    (iy+6),h               ;set object number (56 = coin)

  call activateobject

  ld (ix+deadly),2
  ld hl,291
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v1),1  
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex42  ;color address
  ld    bc,spriteaddrex42c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  call  setcharcolandoffset

  pop ix

  
  ret

;crush blocks on request from ram input is MAPadress in HL
crushblockfromram:

  ;get mapdata

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  push ix
  call crushblock
  pop ix

  ;restore rom and return
  jp setpage12tohandleobjectmovement

  ;ret

;this is specially written for the yellow pillars in th cave maps. To initialize them alot of code is needed which needs to be executed from somewhere. This is the hacky way
callsetblocktilesfromrom:

;store memblocks
    ld a,(memblocks.n3)
    ld l,a
    ld a,(memblocks.n4)
    ld h,a
    push hl




    ld a,yellowpillarinitblock
    call block34
    
    
    call inityellowpillar

    pop hl

   ;reset blocks
        ld a,l
        ld                ($9000),a
        ld                a,h
        ld                ($b000),a


    ret 

    
writenewyellowpillar:


    ld a,romprogblock5
    call block34
    
    jp writenewyellowpillarrom ;this one returns @ the call inityellowpillar in RAM.
    

    
objectinteractspecial:

  ld (currentobjectvalue),ix
  push ix
  pop iy

;alle XY waarden die het huidige object bezitten saven voordat we de loop in gaan
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld (currentx),hl
  ld (currenty),de

;alle XY waarden die het huidige object bezitten saven voordat we de loop in gaan
    push ix

;zodra we een object vinden handelen we hem af en returnen we met een carry flag
    xor a
  ld    ix,Objectlist+(lenghtobjectlist*00) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobjectspecial : jp c,.end ;objectnr 20

;no object found return this
  xor a
  pop ix

  ret


.end:

    ld a,(ix+movepat+1)
    and a
    jp nz,.skipcheck
    
    ld a,(ix+movepat)
    cp 39
    jp z,.return
    cp 40
    jp z,.return
    cp 70
    jp z,.return    
    
.skipcheck:    

  push ix
  pop iy ;save the ix value for interaction reference by the routine that called this
;adress terugschrijven
  pop ix

  scf
  
  ret    

.return:

  push ix
  pop iy ;save the ix value for interaction reference by the routine that called this
;adress terugschrijven
  pop ix
  
  xor a
  
  ret  
  
 
handleobjectspecial:

;ben ik het zelf
  ld hl,(currentobjectvalue)
;  ld (ixtransfer),ix
;  ld de,(ixtransfer)
  push ix
  pop de

  xor a
  sbc hl,de
  jp z,.endcheck

  ;check if other object is within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-16;-20
  add hl,de
  
  ld de,(currentx)
  or  a  
  sbc hl,de
  
;left border not found, no collision
  jp nc,.endcheck

  ld  a,(ix+sizeX)
  add a,15;22
  ld  d,0
  ld  e,a
;  or  a  
  add hl,de

;right border not found, no collision
   jp nc,.endcheck
  ;/check if other object is within x range of object

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-22;-28
  add hl,de
 
  ld de,(currenty)
  or  a  
  sbc hl,de
  
;top border not found, no collision
  jp nc,.endcheck

  ld  a,(ix+sizeY)
  add a,32;;15
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;bottom border not found, no collision
   jp nc,.endcheck
  ;/check if other object is  within y range of object
  
.endcheck:

  ret 
 

 
killallenemies:

  push ix

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 20

  pop ix

  ret

.handleobject:

;cannot be destroyed on level exit
  ld a,(ix+clipping)
  cp 6
  ret z
  cp 0
  ret z
 
;not itself and not bullets!
  ld a,(ix+movepat+1)
  and a
  jp nz,destroyobjectwithcoin
  ld a,(ix+movepat)
  cp 68
  ret z
  cp 69
  ret z ;DEBUG and TEMP this should be the item that gets spawned after the kill
  cp 30
  ret z
  cp 103
  ret z
  cp 104
  ret z
  cp 110
  ret z
  cp 45
  ret z ;quick hack not sure why this is happening 
  cp 189
  jp z,destroyobjectwithcloudnostar 
 
 
  jp destroyobjectwithcoin
 
globalvarsonrom: equ globalvars - engine32start + $8000
 
 ;clear all globalvars including those of the worldmap. Offcourse there are some predefined bytes so we need to copy them again 
clearglobalvars:

    ld a,engine32block
    call block34

  ld hl,globalvarsonrom
  ld de,globalvars
  ld bc,endglobalvars
  ldir 
  
  ret 
 
 
spawncape:

   push af

  ld hl,(mariox)
  
  srl h
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  
  ld bc,(marioy)
  
  srl b
  rr c   ;/2
  srl b
  rr c ;/4
  srl b
  rr c ;/8     
  
  inc bc
  inc bc

    ld de,369
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back  
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),e                ;set object number
  ld    (iy+6),d                ;set object number

;we are on page 1 block34 set this value before spawning the object!
  
  ld a,romprogblock
  ld (romprogblocknumber),a

  call  activateobject 
 
    pop af
 
    ret
 
SetSfxnrAndPriority:    ;Set sfx, in: d=priority, e=Sfx number
  ld    a,e
  ld    (Sfxnr),a
  ld    a,d
  ld    (SfxPriority),a
  ld    a,1
  ld    (SetSfx?),a
  
  ;if darky is present do some stereo imaging and use the second psg if the cpu allows it
  ld a,(darky?)
  and a
  ret z

  ld a,(turbor?)
  and a
  jp z,.skipsfx2
  
  ld a,(Sfxon?)
  and a
  jp z,.skipsfx2
  
  ld    a,e
  ld    (Sfxnr2),a
  ld    a,d
  ld    (SfxPriority2),a
  ld    a,1
  ld    (SetSfx2?),a    
  xor a
  ld    (SetSfx?),a
.skipsfx2:
  
  ld a,(mariocoordinates+1)
  cp 150 ;maximale rechts sprite coordinates
  jp nc,.setsoundright

 
  ld a,(mariocoordinates+1)
  cp 97 ;maximale links sprite
  jp c,.setsoundleft
  
  push hl
  ld hl,$0B3D ;left
  call senddarkycommand
  ld hl,$1B3D ;right
  call senddarkycommand  
  pop hl
  
  ret 

.setsoundleft:  

  push hl
  ld hl,$0B3F ;left
  call senddarkycommand
  ld hl,$1B37 ;right
  call senddarkycommand 
  pop hl

    ret

.setsoundright:

  push hl
  ld hl,$0B37 ;left
  call senddarkycommand
  ld hl,$1B3F ;right
  call senddarkycommand 
  pop hl

    ret
 
  
clearallvram:

    ex af,af'
    cp 69
    jp nz,.return
    ex af,af'
    

    di
;fill vram with darkness
;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X
  out ($9b),a
  ;ld a,2 ;page 2
  out ($9b),a ;Y
  
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;X
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;Y 
  
  ld a,255 ;fetch first color byte
  out ($9b),a ;first byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!


  ld de,256/2*256
  dec de ;-1   
    
    
.upload2:
    ld a,255
  out ($99),a
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload2       
    
  ;/fill vram with darkness 
  ei
  
  ret
  
.return:

    ex af,af'
    ret


    
spawnbeta:



  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc e

  
  ld bc,370

;create the shell

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;we force the shell into the marioposition until mario is fully on the map 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),c               ;set object number (pinkshell)
  ld    (iy+6),b               ;set object number (pinkshell)

  call  activateobject


  pop ix

  ret


 
;c = 32? then switch to Z80 otherwise switch to R800 
switchcpucode:	

	di
	PUSH	HL
	PUSH	DE
	PUSH	BC
	PUSH	AF
	PUSH	IX
	PUSH	IY
	EXX
	EX	AF,AF'
	PUSH	HL
	PUSH	DE
	PUSH	BC
	PUSH	AF			; save all Z80 registers
	LD	A,I
	PUSH	AF			; save interrupttable pointer
	LD	(cpusp),SP		; save stackpointer

	ex af,af'
;now switch!	
	LD	A,6
	OUT	($E4),A		; S1990 register 6
	exx
    ld a,c ;which cpu?
    exx
    or %01000000
    out ($E5),a
    
	
	
	di
	nop
	
	
	LD	SP,(cpusp)		; restore stackpointer
	POP	AF
	LD	I,A			; restore interrupttable pointer
	POP	AF
	POP	BC
	POP	DE
	POP	HL
	EXX
	EX	AF,AF'
	POP	IY
	POP	IX
	POP	AF
	POP	BC
	POP	DE
	POP	HL			; restore all Z80 registers
	ei
	ret 
 
 
 

;ret 
    include "flashcommands.h"
    include "darkycmd.h"

copyrow:
  db    000,000,000,000   ;sx,--,sy,spage
  db    000,000,000,001   ;dx,--,dy,dpage
  db    000,001,016,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

worldmaplives:
  db    179,000,042,002   ;sx,--,sy,spage
  db    064,000,027,002   ;dx,--,dy,dpage
  db    008,000,007,000   ;nx,--,ny,--
  db    000,000,$90       ;fast copy

worldmaplocationtext:
  db    000,000,035,002   ;sx,--,sy,spage
  db    089,000,027,002   ;dx,--,dy,dpage
  db    008,000,007,000   ;nx,--,ny,--
  db    000,000,$90       ;slow copy

offsetblargh:         dw 004,000, 004,000, 004,016, 004,016, 020,000, 020,000, 020,016, 020,016, 020,032, 020,032
offsetghostblob:      dw 000,000, 000,000, 000,016, 000,016, 000,032, 000,032,  016,000, 016,000, 016,016, 016,016, 016,032, 016,032
offsetghostblob2:     dw 032,000, 032,000, 032,016, 032,016, 032,032, 032,032
offsetlakitupipe:     dw 0-2,000, 0-2,000, 014,000, 014,000
offsetone:            dw -02,000, -02,000
offsetporcupuffer:    dw 000,000, 000,000, 000,000, 000,016, 000,016, 000,016,  016,000, 016,000, 016,016, 016,016



include "globalvars.asm"

endglobalvars: equ $ - globalvars ;size of the list

fullend: equ ($ - $d880) + $c000 ;end of enginepage32 marker

