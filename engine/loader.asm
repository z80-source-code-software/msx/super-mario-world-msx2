loader:


    xor a
    ld (Sfxon?),a
    ld (Sfxon2?),a


if (debugenable = 0)
    ;make one time intro loop
    ld a,(firstrun)
    and a
    call z,intromap
endif

    ld a,(musicactiveoption)
    ld (musicactive),a ;set music after detection
 
 
    call setworldmapstart 

  
gameloop:

  call restoreeventhandlerx ;always restore the xy stuff because someone could quit a Y map with the quit stuff

  ld a,(nocontrol)
  cp 2
  call z,takelife

;make sure to reset the time inbetween the maps
  xor a
  ld (time),a

  ld a,(musicactiveoption)
  ld (musicactive),a ;see which option was active and act on that.  
  
if (debugenable = 1)


 ld a,3
 ld (mariostate),a
 ;ld a,$ff
 ;ld (yellowswitchactive),a
; ld a,1 
; ld (marioyoshi),a

;ld a,1
;ld (sfxactive),a
;  ld a,6
;  ld (cutscenenr),a
;  call jumptocutscene
;ld a,1
;ld (mariostate),a
;ld a,1
;ld (marioyoshi),a
;ld a,4
;ld (yoshitype),a
;put debug jumps here
;jp testmap
  ;  jp level00
 ;jp level01
; jp level01.loopmap001
 ;jp level02
; jp level02.loopmap003
 ;jp level03
;jp level01.loopmap001
; jp level05
 ;jp level04
 ;jp level04.loopmap009
; jp level05.loopmap010
 ;jp level05.loopmap011
;jp level04.loopmap008
; jp level00
  ;jp level06;.loopmap014
 ;jp level07.loopmap017
 ;jp level07;.loopmap016 ;donut1
; jp level08;.loopmap020 ;donut2
; jp level09 ;ghosthouse
;jp demolevel ;still need to record a demo file for this one
;/put debug jumps here
 ; ld a,2
 ; ld (marioshelltype),a ;save the shelltype after exit
 ;jp level11 ;pipes and stuff
; jp level10; .loopmap017 ;donut3
 ;jp level12 ;donut secret 1
 ;jp topsecret
;jp level11.loopmap040
 ;jp level13 ;icecave
 jp donutghostsecret.loopmap031
; jp donutcastle
; jp donutcastle.loopmap036
 ;jp greenswitchpalace
;jp currentmap
;jp vanilla05
 ; jp vanilla01.loopmap002
; jp vanilla02;.loopmap002
 ; jp vanilla03;.loopmap002 ;blargh land
;jp vanilla05;.loopmap002 ;ghosthouse
;jp vanilla05;.loopmap002 ;ghosthouse
 ; jp vanilla04
    ;jp vanillas1
    ;jp vanillas2
   ; jp vanillas3
;jp trowcutscene1

   ; jp vanillaf
 ; jp vanillacastle.loopmap002
 ;jp butter1
 
; jp butter2loopmap005
;jp testmap
;jp cheese3
; jp cookie4
   ; jp soda5
 ;jp ludwig6.loopmap003
; jp illusion5
; jp illusion4 ;finished
 ; jp illusion3
  ;jp illusion2
; jp illusion1
;jp illusion6
 ;jp illusion8
  ;  jp illusion7
;jp testpoint
;switch palace test

;jp level06 ;yellow switch palace
;jp greenswitchpalace
 ;jp redswpalace
; jp blueswpalace


 ;   jp bowserva1.loopmap002

;    jp chocolate8

 ;   jp bowserva8
  ;  jp bowserva7.loopmap002
 ;   jp bowserva6
  ;  jp bowserva5
 ;   jp bowserva4
  ;  jp bowserva3.loopmap003
   ; jp bowserva2;.loopmap002
;  jp chocolate9;.loopmap003
;  jp chocolate8.loopmap003
 ; jp chocolate7
 ; jp chocolate6 ;ghost house
 ; jp chocolate5
 ; jp chocolate4
 ; jp chocolate3
 ; jp chocolate2
 ; jp chocolate1

 ; jp starworld5
 ; jp starworld4
 ; jp starworld3
 ; jp starworld2
 ; jp starworld1
;  jp intromap

   ; jp special3
  ;  jp special2
    jp special1

  ;  jp testmap
;jp topsecret
;jp donutcastle

 ;   jp bowserva2
    ;jp bowserva8;.loopmap002
 ;   jp bowserva4
 ;   jp bowserva3

  ;  jp bowsercas1.loopmap012
    jp bowsercas1.finalscene


endif

  ld c,0 ;Z80
  call switchcpu
  
  call  loadworldmap ;out in a = 000=YosHou,001=YoIsl1,002=YoIsl2,003=YoIsl3,004=YoIsl4,005=IggyCa,006=YeSwPa,007=DoPla1

  
  
  
;000=LocationYosHou,001=LocationYoIsl1,002=LocationYoIsl2,003=LocationYelSwP,004=LocationYoIsl3,005=LocationYoIsl4
;006=LocationIggyCa,007=LocationDoPla1,008=LocationDoPla2,009=LocationDoSec1,010=LocationGrSwPa,011=DoPla2toDoGhos
;012=LocationDoSeGh,013=LocationDoSec2,014=LocationTopSec,015=DoPla4toDoPla3,016=LocationDoPla4,017=LocationMortCa
;018=LocationVaDom1,019=LocationVaDom2,020=LocationVaSec1,021=LocationReSwPa,022=LocationVaGhos,023=LocationVaDom3
;024=LocationVaDom4,025=LocationLemmCa,026=LocationVaSec2,027=LocationVaSec3,028=LocationVaFort,029=LocationButBr1
;030=LocationButBr2,031=Ludwigcastlelo,032=LocationCheese,033=LocationSodaLa,034=LocationCookie,035=Forestofillus1
;036=Forestghosthou,037=Forestofillus4,038=Forestofillus2,039=LocationBlSwPa,040=Forestofillus3,041=Forestsecretar
;042=Forestfortress,043=Royscastleloca,044=Chocolateisla1,045=Chocoghosthous,046=Chocolateisla2,047=Chocolatesecr1
;048=Chocolateisla3,049=Chocolfortress,050=Chocolateisla4,051=Chocolateisla5,052=Wendyscastlelo,053=LocationSunkGh
;054=LocationValBo1,055=LocationValBo2,056=LocationValFor,057=LocationBackDo,058=LocationValGho,059=LocationLarrCa
;060=LocationValBo3,061=LocationValBo4,062=LocationFrontD,063=LocationStarW1,064=LocationStarW2,065=LocationStarW3
;066=LocationStarW4,067=LocationStarW5,068=LocationSpeci1,069=LocationSpeci2,070=LocationSpeci3



  ld c,1 ;R800
  call switchcpu
  
  call  clearyoshicointable ;clear all yoshi coin information
  call removeallscorebarspritesfromvram                 ;remove all spritedata from the scorebar to prevent artifacts

  ex af,af' ;get value back stored by the worldmap
  
  ld l,a
  ld h,0
  ld e,l
  ld d,h ;-----> copy hl to de
  add hl,hl ;*2
  add hl,de ;*3
  ex de,hl
  ld hl,gamestart
  add hl,de
  jp (hl)
  
;basicly gamestart is the entire map structure. It will have its own sublabels in case of complicated maps
gamestart: 

;000=LocationYosHou
  jp level00 ;yoshishouse
;001=LocationYoIsl1,
  jp level01 ;yoshisisland 1
;002=LocationYoIsl2,
  jp level02 ;yoshisisland 2
;003=LocationYelSwP,
  jp level06 ;yellowswitchpalace
;004=LocationYoIsl3,
  jp level03 ;yoshisisland 3
;005=LocationYoIsl4
  jp level04 ;yoshisisland 4
;006=LocationIggyCa,
  jp level05 ;iggys castle
;007=LocationDoPla1,
  jp level07 ;donutplains 1
;008=LocationDoPla2,
  jp level08 ;donutplains 2
;009=LocationDoSec1,
  jp level12 ;donut secret 1
;010=LocationGrSwPa,
  jp greenswitchpalace ;greenswitchpalace
;011=DoPla2toDoGhos
  jp level09 ;donut plains ghosthouse
;012=LocationDoSeGh,
  jp donutghostsecret ;donut secret ghosthouse
;013=LocationDoSec2,
  jp level13 ;donut secret 2
;014=LocationTopSec,
  jp topsecret
;015=DoPla4toDoPla3,
  jp level10 ;donut plains 3
;016=LocationDoPla4,
  jp level11 ;donut plains 4
;017=LocationMortCa
  jp donutcastle ;morton's castle
;018=LocationVaDom1
  jp vanilla01 ;vanilla dome 1
;019=LocationVaDom2
  jp vanilla02
;020=LocationVaSec1,
  jp vanillas1
;021=LocationReSwPa,
  jp redswpalace
;022=LocationVaGhos,
  jp vanilla05
;023=LocationVaDom3
  jp vanilla03
;024=LocationVaDom4
  jp vanilla04
;025=LocationLemmCa
  jp vanillacastle
;026=LocationVaSec2
  jp vanillas2
;027=LocationVaSec3
  jp vanillas3
;028=LocationVaFort
  jp vanillaf
;029=LocationButBr1
  jp butter1
;030=LocationButBr2
  jp butter2
;031=ButBr2toLudwCa
  jp ludwig6
;032=LocationCheese
  jp cheese3
;033=LocationSodaLa
  jp soda5
;034=LocationCookie
  jp cookie4
;035=Forestofillus1
  jp illusion1
;036=Forestghosthou
  jp illusion5
;037=Forestofillus4
  jp illusion4
;038=Forestofillus2
  jp illusion2
;039=LocationBlSwPa
  jp blueswpalace
;040=Forestofillus3
  jp illusion3
;041=Forestsecretar
  jp illusion6
;042=Forestfortress
  jp illusion7
;043=Royscastleloca
  jp illusion8
;044=Chocolateisla1
  jp chocolate1
;045=Chocoghosthous
  jp chocolate6
;046=Chocolateisla2
  jp chocolate2
;047=Chocolatesecr1
  jp chocolate7
;048=Chocolateisla3
  jp chocolate3
;049=Chocolfortress
  jp chocolate8
;050=Chocolateisla4
  jp chocolate4
;051=Chocolateisla5
  jp chocolate5
;052=Wendyscastlelo
  jp chocolate9
;053=LocationSunkGh
  jp bowserva1
;054=LocationValBo1
  jp bowserva2
;055=LocationValBo2
  jp bowserva3
;056=LocationValFor
  jp bowserva7
;057=LocationBackDo
  jp bowsercas1.loopmap005special
;058=LocationValGho
  jp bowserva6
;059=LocationLarrCa
  jp bowserva8
;060=LocationValBo3
  jp bowserva4
;061=LocationValBo4
  jp bowserva5
;062=LocationFrontD
  jp bowsercas1
;063=LocationStarW1
  jp starworld1
;064=LocationStarW2
  jp starworld2
;065=LocationStarW3
  jp starworld3
;066=LocationStarW4
  jp starworld4
;067=LocationStarW5
  jp starworld5
;068=LocationSpeci1
  jp special1
;069=LocationSpeci2
  jp special2
;070=LocationSpeci3
  jp special3


  
level00:

  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level15startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv015 : ld bc,lenghtobjectlistlv015 : ld a,00 : call	levelloader
  ld a,(nocontrol)
  
 ; ld a,5
 ; ld (makelevelavailable?),a
  
  jp gameloop



level01: ;yoshi 1
  ld a,300/2
  ld (time),a

  xor a
  ld (nocontrol),a
  ;in         musicblock                      startingcoordinates                   animationtilesadress        objectslist               lenghtobjectslist       level to load   
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level00startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv000 : ld bc,lenghtobjectlistlv000 : ld a,15 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap001
  cp 2
  jp z,gameloop
;  cp 3

  ld a,1
  ld (makelevelavailable?),a
  jp gameloop
  
.loopmap001:
  xor a
  ld (tubeupordown),a
  ld a,1
  ld (nocontrol),a 
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level01startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv001 : ld bc,lenghtobjectlistlv001 : ld a,01 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
;  cp 3
;  jp z,.loopmap002
  ld a,1
  ld (tubelaunch),a ;do a tubelaunch
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level00startingcoordinates2 : ld iy,level00animationadress : ld hl,objectlistlv000 : ld bc,lenghtobjectlistlv000 : ld a,15 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap001
  cp 2
  jp z,gameloop
  cp 3
  ld a,1
  ld (makelevelavailable?),a  
  jp z,gameloop



level02: ;yoshi island 2
  ld a,400/2
  ld (time),a



.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level02startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv002 : ld bc,lenghtobjectlistlv002 : ld a,02 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap003
  cp 2
  jp z,gameloop
  cp 3
  jp nz,gameloop
  ld a,2
  ld (makelevelavailable?),a
  jp z,gameloop
.loopmap003:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,level03bkgtiles : ld ix,level01startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv003 : ld bc,lenghtobjectlistlv003 : ld a,03 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 3
  jp z,gameloop
.loopmap0022:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level02startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv002 : ld bc,lenghtobjectlistlv002 : ld a,02 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap003
  cp 3
  jp nz,gameloop
  ld a,2
  ld (makelevelavailable?),a
  jp z,gameloop
 
 

level03: ;yoshi island 3
  ld a,300/2
  ld (time),a



.loopmap004:
  xor a
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level04startingcoordinates  : ld iy,level04animationadress : ld hl,objectlistlv004 : ld bc,lenghtobjectlistlv004 : ld a,04 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap005
  cp 3
  jp nz,gameloop
  ld a,3
  ld (makelevelavailable?),a
  jp z,gameloop
.loopmap005:

  xor a
  ld (tubeupordown),a  
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level05startingcoordinates  : ld iy,level05animationadress : ld hl,objectlistlv005 : ld bc,lenghtobjectlistlv005 : ld a,05 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop 
.loopmap0042:
  ld a,1
  ld (tubeupordown),a
  ld (nocontrol),a  
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level04startingcoordinates2  : ld iy,level04animationadress : ld hl,objectlistlv004 : ld bc,lenghtobjectlistlv004 : ld a,04 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 3
  jp nz,gameloop
  ld a,3
  ld (makelevelavailable?),a
  jp z,gameloop




level04: ;yoshi island 4
  ld a,300/2
  ld (time),a


.loopmap006:
  xor a
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level06startingcoordinates  : ld iy,level06animationadress : ld hl,objectlistlv006 : ld bc,lenghtobjectlistlv006 : ld a,06 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap009
  cp 4
  jp z,.loopmap008 ;we exit a right tube
  cp 3
  ld a,4 ;this can only happen if you cheat
  ld (makelevelavailable?),a  
  jp z,gameloop
.loopmap009:
  ld a,1
  ld (tubeupordown),a  
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,level08bkgtiles : ld ix,level08startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv008 : ld bc,lenghtobjectlistlv008 : ld a,08 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap007
.loopmap007:
  ld a,1
  ld (tubeupordown),a
  ld (nocontrol),a  
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level06startingcoordinates2  : ld iy,level06animationadress : ld hl,objectlistlv006 : ld bc,lenghtobjectlistlv006 : ld a,06 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap007
.loopmap008:
  ld a,4
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level06startingcoordinates3  : ld iy,level00animationadress : ld hl,objectlistlv007 : ld bc,lenghtobjectlistlv007 : ld a,07 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 3
  jp nz,gameloop
  ld a,4
  ld (makelevelavailable?),a
  jp z,gameloop

level05: ;iggys castle
  ld a,300/2
  ld (time),a
 
.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader 
 
 
  ld a,$53 : ld (currentmusicblock),a : ld de,0 : ld ix,level09startingcoordinates  : ld iy,level09animationadress : ld hl,objectlistlv009 : ld bc,lenghtobjectlistlv009 : ld a,09 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  ;cp 1
  ;jp z,.loopmap009
  ;cp 4
  ;jp z,.loopmap008 ;we exit a right tube
;  cp 3
;  jp z,trowcutscene1 ;what!!?? this has to be a bug. disabled for now we will see what happens
  cp 5
  jp z,.loopmap010
.loopmap010:
  xor a
  ld (nocontrol),a
  ld a,$53 : ld (currentmusicblock),a : ld de,level10bkgtiles : ld ix,level10startingcoordinates  : ld iy,level10animationadress : ld hl,objectlistlv010 : ld bc,lenghtobjectlistlv010 : ld a,10 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,gameloop
  cp 5
  jp z,.loopmap011
.loopmap011:
  xor a
  ld (nocontrol),a
  ld a,$51 : ld (currentmusicblock),a : ld de,0 : ld ix,level11startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv011 : ld bc,lenghtobjectlistlv011 : ld a,11 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  cp 5
  jp z,.loopmap010


trowcutscene1:

  call clearlocalvariables ;very important or else other object will remain active in RAM!

  ld a,2
  ld (cutscenenr),a
  call jumptocutscene
  
  ld a,5
  ld (makelevelavailable?),a
  
  jp gameloop


;yellow switch palace
level06:
.loopmap013:
  xor a
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level13startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv013 : ld bc,lenghtobjectlistlv013 : ld a,13 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,gameloop
.loopmap014:
;  xor a
;  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level14startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv014 : ld bc,lenghtobjectlistlv014 : ld a,14 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 3
  ld a,20
  ld (makelevelavailable?),a  
  jp gameloop

 

;donut plains 1
level07: ;COMMENT: note that maplevel nr 12 and 13 are switched!
  ld a,400/2
  ld (time),a

.loopmap012:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,level12bkgtiles : ld ix,level12startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv012 : ld bc,lenghtobjectlistlv012 : ld a,12 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap017
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,6
  ld (makelevelavailable?),a  
  jp z,gameloop
  ld a,(nocontrol)
  cp 5
  ld a,7
  ld (makelevelavailable?),a  
  jp z,gameloop
  
;  jp z,.loopmap017
.loopmap0122:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,level12bkgtiles : ld ix,level12startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv012 : ld bc,lenghtobjectlistlv012 : ld a,12 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap017
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,6
  ld (makelevelavailable?),a  
  jp z,gameloop
  ld a,(nocontrol)  
  cp 5
  ld a,7
  ld (makelevelavailable?),a  
  jp z,gameloop
 
  
.loopmap016:
  ld a,1  
  ld (tubeupordown),a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level16startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv016 : ld bc,lenghtobjectlistlv016 : ld a,16 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap0122
  cp 2
  jp z,gameloop ;died
;  cp 3
 ; jp z,gameloop
.loopmap017:
;check which exit was taken
  ld a,(exitx)
  cp 23
  jp nc,.loopmap016
  

  ld a,4
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap0122
  cp 2
  jp z,gameloop ;died
 ; cp 3
 ; jp z,gameloop
  
  
 ;donut plains 2  
level08:
  ld a,400/2
  ld (time),a

.loopmap019:
  xor a
  ld (nocontrol),a
;  ld a,3
;  ld (mariostate),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level19startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv019 : ld bc,lenghtobjectlistlv019 : ld a,19 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap020
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  cp 4
  jp z,.loopmap021
.loopmap020: ;02b
  ld a,1  
  ld (tubeupordown),a
  ld (nocontrol),a
  ld a,$2f : ld (currentmusicblock),a : ld de,level20bkgtiles : ld ix,level20startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv020 : ld bc,lenghtobjectlistlv020 : ld a,20 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  cp 4
  jp z,.loopmap021
  cp 5
  ld a,8
  ld (makelevelavailable?),a
  jp z,gameloop  
    
.loopmap021: ;02c ;=the exit
  ld a,1  
  ld (tubeupordown),a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level21startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv021 : ld bc,lenghtobjectlistlv021 : ld a,21 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,9
  ld (makelevelavailable?),a   
  jp z,gameloop 
  
 
level09: ;donut plains ghosthouse
  ld a,300/2
  ld (time),a



.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader


.loopmap022:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level22startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv022 : ld bc,lenghtobjectlistlv022 : ld a,22 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 170
  jp c,.loopmap022b
  jp ghosthouseexit2


.loopmap022b2:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level22cstartingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv022b : ld bc,lenghtobjectlistlv022b : ld a,23 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 30
  jp c,ghosthouseexit

  jp .loopmap022c

  
.loopmap022b:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level22bstartingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv022b : ld bc,lenghtobjectlistlv022b : ld a,23 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 30
  jp c,ghosthouseexit

.loopmap022c:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level22cstartingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv022c : ld bc,lenghtobjectlistlv022c : ld a,24 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  ld a,(exitx)
  cp 30
  jp c,.loopmap022d


  jp .loopmap022b2 

.loopmap022d:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level22dstartingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv022d : ld bc,lenghtobjectlistlv022d : ld a,25 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  jp .loopmap022b



ghosthouseexit:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023 : ld bc,lenghtobjectlistlv023 : ld a,26 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
  ld a,15
  ld (makelevelavailable?),a 
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!  
  jp gameloop  

ghosthouseexit2:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023 : ld bc,lenghtobjectlistlv023 : ld a,26 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
  ld a,14
  ld (makelevelavailable?),a   
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!  
  jp gameloop 


;donut plains 3
level10:
  ld a,300/2
  ld (time),a



.loopmap024:
  xor a
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,level24bkgtiles : ld ix,level24startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv024 : ld bc,lenghtobjectlistlv024 : ld a,27 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap017
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,17
  ld (makelevelavailable?),a 
  jp z,gameloop


.loopmap0242:
  ld a,1
  ld (tubeupordown),a
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,level24bkgtiles : ld ix,level24startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv024 : ld bc,lenghtobjectlistlv024 : ld a,27 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap017  
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,17
  ld (makelevelavailable?),a   
  jp z,gameloop


;tic tac toe submap 03-03a
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap0242
  cp 2
  jp z,gameloop ;died
 ; cp 3
 ; jp z,gameloop

 
;donut plains 4
level11:
  ld a,300/2
  ld (time),a


.loopmap025:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level25startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv025 : ld bc,lenghtobjectlistlv025 : ld a,28 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap039
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,18
  ld (makelevelavailable?),a   
  jp z,gameloop

.loopmap0252:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level25startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv025 : ld bc,lenghtobjectlistlv025 : ld a,28 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap039  
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,18
  ld (makelevelavailable?),a    
  jp z,gameloop

.loopmap0253:
  ld a,1
  ;ld (nocontrol),a
  ld (tubelaunch),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level25startingcoordinates3  : ld iy,level00animationadress : ld hl,objectlistlv025 : ld bc,lenghtobjectlistlv025 : ld a,28 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap039  
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,18
  ld (makelevelavailable?),a    
  jp z,gameloop




.loopmap039:

  ld a,(exitx)
  cp 80
  jp nc,.loopmap040

  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level39startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv025b : ld bc,lenghtobjectlistlv025b : ld a,43 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap0252
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
.loopmap040:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a  
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level40startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv025c : ld bc,lenghtobjectlistlv025c : ld a,44 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  cp 4
  jp z,.loopmap0253

;donut secret 1
level12:
  ld a,300/2
  ld (time),a


.loopmap026:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level26startingcoordinates  : ld iy,level26animationadress : ld hl,objectlistlv026 : ld bc,lenghtobjectlistlv026 : ld a,30 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap027
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,.gameloop
  cp 5
  ld a,11
  ld (makelevelavailable?),a   
  jp z,gameloop  
  
  jp gameloop
  

.gameloop:


  ld hl,levelavailabilitylist+1 ;do it lamely by hand because this animation is not on the worldmap and does not have to be either :)
  ld a,(hl)
  set 4,a ;set path to prevent top secret area from fucking up
;  ld (hl),a
;  ld a,(hl)
  set 7,a ;set path to prevent top secret area from fucking up
  ld (hl),a

  ld a,10
  ld (makelevelavailable?),a   
  jp gameloop
  
  
  
.loopmap0262:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level26startingcoordinates2  : ld iy,level26animationadress : ld hl,objectlistlv026 : ld bc,lenghtobjectlistlv026 : ld a,30 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap027
  cp 2
  jp z,gameloop ;died
  cp 3
 ; ld a,10
 ; ld (makelevelavailable?),a   
  jp z,.gameloop
  ld a,(nocontrol)  
  cp 5
  ld a,11
  ld (makelevelavailable?),a   
  jp z,gameloop 
  
  
.loopmap027:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level27startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv026b : ld bc,lenghtobjectlistlv026b : ld a,31 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap0262
  cp 2
  jp z,gameloop ;died
 ; cp 3
;  jp z,gameloop
  jp gameloop

;donut secret 2
level13:
  ld a,300/2
  ld (time),a


.loopmap028:
  xor a
  ld (nocontrol),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level28startingcoordinates  : ld iy,level0207animationadress : ld hl,objectlistlv027 : ld bc,lenghtobjectlistlv027 : ld a,32 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap029
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,16
  ld (makelevelavailable?),a     
  jp z,gameloop

.loopmap0282:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level28startingcoordinates2  : ld iy,level0207animationadress : ld hl,objectlistlv027 : ld bc,lenghtobjectlistlv027 : ld a,32 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap029
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,16
  ld (makelevelavailable?),a    
  jp z,gameloop


.loopmap029:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level29startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv027b : ld bc,lenghtobjectlistlv027b : ld a,33 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap0282
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop



;donut secret ghosthouse

donutghostsecret:
  ld a,400/2
  ld (time),a


;  jp .loopmap033

.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader


.loopmap030:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,level28bkgtiles : ld ix,level30startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv028 : ld bc,lenghtobjectlistlv028 : ld a,34 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 3
;  jp z,gameloop
  jp .loopmap031
  
  
.loopmap0302:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,level28bkgtiles : ld ix,level30startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv028 : ld bc,lenghtobjectlistlv028 : ld a,34 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 3
;  jp z,gameloop
;  jp .loopmap031  
  
  
.loopmap031:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,level28bbkgtiles : ld ix,level31startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv028b : ld bc,lenghtobjectlistlv028b : ld a,35 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 90
  jp nc,.loopmap033
  cp 70
  jp nc,.ghosthouseexit
  cp 50
  jp nc,.loopmap032
  cp 0
  jp nc,.loopmap0302



.loopmap0312:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,level28bbkgtiles : ld ix,level31startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv028b : ld bc,lenghtobjectlistlv028b : ld a,35 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 90
  jp nc,.loopmap033
  cp 70
  jp nc,.ghosthouseexit
  cp 50
  jp nc,.loopmap032
  cp 0
  jp nc,.loopmap0302


.loopmap032: ;small part with all the ghost
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level32startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv028c : ld bc,lenghtobjectlistlv028c : ld a,36 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  ld a,(exitx)
  cp 48
  jp nc,.loopmap0312
  
  jp .loopmap0302


.loopmap033: ;the secret boss
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level33startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv028d : ld bc,lenghtobjectlistlv028d : ld a,37 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  ld a,12
  ld (makelevelavailable?),a 
  push af
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!    
  pop af
  jp z,gameloop  
  cp 5
  ld a,12
  ld (makelevelavailable?),a   
  push af
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!    
  pop af
  jp z,gameloop


.ghosthouseexit:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023s : ld bc,lenghtobjectlistlv023s : ld a,180 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 3
  ld a,13
  ld (makelevelavailable?),a   
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!    
  jp gameloop 


;morton's castle
;  ld a,3
;  ld (mariostate),a


donutcastle:
  ld a,400/2
  ld (time),a

.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader

;  jp .loopmap036

.loopmap034:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level34startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv029 : ld bc,lenghtobjectlistlv029 : ld a,38 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap017  
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
.loopmap035:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level35startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv029b : ld bc,lenghtobjectlistlv029b : ld a,39 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
.loopmap036:
 
  call placeeventhandlery  
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level36startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv029c : ld bc,lenghtobjectlistlv029c : ld a,40 : call	levelloader
  call restoreeventhandlerx  
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
.loopmap037: ;morton's end ;)
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level37startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv029d : ld bc,lenghtobjectlistlv029d : ld a,41 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  
  ld a,19
  ld (makelevelavailable?),a  
  
  call clearlocalvariables ;very important or else other object will remain active in RAM!

  ld a,3
  ld (cutscenenr),a
  call jumptocutscene
  
  
  jp gameloop
;  cp 3
;  jp z,gameloop

;tic tac toe submap 03-03a
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv076 : ld bc,lenghtobjectlistlv017 : ld a,161 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap036
  cp 2
  jp z,gameloop ;died
 ; cp 3
  jp gameloop


greenswitchpalace:
.loopmap038:
  xor a
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,level30bkgtiles : ld ix,level38startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv030 : ld bc,lenghtobjectlistlv030 : ld a,42 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

.loopmap039:
;  xor a
;  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level14startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv030b : ld bc,lenghtobjectlistlv030b : ld a,46 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ld a,21
  ld (makelevelavailable?),a  
  jp gameloop


;topsecret area
topsecret:
.loopmap042:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level42startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv031 : ld bc,lenghtobjectlistlv031 : ld a,47 : call	levelloader 
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  jp gameloop

  

;currentmap:



;vanilla dome 1
vanilla01:
  ld a,400/2
  ld (time),a

  ld a,23
  ld (mainid),a
  call fetchmapuserdata ;fetch the mapdata load mainid by hand
  ld a,(middlereached)
  and a
  jp nz,.loopmap003


.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level43startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv032 : ld bc,lenghtobjectlistlv032 : ld a,48 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  cp 5  
  jp z,.gameloop   
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level45startingcoordinates  : ld iy,level44animationadress : ld hl,objectlistlv032c : ld bc,lenghtobjectlistlv032c : ld a,50 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
.loopmap003:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level44startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv032b : ld bc,lenghtobjectlistlv032b : ld a,49 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  ld a,24
  ld (makelevelavailable?),a   
  jp gameloop

.gameloop:  

  ld a,25
  ld (makelevelavailable?),a   

  jp gameloop

;vanilla dome 2
vanilla02:
  ld a,400/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level48startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv033 : ld bc,lenghtobjectlistlv033 : ld a,53 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap002
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  cp 5
  ld a,26
  ld (makelevelavailable?),a   
  jp z,gameloop 
  ld a,29
  ld (makelevelavailable?),a    
  jp gameloop

.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a 
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level48startingcoordinates2  : ld iy,level01animationadress : ld hl,objectlistlv033 : ld bc,lenghtobjectlistlv033 : ld a,53 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap002
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  cp 5
  ld a,26
  ld (makelevelavailable?),a   
  jp z,gameloop
  ld a,29
  ld (makelevelavailable?),a     
  jp gameloop
  
  
.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a   
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level49startingcoordinates  : ld iy,level0207animationadress : ld hl,objectlistlv033b : ld bc,lenghtobjectlistlv033b : ld a,54 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012
  jp gameloop


;vanilla dome 3
vanilla03:
  ld a,250 ;higher time otherwise its impossible to beat this level
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
;  ld a,1
;  ld (marioyoshi),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level46startingcoordinates  : ld iy,level45animationadress : ld hl,objectlistlv034 : ld bc,lenghtobjectlistlv034 : ld a,51 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap002  
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  cp 4
  jp z,.loopmap017


.loopmap0012:
  ld a,4
  ld (nocontrol),a
;  ld a,1
;  ld (marioyoshi),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level46startingcoordinates2  : ld iy,level45animationadress : ld hl,objectlistlv034 : ld bc,lenghtobjectlistlv034 : ld a,51 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap002  
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop

.loopmap002:
;check exitx here and see if the jump goes to maplevel03-03c 
  ld a,(exitx)
  cp 230
  jp nc,.loopmap003
  

 
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a  
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level47startingcoordinates  : ld iy,level46animationadress : ld hl,objectlistlv034b : ld bc,lenghtobjectlistlv034b : ld a,52 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  ld a,31
  ld (makelevelavailable?),a     
  jp gameloop
  

.loopmap003:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a   
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level47cstartingcoordinates  : ld iy,level0207animationadress : ld hl,objectlistlv034c : ld bc,lenghtobjectlistlv034c : ld a,61 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
;  jp z,gameloop
  jp .loopmap0012


;tic tac toe submap 03-03a
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap0012
  cp 2
  jp z,gameloop ;died
 ; cp 3
 ; jp z,gameloop


;vanilla dome 4
vanilla04:
  ld a,400/2
  ld (time),a


.loopmap041:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level50startingcoordinates  : ld iy,level41animationadress : ld hl,objectlistlv035 : ld bc,lenghtobjectlistlv035 : ld a,45 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap042
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  ld a,32
  ld (makelevelavailable?),a    
  jp gameloop

.loopmap0412:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level50startingcoordinates2  : ld iy,level41animationadress : ld hl,objectlistlv035 : ld bc,lenghtobjectlistlv035 : ld a,45 : call	levelloader
  ld a,(nocontrol)
  cp 1
  jp z,.loopmap042
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  ld a,32
  ld (makelevelavailable?),a  
  jp gameloop


.loopmap042:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level51startingcoordinates  : ld iy,level55animationadress : ld hl,objectlistlv035b : ld bc,lenghtobjectlistlv035b : ld a,55 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  jp .loopmap0412




;vanilla dome 5 (ghosthouse)
vanilla05:
  ld a,300/2
  ld (time),a


.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader

;jp .loopmap002

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level52startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv036 : ld bc,lenghtobjectlistlv036 : ld a,56 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
  jp .loopmap002

.loopmap0012:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level52startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv036 : ld bc,lenghtobjectlistlv036 : ld a,56 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop


.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level53startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv036b : ld bc,lenghtobjectlistlv036b : ld a,57 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop

  ld a,(exitx)
  cp 233
  jp nc,.loopmap003


  jp .loopmap0012


.loopmap003:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023s : ld bc,lenghtobjectlistlv023s : ld a,181 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
  ld a,30
  ld (makelevelavailable?),a  
  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!  
  jp gameloop 
  
  ;ret


;vanilla secret1
vanillas1:

  ld a,300/2
  ld (time),a

.loopmap001:
  call placeeventhandlery 
  xor a
  ld (nocontrol),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level54startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv037 : ld bc,lenghtobjectlistlv037 : ld a,58 : call	levelloader
  call restoreeventhandlerx
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died  

  ld a,(exitx)
  cp 4
  jp c,.loopmap003


.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level55startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv037b : ld bc,lenghtobjectlistlv037b : ld a,59 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld hl,levelavailabilitylist+3 ;do it lamely by hand because this animation is not on the worldmap and does not have to be either :)
  ld a,(hl)
  set 0,a
  ld (hl),a

  jp gameloop

.loopmap003:
  ld a,4
  ld (nocontrol),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level56startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv037c : ld bc,lenghtobjectlistlv037c : ld a,60 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld hl,levelavailabilitylist+3 ;do it lamely by hand because this animation is not on the worldmap and does not have to be either :)
  ld a,(hl)
  set 1,a
  ld (hl),a  
  
  
  ld a,34
  ld (makelevelavailable?),a

  jp gameloop

;vanilla secret2
vanillas2:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level57startingcoordinates  : ld iy,level62animationadress : ld hl,objectlistlv038 : ld bc,lenghtobjectlistlv038 : ld a,62 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,.gameloop
  
  jp .loopmap002
  
.loopmap0012:
  ld a,1
  ld (tubelaunch),a ;do a tubelaunch
  xor a
  ld (nocontrol),a
  ld (standingonexit),a  
  ld (marioduck),a  
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level57startingcoordinates2  : ld iy,level62animationadress : ld hl,objectlistlv038 : ld bc,lenghtobjectlistlv038 : ld a,62 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,.gameloop  
  
;  jp .loopmap002

  
.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2f : ld (currentmusicblock),a : ld de,0 : ld ix,level58startingcoordinates  : ld iy,level55animationadress : ld hl,objectlistlv038b : ld bc,lenghtobjectlistlv038b : ld a,63 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  jp .loopmap0012


.gameloop:

  ld a,35
  ld (makelevelavailable?),a 


  jp gameloop


;vanilla secret3
vanillas3:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level59startingcoordinates  : ld iy,level06animationadress : ld hl,objectlistlv039 : ld bc,lenghtobjectlistlv039 : ld a,64 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,$3f : ld (currentmusicblock),a : ld de,0 : ld ix,level60startingcoordinates  : ld iy,level06animationadress : ld hl,objectlistlv039b : ld bc,lenghtobjectlistlv039b : ld a,65 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld hl,levelavailabilitylist+4 ;do it lamely by hand because this animation is not on the worldmap and does not have to be either :)
  ld a,(hl)
  set 0,a
  ld (hl),a


  jp gameloop


;vanilla fortress
vanillaf:
  ld a,300/2
  ld (time),a

.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader

.loopmap001:
  ld a,4
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level61startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv040 : ld bc,lenghtobjectlistlv040 : ld a,66 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ld a,(exitx)
  cp 66
  jp nc,.loopmap0013


.loopmap0012:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level62startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv040b : ld bc,lenghtobjectlistlv040b : ld a,67 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 6
  jp z,gameloop 
  jp .loopmap002

.loopmap0013: ;second tube
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level62startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv040b : ld bc,lenghtobjectlistlv040b : ld a,67 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 6
  jp z,gameloop

.loopmap002: ;reznor
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level63startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv040c : ld bc,lenghtobjectlistlv040c : ld a,68 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

.levelfinished  
  

  ld a,36
  ld (makelevelavailable?),a 

  jp gameloop  


vanillacastle:
  ld a,250
  ld (time),a
  
.castlecutscene2:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,153 : call	levelloader
  
  ld a,31
  ld (mainid),a
  call fetchmapuserdata ;fetch the mapdata load mainid by hand
  ld a,(middlereached)
  and a
  jp nz,.loopmap003
  
;first part
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level64startingcoordinates  : ld iy,level69animationadress : ld hl,objectlistlv041 : ld bc,lenghtobjectlistlv041 : ld a,69 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld a,(exitx4)
  cp 64
  jp c,.loopmap003


;moving block part part c
.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level65startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv041c : ld bc,lenghtobjectlistlv041c : ld a,70 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died


;part d (the boss)
.loopmap004:
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level67startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv041d : ld bc,lenghtobjectlistlv041d : ld a,72 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  call clearlocalvariables ;very important or else other object will remain active in RAM!

.cutscene:
  ld a,4
  ld (cutscenenr),a
  call jumptocutscene

  ld a,33
  ld (makelevelavailable?),a 

  jp gameloop

;secret part b
.loopmap003:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level66startingcoordinates  : ld iy,level69animationadress : ld hl,objectlistlv041b : ld bc,lenghtobjectlistlv041b : ld a,71 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died



  jp .loopmap002


;butterbridge 1
butter1:
  xor a
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level68startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv042 : ld bc,lenghtobjectlistlv042 : ld a,73 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level69startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv042b : ld bc,lenghtobjectlistlv042b : ld a,74 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died


  ld a,37
  ld (makelevelavailable?),a
  

  jp gameloop


;butterbridge 2
butter2:
  ld a,400/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level68startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv043 : ld bc,lenghtobjectlistlv043 : ld a,75 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,.gameloop
  
  jp .loopmap002



.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a  
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level68startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv043 : ld bc,lenghtobjectlistlv043 : ld a,75 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 3
  jp z,.gameloop

.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level70startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv043b : ld bc,lenghtobjectlistlv043b : ld a,76 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  jp .loopmap0012
;  jp gameloop

.gameloop:

  ld a,38
  ld (makelevelavailable?),a

  jp gameloop

;cheesbridge 3
cheese3:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level71startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv044 : ld bc,lenghtobjectlistlv044 : ld a,77 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap003
  cp 5
  jp z,.loopmap002

  ld a,(exitx4)
  cp 163
  jp nc,.secretexit

  ld a,40
  ld (makelevelavailable?),a
  

  jp gameloop

.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a 
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level71startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv044 : ld bc,lenghtobjectlistlv044 : ld a,77 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap003
  cp 5
  jp z,.loopmap002

  ld a,(exitx4)
  cp 163
  jp nc,.secretexit

  ld a,40
  ld (makelevelavailable?),a  

  jp gameloop

.secretexit:

  ld a,39
  ld (makelevelavailable?),a


  jp gameloop


;this map is special. Mario comes in with yoshi from underneath the screen. We require a special object to handle that
.loopmap002:
  ld a,5
;  xor a
  ld (nocontrol),a
  ld a,4
  ld (yoshitype),a
  ld a,1
  ld (marioyoshi),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level72startingcoordinates  : ld iy,level41animationadress : ld hl,objectlistlv044b : ld bc,lenghtobjectlistlv044b : ld a,78 : call	levelloader
  ld a,(nocontrol)
;  cp 2
;  jp z,gameloop ;died

;you cannot die on this map! dropping below the camera exits the level to the next map
  ;makelevelavailable blablabla
  ld a,40
  ld (makelevelavailable?),a 
  
  
  jp gameloop

.loopmap003:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a  
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level73startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv044c : ld bc,lenghtobjectlistlv044c : ld a,79 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  jp .loopmap0012



;cookiemountain 4
cookie4:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level74startingcoordinates  : ld iy,level62animationadress : ld hl,objectlistlv045 : ld bc,lenghtobjectlistlv045 : ld a,80 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  ld a,41
  ld (makelevelavailable?),a   
  
  jp gameloop

.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a  
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level74startingcoordinates2  : ld iy,level62animationadress : ld hl,objectlistlv045s : ld bc,lenghtobjectlistlv045s : ld a,80 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  ld a,41
  ld (makelevelavailable?),a  
  
  jp gameloop



.loopmap002:
 
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level75startingcoordinates  : ld iy,level06animationadress : ld hl,objectlistlv045b : ld bc,lenghtobjectlistlv045b : ld a,81 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  jp .loopmap0012



;sodalake 5
soda5:
  ld a,300/2
  ld (time),a


.loopmap001:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a 
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level76sstartingcoordinates  : ld iy,level26animationadress : ld hl,objectlistlv046 : ld bc,lenghtobjectlistlv046 : ld a,82 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

.loopmap002:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a 
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level77startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv046b : ld bc,lenghtobjectlistlv046b : ld a,83 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

;unlock location to star secret
  
;there is no path to be drawn so simply unlock   
    ld hl,levelavailabilitylist+10
    ld a,(hl)
    set 1,a
    ld (hl),a  
  
  
  jp gameloop


;ludwig6
ludwig6:
  ld a,300/2
  ld (time),a


.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader


.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level78startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv047 : ld bc,lenghtobjectlistlv047 : ld a,84 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap017

.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level79startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv047b : ld bc,lenghtobjectlistlv047b : ld a,85 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died


.loopmap003:
  call placeeventhandlery  
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level80startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv047c : ld bc,lenghtobjectlistlv047c : ld a,86 : call	levelloader
  call restoreeventhandlerx
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

;final boss for this map

.loopmap004:
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level37startingcoordinates  : ld iy,level87animationadress : ld hl,objectlistlv047d : ld bc,lenghtobjectlistlv047d : ld a,87 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  
   call clearlocalvariables ;very important or else other object will remain active in RAM!

.cutscene:
  ld a,5
  ld (cutscenenr),a
  call jumptocutscene

  ld a,42
  ld (makelevelavailable?),a

  jp gameloop


;tic tac toe submap 04-06
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv076 : ld bc,lenghtobjectlistlv017 : ld a,161 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap002

  jp gameloop

  
;world5! forest of illusion  

;illusion1
illusion1:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level81startingcoordinates  : ld iy,level62animationadress : ld hl,objectlistlv048 : ld bc,lenghtobjectlistlv048 : ld a,88 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

  ld a,43
  ld (makelevelavailable?),a


  jp gameloop

  
.secret:  
 
  ld a,44
  ld (makelevelavailable?),a


  jp gameloop 
 
 
 
;illusion2
illusion2:
  ld a,300/2
  ld (time),a

.loopmap001:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a 
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level82startingcoordinates  : ld iy,level26animationadress : ld hl,objectlistlv049 : ld bc,lenghtobjectlistlv049 : ld a,89 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret
  
  
  ld a,45
  ld (makelevelavailable?),a  
  
  jp gameloop

  
.secret:

  ld a,46
  ld (makelevelavailable?),a  
  
  jp gameloop    
  
  
;illusion3
illusion3:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level83startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv050 : ld bc,lenghtobjectlistlv050 : ld a,90 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  
  ld a,47
  ld (makelevelavailable?),a  
  
  jp gameloop

.loopmap0012:
  ld a,1
  ld (nocontrol),a
 ; xor a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level83startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv050 : ld bc,lenghtobjectlistlv050 : ld a,90 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  
  ld a,47
  ld (makelevelavailable?),a  
  
  jp gameloop


.loopmap0013:
  ld a,1
  ld (nocontrol),a
 ; xor a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level83startingcoordinates3  : ld iy,level00animationadress : ld hl,objectlistlv050 : ld bc,lenghtobjectlistlv050 : ld a,90 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  ld a,47
  ld (makelevelavailable?),a  
  
  jp gameloop


.loopmap002:
 
  ld a,(exitx4)
  cp 100
  jp c,.loopmap017

   
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a 
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,level50bkgtiles : ld ix,level84startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv050b : ld bc,lenghtobjectlistlv050b : ld a,91 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

  jp .loopmap0013

.secret:

  ld a,53
  ld (makelevelavailable?),a  


    jp gameloop



;tic tac toe submap 05-03c
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap0012

  jp gameloop


;illusion4
illusion4:
  ld a,300/2
  ld (time),a

 
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level85startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv051 : ld bc,lenghtobjectlistlv051 : ld a,92 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  ld a,49
  ld (makelevelavailable?),a    
  
  
  jp gameloop
  
.loopmap0012:
  ld a,1
  ld (nocontrol),a
;  xor a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level85startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv051 : ld bc,lenghtobjectlistlv051 : ld a,92 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002

  ld a,49
  ld (makelevelavailable?),a   
  
  jp gameloop  
  
  
.loopmap002:
  ld a,(exitx4)
  cp 100
  jp nc,.loopmap003
 
 
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level86startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv051b : ld bc,lenghtobjectlistlv051b : ld a,93 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld a,50
  ld (makelevelavailable?),a   
  
  
  jp gameloop


.loopmap003:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level87startingcoordinates  : ld iy,level26animationadress : ld hl,objectlistlv051c : ld bc,lenghtobjectlistlv051c : ld a,94 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012
  jp gameloop


;illusion5 ghosthouse
illusion5:
  ld a,400/2
  ld (time),a


.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader



.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level88startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv052 : ld bc,lenghtobjectlistlv052 : ld a,95 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
    
    ld a,(exitx4)
    cp 55/4
    jp c,.loopmap0031 ;jump to secret level
    cp 250/4
    jp c,.loopmap003

    jp .loopmap002

    
.loopmap0012:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level88startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv052 : ld bc,lenghtobjectlistlv052 : ld a,95 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
    
    ld a,(exitx4)
    cp 55/4
    jp c,.loopmap0031 ;jump to secret level
    cp 250/4
    jp c,.loopmap003

    jp .loopmap002    
    

.loopmap0013:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level88startingcoordinates3  : ld iy,level00animationadress : ld hl,objectlistlv052 : ld bc,lenghtobjectlistlv052 : ld a,95 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
    
    ld a,(exitx4)
    cp 55/4
    jp c,.loopmap0031 ;jump to secret level
    cp 250/4
    jp c,.loopmap003

    jp .loopmap002    
    
    

.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level89startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv052b : ld bc,lenghtobjectlistlv052b : ld a,96 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

    ld a,(exitx4)
    cp 230/4
    jp c,.loopmap0012
  
  
  
  jp .loopmap0013
    
    
 
.loopmap003:
    xor a
    ld (nocontrol),a
    ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023s : ld bc,lenghtobjectlistlv023s : ld a,182 : call	levelloader
    ld a,(nocontrol)
    cp 2
    jp z,gameloop ;died
  ;  cp 3
    
  ld a,47
  ld (makelevelavailable?),a
   call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!
 
  jp gameloop


;secret ghosthouse exit    
.loopmap0031:
    xor a
    ld (nocontrol),a
    ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023bs : ld bc,lenghtobjectlistlv023bs : ld a,182 : call	levelloader
    ld a,(nocontrol)
    cp 2
    jp z,gameloop ;died
    ;cp 3
    ld a,48
    ld (makelevelavailable?),a   
 ;   jp z,gameloop 
   call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!
    jp gameloop
  
;illusion secret 1
illusion6:
  ld a,300/2
  ld (time),a

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level90startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv053 : ld bc,lenghtobjectlistlv053 : ld a,97 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  
  ld a,51
  ld (makelevelavailable?),a  

  jp gameloop


;forest fortress
illusion7:
  ld a,400/2
  ld (time),a

.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader



;  jp .loopmap002 ;bug??

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level91startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv054 : ld bc,lenghtobjectlistlv054 : ld a,98 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

 ; jp gameloop

.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level92startingcoordinates  : ld iy,level69animationadress : ld hl,objectlistlv054b : ld bc,lenghtobjectlistlv054b : ld a,99 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

 ; jp gameloop

.loopmap003: ;reznor
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level63startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv040c : ld bc,lenghtobjectlistlv040c : ld a,68 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld a,52
  ld (makelevelavailable?),a 


  jp gameloop   

;roys castle
illusion8:
  ld a,300/2
  ld (time),a

.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,152 : call	levelloader


.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level93startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv055 : ld bc,lenghtobjectlistlv055 : ld a,100 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

;  jp gameloop
.loopmap037: ;roy's end ;) recycle entire morton
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level37startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv029d : ld bc,lenghtobjectlistlv029d : ld a,41 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  
 ; ld a,19
 ; ld (makelevelavailable?),a  
  
  call clearlocalvariables ;very important or else other object will remain active in RAM!

  ld a,6
  ld (cutscenenr),a
  call jumptocutscene
  
  ld a,54
  ld (makelevelavailable?),a   
  
  jp gameloop


;blue switch palace
blueswpalace:


.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level42startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv056 : ld bc,lenghtobjectlistlv056 : ld a,102 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

;  jp gameloop

;blueswitchb use yellow switch palace map
.loopmap002:
;  ld a,4
;  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level14startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv057 : ld bc,lenghtobjectlistlv057 : ld a,103 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  ld a,23
  ld (makelevelavailable?),a  
  jp gameloop


;red switch palace
redswpalace:


.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level42startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv058 : ld bc,lenghtobjectlistlv058 : ld a,104 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

;  jp gameloop

;redswitchb use yellow switch palace map
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level14startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv057 : ld bc,lenghtobjectlistlv057 : ld a,105 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  ld a,22
  ld (makelevelavailable?),a  
  jp gameloop




;Chocolate island 1
chocolate1:
  ld a,300/2
  ld (time),a
  
;1a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level94startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv059 : ld bc,lenghtobjectlistlv059 : ld a,106 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012

  ld a,55
  ld (makelevelavailable?),a   
  
  jp gameloop

  ;first tubelaunch
.loopmap0012:
 
 
    ld a,(exitx4)
    cp 503/4
    jp nc,.loopmap0013
    cp 331/4
    jp nc,.loopmap002 ;exit to special map
    
 
 
  xor a
  ld (nocontrol),a
  ld (standingonexit),a  
  ld (marioduck),a 
  ld a,1
  ld (tubelaunch),a ;do a tubelaunch  
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level94startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv059 : ld bc,lenghtobjectlistlv059 : ld a,106 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012

  ld a,55
  ld (makelevelavailable?),a    
  
  jp gameloop
  
  ;second tubelaunch
.loopmap0013:
  xor a
  ld (nocontrol),a
  ld (standingonexit),a  
  ld (marioduck),a 
  ld a,1
  ld (tubelaunch),a ;do a tubelaunch  
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level94startingcoordinates3  : ld iy,level00animationadress : ld hl,objectlistlv059 : ld bc,lenghtobjectlistlv059 : ld a,106 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012

  ld a,55
  ld (makelevelavailable?),a    
  
  jp gameloop

;1b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level95startingcoordinates  : ld iy,level06animationadress : ld hl,objectlistlv059b : ld bc,lenghtobjectlistlv059b : ld a,107 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0013

;  ld a,56
;  ld (makelevelavailable?),a    
  
  jp gameloop

;Chocolate 2
chocolate2:
  ld a,300/2
  ld (time),a
  
;2a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level94startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060 : ld bc,lenghtobjectlistlv060 : ld a,108 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop

;coin rule as per smw map
    ld a,(gold)
    cp 21
    jp nc,.loopmap004       
    cp 09
    jp nc,.loopmap003
 



;2b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060b : ld bc,lenghtobjectlistlv060b : ld a,109 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  jp .gameloop
;2c
.loopmap003:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060c : ld bc,lenghtobjectlistlv060c : ld a,110 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  jp .gameloop
;2d
.loopmap004:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060d : ld bc,lenghtobjectlistlv060d : ld a,111 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp .gameloop

    
  ;next rule
.gameloop:

    ld a,(time)
    cp 220/2
    jp nc,.loopmap005
    cp 200/2
    jp nc,.loopmap006
    
    jp .loopmap007
    
    


;2e
.loopmap005:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,level60ebkgtiles : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060e : ld bc,lenghtobjectlistlv060e : ld a,112 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop

;add worldmap code in here!
  ld a,57
  ld (makelevelavailable?),a

    jp gameloop

;2f
.loopmap006:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060f : ld bc,lenghtobjectlistlv060f : ld a,113 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop
    jp .exit
;2g
.loopmap007:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060g : ld bc,lenghtobjectlistlv060g : ld a,114 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop
;    jp .exit
    
    
.exit:

    
    ld a,(yoshicoin)
    cp 4
    jp nc,.loopmap009
    
    
    

;exits
;2h
.loopmap008:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060h : ld bc,lenghtobjectlistlv060h : ld a,115 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1

  ld a,58
  ld (makelevelavailable?),a

  jp gameloop
;2i
.loopmap009:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level96startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv060i : ld bc,lenghtobjectlistlv060i : ld a,116 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1

  ld a,58
  ld (makelevelavailable?),a

  jp gameloop


;Chocolate3
chocolate3:
  ld a,300/2
  ld (time),a

;3a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level97startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv061 : ld bc,lenghtobjectlistlv061 : ld a,117 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002
 
  
 
 
  jp .gameloop

;3a
.loopmap0012:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level97startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv061 : ld bc,lenghtobjectlistlv061 : ld a,117 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002
 

 
  jp .gameloop  
  
  
;3b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level98startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv061b : ld bc,lenghtobjectlistlv061b : ld a,118 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap0012
  
  jp gameloop

  
.gameloop:
  
    ld a,(exitx4)
    cp 560/4
    jp nc,.secret ;exit to special map  
  
  
 ;add exit code here  
  ld a,59
  ld (makelevelavailable?),a
  
  
    jp gameloop

.secret:

 ;add exit code here  
  ld a,60
  ld (makelevelavailable?),a


    jp gameloop
    

;Chocolate4
chocolate4:
  ld a,300/2
  ld (time),a

;ld a,1
;ld (mariostate),a

;jp .loopmap0012

;4a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level99startingcoordinates  : ld iy,level119animationadress : ld hl,objectlistlv062 : ld bc,lenghtobjectlistlv062 : ld a,119 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002
  
 ;exit scene here 

  ld a,62
  ld (makelevelavailable?),a
  
  
  jp gameloop ;do not forget to place this back

.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level99startingcoordinates2 : ld iy,level119animationadress : ld hl,objectlistlv062 : ld bc,lenghtobjectlistlv062 : ld a,119 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002
  
 ;exit scene here 
 
  ld a,62
  ld (makelevelavailable?),a
   
 
 
  jp gameloop  
  
 
  
;4b
.loopmap002:
  xor a
  ld (nocontrol),a  
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level100startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv062b : ld bc,lenghtobjectlistlv062b : ld a,120 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  jp .loopmap0012



;Chocolate5
chocolate5:
  ld a,300/2
  ld (time),a

;5a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level101startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv063 : ld bc,lenghtobjectlistlv063 : ld a,121 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
    jp z,.gameloop
  
  
  ld a,63
  ld (makelevelavailable?),a
  
  
  ;exit scene here
  
  
  jp gameloop


.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level101startingcoordinates2 : ld iy,level00animationadress : ld hl,objectlistlv063 : ld bc,lenghtobjectlistlv063 : ld a,121 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
    jp z,.gameloop
  
  
  ;exit scene here
  ld a,63
  ld (makelevelavailable?),a  
  
  jp gameloop  
  
  

  
;tic tac toe submap 05-03c
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
  cp 4
  jp z,.loopmap0012

  jp gameloop  
  
.gameloop:


    ld a,(exitx4)
    cp 170/4
    jp nc,.loopmap002 ;exit to special map    
 
 
    jp .loopmap017   






;5b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a  
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level102startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv063b : ld bc,lenghtobjectlistlv063b : ld a,122 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  jp .loopmap0012



;Chocolate6 ghosthouse
chocolate6:
  ld a,300/2
  ld (time),a

.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader



;6a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level103startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv064 : ld bc,lenghtobjectlistlv064 : ld a,123 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop
;6b
.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level104startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv064b : ld bc,lenghtobjectlistlv064b : ld a,124 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop


  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023s : ld bc,lenghtobjectlistlv023s : ld a,183 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 3
;  ld a,15
;  ld (makelevelavailable?),a    
  ld a,56
  ld (makelevelavailable?),a

  call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!
    jp gameloop


;Chocolate7
chocolate7:
  ld a,300/2
  ld (time),a




;7a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level105startingcoordinates  : ld iy,level45animationadress : ld hl,objectlistlv065 : ld bc,lenghtobjectlistlv065 : ld a,125 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap004

  jp .loopmap002
  
  
;7a
.loopmap0012:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a 
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level105startingcoordinates2  : ld iy,level45animationadress : ld hl,objectlistlv065 : ld bc,lenghtobjectlistlv065 : ld a,125 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap004  
  
  
;7b
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level106startingcoordinates  : ld iy,level45animationadress : ld hl,objectlistlv065b : ld bc,lenghtobjectlistlv065b : ld a,126 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
 ; jp .loopmap003


;7c
.loopmap003:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level107startingcoordinates  : ld iy,level44animationadress : ld hl,objectlistlv065c : ld bc,lenghtobjectlistlv065c : ld a,127 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  jp .loopmap005

;7d
.loopmap004:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level108startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv065d : ld bc,lenghtobjectlistlv065d : ld a,128 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop
  jp .loopmap0012


;7e
.loopmap005:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level109startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv065e : ld bc,lenghtobjectlistlv065e : ld a,129 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1


;write directly to ram as these paths have never been made   
    ld hl,levelavailabilitylist+10
    ld a,(hl)
    set 3,a
    ld (hl),a
    
    dec hl
    dec hl
    ld a,(hl)
    set 4,a
    ld (hl),a
;exit sequence here
 ; ld a,63
 ; ld (makelevelavailable?),a


  jp gameloop




;Chocolate8 fortress
chocolate8:
  ld a,300/2
  ld (time),a


.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,185 : call	levelloader

;8a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level110startingcoordinates  : ld iy,level130animationadress : ld hl,objectlistlv066 : ld bc,lenghtobjectlistlv066 : ld a,130 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop
;8b
.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level111startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv066b : ld bc,lenghtobjectlistlv066b : ld a,131 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop



.loopmap003: ;reznor
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level63startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv040c : ld bc,lenghtobjectlistlv040c : ld a,68 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ;trow exit code here!
  
  ld a,61
  ld (makelevelavailable?),a 


  jp gameloop  




;Chocolate9 ;wendy's castle
chocolate9:
  ld a,300/2
  ld (time),a
  
 ; xor a
 ; ld (time),a ;DEBUG


.castlecutscene3:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,185 : call	levelloader

;  jp .loopmap002


;9a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level112startingcoordinates  : ld iy,level09animationadress : ld hl,objectlistlv067 : ld bc,lenghtobjectlistlv067 : ld a,132 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop
;9b
.loopmap002:
  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level113startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv067b : ld bc,lenghtobjectlistlv067b : ld a,133 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop
;9c boss
.loopmap003:
  call placeeventhandlery 
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level67startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv067c : ld bc,lenghtobjectlistlv067c : ld a,134 : call	levelloader
  call restoreeventhandlerx
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1

  call clearlocalvariables ;very important or else other object will remain active in RAM!

.cutscene:
  ld a,7
  ld (cutscenenr),a
  call jumptocutscene

  ld a,64
  ld (makelevelavailable?),a 

  jp gameloop 

  
  
  
  ;warning!!: we see the ghost ship as valley1 map meaning that each map has 1 lower count. This will not correspond to the mariowiki etc.
  
;bowserva1
bowserva1:
  ld a,400/2
  ld (time),a


;1a
.loopmap001:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level135startingcoordinates  : ld iy,level135animationadress : ld hl,objectlistlv068 : ld bc,lenghtobjectlistlv068 : ld a,135 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 1
;  jp gameloop  
;1b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level136startingcoordinates  : ld iy,level135animationadress : ld hl,objectlistlv068b : ld bc,lenghtobjectlistlv068b : ld a,136 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop  
;1c
.loopmap003:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level137startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv068c : ld bc,lenghtobjectlistlv068c : ld a,137 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop  
  
  ld a,65
  ld (makelevelavailable?),a 
  
  call enablesavemenu
  
  
  jp gameloop
  
  
;bowserva2 valley 1
bowserva2:
  ld a,300/2
  ld (time),a


;1a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,level69bkgtiles : ld ix,level96startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv069 : ld bc,lenghtobjectlistlv069 : ld a,138 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
  jp .gameloop    
 

;tic tac toe submap 03-03a
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
;  cp 4
;  jp z,.loopmap0242
  cp 2
  jp z,gameloop ;died
 ; cp 3
 ; jp z,gameloop

;1a
.loopmap0012:

  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,level69bkgtiles : ld ix,level138startingcoordinates2  : ld iy,level01animationadress : ld hl,objectlistlv069 : ld bc,lenghtobjectlistlv069 : ld a,138 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1
;  jp gameloop    

 
 
.gameloop: 
 
    ld a,(exitx4)    
    cp 520/4
    jp nc,.loopmap002 ;exit to special map  

    jp .loopmap017
 

 ;7e
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level109startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv065e : ld bc,lenghtobjectlistlv065e : ld a,186 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 1


;exit sequence here

  ld a,66
  ld (makelevelavailable?),a 


  jp gameloop
 
 
 
 
;bowserva3 valley 2
bowserva3:
  xor a
  ld (time),a

;3a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level139startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv070 : ld bc,lenghtobjectlistlv070 : ld a,139 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop    
;3b
.loopmap002:
  ld a,4
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level140startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv070b : ld bc,lenghtobjectlistlv070b : ld a,140 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop    
;3c
.loopmap003:
  ld a,4
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level141startingcoordinates  : ld iy,level44animationadress : ld hl,objectlistlv070c : ld bc,lenghtobjectlistlv070c : ld a,141 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret
  

 ;3d
.loopmap004:
  ld a,4
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level109startingcoordinates  : ld iy,level01animationadress : ld hl,objectlistlv065e : ld bc,lenghtobjectlistlv065e : ld a,187 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died


;exit sequence here
  ld a,69
  ld (makelevelavailable?),a 
  

  jp gameloop  
 
.secret:

  ld a,67
  ld (makelevelavailable?),a 

  jp gameloop  
    
  
  
  
;bowserva4 valley 5
bowserva4:
  ld a,300/2
  ld (time),a

;4a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level142startingcoordinates  : ld iy,level41animationadress : ld hl,objectlistlv071 : ld bc,lenghtobjectlistlv071 : ld a,142 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap017
  
;we win  
  
  
  jp .gameloop    

    
;tic tac toe submap 03-03a
.loopmap017:
  ld a,4
  ld (nocontrol),a
  ld a,bonusblock : ld (currentmusicblock),a : ld de,0 : ld ix,level17startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv017 : ld bc,lenghtobjectlistlv017 : ld a,17 : call	levelloader
  ld a,(nocontrol)
;  cp 4
;  jp z,.loopmap0242
  cp 2
  jp z,gameloop ;died
 ; cp 3
 ; jp z,gameloop
    

;4a
.loopmap0012:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level142startingcoordinates2  : ld iy,level41animationadress : ld hl,objectlistlv071 : ld bc,lenghtobjectlistlv071 : ld a,142 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap017
  
;we win  



.gameloop:

  
  ld a,72
  ld (makelevelavailable?),a   

  
  jp gameloop      
    


;bowserva5 valley 6
bowserva5:
  ld a,300/2
  ld (time),a

;5a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level47startingcoordinates  : ld iy,level45animationadress : ld hl,objectlistlv072 : ld bc,lenghtobjectlistlv072 : ld a,143 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002     
  
  jp .endlevel

;5a
.loopmap0012:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level143startingcoordinates2  : ld iy,level45animationadress : ld hl,objectlistlv072 : ld bc,lenghtobjectlistlv072 : ld a,143 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
  jp z,.loopmap002     
  
  jp .endlevel  
  
  
  
;5b
.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level144startingcoordinates  : ld iy,level0207animationadress : ld hl,objectlistlv072b : ld bc,lenghtobjectlistlv072b : ld a,144 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop     

    jp .loopmap0012

.endlevel:

    cp 5
    jp z,.secret

  ld a,75
  ld (makelevelavailable?),a     
    

    jp gameloop

    
.secret:

  ld a,73
  ld (makelevelavailable?),a 
    
    jp gameloop    
    
;bowserva6 valley ghosthouse
bowserva6:
  ld a,300/2
  ld (time),a

.ghosthousecutscene:

  xor a
  ld (nocontrol),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutsceneghost : ld bc,lenghtcutsceneghost : ld a,154 : call	levelloader


;6a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level45startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv073 : ld bc,lenghtobjectlistlv073 : ld a,145 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop     
;6b
.loopmap002:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level145startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv073b : ld bc,lenghtobjectlistlv073b : ld a,146 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop


    ld a,(exitx4)
    cp 100/4    
    jp c,.loopmap0032 ;right
    cp 220/4
    jp c,.loopmap001 
    cp 247/4 ;e
    jp c,.loopmap0032 
    cp 295/4 ;g en f
    jp c,.ghosthouseexit    
    cp 315/4 ;h
    jp c,.loopmap003 ;left



    
    

;6c
.loopmap003:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level146startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv073c : ld bc,lenghtobjectlistlv073c : ld a,147 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop  

    jp .loopmap002


;6c
.loopmap0032:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,hauntedmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level146startingcoordinates2  : ld iy,level00animationadress : ld hl,objectlistlv073c : ld bc,lenghtobjectlistlv073c : ld a,147 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 5
  jp .secret  

  ;  jp .loopmap002

.ghosthouseexit:
  xor a
  ld (nocontrol),a
  ld a,$68 : ld (currentmusicblock),a : ld de,0 : ld ix,level23startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv023s : ld bc,lenghtobjectlistlv023s : ld a,184 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld a,71
  ld (makelevelavailable?),a   
    call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!
  
  jp gameloop   
  
.secret:

    
    ld a,(exitx4)
    cp 125/4    
    jp c,.loopmap002 ;right


  ld a,70
  ld (makelevelavailable?),a   
    call enablesavemenu ;very complex bullshity way of calling this but I will NOT add more mapdata to this game!
  
  jp gameloop     

  
;bowserva7 valley fortress
bowserva7:
  ld a,300/2
  ld (time),a
  
.castlecutscene2:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,153 : call	levelloader
;  ld a,(nocontrol)



;7a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level65startingcoordinates  : ld iy,level09animationadress : ld hl,objectlistlv074 : ld bc,lenghtobjectlistlv074 : ld a,148 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop    
;7b  
.loopmap002: ;reznor
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level63startingcoordinates  : ld iy,level37animationadress : ld hl,objectlistlv040c : ld bc,lenghtobjectlistlv040c : ld a,68 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

 ld a,68
 ld (makelevelavailable?),a 


  jp gameloop  
  
  
  
;bowserva8 larry's castle
bowserva8:
  ld a,300/2
  ld (time),a 

  
.castlecutscene2:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,153 : call	levelloader
;  ld a,(nocontrol)  
;8a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level148startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv075 : ld bc,lenghtobjectlistlv075 : ld a,149 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop  
;8b
.loopmap002:

  ld a,300/2
  ld (time),a ;reset the time since the above code took allready enough time

  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level149startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv075b : ld bc,lenghtobjectlistlv075b : ld a,150 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop    
  
  
  
;This is larry!! the recycled iggy with some extra flames in there.
.loopmap011:
  xor a
  ld (nocontrol),a
  ld a,$51 : ld (currentmusicblock),a : ld de,0 : ld ix,level150startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv074b : ld bc,lenghtobjectlistlv074b : ld a,151 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
;  cp 3
;  jp z,gameloop
;  cp 5
;  jp z,.loopmap010  
  
 ;makelevelavailable trow cutscene 
 
  call clearlocalvariables ;very important or else other object will remain active in RAM! 
 
  ld a,8
  ld (cutscenenr),a
  call jumptocutscene
 
  ld a,74
  ld (makelevelavailable?),a 
  
  jp gameloop
  
intromap:

    ld a,1
    ld (firstrun),a ;that was the firstrun
    
    call clearlocalvariables

    
  xor a
  ld (nocontrol),a
  ld a,beginmusicblock : ld (currentmusicblock),a : ld de,0 : ld ix,level161startingcoordinates  : ld iy,animate0 : ld hl,objectlistintromap : ld bc,lenghtobjectlistintromap : ld a,155 : call	levelloader

  ;  call writesramuserdata  ;write the firstrun byte into the sram
  
  
  ret ;return to calling part work from there

;starworld
starworld1:  
  
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level162startingcoordinates  : ld iy,level01animationadress : ld hl,objectliststarworld1 : ld bc,lenghtobjectliststarworld1 : ld a,156 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

    jp gameloop

.secret:

    ld a,(levelavailabilitylist+3)
    set 1,a
    set 3,a
    ld (levelavailabilitylist+3),a
    
  ld a,76
  ld (makelevelavailable?),a     

    jp gameloop    
    
    
starworld2:  
  
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,underwaterblock : ld (currentmusicblock),a : ld de,0 : ld ix,level163startingcoordinates  : ld iy,level26animationadress : ld hl,objectliststarworld2 : ld bc,lenghtobjectliststarworld2 : ld a,157 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

  ;  jp gameloop    

.loopmap002:
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a 
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level77startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv046b : ld bc,lenghtobjectlistlv046b : ld a,83 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  jp gameloop  
  
.secret:

  ld a,77
  ld (makelevelavailable?),a  

    jp gameloop
  

starworld3:  
  
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level164startingcoordinates  : ld iy,level41animationadress : ld hl,objectliststarworld3 : ld bc,lenghtobjectliststarworld3 : ld a,158 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

    jp gameloop     

.secret:

    ld a,(levelavailabilitylist+7)
    set 7,a
    ld (levelavailabilitylist+7),a
    ld a,(levelavailabilitylist+6)
    set 0,a
    set 1,a
    ld (levelavailabilitylist+6),a
    
  ld a,78
  ld (makelevelavailable?),a  

    jp gameloop  
  

starworld4:  
  
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level164startingcoordinates  : ld iy,level41animationadress : ld hl,objectliststarworld4 : ld bc,lenghtobjectliststarworld4 : ld a,159 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

    jp gameloop     

.secret:

    ld a,(levelavailabilitylist+9)
    set 2,a
    ld (levelavailabilitylist+9),a
    
  ld a,79
  ld (makelevelavailable?),a  

    jp gameloop  

starworld5:  
  
  xor a
  ld (nocontrol),a
  ld a,marioathsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level142startingcoordinates  : ld iy,level41animationadress : ld hl,objectliststarworld5 : ld bc,lenghtobjectliststarworld5 : ld a,160 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 5
  jp z,.secret

    jp gameloop     

.secret:

    ld a,(levelavailabilitylist+10)
    bit 7,a
    jp z,gameloop
    bit 6,a
    jp z,gameloop
    bit 5,a
    jp z,gameloop ;only unlock when all other starworlds are finished
    bit 4,a
    jp z,gameloop

  ld a,80
  ld (makelevelavailable?),a  

    jp gameloop 

    
    
bowsercas1: 


  ld a,400/2
  ld (time),a

.castlecutscene1:  


  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,153 : call	levelloader

  
  ld a,71
  ld (mainid),a
  call fetchmapuserdata ;fetch the mapdata load mainid by hand
  ld a,(middlereached)
  and a
  jp nz,.loopmap005  
  
  
    jp .loopmap001
  
.loopmap005special
  
  ld a,400/2
  ld (time),a

  xor a
  ld (nocontrol),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level160startingcoordinates  : ld iy,animate0 : ld hl,cutscenefortress : ld bc,lenghtcutscenefortress : ld a,153 : call	levelloader  
  
    jp .loopmap005
  
  
 ; jp .loopmap012
;1a
.loopmap001:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level42startingcoordinates  : ld iy,level10banimationadress : ld hl,objectlistlv078a : ld bc,lenghtobjectlistlv078a : ld a,162 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop    

  ld a,(exitx)
  cp 150
  jp nc,.loopmap007 
  cp 100
  jp nc,.loopmap011  
  cp 40
  jp nc,.loopmap009





;1b
.loopmap002:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level66startingcoordinates  : ld iy,level10banimationadress : ld hl,objectlistlv078b : ld bc,lenghtobjectlistlv078b : ld a,163 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
 ; cp 1
;  jp .loopmap003   

;1c
.loopmap003:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level42startingcoordinates  : ld iy,level10banimationadress : ld hl,objectlistlv078c : ld bc,lenghtobjectlistlv078c : ld a,164 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  cp 1
;  jp gameloop  

 ld a,(exitx)
  cp 150
  jp nc,.loopmap006 
  cp 100
  jp nc,.loopmap004  
  cp 40
  jp nc,.loopmap010
  
  jp .loopmap008

;1d
.loopmap004:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level50startingcoordinates  : ld iy,level10banimationadress : ld hl,objectlistlv078d : ld bc,lenghtobjectlistlv078d : ld a,165 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
;  jp .loopmap005   

;1e ;backdoor
.loopmap005:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level179startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv078e : ld bc,lenghtobjectlistlv078e : ld a,166 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap012  

;1f
.loopmap006:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level10startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv078f : ld bc,lenghtobjectlistlv078f : ld a,167 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap005 

;1g
.loopmap007:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level45startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv078g : ld bc,lenghtobjectlistlv078g : ld a,168 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap003  

;1h
.loopmap008:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level81startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv078h : ld bc,lenghtobjectlistlv078h : ld a,169 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap005 

;1i
.loopmap009:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level81startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv078i : ld bc,lenghtobjectlistlv078i : ld a,170 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap003  

;1j
.loopmap010:
  ld a,4
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level165startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv078j : ld bc,lenghtobjectlistlv078j : ld a,171 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap005 

;1k
.loopmap011:
  xor a
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,fortresssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level45startingcoordinates  : ld iy,level34animationadress : ld hl,objectlistlv078k : ld bc,lenghtobjectlistlv078k : ld a,172 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died
  ;cp 1
  jp .loopmap003  

;1l final boss!
.loopmap012:
  ;  ld a,1
  ;  ld (sfxactive),a ;TEMP DEBUG

  ld a,5
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld (time),a ;unlimited time to defeat this boss
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level81startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv078l : ld bc,lenghtobjectlistlv078l : ld a,173 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ld a,(finalscenecompleted)
  and a
  jp nz,gameloop ;boss has been defeated
 ; jp gameloop ;temp DEBUG for BOSS
  
 ;makelevelavailable trow cutscene 
 
.finalscene:  
   
 
 
  call clearlocalvariables ;very important or else other object will remain active in RAM!  
  ld a,9
  ld (cutscenenr),a
  call jumptocutscene ;final cutscene. credits screen
  call cutscenespecial ;transition works  
  call clearlocalvariables ;very important or else other object will remain active in RAM!  
  ld a,10
  ld (cutscenenr),a
  call jumptocutscene ;final cutscene. monster credits screen
  xor a
  ld (cutscenenr),a ;dirty hack  
  
  ld a,1
  ld (finalscenecompleted),a
  
  call enablesavemenu ;ask the user to save just for this once
  
  jp gameloop
  

  
cutscenespecial:
  xor a ;block all movement let the cutscene helper handle this
  ld (nocontrol),a
  ld a,finalsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level15startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv015s : ld bc,lenghtobjectlistlv015s : ld a,00 : call	levelloader
  ret   
  

  
  
;special world
special1:

  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level174startingcoordinates  : ld iy,animate0 : ld hl,objectlistspecial1 : ld bc,lenghtobjectlistspecial1 : ld a,174 : call	levelloader
  ld a,(nocontrol)  
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap002
  
  jp gameloop  

.loopmap002:
  ld a,1
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level175startingcoordinates  : ld iy,animate0 : ld hl,objectlistspecial1b : ld bc,lenghtobjectlistspecial1b : ld a,175 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

.loopmap003:
  
  ld a,1
  ld (nocontrol),a
  ld (tubeupordown),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level174startingcoordinates2  : ld iy,animate0 : ld hl,objectlistspecial1 : ld bc,lenghtobjectlistspecial1 : ld a,174 : call	levelloader
  ld a,(nocontrol)  
  cp 2
  jp z,gameloop
  cp 1
  jp z,.loopmap002
  
  jp gameloop
 
 
special2:

  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level164startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistspecial2 : ld bc,lenghtobjectlistspecial2 : ld a,176 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  cp 5
  jp z,.loopmap001 ;o yeah
  
  jp gameloop  
 
 
.loopmap001: 
   
  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level110startingcoordinates  : ld iy,animate0 : ld hl,objectlistspecial2b : ld bc,lenghtobjectlistspecial2b : ld a,179 : call	levelloader
   
   
   
  jp special2
 
 
 
special3:
      ;  jp .loopmap001

  xor a
  ld (nocontrol),a
  ld a,overworldsongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level76startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistspecial3 : ld bc,lenghtobjectlistspecial3 : ld a,177 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop
  
 ; jp gameloop  

.loopmap001:
  xor a
  ld (nocontrol),a
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level178startingcoordinates  : ld iy,animate0 : ld hl,objectlistspecial3b : ld bc,lenghtobjectlistspecial3b : ld a,178 : call	levelloader
  ld a,(nocontrol)
  cp 2
  jp z,gameloop ;died

  ;don't forget the jump back to the right place on the worldmap
  ld a,36
  ld (makelevelavailable?),a   
  
  jp gameloop  
  
  
  
testmap:


    ld a,1
    ld (sfxactive),a ;TEMP DEBUG

  xor a
  ld (nocontrol),a
  ld a,cavesongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level25startingcoordinates  : ld iy,animate0 : ld hl,objectlisttestmap : ld bc,lenghtobjectlisttestmap : ld a,101 : call	levelloader
  ld a,(nocontrol)
  ;  cp 5
  ;  jp z,bowsercas1.loopmap012
  

  ld a,5
  ld (nocontrol),a
  xor a
  ld (tubeupordown),a
  ld (time),a ;unlimited time to defeat this boss
  ld a,bosssongblock : ld (currentmusicblock),a : ld de,0 : ld ix,level81startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv078l : ld bc,lenghtobjectlistlv078l : ld a,173 : call	levelloader
  ld a,(nocontrol)
 ; cp 2
 ; jp z,gameloop ;died  
  
  

    jp testmap


demolevel:
.loopmap018:
  xor a
  ld (nocontrol),a
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level18startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv018 : ld bc,lenghtobjectlistlv018 : ld a,18 : call	levelloader

    


;nocontrol
; 1 = mario exits the level through a vertical tube. This can be both sides
; 2 = mario dies
; 3 = mario has finished the level
; 4 = mario exits the level through a horizontal tube
; 5 = mario exits through a door
; 6 = mario killed a boss. Jump into some sort of video thingy or special mode
	jp		loader



takelife:

  xor a
  ld (powerreserve),a ;clear powerreserve

  ld a,(time)
  dec a
  push af
    call clearlocalvariables
  pop af
  ld a,1
  ld (cutscenenr),a
  call z,jumptocutscene

  ld a,(lives)
  dec a
  ld (lives),a
  ret nz

  call clearlocalvariables

  xor a
  ld (cutscenenr),a
  call jumptocutscene

;jump out of the call and return to complete start
  inc sp
  inc sp

  xor a ;reset entire game block
  call block12
  
  jp startgame

 ; ret



levelloader:                                            ;in a->level to load, in ix->startingcoordinates for mappointer and mario

  ld    (currentlevel),a


;save hl and bc  
  ld (hlvar),hl
  ld (bcvar),bc
  ld (devar),de

  call  copymapdatatopage0

  call  clearlocalvariables
  call  copyobjectslist                                 ;copy the objectslist into the eventhandler
  
  push hl
  push de
  call	setmapdata					;fetch any special variables from the mapdata and apply them to the local vars  
  call  fetchmapuserdata                                ;get any special data that was written for this map
  pop de
  pop hl
  call  startingcoordinates                             ;set startingcoordinates for mappointer and mario
 
    ld ix,mapdata
 
;  call  loadmapdatapositionin_ix
	call	loadgrapx                                       ;in -> mapdatapositionin_ix
  call  loadmap                                         ;in -> mapdatapositionin_ix
  
  call  initcamera                                      ;initieer de camera op een vaste positie rondom mario
  call  writetilechecktable                             ;schrijf de tilechecktable van te voren

  call  setyoshicoins       ;remove any yoshicoins we find in the objectslist that match the saved table

  call  setdarky
  
  call  enableInterrupts                                ;enable vblankint and lintint and set lineint height
  
  call  initmusic_and_goEngine 
  
  ld a,(nocontrol)
  cp 20
  jp z,.gameloop ;catch return by pauze button
  
  ld a,(noyoshi+1)
  and a
  ret z
  ld a,(noyoshi)
  ld (marioyoshi),a

  ret


.gameloop:
    
    xor a
    ld (nocontrol),a
    
    inc sp
    inc sp
    
  ld a,(noyoshi+1)
  and a
  jp z,gameloop
  ld a,(noyoshi)
  ld (marioyoshi),a

  jp gameloop    
    
  
  
setdarky:

    ;only if darky is present
    ld a,(darky?)
    and a
    ret z
    
;route audio to the ePSG and thus utilize a alternative PSG player    
    ld hl,InterruptHandler.mainsfx
    ld de,MainSfxIntRoutined
    ld a,$CD
    ld (hl),a
    inc hl
    ld (hl),e
    inc hl
    ld (hl),d
 
    ;effects off
    ld hl,$3207 ;set line in + bypass on ;4 for line in
    call senddarkycommand
    
    
    ld a,(currentmusicblock)
    cp cavesongblock 
    ret nz
  
    ld hl,$3204 ;set line in + bypass off
    call senddarkycommand    
    


    ret

  ;we are out of romloader space so we need to find ourself a place to copy the mapdata to. We can use page0 because there is space over there of unused engine @ this moment
  ;meaning that if we copy the entire mapdata into that empty space we can load the data from there without having to do all kinds of loading and switching shit
  ;now the aproach is a little different. We only copy what we need to prevent trouble. Many loops are gone making maploading faster.
  
  
copymapdatatopage0:
    
    ld a,mapdatablock
    call block12 ;fetch mapdatablock
    
    
  ld    a,(currentlevel)
  ld    hl,mapromdata
  ld    de,lenghtmapdata
.loop:
  cp    (hl)
  jp   z,.done
  add   hl,de
  jp    .loop

  
.done:
    
;    ld hl,mapromdata
    ld bc,lenghtmapdata ;about 4Kb should be enough.
    ld de,engaddr ;destination
    
    ldir ;copy 16 bytes of mapdata to RAM @engaddr aka. mapdata:


    ret
    
    

enableInterrupts:
  di
  ld    hl,InterruptHandler ;set new normal interrupt
  ld    ($38+1),hl
  ld    a,$c3
  ld    ($38),a
 
  ld    a,(VDP_0)
  or    %0001'0000          ;set ei1 (enable lineints)
  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,0+128
;  ei
  out   ($99),a

  ld    a,lineinterruptheight   ;set lineinterrupt height
;  di
  out   ($99),a
  ld    a,19+128
  ei
  out   ($99),a
  ret


;  include "startingcoordinates.asm" ;moved to another block
;  include "mapdata.asm" ;mapdata has been moved to another adress


setmapdata:

  ld a,(currentlevel)
  cp 30
  call z,.setwater
  cp 53
  call z,.setwater  
  cp 66
  call z,.setwater  ;set in by hand this map has no alternate inxits  
  cp 67
  call z,.setwater  ;set in by hand this map has no alternate inxits
  cp 89
  call z,.setwater
  cp 135
  call z,.setwater 
  cp 136
  call z,.setwater 
  cp 171
  call z,.setwater
  
;get mapdata
  ld hl,mapdata


  inc hl
   ld a,(hl)
   ld (nosheer),a
   inc hl
   ld a,(hl)
   ld (noblock),a
   inc hl
   inc hl
   inc hl
   inc hl
   inc hl ;extra
   ld a,(hl)
   ld (mainid),a
   inc hl
   inc hl
   ld a,(hl)
   dec a
   jp z,.dissallowyoshi
   
    xor a
    ld (noyoshi+1),a ;store the current value   
    

   ret

.dissallowyoshi:
  
    ld a,(marioyoshi)
    ld (noyoshi),a ;dissallow yoshi store the current value 
    xor a
    ld (marioyoshi),a

    ld a,1
    ld (noyoshi+1),a


    ret

    
    
.setwater:
  
  push af
  ld a,1
  ld (waterlevel),a
  pop af
  ret


fetchmapuserdata:

  ld a,(demo)
  and a
  ret nz

  ld hl,mapdatatable
  ld de,mapdatatablelenght

.loop:

  ld a,(hl)
  cp 255
  ret z
  ld b,a
  ld a,(mainid)
  cp b
  jp z,.setvariables
 
  add hl,de

  
;  inc hl
;  inc hl ;add tablelenght

  jp .loop


 ; ret

.setvariables:

  inc hl
  ld a,(hl)
  ld (moonpoweruptaken),a
  inc hl
  ld a,(hl)
  ld (completed),a
  inc hl
  ld a,(hl)
  ld (middlereached),a
  inc hl
  ld a,(hl)
  ld (yoshicoinscolected),a

  ret



;animation code was here now moved to animationcode.asm

setblock2:

  ld a,objectslistblock2 ;AA
  call  block12                                        ;set block in page 1 ($4000 - $8000)


  ret


copyobjectslist:


  ld		a,(slot.page12rom)	                                ;all RAM except page 1 and 2
	out		($a8),a	

;Problem!: objectslist exceeds 16 KB!!!

  ld a,objectslistblock ;2c
  call  block12                                        ;set block in page 1 ($4000 - $8000)


;get mapdata and check in which block we must fetch the objectslist
;  ld hl,mapdata
  ld a,(currentlevel)
  cp 89
  call nc,setblock2
  
;  ;reset the objectslistpointer
;  ld    hl,levelObjectList+lenghtlevelObjectList
;  ld    (levelObjectListpointer),hl

  ld hl,(hlvar)
  ld bc,(bcvar)

  ld de,levelObjectList
  
  ldir

;then copy the bkgtileslist if any
  ld hl,(devar)
;  xor a
;  ld de,0
;  sbc hl,de
  ld a,l
  or h
  jp z,copyspritehiderlist ;no list defined in the loader


  ld hl,(devar)
;  ld bc,maxbkgbuffer

  ld de,levelbackgroundtileslist
  
;  ldir

  ld b,bkgbufferrows

  exx
  ld hl,(bcvar)
  exx
  
.copyloop:
  push bc
  ld bc,bkgbuffercollumns
  ldir
  pop bc

  exx
  ld de,bkgbuffercollumns
  add hl,de
  exx
  
  
  ld a,(hl)
  and a
  jp z,copyspritehiderlistspecial
  djnz .copyloop

copyspritehiderlistspecial:

    exx
    ld de,6
    add hl,de
    ld (bcvar),hl
    exx

  

;copy the objecthiderlist to RAM
;check mapflag for spritehider
copyspritehiderlist:

;get mapdata
  ld hl,mapdata
  
   ld de,10
   add hl,de ;find entry
   
   ld a,(hl)
   and a
   jp z,.nospritehider ;flag is set to no
 
;    ld a,1
    ld (spritehideractive),a ;set active

  ld hl,(hlvar) ;location of levelobjectslist
  ld bc,(bcvar) ;lenght.

  add hl,bc ;now we are at the start of the table
  push hl 
  ;now get the size of the table
  
  ld bc,0
.getsizeloop:
  
    ld a,(hl)
    ld e,a
    inc hl
    ld a,(hl)
    or e
    jp z,.sizeobtained
    
    inc hl
    inc hl
    
    inc bc
    inc bc
    inc bc
    
    jp .getsizeloop
  
  
.sizeobtained:

  pop hl
  ld de,spritehidertable
  
  ldir ;copy to sprithidertable in ram and return
 

  ret


.nospritehider:

;  xor a
  ld (spritehideractive),a ;spritehider is not active
  ret
  

clearlocalvariables:

  xor a
  ld hl,localvars
  ld de,localvars+1
  ld bc,endlocalvars-1
  ld (hl),a
  ldir

;  ld bc,localvars
;  ld hl,endlocalvars
;  ld de,0
;  xor a
;  .clearlocalvars:
;  ld (bc),a
;  inc bc
;  dec hl
;  xor a
;  sbc hl,de
;  jp nz,.clearlocalvars

;mario must always look to the right
  ld a,1
  ld (marioleftrightspr),a
  ld (marioleftright),a


  ;clear the entire spat
  ld bc,fulllengthemptyspat
  ld de,spat
  ld hl,emptyspat
  ldir
  
  ;clear the spritelist
  ld bc,32
  ld de,sprlst
  ld hl,emptysprlst
  ldir

  ret

loadmapdatapositionin_ix:
  ld    a,(currentlevel)
  ld    ix,mapdata
  ld    de,lenghtmapdata
.loop:
  cp    (ix)
  ret   z
  add   ix,de
  jp    .loop

  ret



loadgrapx:	                                              ;in -> mapdatapositionin_ix
  ld    b,(ix+mapdata.grpxblck)   
  ;ld    b,a
  ld    h,(ix+mapdata.grpxaddr+1)    
;  ld    h,a
  ld    l,(ix+mapdata.grpxaddr)    
;  ld    l,a


  ld    de,$4000
  call  unpackfrompage3to2                                ;hl->source, de->destiny, b->block, out-> slot.page12rom, loaderblock in block 3 ($8000)

  ld		a,(slot.page2rom)	                                ;all RAM except page 2
	out		($a8),a
	

;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    4     4    0   0  	A16 A15 A14 A13 0   0       ;if you set bit 1+0 to 0 you enable mirroring
;set character table    %0000'0000 = character table at $0000

  ld    a,%0000'0000    ;character table at $0000
	ld		(VDP_0+4),a  
	di
	out		($99),a		
	ld		a,4+128
  ei
	out		($99),a

;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    3     3    A13 0  	0   1   1   1   1   1       ;if you set bit 6+5 to 0 you enable mirroring
;    10    10   0   0  	0   0   0   A16 A15 A14
;set character table    %0000'0000 = character table at $0000

	ld		a,%1001'1111    ;color table at $2000
	ld		(VDP_0+3),a  
	di
	out		($99),a		
	ld		a,3+128
  ei
	out		($99),a

	ld		a,%0000'0000    ;color table at $2000 
	di
	out		($99),a		
	ld		a,10+128
  ei
	out		($99),a

;write the character and color tables 4x to Vram
  ;1st character pattern table at ($0000)
	xor		a
	ld		hl,$0000                                          ;character pattern table address in Vram
	call	SetVdp_Write
	ld		hl,pattaddre	                                    ;character pattern table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes

  ;2nd character pattern table at ($4000)
	xor		a
	ld		hl,$4000		                                      ;character pattern table address in Vram
	call	SetVdp_Write
	ld		hl,pattaddre	                                    ;character pattern table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes

  ;3d character pattern table at ($8000)
	xor		a
	ld		hl,$8000		                                      ;character pattern table address in Vram
	call	SetVdp_Write
	ld		hl,pattaddre	                                    ;character pattern table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes

  ;4th character pattern table at ($c000)
	xor		a
	ld		hl,$c000		                                      ;character pattern table address in Vram
	call	SetVdp_Write
	ld		hl,pattaddre	                                    ;character pattern table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes	

  ;1st color table at ($2000)
	xor		a
	ld		hl,$2000                                          ;color table address in Vram
	call	SetVdp_Write
	ld		hl,coloraddre	                                    ;color table table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes

  ;1st color table at ($6000)
	xor		a
	ld		hl,$6000                                          ;color table address in Vram
	call	SetVdp_Write
	ld		hl,coloraddre	                                    ;color table table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes	

  ;1st color table at ($a000)
	xor		a
	ld		hl,$a000                                          ;color table address in Vram
	call	SetVdp_Write
	ld		hl,coloraddre	                                    ;color table table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes

  ;1st color table at ($e000)
	xor		a
	ld		hl,$e000                                          ;color table address in Vram
	call	SetVdp_Write
	ld		hl,coloraddre	                                    ;color table table in Rom
	ld		bc,$0098
	call  outix2048                                         ;write 2048 bytes 20% faster
;	otir	otir	otir	otir	otir	otir	otir	otir          ;write 2048 bytes
;/write the character and color tables 4x to Vram


	ld		hl,paletaddre
	call	SetPalette
	ld		hl,paletaddre
  ld    de,currentpalette
  ld    bc,16*2
  ldir


  
  ld    hl,currentpalette
  ld    de,currentpaletteONEtintdarker
  ld    bc,16*2
  ldir
  
  ld    hl,currentpaletteONEtintdarker
  ld    b,1                               ;amount of steps to darken

  ld    c,b                               ;amount of steps to darken stored in c
;  ld    e,16                              ;16 colors
  

  
;.loop:
;  call  .darkencolor
;  dec   e
;  jp    nz,.loop
  

  
.darkencolor:
;first make blue darker
  ld    a,(hl)
  and   %0000'1111                        ;blue
  jp    z,.endDarkenBlue                  ;there is no blue at all in this palletecolor
.darkenblueloop:
  dec   a
  jp    z,.endDarkenBlue                  ;there is no more blue in this palletecolor
  djnz  .darkenblueloop
.endDarkenBlue:  
  ld    d,a                               ;store the darker blue in d

  ld    b,c                               ;amount of steps to darken
;then make red darker
  ld    a,(hl)
  and   %1111'0000                        ;red
  jp    z,.endDarkenRed                   ;there is no red at all in this palletecolor
.darkenredloop:
  sub   a,16
  jp    z,.endDarkenRed                   ;there is no more red in this palletecolor
  djnz  .darkenredloop
.endDarkenRed:  
;then add blue and red together again
  or    d
  ld    (hl),a
  
  inc   hl                                ;green
  ld    b,c                               ;amount of steps to darken
;and finally make green darker
  ld    a,(hl)
  and   %0000'1111                        ;green
  jp    z,.endDarkenGreen                 ;there is no green at all in this palletecolor
.darkengreenloop:
  dec   a
  jp    z,.endDarkenGreen                 ;there is no more green in this palletecolor
  djnz  .darkengreenloop
.endDarkenGreen:  
  ld    (hl),a
  
  inc   hl                                ;next color
  ld    b,c                               ;amount of steps to darken

;write the coresponding animation to the vram
  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
  out   ($a8),a        

  ld    a,animationtilesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)
  ld    c,$98

  jp writeanimationtiles

 ; jp (iy)
    
;/write the animation tiles to each corresponding character and color table

;	ret


;set startingcoordinates for mappointer and mario
startingcoordinates:

  ld    a,startingcoordinatesblock
  call  block12                                        ;set block in page 1 ($4000 - $5fff)

  ld a,(currentlevel)
  ld l,a
  ld a,(middlereached)
  cp l
;  dec a
  call z,.incix

  ld hl,0
;  ld l,(ix+0)
;  ld h,(ix+1)
  ld (camerax),hl
;  ld l,(ix+2)
;  ld h,(ix+3)
  ld (cameray),hl
  
    
;  ld    a,(slot.page12rom)                                ;all RAM except page 1+2
;  out   ($a8),a    
  
  
  ld l,(ix+4-3)
  ld h,(ix+5-3)
  ld (marioy),hl
  ld l,(ix+6-3)
  ld h,(ix+7-3)
  ld (mariox),hl

 ; pop hl
                                      ;in hl->startingcoordinates for mappointer and mario
;  ld    de,cameray                                   ;de->  mappointer y, mappointer x, mario y, mario x
;  ld    bc,8
;  ldir


  ret
  
  
.incix:

  ld a,(currentlevel) ;shitty hack to prevent yoshi; s house to go apeshit
  and a
  ret z

  ld a,(ix) ;blocked because this is a alternate inxit
  dec a
  ret z

  
  ld de,lengthstartingcoordinates
  add ix,de

    ret
  
;/set startingcoordinates for camera and mario

loadmap:                                                  ;in -> mapdatapositionin_ix
;set backgroundmap lenght en height
  ld    l,(ix+mapdata.maplengt)
  ld    h,(ix+mapdata.maplengt+1)
  ld    (maplenght),hl                                    ;map lenght
  ld    de,32
  xor   a                                                 ;reset carry
  sbc   hl,de
  ld    (maplenghtmin32),hl                               ;map lenght - 32

  ld    l,(ix+mapdata.mapheight)                          ;map height
  ld    h,(ix+mapdata.mapheight+1)                          ;map height
  ld    (mapheight),hl
;  ld de,23
;  xor a
;	sbc hl,de
;	ld		(mapheightmin23),hl	
;/set backgroundmap lenght en height

;precalculate mapheight in px
  ld de,(mapheight)
;  ld e,a
;  ld d,0
  ;mapheight*8 to  make it match the coordinates
  sla e
  rl d ;de*2
  sla e
  rl d ;de*4
  sla e
  rl d ;de*8
  ld (mapheightx8),de
;/precalculate mapheight in px


;precalculate maplenght in px
  ld de,(maplenght)
  ;maplenght*8 to  make it match the coordinates
  sla e
  rl d ;de*2
  sla e
  rl d ;de*4
  sla e
  rl d ;de*8
  ld (maplenghtx8),de
;/precalculate maplenght in px


;depack the whole map (this must happen from page 3)

  call depackmapdata

;map is now in RAM get the levelengine back
;  ex de,hl
  

  ret



  
initcamera:


   ld    hl,(mariox)
  srl   h
  rr    l ;/2
  srl   h
  rr    l ;/4
  srl   h
  rr    l ;/8
   
  ld    de,16 ;camera ten opzichte van mario setten 16 is midden van beeld
  xor   a
  sbc   hl,de
  
  jp    c,.cameratotallyleft

;camera positie setten
  ld    (camerax),hl
  
;scroll register updaten  
  ld    a,(mariox)
  inc   a
  neg
  and   7
	ld		(VDP_8+21),a
  
  ;set camera for v9938
;  ld    a,(mariox)
;  and   7
  xor a
  ld    (VDP_8+10),a
  ;/set camera for v9938

  jp    .endsetcamerx

.cameratotallyleft:   ;mario is in the left part of the map, dont move camera, camerax = 0
  ld    hl,0
  ld    (camerax),hl
  ld    a,7
	ld		(VDP_8+21),a

  ;set camera for v9938
  xor   a
  ld    (VDP_8+10),a
  ;/set camera for v9938
  
.endsetcamerx:


;write the maximum Y value into register e this is an 8bit value stored in a word. First substract the mapheight with 4 then divide by 2
  ld hl,(mapheight)
  ld de,27
  xor a
  sbc hl,de
  ex de,hl

;set cameray
  ld    hl,(marioy)
  srl   h
  rr    l ;/2
  srl   h
  rr    l ;/4
  srl   h
  rr    l ;/8
  ld c,b
  ld b,0
  xor a
  sbc hl,bc

  ld    bc,0       ;unless mario is in the top part of the map, then cameray = 0
  jp    c,.CameraToporBottom

  push hl
  xor a
  sbc hl,de ;compare with value e
  pop hl
  ld    c,e
  jp    nc,.CameraToporBottom

  ld    (cameray),hl
  
  ld    a,(marioy)
  and   7
	ld		(VDP_8+15),a
  jp    .endsetcamery

.CameraToporBottom:
  ld    (cameray),bc
  xor   a         ;if mario is in bottom or top part of the map, then camery and vertical scroll register are fixed
	ld		(VDP_8+15),a
.endsetcamery:
;/set cameray



  ret  

outix2048:
  ld b,128
.loop:
  outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : 
  djnz .loop
  ret


;turbo r and MSX2+ switching routine. Checks if turbo-R is present and accepts A as input value 0 = Z80 1 = R800 mode.
;On turbo-R, uses the BIOS call that was copied to the mapper on boot to switch between R800 and Z80 mode
;On MSX2+, Simply outs the correct values to the clockchip which is attached to the switched ports $40 and $41
;in c, 0 = Z80, 1 = R800

switchcpu:

  ld a,(turbor?)
  cp 2
  jp z,.switchz80
  cp 3
  ret z
  and a
  ret z

 ;call cpu switch
  ld a,c ;bit0 
  xor 1 ;invert
  rrca ;bit7
  rrca ;bit6
  rrca ;bit5
  and %00100000 ;ditch any other data
  ld c,a
  
  call switchcpucode
  
  ret
             
             
.switchz80:

;support for msx2+
  di

;taken from map.grauw.nl
  LD	A,8
	OUT 	($40),A	;out the manufacturer code 8 (Panasonic) to I/O port 40h
	ld a,c
	xor %00000001 ;invert value
	ei
	OUT	($41),A	;and the mode changes to high-speed clock



    ret


clearyoshicointable:
  
  xor a
  ld (yoshicoin),a
  ld hl,yoshicoinpickuptable
  ld de,yoshicoinpickuptable+1
  ld bc,9
  ld (hl),0
  ldir
  

  ret
  

setyoshicoins:
  
  
  ld ix,levelObjectList
  ld hl,100
  ld de,lenghtlevelObjectList
  ld bc,0 

.findloop:

   ld a,(ix+5)
   cp 55
   call z,.handledeletion
   xor a
   sbc hl,bc
   ret z ;end of loop
   
   dec hl
   
   add ix,de
   jp .findloop 


.handledeletion:

  exx ;switch to alternate register pair
  
  ld hl,yoshicoinpickuptable
  ld b,lenghtyoshicoinpickuptable/2
.loop:
  ld e,(hl)
  inc hl
  ld d,(hl)
  inc hl
  ex de,hl
  push bc
   ld c,(ix+1)
   ld b,(ix+2) ;16 x value of object in list
  xor a
  sbc hl,bc
  pop bc
  jp z,.delete
  ex de,hl
  djnz .loop
  
  exx
  
  ret
 
.delete:

  ld (ix),0 ;set available to 0
  ld (ix+3),0 ;set y coordinate to 0

  exx

  ret


removeallscorebarspritesfromvram:

  ;set vdp address to write at character table and write spritecharacter
  ld hl,$5400
	ld		a,1
	call	SetVdp_Write

  ld de,$600
.characterloop:
  xor a
  out ($98),a

  dec de
  ld a,d
  or e
  jp    nz,.characterloop

  ret

placeeventhandlery:
  ;Write Y based eventhandler jumps onto the eventhandler
  ;write jump to flip xy on eventhandler
  ld    hl,handleObjectlisty.loop
	ld    (handleObjectlist.loopy+1),hl
	ld    a,$c3               
	ld    (handleObjectlist.loopy),a
;second jumps
  ld    hl,adjustlevObjListpointery
	ld    (eventhandler.calleventhandlery+1),hl  
  ld    hl,setnewobjectsy
	ld    (eventhandler.calleventhandlery+4),hl
  ret
  
restoreeventhandlerx:

  ;restore the jumps and make sure the eventhandler turns back to normal
  ;write jump to flip xy on eventhandler
  ld    hl,0
	ld    (handleObjectlist.loopy+1),hl
	ld    a,0               
	ld    (handleObjectlist.loopy),a
;second jumps
  ld    hl,adjustlevObjListpointer
	ld    (eventhandler.calleventhandlery+1),hl  
  ld    hl,setnewobjects
	ld    (eventhandler.calleventhandlery+4),hl  
  ret

enablesavemenu:
    
    ld a,1
    ld (forcesavemenu),a
    
    ret
  

;include "sram.asm"
include "writetoflashrom.asm"
include "worldmapstart.asm" ;debug code for worldmap  

  
