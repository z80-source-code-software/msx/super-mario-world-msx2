
;it works in the following way. When mario catches a midpole the currentlevel number is being stored into the mapid
;This means that only the map that has the matching level number does a increase on the startingcoordinates
;The only moment where this may become a problem is when there are multiple inxits per maplevel
;to prevent accidental ix increase the first byte can be set blocking a middle increase

 
levelstartingcoordinates:
;yohsi 1
level00startingcoordinates:  db 0 : dw 344-24    : dw 192;yoshi 1a
level00startingcoordinatesm: db 0 : dw 344-24    : dw 310*8    ;1562 ;192   ;camera y, camera x, mario y, mario x ;the middle startingcoordinates MUST ALWAYS BE PUT BEHIND THE INITIAL ONE
level00startingcoordinates2: db 1 : dw 272-24    : dw 2096    ;1562 ;192   ;camera y, camera x, mario y, mario x
level01startingcoordinates:  db 0 : dw 144       : dw 028     ;yoshi 1b
;yoshi 2
level02startingcoordinates:  db 0 : dw 344+8     : dw 128;yoshi 2a
level02startingcoordinatesm: db 0 : dw 43*8      : dw 256*8;camera y, camera x, mario y, mario x 
level02startingcoordinates2: db 1 : dw 43*8      : dw 486*8+4;camera y, camera x, mario y, mario x
;yoshi 3
level04startingcoordinates:  db 0 : dw 320       : dw 028;yoshi 3a
level04startingcoordinatesm: db 0 : dw 34*8      : dw 271*8;280*8;028;camera y, camera x, mario y, mario x 320=y
level04startingcoordinates2: db 1 : dw 034*8     : dw 261*8+4;camera y, camera x, mario y, mario x 320=y
level05startingcoordinates:  db 1 : dw 014*8     : dw 003*8+4;yoshi 3b
;yoshi 4
level06startingcoordinates:  db 0 : dw 057*8     : dw 003*8;yoshi 4a
level06startingcoordinates2: db 1 : dw 061*8     : dw 162*8+4;camera y, camera x, mario y, mario x 320=y
level06startingcoordinates3: db 1 : dw 020*8     : dw 003*8;camera y, camera x, mario y, mario x 320=y
level08startingcoordinates:  db 1 : dw 059*8     : dw 004*8+4;yoshi 4c
;iggys castle
level09startingcoordinates:  db 0 : dw 026*8     : dw 004*8+4;iggy a
level09startingcoordinatesm: db 0 : dw 026*8     : dw 213*8+4;camera y, camera x, mario y, mario x 320=y
level10startingcoordinates:  db 1 : dw 024*8     : dw 004*8+4;iggy b
level11startingcoordinates:  db 1 : dw 013*8     : dw 005*8+4;iggy c (end)
;donut 1
level12startingcoordinates:  db 0 : dw 040*8     : dw 013*8+4;camera y, camera x, mario y, mario x 320=y ;donut 1a
level12startingcoordinatesm:  db 0 : dw 040*8     : dw 305*8+4;camera y, camera x, mario y, mario x 320=y
level12startingcoordinates2: db 1 : dw 034*8     : dw 384*8+4;donut 1a
level16startingcoordinates:  db 1 : dw 043*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;donut 1b
;tic tac toe
level17startingcoordinates:  db 0 : dw 046*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;tic tac toe
;yellow switch palace
level13startingcoordinates:  db 0 : dw 019*8     : dw 008*8+4;yellow switch palace, greenswitch palace
;green switch palace
level14startingcoordinates:  db 0 : dw 056*8     : dw 002*8+4;green switch palace/green b
;yoshis house
level15startingcoordinates:  db 0 : dw 020*8     : dw 002*8+4;yoshi's house
;demolevel
level18startingcoordinates:  db 0 : dw 015*8     : dw 005*8+4;demolevel
level18startingcoordinatesm:  db 0 : dw 015*8     : dw 005*8+4;demolevel
;donut 2
level19startingcoordinates:  db 0 : dw 015*8     : dw 003*8+4;donut 2a
level19startingcoordinatesm:  db 0 : dw 015*8     : dw 003*8+4;donut 2a
level20startingcoordinates:  db 0 : dw 039*8     : dw 003*8+4;donut 2b
level21startingcoordinates:  db 0 : dw 038*8     : dw 013*8+4;donut 02c
;donut ghost house
level22startingcoordinates:  db 0 : dw 051*8     : dw 013*8+4;donut ghosthouse
level22bstartingcoordinates: db 0 : dw 051*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y
level22cstartingcoordinates: db 0 : dw 048*8     : dw 046*8+4;camera y, camera x, mario y, mario x 320=y
level22dstartingcoordinates: db 0 : dw 051*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y
;ghost exit
level23startingcoordinates:  db 0 : dw 027*8     : dw 002*8+4;ghosthouse exit
;donut 3
level24startingcoordinates:  db 0 : dw 043*8     : dw 013*8+4;donut 3a
level24startingcoordinatesm: db 0 : dw 043*8     : dw 345*8+4;camera y, camera x, mario y, mario x 320=y
level24startingcoordinates2: db 1 : dw 034*8     : dw 532*8+4;camera y, camera x, mario y, mario x 320=y
;donut 4
level25startingcoordinates:  db 0 : dw 040*8     : dw 013*8+4;donut 4a
level25startingcoordinatesm:  db 0 : dw 040*8     : dw 013*8+4;donut 4a
level25startingcoordinates2: db 1 : dw 038*8     : dw 127*8+4;camera y, camera x, mario y, mario x 320=y
level25startingcoordinates3: db 1 : dw 029*8     : dw 259*8+4;camera y, level174startingcoordinatescamera x, mario y, mario x 320=y
;donut s1
level26startingcoordinates:  db 0 : dw 017*8     : dw 003*8+4;donut secret 1
level26startingcoordinates2: db 0 : dw 007*8     : dw 157*8+4;camera y, camera x, mario y, mario x 320=y
level27startingcoordinates:  db 0 : dw 150*8     : dw 003*8+4;secret 1b
;donut s2
level28startingcoordinates:  db 0 : dw 045*8     : dw 003*8+4;donut secret 2a
level28startingcoordinates2: db 0 : dw 024*8     : dw 354*8+4;camera y, camera x, mario y, mario x 320=y
level29startingcoordinates:  db 0 : dw 040*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret 2b
;donut secret ghost house
level30startingcoordinates:  db 0 : dw 044*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house a
level30startingcoordinates2: db 0 : dw 044*8     : dw 160*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house a
level31startingcoordinates:  db 0 : dw 044*8     : dw 145*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house b
level31startingcoordinates2: db 0 : dw 034*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house b
level32startingcoordinates:  db 0 : dw 028*8     : dw 048*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house c
level33startingcoordinates:  db 0 : dw 017*8     : dw 015*8+4;camera y, camera x, mario y, mario x 320=y ;donut secret house d
;mortons castle
level34startingcoordinates:  db 0 : dw 043*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;donut castle a
level35startingcoordinates:  db 0 : dw 040*8     : dw 081*8+4;camera y, camera x, mario y, mario x 320=y ;donut castle b
level36startingcoordinates:  db 0 : dw 235*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;donut castle c
level37startingcoordinates:  db 0 : dw 013*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;donut castle d
;green switch palace
level38startingcoordinates:  db 0 : dw 003*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;green switch palace/blue switch palace
;donut 4
level39startingcoordinates:  db 0 : dw 041*8     : dw 010*8+4;camera y, camera x, mario y, mario x 320=y ;doncut plain 4 b
level40startingcoordinates:  db 0 : dw 018*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;doncut plain 4 c
;top secret area
level42startingcoordinates:  db 0 : dw 021*8     : dw 016*8+4;camera y, camera x, mario y, mario x 320=y ;top secret area
;vanilla 1
level43startingcoordinates:  db 0 : dw 025*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 1a
level44startingcoordinates:  db 1 : dw 029*8     : dw 007*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 1b
;level44startingcoordinates:  db 0| dw 034*8     | dw 320*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 1b ;debug coordinates
level45startingcoordinates:  db 0 : dw 038*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 1c
;vanilla 3
;level46startingcoordinates:  db 0| dw 008*8     | dw 300*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3a ;debug coordinates
level46startingcoordinates:  db 0 : dw 034*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3a
level46startingcoordinates2:  db 1 : dw 044*8     : dw 499*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3a
;level47startingcoordinates:  db 0| dw 007*8     | dw 335*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3b ;debug coordinates
level47startingcoordinates:  db 0 : dw 041*8     : dw 001*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3b
level47cstartingcoordinates:  db 0 : dw 019*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3c
;vanilla 2
;level48startingcoordinates:  db 0 : dw 006*8     : dw 348*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2a ;debug!
level48startingcoordinates:  db 0 : dw 025*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2a
level48startingcoordinatesm: db 0 : dw 006*8     : dw 348*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2a
level48startingcoordinates2: db 1 : dw 040*8     : dw 515*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2a
;level48startingcoordinates:  db 0| dw 009*8     | dw 430*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2a ;debug coordinates
level49startingcoordinates:  db 0 : dw 017*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 2b
;vanilla 4
level50startingcoordinates:  db 0 : dw 025*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 4a
level50startingcoordinatesm:  db 0 : dw 041*8     : dw 314*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 4a
level50startingcoordinates2:    db 1 : dw 040*8     : dw 497*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 4a
level51startingcoordinates:  db 0 : dw 037*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 4b
;vanilla 5
level52startingcoordinates:  db 0 : dw 043*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5a
level52startingcoordinates2: db 0 : dw 042*8     : dw 178*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5a
level53startingcoordinates:  db 0 : dw 043*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5b
;vanilla 6
level54startingcoordinates:  db 0 : dw 210*8     : dw 016*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 6a
level55startingcoordinates:  db 0 : dw 044*8     : dw 016*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 6b
level56startingcoordinates:  db 0 : dw 044*8     : dw 016*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 6c
;vanilla 7
level57startingcoordinates:  db 0 : dw 042*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 7a
;level57startingcoordinates:  db 0| dw 028*8     | dw 395*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 7a ;debug coordinates
level57startingcoordinatesm:  db 0 : dw 042*8     : dw 311*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 7a
level57startingcoordinates2:  db 1 : dw 028*8     : dw 544*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 7a
level58startingcoordinates:  db 0 : dw 027*8     : dw 131*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 7b
;vanilla 8
level59startingcoordinates:  db 0 : dw 038*8     : dw 007*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 8a
;level59startingcoordinates:  db 0| dw 038*8     | dw 490*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 8a ;debug coordinates
level60startingcoordinates:  db 0 : dw 034*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 8b
;vanilla fortress
level61startingcoordinates:  db 0 : dw 020*8     : dw 011*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla fortress a
level62startingcoordinates:  db 0 : dw 037*8     : dw 020*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla fortress b
level62startingcoordinates2: db 0 : dw 037*8     : dw 146*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla fortress b
level63startingcoordinates:  db 0 : dw 018*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla fortress c
;lemmys castle
level64startingcoordinates:  db 1 : dw 028*8     : dw 014*8+4;camera y, camera x, mario y, mario x 320=y ;lemmys castle a
level65startingcoordinates:  db 0 : dw 017*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;lemmys castle c
level66startingcoordinates:  db 1 : dw 023*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;lemmys castle b
level67startingcoordinates:  db 0 : dw 013*8     : dw 012*8+4;camera y, camera x, mario y, mario x 320=y ;lemmys castle d (shares with wendy's castle c)
;butter bridge 1 (shares with butterbridge 2)
level68startingcoordinates:  db 0 : dw 038*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;butter bridge 1a
level68startingcoordinatesm: db 0 : dw 038*8     : dw 280*8+4;camera y, camera x, mario y, mario x 320=y ;butter bridge 1a
level68startingcoordinates2: db 1 : dw 038*8     : dw 432*8+4;camera y, camera x, mario y, mario x 320=y ;butter bridge 1a
level69startingcoordinates:  db 0 : dw 034*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;butter bridge 1b
;butter bridge 2
level70startingcoordinates:  db 0 : dw 013*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;butter bridge 2b
;cheesebridge 3
level71startingcoordinates:  db 0 : dw 035*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;cheesebridge 3a
level71startingcoordinatesm: db 0 : dw 035*8     : dw 304*8+4;camera y, camera x, mario y, mario x 320=y ;cheesebridge 3a
level71startingcoordinates2: db 1 : dw 034*8     : dw 444*8+4;camera y, camera x, mario y, mario x 320=y ;cheesebridge 3a
level72startingcoordinates:  db 0 : dw 052*8     : dw 013*8+4;camera y, camera x, mario y, mario x 320=y ;cheesebridge 3b
level73startingcoordinates:  db 0 : dw 037*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;cheesebridge 3c
;cookiemountain 4
level74startingcoordinates:  db 0 : dw 038*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;cookiemountain 4a
level74startingcoordinatesm: db 0 : dw 038*8     : dw 351*8+4;camera y, camera x, mario y, mario x 320=y ;cookiemountain 4a
level74startingcoordinates2: db 1 : dw 005*8     : dw 177*8+4;camera y, camera x, mario y, mario x 320=y ;cookiemountain 4a
level75startingcoordinates:  db 0 : dw 005*8     : dw 138*8+4;camera y, camera x, mario y, mario x 320=y ;cookiemountain 4b
;sodalake 5
level76startingcoordinates:  db 0 : dw 036*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;was ooit sodalake is nu special3
level76sstartingcoordinates: db 0 : dw 033*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;sodalake 5a
level77startingcoordinates:  db 0 : dw 042*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;sodalake 5b
;ludwig 6
level78startingcoordinates:  db 0 : dw 037*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;ludwig 6a
level79startingcoordinates:  db 0 : dw 018*8     : dw 301*8+4;camera y, camera x, mario y, mario x 320=y ;ludwig 6b
level80startingcoordinates:  db 0 : dw 142*8     : dw 018*8+4;camera y, camera x, mario y, mario x 320=y ;ludwig 6c
;illusion1
level81startingcoordinates:  db 0 : dw 024*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion1
level81startingcoordinatesm: db 0 : dw 023*8     : dw 290*8+4;camera y, camera x, mario y, mario x 320=y ;illusion1
;illusion2
level82startingcoordinates:  db 0 : dw 037*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion2a
;illusion3
level83startingcoordinates:  db 0 : dw 024*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3a
;level83startingcoordinates:  db 0 : dw 024*8     : dw 500*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3a ;debug
level83startingcoordinatesm: db 0 : dw 023*8     : dw 293*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3a
level83startingcoordinates2: db 1 : dw 019*8     : dw 132*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3a
level83startingcoordinates3: db 1 : dw 019*8     : dw 587*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3a
level84startingcoordinates:  db 0 : dw 011*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion3b
;illusion4
level85startingcoordinates:  db 0 : dw 042*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4a
level85startingcoordinatesm: db 0 : dw 028*8     : dw 314*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4a
level85startingcoordinates2: db 1 : dw 039*8     : dw 285*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4a
;level85startingcoordinates3: db 1| dw 023*8     | dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4a
level86startingcoordinates:  db 0 : dw 015*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4b
level87startingcoordinates:  db 0 : dw 037*8     : dw 099*8+4;camera y, camera x, mario y, mario x 320=y ;illusion4c
;illusion ghost house
level88startingcoordinates:  db 0 : dw 031*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion5a
;level88startingcoordinates:  db 0| dw 025*8     | dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion5a ;debug
level88startingcoordinates2: db 1 : dw 037*8     : dw 241*8+4;camera y, camera x, mario y, mario x 320=y ;illusion5a
level88startingcoordinates3: db 1 : dw 031*8     : dw 230*8+4;camera y, camera x, mario y, mario x 320=y ;illusion5a
level89startingcoordinates:  db 0 : dw 044*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion5b
;illusion secret
level90startingcoordinates:  db 0 : dw 023*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion6a
;forest fortress
level91startingcoordinates:  db 0 : dw 044*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion7a
level92startingcoordinates:  db 0 : dw 044*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion7b
;roy's castle
level93startingcoordinates:  db 0 : dw 027*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;illusion8a
;chocolate1
level94startingcoordinates:  db 0 : dw 041*8     : dw 010*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate1a
level94startingcoordinatesm: db 0 : dw 041*8     : dw 319*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate1a
level94startingcoordinates2: db 1 : dw 027*8     : dw 230*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate1a
level94startingcoordinates3: db 1 : dw 030*8     : dw 530*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate1a
level95startingcoordinates:  db 1 : dw 033*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate1b
level96startingcoordinates:  db 1 : dw 044*8     : dw 006*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate2b to i
;chocolate3
level97startingcoordinates:  db 0 : dw 038*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate3a
level97startingcoordinatesm: db 0 : dw 040*8     : dw 310*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate3a
level97startingcoordinates2: db 1 : dw 031*8     : dw 461*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate3a
level98startingcoordinates:  db 1 : dw 016*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate3b
;chocolate4
level99startingcoordinates:   db 0 : dw 003*8     : dw 006*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate4a
level99startingcoordinates2:  db 1 : dw 010*8     : dw 349*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate4a
level100startingcoordinates:  db 0 : dw 028*8     : dw 014*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate4b
;chocolate5
level101startingcoordinates:  db 0 : dw 027*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate5a
level101startingcoordinatesm: db 0 : dw 032*8     : dw 342*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate5a
level101startingcoordinates2: db 1 : dw 035*8     : dw 335*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate5a
level102startingcoordinates:  db 1 : dw 041*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate5b
;chocolate6
level103startingcoordinates:  db 0 : dw 044*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate6a
level104startingcoordinates:  db 0 : dw 044*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate6b
;chocolate7 chocolate secret
level105startingcoordinates:  db 0 : dw 045*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7a
level105startingcoordinatesm: db 0 : dw 045*8     : dw 239*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7a
level105startingcoordinates2: db 1 : dw 028*8     : dw 210*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7a
level106startingcoordinates:  db 1 : dw 006*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7b
level107startingcoordinates:  db 1 : dw 046*8     : dw 009*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7c
level108startingcoordinates:  db 1 : dw 080*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7d
level109startingcoordinates:  db 1 : dw 044*8     : dw 017*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate7e
;chocolate8 chocolate fortress
level110startingcoordinates:  db 0 : dw 022*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate8a
level110startingcoordinatesm: db 0 : dw 022*8     : dw 249*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate8a
level111startingcoordinates:  db 1 : dw 006*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate8b
;chocolate9 wendys castle
level112startingcoordinates:  db 0 : dw 025*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate9a
level112startingcoordinatesm: db 0 : dw 025*8     : dw 365*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate9a
level113startingcoordinates:  db 1 : dw 026*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate9b

;bowserva1 sunken ghostship
level135startingcoordinates:  db 0 : dw 005*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva1
level136startingcoordinates:  db 0 : dw 005*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva1b
level137startingcoordinates:  db 0 : dw 017*8     : dw 030*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva1c

;bowserva2
;level96startingcoordinates:  db 0 : dw 044*8     : dw 006*8+4;camera y, camera x, mario y, mario x 320=y ;chocolate2b to i ;duplicate use
level138startingcoordinates2:  db 0 : dw 036*8     : dw 301*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva2a

;bowserva3
level139startingcoordinates:  db 0 : dw 041*8     : dw 010*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva3a
level139startingcoordinatesm: db 0 : dw 035*8     : dw 294*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva3a
level140startingcoordinates:  db 0 : dw 042*8     : dw 015*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva3b
level141startingcoordinates:  db 0 : dw 030*8     : dw 053*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva3c

;bowserva4
level142startingcoordinates:  db 0 : dw 043*8     : dw 004*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5a
level142startingcoordinatesm: db 0 : dw 043*8     : dw 298*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5a
level142startingcoordinates2: db 1 : dw 033*8     : dw 278*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 5a

;bowserva5
;level47startingcoordinates:  db 0 : dw 041*8     : dw 001*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 3b
level143startingcoordinates2:  db 0 : dw 017*8     : dw 043*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva5a
level144startingcoordinates:  db 0 : dw 025*8     : dw 121*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva5b


;bowserva6
;level45startingcoordinates:  db 0 : dw 038*8     : dw 002*8+4;camera y, camera x, mario y, mario x 320=y ;vanilla 1c
level145startingcoordinates:  db 0 : dw 037*8     : dw 114*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva6a
level146startingcoordinates:  db 0 : dw 042*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva6b
level146startingcoordinates2: db 0 : dw 042*8     : dw 133*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva6b

;bowserva7
;level65startingcoordinates:  db 0 : dw 017*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y ;lemmys castle c

;bowserva8
level148startingcoordinates:  db 0 : dw 044*8     : dw 012*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva8a
level148startingcoordinatesm: db 0 : dw 044*8     : dw 231*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva8a
level149startingcoordinates:  db 1 : dw 023*8     : dw 005*8+4;camera y, camera x, mario y, mario x 320=y ;bowserva8b
level150startingcoordinates:  db 1 : dw 013*8     : dw 005*8+4;bowserva8c (end)

;cutscene castle 1 et al.
level160startingcoordinates:  db 0 : dw 020*8     : dw 001*8+4;camera y, camera x, mario y, mario x 320=y
;introscene
level161startingcoordinates:  db 0 : dw 020*8     : dw 015*8+4;camera y, camera x, mario y, mario x 320=y

;starworld1
level162startingcoordinates:  db 0 : dw 027*8     : dw 014*8+4;camera y, camera x, mario y, mario x 320=y
;starworld2
level163startingcoordinates:  db 0 : dw 033*8     : dw 003*8+4;camera y, camera x, mario y, mario x 320=y
;starworld3
level164startingcoordinates:  db 0 : dw 044*8     : dw 014*8+4;camera y, camera x, mario y, mario x 320=y


level165startingcoordinates:  db 0 : dw 024*8     : dw 006*8+4;bowserscastle j

level174startingcoordinates:  db 0 : dw 048*8     : dw 009*8+4;special 1a
level174startingcoordinates2: db 1 : dw 047*8     : dw 292*8+4;special 1a 2
level175startingcoordinates:  db 0 : dw 000*8     : dw 009*8+4;special 1b
level178startingcoordinates:  db 0 : dw 014*8     : dw 002*8+4;special 3b

;midwaypoint bowsers castle
level179startingcoordinates:   db 0 : dw 029*8     : dw 007*8+4;camera y, camera x, mario y, mario x 320=y ;bowsercas1e
level179startingcoordinatesm:  db 1 : dw 026*8     : dw 218*8+4;camera y, camera x, mario y, mario x 320=y ;bowsercas1e (midpole)


lengthstartingcoordinates:  equ 5
 
