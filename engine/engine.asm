LevelEngine:


;di
;ld a,15     ;this routine changes backdrop color
;out ($99),a
;ld a,7+128
;ei
;out ($99),a	
    
  ld a,(holdscreen)
  and a
  call nz,wait

;framecountertje voor ons gemak
  ld    a,(framecounter)
  inc   a
  ld    (framecounter),a

 
  
  
  call  setnewlineintheight
  call  setscreenoffset
  call  swapnametable 
  call  AnimateTiles   	
  
  call  swap_spat_col_and_char_table 
 
 
 
;==============[ROM program area]=======================	 
  
  ld a,(slot.page12rom)
  out ($a8),a
  ld a,romprogblock2
  call block12

  
;======================================================= do this from ROM
scorebaradress:
  call handlescorebar ;This is just to see how it works out. We move this one to the ROM area once it works
  call setspritesplit
  call handlepowerreserve ;move to rom area as well.
;========================================================   



;als ik putmariosprite na mariophysics zet gaat mario een beetje hobbelen wanneer je rent
readcontrolsadress:
  call	ReadControls ;deze is een grote verbetering keyread en responses zijn zeer porno geworden  
;  call recorddemo
  ld a,(demo)
  dec a
  call z,playdemo
 

  ld a,(timecounter)
  inc a
  ld (timecounter),a
  cp 30
  call nc,addsecond
  
  
  
;stompcounter for sfxengine
  ld a,(stompcounter)
  dec a
  and a
  call z,resetstompcount
  jp z,.nostompcounter
  ld (stompcounter),a
.nostompcounter:

;idlecounter that checks if mario is actually doing something
  ld a,(Controls)
  and %01111111 ;bit 7 is unused
  call nz,resetidlecount
  
  ld hl,(idlecounter)
  ld de,0
  xor a
  sbc hl,de
  jp z,.noidlecount
  dec hl
  ld (idlecounter),hl
.noidlecount:


;count when mario is hit
  ld a,(mariogod)
  and a
  call nz,mariohittimeout	

       
  ld    a,romprogblock
  call  block34  
  
 
  
  
.disablemariofun:
	call  mariofun ;If you are idling too long. This small movie is played ;)
	call  mariophysics            ;De hele physics, zwaartekracht, vliegen, springen, tiledetectie, etc
	call  mariosprite             ;zorg dat sprite veranderd aan de hand van de physics
	call  mariotransform ;de transform routine die constant mario transformt wanneer nodig
		
		
	
;	ld    a,cheatblock
;  call  block34	
;  call  cheat ;tijdelijk cheat power routine om dingetjes te kunnen testen 

  ;reset the player then set the new music block
  di
  ld a,(playwinmusic)
  and a
  call nz,switchmusicwin
  ei

  ;switch to pbox music
  di
  ld a,(switchmusicpforbox)
  and a
  call nz,switchmusicpbox
  ei

  ;switch music for mariostar
  di
  ld a,(switchmusicforstar)
  and a
  call nz,switchmusicstar
  ei  

  
;romprogblock 2       
  ld    a,romprogblock2
  call  block12
  
  
  call  handleyoshitongue

  call handlescorebarsprites
  
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;============[end of rom program area]==================
 

 

;  call  handleyoshitongue ;the yoshitongue that grabs the opponents
  call  handlepbox ;handlepboxtimer etc

 
  
  call  setbackground   ;build the screen
  call  fadeoutscreen ;fade out the screen on level exit
  
  
  call  putmariosprite          ;out color table to vram, character table to vram, spat to ram
;if the RAM gets full again the eventhandler can be moved to rom as well as long as activateobject remains in ram



  call  eventhandler            ;handles events such as enemies, pieces of background made of sprites, big moving objects etc
    
    
;revert back to ram after the eventhandler
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a	
	
	
;sometimes. we must do a extra screenrebuild after the eventhandler
  ld a,(forcesetbackground)
  and a
  call nz,buildscreen

	
  call  blockhandler            ;handles blocks that mario activated
  
  call  pollpauze ;pauze 
  call lockmariotoobject ;This prevents the floating bug on moving platforms  
  call  putspattovram           ;out spat to vram

  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock6
  call  block34

 
  call hidesprites

;  ld		a,(slot.ram)	;back to full RAM
;	out		($a8),a


  
  
;tweede vblank check 

  ld    a,1
  ld    hl,vblankflag
.checkflag:
  cp    (hl)
  jp    nc,.checkflag

  ld a,(levelend?)
  or a
  jp nz,endengineandrestorerom


  
  ld    (hl),0
  jp    LevelEngine


endengineandrestorerom:

;sfx is disabled so screw this
    ld a,(sfxactive)
    and a
    jp z,.end

    ld a,(Sfxon?)
    and a
    jp nz,endengineandrestorerom
    
    
    ld a,(darky?) ;exclude darky in case some custom inthandler or anything is in play
    and a
    jp z,.end
    ;darky second psg
    ld a,(Sfxon2?)
    and a
    jp nz,endengineandrestorerom

.end:    

  call Settempisr ;directly disable lineint and write the temp isr so that the transition to the worldmap or other call cannot be interrupted or scrambled by the lineint

  call disablescreen

;  di

  xor a
  ld (levelend?),a
  ld (scorebaractive),a
  ld (removebar),a
;  ld (powerreserve),a
  ld (showpowerup),a
  ld (showcoin),a
  ld (showyoshi),a
  ld (showx),a
  ld (showtime),a
    ;ld a,1
    ld (lineintheight),a ;test some special code may disrupt the lineint so reset this shizzle
  
  ld hl,(noyoshi)
  ld a,l
  or h
  call z,.resetspecialyoshi
  
  ;reset the scorebar or else bad things happen
  
  
  call writemapuserdata
  
;stop the music player or else the switching on the loader will cause a hang
;  xor a
;  ld (playing),a
 ;clear the spritehider table or else!!!! illusion3 will completely crack up
 ld hl,spritehidertable
 xor a
 ld (hl),a
 ld de,spritehidertable+1
 ld bc,60-1
 ldir
 
 
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a 
  
  ;stop the SCC player
        ld a,$3f
        ld ($9000),a
        xor      a
        ld      (SccSfxOn),a
        ld      (988Fh),a	; all channels inactive
  
  
  ;close the fmpak's output as well.
    call stopmusic

       
  ld    a,loaderblock
  call  block34 ;switch rom back to loader

  ret ;return to loader


.resetspecialyoshi:

    ld a,(marioyoshi)
    and a
    ret nz

    xor a
    ld (yoshitype),a
    ret
  
  
;wait a few frames before starting the physics en objectmovement but allow the eventhandler en buildscreen to build up the level screen
wait:

  dec a
  ld (holdscreen),a
  jp z,enablescreen

;  ld a,1
;  ld (setobjectscurrentscreen?),a


  ret



writemapuserdata:

  ld a,(nocontrol)
  cp 3
  call z,.setcompleted


    ld hl,mapdatatable
    ld de,mapdatatablelenght

.loop:

  ld a,(hl)
  cp 255
  ret z
  ld b,a
  ld a,(mainid)
  cp b
  jp z,.setvariables
  
  add hl,de
;  inc hl
;  inc hl ;add tablelenght

  jp .loop


 ; ret

.setvariables:


  inc hl
  ld a,(moonpoweruptaken)
  ld (hl),a
  inc hl
  ld a,(completed)
  ld (hl),a
  inc hl
  ld a,(middlereached)
  ld (hl),a
  inc hl  
 ; ld a,(yoshicointemp)
  ld a,(yoshicoinscolected)
  ld (hl),a


  ret

.setcompleted:

  ld a,1
  ld (completed),a
  ret

handlepbox:

;don't do this if there is no reason for doing this
  ld a,(bluepboxactive)
  and a
  ret z

  ld hl,(pboxcounter)
  ld de,1
  xor a
  sbc hl,de
  ld (pboxcounter),hl
  ret nz

  xor a
  ld (doublepboxactive),a ;disable this byte
  
  call restorecoin ;restore the coins
  call getbrownblocksback ;everything that wasn' t a coin is now read back from the pillarbkgbuffer	  

;restore the music ;and I DONT know why I have to switch blocks here!
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  ld a,(previousmusicblock)
  ld (currentmusicblock),a  
;  call switchmusic
  call startmusic


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


	
  ret

restoremusic:
;restore the music ;and I DONT know why I have to switch blocks here!
 ; ld    a,(slot.page12rom)                             ;all RAM except page 12
 ; out   ($a8),a        
 ; ld    a,romprogblock
 ; call  block12

  ld a,(previousmusicblockp)
  ld (currentmusicblock),a  
;  call switchmusic
  call startmusic


  ld a,(slot.page12rom)
  out ($a8),a
  ld a,romprogblock2
  call block12
  
 ; ld		a,(slot.ram)	;back to full RAM
	;out		($a8),a


	
  ret  
  
  
restoremusicafterpbox:

  ld a,(previousmusicblock)
  ld (currentmusicblock),a  
  call switchmusic
;  call startmusic

  jp setpage12tohandleobjectmovement

;  ret



resetidlecount:

  ld a,(funloop)
  and a
  call nz,.endallchangedvars

 
;  ld hl,(idlecounter)
;  ld de,0
;  xor a
;  sbc hl,de
;  call z,.endallchangedvars
 
 
  ld hl,1000
  ld (idlecounter),hl
  ret

.endallchangedvars:

  xor a
  ld (marioshell),a
  ld (mariolook),a
  ld (funloop),a
  ret


resetstompcount:

  push af

  xor a
  ld (stompcount),a

  pop af

  ret


activateobjectfrompagenul:

  call activateobject

  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block34


  ret


;include "handleyoshitongue.asm"

fakeobjectslist:
;active?,lobjlad,empty,y,x,sprlstadd,movepat,amountspr,updatespr?,coloraddress,spataddrss,v1,v2,v3,v4,   offsets (these are the y,x offsets per objectsprite)                                                        ,spritecolROM,spritecharROM,sizeX,sizeY,deadly,clipping,hitpoints,objectrelease,deathtimer,objectfp,sheertile,killshell,objectreturn
  db 0  : dw   0,  0  ,0,0,     0,     00  : db  0    ,    0   : dw     0,       0   : db 00,00,00,00 : dw 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000, 000,000,     000    ,   000      : db 04,   00,    00,   00,       00,       00,           00,       00,       00,       00,       00, 00  



;=======================================[begin interrupt handler]===================================

InterruptHandler:

 
  push  af
     
  
  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,.Lineint        ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,vblank          ;vblank detected, so jp to that routine

  
  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret
  
  
.Lineint: 
 

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix

  ld a,(lineintheight)
  and a
  jp z,.nosplit ;don't do lineint on 0

  ld a,(turbor?)
  and a
  push af


;Palettesplit-> Changes: HL, BC, A
    ld                hl,currentpalette
        xor                a
        out                ($99),a
        ld                a,16+128
        out                ($99),a

  ld    a,2               ;Set Status register #2
  out   ($99),a
  ld    a,15+128          ;we are about to check for HR
  out   ($99),a

  ld    b,%0010'0000      ;bit to check for HBlank detection
  ld    c,$9a             ;outport to write palette data


.Waitline:
  in    a,($99)           ;Read Status register #2
  and   b                 ;first wait for the next line to start
  jr    nz,.Waitline

.Waitline2:
  in    a,($99)           ;Read Status register #2
  and   b                 ;first wait for the next line to start
  jr    nz,.Waitline2

  pop af ;10 Tstates
  call nz,manualwait ;10 Tstates
;--------------------------------- +
;             20 Tstates                 
;need 12 extra
;nop : nop : nop ;12 Tstates sums up to 32 :)
  
;nop : nop : nop : nop : nop : nop : nop : nop : ;-32 Tstates


;and now write color 0, which is the blue air, the background color
  ld    b,(hl)            ;palettecolor 0 red + blue
  inc   hl
  ld    a,(hl)            ;palettecolor 0 green
  inc   hl 
  out   (c),b   ;11 + 2  ED xx
  out   (c),a   ;11 + 2  ED xx

;  ld a,(scorebaractive)
;  and a
;  jp z,.nosplit

  ;Sprite split
	ld		a,(VDP_0+5)
	out		($99),a
	ld		a,5+128
	out		($99),a
	ld		a,(VDP_8+3)
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,(VDP_0+6)
	out		($99),a
	ld		a,6+128
	out		($99),a
 
.nosplit:
  
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
      ld                a,(slot.page12rom)        ;all RAM except page 1+2
        out                ($a8),a         


  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a

;ld a,3     ;this routine changes backdrop color, and can be used in between these engine routines, to see how fast the routines are
;out ($99),a
;ld a,7+128
;out ($99),a	


  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.

;ld a,15     ;this routine changes backdrop color, and can be used in between these engine routines, to see how fast the routines are
;out ($99),a
;ld a,7+128
;out ($99),a	


  ;handle sfx
        ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc               a
        ld                ($7000),a
        ld                a,Sfxblock
        ld                ($9000),a
        inc               a
        ld                ($b000),a
        
.mainsfx:    
  call  MainSfxIntRoutine       ;keep running sfx every int
  call  SetSfx                                     ;if sfx is on, then set to play
  
   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a
        
        

  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        

  ld a,(lineintflag)
  inc a
  ld (lineintflag),a

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 
  

  ei
  ret

vblank: 

;scc replayer this is the most reliable interval
  in a,($a8)
  push af

  ld a,(slot.page12rom)
  out ($a8),a

        ld                a,SCCreplayerblock+1   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
;        ld                ($5000),a
;        inc               a
        ld                ($7000),a
        ld a,$3f ;get bank ready for SCC access
        ld                ($9000),a
;        inc               a
;        ld                ($b000),a
        
        push hl
        push de
        push bc
        
  call  SccReplayerUpdate       ;keep running sfx every int
  
        pop bc
        pop de
        pop hl
   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
;        ld                a,(memblocks.4)
;        ld                ($b000),a
    pop af
    out ($a8),a

  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a

  ld a,(lineintheight)
  cp 4;and a
  jp c,.nosplit ;don't do lineint on 0


  xor   a                   ;on vblank make color 0 (the air, the background) 1 tint darker
  out   ($99),a
  ld    a,16+128
  out   ($99),a
  ld    a,(currentpaletteONEtintdarker)
  out   ($9a),a
  ld    a,(currentpaletteONEtintdarker+1)
  out   ($9a),a 

  
;make sprite split happen

;  ld a,(scorebaractive)
;  and a
;  jp z,.nosplit

;	  ld a,(upperspatattadress)
	ld		a,%1010'1111    ;spr att table to $15600
	out		($99),a		;spr att table to $15600
	ld		a,5+128
	out		($99),a
	ld		a,%0000'0010 ;2
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,%0010'1011    ;spr chr table to $15200
	out		($99),a		;spr chr table to $15200
	ld		a,6+128
	out		($99),a 
  
.nosplit:  
  

  pop   af 
  ei
  ret
  
lenghtinterrupthandler: equ $ - InterruptHandler  

;/====================================[einde interrupt handler]================================================

setspritesplit:
;only on the first frame
  ld a,(scorebaractive)
  and a
  ret z

  ld a,(lineintheight)
  cp 4;and a
  ret c ;don't do lineint on 0 and 2 to prevent flashing by missing the int
  
  
  ld a,(vblankflag)
  cp 1
  ret z

;((%11101111 and %11111100)+2*256)*#80=#17600 ;Nyyrriky has saved my ass once again.
;y=128*(512+(x and %11111100) ;compiled from the above formula y=adress x=byte
;x=(y/128)-512 ;do nor on x
  
;TODO: set the attr table in RAM and flip the table at each frame  

;$14800 = 10010011
;$15600 = 10101111
;$16600 = 11001111

;  ld b,%11001111 ;$16600
;  ld hl,$5600 ;$15600
;  ld (invisupperspatattadress),hl
;  ld a,(upperspatattadress)
;  cp %10101111 ;$15600
;  jp z,.next
;  ld b,%10101111 ;$15600
;  ld hl,$6600 ;$16600
;  ld (invisupperspatattadress),hl
  
.next:

;	ld a,b
;	ld (upperspatattadress),a

	di
	ld		a,%1010'1111    ;spr att table to $15600
	out		($99),a
	ld		a,5+128
	out		($99),a
	ld		a,%0000'0010 ;2
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,%0010'1011    ;spr chr table to $15200
	out		($99),a		;spr chr table to $15200
	ld		a,6+128
	ei
	out		($99),a 

  ret


setnewlineintheight:
  ld a,(lineintheight)
  ld b,a
  ld                a,(VDP_8+15)      ;lineinterrupt has to adapt to the vertical screen offset
;  add   a,lineinterruptheight
  add a,b
  di
  out   ($99),a
  ld    a,19+128
  ei
  out   ($99),a
  ret 
  
  
setscreenoffset:
;set horizontal scroll offset
	ld		a,(VDP_8+21)
  di
	out		($99),a
	ld		a,27+128
	ei
	out		($99),a
;/set horizontal scroll offset

;set horizontal scroll offset for the v9938
  ld    a,(v9958found?)
  or    a
  jp    nz,.v9958found

	ld		a,(VDP_8+10)
  di
	out		($99),a
	ld		a,18+128
	ei
	out		($99),a
.v9958found:
;/set horizontal scroll offset for the v9938


;set vertical scroll offset
	ld		a,(VDP_8+15)
	di
	out		($99),a
	ld		a,23+128
	ei
	out		($99),a
;/set vertical scroll offset
  ret

;memblocks was here

;we berekenen van te voren alle mapy waarden voor de tilechecks
writetilechecktable:

;de waarde van de mapbreedte zetten op een aparte plek in de ram
  ld hl,0
  ld (writetablevalue),hl
  
;  ld de,(maplenght)
  
  ;adress zetten om naar te schrijven
  ld hl,presearchytable
  ld (writetablepointer),hl
 
  ld bc,(mapheight)
 
 
;  ld b,80 ;zoveel entries schrijven we vol
  
  
.write:


  ld de,(writetablevalue)
  ;----->waarde zit nu in DE

  ld hl,(writetablepointer)
  ld (hl),e
  inc hl
  ld (hl),d
  ;waarde van de writetablevalue is in de tabel geschreven
  ;nu moeten we de pointer met 2 verhogen

  ld hl,(writetablepointer) ;waarde binnehalen
  inc hl ;pointer twee keer verhogen
  inc hl
  ld (writetablepointer),hl

  
  ;waarde ophalen van de waarde die we moeten schrijven
  ld hl,(writetablevalue)
  ld de,(maplenght)
  add hl,de
  ld (writetablevalue),hl 
  
  dec bc
  ld a,c
  or b
  jp nz,.write
  
 ; djnz .write ;totdat b 0 is
;clear last bit so that any other code knows that this is the end of the table
  ld hl,(writetablepointer) ;waarde binnehalen
  xor a
  ld (hl),a
  inc hl
  ld (hl),a

  ret

writetablevalue:  dw 0
writetablepointer:  dw 0


 
putspattovram:
	ld		hl,(invisspratttableaddress)		;sprite attribute table in VRAM ($17600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,spat			;sprite attribute table
  ld    c,$98
	call	outix128

	ret

spat:						;sprite attribute table
	db		226,000,000,0	,226,000,004,0	,226,000,008,0	,226,000,012,0
	db		226,000,016,0	,226,000,020,0	,226,000,024,0	,226,000,028,0
	db		226,000,032,0	,226,000,036,0	,226,000,040,0	,226,000,044,0
	db		226,000,048,0	,226,000,052,0	,226,000,056,0	,226,000,060,0

	db		226,000,064,0	,226,000,068,0	,226,000,072,0	,226,000,076,0
	db		226,000,080,0	,226,000,084,0	,226,000,088,0	,226,000,092,0
	db		226,000,096,0	,226,000,100,0	,226,000,104,0	,226,000,108,0
	db		226,000,112,0	,226,000,116,0	,226,000,120,0	,226,000,124,0

upperspatmain: db 0
	
upperspat:						;sprite attribute table above the spritesplit
	db		226,120,000,0	,226,120,004,0	,226,200,008,0	,226,200,012,0 ;coinx+powerup
	db		226,216,016,0	,226,216,020,0	,226,224,024,0	,226,224,028,0 ;showcoin
	db		226,150,032,0	,226,150,036,0	,226,158,040,0	,226,158,044,0 ;yoshicoin
	db		226,166,048,0	,226,166,052,0	,226,174,056,0	,226,174,060,0 ;yoshicoin

	db		226,040-24,064,0	,226,040-24,068,0	,226,048-24,072,0	,226,048-24,076,0 ;timex + time
	db		226,056-24,080,0	,226,056-24,084,0	,226,064-24,088,0	,226,064-24,092,0 ;time
	db		226,000,096,0	,226,000,100,0	,226,000,104,0	,226,000,108,0 ;x +xdigit
	db		226,000,112,0	,226,000,116,0	,226,000,120,0	,226,000,124,0 ;xdigit
	
emptyspat:						;sprite attribute table that is only used for clearing
	db		226,000,000,0	,226,000,004,0	,226,000,008,0	,226,000,012,0
	db		226,000,016,0	,226,000,020,0	,226,000,024,0	,226,000,028,0
	db		226,000,032,0	,226,000,036,0	,226,000,040,0	,226,000,044,0
	db		226,000,048,0	,226,000,052,0	,226,000,056,0	,226,000,060,0

	db		226,000,064,0	,226,000,068,0	,226,000,072,0	,226,000,076,0
	db		226,000,080,0	,226,000,084,0	,226,000,088,0	,226,000,092,0
	db		226,000,096,0	,226,000,100,0	,226,000,104,0	,226,000,108,0
	db		226,000,112,0	,226,000,116,0	,226,000,120,0	,226,000,124,0
fulllengthemptyspat: equ $ - emptyspat
	

swap_spat_col_and_char_table:


;((%11101111 and $FC)+2*256)*#80=#17600 ;Nyyrriky has saved my ass once again.
;y=128*(512+(x and %11111100) ;compiled from the above formula y=adress x=byte
;x=(y/128)-512 ;do nor on x

	ld		a,(VDP_0+6)     ;check current sprite character table
  cp    %0010'1111      ;spr chr table at $17800 now ?
  ld    hl,$6c00        ;spr color table $16c00
  ld    de,$7400        ;spr color table buffer $17400
	ld		a,%1101'1111    ;spr att table to $16e00    
	ld		b,%0010'1110    ;spr chr table to $17000
  jp    z,.setspritetables
  ld    hl,$7400        ;spr color table $17400
  ld    de,$6c00        ;spr color table buffer $16c00
	ld		a,%1110'1111    ;spr att table to $17600
	ld		b,%0010'1111    ;spr chr table to $17800

.setspritetables:
	di
	ld		(VDP_0+5),a
	out		($99),a		;spr att table to $17600
	ld		a,5+128
	out		($99),a
	ld		a,$02     ;%0000'0010
	ld		(VDP_8+3),a
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,b
	ld		(VDP_0+6),a
	out		($99),a		;spr chr table to $17800
	ld		a,6+128
	ei
	out		($99),a

  ld    bc,$200
  ld    (sprcoltableaddress),hl
  add   hl,bc
  ld    (spratttableaddress),hl
  add   hl,bc
  ld    (sprchatableaddress),hl
  ex    de,hl
  ld    (invissprcoltableaddress),hl
  add   hl,bc
  ld    (invisspratttableaddress),hl
  add   hl,bc
  ld    (invissprchatableaddress),hl
  ret

swapnametable:
;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    2          0   A16	A15 A14 A13 A12 A11 A10
;set name table        %0000'0110 = name table op $1800,   %0000'0111 = name table op $1C00
	ld		a,(VDP_0+2)
  cp    %0000'0110      ;name table at $1800 now ?

	ld		a,%0000'0111    ;name table at $1C00
  ld    hl,$1C00        ;name table
  ld    de,$1800        ;name table buffer
  jp    z,.setnametable
	ld		a,%0000'0110    ;name table at $1800
  ld    hl,$1800        ;name table
  ld    de,$1C00        ;name table buffer

.setnametable:
	ld		(VDP_0+2),a  
	di
	out		($99),a		
	ld		a,2+128
  ei
	out		($99),a
  ld    (nametableaddress),hl
  ld    (invispagenametable),de
  ret

  
  
setbackground:

  ld a,(mariotransforming)
  cp 10
  jp z,.endsetcamerx

;oldcamerascroll aan = camera locken op het midden van het beeld
;  ld a,(oldcamerascroll)
;  dec a
;  jp z,.setcamerasolidy

;in het water tellen andere regels
  ld a,(marioswim)
  dec a
  jp z,.setcameraymid

  ld a,(marioclimbing)
  dec a
  jp z,.setcameraymid

;TEMP: only used as a temporary solution towards the camera vs sprite problem
  ld a,(standingonplatform)
  dec a
  jp z,.setcameraymid
  ld a,(forcetile)
  dec a
  jp z,.setcameraymid
  ld a,(maxtile)
  dec a
  jp z,.setcameraymid   
;/TEMP  
  ld a,(staticcamy)
  and a
  jp nz,.endsetcamery
  

;cameray tijdelijk opslaan
  ld bc,(cameray)
  ld (bcvar),bc
  
  
;mario staat gelockt op een tile dus de camera gaat compenseren
  call checktilebelowmarioram
  call c,.movecameraup

  ld a,(mariosheertile)
  and a
  call nz,.movecameraup 
  

;mario gaat vliegen dan wil je hem mooi in het midden van het beeld
  ld a,(marioflying) 
  dec a
  jp z,.setcameraymid

;mario staat op een sheertile dus camera volgt mee
  ld a,(mariosheertile)
  and a
  jp nz,.prepforsheertile
  
;mario valt naar beneden dus de camera volgt mee  
  ld a,(fallspeed)
  and a
  jp nz,.prepcameramid

;als in de vorige frame de camera heeft bewogen doordat mario op een platform staat 
;dan moet er niet meer worden gecheckt aan de bovenkant
;het zou ook met checktile kunnen maar dit is sneller
  ld bc,(bcvar)
  ld hl,(cameray)
  xor a
  sbc hl,bc
  jp c,.endsetcamery

;mario springt boven het beeld uit dus de camera gaat mee 
  ld a,(mariocoordinates)
  cp 19 ;21
  jp c,.setcamerayhigh

  jp .endsetcamery

  

;de camera moet pas gaan volgen wanneer de sprite een bepaald punt in het beeld heeft bereikt
.prepcameramid:


;wanneer mario al op een tile staat dan is het niet zinvol meer om de camera te setten wanneer 
;je ook van een tile afloopt word het netter op deze manier
  ld a,(fallspeed)
  and a
  jp z,.endsetcamery

;when mario starts falling start following after falling otherwise ignore movement  
.prepforsheertile:
;BUG: with very long sheer tiles mario is sometimes able to exit the screen...
  ld a,(mariocoordinates)
  cp 96 ;95
  jp c,.endsetcamery
  
.setcameraymid:

  xor a
  ld (maxtile),a ;reset this value must be established each frame
  
  ld b,12
  jp .docameray

.setcamerayhigh:

   ld b,2
   jp .docameray
   
;mario staat op een tile dus de camera word naar het midden verplaatst
.movecameraup:

;camera is al hoog genoeg niets doen
  ld a,(mariocoordinates)
  cp 83 ;83 lijkt te werken
  ret nc  
 
;mario moet echt stilstaan op de tile en niet in een jump zitten!
;BUG: mario geeft geen antifloat op een sheerlock!

  ld a,(mariofloating)
  dec a
  ret z

;op sheertiles negeren we de lock ;commenting this out may introduce new regressions. We will see
;  ld a,(mariosheertile)
;  and a
;  ret nz

  
;  ret ;DEBUG
  
;bovenkant limiteren
  ld    hl,(marioy)
  srl   h
  rr    l ;/2
  srl   h
  rr    l ;/4
  srl   h
  rr    l ;/8
  ld    a,l
  sub   12        ;camera starts 12 tile above mario


  ld    c,0       ;unless mario is in the top part of the map, then cameray = 0
  ret    c


;camera 8 pixels omhoog
  ld hl,(cameray)
  dec hl
  ld (cameray),hl



;altijd de scroll register zetten ander kan hij gaan hakkelen
  ld    a,(marioy)
  and   7
	ld		(VDP_8+15),a
	ret
	
	


.setcamerasolidy:

        ld b,12


.docameray:


;write the maximum Y value into register e this is an 8bit value stored in a word. First substract the mapheight with 4 then divide by 2
  ld hl,(mapheight)
  ld de,27
  xor a
  sbc hl,de
  ex de,hl

;set cameray
  ld    hl,(marioy)
  srl   h
  rr    l ;/2
  srl   h
  rr    l ;/4
  srl   h
  rr    l ;/8
  ld c,b
  ld b,0
  xor a
  sbc hl,bc

  ld    bc,0       ;unless mario is in the top part of the map, then cameray = 0
  jp    c,.CameraToporBottom

  push hl
  xor a
  sbc hl,de ;compare with value e
  pop hl
  ld    c,e
  jp    nc,.CameraToporBottom

  ld    (cameray),hl
  
  ld    a,(marioy)
  and   7
	ld		(VDP_8+15),a
  jp    .endsetcamery

.CameraToporBottom:
  ld    (cameray),bc
  xor   a         ;if mario is in bottom or top part of the map, then camery and vertical scroll register are fixed
	ld		(VDP_8+15),a
.endsetcamery:
;/set cameray


;set camerax

;oldcamerascroll aan = camera locken op het midden van het beeld
  ld a,(oldcamerascroll)
  dec a
  jp z,.docamerastaticx


;naar rechts bepalen de camera gaat iets doen wanneer mario de juiste coordinaten heeft bereikt

  ld a,(mariocoordinates+1)
  cp 151 ;maximale rechts sprite coordinates
  jp nc,.setcameraxright

 
  ld a,(mariocoordinates+1)
  cp 96 ;maximale links sprite
  jp c,.setcameraxleft
 
 
  
  jp    .endsetcamerx



.setcameraxleft:
  
   ld de,11

;secure the standing on box matter otherwise mario can exit the cameraview
  ld a,(standingonsprite)
  and a
  jp nz,.docamerax
   
  ld a,(marioleftright)
  dec a
  jp z,.endsetcamerx 
   
   jp .docamerax


.setcameraxright:
  
   ld    de,18  
   
   ;secure the standing on box matter otherwise mario can exit the cameraview
  ld a,(standingonsprite)
  and a
  jp nz,.docamerax
    

  ld a,(marioleftright)
  and a
  jp z,.endsetcamerx

    jp .docamerax
    
.setcamerasolidx:

   ld    de,16      



.docamerax:


  push de

  ld hl,(maplenght)
  ld de,32
  xor a
  sbc hl,de
  ld b,h
  ld c,l
  
  pop de

  ld    hl,(mariox)
  srl   h
  rr    l ;/2
  srl   h
  rr    l ;/4
  srl   h
  rr    l ;/8
   
;  ld    de,16 ;camera ten opzichte van mario setten 16 is midden van beeld
  xor   a
  sbc   hl,de
  
  jp    c,.cameratotallyleft
;camera positie setten
  ld    (camerax),hl

  xor a
  sbc hl,bc
  jp    nc,.cameratotallyright


  xor a
  ld (camreachedlimit),a

;;msx 1 scrolls geen register updaten dus 8 pixel scrolls ;)
;  ld a,(msxeen)
;  dec a
;  jp z,.endsetcamerx
  
;scroll register updaten  
  ld    a,(mariox)
  inc   a
  neg
  and   7
	ld		(VDP_8+21),a
  
  ;set camera for v9938
  ld    a,(mariox)
  and   7
  ld    (VDP_8+10),a
  ;/set camera for v9938

  jp    .endsetcamerx

.setcamlimit:

  ld a,1
  ld (camreachedlimit),a
  ret

.docamerastaticx:


;see if we are at the end of the map
  ld hl,(camerax)
  ld de,32
  add hl,de
  ex de,hl

  ld hl,(maplenght)
  xor a
  sbc hl,de
  jp z,.endsetcamerx ;end of map reached

  


  ld a,(framecounter)
  and 1
  jp nz,.endsetcamerx   ;move camera once every 8 frames


  ;set camera for v9938
 ; ld    a,(VDP_8+10)
 ; and   7
 ; ld    (VDP_8+10),a
  ;/set camera for v9938

  ld a,(VDP_8+21)
  dec a
  and 7
  push af
  ld (VDP_8+21),a

  ld b,a
  ld a,7
  sub b

  ld    (VDP_8+10),a ;add v9938 support

  pop af

  cp 7
  jp nz,.endsetcamerx   ;once every 8 movement steps camera gets delayed for 1 frame




  
.move1tile:
  ld hl,(camerax)
  inc hl
  ld (camerax),hl
    


 jp .endsetcamerx




.cameratotallyright:   ;mario is in the right part of the map, dont move camera, camerax = maplenght - x

  ld a,(endlevelbywin)
  and a
  call nz,.setcamlimit

  ld hl,(maplenght)
  ld de,32
  xor a
  sbc hl,de
  ld    (camerax),hl
  ld    a,7
	ld		(VDP_8+21),a

  ;set camera for v9938
  xor   a
  ld    (VDP_8+10),a
  ;/set camera for v9938

  jp .endsetcamerx


.cameratotallyleft:   ;mario is in the left part of the map, dont move camera, camerax = 0
  ld    hl,0
  ld    (camerax),hl
  ld    a,7
	ld		(VDP_8+21),a

  ;set camera for v9938
  xor   a
  ld    (VDP_8+10),a
  ;/set camera for v9938

.endsetcamerx:
;/set camerax


buildscreen: ;dummy label for reference can safely be removed

;now build up the background
	xor		a
	ld		hl,(invispagenametable)	;name table starts at 6144 ($1800)
	call	SetVdp_Write

  ;set backgroundpointer in hl

	ld		hl,(maplenght)
	ld		de,(cameray)
  ld a,l
  or h
	ld		hl,bgrmapaddr  
	jr		z,.setmapwidthx
	
	
	;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
	
	
	;896 tiles per screen
;.setmapwidthy:
;	add		hl,de
;	djnz	.setmapwidthy
.setmapwidthx:
  ld    de,(camerax)
	add		hl,de
	
  ld    de,(maplenghtmin32)
	ld		a,28        ;28 rows to output
	ld 		c,$98       ;out port
	
.buildupscreen:
  call  outix32     ;32 tiles per row to output
	add		hl,de
  dec   a                
  jp    nz,.buildupscreen  

;/now build up the background

	ret	

;===============================================================================================
;zet mario sprites op locatie (mariocoordinates) schrijft de font, color en attributes
;dit zou een losse call moeten worden die aan de hand van een sprite nummer dat aan te sturen is
;===============================================================================================

;bereken mario zijn positie op het scherm aan de hand van de mappointer en mariox en marioy waarden
calculatemarioscreenposition:


  ld hl,(camerax) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8
  
  
  ;we missen een stukje informatie in camerax die wij toevoegen door de vdp van de camera uit te lezen
  ld		a,(VDP_8+21)
  ld c,a
  ld b,0
  xor a
  sbc hl,bc
  ld b,h ;transfer hl->bc
  ld c,l
  
  ld hl,(mariox) ;coordinaten inlezen
  
  xor a
  sbc hl,bc ;positie van de sprite in ons beeld in hl
  
  ld a,h
  and a
  ret nz ;mario valt buiten het beeld dus mogen we niet meer bewegen
  
  ld a,l
  cp 12
  call c,.setmin
  ld (mariocoordinates+1),a ;positie wegschrijven naar de spat
  
  ;en nu de y coordinaten
  
  ld hl,(cameray) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8

  ld		a,(VDP_8+15) ;verticale offset fixen
  ld b,0
  ld c,a
  add hl,bc
  ld b,h ;transfer van hl naar bc
  ld c,l

  ld hl,(marioy) ;coordinaten inlezen

  xor a
  sbc hl,bc ;positie van de sprite in ons beeld in hl 
  call c,.setzero ;negative number = bad
  
  ld a,l
  ld (mariocoordinates),a ;positie wegschrijven naar de spat


  ret

.setmin:
  ld a,12
  ret

.setzero:

  ld l,a
  ret

;-------------------------------------------------------------------BUG BELOW THIS LINE----------------------------------------------------------------


displaythunder:

 ; ld a,(bulletsingame)
 ; and a
 ; ret nz


  ld a,(marioflyslide)
  dec a
  ret z


	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a

      
  ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	ld de,384 ;+12 sprites verder
;	ld de,448 ;+14 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,explosioncloudblocknumber
	call	block12;34		;set blocks in page 1/2 $4000- $8000


  ld a,(marioshot)
  cp 0
  jp c,.setsprite1
  cp 5
  jp c,.setsprite2 
  cp 7
  jp c,.setsprite3
  cp 9
  jp c,.setsprite2 
  
.setsprite1:
;determine left or right cloud sprite and set address
	ld		hl,spriteadrthunderleft ;adress on block	
	  ld a,(marioshot)
	  and 2
	  jp nz,.done	
.right:
 	ld		hl,spriteadrthunderright ;adress on block

  jp .done

.setsprite2:
;determine left or right cloud sprite and set address
	ld		hl,spriteadrthunderleft2 ;adress on block	
	  ld a,(marioshot)
	  and 2
	  jp nz,.done	
.right2:
 	ld		hl,spriteadrthunderright2 ;adress on block
  jp .done

.setsprite3:
;determine left or right cloud sprite and set address
	ld		hl,spriteadrthunderleft3 ;adress on block	
	  ld a,(marioshot)
	  and 2
	  jp nz,.done	
.right3:
 	ld		hl,spriteadrthunderright3 ;adress on block


.done: 		 	
	ld		c,$98
	call	outix32
      
    
  ;put hero sprite color

	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,192 ;+12 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  
	ld hl,colradrwhitesprite
	ld		c,$98
	call	outix16
;/put hero sprite color
 
  ;put spat to ram
	ld		a,(VDP_8+15)
  ld    b,a
 
 

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

 
 ret

;-----------------------------------------------------------BUG ABOVE THIS LINE -------------------------------------------------------------------
 
 
explosioncloudblocknumber: equ $19



displaykickorexplosioncloud:

;  ld a,(bulletsingame)
;  and a
;  ret nz

	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a

      
  ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	ld de,384 ;+12 sprites verder
;	ld de,448 ;+14 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,explosioncloudblocknumber
	call	block1234		;set blocks in page 1/2 $4000- $8000
  

;determine left or right cloud sprite and set address
	ld		hl,spriteadrkickorexplosioncloudleft ;adress on block	
	  ld a,(marioleftrightspr)
	  and a
	  jp z,.done
	
.right:
 	ld		hl,spriteadrkickorexplosioncloudright ;adress on block
.done: 		 	
	ld		c,$98
	call	outix32
      
    
  ;put hero sprite color

	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,192 ;+12 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  
	ld hl,colradrwhitesprite
	ld		c,$98
	call	outix16
;/put hero sprite color
 
  ;put spat to ram
	ld		a,(VDP_8+15)
  ld    b,a
 
 

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

 
 ret
 




setmariocloud:


 ; ld a,(bulletsingame)
 ; and a
 ; ret nz

;  ld a,(marioshell)
;  dec a
;  jp z,.nocloud

;na het omdraaien mag het maar tijdens het draaien niet

  ld a,(mariocloud)
  and a
  jp z,.notimer

  inc a
  ld (mariocloud),a
  
  cp 10
  jp z,.nocloud

  ret

.notimer:


;in de lucht werkt het natuurlijk ook niet 
  ld a,(mariofloating)
  and a
  ret nz

  ld a,(fallspeed)
  and a
  ret nz


;  ld a,(marioshell)
;  dec a
;  jp z,.nocloud
  
  ld a,(marioflying)
  dec a
  jp z,.nocloud
  
  ld a,(marioflyingspecial)
  dec a
  jp z,.nocloud


;  ld a,(marioduck)
;  and a
;  jp nz,.activate

;alleen wanneer we sliden
  ld a,(marioslide)
  and a
  ret z

.activate:

;activeren
  ld a,1
  ld (mariocloud),a


  ret


.nocloud:

  xor a
  ld (mariocloud),a
  
  
    ld a,226         ;y
  ld    (spat+48),a
  ld    (spat+52),a


  xor a
  ld    (spat+49),a
  ld    (spat+53),a
  
  ret

hidemariospat:


  ld a,226         ;y
  ld    (spat+48),a
  ld    (spat+52),a

  ld    (spat+56),a
  ld    (spat+60),a
  ld    (spat+64),a
  ld    (spat+68),a


  xor a
  ld    (spat+49),a
  ld    (spat+53),a

  ld    (spat+57),a
  ld    (spat+61),a
  ld    (spat+65),a
  ld    (spat+69),a


  ret

  

putmariosprite:


  ld hl,(marioy)
  ld de,0
  xor a
  sbc hl,de
  jp z,hidemariospat

    
    xor a
    ld (foregroundfoundright),a
    ld (foregroundfoundleft),a

  ld  b,5
  ld de,0
  call checkgeneraltile ;in: b=tiles below mario, de=tiles right of mario, out: hl->tile, changes: af,bc,de,hl
  ld a,(hl)
  cp 192
  jp c,.foregroundrightnotfound
  cp 241
  jp nc,.foregroundrightnotfound

  ld  a,1
  ld (foregroundfoundright),a

.foregroundrightnotfound:
  
  dec hl
  
  ld a,(hl)
  cp 192
  jp c,.foregroundrightnotfound2
  cp 241
  jp nc,.foregroundrightnotfound2
  
  ld  a,1
  ld (foregroundfoundright),a
  
.foregroundrightnotfound2:

  ld  b,5
  ld de,1
  call checkgeneraltile ;in: b=tiles below mario, de=tiles right of mario, out: hl->tile, changes: af,bc,de,hl
  ld a,(hl)
  cp 192
  jp c,.foregroundleftonenotfound
  cp 241
  jp nc,.foregroundleftonenotfound 

  ld  a,1
  ld (foregroundfoundleft),a


.foregroundleftonenotfound:

    inc hl
    
  ld a,(hl)
  cp 192
  jp c,.foregroundleftnotfound
  cp 241
  jp nc,.foregroundleftnotfound 
  
  
  ld  a,1
  ld (foregroundfoundleft),a
.foregroundleftnotfound:



	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a	

;sprites laten staan als er wolkjes komen
  ld a,(mariocloud)
  and a
  jp nz,.leavesprites

;ook met kogeltjes laten staan
;  ld a,(bulletsingame)
;  and a
;  jp nz,.leavesprites

  
;tweede sprite altijd eerst destroyen  
  ld a,226         ;y
  ld    (spat+48),a
  ld    (spat+52),a


  xor a
  ld    (spat+49),a
  ld    (spat+53),a
  
.leavesprites: 


;we gaan kijken wat de voorwaarden zijn en als die kloppen dan maken we een wolkje
  call setmariocloud

    
;put hero sprite character
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
;	ld de,384 ;+12 sprites verder
	ld de,448 ;+14 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,(blocknumber)
	call	block12;34		;set blocks in page 1/2 $4000- $8000

	ld		hl,$4000 ;adress on block
	ld de,(mariospritenumber)
	add   hl,de
	ld		c,$98
	call	outix128
;/put hero sprite character


;  ld a,(bulletsingame)
;  and a
;  jp nz,.donesetmariosprite2


	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	ld de,384 ;+12 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write


;  ld a,(marioshell)
;  dec a
;  jp z,.setshellspriteattr

  ld a,(mariocloud)
  and a
  jp nz,.setcloudsprattr


  ld		hl,$4000 ;adress on block
	ld de,(mariospritenumber2)
	add   hl,de
	ld		c,$98
	call	outix64
  jp .donesetmariosprite2

	
;.setshellspriteattr:
;
;  ld a,$19
;  call block1234
;
;  ld hl,6720
;  ld (mariospritenumber2),hl
;
;  ld		hl,$4000 ;adress on block
;	ld de,(mariospritenumber2)
;	add   hl,de
;	ld		c,$98
;	call	outix64
;  
;  ld a,(blocknumber)
;  call block1234
;
;  jp .donesetmariosprite2


.setcloudsprattr:



  ld a,(mariocloud)
  dec a
  jp z,.setsprite1
  dec a
  jp z,.setsprite1
  dec a
  jp z,.setsprite2
  dec a
  jp z,.setsprite2
  dec a
  jp z,.setsprite3
  dec a
  jp z,.setsprite3
  dec a
  jp z,.setsprite4
  dec a
  jp z,.setsprite5
  dec a
  jp z,.setsprite6
  
  
.setsprite1:

  ld hl,6816
    ld de,7008
  jp .setspritedone

.setsprite2:

  ld hl,6912
    ld de,7008
  jp .setspritedone

.setsprite3:

  ld hl,7008
    ld de,7008
  jp .setspritedone

.setsprite4:

    ld hl,7008
  ld de,6912
  jp .setspritedone

.setsprite5:

    ld hl,7008
  ld de,7008
  jp .setspritedone


.setsprite6:

  ld hl,6816
  ld de,7008
  jp .setspritedone


.setspritedone:



  ld (mariospritenumber2),hl
  ld (mariospritenumber3),de


  ld a,$19
  call block1234


  ld		hl,$4000 ;adress on block
	ld de,(mariospritenumber2)
	add   hl,de
	ld		c,$98
	call	outix32
	
;moet twee keer gebeuren	
	ld		hl,$4000 ;adress on block
	ld de,(mariospritenumber3)
	add   hl,de
	ld		c,$98
	call	outix32
  
  ld a,(blocknumber)
  call block1234


	
.donesetmariosprite2:

;put hero sprite color

	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,224 ;+14 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

;	ld		hl,$4000+128
  ld hl,$4000
 
   
	ld de,(mariospritenumber)
	add   hl,de
	ld de,128
	add hl,de
	ld		c,$98
	call	outix64
	
;	ld a,(blocknumber)
;  call block1234
;/put hero sprite color

.endsetherospriteclr:

;  ld a,(bulletsingame)
;  and a
;  jp nz,.endsetsprite2colr


	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,192 ;+12 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write


;  ld a,(marioshell)
;  dec a
;  jp z,.setshellspritecolr

  ld a,(mariocloud)
  and a
  jp nz,.setcloudspritecolr


  ld hl,$4000
	ld de,(mariospritenumber2)
	add   hl,de
	ld de,64
	add hl,de
	ld		c,$98
	call	outix32
  jp .endsetsprite2colr

.setcloudspritecolr:


  ld a,$19
  call block1234

  ld hl,$4000
	ld de,(mariospritenumber2)
	add   hl,de
	ld de,64
	add hl,de
	ld		c,$98
	call	outix16
	
	ld hl,$4000
	ld de,(mariospritenumber3)
	add   hl,de
	ld de,64
	add hl,de
	ld		c,$98
	call	outix16
	
  
;  ld a,(blocknumber)
;  call block1234
 

 
 
  
.endsetsprite2colr:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;  ld a,(msxeen)
;  dec a
;
;  call nz,calculatemarioscreenposition ;waar is onze sprite ten op zichte van de camera  
;  call z,.scrollmsxeenstyle

  call calculatemarioscreenposition ;waar is onze sprite ten op zichte van de camera


;put spat to ram
	ld		a,(VDP_8+15)
  ld    b,a

  ld a,(marioyoshi)
  dec a
  call z,.higherspaty16


;wanneer mario slide op de grond is de sprite 8 pixels hoger dus trekken we de y coordinaten 8 naar beneden tijdens 
;de slide zodat we geen aparte sprite hoeven te bouwen
  ld a,(marioflyslide)
  dec a
  call z,.lowerspaty8

;waneer je van een schuin platformpje afglijd moet de reset heel even wachten dan is het mooier bij het vallen
 ld a,(mariosheertile)
 and a
 call nz,resetsheerfalltimer
 
 
 ld a,(mariosheertile)
 and a
 call z,runsheerfalltimer

 
;TODO: deze call veel inteligenter maken zodat hij ook op hoekjes checkt etc etc etc  
;spat verlagen wanneer mario op een schuine tile loopt anders staat hij te hoog  

  call lowerspatforsheertile

;flash mario each 4th frame after being hit
  ld a,(mariogod)
  cp 70
  jp nc,.nospriteflash
  cp 1 ;skip the first frame
  jp z,.nospriteflash
  and 3
  jp nz,destroysprite
.nospriteflash:  

  ld a,(nosprite)
  dec a
  jp z,destroysprite
  
  
;add vertical scroll offset
  ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a
;/add vertical scroll offset

  
;  ld a,(endlevelbywin)
;  and a
;  jp nz,.nohorizontalspat


;make the horizontal coordinates count

  ld    a,(v9958found?)
  or    a

  ld    a,(mariocoordinates+1) 

  ;this code is for v9938
  jp    nz,.v9958found
  ld    c,a
	ld		a,(VDP_8+10)
  add   a,c
  sub   a,7
.v9958found:  
  ;/this code is for v9938
  
  
  sub 4
  ld    (spat+57),a
  ld    (spat+61),a
  ld    (spat+65),a
  ld    (spat+69),a
;/make the horizontal coordinates count

.nohorizontalspat:

;reststuk aanzetten wanneer we omdraaien met schelpje

;  ld a,(marioshell)
;  and a
;  call nz,.putmariospriteshell2

;alle extra sprites die mario mogen vergezellen
;  ld a,(bulletsingame)
;  cp 1
;  jp z,.killsecondsprite
;  cp 2
;  jp z,.noextrasprites


  ld a,(mariocloud)
  and a
  call nz,.putmariocloud

  ld a,(mariokickshell)
  dec a
  call z,.putmariokickcloud
  
  ld a,(mariokillshell)
  and a
  call nz,.putmariokickobject

  ld a,(marioshot)
  and a
  call nz,.putmarioshoteffects
  

;reststuk aanzetten wanneer we wel vliegen maar niet droppen
  ld a,(marioflydrop)
  xor %00000001
  ld c,a
  ld a,(marioflyspecial)
  and c
  call nz,.putmariosprite2


;/put spat to ram
	

.noextrasprites:
	
;  ld		a,(slot.ram)	;back to full RAM
;	out		($a8),a
  ret


.killsecondsprite:


    ld a,226         ;y
;  ld    (spat+48),a
  ld    (spat+52),a


  xor a
;  ld    (spat+49),a
  ld    (spat+53),a
  
;  ld		a,(slot.ram)	;back to full RAM
;	out		($a8),a
  ret


;/putmariosprite

.putmarioshoteffects:

;do not allow on yoshi
  ld a,(marioyoshi)
  dec a
  ret z


;alleen met vliegende mario
  ld a,(mariostate)
  cp 3
  ret nz

;ook niet als we transformeren
  ld a,(mariotransforming)
  and a
  ret nz

  call displaythunder


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 15                            ;add vert scroll offset
  ld    (spat+48),a

  ld a,(marioshot)
  and 2
  jp nz,.setspatkickrightfar

  
  ld a,(mariocoordinates+1)
  add 10
  ld    (spat+49),a

  ret

.setspatkickrightfar:

  
  ld a,(mariocoordinates+1)
  sub 18
  ld    (spat+49),a


  ret



.putmariokickobject:

  call displaykickorexplosioncloud


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 10                            ;add vert scroll offset
  ld    (spat+48),a

  ld a,(marioleftrightspr)
  dec a
  jp nz,.setspatkickright

  
  ld a,(mariocoordinates+1)
  add 2
  ld    (spat+49),a

  ret


.putmariokickcloud:

  call displaykickorexplosioncloud


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 14                            ;add vert scroll offset
  ld    (spat+48),a

  ld a,(marioleftrightspr)
  dec a
  jp nz,.setspatkickright

  
  ld a,(mariocoordinates+1)
  add 2
  ld    (spat+49),a

  ret
 
.setspatkickright:

  
  ld a,(mariocoordinates+1)
  sub 12
  ld    (spat+49),a


  ret



;wolkje naar de spat uitbouwen
.putmariocloud:

  ;in case mario is on yoshi
  ld a,(marioyoshi)
  dec a
  jp z,.setspat3

  ld a,(mariocloud)
  cp 1
  jp z,.setspat1
  cp 3
  jp z,.setspat2
  cp 9
  jp z,.nospat


  ret

.setspat1:


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 28                            ;add vert scroll offset
  ld    (spat+48),a
  
  ld a,(mariocoordinates+1)
  ld    (spat+49),a
  
  ret

.setspat2:


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 28                            ;add vert scroll offset
  ld    (spat+52),a

  ld a,(mariocoordinates+1)

  ld    (spat+53),a

  ret

.setspat3:


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 43                            ;add vert scroll offset
  ld    (spat+52),a

  ld a,(mariocoordinates+1)

  ld    (spat+53),a

  ret


.nospat:

  ld a,226         ;y
  ld    (spat+48),a
  ld    (spat+52),a


  xor a
  ld    (spat+49),a
  ld    (spat+53),a


  ret


.putmariosprite2:


  ld    a,(mariocoordinates)            ;y
  add   a,b
  add 8                            ;add vert scroll offset
  ld    (spat+48),a
  ld    (spat+52),a


;richting bepalen en aan de hand daarvan de goeie coordinaten setten
  ld a,(marioleftright)
  and a
  jp z,.putspriteright
  

  ld a,(mariocoordinates+1)
  ld b,20
  sub b 
  ld c,a


  ld    a,(v9958found?)
  or    a

  ld a,c
  ;this code is for v9938
  jp    nz,.v9958found2


  ld a,(mariocoordinates+1)
  ld b,20
  sub b 

;make the horizontal coordinates count
  ld    c,a
	ld		a,(VDP_8+10)
  add   a,c
  sub   a,7
.v9958found2:  
  ;/this code is for v9938



  ld    (spat+49),a
  ld    (spat+53),a
  
  ret


.putspriteright:

  ld a,(mariocoordinates+1)
  ld b,12
  add b 

  ld c,a


  ld    a,(v9958found?)
  or    a

  ld a,c
  ;this code is for v9938
  jp    nz,.v9958found3


  ld a,(mariocoordinates+1)
  ld b,12
  add b 

;make the horizontal coordinates count
  ld    c,a
	ld		a,(VDP_8+10)
  add   a,c
  sub   a,7
.v9958found3:  
  ;/this code is for v9938





  ld    (spat+49),a
  ld    (spat+53),a



  ret


.higherspaty16:


  ld a,(marioduck)
  dec a
  jp z,.higherspaty8

  ld a,b
  sub 14
  ld b,a

  ret
 

.higherspaty8:


  ld a,b
  sub 6
  ld b,a

  ret




.lowerspaty8:

  ld a,b
  add 8
  ld b,a

  ret

.scrollmsxeenstyle:

  ld a,128
  ld (mariocoordinates+1),a
  ld a,148
  ld (mariocoordinates),a
  
  ret


lowerspatforsheertile: 


  ld a,(mariosheertile)
  and a
  jp nz,.lowerspaty4
;  jp .lowerspaty4

  ld c,0
  
  ld a,(tilecorner)
  dec a
  jp z,.lowerspaty6

  ret


.lowerspaty4:

  ld c,0

  ld a,(foregroundfoundright)
  dec a
  jp nz,.nextcheck

  
  ld  a,(mariox)
  and 7
  cp 4
  jp nc,.dolowerspat

  sub 4
  ld c,a 

  jp .dolowerspat

.nextcheck:

  ld a,(foregroundfoundleft)
  dec a
  jp nz,.dolowerspat
;  ret nz
  
  ld  a,(mariox)
  and 7
  cp 4
  jp c,.dolowerspat

  push af
  call killsheerfalltimer ;anders krijgen we bugs
  pop af


  sub a,4
  ld c,a

  ld a,b  
  add 4
  sub c
  ld b,a
  ret
 
.dolowerspat:

  ld a,b
  add 4
  add c
  ld b,a  
  ret  

.lowerspaty6:
  ld a,b
  add 2
  ld b,a

  ret

resetsheerfalltimer:

  xor a
  ld (sheerfalltimer),a
  ret
  
runsheerfalltimer:

;op een hoekje niets doen
   ld a,(tilecorner)
   dec a
   ret z 

;checken of we gelockt staan
  ld a,(tilelocked)
  dec a
  ret z

;jumpen is instant reset

  ld a,(jumpkeyhold) 
  dec a
  jp z,killsheerfalltimer


    ld a,(sheerfalltimer)
    cp 9
    ret nc ;overflow protection
    inc a
    ld (sheerfalltimer),a


;TODO: kijken naar de fallspeed dan gaat die nog mooier!
 ld a,(sheerfalltimer)
 cp 1
 jp z,.lowerspaty8
 cp 2
 jp z,.lowerspaty7
 cp 3
 jp z,.lowerspaty6
 cp 4
 jp z,.lowerspaty5
 cp 5
 jp z,.lowerspaty4
 cp 6
 jp z,.lowerspaty3
 cp 7
 jp z,.lowerspaty2 
  cp 8
 jp z,.lowerspaty1
 

  ret


.lowerspaty1:

  ld a,b
  add 1
  ld b,a


  ret

.lowerspaty2:

  ld a,b
  add 2
  ld b,a


  ret
.lowerspaty3:

  ld a,b
  add 3
  ld b,a


  ret
.lowerspaty4:

  ld a,b
  add 4
  ld b,a


  ret
.lowerspaty5:

  ld a,b
  add 5
  ld b,a


  ret
.lowerspaty6:

  ld a,b
  add 6
  ld b,a


  ret
.lowerspaty7:

  ld a,b
  add 7
  ld b,a


  ret
.lowerspaty8:

  ld a,b
  add 8
  ld b,a


  ret





killsheerfalltimer:


  ld a,10
  ld (sheerfalltimer),a
  ret


destroysprite:
 
;tweede sprite altijd eerst destroyen  
  ld a,226         ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+48),a
  ld    (spat+52),a


  xor a
  ld    (spat+49),a
  ld    (spat+53),a
  
  ;add vertical scroll offset
  ld a,226
  ld    (spat+56),a
  ld    (spat+60),a
  ld    (spat+64),a
  ld    (spat+68),a

  xor a
  ld    (spat+57),a
  ld    (spat+61),a
  ld    (spat+65),a
  ld    (spat+69),a
  

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  
  ret
  

clearsprfromvram: ;clear the sprite from the vram if the frame does not match the current

  ld a,(ix+clearvram)
  and a
  ret nz

.force:
;  ret

  ld l,(ix+coloraddress)
  ld h,(ix+coloraddress+1)
  


  ;set vdp address to write at character table and write spritecharacter
;  ex    de,hl                   ;set color address in hl
  add   hl,hl                   ;(color address*2) = character address (character address is 2x higher than color)
	ld		de,(invissprchatableaddress)
	add   hl,de
	ld		a,1
	call	SetVdp_Write


	ld		c,$98
  ld    a,(ix+amountspr)        ;amount of sprites
.characterloop:
  ld hl,dummydata32
	call	outix32                 ;out 32*a bytes (1 sprite=32 bytes) of sprite characterdata, hl-> adress on block
  dec   a
  jp    nz,.characterloop

  ld (ix+clearvram),1


  ret


dummydata32: db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0




;=====================================================[Alle checktileroutines terugplaatsen na cleanup]=====================================================
;verplaatst naar page 3 we hebben meer dan genoeg RAM voordat de stack begint

;  include "checktile.asm"


;=====================================================[checktile]===========================================================================================

;include "handlescorebar.asm" ;-----------------------------------> TO ROM!!




available:  equ %1000'0000
force00:    equ 128+64+00
force01:    equ 128+64+01
force02:    equ 128+64+02
force03:    equ 128+64+03
force04:    equ 128+64+04
force05:    equ 128+64+05
force06:    equ 128+64+06
force07:    equ 128+64+07
force08:    equ 128+64+08
force09:    equ 128+64+09
force10:    equ 128+64+10
force11:    equ 128+64+11
force12:    equ 128+64+12
force13:    equ 128+64+13
force14:    equ 128+64+14
force15:    equ 128+64+15
force16:    equ 128+64+16
force17:    equ 128+64+17
force18:    equ 128+64+18
force19:    equ 128+64+19
force20:    equ 128+64+20
force21:    equ 128+64+21
force22:    equ 128+64+22
force23:    equ 128+64+23
force24:    equ 128+64+24
force25:    equ 128+64+25
force26:    equ 128+64+26
force27:    equ 128+64+27
force28:    equ 128+64+28
force29:    equ 128+64+29
force30:    equ 128+64+30
force31:    equ 128+64+31
seclist:    equ 128+32
;derde bit is onbenut deze kunnen wij gebruiken voor de secundaire objectslist
;%11011111 bit5


levelObjectListpointer: dw  levelObjectList+lenghtlevelObjectList


sprlst:                         ;spritelist: 32 bytes representing each sprite, 00=sprite is free to use, 01=sprite is in use
  db  00,00,00,00               ;note that sprite 12 - 17 are always in use by mario
  db  00,00,00,00
  db  00,00,00,00
  db  -1,-1,-1,-1 ;reserved voor mario
  db  -1,-1,00,00
  db  00,00,00,00
  db  00,00,00,00
  db  00,00,00,00
  db  10,-1 ;end of sprlst 
endsprlst:

emptysprlst:                    ;spritelist: 32 bytes representing each sprite, 00=sprite is free to use, 01=sprite is in use
  db  00,00,00,00               ;note that sprite 12 - 17 are always in use by mario
  db  00,00,00,00
  db  00,00,00,00
  db  -1,-1,-1,-1 ;reserved voor mario
  db  -1,-1,00,00
  db  00,00,00,00
  db  00,00,00,00
  db  00,00,00,00
  db  10,-1 ;end of sprlst



AmountofPossibleActiveObjects:  equ 20
  
offsetslenght:equ 48

active?:          equ 0
levobjlistadd:    equ 1
v5:               equ 3 ;(V5 is a 16 bits value)
yobject:          equ 5
xobject:          equ 7
sprlstadd:        equ 9
movepat:          equ 11
amountspr:        equ 13
updatespr?:       equ 14
coloraddress:     equ 15
spataddress:      equ 17
v1:               equ 19
v2:               equ 20
v3:               equ 21
v4:               equ 22
v6:               equ 88 ;if counter is not used in anyway v6 can be used as a 16 bit variable
v7:               equ 89
offsets:          equ 23
nosprupdate:      equ 69 ;uses the last offset byte to exlude sprites from automatic updates
nosheercheckb:    equ 70 ;uses the last offset byte to exclude it from the sheertile checks (Z80 optimisation)
spritecolROM:     equ 71
spritecharROM:    equ 73
sizeX:            equ 75
sizeY:            equ 76
deadly:           equ 77
clipping:         equ 78
hitpoints:        equ 79 
objectrelease:    equ 80
spritesblock:     equ 81
edible:           equ 82
deathtimer:       equ 83
objectfp:         equ 84
sheertile:        equ 85
killshell:        equ 86 ;this variable is used by objectinteract to determine if the other object should die as well
objectreturn:     equ 87 ;if this one is set to 1 the object will no longer return after it has been killed
counter:          equ 88 ;16 bit counter for long object spawn times
random:           equ 90 ;random counter
updateoddoreven?: equ 91 ;updateoddoreven?
clearvram:        equ 92 ;0 = clear the vram once
standingonspr:    equ 93 ;standing onsprite 1 means the object is standing on a sprite, clear at each frame
lockdata:         equ 94 ;the ix adress of the elevator the sprite had touched

setobjectscurrentscreen?: db  1
cameraposition:           ds  2

CustomObject:
  db available :  dw  455 : dw 039,010  ;custom object is used to place any object in screen at any desired moment


cameraytimes8:      ds  2
cameraxprecise:     ds  2
yobjectmirror:      ds  2
xobjectmirror:      ds  2
amountofspr:        ds  1

removeobjectspat:
  
  ld b,(ix+amountspr)
  ld a,b
  and a
  ret z ;0 sprites

  ld    l,(ix+spataddress)          
  ld    h,(ix+spataddress+1)

  ld de,4

.loop:
   ld (hl),226
  add hl,de
  djnz .loop

  ret

  
removefromsprlst:

  ;remove sprite from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
  ld    b,(ix+amountspr)        ;amount of sprites
.cleanspritesfromspritelist:
;  ld    (hl),0
  ld a,(hl)
  and a
  jp z,.skip ;never allow -
  dec a
  ld (hl),a

.skip:

  inc   hl
  djnz  .cleanspritesfromspritelist
  ;/remove sprite from spritelist
  
  ret  

  
  
;sometimes one sprite is overlapped by two sprites. therefore when a new overlap occurs the vram will show garbage on this other sprite because the sprite is not cleared due to the second overlap
;still being present. We must find the sprite sharing this overlap position and force a vram clearing. This call does NOT clear the vram only set the clearvram byte on the proper ix value of 
;the counter sprite.
clearvramonothersprite:

  push hl
  ex af,af'
  exx
  pop de

  
;push the ix byte or else!!
  push iy
  push ix
  
;put hl in de for address comparison

  ld    ix,Objectlist+(lenghtobjectlist*00) : bit 0,(ix) : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : bit 0,(ix) : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : bit 0,(ix) : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : bit 0,(ix) : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : bit 0,(ix) : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : bit 0,(ix) : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : bit 0,(ix) : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : bit 0,(ix) : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : bit 0,(ix) : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : bit 0,(ix) : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : bit 0,(ix) : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : bit 0,(ix) : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : bit 0,(ix) : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : bit 0,(ix) : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : bit 0,(ix) : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : bit 0,(ix) : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : bit 0,(ix) : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : bit 0,(ix) : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : bit 0,(ix) : call nz,.handleobject ;objectnr 20

  pop ix
  pop iy

  exx
  ex af,af'

  ret



.handleobject:


  ld b,(ix+amountspr)
  ld l,(ix+sprlstadd)
  ld h,(ix+sprlstadd+1) ;get the sprlst adress of this object
.loop:
  ; push hl
   xor a
   sbc hl,de ;this is the countersprite as the sprlst adress is a match.
  ; pop hl
   jr z,.setclearvrambyte
   add hl,de
   inc hl
   djnz .loop
   
   ret
   
.setclearvrambyte:
   
  ld (ix+clearvram),0
  ret
  
  

;wanneer twee objecten elkaar raken moeten ze van alle andere actieve objecten checken of er iets moet gebeuren
;in = de ix van het object dat wilt weten waar we zijn
;out = carry flag als er colission is en A = 1 als dat het enige is en A = 2 als het object erdoor sterft
;OUT in B = de snelheid van het object dat is geraakt
;out = IY van het object wat interactie heeft aangegaan
objectinteract:


  ld (currentobjectvalue),ix
  push ix
  pop iy

;alle XY waarden die het huidige object bezitten saven voordat we de loop in gaan
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld (currentx),hl
  ld (currenty),de

;zodra we een object vinden handelen we hem af en returnen we met een carry flag
    xor a ;bit command does not reset carry flag so the first line corrupts if by accident the carry flag was set
  ld    ix,Objectlist+(lenghtobjectlist*00) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : bit 0,(ix) : call nz,handleobject : jp c,.end ;objectnr 20

;no object found return this
  xor a
  ld ix,(currentobjectvalue) 
  
  
  ret


.end:


  ld a,(ix+movepat+1)
  and a
  jp nz,.skipchecks 
  ld a,(ix+movepat)
  cp 11
  jp z,.killotherobject
  cp 30
  jp z,.killotherobject ;bullet
  cp 34
  jp z,.killotherobject ;chuck
  cp 36
  jp z,.killotherobject ;chuck jumping

.skipchecks:
  
  ld a,(ix+clipping) ;gaan we echt dood?
  ld b,(ix+v1)
  
  
  push ix
  pop iy ;save the ix value for interaction reference by the routine that called this
;adress terugschrijven
  ld ix,(currentobjectvalue)
  scf
  
  ret

;als je de shell vasthebt moet er een dubbele kill worden gemaakt maar je tegenobject is al verdwenen.
;als je tegenobject een movementpattern 11 heeft (shelhold) dan moet die automatisch meesterven
;Daarom word in dit object een bitje hooggezet en word afgehandeld door movementpattern 11
.killotherobject:

;get the other objects clipping value 
  push ix
  ld ix,(currentobjectvalue)
  ld a,(ix+clipping) ;gaan we echt dood?
  pop ix


  ld (ix+killshell),a ;then we really know the clipping of the object that caused this shell to selfdestruct

  ld a,(ix+clipping)
  ld b,(ix+v1)
 

  push ix
  pop iy ;save the ix value for interaction reference by the routine that called this
  
;adress terugschrijven
  ld ix,(currentobjectvalue)

  ret



handleobject:

;ben ik het zelf
  ld hl,(currentobjectvalue)
 ; push ix
 ; pop de
    ld e,ixl
    ld d,ixh
 
 
  xor a
  sbc hl,de
  ;jr z,.endcheck
    ret z
  

;is it a bullet? also when the object has a clipping of 1 we handle it differently
  ld a,(ix+clipping)
  and a ;cp 0
  ret z
  dec a ;1 cp 1
  jr z,handlebullet
  cp 5-1
  jr z,handlebullet
  

;geen clipping is gewoon niet afhandelen
;  ld a,(ix+clipping)
;  and a
;  ret z


  ;check if other object is within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-16;-20
  add hl,de
  
  ld de,(currentx)
  xor  a  
  sbc hl,de
  
;left border not found, no collision
  ;jp nc,.endcheck
    ret nc
  
  ld d,a ;ld d,0
  ld  a,(ix+sizeX)
  add a,15;22
 ; ld  d,0
  ld  e,a
;  or  a  
  add hl,de

;right border not found, no collision
   ;jp nc,.endcheck
   ret nc
  ;/check if other object is within x range of object

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-22;-28
  add hl,de
 
  ld de,(currenty)
  xor  a  
  sbc hl,de
  
;top border not found, no collision
 ; jp nc,.endcheck
    ret nc
 
  ld d,a ;ld d,0
  ld  a,(ix+sizeY)
  add a,32;;15
  ;ld  d,0
  ld  e,a
  or  a  
  add hl,de

;bottom border not found, no collision
;   jp nc,.endcheck
  ;/check if other object is  within y range of object
;    scf 
;    ret
 
.endcheck:

  ret


handlebullet:

;geen clipping is gewoon niet afhandelen
 ; ld a,(ix+clipping)
 ; and a
 ; ret z

;;ben ik het zelf
;  ld hl,(currentobjectvalue)
; ; push ix
; ; pop de
;    ld e,ixl
;    ld d,ixh  
  
  
 ; xor a
 ; sbc hl,de
 ;; jp z,.endcheck
 ;   ret z
 

  ;check if other object is within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-20
  add hl,de
  
  ld de,(currentx)
  or  a  
  sbc hl,de
  
;left border not found, no collision
 ; jp nc,.endcheck
    ret nc
 
  ld  a,(ix+sizeX)
  add a,22
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;right border not found, no collision
  ; jp nc,.endcheck
  ret nc
  ;/check if other object is within x range of object

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-28
  add hl,de
 
  ld de,(currenty)
  or  a  
  sbc hl,de
  
;top border not found, no collision
  ;jp nc,.endcheck
    ret nc
  
  ld  a,(ix+sizeY)
  add a,32;;15
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;bottom border not found, no collision
  ; jp nc,.endcheck
  ;ret nc
  ;/check if other object is  within y range of object
  
.endcheck:

  ret



currentobjectvalue: dw 0 ;zo kunnen we de objects van elkaar onderscheiden
;ixtransfer:         dw 0 ;ix in hl zien te douwen
currentx:           dw 0 ;de x waarde van de ix die deze routine opvraagt
currenty:           dw 0 ;de y "




;checktop de schuine tiles. In principe alleen nodig voor rex en schelpje en dat vage ventje in map 1
; C = schuine tile gevonden; NC = geen schuine tile gevonden
checksheerobjecttile:

  call nosheercheck
  ret z


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right




;next tile
  ld    b,0
  ld c,(ix+sizeY)

  ld    de,0 ;6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.diagonaltilegoingupfoundleft

;HMMMZ!!!
;  ld    b,0
;  ld c,(ix+sizeY)
;  dec bc
;  dec bc
;  dec bc
;  dec bc
;  dec bc
;  dec bc 
;  dec bc 
;  dec bc 
;  ld    de,-8 ;6                ;horizontal increase to tilecheck
;  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
;check next tile
  dec hl
  ld de,(maplenght)
  xor a
  sbc hl,de
  ld a,(hl)
  
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.diagonaltilegoingupfoundleft



  jp .endcheckrightdiagonalup

.movingright:


  ;check collision with sheertile right
  ld    b,0
  ld c,(ix+sizeY)

  ld    de,-8                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.nextcheckrightdiagonalup
  cp    242                     ;check for tilenumber 242-246 (all diagonal tiles going up)
  jp    nc,.diagonaltilegoingupfound
.nextcheckrightdiagonalup:  


;next tile
;  ld    b,0
;  ld c,(ix+sizeY)

                  ;vertical increase to tilecheck                
;  ld    de,0 ;6                ;horizontal increase to tilecheck
;  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  inc hl
  ld a,(hl)
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.endcheckrightdiagonalup
  cp    242                     ;check for tilenumber 242-246 (all diagonal tiles going up)
  jp    nc,.diagonaltilegoingupfound

.endcheckrightdiagonalup:

  call setpage12tohandleobjectmovement
  xor a

  ret


.diagonaltilegoingupfound:

  call locksheertile.left

  ld    (ix+objectfp),0

  ld a,(ix+v1)
  neg
  ld (ix+v2),a
    call setpage12tohandleobjectmovement
  scf
  ret

.diagonaltilegoingupfoundleft:

  call locksheertile.right

  ld    (ix+objectfp),0

  ld a,(ix+v1)
 ; neg
  ld (ix+v2),a
    call setpage12tohandleobjectmovement
  scf
  ret


checksheerslidingguy: 


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;next tile
  ld bc,17     
  ld    de,0 ;6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    242                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  ;nc is gevonden

  call setpage12tohandleobjectmovement
  ret




checksheerobjecttiledown: 

  call nosheercheck
  ret z



  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right


;next tile
  ld    b,0
  ld c,(ix+sizeY)
  
  
  ld    de,0 ;6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.endcheck
  cp    242                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.diagonaltilegoingdownfoundleft


;we checken 1 tile vooruit en als we 1 pixel lager de schuine tile aantreffen dan verlagen we de yobject met 1 pixel
;van te voren zodat die netjes de schuine tile in schuift.
;next tile
  ld    b,0
  ld c,(ix+sizeY)

  inc bc ;een pixel lager zoeken
  ld    de,-1 ;5 een lijkt beter te werken                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.endcheck
  cp    242                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.lower1pixel
  
  call setpage12tohandleobjectmovement

  xor a
  ret
  
  

.movingright:


;next tile
  ld    b,0
  ld c,(ix+sizeY)
  
  ld    de,0 ;6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.diagonaltilegoingdownfound 


;we checken 1 tile vooruit en als we 1 pixel lager de schuine tile aantreffen dan verlagen we de yobject met 1 pixel
;van te voren zodat die netjes de schuine tile in schuift.
;next tile
  ld    b,0
  ld c,(ix+sizeY)

  inc bc ;een pixel lager zoeken
  ld    de,1 ;5 een lijkt beter te werken                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    247                     ;check for tilenumber 247-255 (all diagonal tiles going down)
  jp    nc,.lower1pixel

.endcheck:
  call setpage12tohandleobjectmovement

  xor a

  ret

.diagonaltilegoingdownfoundleft:


  call locksheertile.left

  ld a,(ix+v1)
  neg
  ld (ix+v2),a
    call setpage12tohandleobjectmovement
  scf
  ret


.diagonaltilegoingdownfound:

  call locksheertile.right

  ld a,(ix+v1)
  ld (ix+v2),a
    call setpage12tohandleobjectmovement
  scf
  ret

.lower1pixel:


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  inc hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h
    call setpage12tohandleobjectmovement
  scf
  ret  





;checkt alleen naar links en rechts en returned een flag in de carry
checkobjecttilelr:

;map tijdelijk terughalen

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right


  ;check collision with wall left
  ld    bc,8                   ;vertical increase to tilecheck
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp    c,.endcheckright          ;c=no foreground tiles found
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp c,.done
 
    call setpage12tohandleobjectmovement 
  
  xor a
  
  ret
  
.movingright:  
  
  
    ;check collision with wall right
  ld    bc,8                   ;vertical increase to tilecheck
  ld    de,009                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp c,.endcheckright
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp c,.done

.endcheckright:

    call setpage12tohandleobjectmovement

  xor a ;reset carry


  ret

.done:

    call setpage12tohandleobjectmovement

  scf
  ret


;checkt alle kanten op behalve omhoog met BC word geset B = v2 schuine tiles C = V2 bij vallen returned een carry flag als er solid foreground is gevonden niet als er een schuine tile is gevonden
checkobjecttilescheckwall:

;map tijdelijk terughalen

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right
  
.movingleft:
  ;check collision with wall left
  ld    b,0
  ld    c,(ix+sizeY)            ;vertical increase to tilecheck
  dec bc
  ld    de,-08                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp    c,.endcheckleftwall     ;c=no foreground tiles found
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp nc,.endcheckleftwall
  call lockobjectx.left
  
    call setpage12tohandleobjectmovement
  
  scf
  ret
.endcheckleftwall:
  ;/check collision with wall left
  
    call setpage12tohandleobjectmovement
  
  xor a
  ret

.movingright:
  ;check collision with wall right
  ld    b,0
  ld c,(ix+sizeY)
  dec bc
  ld    de,007                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp    c,.endcheckrightwall     ;c=no foreground tiles found
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp nc,.endcheckrightwall
  call lockobjectx.right

    call setpage12tohandleobjectmovement

  scf
  ret
     
.endcheckrightwall:
  ;/check collision with wall right

  call setpage12tohandleobjectmovement

  xor a
  ret

  
  
  
  
  
;test test==============================test test==================================
checkobjecttilescheckwallforchuck:

;map tijdelijk terughalen

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right
  
.movingleft:
  ;check collision with wall left
  ld    b,0
  ld    c,(ix+sizeY)            ;vertical increase to tilecheck
  dec bc
  ld    de,-16                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp    c,.endcheckleftwall     ;c=no foreground tiles found
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp nc,.endcheckleftwall
;  call lockobjectx.left
  
    call setpage12tohandleobjectmovement
  
  scf
  ret
.endcheckleftwall:
  ;/check collision with wall left
  
    call setpage12tohandleobjectmovement
  
  xor a
  ret

.movingright:
  ;check collision with wall right
  ld    b,0
  ld c,(ix+sizeY)
  dec bc
  ld    de,016                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber < 192 (all background tiles)
  jp    c,.endcheckrightwall     ;c=no foreground tiles found
  cp    234                     ;check for tilenumber 192-233 (all hard foreground tiles)
  jp nc,.endcheckrightwall
;  call lockobjectx.right

    call setpage12tohandleobjectmovement

  scf
  ret
     
.endcheckrightwall:
  ;/check collision with wall right

  call setpage12tohandleobjectmovement

  xor a
  ret
;/==========================end of test==============================


checkobjecttilesfloorbelowfeet:

  ;fetch map back into the ram

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  ld    b,0
  ld    c,(ix+sizeY)
  inc   bc ;BUG: nog niet precies
  ld    de,-08                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    c,.nofloorfound
  inc   hl
  inc   hl
  ld    a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    c,.nofloorfound
  

.floorfound:
  
  call setpage12tohandleobjectmovement
  
    xor a
    
    ret
  
  
.nofloorfound:

  call setpage12tohandleobjectmovement

  scf

  ret


checkobjecttilesflooronly:


;map tijdelijk terughalen

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;also check specificaly for the vines

  ld    b,0
  ld    c,(ix+sizeY)
  inc   bc ;BUG: nog niet precies
  ld    de,-08                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    nc,.floorfound
  inc   hl
  inc   hl
  ld    a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    c,.nofloorfound
 
 
   call setpage12tohandleobjectmovement
 
  scf
  
  ret
  
  
.nofloorfound:
 
  call setpage12tohandleobjectmovement
  
  xor a
  
  ret

.floorfound:

  call setpage12tohandleobjectmovement

  scf

  ret

checkobjecttilesfloor:


;check if object is on the left edge of the map and return a NC

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  xor a
  sbc hl,de
  jp c,.floorfound


  ld a,(ix+standingonspr)
  and a
  jp nz,objectspritelock ;make this a call which returns to itself dangerous but functional


;map tijdelijk terughalen

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    b,0
  ld    c,(ix+sizeY)
  inc   bc ;BUG: nog niet precies
  ld    de,-07;-08                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    nc,.floorfound
  inc   hl
  ld a,(ix+xobject) ;get lowest byte
  and 7
  cp 7
  jp z,.skip ;Why?? well because the first check is pixel precise the second however round of so it isn't this is how we check if the object is touching the edge
  inc   hl
.skip:  
  ld    a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp    c,.nofloorfound

.floorfound:
  
  
  call  lockyobject

  xor a ;carry resetten!
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  ld    (ix+objectfp),a

  call setpage12tohandleobjectmovement

  ret

.nofloorfound:
  call objectfallacceleration ;valversnelling ophalen  

;  ld c,vershellspeed

  ld    (ix+v2),c               ;v2 is vertical movement speed and direction  

  call setpage12tohandleobjectmovement

  scf

  ret



destroyobject:

;  ld (ix+active?),0
  ld (ix+objectreturn),1

.objectreturns:
  
  ;we zetten dan de sprite ergens ver weg dus bestaat hij niet meer
  ld    (ix+yobject),0          ;y value of sprite (16 bits)
  ld    (ix+yobject+1),0       ;y value of sprite (16 bits)


  ld hl,65535
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld (ix+objectrelease),0 ;this to prevent doubling or yosi not reappear

  call removeobjectspat

  ret  







;fetch height distance to object return distance in A (8bits) and HL (16 bits) returns 0 if mario is below the object HL remains intact
getmarioheight:


  ld de,(marioy)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  xor a
  sbc hl,de
  jp c,.negative
  
  ld a,l

  ret

.negative:

  xor a
  scf
  ret


;de afstand van mario tot een object fetchen. De afstand is een absolute waarde die word teruggegeven in register C
getmariodistance:

  ld c,-1 ;waarde is altijd max

;x coordinaten van de object inlezen
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)


;x coordinaten van mario inlezen
   ld hl,(mariox)
   
   xor a ;carry reset
   sbc hl,de ;verschil bepalen
   ;nu komt er 8 of -8 uit boeit niet maar we gaan dus kijken of de x coordinaten overeen komen
   ld a,h ;h moet 0 zijn! anders hebben we dus een min getal
   and a
   jp nz,.nextcheck
   ld a,l
   ld c,a

  ret
  
.nextcheck:

    ex de,hl
  
    ld de,(mariox) ;coordinaten refetchen
   ;andere kant op checken oftwel naar links
   xor a
   sbc hl,de ;verschil bepalen
   ;nu komt er 8 of -8 uit boeit niet maar we gaan dus kijken of de x coordinaten overeen komen
   ld a,h ;h moet 0 zijn!
   and a
   ret nz
   ld a,l
   ld c,a
 ;  sub a,8

  ret



;we kijken of mario links of rechts van het object staat de waarde komt in de carry flag carry=positief nc=negatief
getmarioleftright:  ;out c=mario is left of object
  ;find left border (we use the CENTER of the object)
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-17
  add hl,de
  
  ld  a,(ix+sizeX)
  srl a           ;/2 
  ld  d,0
  ld  e,a
  add hl,de
  
  ld  de,(mariox)
  or  a  
  sbc hl,de
  ret

mariohittimeout:

  ld a,(mariogod)
  inc a
  cp 80
  jp z,.nogod
  ld (mariogod),a

  xor a ;carry resetten!
  
  ret
  
.nogod:

  xor a
    ld (mariogod),a
  ret



checkmariointeractshoot:


  xor a
;ook niet als we geen veertje hebben
  ld a,(mariostate)
  dec a ;carry bit niet geset ;P
  dec a
  dec a
  ret nz

;niet checken als we niet schieten
  ld a,(marioshot)
  and a
  ret z

;do not check if mario is on yoshi

  ld a,(marioyoshi)
  dec a
  ret z


  ;check if mario within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-28
  add hl,de
  
  ld  de,(mariox)
  or  a  
  sbc hl,de
  
  ret nc            ;left border not found, no collision

  ld  a,(ix+sizeX)
  add a,32 ;9
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

  ret nc            ;right border not found, no collision
  ;/check if mario within x range of object

  ;check if mario within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-28
  add hl,de
  
  ld  de,(marioy)
  or  a  
  sbc hl,de
  
  ret nc            ;top border not found, no collision

  ld  a,(ix+sizeY)
  add a,15
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

;  ret nc            ;bottom border not found, no collision
  ;/check if mario within x range of object
  
 
  
  ret  
  


;de rampjump is wanneer een enemy met 2 hitpoints word geraakt met een rampslide maar niet direct doodgaat maar gewoon 1 hitpoint verliest. Mario
;verliest dan zijn snelheid en maakt een klein sprongetje. dit word aangeroepen vanuit de eventhandler on death van de object
setrampjump:

  ld (ix+deadly),0 ;object tijdelijk weak maken
  ld (ix+deathtimer),0 ;timer setten

;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
  
;  ld e,(ix+sizeY)
;  ld d,0

;  xor a
;  sbc hl,de
;  ld (marioy),hl

;snelheid opslaan
  ld a,(mariospeed)
  ld (tempspeed),a 

  ld a,1
  ld (rampjump),a

  jp mariointeract.setjump



;object sterft door een schelpje of een rampslide van mario dan moet hij anders verdwijnen 
;we gebruiken de deathtimer als pointer om zo te voorkomen dat de hele tabel volloopt met bullshit
;omdat het object uit het beeld verdwijnt ruimt de eventhandler hem vanzelf voor ons op :)
objectdeathjump:

  ld a,(ix+deathtimer)
  cp 8
  jp nc,.fall

  
  ld d,0
  ld e,(ix+deathtimer) ;pointer fetchen
  ld hl,objectjumpsmall ;waarde fetchen
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.fall
  ld e,a ;jumpspeed in de verplaatsen
  ld d,0
  
  ;set de jumpccordinates
  
  ;fetch y coordinaten
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  sbc hl,de ;substract
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;schrijf weg
  
  ret
  

   
.fall:

  ld a,(ix+deathtimer)
  cp 16
  jp nc,.maxspeed


  ld d,0
  ld e,(ix+deathtimer) ;pointer fetchen
  ld a,e
  sub 8 ;compenseren want de timer loopt gewoon door
  ld e,a
  
  ld hl,objectfallacc ;waarde fetchen
  
  ld a,(waterlevel)
  and a
  call nz,.fallslow  
  
  add hl,de
  ld a,(hl)

  ld e,a ;jumpspeed in de verplaatsen
  ld d,0
  
  ;set de jumpccordinates
  
  ;fetch y coordinaten
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de ;add
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;schrijf weg



  ret


.maxspeed:


  ld de,10
  ld a,(waterlevel)
  and a
  call nz,.slow
  
  ;fetch y coordinaten
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de ;add
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;schrijf weg

    ret

.slow:

  ld de,2
  ld (ix+v1),0

  ret


.fallslow:
  
  ld hl,fallslowtable
  ret


;valversnelling returned de waarde in register C
objectfallacceleration:


  ld a,(ix+objectfp)
  cp 8
  jp nc,.maxspeed

  
  ld hl,objectfallacc ;waarde fetchen
  
  ld a,(waterlevel)
  and a
  call nz,.fallslow  

  ;check for a water level
  ld de,(waterheight)
  ld a,e
  or d
  call nz,.fallslow ;no waterlevel present 

  ld d,0
  ld e,(ix+objectfp) ;pointer fetchen  
  
  add hl,de
  ld a,(hl)
  
  ld c,a

  ;ld c,4

  ret


.maxspeed:


  ld a,(waterlevel)
  and a
  jp nz,.slow 


    ld c,6
    
  ;check for a water level
  ld hl,(waterheight)
  ld a,l
  or h
  ret z ;no waterlevel present
  
  ld de,32;64
  xor a
  sbc hl,de
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.slow
  


    ret

.slow:

  ld c,2

  ld a,(ix+movepat+1)
  and a
  jp nz,.skipchecks
  ld a,(ix+movepat)
  cp 30
  ret z ;don't count in bullets
  ld a,(currentlevel)
  cp 6
  ret nz
;  cp 64
;  ret nz
  
.skipchecks:
  
  ld c,1
  ld (ix+v1),0


  ret


.fallslow:
  
  ld hl,fallslowtable
  ret


;object jump tabellen, hier voeg ik later ook de de falltable aan toe, valversnelling is gewoon mooier
;objectjumpsmall:      db 6,5,4,2,2,1,1,1,0 ;jumpje
objectjumpsmall:      db 0,10,8,6,4,3,2,1,0 ;jumpje
;objectfallspeedtable: db 1,2,4,5,6,7,8,9,10 ;vallen
;objectfallspeedtable: db 1,1,1,2,2,2,4,4,6 ;vallen ;is nu hetzelfde als objectfallacc
objectfallacc:        db 1,1,1,2,2,2,4,4,6 ;vallen vanaf een platformpje
fallslowtable:        db 1,1,1,1,1,2,2,2,2 ;vallen vanaf een platformpje



.maxspeed:


  ld de,10
  ;fetch y coordinaten
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de ;add
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;schrijf weg

    ret

 
  

;get the bkgaddress of any given object: IN IX of object out bkgadress in HL Y only in DE
getblockcoordinates:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    

  ld d,0
  ld e,l

;compenseer op de offsets    
   inc e
;   inc e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de ;yposition found of block

  push hl
  
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)

  srl d ;nu moet je hl delen door 8
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    

  dec e

  add hl,de
  ;now we have the tilex and tiley from where the block is currently at

  pop de ;not sure if this value is used somewhere

  ret


createbrownblock:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a
 
  call getblockcoordinates
  
  ;now write the block into the map
  ld a,204
  ld (hl),a
  inc hl
  ld a,205
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  ld a,207
  ld (hl),a
  dec hl
  ld a,206
  ld (hl),a

  
  jp setpage12tohandleobjectmovement
  

;  ret

;in = b = tilenumber of first block
createanyblock:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a
 
  call getblockcoordinates
  inc hl
  
  ;now write the block into the map
  ld a,b
  ld (hl),a
  inc hl
  inc a
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  inc a
  inc a
  ld (hl),a
  dec hl
  dec a
  ld (hl),a

  
  jp setpage12tohandleobjectmovement
  

 ; ret


;=======================================[cheat]================================================
;staat nu in ROM
;==============================================================================================
 
    
 
;=======================================[mariotransform]=======================================
;Staat nu in ROM
;==============================================================================================
  
  
checkjumpkeyhold:
  

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  call checkjumpkeyholdram
  
;  push af
  ex af,af'
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

;  pop af
  ex af,af'
  
  
  
  ret
  
  

;check op de juiste voorwaarden en release dan de jumpkey
checkjumpkeyholdram:

;is de keyhold al actief?
  ld a,(jumpkeyhold)
  dec a
  jp z,.keyholdactive


  ld a,(marionotdown)
    xor %00000001 ;invert
  ld b,a
  ld a,(mariojumpkey)
  and b
  ret z
  
  ld a,1
  ld (jumpkeyhold),a

  ret
  
.keyholdactive:

  ld a,(marionotdown)
  ld b,a
  ld a,(mariojumpkey)
  xor %00000001
  and b
  ret z

  ld a,(forcetile)
  and a
  jp nz,.skipchecks

;if we are standing on a sprite we must ignore any tilechecks. So that the sprite is treated as a tile.
  ld a,(standingonsprite)
  dec a
  jp z,.skipchecks

;check twee tiles lager waar mario is  
  call .checktilebelowmarioh
  ret nc

.skipchecks:
  
  xor a
  ld (jumpkeyhold),a
  
  ret
  
  



;tile checken alleen foreground tiles checken en op basis daarvan keyhold resetten
.checktilebelowmarioh:


  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,5 ;4 tiles lager, look below mario's feet
  
  ld hl,bgrmapaddr ;(das $4000)
  ld de,(maplenght) ;das 638 voor de map van level00
  or a
  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
  
  ld b,a
.searchmapy:
  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
 djnz  .searchmapy
.yset:

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat


  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc hl
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret c

;nog een keer lager checken  
  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapyh:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapyh
;.yseth:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat


  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc hl
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret


blockhandler:                   ;handles blocks that mario activated
  ld    a,(blocksactive?)       ;check if there are active blocks
  or    a
  ret   z                       ;no block is active, return

  call  handleactivedblocks
;  call  activatenewblocks
;  ret

activatenewblocks:
  ld    a,(activatenewblock?)   ;is there a block that has been activated in the last frame ?
  or    a 
  ret   z

  xor   a
  ld    (activatenewblock?),a   ;consider activation of this block taken care of

  ld    ix,blocklist            ;first check for empty space in our blocklist
  ld    de,blocklenght
  ld    b,amountofblocksinlist  ;check all blocks in list
.loop:
  cp    (ix)                    ;check if first byte in the blocklist is a 0 (0 means this block is inactive)
  jp    z,.blockfound
  add   ix,de                   ;check next block
  djnz  .loop
  ret                           ;no inactive block found, this means all the blocks in the list are active (probably they are all rotating blocks currently rotating)

.blockfound:                    ;inactive block is found, lets set variables for this block
  ld    (ix+0),1                ;this block is now active
  ld    (ix+2),0                ;step 0 of this block will be handled next frame

  ld    hl,(blockaddress)       ;now set the blockaddress in the blocklist 
;now first determine the topleft of the block, because mario could have jumped left or right of block
  bit   0,(hl)                  ;if the tilenumber mario jumped at is even, then mario jumped at the left part of the tile
  jp    z,.endsetxblock
  dec   hl                      ;mario jumped at the right part of the block, so move blockaddress left 1 tile
.endsetxblock:
  ld    a,(hl)                  ;tilenumber (already on the left side) of block that mario jumped at
  cp    192                     ;192 is lefttop of questionmark
  jp    z,.endsetyblock
  cp    196                     ;196 is lefttop of exclamationmark
  jp    z,.endsetyblock
  cp    200                     ;200 is lefttop of rotating block
  jp    z,.endsetyblock

  ld    de,(maplenght)
  xor   a
  sbc   hl,de                   ;move up 1 tile in the map, now we have found the topleft park of the block
.endsetyblock:
  ld    (ix+3),h                ;write address of topleft tile of block into blocklist
  ld    (ix+4),l
;/now first determine the topleft of the block, because mario could have jumped left or right of block  

;now set block type
  ld    a,(hl)                  ;tilenumber (already on the lefttop side) of block that mario jumped at
  ld    b,0                     ;b=0 is questionmark
  cp    192                     ;192 is questionmark
  jp    z,.settype
  ld    b,1                     ;b=1 is exclamationmark
  cp    196                     ;192 is exclamationmark
  jp    z,.settype
  ld    b,2                     ;b=2 is rotating block
.settype:
  ld    (ix+1),b                ;set type of block
;/now set block type

;remove block from map
;  xor   a
;  ld    (hl),a
;  inc   hl
;  ld    (hl),a
;  ld    de,(maplenght)
;  add   hl,de
;  ld    (hl),a
;  dec   hl
;  ld    (hl),a
;/remove block from map
 
  push af
  push hl
;  push de
  call makebkgblock.usehl
; ld		a,(slot.ram)	;back to full RAM
;	out		($a8),a
;  pop de
	pop hl
  pop af

  ld    de,(maplenght)
  add   hl,de

;when mario jumps at block, a sprite is activated that looks just like the object
  and   a
  ld    de,$4000
  sbc   hl,de
  ld    de,(maplenght)
  ld    b,-2
.ysearchloop:
  sbc   hl,de
  inc   b
  jp    nc,.ysearchloop
  add   hl,de

  push ix

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  dec b
  dec b
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    a,(ix+1)                ;type of block (0 is questionmark, 1 is exclamationmark, 2 is rotating block)
  add   a,10
  ld    (iy+5),a                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
;/when mario jumps at block, a sprite is activated that looks just like the object

  pop ix
  ret

  ld a,(currentlevel)
  cp 51
  ret z



handleactivedblocks:
  xor   a : ld    hl,blocklist+(0*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(1*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(2*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(3*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  ret

.activeblockfound:
  inc   hl                      ;type
  ld    a,(hl)
  or    a                       ;question mark
  jp    z,questionmark
  dec   a
  jp    z,exclamationmark

rotatingblock:
  inc   hl                      ;step
  inc   (hl)                    ;increase step
  ld    a,(hl)
  cp    3
  ret   c                       ;step 1 = make block move upwards for a few frames
  jp    z,.step2                ;step 2 = place background rotating block
  cp    130                     ;step 3 = place foreground rotating block / end
  ret   c

.step3:                         ;place foreground rotating block / end

;get the mario distance and prevent this step if mario is near
  push hl
  ld de,0
  ld b,3
  call checkgeneraltile ;adress of mario in hl
  ex de,hl  
  pop hl
  
  inc hl ;address
  ld b,(hl)
  inc hl
  ld c,(hl)
  dec hl
  dec hl ;restore hl value
  
  push hl ;push again
  
  ld h,b
  ld l,c ;get the value from theblock
  
  xor a
  sbc hl,de
    pop hl
  jp z,.resetstep3



  ld    a,(framecounter)        
  and   15
  cp    3
  ret   nc                      ;return if block is not in startposition at this point
                                ;place foreground rotating block / end
  dec   hl
  dec   hl                      ;active?
  ld    (hl),0                  ;this block is no longer active
  inc   hl
  inc   hl
  inc   hl                      ;block address (2 bytes)
  ld    d,(hl)
  inc   hl                      ;block address (2 bytes)
  ld    e,(hl)

  ld    ixh,d
  ld    ixl,e

  ld    (ix),200                ;tiles 200,201,202,203 are tiles of rotatingblock foreground
  ld    (ix+1),201
  ld    de,(maplenght)
  add   ix,de
  ld    (ix),202
  ld    (ix+1),203 ;BUG!! something bad is happening here causing an illegal ram write during mario flight on specific maps
  jp    checkifanyblocksarestillactive
;  ret

.resetstep3:

  ld (hl),100
  ret



.step2:                         ;place background rotating block
  inc   hl                      ;block address (2 bytes)
  ld    d,(hl)
  inc   hl                      ;block address (2 bytes)
  ld    e,(hl)

  ld    ixh,d
  ld    ixl,e

  ld    (ix),188
  ld    (ix+1),189
  ld    de,(maplenght)
  add   ix,de
  ld    (ix),190
  ld    (ix+1),191
  ret
  
exclamationmark:
questionmark:
  inc   hl                      ;step
  inc   (hl)                    ;increase step
  ld    a,(hl)
  cp    3                       ;first 3 frames the questionmark spriteobject moves up
  ret   c                       ;step 1 = make block move upwards for a few frames

  dec   hl                      ;type
  dec   hl                      ;active?
  ld    (hl),0                  ;inactive
  inc   hl                      ;type
  inc   hl                      ;step
  inc   hl                      ;block address (2 bytes)
  ld    d,(hl)
  inc   hl                      ;block address (2 bytes)
  ld    e,(hl)

  ld    ixh,d
  ld    ixl,e

  ld a,(multiblockactive)
  dec a
  jp z,.restoreblock

  ld    (ix),204                ;when question mark is hit
  ld    (ix+1),205              ;replace it with brown block
  ld    de,(maplenght)
  add   ix,de
  ld    (ix),206
  ld    (ix+1),207
  ret

.restoreblock:

  ld    (ix),192                ;when question mark is hit
  ld    (ix+1),193              ;replace it with questionmark block
  ld    de,(maplenght)
  add   ix,de
  ld    (ix),194
  ld    (ix+1),195
  ret
  


checkifanyblocksarestillactive:
  xor   a
  ld    hl,blocklist            ;first check for empty space in our blocklist
  ld    de,blocklenght
  ld    b,amountofblocksinlist  ;check all blocks in list
.loop:
  cp    (hl)                    ;check if first byte in the blocklist is a 0 (0 means this block is inactive)
  ret   nz                      ;active block found
  add   hl,de                   ;check next block
  djnz  .loop
;at this point there are no active block, so totaly disable the blockhandler
  ld    (blocksactive?),a       ;check if there are active blocks
  ret    

blocksactive?:        db  0     ;if there are any active blocks this value is 1
activatenewblock?:    db  0     ;did mario activate a block in the last frame ?(if yes, then this value is 1)
blockaddress:         dw  0     ;this (16-bit) value is a pointer to the map. It points to the block that was activated by mario in the last frame
blocklenght:          equ 5
amountofblocksinlist: equ 4
blocklist:                      ;active?, type,step, block address (2 bytes)
  db    0,0,0,0,0 ;block 1
  db    0,0,0,0,0 ;block 2
  db    0,0,0,0,0 ;block 3
  db    0,0,0,0,0 ;block 4
  
  


AnimateTiles:

  ld a,(animatespecial)
  dec a
  jp z,.dotwopageflip

  ld a,(ditileanimations)
  dec a
  ret z
  
  ld a,(notileanimations)
  dec a
  ret z


  ld    a,(framecounter)
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz

.step4:
  ld    a,%0001'1000    ;character table at $c000
	ld		b,%0000'0011    ;color table at $e000
  jp    .setcharacterandcolortable

.step3:
  ld    a,%0001'0000    ;character table at $8000
	ld		b,%0000'0010    ;color table at $a000
  jp    .setcharacterandcolortable

.step2:
  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  jp    .setcharacterandcolortable

.step1:
  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
;  jp    .setcharacterandcolortable

.setcharacterandcolortable:
;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    4     4    0   0  	A16 A15 A14 A13 0   0
	di
	out		($99),a         ;set character table address		
	ld		a,4+128
	out		($99),a

;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    3     3    A13 0  	0   1   1   1   1   1
;    10    10   0   0  	0   0   0   A16 A15 A14

  ld    a,b
	out		($99),a         ;set color table address			
	ld		a,10+128
  ei
	out		($99),a
  ret

.dotwopageflip:
;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp nz,.step1
  jp .step2


;fetch the adress back from the tilebuffer and rewrite the value onto the map
restorecoin:

  xor a
  ld (bluepboxactive),a
  ld a,2
  ld (setobjectscurrentscreen?),a
  
  ld ix,brownblocktilebuffer
  
.loop:
    ld de,7
    add ix,de
    ld a,(ix)
    cp 2
    ret z ;end of list reached so we are done ;must become ret z
    and a
    ret z
    
    ld l,(ix+5)
    ld h,(ix+6) ;fetch the bkgadress
    
    ld a,(ix+1)
    ld (hl),a
    inc hl
    ld a,(ix+2)
    ld (hl),a
    
    ld de,(maplenght)
    add hl,de
    dec hl
    
    ld a,(ix+3)
    ld (hl),a
    inc hl
    ld a,(ix+4)
    ld (hl),a
    
    ;reset the rest of the table values
    ld (ix),0 ;reset the table values
    ld (ix+1),0
    ld (ix+2),0
    ld (ix+3),0
    ld (ix+4),0
    ld (ix+5),0
    ld (ix+6),0

  jp .loop

  


transformintobrownblock: ;in location of object (called from object). (ix+objectrelease) amount of consecutive blocks to create
;in de = mapoffset


  ld b,(ix+objectrelease) ;read the objectrelease value from the object that does the call
  push bc


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;fetch the bkgadress  

  ld de,0
  ld bc,8
  call objecttilecheck

;tileadress is now in hl

  ld (hlvar),hl ;store the adress obtained

;in the case of supercoins we want to store and create multiple blocks in line
  pop bc

.masterloop:

  push bc
  push ix
  
  ld ix,brownblocktilebuffer 
  ld de,7 ;length of the list
  
.search:  

  add ix,de
  ld a,(ix)
  cp 2
  jp z,.nostore ;table is full
  cp 1
  jp z,.search ;do the next spot
  ;found a free spot search again if the mapadress is allready in use
 
  ld ix,brownblocktilebuffer ;reset
  
.loop2:

  ld de,7
  add ix,de
  ld a,(ix)
  cp 2
  jp z,.nostore ;end of the table
  
  ld c,(ix+5)
  ld b,(ix+6)
  ld hl,(hlvar) ;bkgadress
  xor a
  sbc hl,bc
  jp z,.nostore ;found an equal adress
 
 ;space is empty so store here
  ld a,(ix)
  and a
  jp z,.store
  
  jp .loop2
;so we found that this tile is not stored yet now we store it

.store:
  ld (ix),1
  
  ld hl,(hlvar) ;refetch the adress
  ld (ix+5),l
  ld (ix+6),h ;write it into the table
  
  ld a,(hl)
  ld (ix+1),a
  inc hl
  ld a,(hl)
  ld (ix+2),a
  dec hl
  
  ld de,(maplenght)
  add hl,de
  
  ld a,(hl)
  ld (ix+3),a
  inc hl
  ld a,(hl) 
  ld (ix+4),a


.nostore:

  pop ix

  ld hl,(hlvar) ;refetch the adress

;write the new tiles into the background

  ld a,(ix+hitpoints)
  dec a
  jp z,.writezero


  ld a,204
  ld (hl),a
  inc hl
  inc a
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  inc a
  dec hl
  ld (hl),a
  inc hl
  inc a
  ld (hl),a

.writezerodone:

;set the mapadress to next block location
  inc hl
  ld de,(maplenght)
  xor a
  sbc hl,de

  ld (hlvar),hl ;write it back
 
  pop bc ;pop back the preread objectrelease value and determine if sufficient blocks where spawn
  dec b
  jp nz,.masterloop
 
  
  call setpage12tohandleobjectmovement
  
  ret

.writezero:

  ld a,(hl)
  cp 204
  jp nz,.skipblock

  xor a
  ld (hl),a
  inc hl
;  inc a
  ld (hl),a
  ld de,(maplenght)
  add hl,de
;  inc a
  dec hl
  ld (hl),a
  inc hl
;  inc a
  ld (hl),a

  jp .writezerodone

.skipblock:

  inc hl
  ld de,(maplenght)
  add hl,de
  
  jp .writezerodone

brownblocktilebuffer:
;tilebuffer for the brown blocks if they spawn on a non zero tile
;in use, tilenumbers ,     mapaddress
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;5
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;10
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;15
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;20
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;25
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0
db 0,      0,0,0,0          dw  0 ;30
db 0,      0,0,0,0          dw  0 
db 0,      0,0,0,0          dw  0 
db 0,      0,0,0,0          dw  0 
db 0,      0,0,0,0          dw  0 
db 0,      0,0,0,0          dw  0 ;35
db 2 ;boundary of the table




makevine:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;fetch the bkgadress  

;  ld de,0
;  ld bc,8
;  call objecttilecheck

  ld de,(maplenght)
  add hl,de

;write the new tiles into the background

  ld a,(hl)
  cp 191
  jp nc,.skipfirstline

  ld a,252
  ld (hl),a
  inc hl
  inc a
  ld (hl),a  
  ld de,(maplenght)
  add hl,de
  inc a
  dec hl
  ld (hl),a
  inc hl
  inc a
  ld (hl),a

;reset the v4 counter
  ld (ix+v4),1

  
  call setpage12tohandleobjectmovement
  
  ret

.skipfirstline: 

  ld a,254 
  ld de,(maplenght)
  add hl,de
  ld (hl),a
  inc hl
  inc a
  ld (hl),a

;reset the v4 counter
  ld (ix+v4),1

  
  call setpage12tohandleobjectmovement
  
  ret

makebkgblock:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;fetch the bkgadress  

  ld de,0
  ld bc,16
  call objecttilecheck

.usehl:

  push hl
  ld hl,levelbackgroundtileslist
  ld de,0
  xor a
  sbc hl,de
  pop hl
  jp nz,.writetilesfromlist



.writeblank:
;write the new tiles into the background

  xor a
  ld (hl),a
  inc hl
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  dec hl
  ld (hl),a
  inc hl
  ld (hl),a
  
  call setpage12tohandleobjectmovement
  
  ret

.writetilesfromlist:

  ld (devar),hl ;push the bkgadress in devar
  ;ld a,6 ;loop through the list
  ld a,bkgbufferrows
  ld bc,lenghtlevelbackgroundtileslist
  

  ld iy,levelbackgroundtileslist ;get the list in iy (use iy as hl)


.fetchloop:

  ld de,(devar) ;put bkgadress in de
  ld l,(iy+0)
  ld h,(iy+1)
  and a ;no xor otherwise register is set to 0
  sbc hl,de
  jp z,.adresfound
  add iy,bc
  dec a
  jp nz,.fetchloop
  
  ex de,hl
  
  jp .writeblank


.adresfound:
 
   ex de,hl

  ld a,(iy+2)
  ld (hl),a
  inc hl
  ld a,(iy+3)
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  dec hl
  ld a,(iy+4)
  ld (hl),a
  inc hl
  ld a,(iy+5)
  ld (hl),a


  call setpage12tohandleobjectmovement
  
  ret


makemontyhole:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;fetch the bkgadress  

  ld de,0
  ld bc,8
  call objecttilecheck


;write the new tiles into the background

  ld a,168
  ld (hl),a
  inc hl
  inc a
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  inc a
  dec hl
  ld (hl),a
  inc hl
  inc a
  ld (hl),a
  
  call setpage12tohandleobjectmovement
  
  ret




checkforblockcrush:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  call checkforblockcrushram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block34

  pop af
  
  ret

writetilesfromlistfromrom:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  call writetilesfromlist
  
  jp setpage12tohandleobjectmovement
  ;ret


writetilesfromlist:

  ld (devar),hl ;push the bkgadress in devar
  ;ld a,6 ;loop through the list
  ld a,bkgbufferrows
  ld bc,lenghtlevelbackgroundtileslist
  

  ld iy,levelbackgroundtileslist ;get the list in iy (use iy as hl)


.fetchloop:

  ld de,(devar) ;put bkgadress in de
  ld l,(iy+0)
  ld h,(iy+1)
  and a ;no xor otherwise register is set to 0
  sbc hl,de
  jp z,.adresfound
  add iy,bc
  dec a
  jp nz,.fetchloop
  
  ex de,hl
  
  jp .writeblank


.adresfound:
 
   ex de,hl

  ld a,(iy+2)
  ld (hl),a
  inc hl
  ld a,(iy+3)
  ld (hl),a
  ld de,(maplenght)
  add hl,de
  dec hl
  ld a,(iy+4)
  ld (hl),a
  inc hl
  ld a,(iy+5)
  ld (hl),a

  ret
 ; jp .continue
;  ret


.writeblank:
;write the new tiles into the background

  xor   a
  ld    (hl),a
  inc   hl
  ld    (hl),a
  ld    de,(maplenght)
  add   hl,de
  ld    (hl),a
  dec   hl
  ld    (hl),a
  
;  jp .continue

  ret


;we checken of we een crushable block hebben geraakt return hl->mapadress block  c=block found
;tevens handeld deze routine direct het verwijderen van de block af maar laat de mariophysics
;zijn eigen ding doen
checkforblockcrushram:


;spinjump en mario moet groot zijn
  ld a,(mariospinjump)
  and a
  ret z
  
  ld a,(mariostate)
  and a
  ret z


  ld b,4
  ld de,0
  call checkgeneraltile
  ld a,(hl)
  cp 200
  jp z,crushblock
  inc hl
  ld a,(hl)
  cp 200
  jp z,crushblock
  dec hl
  ld a,(hl)
  cp 201
  jp z,.sethl

  xor a
  ret 

.sethl:

  dec hl

;crushes any given block that is set in adress ---> hl
crushblock:

  ld a,(currentlevel)
  cp 51
  ret z ;do not do this one.
  cp 52
  ret z

  call writetilesfromlist

.continue:

;when mario jumps at block, a sprite is activated that looks just like the object
  and   a
  ld    de,$4000
  sbc   hl,de
  ld    de,(maplenght)
  ld    b,-2
.ysearchloop:
  sbc   hl,de
  inc   b
  jp    nc,.ysearchloop
  add   hl,de

  ld a,b

  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),a

;we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),2
    ld (ix+v2),8

  ld hl,(blockhlstorage) ;hl opslaan
  ld a,(blockbcstorage)
  ld b,a

;object 2
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld a,(blockbcstorage)
  ld b,a

;object 3

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
    ld (ix+v2),8

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld a,(blockbcstorage)
  ld b,a

;object 4

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),2

  call Playsfx.breakblock

    scf
  ret

  
;soort push hl maar dan beter  
blockhlstorage: dw 0
;blockbstorage: db 0
blockbcstorage: dw 0


removebanzaibillrom:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  call removebanzaibill 

 call setpage12tohandleobjectmovement
 
 ret


removebanzaibill:

;object must be in movement
  ld a,(ix+v3)
  dec a
  ret nz


  ;first remove the old part
  ld hl,tilebuffer
  ld de,(banzaimapadress)
  ld b,8

.copybuffertomap:
    push bc
    ldi
    ldi
    ldi
    ldi
    pop bc
    push hl
    ex de,hl ;de is now in hl
    ld de,(maplenght)
    add hl,de
    ld de,4
    xor a
    sbc hl,de
    ex de,hl
    pop hl    
    
  djnz .copybuffertomap


  ret


movebanzaibill:

;mario sterft alles stop zetten
  ld a,(mariotransforming)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ;first remove the old part
  ld hl,tilebuffer
  ld de,(banzaimapadress)
  ld b,8
  
.copybuffertomap:
    push bc
    ldi
    ldi
    ldi
    ldi
    pop bc
    push hl
    ex de,hl ;de is now in hl
    ld de,(maplenght)
    add hl,de
    ld de,4
    xor a
    sbc hl,de
    ex de,hl
    pop hl    
    
  djnz .copybuffertomap



  ld hl,(banzaimapadress)
  dec hl
  ld (banzaimapadress),hl



putbanzaibill:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a



  ;save the local tiledata first
  ld de,tilebuffer
  ld b,8 ;do the copy in 8 rows

  
.copymaptobuffer:
    push bc
    ldi
    ldi
    ldi
    ldi
    pop bc
 ;   push hl
;    ex de,hl ;de is now in hl
  push de
    ld de,(maplenght)
    add hl,de
    ld de,4
    xor a
    sbc hl,de
  pop de
;    ex de,hl
;    pop hl
  djnz .copymaptobuffer
  
 
  ;put the tiles in place
  ;hl is the tiledata
  ;de is the adress where we copy the tiles to
  
  ld de,(banzaimapadress)
  ld hl,banzaibilldata
  ld b,8 ;do the copy in 8 rows
  
.copybanzaibilltomap:
    push bc
    ldi
    ldi
    ldi
    ldi
    pop bc
    push hl
    ex de,hl ;de is now in hl
    ld de,(maplenght)
    add hl,de
    ld de,4
    xor a
    sbc hl,de
    ex de,hl
    pop hl
  djnz .copybanzaibilltomap
 
 
  ;place back the two copied tiles from the buffer
  
.restorebanzaibkgfrombuffer:

  push ix

  ld ix,tilebuffer
  
  ld a,(ix+1)
  ld hl,(banzaimapadress)
  inc hl
  ld (hl),a
  
  
  ld a,(ix+29)
  ld hl,(banzaimapadress)
  ld de,(maplenght)
  add hl,de  
  add hl,de  
  add hl,de  
  add hl,de  
  add hl,de  
  add hl,de  
  add hl,de
  inc hl  
  ld (hl),a
  
  pop ix
 
 
 
 call setpage12tohandleobjectmovement
 
 ret


banzaimapadress:  dw 0

banzaibilldata: db 089,000,090,092
                db 127,128,136,164
                db 127,173,184,173
                db 127,173,184,173
                db 127,173,184,173
                db 127,173,184,173
                db 127,185,136,164
                db 251,000,252,253

tilebuffer:     db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                db 000,000,000,000
                
           
;spawn objects in map that are taken in by mario
spawnobjects:


if (beta = 1)
    call spawnbeta ;remove after release date!!
endif


;  ld a,(standingonexit)
  ld a,(nocontrol)
  cp 1
  call z,.spawntubecover
  cp 4
  call z,.spawntubecoverright

  ld a,(currentlevel)
  cp 6
  call z,.spawnwater
  cp 9
  call z,.spawnfence
  cp 121
  call z,.spawn121water

  ld a,(waterlevel)
  and a
  call nz,spawnwaterswim

  ld a,(marioyoshi)
  dec a
  jp z,.spawnyoshi


  ld a,(marioshell)
  and a
  ret z
  
  ;spawn a new shell in place that is forced into a new sprite position
 

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc e


;create the shell

  push ix

  ld    iy,CustomObject
  ld    (iy),force10 ;we force the shell into the marioposition until mario is fully on the map 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),51               ;set object number (pinkshell)
  ld    (iy+6),0               ;set object number (pinkshell)

  call  activateobject


  pop ix

  ret


.spawn121water:

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,44
 
 
 
  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),104              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  push ix
  
  call  activateobject

  ld (ix+v4),0
 
 
  pop ix

  ret



.spawnyoshi:


    ;spawn a new shell in place that is forced into a new sprite position
 

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the yoshibody

  push ix

  ld    iy,CustomObject
  ld    (iy),force12 ;we force the shell into the marioposition until mario is fully on the map 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),59               ;set object number (yoshi)
  ld    (iy+6),0               ;set object number (yoshi)

  call  activateobject

  ld (ix+movepat),103
  ld (ix+objectrelease),255 ;make sure yoshi is detected by the engine
  ld (ix+v4),0 ;reset this we use this as the activation timer
  ld a,1
  ld (yoshileftright),a ;yoshi is always facing the right on spawn


  ld (yoshiix),ix ;save the yoshi ix

;  pop ix
  
  
  
  ;get the coordinates and spawn the head 
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

  pop ix

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force12 ;use the shellhold position
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),79               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0
    ld (ix+v2),1
;  ld (ix+clipping),0
;  ld (ix+movepat),71

;adjust the head with 1 pixel to make it fit correctly
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld (yoshiheadix),ix ;store the adress of the yoshihead so we can do modifications on it from anywhere

  pop ix
  
  
  
  ret



.spawntubecover:

  ld a,(tubelaunch)
  dec a
  ret z


  ld a,1
  ld (standingonexit),a
  ld (nocontrol),a
  ld (marioduck),a

;spawn the tubecover that brings mario in the map


  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec hl
  inc hl
;  inc hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
;  inc e

;create the tubecover

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),53                ;set object number (pinkshell)
  ld    (iy+6),0                ;set object number (pinkshell)

  call  activateobject

  ld (ix+v4),0
  ld (ix+objectfp),0
  
  ld a,(tubeupordown)
  dec a
  call z,.setobjectrelease

  pop ix

  ret

.setobjectrelease:

  ld (ix+objectrelease),1

  ret



.spawntubecoverright:

  ld a,(tubelaunch)
  dec a
  ret z
  
;spawn the tubecover that brings mario in the map


  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
;  inc hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

;create the tubecover

  push ix

  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),114              ;set object number (pinkshell)
  ld    (iy+6),0              ;set object number (pinkshell)

  call  activateobject

  ld (ix+v4),0
  ld (ix+objectfp),0

  ld a,(currentlevel)
  cp 60
  call z,.moveleft
 

  pop ix

  ret


.moveleft:

  ld (ix+objectrelease),1
  xor a
  ld (marioleftrightspr),a
  ld (marioleftright),a

  ret




.spawnwater:

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,69

;create the water

  push ix

  ld    iy,CustomObject
  ld    (iy),available         ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),104              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  call  activateobject

  ld (ix+v4),0
 
  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,68
 
 
  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),105              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  call  activateobject

  ld (ix+v4),0
 
 
  pop ix

  ret

.spawnfence:

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,69

;create the water

  push ix

  ld    iy,CustomObject
  ld    (iy),available         ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),104              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  call  activateobject

  ld (ix+v4),0
 
  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,18
 
 
  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),122              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  call  activateobject

  ld (ix+v4),0
 
 
  pop ix

  ret
  
switchmusicnormal:

;store the original music block
  ld a,(previousmusicblockp)
  ld (currentmusicblock),a  
  call switchmusic
  
  ret


switchmusictimeup:

;store the original music block
  ld a,(currentmusicblock)
  ld (previousmusicblockp),a


  ld a,timeupmusicblock
  ld (currentmusicblock),a  
  call switchmusic
  
  ret
  

switchmusicpbox:

  xor a
  ld (switchmusicpforbox),a

  ld a,(doublepboxactive)
  and a
  jp nz,.skipstoremusicblock

;store the original music block
  ld a,(currentmusicblock)
  ld (previousmusicblock),a

.skipstoremusicblock  
  

  ld a,$3a
  ld (currentmusicblock),a  
  call switchmusic
  
  ret


switchmusic:


  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings
  push bc

  xor a
  ld (playwinmusic),a


;  ld    a,musicreplayerblock
;  call  block34             ;set music replayer in page 2, at $8000
;
;  ld         a,(currentmusicblock)
;  call       block12                       ;set music in page 1, at $4000

  xor a
  call  block12             ;set music replayer in page 2, at $8000

  ld         a,(currentmusicblock)
  call       block34                       ;set music in page 1, at $4000



;  - In: A = No. of Repeats (0 = infinite), HL = Address of BGM data
  ld a,1
;  ld    hl,$4000
  call  startfm             ;to initialise the music.

  pop bc
  
    ld a,b
    call block34


        pop af
        out                ($a8),a        

    ret

startmusic:

  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a   

  xor a
  call  block12             ;set music replayer in page 2, at $8000

  ld         a,(currentmusicblock)
  call       block34                       ;set music in page 1, at $4000


;  - In: A = No. of Repeats (0 = infinite), HL = Address of BGM data
;  - Out: A = Playing flag (0 = not playing, 255 = playing)
  xor a
;  ld    hl,$4000
  call  startfm             ;to initialise the music.

        ld                a,(slot.ram)              ;back to full RAM
        out                ($a8),a       

  ret



pollpauze:
 
  ld a,(mariotransforming)
  and a
  ret nz ;cannot press pauze during transformation
  
  ld a,(pauze)
  dec a
  call z,displaypauze
  
  
  ld a,(setpauze)
  dec a
  jp z,.setpauze


;poll op pauze

  ld b,4
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
	bit 5,a
	
  jp z,.setppressed

    ld b,7
  
   in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
	bit 2,a ;poll the esc key is easier and more intiuitive for the user
  
  jp z,.gooptionsmenu
 
  
	ret
	
.setppressed:
	
    ld a,1
    ld (setpauze),a
    
    ret
	
.setpauze:

	
  ld b,4
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
	bit 5,a
	
	ret z
	
    ld a,(pauze)
    dec a
    jp z,.resetpauze
	
	
	call Playsfx.pauze
	
       ld a,1
       ld (pauze),a
;       xor a
;       ld a,$7F
;       ld (Playing),a  
       call stopmusic
   
        ld a,(time)
        ld (pauzetime),a
        ld a,(oldcamerascroll)
        ld (cameratype),a ;save cameratype static or not??
   
       xor a
       ld (setpauze),a

       ld (musicactive),a ;stop all music
       ld (time),a
       ld (oldcamerascroll),a
       
       ret

.resetpauze:

  xor a
  ld (pauze),a
  ld (setpauze),a
  
  ld a,(pauzetime)
  ld (time),a ;write current time back
  
  ld a,(musicactiveoption)
  ld (musicactive),a ;restore music if it was active
  
        ld a,(cameratype)
        ld (oldcamerascroll),a ;restore cameratype static or not??  
  
;       ld a,$FF
;       ld (Playing),a
       
       call startmusic
       
    	call Playsfx.pauze   
       
  jp displaypauze.remove

.gooptionsmenu:
    

  ld    a,(VDP_0+1)       ;screen off
  and   %1011'1111
  out   ($99),a
  ld    a,1+128
  out   ($99),a
 
 
  ld a,(sfxactive)
  and a
  jp z,.skipstep
  
.waitforsound:
   
  ld a,(Sfxon?)
  and a
  jp nz,.waitforsound ;wait for any sound to finish
    
.skipstep:  

  call stopmusic ;stop the FMPAK
  
;first save the ram/rom settings to the stack
  in a,($a8)
  push af
  
    ld a,(slot.page12rom)
    out ($a8),a
    
      ;stop the SCC player
        ld a,$3f
        ld ($9000),a
        xor      a
        ld      (SccSfxOn),a
        ld      (988Fh),a	; all channels inactive
    
    
    ld a,optionsmenublock
    call block34
    
    di
    
    call Settempisr ;disable the original ISR
    
    ld a,1
    ld (blockcpusetting),a ;not allowed to change the cpu settings from the menuoptions
    
    call menuoptions ;go!
    
   
;get the old InterruptHandler back
  di
  ld    hl,InterruptHandler ;set new normal interrupt
  ld    ($38+1),hl
  ld    a,$c3
  ld    ($38),a
 
  ld    a,(VDP_0)
  or    %0001'0000          ;set ei1 (enable lineints)
  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,0+128
;  ei
  out   ($99),a

  ld    a,lineinterruptheight   ;set lineinterrupt height
;  di
  out   ($99),a
  ld    a,19+128
  ei
  out   ($99),a
  
  ;switch to screen 4
  ld    a,(VDP_0)
  and   %1111'1101		      ;m3=0
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 4

;backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 
  ld    a,%0000'1111          ;color 15= black, backdrop black
  di
  out   ($99),a
  ld    a,7+128
  ei
  out   ($99),a
;/backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0


;write the pallete values back
  
  ld    hl,currentpalette
  call  SetPalette

 
  xor a
  ld (blockcpusetting),a
 
;reenable the screenoutput  
  call enablescreen
  
  call startmusic ;restart the music
  
;restore RAM/ROM settings  

  pop af
  out ($a8),a
  
    ret


displaypauze:

  ld hl,1000
  ld (idlecounter),hl ;keep resetting the idlecounter

;poll for v key
  ld b,5
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
	bit 3,a
	
	call z,.setendlevel

;TODO: dit moet gewoon een object worden in de eventhandler

	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a	

  ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
;	ld de,896 ;+28 sprites verder
;	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,$19
	call	block1234		;set blocks in page 1/2 $4000- $8000

	ld		hl,$4000 ;adress on block
	ld de,6528
	add   hl,de
	ld		c,$98
	call	outix128 ;vier sprites
	

;put hero sprite color

	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
;	ld de,448 ;+30 color tables verder
;	add hl,de
	ld		a,1
	call	SetVdp_Write

;	ld		hl,$4000+128
  ld hl,$4000
  
	ld de,6528
	add   hl,de
	ld de,128
	add hl,de
	ld		c,$98
	call	outix64


  ld a,(framecounter)
  and 8
  jp z,.remove

	
	ld a,100
	ld (spat),a
	ld (spat+4),a
;	add 16
	ld (spat+8),a
	ld (spat+12),a
	
	
	ld a,120
	ld (spat+1),a
	ld (spat+5),a
	add 16
	ld (spat+9),a
	ld (spat+13),a


	ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ret

.setendlevel:

  ld a,(completed)
  and a
  ret z ;not completed yet

  ld a,20
  ld (nocontrol),a
  ld a,1
  ld (levelend?),a 
  
  xor a
  ld (droppowerup),a ;prevent stupid things from happening
  
  ret

.remove:

  ld a,226
	ld (spat),a
	ld (spat+4),a
	ld (spat+8),a
	ld (spat+12),a
	
	
	xor a
	ld (spat+1),a
	ld (spat+5),a
	ld (spat+9),a
	ld (spat+13),a


	ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

	
	ret


checkforclimbedge:


.up:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  ;mapadressdata is allready in hl
  ld    a,96 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp    z,.ret   ;foreground tile found, then return
  ld    a,97 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp    z,.ret   ;foreground tile found, then return
  ld    a,98 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp    z,.ret   ;foreground tile found, then return
 
   
  inc   hl  ;also check the next tile
  ld    a,96 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp    z,.ret   ;foreground tile found, then return
  ld    a,97 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp    z,.ret   ;foreground tile found, then return
  ld    a,98 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  jp z,.ret

  
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block34


  xor a


  ret


  
.ret:
   
    
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block34

  scf 


  ret



objecttilecheckfromrom:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

    call  objecttilecheck
    
   call setpage12tohandleobjectmovement 


  ret


shelluptilecheck:

  ld c,0
  push bc
  
.start:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  ld    bc,-16;10                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.adjustspeed

  ld    bc,-16;10                   ;vertical increase to tilecheck
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.adjustspeed


  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.checkblock
  dec hl
  ld a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.checkblock  


  ld    bc,0                 ;vertical increase to tilecheck
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.checkblock
  inc hl
  ld a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.checkblock 


  pop bc

      call setpage12tohandleobjectmovement
  
    xor a
  ret

.checkblock:

  pop bc
  ld a,c
  and a
  jp nz,.endblocktilescheck

  ld    a,203                   ;alle blocks (vraagteken, uitroepteken en roterend block)
  cp    (hl)                    ;kijk of de huidige tile een block is
  jp    c,.endblocktilescheck


  ld    a,1
  ld    (activatenewblock?),a   ;mario activates a new block
  ld    (blocksactive?),a       ;there is now at least 1 block active, so activate blockhandler
  ld    (blockaddress),hl       ;hl points to tile in map

;PROBLEM: when a object hits the box it will cause the box to malfunction since there is a clipping value
;objectinteract will not do the proper handling since the object is high in the list. If we however disable the clipping
;the shell will no longer kill other object during its fall.
  ld (ix+clipping),0 ;DEBUG will cause bugs after a shell hits a box


.endblocktilescheck:

;      call setpage12tohandleobjectmovement
;  scf
;  ret

  
  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    233+1                     ;softtiles excluden
      call nc,setpage12tohandleobjectmovement
  ret nc
  dec hl
  dec hl
  ld a,(hl)
  cp 233+1
        call nc,setpage12tohandleobjectmovement
  ret nc

      call setpage12tohandleobjectmovement

  scf
  ret

.adjustspeed:

  pop bc

;exclude softtiles
  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    233+1                     ;softtiles excluden
      call nc,setpage12tohandleobjectmovement
  ret nc
  dec hl
  dec hl
  ld a,(hl)
  cp 233+1
        call nc,setpage12tohandleobjectmovement
  ret nc

  ld a,(ix+v2)
  cp 7
  jp c,.noadjust ;speed is lower than maximum so no adjusting is required

  ld (ix+v2),7
  xor a

.noadjust: 
  
  xor a
  
  call setpage12tohandleobjectmovement
  
  ret

.noblockcheck:
  
  ld c,1
  push bc

  jp .start


checktilesmovpattern96:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;schuine tiles afvangen bij het vallen

  ld bc,16+8
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
  ld de,maplenght
 
 
  ld bc,0+8
  ld    de,6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
 
 
  ld bc,16
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
 
  ld bc,0
  ld    de,6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
;/schuine tiles afvangen bij het vallen 
 
 
 
  ld    b,0
  ld    c,(ix+sizeY)
  ld    de,-7                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.nextframe
  inc  hl
  inc  hl
  ld a,(hl)
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern96.nextframe


    call setpage12tohandleobjectmovement


  jp MovementPattern96.returnfrompage3





; bit	7	  6	  5		    4		    3		    2		  1		  0
;		     	Z 	C 		  X   	  right	  left	down	up	(keyboard)


forcerebuildbackground:

  ld a,1
  ld (forcesetbackground),a

  ret


changeslidingboxgfx:

;get the mapdata
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

    
  ;mapadres is in HL
  
  ld a,(ix+offsets)
  dec a
  jp z,makehorizontalgfx


  ld de,-32
  ld bc,16
  call objecttilecheck

  
  ;make vertical boxgfx on map
  
  ;write bkg first
  ld b,10
.loop0:  
  xor a
  ld (hl),a
  inc hl
  djnz .loop0
  
  ld de,(maplenght)
  add hl,de ;switch one line
  
  dec hl ;shift 1 left 
  
   ld b,10
   
.loop02:  
  xor a
  ld (hl),a
  dec hl
  djnz .loop02
  
  
  ;then new tilenumbers in the new position
  
  ld de,5 ;shift 5 tiles to the right
  add hl,de

  
  ld b,5
    ld de,(maplenght)
  
.loopheight: ;go 5 tiles higher  
  xor a
  sbc hl,de
  djnz .loopheight

  
  ;no we are at the upper corner of the vertical tile. we must now write the new tile values into the map
  ld b,10 ;ten rows
  ;maplenght is still in de
  ld a,128 ;first tile number to write
  
.looprow:  
  ld (hl),a
  inc a
  inc hl
  ld (hl),a
  inc a
  dec hl
  add hl,de
  djnz .looprow
  
  call setpage12tohandleobjectmovement

  ret

makehorizontalgfx:

  ld de,0
  ld bc,-16
  call objecttilecheck


  ;now that we have found the upper corner we write back 0 and delete the previous tiles from map
  ld b,10 ;ten rows
  xor a
  ld de,(maplenght)
  
.looprow:  
  ld (hl),a
  inc hl
  ld (hl),a
  dec hl
  add hl,de
  djnz .looprow


;shift up and find the right row
  ld b,6
.findrow:
  xor a
  sbc hl,de
  djnz .findrow

  ld de,4
  xor a
  sbc hl,de
  
  ld de,(maplenght)
  
  ;write back the old tiles
  
  ld a,108
  ld b,10
  
.looprow1:
  ld (hl),a
  inc a
  inc hl
  djnz .looprow1
 
  ld b,10
  dec hl
  add hl,de
  ld a,127
  
.looprow2:
  ld (hl),a
  dec a
  dec hl
  djnz .looprow2


  jp setpage12tohandleobjectmovement

 ; ret

setcharcolandoffset:
  ld    (ix+spritecolROM+0),l   ;set color address
  ld    (ix+spritecolROM+1),h
  ld    (ix+spritecharROM+0),c  ;set character address
  ld    (ix+spritecharROM+1),b 
  ld    (ix+spritesblock),a
.offsetsonly:  
  push  ix                      ;set offsets
  pop   hl
  ld    bc,offsets
  add   hl,bc
  ex    de,hl

;BUG: when the screen really gets crowdy with objects a 0 is written to ix+amounspr. This should not happen at all! We now catch this bug by returning here.
  ld a,(ix+amountspr)
  and a
  ret z;no 0 allowed!!
  add a,a
  add a,a
  ld c,a
  ld b,0
  ldir
  ret
  

  
initswitch:

  inc (ix+v7)

  ld a,(currentlevel)
  cp 103
  jp z,.switchbluecolor
  cp 105
  jp z,.switchredcolor
  cp 46
  ret nz

  
  ld a,11 ;color number
  ld e,0
  ld d,4
  call switchcolor
  
  ld a,9 ;color number
  ld e,0
  ld d,6
  call switchcolor
  
  ret
  
.switchbluecolor:
  
  ld a,11 ;color number
  ld e,6
  ld d,5
  call switchcolor
  
  ld a,9 ;color number
  ld e,6
  ld d,4
  call switchcolor
  
  ret  
    
.switchredcolor:
  
  ld a,11 ;color number
  ld e,255-15+3
  ld d,3
  call switchcolor
  
  ld a,9 ;color number
  ld e,255-15
  ld d,0
  call switchcolor
  
  ret   

setpage12tohandleobjectmovementfast:

  push af

  ld a,romprogblock2
;faster to do it manually by hand

  di
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
;	inc		a
  ld a,(romprogblocknumber)
	ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
	ei
	
	pop af
	
	
	ret  	
	
	
  
writeanimationtiles:
    

    ld a,animationcodeblock
    call block34                                     ;set block in page 1 ($4000 - $5fff)
  

    ld hl,writeanimationtiles.return
    push hl ;create ret
    jp (iy) ;will return here
.return:


    ld a,loaderblock
    call block34                                     ;set block in page 1 ($4000 - $5fff)

    
    ret ;now return for real 
;==================================================================[geluiden engine]=============================================================================
;include "playsfx.asm" to engine 3

;==================================================================[end of that]==================================================================================


include "recorddemo.asm"

;include "spritehider.asm" ;move this to ROM



page0lenght: equ $ - LevelEngine ;size of page0
  

include "variables.asm"
