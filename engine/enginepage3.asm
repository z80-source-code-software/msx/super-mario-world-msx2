phase	$c000

initmusic_and_goEngine:

;set horizontal scroll offset v9958
  ld a,4
;	ld		(VDP_8+21),a ;Hey gast dit klopt allemaal niet zo
  di
	out		($99),a
	ld		a,18+128
	ei
	out		($99),a
;/set horizontal scroll offset v9958


;set horizontal scroll offset v9958
  ld    a,7
	ld		(VDP_8+21),a
  di
	out		($99),a
	ld		a,27+128
	ei
	out		($99),a
;/set horizontal scroll offset v9958


  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a 

;  ld    a,musicreplayerblock
;  call block34
; 
;	ld		a,(currentmusicblock)
;  call block12

  xor a
  call block12
  ld a,(currentmusicblock)
  call block34


;  - In: A = No. of Repeats (0 = infinite), HL = Address of BGM data
;  - Out: A = Playing flag (0 = not playing, 255 = playing)
  xor   a
;  ld    hl,$4000
  call  startfm             ;to initialise the music.

	ld		a,(slot.ram)	      ;back to full RAM
	out		($a8),a		
  ei

 ; ld a,15
 ; ld (yellowswitchactive),a ;debug

  call  Transformyellowblocks                                 ;transform the outlined blocks into blocks
  call  spawnobjects  ;respawn objects that are taken into this map (shells, blocks etc)

  ld hl,1000
  ld (idlecounter),hl ;set the idlecounter

  ld a,1
  ld (setobjectscurrentscreen?),a
  ; ld (sfxactive),a ;temp debug
  ; ld (musicactive),a ;temp debug
  
  ld a,(nocontrol)
  cp 4 ;exit to a blasttube
  call z,.resetnocontrol ;TEMP!!
  cp 2 ;mario dies
  call z,.resetnocontrol ;TEMP!!
  cp 3
  call z,.resetnocontrol
  cp 6
  call z,.resetnocontrol
    
  ld a,5
  ld (holdscreen),a
 
  ld a,(demo)
  dec a
  call z,.setdemo
 
;  ld hl,$8000
;  ld (demopointer),hl

  ld a,(slot.page12rom) ;clear the upper spat
  out ($a8),a
  ld a,romprogblock2
  call block12

  ld hl,emptyupperspat
  ld de,upperspat
  ld bc,fulllengthemptyspat
  ldir
  
  ld a,(slot.ram)
  out ($a8),a
  
	jp		LevelEngine

  
.resetnocontrol:

  xor a
  ld (nocontrol),a

  ret

;input different inthandler
.setdemo:
  
  di
  ld    hl,demointhandler ;set demo InterruptHandler
  ld    ($38+1),hl
  ld    a,$c3
  ld    ($38),a 
  ei

;kill the scorebarhandler
  xor a ;nop
  ld hl,scorebaradress
  ld (hl),a
  inc hl
  ld (hl),a
  inc hl
  ld (hl),a
  ld a,$C9 ;return
  ld hl,setspritesplit
  ld (hl),a
  ld hl,pollpauze
  ld (hl),a

  ;set demo lineint height
  ld a,32
  ld (lineintheight),a

  ret


inix32:
  ini : ini : ini : ini : ini : ini : ini : ini : 
  ini : ini : ini : ini : ini : ini : ini : ini : 
  ini : ini : ini : ini : ini : ini : ini : ini : 
  ini : ini : ini : ini : ini : ini : ini : ini : 
  ret


outix136:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix128:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
	outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
outix96:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix88:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix80:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix72:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix64:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
outix48:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
outix32:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
outix16:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
outix8:
   ; di
	outi : outi : outi : outi : outi : outi : outi : outi : 
;	ei
	ret




ldix67:
  ldi
ldix66:
  ldi
ldix65:
  ldi
ldix64:	
	ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : 
ldix48:	
	ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : 
ldix32:	
	ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : 
ldix16:	
	ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : 
ldix8:	
	ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : 
	ret



;thanks luppie for your support in this one
stopmusic: 

  ld a,(turbor?)
  dec a
  jp z,.wrtr800fm


  xor a
	ld b,a
.wrtfm:	
  out ($7c),a
	push af
	ld a,b
	out ($7d),a
	pop af
	ex (sp),hl
	ex (sp),hl
	ex (sp),hl
	ex (sp),hl	
	inc a
	cp $39
	jr c,.wrtfm
	ret

;use slower write or the instruments will hang anyway
.wrtr800fm:

  xor a
	ld e,a

.wrtr800fmloop:	
		push	bc
		push	af
		out	(7Ch), a
		ld	b, 2
		call	.wait
		ld	a, e
		out	(7Dh), a
		ld	b, 7
		call	.wait
		pop	af
		pop	bc
		inc a
		cp $39
		jp c,.wrtr800fmloop
		ret
; Wait

.wait:		
		in	a, (0E6h)
		ld	c, a

.wait2:		
		in	a, (0E6h)
		sub	c
		cp	b
		jr	c, .wait2
		ret

;/wait
 




handlestartmusic:
;start music

  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a   

  xor a
  call  block12             ;set music replayer in page 2, at $8000

  ld         a,(currentmusicblock)
  call       block34                       ;set music in page 1, at $4000

;  - In: A = No. of Repeats (0 = infinite), HL = Address of BGM data
;  - Out: A = Playing flag (0 = not playing, 255 = playing)
  xor a
;  ld    hl,$4000
  call  startfm             ;to initialise the music.

;important note: at the end, worldmapengineblock is put back at $8000 in page 2 in rom
  ld a,worldmapengineblock
  jp  block34
  
  

handlesfxint:

  ;handle sfx
        ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc               a
        ld                ($7000),a
        ld                a,Sfxblock
        ld                ($9000),a
        inc               a
        ld                ($b000),a

  ld    a,(SetSfx?)           ;should a Sfx be played?
  or    a
  jr    nz,.setnewsfx


  call  MainSfxIntRoutine       ;keep running sfx every int

.setnewsfx:
  call  SetSfx                                     ;if sfx is on, then set to play


        ld                a,SCCreplayerblock+1   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
;        ld                ($5000),a
;        inc               a
        ld                ($7000),a
        ld a,$3f ;get bank ready for SCC access
        ld                ($9000),a
;        inc               a
;        ld                ($b000),a
        
   ;     push hl
   ;     push de
   ;     push bc
        
  call  SccReplayerUpdate       ;keep running sfx every int
  
    ;    pop bc
    ;    pop de
    ;    pop hl

   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a



  ret


handlemusicint:

  ;handle music
  
      
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
  ld                a,(slot.page12rom)        ;all RAM except page 1+2
  out                ($a8),a         

  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a

  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.

   ;reset blocks
    ld                a,(memblocks.n1)     ;reset blocks
    ld                ($5000),a
    ld                a,(memblocks.n2)
    ld                ($7000),a
    ld                a,(memblocks.n3)
    ld                ($9000),a
    ld                a,(memblocks.n4)
    ld                ($b000),a



  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        

  ret

;direct reading of status1
readkeydirect:

  ld a,(joymega?)
  dec a
  jp z,.joymega
  
  
  ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
  xor %11111111

  ret


.joymega:

  di
	ld	a, 15	; Lee el puerto de joystick y almacena
	out	($A0), a	; los estados en las variables.
	in	a, ($A2)
	and	10101111b
;	push	af
	out	($A1), a
	ld	a, 14
	out	($A0), a
	ei
	in	a, ($A2)

  ld b,a ;store temp
  rlca
  rlca
  rlca
  rlca ;to bit 7
  and %10000000
  ld c,a
  ld a,b
  rlca
  rlca ;to bit 4
  and %00010000
  or c ;add bit 7

  xor 255 ;invert
  
  ret



;Standard keyboard mapping/ this is the same for joystick
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		     	Z 	C 		  X   	  right	  left	down	up	(keyboard)
;
;Alternate control scheme proposed by Mars2000
; bit	7	  6	      5		      4		        3		    2		  1		  0
;		     	graph 	ctrl 		  space   	  right	  left	down	up	(keyboard)
ReadControls:	

;The jp .keyboard is being overwritten by the correct adress on select
  jp .keyboard

.joystick:
;2button support. Reads two buttons and sets the spinjump bit if up and button A are simultaniously pressed

  di
	ld	a, 15	; Lee el puerto de joystick y almacena
	out	($A0), a	; los estados en las variables.
	in	a, ($A2)
	and	10101111b
	out	($A1), a
	ld	a, 14
	out	($A0), a
	ei
	in	a, ($A2) ;read left right up down and button 1 and 2

  cpl
  and %00111111
  ld c,a
  ;check if up and down are both pressed
  
  and %00010001
  cp 17
  jp nz,.nospinjump
 
  set 6,c
  res 4,c  
  
.nospinjump:

  ld a,c
    
  ld (Controls),a



  jp readcheatcontrols



.keyboard2:
;alternate keyset as proposed by mars2000

;read arrow keys

  ld b,8 ;<- | | -> +space
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

  ld b,0
  rra ;space to carry
  rl b ;space into b
  sla a
  sla a; -> into carry
  rl b ; -> into b
  rla
  rla
  rla ;<- into carry
  rl b
  rra
  rra ;down into carry
  rl b ;down into b
  rlca
  rl b
  
  ld a,b
  
  sla a
  sla a
  sla a ;prepare for grph and ctrl insertion
  
  ld c,a

;grph and ctrl uitlezen 
 
  ld b,6 ;grph and ctrl
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
  
  rra
  rra
  rr c
  rra
  rr c
  rr c ;done

  ld a,c
  cpl ;invert bits
  ld c,a

 ld  hl,Controls
 ld  a,(hl)
 xor  c
 and  c
 ld  (NewPrControls),a
 ld  (hl),c


  jp readcheatcontrols


.joymega:


;Taken from joymega tester:
;credits to Sergio Guerrero
;	; JOYSTICK TESTATOR 1.00
	; Por Sergio Guerrero 24-1-2.000
	; Testea el estado de un mando de 3 o 6 botones de MegaDrive.
	
	di ;this can be troublesome on lineint!
	ld	a, 15	; Lee el puerto de joystick y almacena
	out	($A0), a	; los estados en las variables.
	in	a, ($A2)
	and	10101111b
	push	af
	out	($A1), a
	ld	a, 14
	out	($A0), a
	in	a, ($A2)
	ld d,a ;status1
	ld	a, 15
	out	($A0), a
	pop	af
	or	00010000b
	out	($A1), a
	ld	a, 14
	out	($A0), a
	in	a, ($A2)
	ei
	ld e,a ;status2

;bit in Controls byte
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		     	Z 	C 		  X   	  right	  left	down	up	(keyboard)
;



;variable in status 1 
;=================================================
;        up  down  left  right   x    c
; bit    0    1     2      3     4    5
;=================================================


  ld a,d
	and %00111111 ;keep upper bits to 0
	ld c,a ;store for later multiplex with key A and start
	
  ld a,e ; A and STart
	and	00001100b ;these must be zero otherwise things go wrong
	jr	nz, .kkA
  ld a,e
	and	00110000b
	rlca
	rlca
	and %11000000
	or c
	
.kkA:

  cpl
  ld (Controls),a
  

  ret
  
 ; jp readcheatcontrols ;why jump? its faster then do the keyboard as well.

  
  
  
  
;TODO: optimise this huge mess!    
.keyboard:  

  ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

;nu nog de toetsen goed omdraaien
;om de readout compatible te maken met de oude routine

 
;new keyboard code must be faster.
    ld d,0 ;7T
    rla ;4T
    ld c,a ;4T
    rl d ; <- ;8T
    rla ;4T
    rla ;4T
    rla ;4T
    rl d ;8T
    ld a,c ;4T
    rla ;4T
    rl d ;8T
    rla ;4T
    rl d ;8T ;in d zitten de pijltjes toetsen
    
;total = 71 T total    
 
;C uitlezen 
  
  ld b,3 ;C
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
  rrca ;4T
  rrca ;4T
  rrca ;4T
  and %00100000 ;7T
  ld c,a ;4T

;Z en X uitlezen 
 
  ld b,5 ;Z en X
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

  bit 3,a
  call z,droppowerreservedetected

    rra ;4T
    and %01010000 ;7T
    or d ;4T
    or c ;4T
    
;total = 42 T total    
  
  
  cpl
;  xor %11111111 ;onze engine verwacht de inverse van de reads
;  ld (Controls),a
  ld c,a
 
 
 ld  hl,Controls
 ld  a,(hl)
 xor  c
 and  c
 ld  (NewPrControls),a
 ld  (hl),c

    ret
 
readcheatcontrols:
;lees allemaal toetsen in controls2 bruikbaar om mee te cheaten

  ld b,0
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
  ld (controls2),a
  
  ret

readcontrolslenght: equ $ - ReadControls
 


;
;Set VDP port #98 to start writing at address AHL (17-bit)
;
SetVdp_Write: 
;first set register 14 (actually this only needs to be done once
	rlc     h
	rla
	rlc     h
	rla
	srl     h
	srl     h
	di
	out     ($99),a       ;set bits 15-17
	ld      a,14+128
	out     ($99),a
;/first set register 14 (actually this only needs to be done once

	ld      a,l           ;set bits 0-7
	nop
	out     ($99),a
	ld      a,h           ;set bits 8-14
	or      64            ; + write access
	ei
	out     ($99),a       
	ret

;
;Set VDP port #98 to start reading at address AHL (17-bit)
;
SetVdp_read: 
;first set register 14 (actually this only needs to be done once
	rlc     h
	rla
	rlc     h
	rla
	srl     h
	srl     h
	di
	out     ($99),a       ;set bits 15-17
	ld      a,14+128
	out     ($99),a
;/first set register 14 (actually this only needs to be done once

	ld      a,l           ;set bits 0-7
	nop
	out     ($99),a
	ld      a,h           ;set bits 8-14
	and %0011'1111 ;read access
	ei
	out     ($99),a       
	ret

	
Depack:     ;In: HL: source, DE: destination

	inc	hl		;skip original file length
	inc	hl		;which is stored in 4 bytes
	inc	hl
	inc	hl

	ld	a,128
	
	exx
	ld	de,1
	exx
	
depack_loop:

	;secure this loop from exiting $BFFF! There is a serious BUG going on here!
	;TODO: Find the source of this shit bug. It only happens when calling the mapdata from a certain adress on the rom.
	push af
	push hl
	push de
	
	ld hl,$BFFF
    xor a
    sbc hl,de
    jp c,.return
    
    pop de
    pop hl
    pop af


	call getbits
	jr	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)
    
	jr	depack_loop

.return:
	
        pop de
        pop hl
        pop af
        ret
	
	
	
;handle compressed data
output_compressed:
	ld	c,(hl)		;get lowest 7 bits of offset, plus offset extension bit
	inc	hl		;to next byte in compressed data

output_match:
	ld	b,0
	bit	7,c
	jr	z,output_match1	;no need to get extra bits if carry not set

	call getbits
	call rlbgetbits
	call rlbgetbits
	call rlbgetbits

	jr	c,output_match1	;since extension mark already makes bit 7 set 
	res	7,c		;only clear it if the bit should be cleared
output_match1:
	inc	bc
	
	
;return a gamma-encoded value
;length returned in HL
	exx			;to second register set!
	ld	h,d
	ld	l,e             ;initial length to 1
	ld	b,e		;bitcount to 1

;determine number of bits used to encode value
get_gamma_value_size:
	exx
	call getbits
	exx
	jr	nc,get_gamma_value_size_end	;if bit not set, bitlength of remaining is known
	inc	b				;increase bitcount
	jr	get_gamma_value_size		;repeat...

get_gamma_value_bits:
	exx
	call getbits
	exx
	
	adc	hl,hl				;insert new bit in HL
get_gamma_value_size_end:
	djnz	get_gamma_value_bits		;repeat if more bits to go

get_gamma_value_end:
	inc	hl		;length was stored as length-2 so correct this
	exx			;back to normal register set
	
	ret	c
;HL' = length

	push	hl		;address compressed data on stack

	exx
	push	hl		;match length on stack
	exx

	ld	h,d
	ld	l,e		;destination address in HL...
	sbc	hl,bc		;calculate source address

	pop	bc		;match length from stack

	ldir			;transfer data

	pop	hl		;address compressed data back from stack

	jr	depack_loop

rlbgetbits:
	rl b
getbits:
	add	a,a
	ret	nz
	ld	a,(hl)
	inc	hl
	rla
	ret       

unpackfrompage3to2:         ;hl->source, de->destiny, b->block, out-> slot.page12rom, loaderblock in block 3 ($8000)
	ld		a,(slot.page2rom)	  ;all RAM except page 2
	out		($a8),a	

  ld    a,b
	call	block34			        ;at address $8000 / page 2
	call  Depack

	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a	
	ld		a,loaderblock
	call	block34			        ;at address $8000 / page 2
  ret

;unpackfrompage3to2_andfill: ;hl->source, de->destiny, b->block, out-> slot.page12rom, loaderblock in block 3 ($8000)
;;this is used to fill the foreground map
;	ld		a,(slot.page2rom)	  ;all RAM except page 2
;	out		($a8),a	
;
;  ld    a,b
;	call	block34			        ;at address $8000 / page 2
;	call  Depack
;
;  xor   a
;  ld    bc,256*8
;.loop:
;  ld    (de),a  
;  inc   de
;  dec   c
;  jp    nz,.loop
;  djnz  .loop
;
;	ld		a,(slot.page12rom)	;all RAM except page 1+2
;	out		($a8),a	
;	ld		a,loaderblock
;	call	block34			        ;at address $8000 / page 2
;  ret

depackmapdata: ;out = DE last write adress

;get the mapdata
  ld		a,(slot.page12rom)	  ;all RAM except page 2
	out		($a8),a	 
  
  ld    a,(ix+mapdata.backgrmapblock)                     ;background map block
  ld    l,(ix+mapdata.backgrmapaddr)                      ;backgrmapaddr
  ld    h,(ix+mapdata.backgrmapaddr+1)  
  
;  call block34 ;find the map
  di
  ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
  ei 
  
;  ld hl,backgrl00
  ld bc,$2000 ;about 4Kb should be enough for each map
  ld de,engaddr ;destination
  
  ldir ;copy the mapdata in page 0



  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a 

  ld hl,engaddr
  ld de,$4000 ;Page 1
  
  call Depack
  push de
 
 	ld		a,(slot.page12rom)	;all RAM except page 1+2
	out		($a8),a	 
  
    xor a
  ;call block123
  di
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
	inc		a
	ld		(memblocks.n3),a
	ld		($9000),a
  ei

  ld		hl,engine
	ld		de,engaddr
	ld		bc,page0lenght        ;load engine
	ldir
  
  
	ld		a,loaderblock
;	call	block34			        ;at address $8000 / page 2
  di
  ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
	ei
  pop de ;keep DE for possible bkg data copy

kker:

  ret

SetPalette:
	xor		a
	di
	out		($99),a
	ld		a,16+128
	out		($99),a
	ld		bc,$209A
	otir
	ei
	ret

disablescreen:


  ld    a,(VDP_0+1)       ;screen off
  di
  and   %1011'1111
  out   ($99),a
  ld    a,1+128
  ei
  out   ($99),a

   
  ret


;only works in screen 4!!! do not use this in the worldmap!
enablescreen:

  ld    a,(VDP_0+1)       ;screen on
  di
  or    %0100'0000
  out   ($99),a
  ld    a,1+128
  ei
  out   ($99),a
   
  ret


include "checktile.asm"



  
;*************************************************************
; met $7f, $bf en $ff in $9000 wordt de SCC voorgeschakeld
; ascii8 blokken:
; $5000 -> $6000 -> 
; $7000 -> $6800 -> 
; $9000 -> $7000 -> 
; $b000 -> $7800 -> 
;*************************************************************

;block1:
;	di
;	ld (memblocks.n1),a
;	ld ($5000),a
;	ei
;	ret

;block2:	
;	di
;	ld		(memblocks.2),a
;	ld		($7000),a
;	ei
;	ret

block3:		
	di
	ld		(memblocks.n3),a
	ld		($9000),a
	ei
	ret

;block4:		
;	di
;	ld		(memblocks.4),a
;	ld		($b000),a
;	ei
;	ret

block12:	
	di
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
	ei
	ret

block23:	
	di
	ld		(memblocks.n2),a
	ld		($7000),a
	inc		a
	ld		(memblocks.n3),a
	ld		($9000),a
	ei
	ret

block34:	
	di
	ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
	ei
	ret

;block123:	
;	di
;	ld		(memblocks.1),a
;	ld		($5000),a
;	inc		a
;	ld		(memblocks.2),a
;	ld		($7000),a
;	inc		a
;	ld		(memblocks.3),a
;	ld		($9000),a
;	ei
;	ret

;block234:	
;	di
;	ld		(memblocks.2),a
;	ld		($7000),a
;	inc		a
;	ld		(memblocks.3),a
;	ld		($9000),a
;	inc		a
;	ld		(memblocks.4),a
;	ld		($b000),a
;	ei
;	ret

block1234:	 
  
  di
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
	inc		a
	ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
	ei
	ret


setpage12tohandleobjectmovement:

  push af

  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a   

  ld a,romprogblock2
;faster to do it manually by hand

  di
	ld		(memblocks.n1),a
	ld		($5000),a
	inc		a
	ld		(memblocks.n2),a
	ld		($7000),a
;	inc		a
  ld a,(romprogblocknumber)
	ld		(memblocks.n3),a
	ld		($9000),a
	inc		a
	ld		(memblocks.n4),a
	ld		($b000),a
	ei
	
	pop af
	
	
	ret  


openmenuoptions: 


  ld a,optionsmenublock
  call block34
  
  call menuoptions    

  ld a,worldmapengineblock          ;if you wanna go back to worldmap then this has to be worldmapengineblock
  call block34              ;i already tried saving the block before call menuoptions, and recalling them here, 
  ret                       ;but i ran into weird bugs :(
  
Settempisr:	

  di
  ld    a,(VDP_0)
  and   %1110'1111          ;reset ei1 (disable lineints)
  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,0+128
  out   ($99),a
 
;  di
 
	ld		hl,.tempisr
	ld		de,$38
	ld		bc,3;6
	ldir
  ei
	ret  
 
.tempisr:	
;	push	af
;	in		a,($99)             ;check and acknowledge vblank int (ei0 is set)
;	pop		af
	jp tempinterrupt ;this way we can keep the lineinterrupts enabled at all times. hopefully successfully killing the UFO bug
;	ei	
;	ret  
  
;New tempisr: Somehow for reasons that I do not understand even after disabling the lineints on the VDP. In very rare cases the VDP even after allowing a few interrupts or long wait time still trows a lineinterrupt. Because the basic 6byte tempisr does not acknowledge the lineint the interrupt remains stuck in a loop forever. The new tempisr solves these minor issues.
tempinterrupt:

 
  push  af


  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt

  pop   af                
  ei
  ret  

;yes you may all murder me slaughter me and trow me of a cliff but the BIOS is simply in the way
;I basicly write a jump in the BIOS hook to this baby which ALWAYS acknowledges the lineint, vblank
;then pops back what the bios pushed and returns to the last point by incrementing the SP twice.
;Now we can safely call the BIOS without being affraid of it screwing up any programs or illegaly used system
;area.
inthook:

  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt

  pop ix
  pop iy
  pop af
  pop bc
  pop de
  pop hl
  ex af,af'
  exx
  pop af
  pop bc
  pop de
  pop hl
 
  inc sp
  inc sp
  ei

  ret  
  
intromenuisr:


 
  push  af


  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,.Lineint        ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,.vblank          ;vblank detected, so jp to that routine


  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret
  
  
.Lineint: 
 

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix


	ld		a,%1010'1111    ;spr att table to $15600
	out		($99),a		;spr att table to $15600
	ld		a,5+128
	out		($99),a
	ld		a,%0000'0010 ;2
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,%0010'1011    ;spr chr table to $15200
	out		($99),a		;spr chr table to $15200
	ld		a,6+128
	out		($99),a 
 
  
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
      ld                a,(slot.page12rom)        ;all RAM except page 1+2
        out                ($a8),a         

  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a

  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.


  ;handle sfx
      ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc                a
        ld                ($7000),a
        ld    a,Sfxblock
  ld    ($9000),a
     inc                a
        ld                ($b000),a
  call  MainSfxIntRoutine       ;keep running sfx every int
  call  SetSfx                                     ;if sfx is on, then set to play

   ;reset blocks
      ld                a,(memblocks.n1)     ;reset blocks
    ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a
        
        

  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        


  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 



  ei
  ret

.vblank:
 
  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a


  ;Sprite split
	ld		a,(VDP_0+5)
	out		($99),a
	ld		a,5+128
	out		($99),a
	ld		a,(VDP_8+3)
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,(VDP_0+6)
	out		($99),a
	ld		a,6+128
	out		($99),a
 
  
  pop   af 
  ei
  ret
  
;This little routine is used in the worldmap engine, and it copies the tilemap to page 1 in ram.
;Any changes in the map (when new levels become available after you finish a level) can then
;directly be updated in ram, which is easier for building up the screen (especially when scrolling).
;important note: at the end, worldmapengineblock is put back at $8000 in page 2 in rom
copymaptopage1inram: 
  call  block34
  ldir
  ld    a,worldmapengineblock
  jp    block34 

;important note: at the end, worldmapengineblock is put back at $8000 in page 2 in rom
loadworldmap:
  ld	  a,worldmapengineblock
  call	block34			        ;at address $8000 / page 2
  call    worldmap

  ld a,loaderblock
  call block34
  ret
 
  
;variables for SeeSfxreplayer
tempo:  DB    0,0
PATADD: DS    2               ;Current pattern address
_HALT:  DB    0               ;Halt counter (0-15)
loopnr: DB    0               ;Current loop number (0-3)
loopbf: DS    4*3
clpadr: DW    $00             ;"Continue" loop address

;       reg.  0 1 2 3 4 5 6 7 8 9 A B C D
PsgReg: DB    0,0,0,0,0,0,0,0,0,0,0,0,0,0 ;   - PSG port store buffer -


;GameisPaused?:        db  0
Sfxnr:                ds  1
ForceSfx_PR:          ds  1
SfxpreviousPriority:  ds  1
SfxPriority:          ds  1
SetSfx?:              db  0
Sfxon?:               db  0
ForceSfx?:            db  0



endenginepage3:
dephase
enginepage3length:	equ	$-enginepage3


include "variablespage3.asm"

