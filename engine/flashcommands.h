eraseflashrom:

    ld a,(flashrom?)
    cp 2
    jp z,eraseflashromrbsc


   
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    ld a,$50
    ld (hl),a   

    di    
    
    ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    
    ;erase block command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$80
    ld ($4aaa),a
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$30
    ld ($4000),a    
    
    ei
    
    
    ld    a,$FF   ; // This is the value of the flashROM memory after erasing it.
    ld    de,$4555   ; // Any address in the flashROM area should work.
    call    check    ;// Waits until the command has finished, and checks if there was any error (do not use wait loops!)
    
 ;   di
    
    ;read command
    ld a,$f0
    ld ($4000),a      
    
 ;   ei
    
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    xor a
    ld (hl),a    ;its save to close the write of like this. This just happens to accidentally select all the right values for a megaflashrom SCC + SD
    
    
  ;  ei  
  
    ret
 
 
eraseflashromrbsc:    


    
;    ;reset command
;    ld a,$aa
;    ld ($4aaa),a
;    ld a,$55
;    ld ($4555),a
;    ld a,$f0
;    ld ($4000),a
    
    di
    
    ;erase block command
    ld a,$aa
    ld ($4555),a
    ld a,$55
    ld ($42aa),a
    ld a,$80
    ld ($4555),a
    ld a,$aa
    ld ($4555),a
    ld a,$55
    ld ($42aa),a
    ld a,$30
    ld ($4000),a    
    
    ei
    
    ld    a,$FF   ; // This is the value of the flashROM memory after erasing it.
    ld    de,$4555   ; // Any address in the flashROM area should work.
    call    check    ;// Waits until the command has finished, and checks if there was any error (do not use wait loops!)
    
  ;  di
    
    ;read command
    ld a,$f0
    ld ($4000),a      
    
  ;  ei
 
  
    ret  

    
;-------------------------------------------------------------------------------
; Check flash command
; Out: Cy = error
;-------------------------------------------------------------------------------
check:
        push    bc
        ld    c,a
Check1:
        ld    a,(de)
        xor    c
        jp    p,Check2
        xor    c
        and    $20
        jp    z,Check1
        ld    a,(de)
        xor    c
        jp    p,Check2
        scf
Check2:
        pop    bc    
    
        ret

;write value of register c into hl
writetoflashrom:

    ld a,(flashrom?)
    cp 2
    jp z,writeflashromrbsc


  ;  di

    push hl
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    ld a,$50
    ld (hl),a  
    pop hl
  
     ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    ;write command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$a0
    ld ($4aaa),a

    ld a,c
    ld (hl),a
    
    
.wait:    ;wait for successfull write
    ld a,(hl)
    cp c
    jp nz,.wait  
    

    ;read command
    ld a,$f0
    ld ($4000),a   

    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    xor a
    ld (hl),a        
    
  ;  ei   
    
    ret 	
 
 
writeflashromrbsc:    
    


;     ;reset command
;    ld a,$aa
;    ld ($4aaa),a
;    ld a,$55
;    ld ($4555),a
;    ld a,$f0
;    ld ($4000),a
    
    di
    
    ;write command
    ld a,$aa
    ld ($4555),a
    ld a,$55
    ld ($42aa),a
    ld a,$a0
    ld ($4555),a

    ld a,c
    ld (hl),a
    
    ei
    
.wait:    ;wait for successfull write
    ld a,(hl)
    cp c
    jp nz,.wait  
    

    ;read command
    ld a,$f0
    ld ($4000),a   
 
    
    ret  
 
    
;usage: Just like ldir
ldirtoflashrom:


    ld a,(flashrom?)
    cp 2
    jp z,ldirtoflashromrbsc
  ;  di
 
    push hl
    push de
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    ld a,$50
    ld (hl),a 
    pop de
    pop hl
 
.writeloop: ;loop and reinitiate each byte increase write adress at each write 
 
    di
 
    ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    ;write command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$a0
    ld ($4aaa),a

    ld a,(hl)
    ld (de),a

    ei
    
.wait:    ;wait for successfull write
    ld a,(de)
    cp (hl)
    jp nz,.wait
    
    inc hl
    inc de
    dec bc
    
    ld a,b
    or c
    
    jp nz,.writeloop
  
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    xor a
    ld (hl),a      
  
  ;  di
 
    ;read command
    ld a,$f0
    ld ($4000),a   
 
  ;  ei
    	
    ret 

ldirtoflashromrbsc:


 
.writeloop: ;loop and reinitiate each byte increase write adress at each write 
 
;    ;reset command
;    ld a,$aa
;    ld ($4aaa),a
;    ld a,$55
;    ld ($4555),a
;    ld a,$f0
;    ld ($4000),a
    
    di
    
    ;write command
    ld a,$aa
    ld ($4555),a
    ld a,$55
    ld ($42aa),a
    ld a,$a0
    ld ($4555),a

    ld a,(hl)
    ld (de),a

    ei
    
.wait:    ;wait for successfull write
    ld a,(de)
    cp (hl)
    jp nz,.wait
    
    inc hl
    inc de
    dec bc
    
    ld a,b
    or c
    
    jp nz,.writeloop
   
 ;   di
 
    ;read command
    ld a,$f0
    ld ($4000),a   
 
  ;  ei

    	
    ret     
    
    
