;menu that holds the options. For now it only has the button quit
;and the button to select the controller type. My plan is to construct the menu
;out of gfx parts that are loaded from the rom in directly fired into VRAM on screen5
;Dunno how norakomi made those awesome menu's in manbow2 for example but I am sure I will get
;something working as well :)

;definitions
menuposition: dw $2f32 ;starting upper left position of the menu
cursorposition: equ $90 ;how far is the cursor to the right

setcpuinfo:

  ld a,(turbor?)
  cp 1
  jp z,.r800
  cp 2
  jp z,.z806
  cp 3
  jp z,.z807

  ret

.r800:
  
    ld a,3
    ld (optionsecondary),a
    ret
    
.z806:
  
    ld a,1
    ld (optionsecondary),a
    ret
    
.z807:
  
    ld a,2
    ld (optionsecondary),a
    ret
    
    
   
;NOTE: the whole menu is located on VRAM page4. This to keep the gfx in screen4 intact so that this menu can be called from ingame
menuoptions:

;init code

;switch to screen 5
  ld    a,(VDP_0)
  or    %0000'0010		      ;m3=1
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 5 

;set fill vram command for VDP here.
  call clearpage4vram



;set Controls to keyboard
  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.keyboard
  ld (hl),e
  inc hl
  ld (hl),d
  
  xor a
  ld (joymega?),a

;clear optionsmenuheight and optionsmenuposition
  xor a
  ld (optionsmenuheight),a
  ld (optionsmenuposition),a
  
  ld a,1
  ld (sfxactive),a ;set sfx to active so that the menu sound can be heard
  

;detect cpu and set the option in the selection
  call setcpuinfo


;Thank you Guillian for the helpfull tip
  ld a,$7f; page 4 $1f ; Page 0 vram
  out ($99),a
  ld a,128 + 2
  out ($99),a


;backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 
  xor a
  di
  out   ($99),a
  ld    a,7+128
  ei
  out   ($99),a
;/backdrop color-> reg7   TC3  TC2	TC1    TC0 BDC3  BDC2 BDC1 BDC0 
;disable sprites
 ld    a,%0010'1010 
 ld  (VDP_8+0),a
 di
 out  ($99),a
 ld  a,8+128
 ei
 out  ($99),a
  
  
  ld hl,defaultpalette
  call SetPalette
 ; nop ;ALLIGNMENTBUG
  
  call VDPwait ;wait for the ramfill command to finish


; ;initiate enviroment. Call BIOS back
;  di
;  ld a,(slot.bios)                             ;bios page 0 rom page1+2
;  out   ($a8),a    
;  
;  call $44 ;enable screen
  di

;set menu isr to handle sfx and framerate

  ld a,(slot.page12rom)
  out ($a8),a

  call enablescreen


  ld    hl,menuinterrupt 
  ld    ($38+1),hl          ;set new normal interrupt
  ld    a,$c3               
  ld    ($38),a

  ei


;construct the menu from gfx and table
  ld bc,(menuposition) ;upper left XY coordinates of menu items
  ld hl,optionsmenustructure ;menu table
  call drawmenu 
  ld a,(optionsmenuheight)
  ld (optionsmenuposition),a ;set pointer position
;we make the menu statically out of three columns so we add the static linked column size to the pointer
  ld a,b
  add a,cursorposition ;$90 ;to the right
  ld b,a
  ld a,c
  sub 6 ;up 6 pixels to allign to the lowest menu item
  ld c,a
  ld hl,optionitemarrow ;fetch arrow gfx
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage ;draw the arrow
  


;inner code
menuloop:


  call ReadControls ;read the control from the keyboard (and joystick in the future?)
  
  ld a,(NewPrControls)
  bit 0,a
  call nz,movecursorup
  ld a,(NewPrControls)
  bit 1,a
  call nz,movecursordown

  ld a,(NewPrControls)
  bit 2,a
  call nz,movemenuitem.left
  ld a,(NewPrControls)
  bit 3,a
  call nz,movemenuitem.right
  ld a,(NewPrControls)
  bit 4,a
  jp nz,executemenuitem  

  
;  ld b,7
;  
;	in	a,($AA)
;	and	$F0		;only change bits 0-3
;	or	b		;take row number from B
;	out	($AA),a
;	in	a,($A9)		;read row into A
;	
;	cpl
;	
; ld c,a
;
; ld  hl,standingonspr ;YES this is VERY VERY DIRTY!! I simply use a empty piece of ram I know it won't hurt.
; ld  a,(hl)
; xor  c
; and  c
;; ld  (NewPrControls),a
; ld  (hl),c	
;	
;	bit 2,a
;	jp nz,exitmenuloop
    

  ld    a,1
  ld    hl,vblankflag
.checkflag:
  cp    (hl)
  jp    nc,.checkflag

  ld    (hl),0
  jp menuloop


movecursorup:

  ld a,(optionsmenuposition)
  cp 1
  ret z ;check if the cursor is at the uppermost position
  
  dec a
  ld (optionsmenuposition),a ;register that the cursor has moved
  
  call clearcursor
  halt ;wait inbetween the V9958 commands
  call drawcursor
  
       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
  
  ret




movecursordown:

  ld a,(optionsmenuheight)
  ld c,a

  ld a,(optionsmenuposition)
  cp c
  ret z ;check if the cursor is at the uppermost position
  
  inc a
  ld (optionsmenuposition),a ;register that the cursor has moved
  
  call clearcursor
  halt ;wait inbetween the V9958 commands
  call drawcursor

       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number


  ret


drawcursor:

  ld bc,(menuposition)
  ;set new cursor position and draw the cursor
  ld a,b
  add a,cursorposition ;$90 ;to the right
  ld b,a

  ld a,(optionsmenuposition)
  add a,a ;*2
  ld e,a
  add a,a ;*4
  add a,e ;*6

  add a,c
  sub 6
  ld c,a


  ld hl,optionitemarrow ;fetch arrow gfx
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage ;draw the arrow
  
  ret


;clear the cursor from the are basicly do a zero VRAM fill on the cursor position
clearcursor: ;clears cursor data by using menuposition information


  ld bc,(menuposition)
  ld a,b
  add a,cursorposition ;$90
  ld b,a ;go to third column
  
  ld a,(optionsmenuheight)
  add a,a ;*2
  ld e,a
  add a,a ;*4
  add a,e ;*6
  
  ld e,a
  

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ;then set the coordinates
  ld a,b
  out ($9b),a ;X1
  xor a
  out ($9b),a ;X2
  push hl
  push bc
  ld h,0
  ld l,c
  ld bc,768
  add hl,bc
  ld a,l
  out ($9b),a ;Y1
  ld a,h
  out ($9b),a ;Y2
  pop bc
  pop hl
  
  
  ld a,6
  out ($9b),a ;X width
  xor a
  out ($9b),a ;X width 2
  ld a,e
  out ($9b),a ;Y width
  xor a
  out ($9b),a ;Y width 2  
  
  ;first byte should be here
  xor a
  out ($9b),a
  
  ;left right up down
 ; xor a
  out ($9b),a

  ;execute command
  ld a,%11000000
  out ($9b),a

  
  
  ei
  ret

clearpage4vram: ;clears cursor data by using menuposition information    

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ;then set the coordinates
  xor a
  out ($9b),a ;X1
  out ($9b),a ;X2
  ld hl,768
  ld a,l
  out ($9b),a ;Y1
  ld a,h
  out ($9b),a ;Y2

;  xor a
  out ($9b),a ;X width
  ld a,1
  out ($9b),a ;X width 2
  ld a,212
  out ($9b),a ;Y width
  xor a
  out ($9b),a ;Y width 2
  
  ;first byte should be here
  xor a
  out ($9b),a
  
  ;left right up down
;  xor a
  out ($9b),a

  ;execute command
  ld a,%11000000
  out ($9b),a 
  
  ei
  
  
  ret


movemenuitem:

.checkcpublocklft:

  ld a,(blockcpusetting)
  dec a
  ret z
  
  jp .continuelft


.left:

;check if cpu is allowed to be changed
  ld a,(optionsmenuposition)
  dec a
  jp z,.checkcpublocklft

.continuelft:

  ld a,(optionsmenuposition)
  dec a ;compensate for absolute numbers
  ld hl,optionsmenustructure
  ld b,0
  
  ld c,a ;fetch menutype from menutable
  add a,a ;*2
  add a,c ;*3
  ld c,a
  add hl,bc
  ld a,(hl)
  cp 128
  ret z ;is the quit button
  
  
  ld h,0
  ld l,a
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl ;*16
  ex de,hl
  ld hl,secondaryoptionsmenu
  add hl,de
    
  ld a,(hl)
  and a
  ret z ;no options

  
  push hl ;save the pointer for later
  
  ld a,(optionsmenuposition)
  dec a
  ld hl,optionsecondary
  ld d,0
  ld e,a
  add hl,de

  push hl
  pop bc ;make a copy of hl in bc
  
  ;position must not be changed when there is no reason to
  ld a,(hl)
  pop hl
  and a
  ret z
  dec a

  
;  pop hl
  ld d,0
  add a,a ;*2
  add a,a ;*4
  ld e,a
  add hl,de
  
  ld a,(hl) ;check if next option is available
  and a
  ret z
  
  
  ld a,(bc)
  dec a
  ld (bc),a ;change position of secondary menu
  
  
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl 
  
 
  ld bc,(menuposition) 

  ld a,b
  add a,$60
  ld b,a

    ;add correct Y coordinates into c for each menu item
  
  ld a,(optionsmenuposition)
  dec a
  jp z,.noadd2
  ld e,b ;temp
  ld b,a
  ld a,c
  
.add2:  
  add a,6
  djnz .add2
  
  ld c,a
  ld b,e
  
.noadd2: 
  
  
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage

       ld    de,03*256 + sfxchuckstomp
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number


  ret

.checkcpublock:

  ld a,(blockcpusetting)
  dec a
  ret z
  
  jp .continue

.right:

;check if cpu is allowed to be changed
  ld a,(optionsmenuposition)
  dec a
  jp z,.checkcpublock

.continue:
;  nop ;ALLIGNMENTBUG
  
  ld a,(optionsmenuposition)
  dec a ;compensate for absolute numbers
  ld hl,optionsmenustructure
  ld b,0
  
  ld c,a ;fetch menutype from menutable
  add a,a ;*2
  add a,c ;*3
  ld c,a
  add hl,bc
  ld a,(hl)
  cp 128
  ret z ;is the quit button
  
  
  ld h,0
  ld l,a
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl ;*16
  ex de,hl
  ld hl,secondaryoptionsmenu
  add hl,de
    
  ld a,(hl)
  and a
  ret z ;no options

  
  push hl ;save the pointer for later
  
  ld a,(optionsmenuposition)
  dec a
  ld hl,optionsecondary
  ld d,0
  ld e,a
  add hl,de

  push hl
  pop bc ;make a copy of hl in bc
  
  ;position must not be changed when there is no reason to
  ld a,(hl)
  inc a 
  
  pop hl
  
  cp 4 ;outer right limit
  ret nc
  
  ld d,0
  add a,a ;*2
  add a,a ;*4
  ld e,a
  add hl,de
  
  ld a,(hl) ;check if next option is available
  and a
  ret z
  
  ld a,(bc)
  inc a
  ld (bc),a ;change position of secondary menu
  
  
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl ;fetch data pointer
    
  
  ld bc,(menuposition) 

  ld a,b
  add a,$60
  ld b,a

  ;add correct Y coordinates into c for each menu item
  
  ld a,(optionsmenuposition)
  dec a
  jp z,.noadd
  ld e,b ;temp
  ld b,a
  ld a,c
  
.add:  
  add a,6
  djnz .add
  
  ld c,a
  ld b,e
  
.noadd:  
  
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage

       ld    de,03*256 + sfxchuckstomp
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
;  nop ;ALLIGNMENTBUG
    
  ret

  
setkeyboard:


  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.keyboard
  ld (hl),e
  inc hl
  ld (hl),d
  
  xor a
  ld (joymega?),a

  jp executetablecommand.return
  
setkeyboard2:

  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.keyboard2
  ld (hl),e
  inc hl
  ld (hl),d
  
  xor a
  ld (joymega?),a
  
  jp executetablecommand.return

setjoymega:

  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.joymega
  ld (hl),e
  inc hl
  ld (hl),d
  
  ld a,1
  ld (joymega?),a
  
  jp executetablecommand.return

setjoystick:

  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.joystick
  ld (hl),e
  inc hl
  ld (hl),d
  
  ld a,1
  ld (joymega?),a
  
  jp executetablecommand.return


setsfxon:

  ld a,1
  ld (sfxactive),a


  jp executetablecommand.return

setsfxoff:

  xor a
  ld (sfxactive),a


  jp executetablecommand.return

setmusicon:

  ld a,1
  ld (musicactive),a
  ld (musicactiveoption),a


  jp executetablecommand.return

setmusicoff:

  xor a
  ld (musicactive),a
  ld (musicactiveoption),a

  jp executetablecommand.return


setr800:


;  ld a,(slot.bios)
;  out ($a8),a

;  ld a,($2d)    ;ok (suppress memory access warning)
  ld a,(biostype)    ;ok (suppress memory access warning)
  cp 3
  ld a,(slot.page12rom)
  out ($a8),a
  jp nz,.cancelsetting

  ld a,1
  ld (turbor?),a
  
  ret

.cancelsetting:

  xor a
  ld (optionsecondary),a
  ret
  
setz807:



  ld a,3
  ld (turbor?),a

  ret

setz806:


  ld a,2
  ld (turbor?),a

  ret

setz804:


  xor a
  ld (turbor?),a

  ret


;setr800off:
;
;  ld a,%10000000
;  call $180 ;switch with bios
;  
;  ret
  
executemenuitem:

  ld a,(optionsmenuposition)
  dec a ;compensate for absolute numbers
  ld hl,optionsmenustructure
  ld b,0
  
  ld c,a ;fetch menutype from menutable
  add a,a ;*2
  add a,c ;*3
  ld c,a
  add hl,bc
  ld a,(hl)
  cp 128 
  jp z,exitmenuloop
  
  jp menuloop


exitmenuloop:


;go through the secondary options table and execute all the commands

  ld hl,optionsmenustructure
  
.searchoptionmenuitems:
  ld a,(hl)
  cp 255
  jp z,.exit ;last item found exit
  
  
  push hl
  call executetablecommand
  pop hl
  
  ld de,3
  add hl,de
  
  jp .searchoptionmenuitems

  
  
  
.exit:  

  ld a,(Sfxon?)
  and a
  jp nz,.exit ;wait for any sound to finish

;set original isr back
  call Settempisr


;exit code
;  di
;  ld a,(slot.bios)
;  out ($a8),a
;
;
;  call $41 ;disable screen again
;  di
  call disablescreen


;reenable sprites
 ld    a,%0010'1000 
 ld  (VDP_8+0),a
 di
 out  ($99),a
 ld  a,8+128
; ei
 out  ($99),a

 ;return initial values of romslot
 ld a,(slot.page12rom)
 ei
 out ($a8),a


  ret


;.Settempisr:	
;  ld    a,(VDP_0)
;  and   %1110'1111          ;reset ei1 (disable lineints)
;  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
;  di
;  out   ($99),a
;  ld    a,0+128
;;  ei
;  out   ($99),a
;
;	ld		hl,.tempisr
;	ld		de,$38
;	ld		bc,6
;	ldir
;  ei
;	ret  
; 
;.tempisr:	
;	push	af
;	in		a,($99)             ;check and acknowledge vblank int (ei0 is set)
;	pop		af
;	ei	
;	ret


executetablecommand:


;make correct entry search through secondaryoptionsmenutable
  ld a,(hl)
  
  ld hl,secondaryoptionsmenu 
  and a
  jp z,.firstitem
  cp 128 ;do not do the exit button
  ret z
  
  ld de,16
  ld b,a 
  
.multiply:  
  add hl,de
  
  djnz .multiply

   
.firstitem:
  
  
  push hl
;  pop ix ;

  ld hl,optionsecondary
  ld d,0
  ld e,a
  add hl,de ;find option selected
  ld a,(hl) ;this is the selection from the menu item involved

  add a,a ;*2
  add a,a ;*4
  ld e,a
  ld d,0
;  push ix
  pop hl
  
  add hl,de
  
  inc hl
  inc hl ;go to the command
  
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl ;command fetched
 
  jp (hl) ;execute command as given in table
 
.return:

  ret
	

drawmenu: ;in hl adress to the menustructure table

  ld a,(hl) ;check if end is reached
  cp 255
  ret z
  cp 128
  call nz,.drawsecondary
  


  push hl
  
  push bc
  
  inc hl
  ld c,(hl)
  inc hl
  ld b,(hl)

  ld h,b
  ld l,c ;transfer bc--->hl
  
  pop bc
  
  
;  ld bc,$0a32 ;XY coordinates
;  ld de,$5405 ;XY size
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage
  
  push bc
  pop hl
  ld bc,6
  add hl,bc ;offset for next item
  push hl
  pop bc
  
  pop hl
  
  inc hl
  inc hl
  inc hl ;popped byte needs three shifts


  ld a,(optionsmenuheight)
  inc a
  ld (optionsmenuheight),a ;generate menu entries maximum

  jp drawmenu
	

.drawsecondary:

  cp 128
  ret z ;not if it is the quit button

;fetch the position of this menuitem  
  
  push hl ;store data
  push bc

  ld hl,secondaryoptionsmenu
  and a
  jp z,.noadd
  push bc
  ld b,a
  ld de,16
.addloop:
  add hl,de
  djnz .addloop
  pop bc
  
.noadd:  
  
   push hl
  
  
  ld hl,optionsecondary
  ld e,a
  ld d,0
  add hl,de ;get the current option
  ld a,(hl); currently selected option in this slot
 
    pop hl

  add a,a
  add a,a ;*4
  ld e,a
  ld d,0
  add hl,de
  
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl ;option gfx data

  
  ld a,b
  add a,$60
  ld b,a

  
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch XY byte
  inc hl ;set pointer correct
  call drawmenuimage


  
  pop bc
  pop hl


  ret

	
	
drawmenuimage: ;in coordinates of the menuitem in bc(xy), size of menuitem in de(xy), gfx adress in hl 

;example of input
;  ld bc,$3232 ;XY coordinates
;  ld de,$5405 ;XY size
;  ld hl,optionitemcontroller ;GFX adress
  push hl
  push bc

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ;then set the coordinates
  ld a,b
  out ($9b),a ;X1
  xor a
  out ($9b),a ;X2
  push hl 
  ld l,c
  ld h,0
  ld bc,768
  add hl,bc
  ld a,l
  out ($9b),a ;Y1
  ld a,h
  out ($9b),a ;Y2
  pop hl
  
  ld a,d
  out ($9b),a ;X width
  xor a
  out ($9b),a ;X width 2
  ld a,e
  out ($9b),a ;Y width
  xor a
  out ($9b),a ;Y width 2
  
;calculate amount of copys
  ld b,e
  ld a,d
  add a,a
.multiplyd:

  add a,a

  djnz .multiplyd
 
  ld b,a
  
  ;first byte should be here
;  ld hl,testgfx
  ld a,(hl)
  out ($9b),a
  
  ;left right up down
  xor a
  out ($9b),a

  ;execute command
  ld a,255
  out ($9b),a

;  ld hl,testgfx
  inc hl ;skip first byte of data
;  ld b,90 ;9 rows left
  ld c,$99
  
  ;send rest of gfx data to VDP
.upload:
  
  outi
  ld a,44+128
  out ($99),a
  
  xor a
  cp b
  jp nz,.upload
  

  pop bc
  pop hl
  
  ei
  ret




;menu interrupt handler required to do the SFX and control the framerate
menuinterrupt:


 
  push  af
  
  ld a,1                 ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge lineint
  

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,.vblank          ;vblank detected, so jp to that routine


  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret

.vblank:

 ; ld    a,(vblankflag)
 ; inc   a
 ; ld    (vblankflag),a
  
;  pop af  
;  ei
;  ret ;TEMP

  

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix

    
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle sfx
        ld                a,(slot.page12rom)        ;all RAM except page 1+2
        out                ($a8),a         


    call handlesfxint



  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        

 
  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 

  ei
  ret


;
; taken directly from map.grauw.nl
;
; WTF! sometimes the VDP in openmsx refuses to set the CE register, better wait by hand then
VDPwait:

	halt
	halt
	halt
	halt
	halt    

	ret

;	ld	a,2
;	di
;	out	($99),a		;select s$2
;	ld	a,15+128
;	out	($99),a
;	in	a,($99)
;	rra
;	ld	a,0		;back to s$0, enable ints
;	out	($99),a
;	ld	a,15+128
;	ei
;	out	($99),a		;loop if vdp not ready (CE)
;	jp	c,VDPwait
;	ret



nocommand:	jp executetablecommand.return
  
;not sure about this yet... I have to create some kind of menu structure by using a table
                  ;itemamounts/properties,		gfxaddress
optionsmenustructure:
			db 0   : 	            dw optionitemcpu ;this one MUST always come first in the optionsecondary menu
			db 1   : 	            dw optionitemsfx
			db 2   : 	            dw optionitemmusic
			db 3   : 	            dw optionitemcontroller
	;		db 128 |		            dw optionitemcontinue
      db 128 : 	            dw optionitemquit ;128 is the quit button
                        db 255 ;end of table

                  ;secondary menu entries
secondaryoptionsmenu:
			dw optionitemz84,setz804,optionitemz86,setz806,optionitemz87,setz807, optionitemr800,setr800
			dw optionitemgreen,setsfxon,optionitemred,setsfxoff,0,0,0,0
			dw optionitemgreen,setmusicon,optionitemred,setmusicoff,0,0,0,0
			dw optionitemkeyboard, setkeyboard, optionitemjoymega, setjoymega, optionitemjoystick,setjoystick,optionitemkeyboard2,setkeyboard2 ;BUG: if there is any value other than 0 behind this line the menu will do unexpected things
                        dw 0,0,0,0,0,0,0,0

                        
			
optionitemcontroller:	dw $5405 ;XY size
			db $00,$ff,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$ff,$00,$ff,$ff,$00,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$ff,$ff,$00,$ff,$ff,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$ff,$00,$00,$ff,$ff,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$ff,$00
			db $00,$ff,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff,$00,$ff,$00,$ff

optionitemcontinue:	dw $4405 ;XY size
			db $00,$ff,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$ff,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$ff,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$ff,$00,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00
			db $00,$ff,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$ff,$ff,$00
			
optionitemarrow:	dw $0605
			db $00,$00,$ff
			db $00,$ff,$ff
			db $ff,$ff,$ff
			db $00,$ff,$ff
			db $00,$00,$ff
			
optionitemquit:		dw $1e05
			db $00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$ff,$ff
			db $ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00
			db $ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00
			db $ff,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00
			db $00,$ff,$ff,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00
			
optionitemsfx:		dw $1a05
			db $00,$ff,$ff,$00,$ff,$ff,$ff,$00,$ff,$00,$00,$00,$ff
			db $ff,$00,$00,$00,$ff,$00,$00,$00,$00,$ff,$00,$ff,$00
			db $00,$ff,$00,$00,$ff,$ff,$00,$00,$00,$00,$ff,$00,$00
			db $f0,$00,$ff,$00,$ff,$00,$00,$00,$00,$ff,$00,$ff,$00
			db $ff,$ff,$ff,$00,$ff,$00,$00,$00,$ff,$00,$00,$00,$ff
			
optionitemmusic:	dw $2805
			db $ff,$00,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$ff
			db $ff,$ff,$00,$ff,$ff,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00,$00,$ff,$00,$ff,$00,$00
			db $ff,$00,$ff,$00,$ff,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$00,$ff,$00,$ff,$00,$00
			db $ff,$00,$00,$00,$ff,$00,$ff,$00,$00,$ff,$00,$f0,$00,$ff,$00,$ff,$00,$ff,$00,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$ff,$ff,$00,$00,$ff,$ff,$ff,$00,$ff,$00,$00,$ff,$ff			
			
optionitemcpu:		dw $1a05
			db $00,$ff,$ff,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00
			db $ff,$00,$00,$00,$ff,$00,$ff,$00,$ff,$00,$00,$ff,$00
			db $ff,$00,$00,$00,$ff,$ff,$00,$00,$ff,$00,$00,$ff,$00
			db $ff,$00,$00,$00,$ff,$00,$00,$00,$ff,$00,$00,$ff,$00
			db $00,$ff,$ff,$00,$ff,$00,$00,$00,$00,$ff,$ff,$00,$00
			
optionitemr800:		dw $1e05
			db $ff,$ff,$00,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff
			db $ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff
			db $ff,$ff,$00,$00,$ff,$ff,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff
			db $ff,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff,$00,$ff
			db $ff,$00,$ff,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff,$00,$ff,$ff,$ff			

optionitemz84:		dw $1e05
			db $00,$f0,$00,$00,$ff,$f0,$00,$0f,$ff,$00,$ff,$ff,$ff,$00,$00
			db $0f,$00,$00,$00,$ff,$0f,$00,$f0,$ff,$00,$00,$0f,$00,$00,$00
			db $ff,$ff,$f0,$00,$ff,$00,$ff,$00,$ff,$00,$00,$f0,$00,$00,$00
			db $00,$f0,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff,$00,$00,$00,$00
			db $00,$f0,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff,$ff,$ff,$00,$00

optionitemz86:		dw $1e05
			db $00,$ff,$00,$00,$ff,$f0,$00,$0f,$ff,$00,$ff,$ff,$ff,$00,$00
			db $0f,$00,$00,$00,$ff,$0f,$00,$f0,$ff,$00,$00,$0f,$00,$00,$00
			db $0f,$ff,$f0,$00,$ff,$00,$ff,$00,$ff,$00,$00,$f0,$00,$00,$00
			db $0f,$00,$f0,$00,$ff,$00,$00,$00,$ff,$00,$ff,$00,$00,$00,$00
			db $00,$ff,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff,$ff,$ff,$00,$00
			
optionitemz87:		dw $1e05
			db $ff,$ff,$ff,$00,$ff,$f0,$00,$0f,$ff,$00,$ff,$ff,$ff,$00,$00
			db $00,$00,$ff,$00,$ff,$0f,$00,$f0,$ff,$00,$00,$0f,$00,$00,$00
			db $00,$ff,$00,$00,$ff,$00,$ff,$00,$ff,$00,$00,$f0,$00,$00,$00
			db $00,$f0,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff,$00,$00,$00,$00
			db $00,$f0,$00,$00,$ff,$00,$00,$00,$ff,$00,$ff,$ff,$ff,$00,$00			
			
			
			
optionitemkeyboard: dw $0e05
      db $0f,$ff,$ff,$ff,$ff,$00,$ff
      db $0f,$5f,$5f,$5f,$5f,$00,$0f
      db $0f,$ff,$ff,$ff,$ff,$00,$0f
      db $0f,$5f,$5f,$5f,$5f,$00,$0f
      db $0f,$ff,$ff,$ff,$ff,$00,$0f

optionitemkeyboard2: dw $0e05
      db $0f,$ff,$ff,$ff,$ff,$0f,$ff
      db $0f,$df,$df,$df,$df,$00,$0f
      db $0f,$ff,$ff,$ff,$ff,$00,$f0
      db $0f,$df,$df,$df,$df,$0f,$00
      db $0f,$ff,$ff,$ff,$ff,$0f,$ff      
      
optionitemjoymega: dw $0e05
      db $00,$ff,$00,$f0,$00,$ff,$00
      db $ff,$aa,$ff,$ff,$ff,$22,$ff
      db $fa,$aa,$af,$ff,$f7,$ff,$7f
      db $ff,$aa,$ff,$ff,$ff,$dd,$ff
      db $00,$ff,$00,$00,$00,$ff,$00

optionitemjoystick: dw $0e05
      db $00,$00,$00,$0d,$00,$00,$00
      db $00,$00,$00,$0f,$00,$00,$00
      db $00,$00,$00,$0f,$00,$00,$00
      db $00,$00,$fd,$ff,$ff,$00,$00
      db $00,$00,$0f,$00,$f0,$00,$00  
      
optionitemgreen: dw $0e05
      db $22,$00,$00,$00,$22,$00,$00
      db $02,$20,$00,$02,$20,$00,$00
      db $00,$22,$00,$22,$00,$00,$00
      db $00,$02,$22,$20,$00,$00,$00
      db $00,$00,$22,$00,$00,$00,$00
      
optionitemred: dw $0e05
      db $dd,$00,$00,$00,$dd,$00,$00
      db $00,$dd,$00,$dd,$00,$00,$00
      db $00,$00,$dd,$00,$00,$00,$00
      db $00,$dd,$00,$dd,$00,$00,$00
      db $dd,$00,$00,$00,$dd,$00,$00  

defaultpalette: db $00,$00,$00,$00,$11,$06,$33,$07,$17,$01,$27,$03,$51,$01,$27,$06
                db $71,$01,$73,$03,$61,$06,$64,$06,$11,$04,$65,$02,$55,$05,$77,$07            
