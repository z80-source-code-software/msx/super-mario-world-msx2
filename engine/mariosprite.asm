;===============================================================================
;Alle sprites worden vanaf hier aangestuurd. Gezien mario kan springen en vallen
;en vuur kan hebben en op yoshi kan zitten etc etc
;===============================================================================

;TODO: Deze routine is al voor een klein deel verbonden met mariophysics. Nadat alle
;sprites zijn ingevuld moeten we deze routine gewoon samenvoegen met mariophysics

resetmariowalktablep:

  xor a
  ld (mariowalktable),a
  ret  

;kopieer huidige mario positie in de sprite engine
copyleftright:

;speciaal wanneer we van richting verwisselen maar niet op een sheertile staan
  ld a,(holdspritedir)
  dec a
  ret z


;BUG: de routine is net te laat met een copy doen na een slide. Slide = direct copy doen!
;misschien een inversie doen zodra we een slide detecteren in tegengestelde richting
;Return de routine zodra we merken dat de keypresses niet overeen komen met de richting
  
  call readkeydirect


;maar als we meerdere toetsen tegelijk gaan rammen dan doen we altijd de copy en laten we de physics engine
;bepalen welke kant mario op kijkt
  ld b,a
  and %10010000
  and %10010000 ;twee keer om andere toetsen uit te sluiten
  jp nz,.docopy
  ld a,b ;acumulator inhoud terughalen


  bit 7,a
  jp nz,.checkleft
  bit 4,a
  jp nz,.checkright



.checksheerduck:

  ld a,(mariosheerduck)
  and a
  jp z,.docopyforreal
  ret

  
.docopy:


;wanneer we van een schuine tile naar beneden glijden mogen we in de lucht niet van richting veranderen
;Dat mogen we wel als we niet in de gebukte staat zijn.
  ld a,(fallspeed)
  and a
  jp nz,.checksheerduck
  
  ld a,(mariospeed)
  and a
  jp nz,.checksheerduck
  
.docopyforreal:

  ld a,(marioleftright)
  ld (marioleftrightspr),a

  ret

.checkleft:

;tegenstrijdige keypresses niet toestaan  
  ld a,(marioleftright)
  dec a
  jp z,.docopy
  
  ret


.checkright:

  ld a,(marioleftright)
  and a
  jp z,.docopy

  ret

;lees pijltjes toetsen uit. Geen pijltjes toetsen ingedrukt geen copy doen
resetsheerlock:


;mag niet wanneer we gebukt zijn   
;  ld a,(marioduck)
;  dec a
;  ret z

    call readkeydirect


  bit 7,a
  jp nz,.doreset
  bit 4,a
  jp nz,.doreset
   
  ret 
  
.doreset:
   
;  bit 6,a
;  ret nz 
   
  xor a
  ld (mariosheerhold),a
  ret



;mario sprite engine

invertleftrightspr:

;anders krijg je wierde slides in de lucht
  ld a,(mariofloating)
  dec a
  ret z

;alleen bij een slide
  ld a,(marioslide)
  and a
  ret z

;  jp .doinvert


  call readkeydirect

  bit 7,a
  jp nz,.doinvert
  bit 4,a
  jp nz,.doinvert


  ret

.doinvert:


  ld a,(marioleftrightspr)
  xor %00000001
  ld (marioleftrightspr),a

  ret


runturnshelltimer:

;als je de shell loslaat terwijl de timer loopt is het big trouble! dus meteen de timer resetten
  ld a,(marioshell)
  and a
  jp z,.stoptimer


  ld a,(marioturnshell)
  inc a
  cp 6
  jp nc,.stoptimer
  ld (marioturnshell),a
    ret
    
.stoptimer:
 
     xor a
     ld (marioturnshell),a
     ret 
  

usetimedsprites:

;TODO: branchen naar yoshi sprites. We moeten dus de yoshi marios indelen in losse branches om dit mogelijk te maken.

  ld a,(mariostate)
  and a
  jp z,.smallmario
  cp 3
  jp z,.feathermario

  ld a,(framecounter)
  and 2
  jp z,mariolarge
  jp mariofire

;  ret

.smallmario:

  ld a,(framecounter)
  and 2
  jp z,marionormalfire
  jp marionormal

.feathermario:

  ld a,(framecounter)
  and 2
  jp z,mariofeather
  jp mariofire



mariosprite:

  ld a,(inhibitsprite)
  and a
  ret nz

  ld a,(pauze)
  dec a
  ret z

  ld a,(demo)
  dec a
  call z,copyleftright.docopyforreal


;deze routine volledig uitschakelen zodat hij vanuit de transformatie kan worden aangestuurd zonder shit
;scheelt ook clockpower tijdens de event
  ld a,(mariotransforming)
  and a
  ret nz


;ergens is de turnshell geactiveerd
  ld a,(marioturnshell)
  and a
  call nz,runturnshelltimer

;ergens is de turnyoshi geactiveerd
  ld a,(marioturnyoshi)
  and a
  call nz,runturnyoshitimer


;check of we een copy doen van de marioleftright positie  
  ld a,(mariosheerhold)
  dec a
  call nz,copyleftright
  call resetsheerlock

 
 ;spr waarde inverteren wanneer er een slide plaatsvind in tegengestelde richting
 ld a,(marioleftright)
 ld b,a
 ld a,(marioleftrightspr)
 xor b
 call z,invertleftrightspr
 
  
 ;voetjes stilzetten als we niks doen
  ld a,(mariosheerslide)
  dec a
  call z,resetmariowalktablep

;yohsi?
  ld a,(marioyoshi)
  dec a
  jp z,marioyoshisprite

.ignoreyoshi:

;zijn we aan het zwemmen
  ld a,(marioswim)
  dec a
  jp z,marioswimming

;zijn we aan het klimmen
  ld a,(marioclimbing)
  dec a
  jp z,mariosprclimbing
  
  ld a,(mariostar)
  and a
  jp nz,usetimedsprites

.notimedsprites:

  ld a,(mariostate)
  cp 1
  jp z,mariolarge
  cp 2
  jp z,mariofire
  cp 3
  jp z,mariofeather
;  cp 4
;  jp z,.marioyoshi
  cp 10
  jp z,mariodead


;kleine mario
marionormal:


  ld a,$0d
  ld (blocknumber),a
;  ld hl,$4000
;  ld (blockadress),hl

  jp beginmarionormal
  
marionormalfire:

  ld a,$26
  ld (blocknumber),a
;  ld hl,$4000
;  ld (blockadress),hl

  jp beginmarionormal


beginmarionormal:

;mario is saying peace

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace


;turnshell afdwingen
  ld a,(marioturnshell)
  and a
  jp nz,.marioturnshell 



;we maken vanaf hier een branch die direct de shellpickup aanstuurt zolang de event plaatsvind

  ld a,(mariopickupshell)
  dec a
  jp z,.mariospritepickupshell

  ld a,(mariokickshell)
  dec a
  jp z,.mariospritekickshell

;en zwemmen wij toevallig??
  ld a,(marioswim)
  dec a
  jp z,marioswimmingnormal
  
  ;zijn we aan het klimmen
  ld a,(marioclimbing)
  dec a
  jp z,mariosprcclimbingnormal
  
;we checken hier of mario werkelijk stilstaat
;zo ja dan resetten we even de walktable om het mooier te laten werken


    ld a,(mariospeed)
    cp 0
    call z,.resetmariowalktable

;als we vallen moeten we een fall sprite laten zien
  ld a,(fallspeed)
  and a
  jp nz,.mariofall


;is mario in een sprong? nee dan lopen we dus
    ld a,(mariofloating)
    and a
    jp z,.mariowalk    
   
   call .mariojump

;mario is aan het vallen
;  ld a,(marionotdown)
;  dec a
;  jp z,.mariofall
   
   ret

.mariopeace:

  ld a,$26
  ld (blocknumber),a

  ld hl,6912
  call setspritenumber
  ret

  

 
.marioturnshell:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriteturnshellleft  
 

  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell1 


  jp .mariospriteturnshell2

 
.mariospriteturnshellleft:


  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell2 

  jp .mariospriteturnshell1 


 
; ret
 
 
.mariospinjump:
  

;lekker slordig en ranzig. Scheelt toch een aantal M cycles

  ld a,(mariospinjumptable)
  inc a
  ld (mariospinjumptable),a
    cp 9
  call z,.resetmariospinjumptable
  
  cp 1
  jp z,.mariospinjumpspritea
  cp 2
  jp z,.mariospinjumpspriteb
  cp 3
  jp z,.mariospinjumpspritec 
  cp 4
  jp z,.mariospinjumpsprited
  cp 5
  jp z,.mariospinjumpspritea
  cp 6
  jp z,.mariospinjumpspriteb
  cp 7
  jp z,.mariospinjumpspritec 
  cp 8
  jp z,.mariospinjumpsprited

  jp .mariospinjumpsprited
  
  ret
  
  
.mariospinjumpspritea:
  ld hl,3840
  call setspritenumber
  ret
  
.mariospinjumpspriteb:
  ld hl,192
  call setspritenumber
  ret
  
.mariospinjumpspritec:
  ld hl,4032
  call setspritenumber
  ret
  
.mariospinjumpsprited:
  ld hl,576
  call setspritenumber
  ret


.resetmariospinjumptable:

  xor a
  ld (mariospinjumptable),a
  ret

   
  
.mariojump:

  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;rennen we op full speed dan moeten we die sprite gebruiken
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunjump:

;voorkom heen en weer beweeg van de sprite als je in de slide op jump drukt
;  ld a,(marioslide)
;  dec a
;  ret z

;welke richting op
   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritejumpright  
 

 
.mariospritejumpleft:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2 

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

;schelpje in onze handen geeft een andere sprite
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
   
  ld hl,768
  call setspritenumber
  ret

.mariospritejumpright:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1

  ld hl,960
  call setspritenumber
  ret


;alleen wanneer mario op max speed rent
.mariorunjump:

;maken we een spinjump?
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump


   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriterunjumpright  
 

 
.mariospriterunjumpleft:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
   
  ld hl,2304
  call setspritenumber
  ret

.mariospriterunjumpright:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1

  ld hl,2496
  call setspritenumber
  ret


.mariofall:

;vanuit een spinjump moet je spinnend vallen
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;zijn we nog steeds aan het rennen dan moeten we speciaal vallen
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunfall:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritefallright 
  
.mariospritefallleft:

;sliden in de val toestaan bij schelpje en slide
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2 


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
   
  ld hl,1152
  call setspritenumber
  ret

.mariospritefallright:

;sliden in de val toestaan bij schelpje
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1

  ld hl,1344
  call setspritenumber
  ret

 
.mariowalk:
 
  
  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.mariorun
  
  
  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospritewalkright
  
 
.mariospritewalkleft:
  
 
;ergens hier moeten we branchen waarbij we kijken of we een slide doen
  
  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideright

;staan we gebukt?
  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
 

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownleft


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospritewalkleft2

;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
  ld a,(mariospeed)
  cp 4
  jp c,.mariowalkfootleftslow 
 
 
 
.mariowalkfootleftfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospritewalkleft1
  
  jp .mariospritewalkleft2
;  ret


.mariowalkfootleftslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospritewalkleft1
  
  jp .mariospritewalkleft2
;  ret
 
 
.mariospritewalkleft1:
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1


  ld hl,384
  call setspritenumber
  ret
  
.mariospritewalkleft2: ;staat stil
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,576
  call setspritenumber
  ret

.mariospritewalkright:  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideleft


;staan we gebukt??
  ld a,(marioduck)
  dec a
  jp z,.mariodownright

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownright


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospritewalkright2


;bepaal de snelheid waarmee de voetjes moeten bewegen
 
  ld a,(mariospeed)
  cp 4
  jp c,.mariowalkfootrightslow
 
 
.mariowalkfootrightfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospritewalkright1
  
  jp .mariospritewalkright2
;  ret

.mariowalkfootrightslow: 

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospritewalkright1
  
  jp .mariospritewalkright2
;  ret
   


.mariospritewalkright1:
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  
  ld hl,0
  call setspritenumber
  ret

.mariospritewalkright2: ;staat stil

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,192
  call setspritenumber
  ret

.mariospriteslideleft:
 

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  and a
  jp z,.mariospriteslideright 
 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2

;we hebben geen toetsen ingedrukt staan?
  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,3456
  call setspritenumber
  ret

.mariospriteslideright:


;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  dec a
  jp z,.mariospriteslideleft


  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1

  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,3648
  call setspritenumber
  ret


.mariodownleft:


  ld a,(mariolook)
  and a
  jp nz,.marioupleft

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkleft2


  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell1

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownleft


  ld hl,2688
  call setspritenumber
  ret 


.mariodownright:

  ld a,(mariolook)
  and a
  jp nz,.marioupright

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkright2

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell2

  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownright


  ld hl,2880
  call setspritenumber
  ret 
 

.mariospritesheerdownleft:

  ld hl,14976
  call setspritenumber
  ret
  
.mariospritesheerdownright:

  ld hl,15168
  call setspritenumber
  ret     
 
  
.marioupleft:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell1

  ld hl,3072
  call setspritenumber
  ret 


.marioupright:


  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell2

  ld hl,3264
  call setspritenumber
  ret  
  

;Mario rent!

.mariorun:



  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospriterunright
  
 
.mariospriterunleft:
  
 
 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownleft


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 2
  call nc,.resetmariowalktable
  
  cp 1
  jp c,.mariospriterunleft1
  
  jp .mariospriterunleft2
  ret
 
 
 
.mariospriterunleft1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
  

  ld hl,1536
  call setspritenumber
  ret
  
.mariospriterunleft2:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,1728
  call setspritenumber
  ret

.mariospriterunright:  

 
 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownright


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 2
  call nc,.resetmariowalktable
  
  cp 1
  jp c,.mariospriterunright1
  
  jp .mariospriterunright2
  ret


.mariospriterunright1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  ld hl,1920
  call setspritenumber
  ret

.mariospriterunright2: ;staat stil

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,2112
  call setspritenumber
  ret


;/mario rent

;/sprite mario normal

;allemaal code voor het schelpje. naar beneden verplaatsen when done

.mariospritepickupshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritepickupshellleft
  cp 1
  jp z,.mariospritepickupshellright
  

  ret

.mariospritekickshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritekickshellleft
  cp 1
  jp z,.mariospritekickshellright
  

  ret


.mariospritepickupshellleft:

  ld hl,5376
  call setspritenumber
  ret


.mariospritepickupshellright:

  ld hl,5568
  call setspritenumber
  ret

.mariospritekickshellleft:

  ld hl,13824
  call setspritenumber
  ret


.mariospritekickshellright:

  ld hl,14016
  call setspritenumber
  ret



.mariospriteholdshellleft1:

  ld hl,4608
  call setspritenumber
  ret


.mariospriteholdshellleft2: ;staat stil

  ld hl,4224
  call setspritenumber
  ret
  
.mariospriteholdshellright1:

  ld hl,4800
  call setspritenumber
  ret


.mariospriteholdshellright2: ;staat stil

  ld hl,4416
  call setspritenumber
  ret  
  
;gebranched vanuit de slide code je zal hier wat moeten aanpassen in de fysics.  
.mariospriteturnshell1:  

;  ld a,(mariosheertile)
;  and a
;  ret nz


  ld hl,4992
  call setspritenumber
  ret  
  
.mariospriteturnshell2:  

  ld hl,5184
  call setspritenumber
  ret
 
;mario gebukt met schelpje OF pakt schelpje op  
.mariospritetakeshell1:  ;left

  ld hl,5376
  call setspritenumber
  ret   
  
.mariospritetakeshell2:  ;right

  ld hl,5568
  call setspritenumber
  ret     
  
.mariospriteupshell1: ;left

  ld hl,5760
  call setspritenumber
  ret


.mariospriteupshell2: ;right
 
  ld hl,5952
  call setspritenumber
  ret
  
;/code voor schelpje


;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a
  ret


;===========================================grote mario===============================================
mariolarge:

  
   
;uit welke block halen we deze sprite??
  
  ld a,$0d
  ld (blocknumber),a
;  ld hl,$4000
;  ld (blockadress),hl


;mario is saying peace

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace
  
  
  ;turnshell afdwingen
  ld a,(marioturnshell)
  and a
  jp nz,.marioturnshell 
  
  
;is mario iets aan het oppakken dan negeren we alles en handelen we dat eerst af
  
  ld a,(mariopickupshell)
  dec a
  jp z,.mariospritepickupshell

  ld a,(mariokickshell)
  dec a
  jp z,.mariospritekickshell

;en zwemmen wij toevallig??
  ld a,(marioswim)
  dec a
  jp z,marioswimminglarge


  ;zijn we aan het klimmen
  ld a,(marioclimbing)
  dec a
  jp z,mariosprcclimbinglarge


;we checken hier of mario werkelijk stilstaat
;zo ja dan resetten we even de walktable om het mooier te laten werken





    ld a,(mariospeed)
    cp 0
    call z,.resetmariowalktable


;als we vallen moeten we een fall sprite laten zien
  ld a,(fallspeed)
  and a
  jp nz,.mariofall

;is mario in een sprong? nee dan lopen we dus
    ld a,(mariofloating)
    and a
    jp z,.mariowalk    
   
   call .mariojump

;mario is aan het vallen
;  ld a,(marionotdown)
;  dec a
;  jp z,.mariofall
   
   ret
 
 
.mariopeace:

  ld a,$26
  ld (blocknumber),a

  ld hl,7104
  call setspritenumber
  ret 
 
 
.marioturnshell:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriteturnshellleft  
 

  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell1 


  jp .mariospriteturnshell2

 
.mariospriteturnshellleft:


  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell2 

  jp .mariospriteturnshell1 


 
; ret
 
 
 
.mariospinjump:
  

;lekker slordig en ranzig. Scheelt toch een aantal M cycles

  ld a,(mariospinjumptable)
  inc a
  ld (mariospinjumptable),a
  cp 9
  call z,.resetmariospinjumptable
  
  cp 1
  jp z,.mariospinjumpspritea
  cp 2
  jp z,.mariospinjumpspriteb
  cp 3
  jp z,.mariospinjumpspritec 
  cp 4
  jp z,.mariospinjumpsprited
  cp 5
  jp z,.mariospinjumpspritea
  cp 6
  jp z,.mariospinjumpspriteb
  cp 7
  jp z,.mariospinjumpspritec 
  cp 8
  jp z,.mariospinjumpsprited

  jp .mariospinjumpsprited
  
;  ret
  
  
.mariospinjumpspritea:
  ld hl,14784
  call setspritenumber
  ret
  
.mariospinjumpspriteb:
  ld hl,8064
  call setspritenumber
  ret
  
.mariospinjumpspritec:
  ld hl,14592
  call setspritenumber
  ret
  
.mariospinjumpsprited:
  ld hl,8640
  call setspritenumber
  ret


.resetmariospinjumptable:

  xor a
  ld (mariospinjumptable),a
  ret

   
  
.mariojump:

  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;rennen we op full speed dan moeten we die sprite gebruiken
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunjump:

;welke richting op
   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritejumpright  
 

 
.mariospritejumpleft:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

;schelpje in onze handen geeft een andere sprite
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
   
  ld hl,6912
  call setspritenumber
  ret

.mariospritejumpright:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,7104
  call setspritenumber
  ret


;alleen wanneer mario op max speed rent
.mariorunjump:

;maken we een spinjump?
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump


   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriterunjumpright  
 

 
.mariospriterunjumpleft:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
   
  ld hl,7296
  call setspritenumber
  ret

.mariospriterunjumpright:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,7488
  call setspritenumber
  ret


.mariofall:

;vanuit een spinjump moet je spinnend vallen
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;zijn we nog steeds aan het rennen dan moeten we speciaal vallen
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunfall:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritefallright 
  
.mariospritefallleft:

;sliden in de val toestaan bij schelpje en slide
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
   
  ld hl,7680
  call setspritenumber
  ret

.mariospritefallright:

;sliden in de val toestaan bij schelpje
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright3

  ld hl,7872
  call setspritenumber
  ret

 
.mariowalk:
 
  
  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.mariorun
  
  
  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospritewalkright
  
 
.mariospritewalkleft:
  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideright

;staan we gebukt?
  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
 

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownleft


;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)
 
  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootleftslow
 
 
;  ld hl,mariospeedtable
;  add hl,de
;  ld a,(hl)
;  cp 3
;  jp z,.mariowalkfootleftslow
 
 
.mariowalkfootleftfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkleft3

  cp 1
  jp nc,.mariospritewalkleft2
  
  cp 0
  jp nc,.mariospritewalkleft1
  

;ret

;TODO: dit grapje gaat zo niet werken je zal nauwkeuriger moeten definieren bij welke pointer welke sprite hoort
.mariowalkfootleftslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable


  cp 4
  jp nc,.mariospritewalkleft3

  cp 2
  jp nc,.mariospritewalkleft2

  cp 0
  jp nc,.mariospritewalkleft1
  
;  ret
 
 
.mariospritewalkleft1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1


  ld hl,8064
  call setspritenumber
  ret
  
.mariospritewalkleft2: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,8256
  call setspritenumber
  ret
  
.mariospritewalkleft3: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,8448
  call setspritenumber
  ret  

.mariospritewalkright:  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideleft


;staan we gebukt??
  ld a,(marioduck)
  dec a
  jp z,.mariodownright

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownright

;bepaal de snelheid waarmee de voetjes moeten bewegen
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)
 

  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootrightslow


.mariowalkfootrightfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkright3

  cp 1
  jp nc,.mariospritewalkright2
  
  cp 0
  jp nc,.mariospritewalkright1
  

;ret

.mariowalkfootrightslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable


  cp 4
  jp nc,.mariospritewalkright3

  cp 2
  jp nc,.mariospritewalkright2

  cp 0
  jp nc,.mariospritewalkright1
  
;  ret   


.mariospritewalkright1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  
  ld hl,8640
  call setspritenumber
  ret

.mariospritewalkright2: 

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,8832
  call setspritenumber
  ret

.mariospritewalkright3:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright3

  ld hl,9024
  call setspritenumber
  ret



.mariospriteslideleft:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  and a
  jp z,.mariospriteslideright 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2
  
  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,9216
  call setspritenumber
  ret

.mariospriteslideright:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  dec a
  jp z,.mariospriteslideleft 

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1

  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,9408
  call setspritenumber
  ret


.mariodownleft:

  ld a,(mariolook)
  and a
  jp nz,.marioupleft

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkleft1

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell1

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownleft


  ld hl,9600
  call setspritenumber
  ret 


.mariodownright:

  ld a,(mariolook)
  and a
  jp nz,.marioupright

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkright1


  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec

  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell2

;sheerduck speciale sprite laten zien ;copy  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownright


  ld hl,9792
  call setspritenumber
  ret 
 

.mariospritesheerdownleft:

  ld hl,15360
  call setspritenumber
  ret 


.mariospritesheerdownright:

  ld hl,15552
  call setspritenumber
  ret 
  
.marioupleft:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell1

  ld hl,9984
  call setspritenumber
  ret 


.marioupright:


  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell2

  ld hl,10176
  call setspritenumber
  ret  
  

;Mario rent!

.mariorun:
 


  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospriterunright
  
 
.mariospriterunleft:
  

 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownleft

  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret
 
 
 
.mariospriterunleft1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
  

  ld hl,10368
  call setspritenumber
  ret
  
.mariospriterunleft2:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,10560
  call setspritenumber
  ret
  
.mariospriterunleft3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,10752
  call setspritenumber
  ret

.mariospriterunright:  

 
 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownright


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunright1
  
  cp 1
  jp z,.mariospriterunright2
  
  cp 2
  jp z,.mariospriterunright3
  ret


.mariospriterunright1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  ld hl,10944
  call setspritenumber
  ret

.mariospriterunright2: 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,11136
  call setspritenumber
  ret
  
.mariospriterunright3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,11328
  call setspritenumber
  ret  


;/mario rent

;/sprite mario normal



;allemaal code voor het schelpje. naar beneden verplaatsen when done



.mariospritepickupshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritepickupshellleft
  cp 1
  jp z,.mariospritepickupshellright
  

  ret

.mariospritekickshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritekickshellleft
  cp 1
  jp z,.mariospritekickshellright
  

  ret


.mariospritepickupshellleft:

  ld hl,13056
  call setspritenumber
  ret


.mariospritepickupshellright:

  ld hl,13248
  call setspritenumber
  ret

.mariospritekickshellleft:

  ld hl,14208
  call setspritenumber
  ret


.mariospritekickshellright:

  ld hl,14400
  call setspritenumber
  ret




.mariospriteholdshellleft1: ;staat stil

  ld hl,11520
  call setspritenumber
  ret


.mariospriteholdshellleft2: 

  ld hl,11712
  call setspritenumber
  ret
  
.mariospriteholdshellleft3: 

  ld hl,11904
  call setspritenumber
  ret  

;naar rechts :P  
.mariospriteholdshellright1: ;staat stil

  ld hl,12096
  call setspritenumber
  ret


.mariospriteholdshellright2: 

  ld hl,12288
  call setspritenumber
  ret  
 
.mariospriteholdshellright3: 

  ld hl,12480
  call setspritenumber
  ret   
  
;gebranched vanuit de slide code je zal hier wat moeten aanpassen in de fysics.  
.mariospriteturnshell1:  

  ld hl,12672
  call setspritenumber
  ret  
  
.mariospriteturnshell2:  

  ld hl,12864
  call setspritenumber
  ret
 
;mario gebukt met schelpje OF pakt schelpje op  
.mariospritetakeshell1:  ;left

  ld hl,13056
  call setspritenumber
  ret   
  
.mariospritetakeshell2:  ;right

  ld hl,13248
  call setspritenumber
  ret     
  
.mariospriteupshell1: ;left

  ld hl,13440
  call setspritenumber
  ret


.mariospriteupshell2: ;right
 
  ld hl,13632
  call setspritenumber
  ret
  
;/code voor schelpje


;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a
  ret


.resetmarioruntable:

  xor a
  ld (marioruntable),a
  ret
 

;===========================================Vuur mario===============================================
mariofire:
 
 
 
 ;uit welke block halen we deze sprite??
  
  ld a,$09
  ld (blocknumber),a
;  ld hl,$4000
;  ld (blockadress),hl
 
  ;mario is saying peace

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace
  
  
  ;turnshell afdwingen
  ld a,(marioturnshell)
  and a
  jp nz,.marioturnshell 
  
;is mario iets aan het oppakken dan negeren we alles en handelen we dat eerst af
  
  ld a,(mariopickupshell)
  dec a
  jp z,.mariospritepickupshell

  ld a,(mariokickshell)
  dec a
  jp z,.mariospritekickshell

;en zwemmen wij toevallig??
  ld a,(marioswim)
  dec a
  jp z,marioswimmingfire


  ;zijn we aan het klimmen
  ld a,(marioclimbing)
  dec a
  jp z,mariosprcclimbingfire

;we checken hier of mario werkelijk stilstaat
;zo ja dan resetten we even de walktable om het mooier te laten werken

  ld a,(marioshot)
  ld b,a
  xor a
  ld a,(mariostate)
  inc a
  rra
  rra
;  xor %00000001
  and b
  jp nz,.mariospinjump



    ld a,(mariospeed)
    cp 0
    call z,.resetmariowalktable

;als we vallen moeten we een fall sprite laten zien
  ld a,(fallspeed)
  and a
  jp nz,.mariofall


;is mario in een sprong? nee dan lopen we dus
    ld a,(mariofloating)
    and a
    jp z,.mariowalk    
   
   call .mariojump

;mario is aan het vallen
;  ld a,(marionotdown)
;  dec a
;  jp z,.mariofall
   
   ret


.mariopeace:

  ld a,$26
  ld (blocknumber),a

  ld hl,7296
  call setspritenumber
  ret 

.marioturnshell:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriteturnshellleft  
 

  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell1 


  jp .mariospriteturnshell2

 
.mariospriteturnshellleft:


  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell2 

  jp .mariospriteturnshell1 

;ret

 
.mariospinjump:
  

;lekker slordig en ranzig. Scheelt toch een aantal M cycles

  ld a,(mariospinjumptable)
  inc a
  ld (mariospinjumptable),a
  cp 9
  call z,.resetmariospinjumptable
  
  cp 1
  jp z,.mariospinjumpspritea
  cp 2
  jp z,.mariospinjumpspriteb
  cp 3
  jp z,.mariospinjumpspritec 
  cp 4
  jp z,.mariospinjumpsprited
  cp 5
  jp z,.mariospinjumpspritea
  cp 6
  jp z,.mariospinjumpspriteb
  cp 7
  jp z,.mariospinjumpspritec 
  cp 8
  jp z,.mariospinjumpsprited

  jp .mariospinjumpsprited
  
  ret
  
  
.mariospinjumpspritea:
  ld hl,7488
  call setspritenumber
  ret
  
.mariospinjumpspriteb:
  ld hl,1152
  call setspritenumber
  ret
  
.mariospinjumpspritec:
  ld hl,7296
  call setspritenumber
  ret
  
.mariospinjumpsprited:
  ld hl,1728
  call setspritenumber
  ret


.resetmariospinjumptable:

  xor a
  ld (mariospinjumptable),a
  ret

   
  
.mariojump:

  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;rennen we op full speed dan moeten we die sprite gebruiken
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunjump:

;welke richting op
   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritejumpright  
 

 
.mariospritejumpleft:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2 

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

;schelpje in onze handen geeft een andere sprite
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
   
  ld hl,0
  call setspritenumber
  ret

.mariospritejumpright:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,192
  call setspritenumber
  ret


;alleen wanneer mario op max speed rent
.mariorunjump:

;maken we een spinjump?
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump


   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriterunjumpright  
 

 
.mariospriterunjumpleft:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
   
  ld hl,384
  call setspritenumber
  ret

.mariospriterunjumpright:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,576
  call setspritenumber
  ret


.mariofall:

;vanuit een spinjump moet je spinnend vallen
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;zijn we nog steeds aan het rennen dan moeten we speciaal vallen
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunfall:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritefallright 
  
.mariospritefallleft:

;sliden in de val toestaan bij schelpje en slide
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2 


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
   
  ld hl,768
  call setspritenumber
  ret

.mariospritefallright:

;sliden in de val toestaan bij schelpje
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


;TODO: OMG JE KAN OOK BUKKEN MET SCHELPJE!!

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright3

  ld hl,960
  call setspritenumber
  ret

 
.mariowalk:
 
  
  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.mariorun
  
  
  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospritewalkright
  
 
.mariospritewalkleft:
  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideright

;staan we gebukt?
  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
 

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownleft


;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)

  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootleftslow
 
;  ld hl,mariospeedtable
;  add hl,de
;  ld a,(hl)
;  cp 3
;  jp z,.mariowalkfootleftslow
 
 
.mariowalkfootleftfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkleft3

  cp 1
  jp nc,.mariospritewalkleft2
    
  cp 0
  jp nc,.mariospritewalkleft1
  

  

  
  
  ;ret


.mariowalkfootleftslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable

  cp 4
  jp nc,.mariospritewalkleft3

  cp 2
  jp nc,.mariospritewalkleft2
    
  cp 1
  jp nc,.mariospritewalkleft1
  

  


  
 ; ret
 
 
.mariospritewalkleft1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1


  ld hl,1152
  call setspritenumber
  ret
  
.mariospritewalkleft2: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,1344
  call setspritenumber
  ret
  
.mariospritewalkleft3: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,1536
  call setspritenumber
  ret  

.mariospritewalkright:  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideleft


;staan we gebukt??
  ld a,(marioduck)
  dec a
  jp z,.mariodownright

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownright

;bepaal de snelheid waarmee de voetjes moeten bewegen
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)
 
  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootrightslow

;  ld hl,mariospeedtable
;  add hl,de
;  ld a,(hl)
;  cp 3
;  jp z,.mariowalkfootrightslow
 
 
.mariowalkfootrightfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkright3

  cp 1
  jp nc,.mariospritewalkright2
    
  cp 0
  jp nc,.mariospritewalkright1
  

  

  
  
  ;ret


.mariowalkfootrightslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable

  cp 4
  jp nc,.mariospritewalkright3

  cp 2
  jp nc,.mariospritewalkright2
    
  cp 0
  jp nc,.mariospritewalkright1
  

  


  
 ; ret
.mariospritewalkright1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  
  ld hl,1728
  call setspritenumber
  ret

.mariospritewalkright2: 

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,1920
  call setspritenumber
  ret

.mariospritewalkright3:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright3

  ld hl,2112
  call setspritenumber
  ret



.mariospriteslideleft:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  and a
  jp z,.mariospriteslideright 


  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2
  
  ld a,(mariosheerhold)
  and a
  ret nz
  

  ld hl,2304
  call setspritenumber
  ret

.mariospriteslideright:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  dec a
  jp z,.mariospriteslideleft 


  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1

  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,2496
  call setspritenumber
  ret


.mariodownleft:

  ld a,(mariolook)
  and a
  jp nz,.marioupleft

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkleft1

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell1

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownleft


  ld hl,2688
  call setspritenumber
  ret 


.mariodownright:

  ld a,(mariolook)
  and a
  jp nz,.marioupright

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkright1

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell2

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownright


  ld hl,2880
  call setspritenumber
  ret 

.mariospritesheerdownleft:

  ld hl,7680
  call setspritenumber
  ret 
  
.mariospritesheerdownright:

  ld hl,7872 
  call setspritenumber
  ret 


  
.marioupleft:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell1

  ld hl,3072
  call setspritenumber
  ret 


.marioupright:


  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell2

  ld hl,3264
  call setspritenumber
  ret  
  

;Mario rent!

.mariorun:
 


  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospriterunright
  
 
.mariospriterunleft:
  

 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownleft


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret
 
 
 
.mariospriterunleft1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
  

  ld hl,3456
  call setspritenumber
  ret
  
.mariospriterunleft2:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,3648
  call setspritenumber
  ret
  
.mariospriterunleft3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,3840
  call setspritenumber
  ret

.mariospriterunright:  

 
 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownright


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunright1
  
  cp 1
  jp z,.mariospriterunright2
  
  cp 2
  jp z,.mariospriterunright3
  ret


.mariospriterunright1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  ld hl,4032
  call setspritenumber
  ret

.mariospriterunright2: 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,4224
  call setspritenumber
  ret
  
.mariospriterunright3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,4416
  call setspritenumber
  ret  


;/mario rent

;/sprite mario normal



;allemaal code voor het schelpje. naar beneden verplaatsen when done



.mariospritepickupshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritepickupshellleft
  cp 1
  jp z,.mariospritepickupshellright
  

  ret

.mariospritekickshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritekickshellleft
  cp 1
  jp z,.mariospritekickshellright
  

  ret


.mariospritepickupshellleft:

  ld hl,6144
  call setspritenumber
  ret


.mariospritepickupshellright:

  ld hl,6336
  call setspritenumber
  ret

.mariospritekickshellleft:

  ld hl,6912
  call setspritenumber
  ret


.mariospritekickshellright:

  ld hl,7104
  call setspritenumber
  ret




.mariospriteholdshellleft1: ;staat stil

  ld hl,4608
  call setspritenumber
  ret


.mariospriteholdshellleft2: 

  ld hl,4800
  call setspritenumber
  ret
  
.mariospriteholdshellleft3: 

  ld hl,4992
  call setspritenumber
  ret  

;naar rechts :P  
.mariospriteholdshellright1: ;staat stil

  ld hl,5184
  call setspritenumber
  ret


.mariospriteholdshellright2: 

  ld hl,5376
  call setspritenumber
  ret  
 
.mariospriteholdshellright3: 

  ld hl,5568
  call setspritenumber
  ret   
  
;gebranched vanuit de slide code je zal hier wat moeten aanpassen in de fysics.  
.mariospriteturnshell1:  

  ld hl,5760
  call setspritenumber
  ret  
  
.mariospriteturnshell2:  

  ld hl,5952
  call setspritenumber
  ret
 
;mario gebukt met schelpje OF pakt schelpje op  
.mariospritetakeshell1:  ;left

  ld hl,6144
  call setspritenumber
  ret   
  
.mariospritetakeshell2:  ;right

  ld hl,6336
  call setspritenumber
  ret     
  
.mariospriteupshell1: ;left

  ld hl,6528
  call setspritenumber
  ret


.mariospriteupshell2: ;right
 
  ld hl,6720
  call setspritenumber
  ret
  
;/code voor schelpje


;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a
  ret


;=================================================vliegende mario================================================
mariofeather:

;wip 
 ;uit welke block halen we deze sprite??
  
  ld a,$05
  ld (blocknumber),a
  
  
  ;mario is saying peace

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace
  
  
  ld a,(marioflyspecial)
  dec a
  jp z,mariofeatherspecial
 
 ;turnshell afdwingen
  ld a,(marioturnshell)
  and a
  jp nz,.marioturnshell 
 
  
;is mario iets aan het oppakken dan negeren we alles en handelen we dat eerst af
  
  ld a,(mariopickupshell)
  dec a
  jp z,.mariospritepickupshell

  ld a,(mariokickshell)
  dec a
  jp z,.mariospritekickshell

;en zwemmen wij toevallig??
  ld a,(marioswim)
  dec a
  jp z,marioswimmingflying

  ;zijn we aan het klimmen
  ld a,(marioclimbing)
  dec a
  jp z,mariosprcclimbingflying

;we checken hier of mario werkelijk stilstaat
;zo ja dan resetten we even de walktable om het mooier te laten werken


  ld a,(marioshot)
  and a
  jp nz,.marioshootsprite



    ld a,(mariospeed)
    cp 0
    call z,.resetmariowalktable

;als we vallen moeten we een fall sprite laten zien
  ld a,(fallspeed)
  and a
  jp nz,.mariofall


;is mario in een sprong? nee dan lopen we dus
    ld a,(mariofloating)
    and a
    jp z,.mariowalk    
   
   call .mariojump

;mario is aan het vallen ;deze check moet wel blijven omdat mario heel even stilhangt in de lucht 
;nadat je de space key released vanuit zweven
  ld a,(marionotdown)
  dec a
  jp z,.mariofall
   
   ret
 

.mariopeace:

  ld a,$26
  ld (blocknumber),a

  ld hl,7488
  call setspritenumber
  ret 

.marioturnshell:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriteturnshellleft  
 

  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell1 


  jp .mariospriteturnshell2

 
.mariospriteturnshellleft:


  ld a,(marioturnshell)
  cp 3
  jp nc,.mariospriteturnshell2 

  jp .mariospriteturnshell1 


 
; ret
 

.marioshootsprite:
  

;lekker slordig en ranzig. Scheelt toch een aantal M cycles

  ld a,(mariospinjumptable)
  inc a
  cp 9
  jp z,.resetmariospinjumptable
  ld (mariospinjumptable),a
  
  cp 1
  jp z,.mariospinjumpspritea
  cp 2
  jp z,.mariospinjumpspriteb
  cp 3
  jp z,.mariospinjumpspritec 
  cp 4
  jp z,.mariospinjumpsprited
  cp 5
  jp z,.mariospinjumpspritea
  cp 6
  jp z,.mariospinjumpspriteb
  cp 7
  jp z,.mariospinjumpspritec 
  cp 8
  jp z,.mariospinjumpsprited


  
  ret

 
.mariospinjump:
  

;lekker slordig en ranzig. Scheelt toch een aantal M cycles

  ld a,(mariospinjumptable)
  inc a
  cp 9
  jp z,.resetmariospinjumptable
  ld (mariospinjumptable),a
  
  cp 1
  jp z,.mariospinjumpspritea
  cp 2
  jp z,.mariospinjumpspriteb
  cp 3
  jp z,.mariospinjumpspritec 
  cp 4
  jp z,.mariospinjumpsprited
  cp 5
  jp z,.mariospinjumpspritea
  cp 6
  jp z,.mariospinjumpspriteb
  cp 7
  jp z,.mariospinjumpspritec 
  cp 8
  jp z,.mariospinjumpsprited


  
  ret
  
  
.mariospinjumpspritea:
  ld hl,7488
  call setspritenumber
  ret
  
.mariospinjumpspriteb:
  ld hl,1152
  call setspritenumber
  ret
  
.mariospinjumpspritec:
  ld hl,7296
  call setspritenumber
  ret
  
.mariospinjumpsprited:
  ld hl,1728
  call setspritenumber
  ret


.resetmariospinjumptable:

  xor a
  ld (mariospinjumptable),a
  ret

   
  
.mariojump:

  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;rennen we op full speed dan moeten we die sprite gebruiken
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunjump:

;welke richting op
   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritejumpright  
 

 
.mariospritejumpleft:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2 

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

;schelpje in onze handen geeft een andere sprite
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2

;mario vliegt niet omhoog (en de jumptoets word vastgehouden)
  ld a,(marioflying)
  and a
  jp z,.jumpleft1 

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.jumpleft1
  cp 2
  jp z,.jumpleft2

.jumpleft1:

  ld hl,0
  call setspritenumber
  ret

.jumpleft2:

  ld hl,10560
  call setspritenumber
  ret

.mariospritejumpright:

;sliden altijd toelaten wanneer mario schelpje heeft en anders nooit
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

;mario vliegt niet omhoog (en de jumptoets word vastgehouden)
  ld a,(marioflying)
  and a
  jp z,.jumpright1 

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.jumpright1
  cp 2
  jp z,.jumpright2


.jumpright1:

  ld hl,192
  call setspritenumber
  ret

.jumpright2:

  ld hl,10752
  call setspritenumber
  ret

;alleen wanneer mario op max speed rent
.mariorunjump:

;maken we een spinjump?
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump


   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospriterunjumpright  
 

 
.mariospriterunjumpleft:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  
  ;jumpkey uitlezen ander mogen we geen knipper geven
  ld a,(mariojumpkey)
  and a
  jp z,.runjumpleft1

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.runjumpleft1
  cp 2
  jp z,.runjumpleft2
  

.runjumpleft1:
   
  ld hl,384
  call setspritenumber
  ret
  
.runjumpleft2:
   
  ld hl,8064
  call setspritenumber
  ret  

.mariospriterunjumpright:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ;jumpkey uitlezen ander mogen we geen knipper geven
  ld a,(mariojumpkey)
  and a
  jp z,.runjumpright1

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.runjumpright1
  cp 2
  jp z,.runjumpright2


.runjumpright1:

  ld hl,576
  call setspritenumber
  ret

.runjumpright2:

  ld hl,8256
  call setspritenumber
  ret


.mariofall:

;vanuit een spinjump moet je spinnend vallen
  ld a,(mariospinjump)
  dec a
  jp z,.mariospinjump

;zijn we nog steeds aan het rennen dan moeten we speciaal vallen
   ld a,(mariorunning)
   dec a
   jp nz,.mariorunjump

.skiprunfall:

   ld a,(marioleftrightspr)
   dec a
   jp z,.mariospritefallright 
  
.mariospritefallleft:

;sliden in de val toestaan bij schelpje en slide
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2


  ld a,(marioduck)
  dec a
  jp z,.mariodownleft


  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2

;jumpkey uitlezen ander mogen we geen knipper geven
  ld a,(mariojumpkey)
  and a
  jp z,.mariofallleft1

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariofallleft1
  cp 2
  jp z,.mariofallleft2 



.mariofallleft1:
   
  ld hl,768
  call setspritenumber
  ret
  
  
.mariofallleft2:
   
  ld hl,7680
  call setspritenumber
  ret  

.mariospritefallright:

;sliden in de val toestaan bij schelpje
  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1


  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2


;jumpkey uitlezen ander mogen we geen knipper geven
  ld a,(mariojumpkey)
  and a
  jp z,.mariofallright1

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariofallright1
  cp 2
  jp z,.mariofallright2 


.mariofallright1:

  ld hl,960
  call setspritenumber
  ret

.mariofallright2:

  ld hl,7872
  call setspritenumber
  ret


.resetmarioflickrtable:

  xor a
  ld (marioflickrtable),a
  ret

 
.mariowalk:
 
  
  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.mariorun
  
  
  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospritewalkright
  
 
.mariospritewalkleft:
  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideright

;staan we gebukt?
  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
 

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownleft


;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)
 
  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootleftslow
 
 
;  ld hl,mariospeedtable
;  add hl,de
;  ld a,(hl)
;  cp 3
;  jp z,.mariowalkfootleftslow
 
 
.mariowalkfootleftfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkleft3

  cp 1
  jp nc,.mariospritewalkleft2
  
  cp 0
  jp nc,.mariospritewalkleft1
  
  ret


.mariowalkfootleftslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 1
  jp z,.mariospritewalkleft1
  
  cp 3
  jp z,.mariospritewalkleft2
  
  cp 5
  jp z,.mariospritewalkleft3
  ret
 
 
.mariospritewalkleft1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1


  ld hl,1152
  call setspritenumber
  ret
  
.mariospritewalkleft2: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,1344
  call setspritenumber
  ret
  
.mariospritewalkleft3: 
 
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,1536
  call setspritenumber
  ret  

.mariospritewalkright:  

;ergens hier moeten we branchen waarbij we kijken of we een slide doen

  ld a,(marioslide)
  dec a
  jp z,.mariospriteslideleft


;staan we gebukt??
  ld a,(marioduck)
  dec a
  jp z,.mariodownright

;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  cp 0
  jp z,.mariodownright

;bepaal de snelheid waarmee de voetjes moeten bewegen
 
;  ld d,0
;  ld hl,mariospeedtablep
;  ld e,(hl)
 
  ld a,(Controls)
  bit 5,a
  jp z,.mariowalkfootrightslow


;  ld hl,mariospeedtable
;  add hl,de
;  ld a,(hl)
;  cp 3
;  jp z,.mariowalkfootrightslow
 
 
.mariowalkfootrightfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.mariospritewalkright3

  cp 1
  jp nc,.mariospritewalkright2
  
  cp 0
  jp nc,.mariospritewalkright1
  
;ret  

.mariowalkfootrightslow: 

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 1
  jp z,.mariospritewalkright1
  
  cp 3
  jp z,.mariospritewalkright2
  
  cp 5
  jp z,.mariospritewalkright3
  ret
   


.mariospritewalkright1: ;staat stil
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  
  ld hl,1728
  call setspritenumber
  ret

.mariospritewalkright2: 

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,1920
  call setspritenumber
  ret

.mariospritewalkright3:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright3

  ld hl,2112
  call setspritenumber
  ret



.mariospriteslideleft:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  and a
  jp z,.mariospriteslideright 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell2

  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,2304
  call setspritenumber
  ret

.mariospriteslideright:

;we kijken na welke kant mario op kijkt na de DoCopy zodat de slide altijd klopt!
  ld a,(marioleftright)
  dec a
  jp z,.mariospriteslideleft 

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioturnshell)
  and a
  jp nz,.mariospriteturnshell1

  ld a,(mariosheerhold)
  and a
  ret nz

  ld hl,2496
  call setspritenumber
  ret


.mariodownleft:

  ld a,(mariolook)
  and a
  jp nz,.marioupleft

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkleft1

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell1

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownleft


;jumpkey vast  
  ld a,(marionotdown)
  ld b,a
  ld a,(mariojumpkey)
  and b
  jp z,.mariodownleft1


  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariodownleft1
  cp 2
  jp z,.mariodownleft2


.mariodownleft1:

  ld hl,2688
  call setspritenumber
  ret 
  
.mariodownleft2:

  ld hl,11328
  call setspritenumber
  ret   


.mariodownright:

  ld a,(mariolook)
  and a
  jp nz,.marioupright

  ld a,(marioduck)
  and a
  jp z,.mariospritewalkright1

  ld a,(standingonexit)
  dec a
  jp z,.mariospinjumpspritec


  ld a,(marioshell)
  dec a
  jp z,.mariospritetakeshell2

;sheerduck speciale sprite laten zien  
  ld a,(mariosheerduck)
  dec a
  jp z,.mariospritesheerdownright


;jumpkey vast  
  ld a,(marionotdown)
  ld b,a
  ld a,(mariojumpkey)
  and b
  jp z,.mariodownright1


  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariodownright1
  cp 2
  jp z,.mariodownright2


.mariodownright1:

  ld hl,2880
  call setspritenumber
  ret 
  
.mariodownright2:

  ld hl,11520
  call setspritenumber
  ret   

.mariospritesheerdownleft:

  ld hl,13824
  call setspritenumber
  ret  

.mariospritesheerdownright:

  ld hl,14016
  call setspritenumber
  ret    

  
.marioupleft:

  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell1

  ld hl,3072
  call setspritenumber
  ret 


.marioupright:


  ld a,(marioshell)
  dec a
  jp z,.mariospriteupshell2

  ld hl,3264
  call setspritenumber
  ret  
  

;Mario rent!

.mariorun:
 


  ld a,(marioleftrightspr)
  dec a
  jp z,.mariospriterunright
  
 
.mariospriterunleft:
  

 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownleft


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret
 
 
 
.mariospriterunleft1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft
  
  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft1
  

  ld hl,3456
  call setspritenumber
  ret
  
.mariospriterunleft2:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft2
  

  ld hl,3648
  call setspritenumber
  ret
  
.mariospriterunleft3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownleft

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellleft3
  

  ld hl,3840
  call setspritenumber
  ret

.mariospriterunright:  

 
 ;mario staat volledig stil dus doet de sprite niets meer 
  ld a,(mariospeed)
  and a
  jp z,.mariodownright


  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.mariospriterunright1
  
  cp 1
  jp z,.mariospriterunright2
  
  cp 2
  jp z,.mariospriterunright3
  ret


.mariospriterunright1:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright1
  
  ld hl,4032
  call setspritenumber
  ret

.mariospriterunright2: 

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,4224
  call setspritenumber
  ret
  
.mariospriterunright3:

  ld a,(marioduck)
  dec a
  jp z,.mariodownright

  ld a,(marioshell)
  dec a
  jp z,.mariospriteholdshellright2

  ld hl,4416
  call setspritenumber
  ret  


;/mario rent

;/sprite mario normal



;allemaal code voor het schelpje. naar beneden verplaatsen when done



.mariospritepickupshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritepickupshellleft
  cp 1
  jp z,.mariospritepickupshellright
  

  ret

.mariospritekickshell:

  ld a,(marioleftrightspr)
  cp 0
  jp z,.mariospritekickshellleft
  cp 1
  jp z,.mariospritekickshellright
  

  ret

.mariospritepickupshellleft:

  ld a,(marioflying)
  and a
  jp z,.mariospritepickupshellleft1 

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariospritepickupshellleft1
  cp 2
  jp z,.mariospritepickupshellleft2


.mariospritepickupshellleft1:

  ld hl,6144
  call setspritenumber
  ret

.mariospritepickupshellleft2:

  ld hl,10944
  call setspritenumber
  ret


.mariospritepickupshellright:

  ld a,(marioflying)
  and a
  jp z,.mariospritepickupshellright1 

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariospritepickupshellright1
  cp 2
  jp z,.mariospritepickupshellright2


.mariospritepickupshellright1:

  ld hl,6336
  call setspritenumber
  ret

.mariospritepickupshellright2:

  ld hl,11136
  call setspritenumber
  ret

.mariospritekickshellleft:

  ld hl,6912
  call setspritenumber
  ret


.mariospritekickshellright:

  ld hl,7104
  call setspritenumber
  ret




.mariospriteholdshellleft1: ;staat stil

  ld hl,4608
  call setspritenumber
  ret


.mariospriteholdshellleft2:

  ld a,(mariojumpkey)
  ld b,a
  ld a,(marioflying)
  ld c,a
  ld a,(marionotdown)
  or c
  and b
  jp z,.mariospriteholdshellleft21

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariospriteholdshellleft21
  cp 2
  jp z,.mariospriteholdshellleft22
 

.mariospriteholdshellleft21:

  ld hl,4800
  call setspritenumber
  ret
  
.mariospriteholdshellleft22:

  ld hl,11712
  call setspritenumber
  ret  
  
.mariospriteholdshellleft3: 

  ld hl,4992
  call setspritenumber
  ret  

;naar rechts :P  
.mariospriteholdshellright1: ;staat stil

  ld hl,5184
  call setspritenumber
  ret


.mariospriteholdshellright2: ;hier moeten knipperen komen


  ld a,(mariojumpkey)
  ld b,a
  ld a,(marioflying)
  ld c,a
  ld a,(marionotdown)
  or c
  and b
  jp z,.mariospriteholdshellright21

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.mariospriteholdshellright21
  cp 2
  jp z,.mariospriteholdshellright22


.mariospriteholdshellright21:

  ld hl,5376
  call setspritenumber
  ret  
  
.mariospriteholdshellright22:

  ld hl,11904
  call setspritenumber
  ret   
 
.mariospriteholdshellright3: 

  ld hl,5568
  call setspritenumber
  ret   
  
;gebranched vanuit de slide code je zal hier wat moeten aanpassen in de fysics.  
.mariospriteturnshell1:  

  ld hl,5760
  call setspritenumber
  ret  
  
.mariospriteturnshell2:  

  ld hl,5952
  call setspritenumber
  ret
 
;mario gebukt met schelpje OF pakt schelpje op  
.mariospritetakeshell1:  ;left

  ld hl,6144
  call setspritenumber
  ret   
  
.mariospritetakeshell2:  ;right

  ld hl,6336
  call setspritenumber
  ret     
  
.mariospriteupshell1: ;left

  ld hl,6528
  call setspritenumber
  ret


.mariospriteupshell2: ;right
 
  ld hl,6720
  call setspritenumber
  ret
  
;/code voor schelpje


;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a
  ret

;================================================mario vliegt horizontaal=========================================

;wip
mariofeatherspecial:

  ld a,(marioleftright)
  dec a
  jp z,.right


  ld a,(marioflydrop)
  dec a
  jp z,.dropdownleft


  ld a,(marioflyup) 
  cp 2
  jp z,.goupleft
  cp 1
  jp z,.goupleft

  ld a,(marioflyhold) 
  dec a
  jp z,.goupleft
  

  ld a,(fallspeed)
  cp 3
  jp nc,.falldownleft
  cp 0
  jp nc,.straightleft



.goupleft:

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.goupleft1
  cp 2
  jp z,.goupleft2


.goupleft1:

  ld hl,8448
  call setspritenumber
  
  ld hl,9984
  call setspritenumber2  
  ret

.goupleft2:

  ld hl,12096
  call setspritenumber
  
  ld hl,13248
  call setspritenumber2  
  ret  


.straightleft:


  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.straightleft1
  cp 2
  jp z,.straightleft2


.straightleft1:

  ld hl,8832
  call setspritenumber
  
  ld hl,10176
  call setspritenumber2  
  ret
  
.straightleft2:

  ld hl,12480
  call setspritenumber
  
  ld hl,13440
  call setspritenumber2  
  ret  


.falldownleft:

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.falldownleft1
  cp 2
  jp z,.falldownleft2


.falldownleft1:

  ld hl,9216
  call setspritenumber

  ld hl,10368
  call setspritenumber2  
  ret
  
.falldownleft2:

  ld hl,12864
  call setspritenumber

  ld hl,13632
  call setspritenumber2  
  ret  


.dropdownleft:


  ld hl,9600
  call setspritenumber
  ret
    



.right:

  ld a,(marioflydrop)
  dec a
  jp z,.dropdownright

  ld a,(marioflyup)
  cp 2
  jp z,.goupright
  cp 1
  jp z,.goupright

;ten alle tijden deze sprite aanhouden in de jump  
  ld a,(marioflyhold)
  dec a
  jp z,.goupright
  

  ld a,(fallspeed)
  cp 3
  jp nc,.falldownright
  cp 0
  jp nc,.straightright



.goupright:

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.goupright1
  cp 2
  jp z,.goupright2


.goupright1:

  ld hl,8640
  call setspritenumber
  
  ld hl,10080
  call setspritenumber2 
  ret
  
.goupright2:

  ld hl,12288
  call setspritenumber
  
  ld hl,13344
  call setspritenumber2 
  ret  


.straightright:

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.straightright1
  cp 2
  jp z,.straightright2


.straightright1:

  ld hl,9024
  call setspritenumber
  
  ld hl,10272
  call setspritenumber2 
  ret

.straightright2:

  ld hl,12672
  call setspritenumber
  
  ld hl,13536
  call setspritenumber2 
  ret


.falldownright:

  ld a,(marioflickrtable)
  inc a
  cp 3
  call z,mariofeather.resetmarioflickrtable
  ld (marioflickrtable),a
  
  cp 1
  jp z,.falldownright1
  cp 2
  jp z,.falldownright2


.falldownright1:

  ld hl,9408
  call setspritenumber
  
  ld hl,10464
  call setspritenumber2   
  ret

.falldownright2:

  ld hl,13056
  call setspritenumber
  
  ld hl,13728
  call setspritenumber2   
  ret


.dropdownright:


  ld hl,9792
  call setspritenumber  
  ret




;==================================================mario zwemt====================================================
marioswimming:


;TODO: branchen naar grote mario etc etc
;uit welke block halen we deze sprite??



  ld a,(mariostate)
  cp 0
  jp z,marioswimmingnormal
  cp 1
  jp z,marioswimminglarge
  cp 2
  jp z,marioswimmingfire
  cp 3
  jp z,marioswimmingflying


marioswimmingnormal:

  ld a,$0d
  ld (blocknumber),a

;TODO: deze routine is nog heel slordig mario doet ook niks wanneer hij met schelpje zwemt
;en dat wint absoluut niet de schoonheidsprijs. 
 ld a,(marioshell)
 dec a
 jp z,.marioswimshell
 
  
  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimup


    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimright
    
.marioswimleft:
  
    
    ld hl,6144
    call setspritenumber
   ret 
    
.marioswimright:

  
    ld hl,6336
    call setspritenumber
    
   ret 


.marioswimup:


    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimupright
    
.marioswimupleft:
    
    
    ld hl,6528
    call setspritenumber
   ret 
    
.marioswimupright:

  
    ld hl,6720
    call setspritenumber
    
   ret 

.marioswimshell:
  
  ld a,$19
  ld (blocknumber),a

  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimdown

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshellright

.marioswimshellleft:
    
   ld hl,2304
   call setspritenumber
   ret   

.marioswimshellright:
    
   ld hl,2496
   call setspritenumber
   ret   


.marioswimdown:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshelldownright
 
.marioswimshelldownleft:
    
   ld hl,2304
   call setspritenumber
   ret  
    
.marioswimshelldownright:

   ld hl,2496
   call setspritenumber
   ret 

marioswimminglarge:

  ld a,$19
  ld (blocknumber),a

;TODO: deze routine is nog heel slordig mario doet ook niks wanneer hij met schelpje zwemt
;en dat wint absoluut niet de schoonheidsprijs. 
 ld a,(marioshell)
 dec a
 jp z,.marioswimshell
 
  
  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimup


    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimright
    
.marioswimleft:
    
    ld hl,1536
    call setspritenumber
   ret 
    
.marioswimright:

  
    ld hl,2112
    call setspritenumber
    
   ret 


.marioswimup:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimupright
    
.marioswimupleft:
    
    ld hl,1152
    call setspritenumber
   ret 
    
.marioswimupright:

  
    ld hl,1728
    call setspritenumber
    
   ret 

.marioswimshell:

  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimdown

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshellright

.marioswimshellleft:
    
   ld hl,2688
   call setspritenumber
   ret   

.marioswimshellright:
    
   ld hl,2880
   call setspritenumber
   ret   


.marioswimdown:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshelldownright
 
.marioswimshelldownleft:
    
   ld hl,2688
   call setspritenumber
   ret  
    
.marioswimshelldownright:

   ld hl,2880
   call setspritenumber
   ret

marioswimmingfire:

  ld a,$19
  ld (blocknumber),a

;TODO: deze routine is nog heel slordig mario doet ook niks wanneer hij met schelpje zwemt
;en dat wint absoluut niet de schoonheidsprijs. 
 ld a,(marioshell)
 dec a
 jp z,.marioswimshell
 
  
  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimup


    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimright
    
.marioswimleft:
    
    ld hl,3456
    call setspritenumber
   ret 
    
.marioswimright:

  
    ld hl,4032
    call setspritenumber
    
   ret 


.marioswimup:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimupright
    
.marioswimupleft:
    
    ld hl,3072
    call setspritenumber
   ret 
    
.marioswimupright:

  
    ld hl,3648
    call setspritenumber
    
   ret 

.marioswimshell:

  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimdown

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshellright

.marioswimshellleft:
    
   ld hl,4224
   call setspritenumber
   ret   

.marioswimshellright:
    
   ld hl,4416
   call setspritenumber
   ret   


.marioswimdown:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshelldownright
 
.marioswimshelldownleft:
    
   ld hl,4224
   call setspritenumber
   ret  
    
.marioswimshelldownright:

   ld hl,4416
   call setspritenumber
   ret
   
marioswimmingflying:


  ld a,$19
  ld (blocknumber),a

;TODO: deze routine is nog heel slordig mario doet ook niks wanneer hij met schelpje zwemt
;en dat wint absoluut niet de schoonheidsprijs. 
 ld a,(marioshell)
 dec a
 jp z,.marioswimshell
 
  
  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimup


    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimright
    
.marioswimleft:
    
    ld hl,4992
    call setspritenumber
   ret 
    
.marioswimright:

  
    ld hl,5568
    call setspritenumber
    
   ret 


.marioswimup:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimupright
    
.marioswimupleft:
    
    ld hl,4608
    call setspritenumber
   ret 
    
.marioswimupright:

  
    ld hl,5184
    call setspritenumber
    
   ret 

.marioswimshell:

  ld a,(jumpspeed)
  or 0
  jp nz,.marioswimdown

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshellright

.marioswimshellleft:
    
   ld hl,5760
   call setspritenumber
   ret   

.marioswimshellright:
    
   ld hl,5952
   call setspritenumber
   ret   


.marioswimdown:

    ld a,(marioleftrightsprw)
    dec a
    jp z,.marioswimshelldownright
 
.marioswimshelldownleft:
    
   ld hl,5760
   call setspritenumber
   ret  
    
.marioswimshelldownright:

   ld hl,5952
   call setspritenumber
   ret
 


  ret

;===========================================[mario klimt]=============================================
mariosprclimbing:



  ld a,(mariostate)
  cp 0
  jp z,mariosprcclimbingnormal
  cp 1
  jp z,mariosprcclimbinglarge
  cp 2
  jp z,mariosprcclimbingfire
  cp 3
  jp z,mariosprcclimbingflying


mariosprcclimbingnormal:

  ld a,$0d
  ld (blocknumber),a


;mario staat volledig stil dus doet de sprite niets meer in klimmen zijn de regels ietsje anders want
;de mariospeed is altijd geset we moeten dus de Controls uitlezen kijken of er beweging is
  ld a,(Controls)
  and %00001111 ;alleen de laatste 4 checken want dat zijn de pijltjes
  or %00000000 ;toets ingedrukt?  
  ret z
 ; jp z,.mariospriteclimb1


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospriteclimb2

;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
  ld a,(mariospeed)
  cp 2
  jp c,.mariowalkfootclimbslow 
 
 
 
.mariowalkfootclimbfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret


.mariowalkfootclimbslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret
 
 
.mariospriteclimb1:
  

  ld hl,15744
  call setspritenumber
  ret
  
.mariospriteclimb2: ;staat stil


  ld hl,15936
  call setspritenumber
  ret


.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a


  ret

mariosprcclimbinglarge:

  ld a,$19
  ld (blocknumber),a


;mario staat volledig stil dus doet de sprite niets meer in klimmen zijn de regels ietsje anders want
;de mariospeed is altijd geset we moeten dus de Controls uitlezen kijken of er beweging is
  ld a,(Controls)
  and %00001111 ;alleen de laatste 4 checken want dat zijn de pijltjes
  or %00000000 ;toets ingedrukt?  
  ;jp z,.mariospriteclimb1
  ret z


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospriteclimb2

;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
  ld a,(mariospeed)
  cp 2
  jp c,.mariowalkfootclimbslow 
 
 
 
.mariowalkfootclimbfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret


.mariowalkfootclimbslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret
 
 
.mariospriteclimb1:
  

  ld hl,0
  call setspritenumber
  ret
  
.mariospriteclimb2: ;staat stil


  ld hl,192
  call setspritenumber
  ret


.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a


  ret


mariosprcclimbingfire:

  ld a,$19
  ld (blocknumber),a


;mario staat volledig stil dus doet de sprite niets meer in klimmen zijn de regels ietsje anders want
;de mariospeed is altijd geset we moeten dus de Controls uitlezen kijken of er beweging is
  ld a,(Controls)
  and %00001111 ;alleen de laatste 4 checken want dat zijn de pijltjes
  or %00000000 ;toets ingedrukt?  
  ;jp z,.mariospriteclimb1
  ret z


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospriteclimb2

;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
  ld a,(mariospeed)
  cp 2
  jp c,.mariowalkfootclimbslow 
 
 
 
.mariowalkfootclimbfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret


.mariowalkfootclimbslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret
 
 
.mariospriteclimb1:
  

  ld hl,384
  call setspritenumber
  ret
  
.mariospriteclimb2: ;staat stil


  ld hl,576
  call setspritenumber
  ret


.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a


  ret

mariosprcclimbingflying:

  ld a,$19
  ld (blocknumber),a


;mario staat volledig stil dus doet de sprite niets meer in klimmen zijn de regels ietsje anders want
;de mariospeed is altijd geset we moeten dus de Controls uitlezen kijken of er beweging is
  ld a,(Controls)
  and %00001111 ;alleen de laatste 4 checken want dat zijn de pijltjes
  or %00000000 ;toets ingedrukt?  
  ;jp z,.mariospriteclimb1
  ret z


;wanneer mario van een schuine tile af slide moeten we geen animatie tonen
  ld a,(mariosheerslide)
  dec a
  jp z,.mariospriteclimb2

;we kijken wat de snelheid is om de voetjes wat leuker te laten bewegen op snelheid 
 
  ld a,(mariospeed)
  cp 2
  jp c,.mariowalkfootclimbslow 
 
 
 
.mariowalkfootclimbfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 4
  call nc,.resetmariowalktable
  
  cp 2
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret


.mariowalkfootclimbslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospriteclimb1
  
  jp .mariospriteclimb2
  ret
 
 
.mariospriteclimb1:
  

  ld hl,768
  call setspritenumber
  ret
  
.mariospriteclimb2: ;staat stil


  ld hl,960
  call setspritenumber
  ret


.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a


  ret

mariodead:


;  ld a,$19
;  ld (blocknumber),a


.mariowalkfootleftslow:

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 6
  call nc,.resetmariowalktable
  
  cp 3
  jp c,.mariospritewalkleft1
  
  jp .mariospritewalkleft2
  ret
 
 
.mariospritewalkleft1:
  


  ld hl,6144
  call setspritenumber
  ret
  
.mariospritewalkleft2:
  

  ld hl,6336
  call setspritenumber

  ret


.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a


  ret

;===============================[mario zit op yoshi]====================================


runturnyoshitimer:

;als je de yoshi loslaat terwijl de timer loopt is het big trouble! dus meteen de timer resetten
  ld a,(marioyoshi)
  and a
  jp z,.stoptimer


  ld a,(marioturnyoshi)
  inc a
  cp 6
  jp nc,.stoptimer
  ld (marioturnyoshi),a
    ret
    
.stoptimer:
 
     xor a
     ld (marioturnyoshi),a
  ;   ld (turnyoshiactive),a
     ret 





marioyoshisprite:

  ld a,(nocontrol)
  dec a
  jp z,mariosprite.ignoreyoshi
  


  ld a,$26
  ld (blocknumber),a


  ld a,(mariostate)
  and a
  jp z,marioyoshismall
  dec a
  jp z,marioyoshilarge
  dec a
  jp z,marioyoshifire
  dec a
  jp z,marioyoshifly

  ret



marioyoshismall:


  ld a,(mariopeace)
  and a
  jp nz,.mariopeace

  
  ld a,(marioleftrightspr)
  and a
  jp nz,.right
  
.left:  
 
  ld a,(marioturnyoshi)
  and a
  jp nz,.turnleft
  
 
  ld a,(marioshot)
  and a
  jp nz,.shootingleft
 
  ld hl,12288
  call setspritenumber

  ret


.mariopeace:

  ld hl,14976
  call setspritenumber

  ret
  

.right:

  ld a,(marioturnyoshi)
  and a
  jp nz,.turnright


  ld a,(marioshot)
  and a
  jp nz,.shootingright

  ld hl,12480
  call setspritenumber

  ret

.shootingleft:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingleft2

  ld hl,12672
  call setspritenumber

  ret

.shootingleft2:

  ld hl,12864
  call setspritenumber

  ret
  


.shootingright:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingright2
  
  
  ld hl,13056
  call setspritenumber

  ret

.shootingright2:

  ld hl,13248
  call setspritenumber

  ret


.turnright:

  ld hl,13440
  call setspritenumber

  ret


.turnleft:

  ld hl,13632
  call setspritenumber

  ret



marioyoshilarge:

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace


  ld a,(marioleftrightspr)
  and a
  jp nz,.right
  
.left:  
 
  ld a,(marioturnyoshi)
  and a
  jp nz,.turnleft
  
 
  ld a,(marioshot)
  and a
  jp nz,.shootingleft
 
  ld hl,7680
  call setspritenumber

  ret

.mariopeace:

  ld hl,15168
  call setspritenumber

  ret
  

.right:

  ld a,(marioturnyoshi)
  and a
  jp nz,.turnright


  ld a,(marioshot)
  and a
  jp nz,.shootingright

  ld hl,7872
  call setspritenumber

  ret

.shootingleft:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingleft2

  ld hl,8064
  call setspritenumber

  ret

.shootingleft2:

  ld hl,8256
  call setspritenumber

  ret
  


.shootingright:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingright2
  
  
  ld hl,8448
  call setspritenumber

  ret

.shootingright2:

  ld hl,8640
  call setspritenumber

  ret


.turnright:

  ld hl,8832
  call setspritenumber

  ret


.turnleft:

  ld hl,9024
  call setspritenumber

  ret


marioyoshifire:


  ld a,(mariopeace)
  and a
  jp nz,.mariopeace


  ld a,(marioleftrightspr)
  and a
  jp nz,.right
  
.left:  
 
  ld a,(marioturnyoshi)
  and a
  jp nz,.turnleft
  
 
  ld a,(marioshot)
  and a
  jp nz,.shootingleft
 
  ld hl,9216
  call setspritenumber

  ret

.mariopeace:

  ld hl,15360
  call setspritenumber

  ret


.right:

  ld a,(marioturnyoshi)
  and a
  jp nz,.turnright


  ld a,(marioshot)
  and a
  jp nz,.shootingright

  ld hl,9408
  call setspritenumber

  ret

.shootingleft:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingleft2

  ld hl,9600
  call setspritenumber

  ret

.shootingleft2:

  ld hl,9792
  call setspritenumber

  ret
  


.shootingright:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingright2
  
  
  ld hl,9984
  call setspritenumber

  ret

.shootingright2:

  ld hl,10176
  call setspritenumber

  ret


.turnright:

  ld hl,10368
  call setspritenumber

  ret


.turnleft:

  ld hl,10560
  call setspritenumber

  ret



marioyoshifly:

  ld a,(mariopeace)
  and a
  jp nz,.mariopeace


  ld a,(marioleftrightspr)
  and a
  jp nz,.right
  
.left:  
 
  ld a,(marioturnyoshi)
  and a
  jp nz,.turnleft
  
 
  ld a,(marioshot)
  and a
  jp nz,.shootingleft
 
  ld hl,10752
  call setspritenumber

  ret

.mariopeace:

  ld hl,15552
  call setspritenumber

  ret

.right:

  ld a,(marioturnyoshi)
  and a
  jp nz,.turnright


  ld a,(marioshot)
  and a
  jp nz,.shootingright

  ld hl,10944
  call setspritenumber

  ret

.shootingleft:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingleft2

  ld hl,11136
  call setspritenumber

  ret

.shootingleft2:

  ld hl,11328
  call setspritenumber

  ret
  


.shootingright:
  
  ld a,(marioshot)
  cp 5
  jp nc,.shootingright2
  
  
  ld hl,11520
  call setspritenumber

  ret

.shootingright2:

  ld hl,11712
  call setspritenumber

  ret


.turnright:

  ld hl,11904
  call setspritenumber

  ret


.turnleft:

  ld hl,12096
  call setspritenumber

  ret






;losse call maken dat spaart ruimte en gezeik
setspritenumber:

  ld (mariospritenumber),hl
  ret  

;restsprite aanzetten indien nodig
setspritenumber2:

  ld (mariospritenumber2),hl
  ret
  

;==================================[einde mario sprite]=================================
  


