;Rominit V1.2c written by Daemos and Norakomi for evil use with cartridges.
;One time code to find all the slots in the MSX and assign them correctly
;Courtesy to those who have helped in allowing us to understand the MSX standard.

;this romloader kicks the bios out and frees the entire page0 as RAM
;Optionally the BIOS can be left intact by commenting the call killbios line and
;move the variables in page3 as reserved bytes.
;Include this entire file in your loader code and copy the Rominitdata
;contents to page3 ($c000) and execute by calling it.

;Optionally the code can save the subslot values to ram as well.
;Define the page1rom page2rom etc as words in that case.

;bytes that need to be defined in page3
;slot:
;.ram:   rb 1
;.page1rom: rb 1
;.page2rom: rb 1
;.page12rom: rb 1
;.bios rb 1

;This romloader writes a tempISR to $38 in page0 of RAM and enables interupts on exit
;No need to manually load any ISR on boot.

;IN = none
;OUT = HL = in H = primary slot register value of BIOS L = Secondary slot register value of BIOS

;Changes all registers disables BIOS permanently by default

Rominitdata: ;use double lable or else this will not get copied correctly to RAM!
;Start if rominit
phase $c000
rominit:

;flowchart of rominit:
;1.) find location of ROM V
;2.) enable rom in page2 V
;3.) search for RAM through main and expanded slots V
;4.) multiplex values to slot.page1rom etc. V
;5.) kill the bios and enable ram in page0 with prewritten inthandler. V


;Taken from MSX2 handbook
findrom:

	CALL	RSLREG		;read primary slot #
	RRCA			;move it to bit 0,1 of [Acc]
	RRCA
	AND	%00000011
	LD	C,A
	LD	B,0
	LD	HL,EXPTBL	;see if this slot is expanded or not
	ADD	HL,BC
	LD	C,A		;save primary slot #
	LD	A,(HL)		;See if the slot is expanded or not
	AND	$80
	OR	C		;set MSB if so
	LD	C,A		;save it to [C]
	INC	HL		;Point to SLTTBL entry
	INC	HL
	INC	HL
	INC	HL
	LD	A,(HL)		;Get what is currently output
				;to expansion slot register
	AND	%00001100
	OR	C		;Finally form slot address
	LD	H,$80
	ld (romslot),a ;save obtained slot adress
	call	ENASLT		;enable page 2

  ;get msx type from BIOS: if turbo-R do not perform RAM search but simply hardset the address by hand
	ld		a,($2d)		;ok (suppress memory access warning)
	cp		3
	jp		z,.setramturbor

.noturbor:	
	
;Now that we have page2 at our disposal and our location it is time to find RAM  
  ld hl,$0000
  ld (searchaddr),hl
  call searchram
  ld (page0ram),a
  ld hl,$4000
  ld (searchaddr),hl
  call searchram
  ld (page1ram),a
  ld hl,$8000
  ld (searchaddr),hl
  call searchram
  ld (page2ram),a
;All usable pages of RAM found

.turbor:

;slot.ram part: write all main slot values of RAM except for the one in page 3
	ld a,(page2ram)
	and %00000011
  rlca
  rlca
  rlca
  rlca
	ld c,a
	ld a,(page1ram)
	and %00000011
  rlca
  rlca
	ld b,a
	ld a,(page0ram)
	and %00000011
	ld e,a
	push bc
	push de
	call RSLREG
	pop de
	pop bc
	and %11000000
	or b
	or c
	or e
	ld (slot.ram),a
;/slot.ram part


;put page1 in ROM.
  and %11110011 ;prep page 1 for data
  ld c,a
  ld a,(romslot)
  and %00000011
  rlca
  rlca
  push af ;store this value
  or c
  ld (slot.page1rom),a
;/put page1 in rom
	
;trow page2 rom in	

  ld a,(slot.ram)
  and %11001111
  ld c,a
  ld a,(romslot)
  and %00000011 ;get primary value
  rlca
  rlca
  rlca
  rlca
  push af ;store this value
  or c
  ld (slot.page2rom),a
;/trow page2 rom in

;page12 rom
  ld a,(slot.ram)
  and %11000011
  ld c,a
  pop af
  or c
  ld c,a
  pop af
  or c
  ld (slot.page12rom),a
;/page12rom
 


;force subslot regs into correct position
	ld		a,(page1ram)
	ld		h,%01000000
	call	ENASLT
	
	ld		a,(romslot)
	ld		h,%01000000
	call	ENASLT
	
	
  ld		a,(page2ram)
	ld		h,%10000000
	call	ENASLT
	
	ld		a,(romslot)
	ld		h,%10000000
	call	ENASLT 	
;/force subslot regs into correct position

;fetch all current subslot values

;OPTIONAL: Fully uncomment this part to save the subslot values to RAM
;for ram. Store subslot values for page 1 and 2 and page12
;  ld a,(page1ram) ;get ram
;  and %00001100
;  ld c,a
;  ld a,(romslot) ;get romslot
;  and %00001100
;  rlca
;  rlca
;  or c
;  ld (slot.page2rom+1),a
;  
;  ld a,(page2ram)
;  and %00001100
;  rlca
;  rlca
;  ld c,a
;  ld a,(romslot)
;  and %00001100
;  or c
;  ld (slot.page1rom+1),a
;  
;  ld a,(romslot)
;  and %00001100
;  ld c,a
;  rlca
;  rlca
;  or c
;  ld (slot.page12rom+1),a
;
;/fetch all current subslot values

;write the tempisr into page0 first before disabling BIOS

  ld hl,$38
  ld b,6  
  ld de,tempisr
  
.looptempisrwr:  
  push hl
  push de
  push bc
  ld a,(de)
  ld e,a
  ld a,(page0ram)
  call WRSLT
  pop bc
  pop de
  pop hl
  inc de
  inc hl
  djnz .looptempisrwr	



  call killbios


  ret


.setramturbor:
;on turboR the main RAM is always in slot 3-0 thus we force this value for all RAM
;Using the main RAM has benefits on TurboR since it is much faster
;Test if this region is actually writable. We assume that there is RAM over there but offcourse the future may release machines
;that act like a turbor but have a different slot layout.
  ld a,%10000011
  ld (searchpoint),a
  ld hl,0
  ld (searchaddr),hl ;we only check page0 If someone splits the fast ram on turbo emulation that machine is not worthy of this game.

  call fetchpoint3
  ;read value first for saving later
  call RDSLT
  ld (tempbyte),a
  xor %11111111 ;read the value and write it reversed.
  ld e,a
  push de
  call fetchpoint3
  call WRSLT
  call fetchpoint3
  call RDSLT
  pop de
  cp e
  jp nz,.noturbor

    ;fix ram? there will be 11111111 at adress 0 not sure if that is bad
  ld a,(tempbyte)
  ld e,a
  call fetchpoint
  call WRSLT ;fix what was broken  
  

  ld a,%10000011
  ld (page0ram),a
  ld (page1ram),a
  ld (page2ram),a
  
  jp .turbor


fetchpoint3:

  ld a,(searchpoint)
  ld hl,(searchaddr) 
  
  ret
 

  

;search all slots for RAM
searchram:

    xor a
    ld (slotcount),a ;reset slot counter

  ld a,(romslot) ;new start at romslot + 1 like this you are never able to find RAM on the same mainslot as rom
  ;the extra gain is that on the turbo-R any expansion RAM is most likely to be skipped unless it is placed in
  ;slot2, since we know this for a fact we could force the memsearch on main slot 3 if a turboR is to be found.
  inc a
  and %00000011 ;remove any other data
  
  ;this prevents usage of memory expansion cartridges which can hang the system.
  ld (searchpoint),a ;start
  
.searchloop: 
 
  ld a,(romslot) ;never look for ram in the same slot as ourself
  and %00000011 ;remove any other data 
  ld c,a
  ld a,(searchpoint) ;this is where we are @
  cp c
  jp z,.noram
 
 
  ld b,0
  ld c,a
  ld hl,EXPTBL
  add hl,bc
  ld a,(hl) ;read if expanded slot or not
  cp $80
  jp z,.expandedslot

  ;continue here if mainslot
  call fetchpoint
  ;read value first for saving later
  call RDSLT
  ld (tempbyte),a
  xor %11111111 ;read the value and write it reversed.
  ld e,a
  push de
  call fetchpoint
  call WRSLT
  call fetchpoint
  call RDSLT
  pop de
  cp e
  jp nz,.noram

  ld a,(tempbyte)
  ld e,a
  call fetchpoint
  call WRSLT ;fix what was broken


  ;We found ram! return
  ld a,(searchpoint)
  and %00000011


  ret


.exitexpandedloop:

  ld a,(searchpoint)
  and %00000011
  inc a
  ld (searchpoint),a

;  cp 3;4
;  ret z ;end of loop ;V1.2a
;NOTE: this loader expects at least 64Kb of RAM in the MSX system. With less RAM available 
;the loader will eternally loop through the slots unable to find more RAM.



  jp .searchloop



;found mainslot treat it that way
.expandedslot:

  call fetchpoint2
  ;read value first for saving later
  call RDSLT
  ld (tempbyte),a
  xor %11111111 ;read the value and write it reversed.
  ld e,a
  push de
  call fetchpoint2
  call WRSLT
  call fetchpoint2
  call RDSLT
  pop de
  cp e
  jp nz,.noexpram

  ld a,(tempbyte)
  ld e,a
  call fetchpoint2
  call WRSLT ;fix what was broken


  ;expanded ram found
  ld a,(searchpoint)
  set 7,a
  
  



  ret


.noexpram:


  ld a,(searchpoint)
  add 4
  ld (searchpoint),a ;next subslot
 
  ld a,(searchpoint)
  and %01111100
  cp 16
  jp z,.exitexpandedloop ;done going through the expanded slot 
 
  
  jp .expandedslot


.noram:

  ld a,(slotcount)
  inc a
  cp 8 ;we have searched enough for RAM!!
  jp nc,trowerror ;fuck out from here
  ld (slotcount),a    


  ld a,(searchpoint)
  inc a
  and %00000011
  ld (searchpoint),a ;next slot
    
  jp .searchloop


  
fetchpoint:

  ld a,(searchpoint)
  and %00000011 ;only mainslot value is important
  ld hl,(searchaddr) 
  
  ret

fetchpoint2:

  ld a,(searchpoint)
  set 7,a ;all values are now important also set this bit since we are searching through expanded slots now
  ld hl,(searchaddr) 
  
  ret

  
trowerror:

  ld a,1
  call CHSCR ;change to screen 1

  
  
  xor a
  call cls
  
  ld hl,tekst
  
.looptekst:
  ld a,(hl)
  and a
  jp z,.done
  call chput
  
  inc hl
  jp .looptekst
  
.done:

    jp .done

tekst: db "ERROR: You require at least 64kb of RAM outside of the games primary slot to play this game",0  
  
  

;define temp variables
searchpoint:  db 0 ;current location of search trough slots format = %E000SSPP
searchaddr:   dw 0
tempbyte:     db 0
slotcount:    db 0 ;count slot search

;these variables must be moved to variables page3
;if bios is preserved
romslot:      db 0 ;location of the cartridge ROM
page0ram:     db 0 ;location of RAM in page 0
page1ram:     db 0 ;location of RAM in page 1
page2ram:     db 0 ;location of RAM in page 2
;/these variables need to be moved to page3


;chput:          equ $A2 ;put some text on screen
CHSCR:          equ $5F ;change screen mode
cls: equ $00c3
WRSLT:        EQU $14
RDSLT:        EQU $C
ENASLT:     	EQU	$24		;enable slot
RSLREG:	      EQU	$138		;read primary slot select register
EXPTBL:     	EQU	$FCC1  ;slot is expanded or not

lenghtrominit: equ $ - rominit

dephase

rominitend:





;Kill the bios in page0 from page1rom. Write the main and expanded slot values before entering
killbios:

  ld a,(page0ram)
  ld b,a
  and %00001100
  rrca
  rrca
  ld c,a ;store expanded slot value for page0 in CPU register
  ld a,b
  and %00000011
  ld d,a ;store primary slot data for page0ram
  rrca
  rrca
  ld b,a ;store primary slot value for page0 in CPU register
  in a,($A8) ;get current primary slot
  and %11000000
  ld e,a ;store page 3 primary slot value

;lets do it  
  di
  in a,($A8)
  ld (slot.bios),a ;store for later
;  ld h,a
  and %00111111 ;page3
  or b
  out ($A8),a ;page 0 is set now

  ld a,($FFFF)
  cpl
;  ld l,a
  and %11111100
  or c
  ld ($FFFF),a ;set correct subslot
  
  in a,($A8)
  and %00111100
  or e
  or d
  out ($A8),a ;restore page 3 stack is available now
   
  ei

;NOTE: BIOS slotdata. Primary and secondary slot are stored in HL, H contains the primary slotregister value of $A8 and L contains the FFFF register value for page0
;these values can be used to get the BIOS back if required (Optionally)
  
  ret


tempisr:	
	push	af
	in		a,($99)             ;check and acknowledge vblank int (ei0 is set)
	pop		af
	ei	
	ret



