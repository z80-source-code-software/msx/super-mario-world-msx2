;Engine3 Is copied by the loader beyond the variables. This small part in RAM is about 1Kb in size only the code is included here
;it is difficult to see how much of the RAM gets used up so we try to avoid using this part at all cost.

;in a tilenumber to write in hl bkgaddress to write to in b amount of tiles in sequence



spawnwaterswim:

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
;  inc hl
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
;  inc e

;create the tubecover

  push ix

  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),153              ;set object number (water)
  ld    (iy+6),0              ;set object number (water)

  ld a,(currentlevel)
  cp 53
  call z,.spawnspecial
  
  call  activateobject

  ld (ix+v4),0
  ld (ix+objectfp),0
 

  pop ix

  ret


.spawnspecial:

  ld    (iy+5),104              ;set object number (water)
  ret
  
  
readanytile:

  push af
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a
  pop af
  
  ld a,(hl)

  jp setpage12tohandleobjectmovement



writeanytile:

  push af
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a
  pop af
  
  ld (hl),a

  jp setpage12tohandleobjectmovement




transformintostar:

;create the object at mario that kills the other objects 
 
  push ix
    
  call  activateobject

  ld (ix+deathtimer),0

  pop ix
    
  ret


copyspritetoram:; equ $F664+512

;  push ix
;  push hl
;  pop ix

  ld    a,(iy+spritesblock)  
  call block1234 ;set blocks

  ld    l,(iy+spritecharROM+0)  ;character address
  ld    h,(iy+spritecharROM+1)

  add hl,bc

;  ld a,b
;  and a
;  jp z,.skip

;  ld de,32
;
;    .multiply:
;  
;    add hl,de
;    
;    djnz .multiply
;more optimal method. Provide the number in BC directly through compiler. 

;.skip:

  ld de,tempspritedata
  ld bc,64
  ldir
  
;  pop ix
  
  jp setpage12tohandleobjectmovement



include "playsfx.asm"



;debug routine
hang:
  jp hang

;stops the engine but continues upon a keypress
break:

  ld b,0

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 1,a
	jp z,.waitforrelease
	
	jp break

;keypress release
.waitforrelease:

  ld b,0

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 1,a
	ret nz
	
	jp .waitforrelease

;;/debug routines
  
setcarry:
  
  scf

  ret
  
  
;check if there is no sheertile returns z if true
nosheercheck:


  ;TODO: simple and easy and works.
  ld a,(nosheer)
  ;dec a
  cp 1
  ;ret z
  
  ret




addsecond:

  ld a,(time)
  cp 25
  push af
;  call z,Playsfx.secretdoor
  call z,switchmusictimeup
  pop af
  cp 22
  push af
  call z,restoremusic
  pop af
  and a
  ret z
  dec a
  jp z,.killmario ;only if the counter hits zero  
  ld (time),a

  xor a
  ld (timecounter),a
  

  ret


.killmario:

  ld a,(nocontrol)
  ;no kill upon win
  ;cp 3
  and a
  jp nz,.resettime

  ld a,(mariotransforming)
  cp 1
  ret z
  cp 10
  ret z
  
  xor a
  ld (mariotransformp),a

  ld a,10
  ld (mariotransforming),a
  
  ret

  
.resettime:


    ld a,20
    ld (time),a
    ret
  
  
manualwait:


  ld a,(turbor?)
  cp 2
  jp z,.turboz80
  cp 3
  ret z ;no compensation required frequency is exactly the double
;  jp z,.turboz802

;insert additional wait states for turbo r

  ld b,37 ;45
.manualwait:
  nop
  djnz .manualwait

  ret

.turboz80:

  ld b,12;5
.manualwait2:
  nop
  djnz .manualwait2

  ret



demointhandler:

 
  push  af


  ld    a,1               ;set s#1
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge line interrupt
  rrca
  jp    c,.Lineint        ;lineint detected, so jp to that routine

  xor   a                 ;set s#0
  out   ($99),a
  ld    a,15+128
  out   ($99),a
  in    a,($99)           ;check and acknowledge vblank interrupt
  rlca
  jp    c,.vblank          ;vblank detected, so jp to that routine


  pop   af                ;at this point neither lineint NOR vblank was
  ei
  ret
  
  
.Lineint: 

  push  bc
  push  de
  push  hl
  exx
  ex    af,af'
  push  af
  push  bc
  push  de
  push  hl
  push  ix


;keep pallete good
;keep pallete good
    ld                hl,currentpalette
        xor                a
        out                ($99),a
        ld                a,16+128
        out                ($99),a

  ld    a,2               ;Set Status register #2
  out   ($99),a
  ld    a,15+128          ;we are about to check for HR
  out   ($99),a

  ld    b,%0010'0000      ;bit to check for HBlank detection
  ld    c,$9a             ;outport to write palette data
  ld    b,(hl)            ;palettecolor 0 red + blue
  inc   hl
  ld    a,(hl)            ;palettecolor 0 green
  inc   hl 
  out   (c),b   ;11 + 2  ED xx
  out   (c),a   ;11 + 2  ED xx


  
  in    a,($a8)
  push  af                  ;store current RAM/ROM page settings

  ;handle music
      ld                a,(slot.page12rom)        ;all RAM except page 1+2
        out                ($a8),a         

  ld    a,(currentmusicblock);set music replayer in page 2, block3+4, at $8000
  ld                ($9000),a
  inc                a
  ld                ($b000),a
  xor a
  ld                ($5000),a
  inc                a
  ld                ($7000),a

  call DRVOPLL             ;every VBlank (or any other fixed interval) to play music.


  ;handle sfx
      ld                a,SeeSfxReplayerblock   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
        ld                ($5000),a
        inc                a
        ld                ($7000),a
        ld    a,Sfxblock
  ld    ($9000),a
     inc                a
        ld                ($b000),a
.sfx:
  call  MainSfxIntRoutine       ;keep running sfx every int
  call  SetSfx                                     ;if sfx is on, then set to play

   ;reset blocks
      ld                a,(memblocks.n1)     ;reset blocks
    ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
        ld                a,(memblocks.n4)
        ld                ($b000),a
        
        

  pop   af                  ;recall RAM/ROM page setting
 out                ($a8),a        

  ld a,(lineintflag)
  inc a
  ld (lineintflag),a

  pop   ix
  pop   hl
  pop   de
  pop   bc
  pop   af
  ex    af,af'
  exx
  pop   hl
  pop   de
  pop   bc


  pop   af 



  ei
  ret

.vblank: 

;scc replayer this is the most reliable interval
  in a,($a8)
  push af

  ld a,(slot.page12rom)
  out ($a8),a

        ld                a,SCCreplayerblock+1   ;set SeeSfxReplayer in page 1, block1+2, at $4000 in ROM
;        ld                ($5000),a
;        inc               a
        ld                ($7000),a
        ld a,$3f ;get bank ready for SCC access
        ld                ($9000),a
;        inc               a
;        ld                ($b000),a
        
        push hl
        push de
        push bc
        
  call  SccReplayerUpdate       ;keep running sfx every int
  
        pop bc
        pop de
        pop hl
   ;reset blocks
        ld                a,(memblocks.n1)     ;reset blocks
        ld                ($5000),a
        ld                a,(memblocks.n2)
        ld                ($7000),a
        ld                a,(memblocks.n3)
        ld                ($9000),a
;        ld                a,(memblocks.4)
;        ld                ($b000),a
    pop af
    out ($a8),a

  ld    a,(vblankflag)
  inc   a
  ld    (vblankflag),a

  pop af
  ei
  ret



switchmusicstar:

  ld a,(switchmusicforstar)
  cp 2
  jp z,.reset

  xor a
  ld (switchmusicforstar),a

;store the original music block
  ld a,(currentmusicblock)
  ld (previousmusicblock),a  
  
  ld a,invinsib
  ld (currentmusicblock),a  
  call switchmusic
  
  ret

.reset:

  xor a
  ld (switchmusicforstar),a

  ld a,(previousmusicblock)
  ld (currentmusicblock),a  
  call startmusic
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a   
  
  ret


switchmusicwin:
  
  ld a,(playwinmusic)
  cp 2
  jp z,.switchbosswinmusic

  ld a,$31
  ld (currentmusicblock),a  
  call switchmusic
  
  ret
  
.switchbosswinmusic:
  
  ld a,$6b
  ld (currentmusicblock),a  
  call switchmusic
  
  ret 



endengine2:
  
