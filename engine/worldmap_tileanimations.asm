
WMAnimYoIsl2toYoIsl3:
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 16+2,20  ;tilenumber, dy, dx
  dw    226-1 : db 16+2,21
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 15+2,20  ;tilenumber, dy, dx
  dw    226-1 : db 15+2,21
  db    4                 ;amount of tiles to put (0=end)
  dw    373-1 : db 13+2,20  ;tilenumber, dy, dx
  dw    374-1 : db 13+2,21  ;tilenumber, dy, dx
  dw    405-1 : db 14+2,20  ;tilenumber, dy, dx
  dw    406-1 : db 14+2,21  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimYoIsl3toYoIsl4:
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 13+2,22  ;tilenumber, dy, dx
  dw    231-1 : db 14+2,22
  db    2                 ;amount of tiles to put (0=end)
  dw    195-1 : db 13+2,23  ;tilenumber, dy, dx
  dw    227-1 : db 14+2,23
  db    3                 ;amount of tiles to put (0=end)
  dw    201-1 : db 13+2,24  ;tilenumber, dy, dx
  dw    233-1 : db 12+2,24
  dw    234-1 : db 12+2,25
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 11+2,24  ;tilenumber, dy, dx
  dw    226-1 : db 11+2,25
  db    4                 ;amount of tiles to put (0=end)
  dw    023-1 : db 09+2,24  ;tilenumber, dy, dx
  dw    024-1 : db 09+2,25
  dw    055-1 : db 10+2,24  ;tilenumber, dy, dx
  dw    056-1 : db 10+2,25
  db    0                 ;amount of tiles to put (0=end)

WMAnimYoIsl4toIggyCa:
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 08+2,24  ;tilenumber, dy, dx
  dw    226-1 : db 08+2,25
  db    3                 ;amount of tiles to put (0=end)
  dw    207-1 : db 06+2,24  ;tilenumber, dy, dx
  dw    239-1 : db 07+2,24
  dw    202-1 : db 07+2,25
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 06+2,23  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 06+2,22  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimYoIsl1toYeSwPa:
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 16+2,08  ;tilenumber, dy, dx
  dw    226-1 : db 16+2,09
  db    2                 ;amount of tiles to put (0=end)
  dw    193-1 : db 15+2,08  ;tilenumber, dy, dx
  dw    194-1 : db 15+2,09
  db    2                 ;amount of tiles to put (0=end)
  dw    233-1 : db 14+2,09  ;tilenumber, dy, dx
  dw    234-1 : db 14+2,10
  db    2                 ;amount of tiles to put (0=end)
  dw    193-1 : db 13+2,09  ;tilenumber, dy, dx
  dw    194-1 : db 13+2,10
  db    2                 ;amount of tiles to put (0=end)
  dw    233-1 : db 12+2,10  ;tilenumber, dy, dx
  dw    234-1 : db 12+2,11
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 11+2,10  ;tilenumber, dy, dx
  dw    226-1 : db 11+2,11
  db    13                ;amount of tiles to put (0=end)
  dw    107-1 : db 11+2,06  ;tilenumber, dy, dx
  dw    107-1 : db 12+2,05
  dw    107-1 : db 12+2,06
  dw    107-1 : db 12+2,07
  dw    107-1 : db 12+2,08
  dw    033-1 : db 13+2,05
  dw    034-1 : db 13+2,06
  dw    035-1 : db 13+2,07
  dw    036-1 : db 13+2,08
  dw    129-1 : db 14+2,05
  dw    130-1 : db 14+2,06
  dw    131-1 : db 14+2,07
  dw    132-1 : db 14+2,08
  db    8                 ;amount of tiles to put (0=end)
  dw    033-1 : db 12+2,05
  dw    034-1 : db 12+2,06
  dw    035-1 : db 12+2,07
  dw    036-1 : db 12+2,08
  dw    065-1 : db 13+2,05
  dw    066-1 : db 13+2,06
  dw    067-1 : db 13+2,07
  dw    068-1 : db 13+2,08
  db    12                ;amount of tiles to put (0=end)
  dw    033-1 : db 11+2,05  ;tilenumber, dy, dx
  dw    034-1 : db 11+2,06
  dw    035-1 : db 11+2,07
  dw    036-1 : db 11+2,08
  dw    065-1 : db 12+2,05
  dw    066-1 : db 12+2,06
  dw    067-1 : db 12+2,07
  dw    068-1 : db 12+2,08
  dw    097-1 : db 13+2,05
  dw    098-1 : db 13+2,06
  dw    099-1 : db 13+2,07
  dw    100-1 : db 13+2,08
  db    04                ;amount of tiles to put (0=end)
  dw    041-1 : db 08+2,05  ;tilenumber, dy, dx
  dw    042-1 : db 08+2,06
  dw    073-1 : db 09+2,05
  dw    074-1 : db 09+2,06
  db    04                ;amount of tiles to put (0=end)
  dw    002-1 : db 02+2,04  ;tilenumber, dy, dx
  dw    003-1 : db 02+2,05
  dw    073-1 : db 03+2,04
  dw    074-1 : db 03+2,05
  db    04                ;amount of tiles to put (0=end)
  dw    004-1 : db 07+2,05  ;tilenumber, dy, dx
  dw    042-1 : db 07+2,06
  dw    073-1 : db 08+2,05
  dw    074-1 : db 08+2,06
  db    04                ;amount of tiles to put (0=end)
  dw    002-1 : db 01+2,04  ;tilenumber, dy, dx
  dw    003-1 : db 01+2,05
  dw    073-1 : db 02+2,04
  dw    074-1 : db 02+2,05
  db    0                 ;amount of tiles to put (0=end)

WMAnimIggyCatoDoPla1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    225-1 : db 08+2,24  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    240-1 : db 04+2,20  ;tilenumber, dy, dx
  dw    241-1 : db 04+2,21
  dw    114-1 : db 05+2,20
  dw    115-1 : db 05+2,21
  dw    146-1 : db 06+2,20
  dw    147-1 : db 06+2,21

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    147-1 : db 06+2,21


  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 04+2,20  ;tilenumber, dy, dx
  dw    226-1 : db 04+2,21
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 03+2,20  ;tilenumber, dy, dx
  dw    226-1 : db 03+2,21
  db    2                 ;amount of tiles to put (0=end)
  dw    086-1 : db 02+2,20
  dw    087-1 : db 02+2,21
  db    2                 ;amount of tiles to put (0=end)
  dw    084-1 : db 01+2,20  ;tilenumber, dy, dx
  dw    085-1 : db 01+2,21
  db    2                 ;amount of tiles to put (0=end)
  dw    425-1 : db 00+2,20  ;tilenumber, dy, dx
  dw    426-1 : db 00+2,21
  db    0                 ;amount of tiles to put (0=end)

WMAnimIggyCatoDoPla1InWorld2:
  db    26                 ;amount of tiles to put (0=end)
  dw    613-1 : db 33+2,09  ;tilenumber, dy, dx
  dw    614-1 : db 33+2,10  ;tilenumber, dy, dx
  dw    645-1 : db 34+2,09  ;tilenumber, dy, dx
  dw    646-1 : db 34+2,10  ;tilenumber, dy, dx
  dw    225-1 : db 35+2,09  ;tilenumber, dy, dx
  dw    226-1 : db 35+2,10  ;tilenumber, dy, dx
  dw    225-1 : db 36+2,09  ;tilenumber, dy, dx
  dw    226-1 : db 36+2,10  ;tilenumber, dy, dx
  dw    329-1 : db 37+2,09  ;tilenumber, dy, dx
  dw    330-1 : db 37+2,10  ;tilenumber, dy, dx
  dw    361-1 : db 38+2,09  ;tilenumber, dy, dx
  dw    362-1 : db 38+2,10  ;tilenumber, dy, dx
  dw    393-1 : db 39+2,09  ;tilenumber, dy, dx
  dw    394-1 : db 39+2,10  ;tilenumber, dy, dx
  dw    361-1 : db 40+2,09  ;tilenumber, dy, dx
  dw    362-1 : db 40+2,10  ;tilenumber, dy, dx
  dw    393-1 : db 41+2,09  ;tilenumber, dy, dx
  dw    394-1 : db 41+2,10  ;tilenumber, dy, dx
  dw    425-1 : db 42+2,09  ;tilenumber, dy, dx
  dw    426-1 : db 42+2,10  ;tilenumber, dy, dx
  dw    080-1 : db 44+2,09  ;tilenumber, dy, dx
  dw    107-1 : db 44+2,10  ;tilenumber, dy, dx
  dw    465-1 : db 45+2,09  ;tilenumber, dy, dx
  dw    466-1 : db 45+2,10  ;tilenumber, dy, dx
  dw    497-1 : db 46+2,09  ;tilenumber, dy, dx
  dw    498-1 : db 46+2,10  ;tilenumber, dy, dx

  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla1toDoPla2:
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 33+2,08  ;tilenumber, dy, dx
  dw    231-1 : db 34+2,08
  db    2                 ;amount of tiles to put (0=end)
  dw    198-1 : db 33+2,07  ;tilenumber, dy, dx
  dw    230-1 : db 34+2,07
  db    1                 ;amount of tiles to put (0=end)
  dw    237-1 : db 33+2,06  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    205-1 : db 32+2,05  ;tilenumber, dy, dx
  dw    206-1 : db 32+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 31+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 31+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 30+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 30+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 29+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 29+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 28+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 28+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 27+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 27+2,06
  db    4                 ;amount of tiles to put (0=end)
  dw    611-1 : db 25+2,05  ;tilenumber, dy, dx
  dw    612-1 : db 25+2,06
  dw    643-1 : db 26+2,05  ;tilenumber, dy, dx
  dw    644-1 : db 26+2,06
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla1toDoSec1:
  db    1                 ;amount of tiles to put (0=end)
  dw    163-1 : db 30+2,10  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    162-1 : db 29+2,09  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    163-1 : db 28+2,10  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    615-1 : db 27+2,09  ;tilenumber, dy, dx
  dw    616-1 : db 27+2,10
  dw    647-1 : db 28+2,09  ;tilenumber, dy, dx
  dw    648-1 : db 28+2,10
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla2toGrSwPa:
  db    4                 ;amount of tiles to put (0=end)
  dw    080-1 : db 24+2,04  ;tilenumber, dy, dx
  dw    199-1 : db 25+2,04
  dw    231-1 : db 26+2,04
  dw    112-1 : db 27+2,04
  db    4                 ;amount of tiles to put (0=end)
  dw    079-1 : db 24+2,03  ;tilenumber, dy, dx
  dw    199-1 : db 25+2,03
  dw    231-1 : db 26+2,03
  dw    080-1 : db 27+2,03
  db    4                 ;amount of tiles to put (0=end)
  dw    490-1 : db 25+2,01  ;tilenumber, dy, dx
  dw    491-1 : db 25+2,02
  dw    492-1 : db 26+2,01
  dw    493-1 : db 26+2,02
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla2toDoGhos:
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 24+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 24+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 23+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 23+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 22+2,05  ;tilenumber, dy, dx
  dw    226-1 : db 22+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    238-1 : db 21+2,05  ;tilenumber, dy, dx
  dw    194-1 : db 21+2,06
  db    2                 ;amount of tiles to put (0=end)
  dw    298-1 : db 20+2,06  ;tilenumber, dy, dx
  dw    255-1 : db 20+2,07
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoSec1toDoSeGh:
  db    1                 ;amount of tiles to put (0=end)
  dw    164-1 : db 28+2,11  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    165-1 : db 27+2,12  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    164-1 : db 28+2,13  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoSec1toDoGhos:
  db    1                 ;amount of tiles to put (0=end)
  dw    673-1 : db 27+2,09  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    163-1 : db 26+2,10  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    162-1 : db 25+2,09  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    391-1 : db 23+2,09  ;tilenumber, dy, dx
  dw    392-1 : db 23+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    350-1 : db 22+2,09  ;tilenumber, dy, dx
  dw    351-1 : db 22+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    235-1 : db 21+2,09  ;tilenumber, dy, dx
  dw    236-1 : db 21+2,10  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoGhostoTopSec:
  db    1                 ;amount of tiles to put (0=end)
  dw    305-1 : db 19+2,09  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 18+2,09  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 17+2,09  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    621-1 : db 15+2,09  ;tilenumber, dy, dx
  dw    622-1 : db 15+2,10  ;tilenumber, dy, dx
  dw    653-1 : db 16+2,09  ;tilenumber, dy, dx
  dw    654-1 : db 16+2,10  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoSeGhtoStarA1:
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 34+2,13  ;tilenumber, dy, dx
  dw    226-1 : db 34+2,14  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    225-1 : db 35+2,13  ;tilenumber, dy, dx
  dw    226-1 : db 35+2,14  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    525-1 : db 35+2,13  ;tilenumber, dy, dx
  dw    526-1 : db 35+2,14  ;tilenumber, dy, dx
  dw    557-1 : db 36+2,13  ;tilenumber, dy, dx
  dw    558-1 : db 36+2,14  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoSeGhtoTubeG1:
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 32+2,16  ;tilenumber, dy, dx
  dw    231-1 : db 33+2,16  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    545-1 : db 32+2,17  ;tilenumber, dy, dx
  dw    231-1 : db 33+2,17  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    546-1 : db 32+2,18  ;tilenumber, dy, dx
  dw    227-1 : db 33+2,18  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoGhostoDoPla3:
  db    4                 ;amount of tiles to put (0=end)
  dw    373-1 : db 19+2,17  ;tilenumber, dy, dx
  dw    374-1 : db 19+2,18  ;tilenumber, dy, dx
  dw    405-1 : db 20+2,17  ;tilenumber, dy, dx
  dw    406-1 : db 20+2,18  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimTubeH1toDoPla3:  
  db    12                 ;amount of tiles to put (0=end)
  dw    373-1 : db 19+2,17  ;tilenumber, dy, dx
  dw    374-1 : db 19+2,18  ;tilenumber, dy, dx
  dw    405-1 : db 20+2,17  ;tilenumber, dy, dx
  dw    406-1 : db 20+2,18  ;tilenumber, dy, dx

  dw    350-1 : db 21+2,17  ;tilenumber, dy, dx
  dw    351-1 : db 21+2,18  ;tilenumber, dy, dx
  dw    391-1 : db 22+2,17  ;tilenumber, dy, dx
  dw    392-1 : db 22+2,18  ;tilenumber, dy, dx

  dw    193-1 : db 26+2,17  ;tilenumber, dy, dx
  dw    202-1 : db 26+2,18  ;tilenumber, dy, dx
  dw    196-1 : db 27+2,19  ;tilenumber, dy, dx
  dw    230-1 : db 28+2,19  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla3toDoPla4:  
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 19+2,19  ;tilenumber, dy, dx
  dw    231-1 : db 20+2,19  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    197-1 : db 19+2,20  ;tilenumber, dy, dx
  dw    235-1 : db 20+2,20  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    205-1 : db 21+2,20  ;tilenumber, dy, dx
  dw    206-1 : db 21+2,21  ;tilenumber, dy, dx
  dw    202-1 : db 20+2,21  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    239-1 : db 22+2,21  ;tilenumber, dy, dx
  dw    202-1 : db 22+2,22  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    373-1 : db 23+2,21  ;tilenumber, dy, dx
  dw    628-1 : db 23+2,22  ;tilenumber, dy, dx
  dw    405-1 : db 24+2,21  ;tilenumber, dy, dx
  dw    660-1 : db 24+2,22  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoPla4toMortCa:
  db    1                 ;amount of tiles to put (0=end)
  dw    261-1 : db 23+2,23  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    293-1 : db 24+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimMortCatoVaDom1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    293-1 : db 24+2,24  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    255-1 : db 22+2,25  ;tilenumber, dy, dx
  dw    197-1 : db 22+2,26  ;tilenumber, dy, dx
  dw    467-1 : db 23+2,25  ;tilenumber, dy, dx
  dw    468-1 : db 23+2,26  ;tilenumber, dy, dx
  dw    499-1 : db 24+2,25  ;tilenumber, dy, dx
  dw    500-1 : db 24+2,26  ;tilenumber, dy, dx

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    293-1 : db 24+2,24  ;tilenumber, dy, dx

  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 21+2,25  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 20+2,25  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    255-1 : db 19+2,25  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    323-1 : db 17+2,25  ;tilenumber, dy, dx
  dw    324-1 : db 17+2,26  ;tilenumber, dy, dx
  dw    355-1 : db 18+2,25  ;tilenumber, dy, dx
  dw    356-1 : db 18+2,26  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimDoSec2toTubeH2:
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 03+2,04  ;tilenumber, dy, dx
  dw    180-1 : db 04+2,04  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 03+2,05  ;tilenumber, dy, dx
  dw    180-1 : db 04+2,05  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimTubeG2toDoSec2:
  db    8                 ;amount of tiles to put (0=end)
  dw    067-1 : db 03+2,02  ;tilenumber, dy, dx
  dw    068-1 : db 03+2,03  ;tilenumber, dy, dx
  dw    099-1 : db 04+2,02  ;tilenumber, dy, dx
  dw    100-1 : db 04+2,03  ;tilenumber, dy, dx
  dw    129-1 : db 05+2,02  ;tilenumber, dy, dx
  dw    130-1 : db 05+2,03  ;tilenumber, dy, dx
  dw    129-1 : db 06+2,02  ;tilenumber, dy, dx
  dw    130-1 : db 06+2,03  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimYeSwPaFinished:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    148-1 : db 34+2,05  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    149-1 : db 33+2,03  ;tilenumber, dy, dx
  dw    346-1 : db 33+2,04  ;tilenumber, dy, dx
  dw    328-1 : db 34+2,03  ;tilenumber, dy, dx
  dw    458-1 : db 34+2,04  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimGrSwPaFinished:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    148-1 : db 34+2,05  ;tilenumber, dy, dx
  db    4 + %1000'0000      ;explosion sfx
  dw    104-1 : db 25+2,01  ;tilenumber, dy, dx
  dw    043-1 : db 25+2,02  ;tilenumber, dy, dx
  dw    494-1 : db 26+2,01  ;tilenumber, dy, dx
  dw    495-1 : db 26+2,02  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimReSwPaFinished:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    153-1 : db 16+2,15  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    320-1 : db 16+2,23  ;tilenumber, dy, dx
  dw    283-1 : db 16+2,24  ;tilenumber, dy, dx
  dw    355-1 : db 17+2,23  ;tilenumber, dy, dx
  dw    356-1 : db 17+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom1toVaDom2:
  db    2                 ;amount of tiles to put (0=end)
  dw    153-1 : db 16+2,15  ;tilenumber, dy, dx
  dw    185-1 : db 17+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    158-1 : db 16+2,16  ;tilenumber, dy, dx
  dw    190-1 : db 17+2,16  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    192-1 : db 17+2,17  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    191-1 : db 16+2,18  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    160-1 : db 16+2,19  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    159-1 : db 15+2,19  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    160-1 : db 14+2,20  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    089-1 : db 12+2,19  ;tilenumber, dy, dx
  dw    090-1 : db 12+2,20  ;tilenumber, dy, dx
  dw    121-1 : db 13+2,19  ;tilenumber, dy, dx
  dw    122-1 : db 13+2,20  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom1toVaSec1:
  db    2                 ;amount of tiles to put (0=end)
  dw    153-1 : db 16+2,12  ;tilenumber, dy, dx
  dw    185-1 : db 17+2,12  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    310-1 : db 16+2,10  ;tilenumber, dy, dx
  dw    135-1 : db 16+2,11  ;tilenumber, dy, dx
  dw    309-1 : db 17+2,10  ;tilenumber, dy, dx
  dw    167-1 : db 17+2,11  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    166-1 : db 16+2,10  ;tilenumber, dy, dx
  db    5                 ;amount of tiles to put (0=end)
  dw    309-1 : db 15+2,08  ;tilenumber, dy, dx
  dw    133-1 : db 15+2,09  ;tilenumber, dy, dx
  dw    134-1 : db 15+2,10  ;tilenumber, dy, dx
  dw    310-1 : db 16+2,08  ;tilenumber, dy, dx
  dw    309-1 : db 16+2,09  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    156-1 : db 14+2,09  ;tilenumber, dy, dx
  dw    157-1 : db 14+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 13+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 13+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 12+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 12+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 11+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 11+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 10+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 10+2,10  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    087-1 : db 08+2,09  ;tilenumber, dy, dx
  dw    088-1 : db 08+2,10  ;tilenumber, dy, dx
  dw    119-1 : db 09+2,09  ;tilenumber, dy, dx
  dw    120-1 : db 09+2,10  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom2toReSwPa:
  db    1                 ;amount of tiles to put (0=end)
  dw    159-1 : db 13+2,21  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    191-1 : db 13+2,22  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    159-1 : db 14+2,22  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    191-1 : db 14+2,23  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    159-1 : db 15+2,23  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    321-1 : db 16+2,23  ;tilenumber, dy, dx
  dw    322-1 : db 16+2,24  ;tilenumber, dy, dx
  dw    353-1 : db 17+2,23  ;tilenumber, dy, dx
  dw    354-1 : db 17+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaSec1toStarB1:
  db    4                 ;amount of tiles to put (0=end)
  dw    308-1 : db 08+2,05  ;tilenumber, dy, dx
  dw    276-1 : db 08+2,06  ;tilenumber, dy, dx
  dw    308-1 : db 09+2,05  ;tilenumber, dy, dx
  dw    308-1 : db 09+2,06  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    276-1 : db 07+2,03  ;tilenumber, dy, dx
  dw    308-1 : db 07+2,04  ;tilenumber, dy, dx
  dw    308-1 : db 08+2,03  ;tilenumber, dy, dx
  dw    308-1 : db 08+2,04  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    308-1 : db 09+2,03  ;tilenumber, dy, dx
  dw    308-1 : db 09+2,04  ;tilenumber, dy, dx
  dw    276-1 : db 10+2,03  ;tilenumber, dy, dx
  dw    308-1 : db 10+2,04  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    201-1 : db 08+2,03  ;tilenumber, dy, dx
  dw    202-1 : db 08+2,04  ;tilenumber, dy, dx
  dw    233-1 : db 09+2,03  ;tilenumber, dy, dx
  dw    234-1 : db 09+2,04  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaSec1toTubeI2:
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 07+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 07+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 06+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 06+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 05+2,09  ;tilenumber, dy, dx
  dw    164-1 : db 05+2,10  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    188-1 : db 04+2,09  ;tilenumber, dy, dx
  dw    189-1 : db 04+2,10  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom2toVaGhos:
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 11+2,19  ;tilenumber, dy, dx
  dw    164-1 : db 11+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 10+2,19  ;tilenumber, dy, dx
  dw    164-1 : db 10+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 09+2,19  ;tilenumber, dy, dx
  dw    164-1 : db 09+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    163-1 : db 08+2,19  ;tilenumber, dy, dx
  dw    164-1 : db 08+2,20  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    188-1 : db 07+2,19  ;tilenumber, dy, dx
  dw    189-1 : db 07+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    129-1 : db 06+2,19  ;tilenumber, dy, dx
  dw    130-1 : db 06+2,20  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    338-1 : db 04+2,19  ;tilenumber, dy, dx
  dw    339-1 : db 04+2,20  ;tilenumber, dy, dx
  dw    370-1 : db 05+2,19  ;tilenumber, dy, dx
  dw    371-1 : db 05+2,20  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaGhostoVaDom3:
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 04+2,21  ;tilenumber, dy, dx
  dw    180-1 : db 05+2,21  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 04+2,22  ;tilenumber, dy, dx
  dw    180-1 : db 05+2,22  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 04+2,23  ;tilenumber, dy, dx
  dw    180-1 : db 05+2,23  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 04+2,24  ;tilenumber, dy, dx
  dw    180-1 : db 05+2,24  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    138-1 : db 04+2,25  ;tilenumber, dy, dx
  dw    170-1 : db 05+2,25  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    139-1 : db 05+2,26  ;tilenumber, dy, dx
  dw    171-1 : db 06+2,26  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    138-1 : db 05+2,27  ;tilenumber, dy, dx
  dw    172-1 : db 06+2,27  ;tilenumber, dy, dx
  dw    141-1 : db 06+2,28  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    129-1 : db 07+2,27  ;tilenumber, dy, dx
  dw    130-1 : db 07+2,28  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    071-1 : db 08+2,27  ;tilenumber, dy, dx
  dw    072-1 : db 08+2,28  ;tilenumber, dy, dx
  dw    103-1 : db 09+2,27  ;tilenumber, dy, dx
  dw    104-1 : db 09+2,28  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom3toVaDom4:
  db    2                 ;amount of tiles to put (0=end)
  dw    129-1 : db 10+2,27  ;tilenumber, dy, dx
  dw    130-1 : db 10+2,28  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    129-1 : db 11+2,27  ;tilenumber, dy, dx
  dw    130-1 : db 11+2,28  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    067-1 : db 12+2,27  ;tilenumber, dy, dx
  dw    068-1 : db 12+2,28  ;tilenumber, dy, dx
  dw    099-1 : db 13+2,27  ;tilenumber, dy, dx
  dw    100-1 : db 13+2,28  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaDom4toLemmCa:
  db    2                 ;amount of tiles to put (0=end)
  dw    129-1 : db 14+2,27  ;tilenumber, dy, dx
  dw    130-1 : db 14+2,28  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    217-1 : db 15+2,27  ;tilenumber, dy, dx
  dw    218-1 : db 15+2,28  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimLemmCatoTubeJ2:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    129-1 : db 14+2,27  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    129-1 : db 15+2,27  ;tilenumber, dy, dx
  dw    130-1 : db 15+2,28  ;tilenumber, dy, dx
  dw    325-1 : db 16+2,27  ;tilenumber, dy, dx
  dw    326-1 : db 16+2,28  ;tilenumber, dy, dx
  dw    357-1 : db 17+2,27  ;tilenumber, dy, dx
  dw    358-1 : db 17+2,28  ;tilenumber, dy, dx

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    129-1 : db 14+2,27  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    137-1 : db 19+2,27  ;tilenumber, dy, dx
  dw    173-1 : db 19+2,28  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    169-1 : db 20+2,27  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    136-1 : db 20+2,26  ;tilenumber, dy, dx
  dw    168-1 : db 21+2,26  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    148-1 : db 20+2,25  ;tilenumber, dy, dx
  dw    180-1 : db 21+2,25  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    209-1 : db 20+2,24  ;tilenumber, dy, dx
  dw    241-1 : db 21+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimLocationStarB1:
  db    4                 ;amount of tiles to put (0=end)
  dw    340-1 : db 08+2,03  ;tilenumber, dy, dx
  dw    341-1 : db 08+2,04  ;tilenumber, dy, dx
  dw    372-1 : db 09+2,03  ;tilenumber, dy, dx
  dw    373-1 : db 09+2,04  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaSec2toVaSec3:
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 05+2,25  ;tilenumber, dy, dx
  dw    231-1 : db 06+2,25  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 05+2,26  ;tilenumber, dy, dx
  dw    231-1 : db 06+2,26  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    023-1 : db 05+2,27  ;tilenumber, dy, dx
  dw    024-1 : db 05+2,28  ;tilenumber, dy, dx
  dw    055-1 : db 06+2,27  ;tilenumber, dy, dx
  dw    056-1 : db 06+2,28  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimVaForttoButBr1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    199-1 : db 05+2,25  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    436-1 : db 05+2,31  ;tilenumber, dy, dx
  dw    469-1 : db 05+2,32  ;tilenumber, dy, dx
  dw    501-1 : db 06+2,31  ;tilenumber, dy, dx
  dw    502-1 : db 06+2,32  ;tilenumber, dy, dx

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    199-1 : db 05+2,25  ;tilenumber, dy, dx

  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,36  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,36  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,47  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,47  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,37  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,37  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,46  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,46  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,38  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,38  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,45  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,45  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,39  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,39  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,44  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,44  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,40  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,40  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,43  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,43  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    325-1 : db 05+2,41  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,41  ;tilenumber, dy, dx
  dw    325-1 : db 05+2,42  ;tilenumber, dy, dx
  dw    357-1 : db 06+2,42  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    623-1 : db 05+2,39  ;tilenumber, dy, dx
  dw    624-1 : db 05+2,40  ;tilenumber, dy, dx
  dw    655-1 : db 06+2,39  ;tilenumber, dy, dx
  dw    656-1 : db 06+2,40  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimButBr1toButBr2:
  db    4                 ;amount of tiles to put (0=end)
  dw    623-1 : db 05+2,43  ;tilenumber, dy, dx
  dw    624-1 : db 05+2,44  ;tilenumber, dy, dx
  dw    655-1 : db 06+2,43  ;tilenumber, dy, dx
  dw    656-1 : db 06+2,44  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimButBr2toLudwCa:
  db    2                 ;amount of tiles to put (0=end)
  dw    199-1 : db 05+2,49  ;tilenumber, dy, dx
  dw    231-1 : db 06+2,49  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimTubeJ1toCheese:
  db    4                 ;amount of tiles to put (0=end)
  dw    625-1 : db 09+2,39  ;tilenumber, dy, dx
  dw    626-1 : db 09+2,40  ;tilenumber, dy, dx
  dw    657-1 : db 10+2,39  ;tilenumber, dy, dx
  dw    658-1 : db 10+2,40  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimCheesetoSodaLa:
  db    2                 ;amount of tiles to put (0=end)
  dw    583-1 : db 10+2,39  ;tilenumber, dy, dx
  dw    584-1 : db 10+2,40  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    585-1 : db 11+2,39  ;tilenumber, dy, dx
  dw    586-1 : db 11+2,40  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    587-1 : db 12+2,39  ;tilenumber, dy, dx
  dw    588-1 : db 12+2,40  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    589-1 : db 13+2,39  ;tilenumber, dy, dx
  dw    590-1 : db 13+2,40  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    591-1 : db 14+2,39  ;tilenumber, dy, dx
  dw    592-1 : db 14+2,40  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    617-1 : db 15+2,39  ;tilenumber, dy, dx
  dw    618-1 : db 15+2,40  ;tilenumber, dy, dx
  dw    649-1 : db 16+2,39  ;tilenumber, dy, dx
  dw    650-1 : db 16+2,40  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimCheesetoCookie:
  db    2                 ;amount of tiles to put (0=end)
  dw    375-1 : db 09+2,43  ;tilenumber, dy, dx
  dw    407-1 : db 10+2,43  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    376-1 : db 09+2,44  ;tilenumber, dy, dx
  dw    408-1 : db 10+2,44  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    377-1 : db 09+2,45  ;tilenumber, dy, dx
  dw    378-1 : db 09+2,46  ;tilenumber, dy, dx
  dw    409-1 : db 10+2,45  ;tilenumber, dy, dx
  dw    410-1 : db 10+2,46  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimCookietoLudwCa:
  db    2                 ;amount of tiles to put (0=end)
  dw    439-1 : db 11+2,45  ;tilenumber, dy, dx
  dw    440-1 : db 11+2,46  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    441-1 : db 12+2,45  ;tilenumber, dy, dx
  dw    442-1 : db 12+2,46  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    471-1 : db 13+2,46  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    472-1 : db 13+2,47  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    472-1 : db 13+2,48  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    472-1 : db 13+2,49  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    472-1 : db 13+2,50  ;tilenumber, dy, dx
  db    1                 ;amount of tiles to put (0=end)
  dw    470-1 : db 13+2,51  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    391-1 : db 11+2,51  ;tilenumber, dy, dx
  dw    392-1 : db 11+2,52  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    391-1 : db 10+2,51  ;tilenumber, dy, dx
  dw    392-1 : db 10+2,52  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    391-1 : db 09+2,51  ;tilenumber, dy, dx
  dw    392-1 : db 09+2,52  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    082-1 : db 08+2,51  ;tilenumber, dy, dx
  dw    083-1 : db 08+2,52  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimLudwCatoForIl1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    106 : db 04+2,51  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    106 : db 04+2,51  ;tilenumber, dy, dx
  dw    079 : db 04+2,52  ;tilenumber, dy, dx
  dw    466 : db 05+2,51  ;tilenumber, dy, dx
  dw    467 : db 05+2,52  ;tilenumber, dy, dx
  dw    498 : db 06+2,51  ;tilenumber, dy, dx
  dw    499 : db 06+2,52  ;tilenumber, dy, dx

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    106 : db 04+2,51  ;tilenumber, dy, dx

  db    12                ;amount of tiles to put (0=end)
  dw    256 : db 00+2,55  ;tilenumber, dy, dx
  dw    257 : db 00+2,56  ;tilenumber, dy, dx
  dw    258 : db 00+2,57  ;tilenumber, dy, dx
  dw    259 : db 00+2,58  ;tilenumber, dy, dx
  dw    288 : db 01+2,55  ;tilenumber, dy, dx
  dw    289 : db 01+2,56  ;tilenumber, dy, dx
  dw    290 : db 01+2,57  ;tilenumber, dy, dx
  dw    291 : db 01+2,58  ;tilenumber, dy, dx
  dw    096 : db 02+2,55  ;tilenumber, dy, dx
  dw    097 : db 02+2,56  ;tilenumber, dy, dx
  dw    098 : db 02+2,57  ;tilenumber, dy, dx
  dw    099 : db 02+2,58  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    308 : db 06+2,53  ;tilenumber, dy, dx
  dw    308 : db 06+2,54  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    308 : db 06+2,55  ;tilenumber, dy, dx
  dw    261 : db 07+2,55  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    349 : db 08+2,55  ;tilenumber, dy, dx
  dw    350 : db 08+2,56  ;tilenumber, dy, dx
  dw    390 : db 09+2,55  ;tilenumber, dy, dx
  dw    391 : db 09+2,56  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    390 : db 10+2,55  ;tilenumber, dy, dx
  dw    391 : db 10+2,56  ;tilenumber, dy, dx
  dw    390 : db 11+2,55  ;tilenumber, dy, dx
  dw    391 : db 11+2,56  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    390 : db 12+2,55  ;tilenumber, dy, dx
  dw    391 : db 12+2,56  ;tilenumber, dy, dx
  dw    390 : db 13+2,55  ;tilenumber, dy, dx
  dw    391 : db 13+2,56  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    390 : db 14+2,55  ;tilenumber, dy, dx
  dw    391 : db 14+2,56  ;tilenumber, dy, dx
  dw    390 : db 15+2,55  ;tilenumber, dy, dx
  dw    391 : db 15+2,56  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    179 : db 16+2,55  ;tilenumber, dy, dx
  dw    180 : db 16+2,56  ;tilenumber, dy, dx
  dw    224 : db 17+2,55  ;tilenumber, dy, dx
  dw    225 : db 17+2,56  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    224 : db 18+2,55  ;tilenumber, dy, dx
  dw    225 : db 18+2,56  ;tilenumber, dy, dx
  dw    224 : db 19+2,55  ;tilenumber, dy, dx
  dw    225 : db 19+2,56  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    204 : db 20+2,55  ;tilenumber, dy, dx
  dw    233 : db 20+2,56  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimForIl1toFoGhos:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 04+2,16  ;tilenumber, dy, dx
  dw    068 : db 04+2,17  ;tilenumber, dy, dx
  dw    068 : db 05+2,16  ;tilenumber, dy, dx
  dw    068 : db 05+2,17  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 04+2,17  ;tilenumber, dy, dx
  dw    052 : db 05+2,17  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 04+2,16  ;tilenumber, dy, dx
  dw    052 : db 05+2,16  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    010 : db 04+2,14  ;tilenumber, dy, dx
  dw    011 : db 04+2,15  ;tilenumber, dy, dx
  dw    042 : db 05+2,14  ;tilenumber, dy, dx
  dw    043 : db 05+2,15  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimFoGhostoForIl4:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 10+2,10  ;tilenumber, dy, dx
  dw    068 : db 10+2,11  ;tilenumber, dy, dx
  dw    068 : db 11+2,10  ;tilenumber, dy, dx
  dw    068 : db 11+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 04+2,13  ;tilenumber, dy, dx
  dw    052 : db 05+2,13  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    025 : db 04+2,12  ;tilenumber, dy, dx
  dw    023 : db 05+2,11  ;tilenumber, dy, dx
  dw    024 : db 05+2,12  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    055 : db 06+2,10  ;tilenumber, dy, dx
  dw    056 : db 06+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 07+2,10  ;tilenumber, dy, dx
  dw    022 : db 07+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 08+2,10  ;tilenumber, dy, dx
  dw    022 : db 08+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 09+2,10  ;tilenumber, dy, dx
  dw    022 : db 09+2,11  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    016 : db 10+2,10  ;tilenumber, dy, dx
  dw    017 : db 10+2,11  ;tilenumber, dy, dx
  dw    048 : db 11+2,10  ;tilenumber, dy, dx
  dw    049 : db 11+2,11  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimForIl1toForIl2:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 04+2,20  ;tilenumber, dy, dx
  dw    068 : db 04+2,21  ;tilenumber, dy, dx
  dw    068 : db 05+2,20  ;tilenumber, dy, dx
  dw    068 : db 05+2,21  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 05+2,22  ;tilenumber, dy, dx
  dw    068 : db 05+2,23  ;tilenumber, dy, dx
  dw    068 : db 06+2,22  ;tilenumber, dy, dx
  dw    068 : db 06+2,23  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 07+2,22  ;tilenumber, dy, dx
  dw    068 : db 07+2,23  ;tilenumber, dy, dx
  dw    068 : db 08+2,22  ;tilenumber, dy, dx
  dw    068 : db 08+2,23  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 04+2,20  ;tilenumber, dy, dx
  dw    052 : db 05+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    026 : db 04+2,21  ;tilenumber, dy, dx
  dw    058 : db 05+2,21  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    027 : db 05+2,22  ;tilenumber, dy, dx
  dw    059 : db 06+2,22  ;tilenumber, dy, dx
  dw    057 : db 06+2,23  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 07+2,22  ;tilenumber, dy, dx
  dw    022 : db 07+2,23  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    021 : db 08+2,22  ;tilenumber, dy, dx
  dw    022 : db 08+2,23  ;tilenumber, dy, dx
  dw    021 : db 09+2,22  ;tilenumber, dy, dx
  dw    022 : db 09+2,23  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    073 : db 10+2,22  ;tilenumber, dy, dx
  dw    074 : db 10+2,23  ;tilenumber, dy, dx
  dw    075 : db 11+2,22  ;tilenumber, dy, dx
  dw    076 : db 11+2,23  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)  
  
WMAnimForIl2toBlSwPa:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 10+2,24  ;tilenumber, dy, dx
  dw    068 : db 10+2,25  ;tilenumber, dy, dx
  dw    068 : db 11+2,24  ;tilenumber, dy, dx
  dw    068 : db 11+2,25  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 10+2,26  ;tilenumber, dy, dx
  dw    068 : db 10+2,27  ;tilenumber, dy, dx
  dw    068 : db 11+2,26  ;tilenumber, dy, dx
  dw    068 : db 11+2,27  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,24  ;tilenumber, dy, dx
  dw    052 : db 11+2,24  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,25  ;tilenumber, dy, dx
  dw    052 : db 11+2,25  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,26  ;tilenumber, dy, dx
  dw    052 : db 11+2,26  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,27  ;tilenumber, dy, dx
  dw    052 : db 11+2,27  ;tilenumber, dy, dx

  db    4                 ;amount of tiles to put (0=end)
  dw    069 : db 10+2,26  ;tilenumber, dy, dx
  dw    070 : db 10+2,27  ;tilenumber, dy, dx
  dw    071 : db 11+2,26  ;tilenumber, dy, dx
  dw    072 : db 11+2,27  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)  
  
WMAnimBlSwPaFinished:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    069 : db 10+2,26  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    020 : db 10+2,26  ;tilenumber, dy, dx
  dw    020 : db 10+2,27  ;tilenumber, dy, dx
  dw    040 : db 11+2,26  ;tilenumber, dy, dx
  dw    041 : db 11+2,27  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimForIl2toForIl3:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 14+2,21  ;tilenumber, dy, dx
  dw    068 : db 14+2,22  ;tilenumber, dy, dx
  dw    068 : db 15+2,21  ;tilenumber, dy, dx
  dw    068 : db 15+2,22  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 14+2,18  ;tilenumber, dy, dx
  dw    068 : db 14+2,19  ;tilenumber, dy, dx
  dw    068 : db 15+2,18  ;tilenumber, dy, dx
  dw    068 : db 15+2,19  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 12+2,22  ;tilenumber, dy, dx
  dw    022 : db 12+2,23  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    053 : db 13+2,22  ;tilenumber, dy, dx
  dw    054 : db 13+2,23  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    028 : db 14+2,21  ;tilenumber, dy, dx
  dw    029 : db 14+2,22  ;tilenumber, dy, dx
  dw    060 : db 15+2,21  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 14+2,20  ;tilenumber, dy, dx
  dw    052 : db 15+2,20  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    016 : db 14+2,18  ;tilenumber, dy, dx
  dw    017 : db 14+2,19  ;tilenumber, dy, dx
  dw    048 : db 15+2,18  ;tilenumber, dy, dx
  dw    049 : db 15+2,19  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)    

WMAnimForIl3toFoGhos:
  db    4                 ;amount of tiles to put (0=end)
  dw    016 : db 14+2,18  ;tilenumber, dy, dx
  dw    017 : db 14+2,19  ;tilenumber, dy, dx
  dw    048 : db 15+2,18  ;tilenumber, dy, dx
  dw    049 : db 15+2,19  ;tilenumber, dy, dx

  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 13+2,15  ;tilenumber, dy, dx
  dw    068 : db 13+2,16  ;tilenumber, dy, dx
  dw    068 : db 14+2,15  ;tilenumber, dy, dx
  dw    068 : db 14+2,16  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 08+2,14  ;tilenumber, dy, dx
  dw    068 : db 08+2,15  ;tilenumber, dy, dx
  dw    068 : db 09+2,14  ;tilenumber, dy, dx
  dw    068 : db 09+2,15  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 06+2,14  ;tilenumber, dy, dx
  dw    068 : db 06+2,15  ;tilenumber, dy, dx
  dw    068 : db 07+2,14  ;tilenumber, dy, dx
  dw    068 : db 07+2,15  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 14+2,17  ;tilenumber, dy, dx
  dw    052 : db 15+2,17  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    063 : db 14+2,15  ;tilenumber, dy, dx
  dw    030 : db 14+2,16  ;tilenumber, dy, dx
  dw    062 : db 15+2,16  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    061 : db 13+2,14  ;tilenumber, dy, dx
  dw    031 : db 13+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 12+2,14  ;tilenumber, dy, dx
  dw    022 : db 12+2,15  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    012 : db 10+2,14  ;tilenumber, dy, dx
  dw    013 : db 10+2,15  ;tilenumber, dy, dx
  dw    044 : db 11+2,14  ;tilenumber, dy, dx
  dw    045 : db 11+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 09+2,14  ;tilenumber, dy, dx
  dw    022 : db 09+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 08+2,14  ;tilenumber, dy, dx
  dw    022 : db 08+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 07+2,14  ;tilenumber, dy, dx
  dw    022 : db 07+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 06+2,14  ;tilenumber, dy, dx
  dw    022 : db 06+2,15  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    010 : db 04+2,14  ;tilenumber, dy, dx
  dw    011 : db 04+2,15  ;tilenumber, dy, dx
  dw    042 : db 05+2,14  ;tilenumber, dy, dx
  dw    043 : db 05+2,15  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)   

WMAnimForIl4toForIl2:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 10+2,16  ;tilenumber, dy, dx
  dw    068 : db 10+2,17  ;tilenumber, dy, dx
  dw    068 : db 11+2,16  ;tilenumber, dy, dx
  dw    068 : db 11+2,17  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 10+2,19  ;tilenumber, dy, dx
  dw    068 : db 10+2,20  ;tilenumber, dy, dx
  dw    068 : db 11+2,19  ;tilenumber, dy, dx
  dw    068 : db 11+2,20  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,12  ;tilenumber, dy, dx
  dw    052 : db 11+2,12  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,13  ;tilenumber, dy, dx
  dw    052 : db 11+2,13  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    012 : db 10+2,14  ;tilenumber, dy, dx
  dw    013 : db 10+2,15  ;tilenumber, dy, dx
  dw    044 : db 11+2,14  ;tilenumber, dy, dx
  dw    045 : db 11+2,15  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,16  ;tilenumber, dy, dx
  dw    052 : db 11+2,16  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,17  ;tilenumber, dy, dx
  dw    052 : db 11+2,17  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,18  ;tilenumber, dy, dx
  dw    052 : db 11+2,18  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,19  ;tilenumber, dy, dx
  dw    052 : db 11+2,19  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,20  ;tilenumber, dy, dx
  dw    052 : db 11+2,20  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 10+2,21  ;tilenumber, dy, dx
  dw    052 : db 11+2,21  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    073 : db 10+2,22  ;tilenumber, dy, dx
  dw    074 : db 10+2,23  ;tilenumber, dy, dx
  dw    075 : db 11+2,22  ;tilenumber, dy, dx
  dw    076 : db 11+2,23  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)   

WMAnimForIl4toForSec:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 14+2,10  ;tilenumber, dy, dx
  dw    068 : db 14+2,11  ;tilenumber, dy, dx
  dw    068 : db 15+2,10  ;tilenumber, dy, dx
  dw    068 : db 15+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 12+2,10  ;tilenumber, dy, dx
  dw    022 : db 12+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 13+2,10  ;tilenumber, dy, dx
  dw    022 : db 13+2,11  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    014 : db 14+2,10  ;tilenumber, dy, dx
  dw    015 : db 14+2,11  ;tilenumber, dy, dx
  dw    046 : db 15+2,10  ;tilenumber, dy, dx
  dw    047 : db 15+2,11  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)   

WMAnimForSectoForFor:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 16+2,10  ;tilenumber, dy, dx
  dw    068 : db 16+2,11  ;tilenumber, dy, dx
  dw    068 : db 17+2,10  ;tilenumber, dy, dx
  dw    068 : db 17+2,11  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 18+2,09  ;tilenumber, dy, dx
  dw    068 : db 18+2,10  ;tilenumber, dy, dx
  dw    068 : db 19+2,09  ;tilenumber, dy, dx
  dw    068 : db 19+2,10  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 18+2,07  ;tilenumber, dy, dx
  dw    068 : db 18+2,08  ;tilenumber, dy, dx
  dw    068 : db 19+2,07  ;tilenumber, dy, dx
  dw    068 : db 19+2,08  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 18+2,03  ;tilenumber, dy, dx
  dw    068 : db 18+2,04  ;tilenumber, dy, dx
  dw    068 : db 19+2,03  ;tilenumber, dy, dx
  dw    068 : db 19+2,04  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    021 : db 16+2,10  ;tilenumber, dy, dx
  dw    022 : db 16+2,11  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    053 : db 17+2,10  ;tilenumber, dy, dx
  dw    054 : db 17+2,11  ;tilenumber, dy, dx
  db    3                 ;amount of tiles to put (0=end)
  dw    028 : db 18+2,09  ;tilenumber, dy, dx
  dw    029 : db 18+2,10  ;tilenumber, dy, dx
  dw    060 : db 19+2,09  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    020 : db 18+2,07  ;tilenumber, dy, dx
  dw    020 : db 18+2,08  ;tilenumber, dy, dx
  dw    052 : db 19+2,07  ;tilenumber, dy, dx
  dw    052 : db 19+2,08  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    020 : db 18+2,05  ;tilenumber, dy, dx
  dw    020 : db 18+2,06  ;tilenumber, dy, dx
  dw    052 : db 19+2,05  ;tilenumber, dy, dx
  dw    052 : db 19+2,06  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    020 : db 18+2,03  ;tilenumber, dy, dx
  dw    020 : db 18+2,04  ;tilenumber, dy, dx
  dw    052 : db 19+2,03  ;tilenumber, dy, dx
  dw    052 : db 19+2,04  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    020 : db 18+2,01  ;tilenumber, dy, dx
  dw    020 : db 18+2,02  ;tilenumber, dy, dx
  dw    052 : db 19+2,01  ;tilenumber, dy, dx
  dw    052 : db 19+2,02  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    020 : db 18+2,00  ;tilenumber, dy, dx
  dw    052 : db 19+2,00  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)   

WMAnimForFortoStarC1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    401 : db 31+2,43  ;tilenumber, dy, dx
  dw    403 : db 31+2,44  ;tilenumber, dy, dx
  dw    498 : db 32+2,43  ;tilenumber, dy, dx
  dw    499 : db 32+2,44  ;tilenumber, dy, dx

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35  ;tilenumber, dy, dx

  db    2                 ;amount of tiles to put (0=end)
  dw    111 : db 31+2,41  ;tilenumber, dy, dx
  dw    254 : db 32+2,41  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    528 : db 31+2,39  ;tilenumber, dy, dx
  dw    529 : db 31+2,40  ;tilenumber, dy, dx
  dw    560 : db 32+2,39  ;tilenumber, dy, dx
  dw    561 : db 32+2,40  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimForIl3toRoysCa:
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 16+2,18  ;tilenumber, dy, dx
  dw    068 : db 16+2,19  ;tilenumber, dy, dx
  dw    068 : db 17+2,18  ;tilenumber, dy, dx
  dw    068 : db 17+2,19  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    068 : db 19+2,18  ;tilenumber, dy, dx
  dw    068 : db 19+2,19  ;tilenumber, dy, dx
  dw    068 : db 20+2,18  ;tilenumber, dy, dx
  dw    068 : db 20+2,19  ;tilenumber, dy, dx

  db    4                 ;amount of tiles to put (0=end)
  dw    021 : db 16+2,18  ;tilenumber, dy, dx
  dw    022 : db 16+2,19  ;tilenumber, dy, dx
  dw    021 : db 17+2,18  ;tilenumber, dy, dx
  dw    022 : db 17+2,19  ;tilenumber, dy, dx
  db    4                 ;amount of tiles to put (0=end)
  dw    021 : db 18+2,18  ;tilenumber, dy, dx
  dw    022 : db 18+2,19  ;tilenumber, dy, dx
  dw    021 : db 19+2,18  ;tilenumber, dy, dx
  dw    022 : db 19+2,19  ;tilenumber, dy, dx
  db    6                 ;amount of tiles to put (0=end)
  dw    021 : db 20+2,18  ;tilenumber, dy, dx
  dw    022 : db 20+2,19  ;tilenumber, dy, dx
  dw    021 : db 21+2,18  ;tilenumber, dy, dx
  dw    022 : db 21+2,19  ;tilenumber, dy, dx
  dw    021 : db 22+2,18  ;tilenumber, dy, dx
  dw    022 : db 22+2,19  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimForIl3toRoysCa_InWorld2:
  db    19                 ;amount of tiles to put (0=end)
  dw    192 : db 31+2,55  ;tilenumber, dy, dx
  dw    201 : db 31+2,56  ;tilenumber, dy, dx
  dw    224 : db 32+2,55  ;tilenumber, dy, dx
  dw    225 : db 32+2,56  ;tilenumber, dy, dx
  dw    224 : db 33+2,55  ;tilenumber, dy, dx
  dw    225 : db 33+2,56  ;tilenumber, dy, dx
  dw    232 : db 34+2,55  ;tilenumber, dy, dx
  dw    233 : db 34+2,56  ;tilenumber, dy, dx

  dw    200 : db 35+2,55  ;tilenumber, dy, dx

  dw    198 : db 35+2,50  ;tilenumber, dy, dx
  dw    230 : db 36+2,50  ;tilenumber, dy, dx
  dw    198 : db 35+2,51  ;tilenumber, dy, dx
  dw    230 : db 36+2,51  ;tilenumber, dy, dx
  dw    198 : db 35+2,52  ;tilenumber, dy, dx
  dw    230 : db 36+2,52  ;tilenumber, dy, dx
  dw    198 : db 35+2,53  ;tilenumber, dy, dx
  dw    230 : db 36+2,53  ;tilenumber, dy, dx
  dw    194 : db 35+2,54  ;tilenumber, dy, dx
  dw    226 : db 36+2,54  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimRoysCatoChoIs1:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    254 : db 36+2,49  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    295 : db 34+2,47  ;tilenumber, dy, dx
  dw    294 : db 34+2,48
  dw    401 : db 35+2,47
  dw    403 : db 35+2,48
  dw    498 : db 36+2,47
  dw    499 : db 36+2,48

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    499 : db 36+2,48

  db    2                 ;amount of tiles to put (0=end)
  dw    224 : db 38+2,47  ;tilenumber, dy, dx
  dw    225 : db 38+2,48  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    224 : db 39+2,47  ;tilenumber, dy, dx
  dw    225 : db 39+2,48  ;tilenumber, dy, dx
  db    2                 ;amount of tiles to put (0=end)
  dw    081 : db 40+2,47  ;tilenumber, dy, dx
  dw    082 : db 40+2,48  ;tilenumber, dy, dx
  db    20                ;amount of tiles to put (0=end)
  dw    169 : db 42+2,46  ;tilenumber, dy, dx
  dw    169 : db 42+2,47  ;tilenumber, dy, dx
  dw    169 : db 42+2,48  ;tilenumber, dy, dx
  dw    169 : db 42+2,49  ;tilenumber, dy, dx

  dw    117 : db 43+2,46  ;tilenumber, dy, dx
  dw    181 : db 43+2,47  ;tilenumber, dy, dx
  dw    181 : db 43+2,48  ;tilenumber, dy, dx
  dw    144 : db 43+2,49  ;tilenumber, dy, dx

  dw    087 : db 44+2,46  ;tilenumber, dy, dx
  dw    217 : db 44+2,47  ;tilenumber, dy, dx
  dw    218 : db 44+2,48  ;tilenumber, dy, dx
  dw    107 : db 44+2,49  ;tilenumber, dy, dx

  dw    119 : db 45+2,46  ;tilenumber, dy, dx
  dw    249 : db 45+2,47  ;tilenumber, dy, dx
  dw    250 : db 45+2,48  ;tilenumber, dy, dx
  dw    107 : db 45+2,49  ;tilenumber, dy, dx

  dw    070 : db 46+2,46  ;tilenumber, dy, dx
  dw    137 : db 46+2,47  ;tilenumber, dy, dx
  dw    138 : db 46+2,48  ;tilenumber, dy, dx
  dw    139 : db 46+2,49  ;tilenumber, dy, dx

  db    20                ;amount of tiles to put (0=end)
  dw    169 : db 43+2,46  ;tilenumber, dy, dx
  dw    169 : db 43+2,47  ;tilenumber, dy, dx
  dw    169 : db 43+2,48  ;tilenumber, dy, dx
  dw    169 : db 43+2,49  ;tilenumber, dy, dx

  dw    117 : db 44+2,46  ;tilenumber, dy, dx
  dw    181 : db 44+2,47  ;tilenumber, dy, dx
  dw    181 : db 44+2,48  ;tilenumber, dy, dx
  dw    144 : db 44+2,49  ;tilenumber, dy, dx

  dw    087 : db 45+2,46  ;tilenumber, dy, dx
  dw    217 : db 45+2,47  ;tilenumber, dy, dx
  dw    218 : db 45+2,48  ;tilenumber, dy, dx
  dw    107 : db 45+2,49  ;tilenumber, dy, dx

  dw    119 : db 46+2,46  ;tilenumber, dy, dx
  dw    249 : db 46+2,47  ;tilenumber, dy, dx
  dw    250 : db 46+2,48  ;tilenumber, dy, dx
  dw    107 : db 46+2,49  ;tilenumber, dy, dx

  dw    070 : db 47+2,46  ;tilenumber, dy, dx
  dw    137 : db 47+2,47  ;tilenumber, dy, dx
  dw    138 : db 47+2,48  ;tilenumber, dy, dx
  dw    139 : db 47+2,49  ;tilenumber, dy, dx

  db    16                ;amount of tiles to put (0=end)
  dw    169 : db 44+2,46  ;tilenumber, dy, dx
  dw    169 : db 44+2,47  ;tilenumber, dy, dx
  dw    169 : db 44+2,48  ;tilenumber, dy, dx
  dw    169 : db 44+2,49  ;tilenumber, dy, dx

  dw    176 : db 45+2,46  ;tilenumber, dy, dx
  dw    007 : db 45+2,47  ;tilenumber, dy, dx
  dw    007 : db 45+2,48  ;tilenumber, dy, dx
  dw    175 : db 45+2,49  ;tilenumber, dy, dx

  dw    087 : db 46+2,46  ;tilenumber, dy, dx
  dw    217 : db 46+2,47  ;tilenumber, dy, dx
  dw    218 : db 46+2,48  ;tilenumber, dy, dx
  dw    107 : db 46+2,49  ;tilenumber, dy, dx

  dw    119 : db 47+2,46  ;tilenumber, dy, dx
  dw    249 : db 47+2,47  ;tilenumber, dy, dx
  dw    250 : db 47+2,48  ;tilenumber, dy, dx
  dw    107 : db 47+2,49  ;tilenumber, dy, dx

  db    12                ;amount of tiles to put (0=end)
  dw    165 : db 45+2,46  ;tilenumber, dy, dx
  dw    165 : db 45+2,47  ;tilenumber, dy, dx
  dw    165 : db 45+2,48  ;tilenumber, dy, dx
  dw    165 : db 45+2,49  ;tilenumber, dy, dx

  dw    176 : db 46+2,46  ;tilenumber, dy, dx
  dw    007 : db 46+2,47  ;tilenumber, dy, dx
  dw    007 : db 46+2,48  ;tilenumber, dy, dx
  dw    175 : db 46+2,49  ;tilenumber, dy, dx

  dw    087 : db 47+2,46  ;tilenumber, dy, dx
  dw    217 : db 47+2,47  ;tilenumber, dy, dx
  dw    218 : db 47+2,48  ;tilenumber, dy, dx
  dw    107 : db 47+2,49  ;tilenumber, dy, dx

  db    12                ;amount of tiles to put (0=end)
  dw    165 : db 45+2,46  ;tilenumber, dy, dx
  dw    165 : db 45+2,47  ;tilenumber, dy, dx
  dw    165 : db 45+2,48  ;tilenumber, dy, dx
  dw    165 : db 45+2,49  ;tilenumber, dy, dx

  dw    000 : db 46+2,46  ;tilenumber, dy, dx
  dw    000 : db 46+2,47  ;tilenumber, dy, dx
  dw    000 : db 46+2,48  ;tilenumber, dy, dx
  dw    000 : db 46+2,49  ;tilenumber, dy, dx

  dw    000 : db 47+2,46  ;tilenumber, dy, dx
  dw    000 : db 47+2,47  ;tilenumber, dy, dx
  dw    000 : db 47+2,48  ;tilenumber, dy, dx
  dw    000 : db 47+2,49  ;tilenumber, dy, dx

  db    05                ;amount of tiles to put (0=end)
  dw    176 : db 47+2,45  ;tilenumber, dy, dx
  dw    007 : db 47+2,46  ;tilenumber, dy, dx
  dw    381 : db 47+2,47  ;tilenumber, dy, dx
  dw    382 : db 47+2,48  ;tilenumber, dy, dx
  dw    175 : db 47+2,49  ;tilenumber, dy, dx

  db    10                ;amount of tiles to put (0=end)
  dw    176 : db 46+2,45  ;tilenumber, dy, dx
  dw    007 : db 46+2,46  ;tilenumber, dy, dx
  dw    381 : db 46+2,47  ;tilenumber, dy, dx
  dw    382 : db 46+2,48  ;tilenumber, dy, dx
  dw    175 : db 46+2,49  ;tilenumber, dy, dx

  dw    103 : db 47+2,45  ;tilenumber, dy, dx
  dw    106 : db 47+2,46  ;tilenumber, dy, dx
  dw    224 : db 47+2,47  ;tilenumber, dy, dx
  dw    225 : db 47+2,48  ;tilenumber, dy, dx
  dw    107 : db 47+2,49  ;tilenumber, dy, dx

  db    15                ;amount of tiles to put (0=end)
  dw    176 : db 45+2,45  ;tilenumber, dy, dx
  dw    007 : db 45+2,46  ;tilenumber, dy, dx
  dw    381 : db 45+2,47  ;tilenumber, dy, dx
  dw    382 : db 45+2,48  ;tilenumber, dy, dx
  dw    175 : db 45+2,49  ;tilenumber, dy, dx

  dw    103 : db 46+2,45  ;tilenumber, dy, dx
  dw    106 : db 46+2,46  ;tilenumber, dy, dx
  dw    224 : db 46+2,47  ;tilenumber, dy, dx
  dw    225 : db 46+2,48  ;tilenumber, dy, dx
  dw    107 : db 46+2,49  ;tilenumber, dy, dx  

  dw    087 : db 47+2,45  ;tilenumber, dy, dx
  dw    213 : db 47+2,46  ;tilenumber, dy, dx
  dw    372 : db 47+2,47  ;tilenumber, dy, dx
  dw    373 : db 47+2,48  ;tilenumber, dy, dx
  dw    107 : db 47+2,49  ;tilenumber, dy, dx   

  db    20                ;amount of tiles to put (0=end)
  dw    117 : db 44+2,45  ;tilenumber, dy, dx
  dw    181 : db 44+2,46  ;tilenumber, dy, dx
  dw    179 : db 44+2,47  ;tilenumber, dy, dx
  dw    180 : db 44+2,48  ;tilenumber, dy, dx
  dw    115 : db 44+2,49  ;tilenumber, dy, dx

  dw    103 : db 45+2,45  ;tilenumber, dy, dx
  dw    106 : db 45+2,46  ;tilenumber, dy, dx
  dw    224 : db 45+2,47  ;tilenumber, dy, dx
  dw    225 : db 45+2,48  ;tilenumber, dy, dx
  dw    107 : db 45+2,49  ;tilenumber, dy, dx  

  dw    087 : db 46+2,45  ;tilenumber, dy, dx
  dw    213 : db 46+2,46  ;tilenumber, dy, dx
  dw    372 : db 46+2,47  ;tilenumber, dy, dx
  dw    373 : db 46+2,48  ;tilenumber, dy, dx
  dw    107 : db 46+2,49  ;tilenumber, dy, dx       

  dw    119 : db 47+2,45  ;tilenumber, dy, dx
  dw    245 : db 47+2,46  ;tilenumber, dy, dx
  dw    404 : db 47+2,47  ;tilenumber, dy, dx
  dw    405 : db 47+2,48  ;tilenumber, dy, dx
  dw    107 : db 47+2,49  ;tilenumber, dy, dx       

  db    25                ;amount of tiles to put (0=end)
  dw    117 : db 43+2,45  ;tilenumber, dy, dx
  dw    181 : db 43+2,46  ;tilenumber, dy, dx
  dw    179 : db 43+2,47  ;tilenumber, dy, dx
  dw    180 : db 43+2,48  ;tilenumber, dy, dx
  dw    115 : db 43+2,49  ;tilenumber, dy, dx

  dw    103 : db 44+2,45  ;tilenumber, dy, dx
  dw    106 : db 44+2,46  ;tilenumber, dy, dx
  dw    224 : db 44+2,47  ;tilenumber, dy, dx
  dw    225 : db 44+2,48  ;tilenumber, dy, dx
  dw    107 : db 44+2,49  ;tilenumber, dy, dx  

  dw    087 : db 45+2,45  ;tilenumber, dy, dx
  dw    213 : db 45+2,46  ;tilenumber, dy, dx
  dw    372 : db 45+2,47  ;tilenumber, dy, dx
  dw    373 : db 45+2,48  ;tilenumber, dy, dx
  dw    107 : db 45+2,49  ;tilenumber, dy, dx       

  dw    119 : db 46+2,45  ;tilenumber, dy, dx
  dw    245 : db 46+2,46  ;tilenumber, dy, dx
  dw    404 : db 46+2,47  ;tilenumber, dy, dx
  dw    405 : db 46+2,48  ;tilenumber, dy, dx
  dw    107 : db 46+2,49  ;tilenumber, dy, dx   

  dw    070 : db 47+2,45  ;tilenumber, dy, dx
  dw    137 : db 47+2,46  ;tilenumber, dy, dx
  dw    137 : db 47+2,47  ;tilenumber, dy, dx
  dw    137 : db 47+2,48  ;tilenumber, dy, dx
  dw    139 : db 47+2,49  ;tilenumber, dy, dx    
  
  db    30                ;amount of tiles to put (0=end)
  dw    117 : db 42+2,45  ;tilenumber, dy, dx
  dw    181 : db 42+2,46  ;tilenumber, dy, dx
  dw    179 : db 42+2,47  ;tilenumber, dy, dx
  dw    180 : db 42+2,48  ;tilenumber, dy, dx
  dw    115 : db 42+2,49  ;tilenumber, dy, dx

  dw    103 : db 43+2,45  ;tilenumber, dy, dx
  dw    106 : db 43+2,46  ;tilenumber, dy, dx
  dw    224 : db 43+2,47  ;tilenumber, dy, dx
  dw    225 : db 43+2,48  ;tilenumber, dy, dx
  dw    107 : db 43+2,49  ;tilenumber, dy, dx  

  dw    087 : db 44+2,45  ;tilenumber, dy, dx
  dw    213 : db 44+2,46  ;tilenumber, dy, dx
  dw    372 : db 44+2,47  ;tilenumber, dy, dx
  dw    373 : db 44+2,48  ;tilenumber, dy, dx
  dw    107 : db 44+2,49  ;tilenumber, dy, dx       

  dw    119 : db 45+2,45  ;tilenumber, dy, dx
  dw    245 : db 45+2,46  ;tilenumber, dy, dx
  dw    404 : db 45+2,47  ;tilenumber, dy, dx
  dw    405 : db 45+2,48  ;tilenumber, dy, dx
  dw    107 : db 45+2,49  ;tilenumber, dy, dx   

  dw    070 : db 46+2,45  ;tilenumber, dy, dx
  dw    137 : db 46+2,46  ;tilenumber, dy, dx
  dw    137 : db 46+2,47  ;tilenumber, dy, dx
  dw    137 : db 46+2,48  ;tilenumber, dy, dx
  dw    139 : db 46+2,49  ;tilenumber, dy, dx   

  dw    102 : db 47+2,45  ;tilenumber, dy, dx
  dw    102 : db 47+2,46  ;tilenumber, dy, dx
  dw    169 : db 47+2,47  ;tilenumber, dy, dx
  dw    170 : db 47+2,48  ;tilenumber, dy, dx
  dw    170 : db 47+2,49  ;tilenumber, dy, dx 

  db    49                ;amount of tiles to put (0=end)
  dw    117 : db 41+2,45  ;tilenumber, dy, dx
  dw    181 : db 41+2,46  ;tilenumber, dy, dx
  dw    179 : db 41+2,47  ;tilenumber, dy, dx
  dw    180 : db 41+2,48  ;tilenumber, dy, dx
  dw    115 : db 41+2,49  ;tilenumber, dy, dx

  dw    103 : db 42+2,45  ;tilenumber, dy, dx
  dw    106 : db 42+2,46  ;tilenumber, dy, dx
  dw    224 : db 42+2,47  ;tilenumber, dy, dx
  dw    225 : db 42+2,48  ;tilenumber, dy, dx
  dw    107 : db 42+2,49  ;tilenumber, dy, dx  

  dw    087 : db 43+2,45  ;tilenumber, dy, dx
  dw    213 : db 43+2,46  ;tilenumber, dy, dx
  dw    372 : db 43+2,47  ;tilenumber, dy, dx
  dw    373 : db 43+2,48  ;tilenumber, dy, dx
  dw    107 : db 43+2,49  ;tilenumber, dy, dx       

  dw    119 : db 44+2,45  ;tilenumber, dy, dx
  dw    245 : db 44+2,46  ;tilenumber, dy, dx
  dw    404 : db 44+2,47  ;tilenumber, dy, dx
  dw    405 : db 44+2,48  ;tilenumber, dy, dx
  dw    107 : db 44+2,49  ;tilenumber, dy, dx   

  dw    070 : db 45+2,45  ;tilenumber, dy, dx
  dw    137 : db 45+2,46  ;tilenumber, dy, dx
  dw    137 : db 45+2,47  ;tilenumber, dy, dx
  dw    137 : db 45+2,48  ;tilenumber, dy, dx
  dw    139 : db 45+2,49  ;tilenumber, dy, dx   

  dw    102 : db 46+2,45  ;tilenumber, dy, dx
  dw    102 : db 46+2,46  ;tilenumber, dy, dx
  dw    169 : db 46+2,47  ;tilenumber, dy, dx
  dw    170 : db 46+2,48  ;tilenumber, dy, dx
  dw    170 : db 46+2,49  ;tilenumber, dy, dx   

  dw    102 : db 47+2,45  ;tilenumber, dy, dx
  dw    102 : db 47+2,46  ;tilenumber, dy, dx
  dw    169 : db 47+2,47  ;tilenumber, dy, dx
  dw    170 : db 47+2,48  ;tilenumber, dy, dx
  dw    170 : db 47+2,49  ;tilenumber, dy, dx    

  dw    102 : db 48+2,45  ;tilenumber, dy, dx
  dw    102 : db 48+2,46  ;tilenumber, dy, dx
  dw    169 : db 48+2,47  ;tilenumber, dy, dx
  dw    170 : db 48+2,48  ;tilenumber, dy, dx
  dw    170 : db 48+2,49  ;tilenumber, dy, dx   
  dw    110 : db 48+2,50  ;tilenumber, dy, dx   

  dw    134 : db 49+2,45  ;tilenumber, dy, dx
  dw    102 : db 49+2,46  ;tilenumber, dy, dx
  dw    169 : db 49+2,47  ;tilenumber, dy, dx
  dw    170 : db 49+2,48  ;tilenumber, dy, dx
  dw    171 : db 49+2,49  ;tilenumber, dy, dx             
  dw    142 : db 49+2,50  ;tilenumber, dy, dx     

  dw    133 : db 50+2,45  ;tilenumber, dy, dx
  dw    165 : db 50+2,46  ;tilenumber, dy, dx          
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs1toChoGho:
  db    02                ;amount of tiles to put (0=end)
  dw    198 : db 43+2,46  ;tilenumber, dy, dx
  dw    230 : db 44+2,46  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    410 : db 43+2,45  ;tilenumber, dy, dx
  dw    442 : db 44+2,45  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    664 : db 43+2,44  ;tilenumber, dy, dx
  dw    696 : db 44+2,44  ;tilenumber, dy, dx  
  db    02                ;amount of tiles to put (0=end)
  dw    375 : db 43+2,43  ;tilenumber, dy, dx
  dw    407 : db 44+2,43  ;tilenumber, dy, dx       
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoGhotoChoIs2:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 45+2,41  ;tilenumber, dy, dx
  dw    331 : db 45+2,42  ;tilenumber, dy, dx
  dw    331 : db 46+2,41  ;tilenumber, dy, dx
  dw    331 : db 46+2,42  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 47+2,41  ;tilenumber, dy, dx
  dw    331 : db 47+2,42  ;tilenumber, dy, dx
  dw    331 : db 48+2,41  ;tilenumber, dy, dx
  dw    331 : db 48+2,42  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 49+2,41  ;tilenumber, dy, dx
  dw    331 : db 49+2,42  ;tilenumber, dy, dx
  dw    331 : db 50+2,41  ;tilenumber, dy, dx
  dw    331 : db 50+2,42  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 51+2,41  ;tilenumber, dy, dx
  dw    331 : db 51+2,42  ;tilenumber, dy, dx
  dw    271 : db 52+2,41  ;tilenumber, dy, dx
  dw    270 : db 52+2,42  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 52+2,42  ;tilenumber, dy, dx
  dw    331 : db 52+2,43  ;tilenumber, dy, dx
  dw    331 : db 53+2,42  ;tilenumber, dy, dx
  dw    270 : db 53+2,43  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    592 : db 53+2,41  ;tilenumber, dy, dx
  dw    631 : db 53+2,42  ;tilenumber, dy, dx
  dw    662 : db 54+2,41  ;tilenumber, dy, dx
  dw    663 : db 54+2,42  ;tilenumber, dy, dx         
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs2toTubeK1:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 56+2,42  ;tilenumber, dy, dx
  dw    302 : db 56+2,43  ;tilenumber, dy, dx
  dw    331 : db 57+2,42  ;tilenumber, dy, dx
  dw    331 : db 57+2,43  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    303 : db 56+2,44  ;tilenumber, dy, dx
  dw    331 : db 56+2,45  ;tilenumber, dy, dx
  dw    331 : db 57+2,44  ;tilenumber, dy, dx
  dw    331 : db 57+2,45  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 52+2,45  ;tilenumber, dy, dx
  dw    331 : db 52+2,46  ;tilenumber, dy, dx
  dw    331 : db 53+2,45  ;tilenumber, dy, dx
  dw    270 : db 53+2,46  ;tilenumber, dy, dx        
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 49+2,39  ;tilenumber, dy, dx
  dw    331 : db 49+2,40  ;tilenumber, dy, dx
  dw    271 : db 50+2,39  ;tilenumber, dy, dx
  dw    331 : db 50+2,40  ;tilenumber, dy, dx   
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 47+2,37  ;tilenumber, dy, dx
  dw    331 : db 47+2,38  ;tilenumber, dy, dx
  dw    271 : db 48+2,37  ;tilenumber, dy, dx
  dw    331 : db 48+2,38  ;tilenumber, dy, dx   
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs2toChoIs3:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 53+2,39  ;tilenumber, dy, dx
  dw    302 : db 53+2,40  ;tilenumber, dy, dx
  dw    331 : db 54+2,39  ;tilenumber, dy, dx
  dw    331 : db 54+2,40  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    630 : db 53+2,37  ;tilenumber, dy, dx
  dw    631 : db 53+2,38  ;tilenumber, dy, dx
  dw    662 : db 54+2,37  ;tilenumber, dy, dx
  dw    663 : db 54+2,38  ;tilenumber, dy, dx  
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs3toCircle:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 55+2,37  ;tilenumber, dy, dx
  dw    331 : db 55+2,38  ;tilenumber, dy, dx
  dw    331 : db 56+2,37  ;tilenumber, dy, dx
  dw    331 : db 56+2,38  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 57+2,36  ;tilenumber, dy, dx
  dw    331 : db 57+2,37  ;tilenumber, dy, dx
  dw    271 : db 58+2,36  ;tilenumber, dy, dx
  dw    270 : db 58+2,37  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 57+2,34  ;tilenumber, dy, dx
  dw    331 : db 57+2,35  ;tilenumber, dy, dx
  dw    271 : db 58+2,34  ;tilenumber, dy, dx
  dw    270 : db 58+2,35  ;tilenumber, dy, dx

  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 55+2,33  ;tilenumber, dy, dx
  dw    331 : db 55+2,34  ;tilenumber, dy, dx
  dw    271 : db 56+2,33  ;tilenumber, dy, dx
  dw    331 : db 56+2,34  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 53+2,33  ;tilenumber, dy, dx
  dw    331 : db 53+2,34  ;tilenumber, dy, dx
  dw    331 : db 54+2,33  ;tilenumber, dy, dx
  dw    331 : db 54+2,34  ;tilenumber, dy, dx  
  db    04                ;amount of tiles to put (0=end)
  dw    303 : db 51+2,35  ;tilenumber, dy, dx
  dw    331 : db 51+2,36  ;tilenumber, dy, dx
  dw    331 : db 52+2,35  ;tilenumber, dy, dx
  dw    331 : db 52+2,36  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 51+2,37  ;tilenumber, dy, dx
  dw    302 : db 51+2,38  ;tilenumber, dy, dx
  dw    331 : db 52+2,37  ;tilenumber, dy, dx
  dw    331 : db 52+2,38  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs3toChoFor:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 53+2,35  ;tilenumber, dy, dx
  dw    331 : db 53+2,36  ;tilenumber, dy, dx
  dw    331 : db 54+2,35  ;tilenumber, dy, dx
  dw    331 : db 54+2,36  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 53+2,33  ;tilenumber, dy, dx
  dw    331 : db 53+2,34  ;tilenumber, dy, dx
  dw    331 : db 54+2,33  ;tilenumber, dy, dx
  dw    331 : db 54+2,34  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    303 : db 53+2,31  ;tilenumber, dy, dx
  dw    331 : db 53+2,32  ;tilenumber, dy, dx
  dw    331 : db 54+2,31  ;tilenumber, dy, dx
  dw    331 : db 54+2,32  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoFortoChoIs4:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    466 : db 53+2,29  ;tilenumber, dy, dx
  dw    434 : db 53+2,30
  dw    498 : db 54+2,29
  dw    499 : db 54+2,30

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35
  
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 55+2,29  ;tilenumber, dy, dx
  dw    331 : db 55+2,30  ;tilenumber, dy, dx
  dw    331 : db 56+2,29  ;tilenumber, dy, dx
  dw    331 : db 56+2,30  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    608 : db 57+2,29  ;tilenumber, dy, dx
  dw    609 : db 57+2,30  ;tilenumber, dy, dx
  dw    640 : db 58+2,29  ;tilenumber, dy, dx
  dw    641 : db 58+2,30  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs4toChoIs5:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 57+2,27  ;tilenumber, dy, dx
  dw    331 : db 57+2,28  ;tilenumber, dy, dx
  dw    331 : db 58+2,27  ;tilenumber, dy, dx
  dw    331 : db 58+2,28  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 57+2,25  ;tilenumber, dy, dx
  dw    331 : db 57+2,26  ;tilenumber, dy, dx
  dw    331 : db 58+2,25  ;tilenumber, dy, dx
  dw    331 : db 58+2,26  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    628 : db 57+2,23  ;tilenumber, dy, dx
  dw    629 : db 57+2,24  ;tilenumber, dy, dx
  dw    660 : db 58+2,23  ;tilenumber, dy, dx
  dw    661 : db 58+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimChoIs5toWendCa:
  db    04                ;amount of tiles to put (0=end)
  dw    331 : db 55+2,23  ;tilenumber, dy, dx
  dw    331 : db 55+2,24  ;tilenumber, dy, dx
  dw    331 : db 56+2,23  ;tilenumber, dy, dx
  dw    331 : db 56+2,24  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimWendCatoSunkGh:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    254 : db 52+2,23  ;tilenumber, dy, dx
  dw    254 : db 52+2,24
  dw    466 : db 53+2,23
  dw    467 : db 53+2,24
  dw    498 : db 54+2,23
  dw    499 : db 54+2,24

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    000 : db 35+2,35
  
  db    02                ;amount of tiles to put (0=end)
  dw    438 : db 52+2,23  ;tilenumber, dy, dx
  dw    439 : db 52+2,24  ;tilenumber, dy, dx
  db    02                ;amount of tiles to put (0=end)
  dw    472 : db 51+2,23  ;tilenumber, dy, dx
  dw    473 : db 51+2,24  ;tilenumber, dy, dx

  db    01                ;amount of tiles to put (0=end)
  dw    152 : db 50+2,24  ;tilenumber, dy, dx
  db    01                ;amount of tiles to put (0=end)
  dw    151 : db 49+2,23  ;tilenumber, dy, dx
  db    01                ;amount of tiles to put (0=end)
  dw    152 : db 48+2,24  ;tilenumber, dy, dx
  db    01                ;amount of tiles to put (0=end)
  dw    151 : db 47+2,24  ;tilenumber, dy, dx
  db    01                ;amount of tiles to put (0=end)
  dw    088 : db 46+2,25  ;tilenumber, dy, dx
  db    01                ;amount of tiles to put (0=end)
  dw    120 : db 46+2,26  ;tilenumber, dy, dx
  db    04                ;amount of tiles to put (0=end)
  dw    618 : db 45+2,27  ;tilenumber, dy, dx
  dw    619 : db 45+2,28  ;tilenumber, dy, dx
  dw    650 : db 46+2,27  ;tilenumber, dy, dx
  dw    651 : db 46+2,28  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimSunkGhtoValBo1:
  db    04                ;amount of tiles to put (0=end)
  dw    673 : db 43+2,26  ;tilenumber, dy, dx
  dw    674 : db 43+2,27  ;tilenumber, dy, dx
  dw    675 : db 43+2,28  ;tilenumber, dy, dx
  dw    676 : db 43+2,29  ;tilenumber, dy, dx

  db    10                ;amount of tiles to put (0=end)
  dw    673 : db 42+2,26  ;tilenumber, dy, dx
  dw    674 : db 42+2,27  ;tilenumber, dy, dx
  dw    675 : db 42+2,28  ;tilenumber, dy, dx
  dw    676 : db 42+2,29  ;tilenumber, dy, dx

  dw    704 : db 43+2,25  ;tilenumber, dy, dx
  dw    705 : db 43+2,26  ;tilenumber, dy, dx
  dw    706 : db 43+2,27  ;tilenumber, dy, dx
  dw    707 : db 43+2,28  ;tilenumber, dy, dx
  dw    708 : db 43+2,29  ;tilenumber, dy, dx
  dw    709 : db 43+2,30  ;tilenumber, dy, dx

  db    16                ;amount of tiles to put (0=end)
  dw    673 : db 41+2,26  ;tilenumber, dy, dx
  dw    674 : db 41+2,27  ;tilenumber, dy, dx
  dw    675 : db 41+2,28  ;tilenumber, dy, dx
  dw    676 : db 41+2,29  ;tilenumber, dy, dx

  dw    704 : db 42+2,25  ;tilenumber, dy, dx
  dw    705 : db 42+2,26  ;tilenumber, dy, dx
  dw    706 : db 42+2,27  ;tilenumber, dy, dx
  dw    707 : db 42+2,28  ;tilenumber, dy, dx
  dw    708 : db 42+2,29  ;tilenumber, dy, dx
  dw    709 : db 42+2,30  ;tilenumber, dy, dx

  dw    678 : db 43+2,25  ;tilenumber, dy, dx
  dw    679 : db 43+2,26  ;tilenumber, dy, dx
  dw    680 : db 43+2,27  ;tilenumber, dy, dx
  dw    681 : db 43+2,28  ;tilenumber, dy, dx
  dw    682 : db 43+2,29  ;tilenumber, dy, dx
  dw    683 : db 43+2,30  ;tilenumber, dy, dx  

  db    22                ;amount of tiles to put (0=end)
  dw    673 : db 40+2,26  ;tilenumber, dy, dx
  dw    674 : db 40+2,27  ;tilenumber, dy, dx
  dw    675 : db 40+2,28  ;tilenumber, dy, dx
  dw    676 : db 40+2,29  ;tilenumber, dy, dx

  dw    704 : db 41+2,25  ;tilenumber, dy, dx
  dw    705 : db 41+2,26  ;tilenumber, dy, dx
  dw    706 : db 41+2,27  ;tilenumber, dy, dx
  dw    707 : db 41+2,28  ;tilenumber, dy, dx
  dw    708 : db 41+2,29  ;tilenumber, dy, dx
  dw    709 : db 41+2,30  ;tilenumber, dy, dx

  dw    678 : db 42+2,25  ;tilenumber, dy, dx
  dw    679 : db 42+2,26  ;tilenumber, dy, dx
  dw    680 : db 42+2,27  ;tilenumber, dy, dx
  dw    681 : db 42+2,28  ;tilenumber, dy, dx
  dw    682 : db 42+2,29  ;tilenumber, dy, dx
  dw    683 : db 42+2,30  ;tilenumber, dy, dx    

  dw    710 : db 43+2,25  ;tilenumber, dy, dx
  dw    711 : db 43+2,26  ;tilenumber, dy, dx
  dw    712 : db 43+2,27  ;tilenumber, dy, dx
  dw    713 : db 43+2,28  ;tilenumber, dy, dx
  dw    714 : db 43+2,29  ;tilenumber, dy, dx
  dw    715 : db 43+2,30  ;tilenumber, dy, dx  

  db    28                ;amount of tiles to put (0=end)
  dw    673 : db 39+2,26  ;tilenumber, dy, dx
  dw    674 : db 39+2,27  ;tilenumber, dy, dx
  dw    675 : db 39+2,28  ;tilenumber, dy, dx
  dw    676 : db 39+2,29  ;tilenumber, dy, dx

  dw    704 : db 40+2,25  ;tilenumber, dy, dx
  dw    705 : db 40+2,26  ;tilenumber, dy, dx
  dw    706 : db 40+2,27  ;tilenumber, dy, dx
  dw    707 : db 40+2,28  ;tilenumber, dy, dx
  dw    708 : db 40+2,29  ;tilenumber, dy, dx
  dw    709 : db 40+2,30  ;tilenumber, dy, dx

  dw    678 : db 41+2,25  ;tilenumber, dy, dx
  dw    679 : db 41+2,26  ;tilenumber, dy, dx
  dw    680 : db 41+2,27  ;tilenumber, dy, dx
  dw    681 : db 41+2,28  ;tilenumber, dy, dx
  dw    682 : db 41+2,29  ;tilenumber, dy, dx
  dw    683 : db 41+2,30  ;tilenumber, dy, dx    

  dw    710 : db 42+2,25  ;tilenumber, dy, dx
  dw    711 : db 42+2,26  ;tilenumber, dy, dx
  dw    712 : db 42+2,27  ;tilenumber, dy, dx
  dw    713 : db 42+2,28  ;tilenumber, dy, dx
  dw    714 : db 42+2,29  ;tilenumber, dy, dx
  dw    715 : db 42+2,30  ;tilenumber, dy, dx   

  dw    684 : db 43+2,25  ;tilenumber, dy, dx
  dw    685 : db 43+2,26  ;tilenumber, dy, dx
  dw    686 : db 43+2,27  ;tilenumber, dy, dx
  dw    687 : db 43+2,28  ;tilenumber, dy, dx
  dw    688 : db 43+2,29  ;tilenumber, dy, dx
  dw    689 : db 43+2,30  ;tilenumber, dy, dx    

  db    34                ;amount of tiles to put (0=end)
  dw    673 : db 38+2,26  ;tilenumber, dy, dx
  dw    674 : db 38+2,27  ;tilenumber, dy, dx
  dw    675 : db 38+2,28  ;tilenumber, dy, dx
  dw    676 : db 38+2,29  ;tilenumber, dy, dx

  dw    704 : db 39+2,25  ;tilenumber, dy, dx
  dw    705 : db 39+2,26  ;tilenumber, dy, dx
  dw    706 : db 39+2,27  ;tilenumber, dy, dx
  dw    707 : db 39+2,28  ;tilenumber, dy, dx
  dw    708 : db 39+2,29  ;tilenumber, dy, dx
  dw    709 : db 39+2,30  ;tilenumber, dy, dx

  dw    678 : db 40+2,25  ;tilenumber, dy, dx
  dw    679 : db 40+2,26  ;tilenumber, dy, dx
  dw    680 : db 40+2,27  ;tilenumber, dy, dx
  dw    681 : db 40+2,28  ;tilenumber, dy, dx
  dw    682 : db 40+2,29  ;tilenumber, dy, dx
  dw    683 : db 40+2,30  ;tilenumber, dy, dx    

  dw    710 : db 41+2,25  ;tilenumber, dy, dx
  dw    711 : db 41+2,26  ;tilenumber, dy, dx
  dw    712 : db 41+2,27  ;tilenumber, dy, dx
  dw    713 : db 41+2,28  ;tilenumber, dy, dx
  dw    714 : db 41+2,29  ;tilenumber, dy, dx
  dw    715 : db 41+2,30  ;tilenumber, dy, dx   

  dw    684 : db 42+2,25  ;tilenumber, dy, dx
  dw    685 : db 42+2,26  ;tilenumber, dy, dx
  dw    686 : db 42+2,27  ;tilenumber, dy, dx
  dw    687 : db 42+2,28  ;tilenumber, dy, dx
  dw    688 : db 42+2,29  ;tilenumber, dy, dx
  dw    689 : db 42+2,30  ;tilenumber, dy, dx     

  dw    716 : db 43+2,25  ;tilenumber, dy, dx
  dw    717 : db 43+2,26  ;tilenumber, dy, dx
  dw    718 : db 43+2,27  ;tilenumber, dy, dx
  dw    719 : db 43+2,28  ;tilenumber, dy, dx
  dw    720 : db 43+2,29  ;tilenumber, dy, dx
  dw    712 : db 43+2,30  ;tilenumber, dy, dx   
  
  db    40                ;amount of tiles to put (0=end)
  dw    673 : db 37+2,26  ;tilenumber, dy, dx
  dw    674 : db 37+2,27  ;tilenumber, dy, dx
  dw    675 : db 37+2,28  ;tilenumber, dy, dx
  dw    676 : db 37+2,29  ;tilenumber, dy, dx

  dw    704 : db 38+2,25  ;tilenumber, dy, dx
  dw    705 : db 38+2,26  ;tilenumber, dy, dx
  dw    706 : db 38+2,27  ;tilenumber, dy, dx
  dw    707 : db 38+2,28  ;tilenumber, dy, dx
  dw    708 : db 38+2,29  ;tilenumber, dy, dx
  dw    709 : db 38+2,30  ;tilenumber, dy, dx

  dw    678 : db 39+2,25  ;tilenumber, dy, dx
  dw    679 : db 39+2,26  ;tilenumber, dy, dx
  dw    680 : db 39+2,27  ;tilenumber, dy, dx
  dw    681 : db 39+2,28  ;tilenumber, dy, dx
  dw    682 : db 39+2,29  ;tilenumber, dy, dx
  dw    683 : db 39+2,30  ;tilenumber, dy, dx    

  dw    710 : db 40+2,25  ;tilenumber, dy, dx
  dw    711 : db 40+2,26  ;tilenumber, dy, dx
  dw    712 : db 40+2,27  ;tilenumber, dy, dx
  dw    713 : db 40+2,28  ;tilenumber, dy, dx
  dw    714 : db 40+2,29  ;tilenumber, dy, dx
  dw    715 : db 40+2,30  ;tilenumber, dy, dx   

  dw    684 : db 41+2,25  ;tilenumber, dy, dx
  dw    685 : db 41+2,26  ;tilenumber, dy, dx
  dw    686 : db 41+2,27  ;tilenumber, dy, dx
  dw    687 : db 41+2,28  ;tilenumber, dy, dx
  dw    688 : db 41+2,29  ;tilenumber, dy, dx
  dw    689 : db 41+2,30  ;tilenumber, dy, dx     

  dw    716 : db 42+2,25  ;tilenumber, dy, dx
  dw    717 : db 42+2,26  ;tilenumber, dy, dx
  dw    718 : db 42+2,27  ;tilenumber, dy, dx
  dw    719 : db 42+2,28  ;tilenumber, dy, dx
  dw    720 : db 42+2,29  ;tilenumber, dy, dx
  dw    712 : db 42+2,30  ;tilenumber, dy, dx     

  dw    690 : db 43+2,25  ;tilenumber, dy, dx
  dw    691 : db 43+2,26  ;tilenumber, dy, dx
  dw    692 : db 43+2,27  ;tilenumber, dy, dx
  dw    693 : db 43+2,28  ;tilenumber, dy, dx
  dw    694 : db 43+2,29  ;tilenumber, dy, dx
  dw    695 : db 43+2,30  ;tilenumber, dy, dx     
  db    0                 ;amount of tiles to put (0=end)

WMAnimSunkGhtoValBo1_InWorld7:
  db    16                ;amount of tiles to put (0=end)
  dw    342-1 : db 15+2,24  ;tilenumber, dy, dx
  dw    343-1 : db 15+2,25  ;tilenumber, dy, dx
  dw    374-1 : db 16+2,24  ;tilenumber, dy, dx
  dw    375-1 : db 16+2,25  ;tilenumber, dy, dx

  dw    153-1 : db 15+2,26  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,26  ;tilenumber, dy, dx
  dw    153-1 : db 15+2,27  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,27  ;tilenumber, dy, dx

  dw    154-1 : db 15+2,28  ;tilenumber, dy, dx
  dw    186-1 : db 16+2,28  ;tilenumber, dy, dx
  dw    155-1 : db 16+2,29  ;tilenumber, dy, dx
  dw    187-1 : db 17+2,29  ;tilenumber, dy, dx
  dw    154-1 : db 16+2,30  ;tilenumber, dy, dx
  dw    186-1 : db 17+2,30  ;tilenumber, dy, dx
  dw    155-1 : db 17+2,31  ;tilenumber, dy, dx
  dw    187-1 : db 18+2,31  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimValBo1toValBo2:
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,23  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,23  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,22  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,22  ;tilenumber, dy, dx
  db    4                ;amount of tiles to put (0=end)
  dw    073-1 : db 15+2,20  ;tilenumber, dy, dx
  dw    074-1 : db 15+2,21  ;tilenumber, dy, dx
  dw    105-1 : db 16+2,20  ;tilenumber, dy, dx
  dw    106-1 : db 16+2,21  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimValBo2toValFor:
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 14+2,20  ;tilenumber, dy, dx
  dw    094-1 : db 14+2,21  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)
  
WMAnimValFortoBackDo:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    093-1 : db 14+2,20  ;tilenumber, dy, dx

  db    4 + %1000'0000      ;explosion sfx
  dw    363-1 : db 11+2,20  ;tilenumber, dy, dx
  dw    364-1 : db 11+2,21
  dw    357-1 : db 12+2,20
  dw    358-1 : db 12+2,21

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    093-1 : db 14+2,20  ;tilenumber, dy, dx
  
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 10+2,20  ;tilenumber,xcz dy, dx
  dw    094-1 : db 10+2,21  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 09+2,20  ;tilenumber, dy, dx
  dw    094-1 : db 09+2,21  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 08+2,20  ;tilenumber, dy, dx
  dw    094-1 : db 08+2,21  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)  

WMAnimValBo2toValGho:
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,19  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,19  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,18  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,18  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimValGhotoLarrCa:
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 14+2,16  ;tilenumber, dy, dx
  dw    094-1 : db 14+2,17  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimValGhotoValBo3:
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,15  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,15  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,14  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,14  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,13  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,13  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 15+2,12  ;tilenumber, dy, dx
  dw    185-1 : db 16+2,12  ;tilenumber, dy, dx
  db    4                ;amount of tiles to put (0=end)
  dw    342-1 : db 15+2,10  ;tilenumber, dy, dx
  dw    343-1 : db 15+2,11  ;tilenumber, dy, dx
  dw    374-1 : db 16+2,10  ;tilenumber, dy, dx
  dw    375-1 : db 16+2,11  ;tilenumber, dy, dx      
  db    0                 ;amount of tiles to put (0=end)

WMAnimValBo3toValBo4:
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 14+2,10  ;tilenumber, dy, dx
  dw    094-1 : db 14+2,11  ;tilenumber, dy, dx 
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 13+2,10  ;tilenumber, dy, dx
  dw    094-1 : db 13+2,11  ;tilenumber, dy, dx  
  db    4                ;amount of tiles to put (0=end)
  dw    344-1 : db 11+2,10  ;tilenumber, dy, dx
  dw    345-1 : db 11+2,11  ;tilenumber, dy, dx
  dw    376-1 : db 12+2,10  ;tilenumber, dy, dx
  dw    377-1 : db 12+2,11  ;tilenumber, dy, dx     
  db    0                 ;amount of tiles to put (0=end)

WMAnimValBo4toStarD1:
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 10+2,10  ;tilenumber, dy, dx
  dw    094-1 : db 10+2,11  ;tilenumber, dy, dx 
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 09+2,10  ;tilenumber, dy, dx
  dw    094-1 : db 09+2,11  ;tilenumber, dy, dx  
  db    4                ;amount of tiles to put (0=end)
  dw    346-1 : db 07+2,10  ;tilenumber, dy, dx
  dw    347-1 : db 07+2,11  ;tilenumber, dy, dx
  dw    378-1 : db 08+2,10  ;tilenumber, dy, dx
  dw    379-1 : db 08+2,11  ;tilenumber, dy, dx 
  db    1                ;amount of tiles to put (0=end)
  dw    319-1 : db 08+2,12  ;tilenumber, dy, dx      
  db    1                ;amount of tiles to put (0=end)
  dw    319-1 : db 08+2,13  ;tilenumber, dy, dx      
  db    1                ;amount of tiles to put (0=end)
  dw    319-1 : db 08+2,14  ;tilenumber, dy, dx      
  db    1                ;amount of tiles to put (0=end)
  dw    319-1 : db 08+2,15  ;tilenumber, dy, dx      
  db    0                 ;amount of tiles to put (0=end)  
  
WMAnimLarrCatoFrontD:
  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    093-1 : db 14+2,20  ;tilenumber, dy, dx

  db    6 + %1000'0000      ;explosion sfx
  dw    161-1 : db 10+2,16  ;tilenumber, dy, dx
  dw    162-1 : db 10+2,17
  dw    363-1 : db 11+2,16
  dw    364-1 : db 11+2,17
  dw    357-1 : db 12+2,16
  dw    358-1 : db 12+2,17

  db    1 + %0100'0000      ;no sfx, basically wait 1 step
  dw    093-1 : db 14+2,20  ;tilenumber, dy, dx
  
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 10+2,16  ;tilenumber,xcz dy, dx
  dw    094-1 : db 10+2,17  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    093-1 : db 09+2,16  ;tilenumber, dy, dx
  dw    094-1 : db 09+2,17  ;tilenumber, dy, dx

  db    0                 ;amount of tiles to put (0=end) 
  
WMAnimValBo4toLarrCa:
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 11+2,12  ;tilenumber, dy, dx
  dw    185-1 : db 12+2,12  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 11+2,13  ;tilenumber, dy, dx
  dw    185-1 : db 12+2,13  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 11+2,14  ;tilenumber, dy, dx
  dw    185-1 : db 12+2,14  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    153-1 : db 11+2,15  ;tilenumber, dy, dx
  dw    185-1 : db 12+2,15  ;tilenumber, dy, dx  
  db    0                 ;amount of tiles to put (0=end)     
  
WMAnimStarW1toStarB2:
  db    1                ;amount of tiles to put (0=end)
  dw    019-1 : db 11+2,09  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    012-1 : db 10+2,08  ;tilenumber, dy, dx
  dw    011-1 : db 11+2,08  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    012-1 : db 09+2,07  ;tilenumber, dy, dx
  dw    011-1 : db 10+2,07  ;tilenumber, dy, dx
  db    1                ;amount of tiles to put (0=end)
  dw    011-1 : db 09+2,06  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)      

WMAnimStarW2toStarC2:
  db    1                ;amount of tiles to put (0=end)
  dw    020-1 : db 07+2,12  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    043-1 : db 06+2,13  ;tilenumber, dy, dx
  dw    044-1 : db 07+2,13  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    043-1 : db 05+2,14  ;tilenumber, dy, dx
  dw    044-1 : db 06+2,14  ;tilenumber, dy, dx
  db    1                ;amount of tiles to put (0=end)
  dw    044-1 : db 05+2,15  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)     

WMAnimStarW3toStarD2:
  db    2                ;amount of tiles to put (0=end)
  dw    087-1 : db 07+2,21  ;tilenumber, dy, dx
  dw    088-1 : db 08+2,21  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    087-1 : db 07+2,22  ;tilenumber, dy, dx
  dw    088-1 : db 08+2,22  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    087-1 : db 07+2,23  ;tilenumber, dy, dx
  dw    088-1 : db 08+2,23  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    087-1 : db 07+2,24  ;tilenumber, dy, dx
  dw    088-1 : db 08+2,24  ;tilenumber, dy, dx    
  db    0                 ;amount of tiles to put (0=end)    

WMAnimStarW3toStarD2_InWorld2:
  db    4                 ;amount of tiles to put (0=end)
  dw    528 : db 31+2,39  ;tilenumber, dy, dx
  dw    529 : db 31+2,40  ;tilenumber, dy, dx
  dw    560 : db 32+2,39  ;tilenumber, dy, dx
  dw    561 : db 32+2,40  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)

WMAnimStarW4toStarE2:
  db    1                ;amount of tiles to put (0=end)
  dw    052-1 : db 12+2,22  ;tilenumber, dy, dx
  db    1                ;amount of tiles to put (0=end)
  dw    025-1 : db 13+2,22  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    011-1 : db 14+2,22  ;tilenumber, dy, dx
  dw    012-1 : db 14+2,23  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    011-1 : db 15+2,23  ;tilenumber, dy, dx
  dw    012-1 : db 15+2,24  ;tilenumber, dy, dx
  db    2                ;amount of tiles to put (0=end)
  dw    011-1 : db 16+2,24  ;tilenumber, dy, dx
  dw    012-1 : db 16+2,25  ;tilenumber, dy, dx
  db    1                ;amount of tiles to put (0=end)
  dw    023-1 : db 17+2,25  ;tilenumber, dy, dx
  db    0                 ;amount of tiles to put (0=end)    

WMAnimStarW4toStarE2_InWorld7:
  db    4                ;amount of tiles to put (0=end)
  dw    346-1 : db 07+2,10  ;tilenumber, dy, dx
  dw    347-1 : db 07+2,11  ;tilenumber, dy, dx
  dw    378-1 : db 08+2,10  ;tilenumber, dy, dx
  dw    379-1 : db 08+2,11  ;tilenumber, dy, dx   
  db    0                 ;amount of tiles to put (0=end)    

WMAnimStarW5toStarF2:
  db    4                ;amount of tiles to put (0=end)
  dw    026-1 : db 07+2,15  ;tilenumber, dy, dx
  dw    027-1 : db 07+2,16  ;tilenumber, dy, dx
  dw    058-1 : db 08+2,15  ;tilenumber, dy, dx
  dw    059-1 : db 08+2,16  ;tilenumber, dy, dx   
  db    0                 ;amount of tiles to put (0=end)      