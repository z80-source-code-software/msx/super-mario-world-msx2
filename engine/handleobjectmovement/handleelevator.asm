;handle all the elevators

handleplatforminteract:

  ld l,(ix+movepat+1)
  ld a,(ix+movepat)
  xor l ;prevent accidental 16 bits matching
  cp 120
  jp nz,.nodowncheck
  

.solidbox:  
  
  
;.setmariointeraction:
;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox


.nodowncheck:

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  push de
  ld de,16
  add hl,de
  pop de
  xor a
  sbc hl,de
  ret nc


.nopopbox:

;  call getmariodistance
;  cp 12
;  jp nc,.handlesidetouch


  ld a,1
  ld (standingonsprite),a
  ld (standingonplatform),a

  ld (spriteixvalue),ix
  

;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  ld de,24
;  xor a
;  sbc hl,de
;  ld (marioy),hl
  
 
  
  ret



.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c


  ld a,1
  ld (jumpingupsprite),a
 
  ret


.handlesidetouch:

  call getmarioleftright
  jp c,.sidetouchright

  ld a,1
  ld (touchside),a
  
  ld (spriteixvalue),ix
  ret

.sidetouchright:

  ld a,2
  ld (touchside),a

  ld (spriteixvalue),ix
  ret


checkobjectlock: ;in iy platform address in ix object address changes allregisters except iy and ix returns carry
;or non carry + ix+standingonsprite value if lock is not true

  ld a,(ix+standingonspr)
  and a
  scf
  ret z
  dec a
  scf
  ret z

;  ld a,(ix+v2)
;  and a
;  scf
;  ret z

  ld a,1 ;tell the other routines we are doing a lockcheck
  ld (hlvar),a 
  
  ld l,(iy+yobject) ;y of platform
  ld h,(iy+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  push hl
  ld a,(ix+sizeY)
  sub 16
  ld e,a
  ld d,0
  xor a
  pop hl
  sbc hl,de ;substract the size of the object
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h
  ld (objectxy),hl


  jp handleplatform.nosubstract


objectspritelock:

  ld a,0
  ld (hlvar),a

;this was saved during the handleobjectmovement  
  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  push hl
  pop iy
    
  
  ld l,(iy+yobject) ;y of platform
  ld h,(iy+yobject+1)
  ld (objectxy),hl
  ld de,8
  xor a
  sbc hl,de
  push hl
  ld a,(ix+sizeY)
  sub 16
  ld e,a
  ld d,0
  xor a
  pop hl
  sbc hl,de ;substract the size of the object
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 

  
;then we must always add the object speed to this object

handleplatform:


  ;this was saved during the handleobjectmovement  
  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  push hl
  pop iy


  ld d,0
  ld a,(iy+v1)
  bit 7,a
  jp z,.nonegate
  neg 
  ld e,a
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  jp .nosubstract

.nonegate:
  ld e,a
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
 ; xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
 
.nosubstract: 

 ;these where calls ...

  ld a,(ix+standingonspr)
  cp 2
  jp z,lockobjectsheer.slightleft
  cp 3
  jp z,lockobjectsheer.slightright
  cp 4
  jp z,lockobjectsheer.iggy
  cp 5
  jp z,lockobjectsheer.mediumleft
  cp 6
  jp z,lockobjectsheer.mediumright
  cp 7
  jp z,lockobjectsheer.maxleft
  cp 8
  jp z,lockobjectsheer.maxright
;  cp 9
;  jp z,lockobjectsheer.normalleft

  ;scf
  xor a
  ret
  
  
checkforspritewall:

  ld a,(ix+objectrelease)
  cp 2
  ret z
  cp 3
  ret z
  cp 4
  ret z


  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  push hl
  pop iy ;fetch the lockdata of the platform

    ;right and left at once
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld e,(iy+xobject)
  ld d,(iy+xobject+1)
  xor a
  sbc hl,de
  ld a,(iy+sizeX)
;  add a,16
  sub 16
  ld d,0
  ld e,a
  xor a
  sbc hl,de
  
  
  ret  
  

platforminteract:

  ld (bcvar),bc

;check for mario first then do the rest
  call mariointeract
  call c,handleplatforminteract

  ld bc,(bcvar)
;in = c sheertile or not c = 1 left c = 2 right etc
.objectonly:


  ld (bcvar),bc

  ld (currentobjectvalue),ix
  push ix
  pop iy

;alle XY waarden die het huidige object bezitten saven voordat we de loop in gaan
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld (currentx),hl
  ld (currenty),de

;zodra we een object vinden handelen we hem af en returnen we met een carry flag
  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,handleplatformobjectinteract : call c,.end ;objectnr 20

;no object found return this
  xor a
  ld ix,(currentobjectvalue)


  ret


.end: ;was .setobjectinteraction

;messy shitty code basicly I want to exclude any value which is 0, 1 or not 2 , 3 or 4
;this is inefficient gotta think something new out

  ld bc,(bcvar)

  ld a,c
  dec a
  jp z,.two
  dec a
  jp z,.three
  dec a
  jp z,.four
  dec a
  dec a
  jp z,.five
  dec a
  jp z,.six
  dec a
  jp z,.seven
  dec a
  jp z,.eight
;0
.one:

  ld (ix+standingonspr),1

  jp .done
  
.two:

  ld (ix+standingonspr),2
  
  jp .done
  
.three:

  ld (ix+standingonspr),3

  jp .done

.four:

  ld (ix+standingonspr),4

  jp .done

.five:

  ld (ix+standingonspr),5

  jp .done
  
.six:

  ld (ix+standingonspr),6
  
  jp .done

.seven:

  ld (ix+standingonspr),7
  
  jp .done

.eight:

  ld (ix+standingonspr),8

.done:
;we make lockdata a 16 bit iy value
  push iy
  pop hl
  ld (ix+lockdata),l
  ld (ix+lockdata+1),h  

  call checkobjectlock ;check objects lock before truly locking

;adress terugschrijven
  ld ix,(currentobjectvalue)
;  scf
  ret


  


handleplatformobjectinteract:

;ben ik het zelf
  ld hl,(currentobjectvalue)
  push ix
  pop de

  xor a
  sbc hl,de
  jp z,.endcheck

;check a predefined value in the offsets some objects don't need to interact so we can skip the whole thing
  ld a,(ix+nosheercheckb)
  dec a
  jp z,.endcheck


 ;ret

  ;right and left at once
  ld de,(currentx)
;  ld hl,(currentx) ;grauw says this is not needed
;  ld de,16
;  xor a
;  sbc hl,de
;  ex de,hl
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  ld a,(iy+sizeX)
;  add a,16
  ld d,0
  ld e,a
  xor a
  sbc hl,de
  jp nc,.endcheck

  ld a,(iy+objectrelease)
  cp 7
  jp nc,.checkspecialy

  ;/check if other object is within x range of object

;PROBLEM: when the ysize is bigger than 32 pixels this code fucks out.

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-30
  add hl,de
 
  ld de,(currenty)
  or  a  
  sbc hl,de
  
;top border not found, no collision
  jp nc,.endcheck

  ld  a,(ix+sizeY)
  add a,32;;15
  ld  d,0
  ld  e,a
;  or  a  
  add hl,de

;bottom border not found, no collision
   jp nc,.endcheck
  ;/check if other object is  within y range of object
  
  ret

.endcheck:

  xor a
  ret


;I don't have the time and "zin" right now to fix the above handler so for the superspecialtiles Ill just do it
;the cheap shit way

.checkspecialy:

;  scf ret

  ;check if other object is  within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
 
  ld hl,(currenty)
  or  a  
  sbc hl,de
  
;top border not found, no collision
  jp nc,.endcheck

  ld hl,(currenty)

  ld  e,(iy+sizeY)
  ld  d,0
  
  add hl,de

  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  
  xor a
  sbc hl,de

;bottom border not found, no collision
   jp c,.endcheck
  ;/check if other object is  within y range of object  


  scf
  ret

;PROBLEM: we are out of rom space. So we cannot add support for object type 9 and 10 without slowing the engine down even further. Since these object bearing platforms are extremely rare 
;and no situations occur with object nearby I decided to leave the support out for these objects

lockobjectsheer:

.normalleft:
  
;  ld hl,sheertablenormalleftobject
  
;  jp .lock
  
.normalright:

;  ld hl,sheertablenormalrightobject  
  
;  jp .lock


.maxleft:

  ld hl,sheertablemaxleftobject

  jp .lock 

.maxright:

  ld hl,sheertablemaxrightobject

  jp .lock 

  
.slightright:

  ld hl,sheertablerightobject

  jp .lock  
  
 
;first obtain the xcoordinate of the object compared to the object it is standing on  
.slightleft:
  
  ld hl,sheertableleftobject

  jp .lock


;first obtain the xcoordinate of the object compared to the object it is standing on  
.mediumleft:
  
  
  ld hl,sheertableleftmediumobject

  jp .lock
  
  
.mediumright:
  
 
  ld hl,sheertablerightmediumobject 
  
  
.lock:  
  push hl
  
  
  ld e,(iy+xobject)
  ld d,(iy+xobject+1)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  ld a,h ;check for minus
  and a
  call nz,.setzero
  
  ld d,0
  ld e,l ;maximum number is 191
  
  pop hl
;  ld hl,objectsheertable
  add hl,de
  
  ld a,specialsheertilesblock34
  call block34
  
  ld a,(hl) ;fetch the value from table
;  bit 7,a
;  call nz,.setzero ;never allow negative numbers to occur
  ld e,a

  ld a,(hlvar)
  and a
  jp nz,.checklock


  ld d,0
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de ;substract table value from absolute lock
  
  ld de,8 ;substract base number as compensation
  xor a
  sbc hl,de
  
;write back to ram
 ld (ix+yobject),l
 ld (ix+yobject+1),h 
  
  
  call setpage12tohandleobjectmovement
  
  ret


.checklock:

  ld d,0
;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
  ld hl,(objectxy)
  add hl,de ;substract table value from absolute lock
  
  ld de,8 ;substract base number as compensation
  xor a
  sbc hl,de

  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  
  xor a
  sbc hl,de ;getdistance
  jp c,.return
  
  ld a,l
  and a
  jp z,.return ;include zero 
;  sub 2 ;sub one tile of distance
;  jp c,.return
  
;problem: when v2 is 0 there can never be a return so objects that turn around walk beyond the lock range
  ld c,(ix+v2) ;get speed
  
  sub c ;see what we get
  jp c,.return
  
  
  ld (ix+standingonspr),0

  call setpage12tohandleobjectmovement

  xor a
  ret

.return:

  call setpage12tohandleobjectmovement

  scf
  ret


 
.setzero:

  xor a
  ld l,a
  ret

  

;objectsheertable: db 24,24,23,23,23,22,22,21,21,21,20,20,20,19,19,18,18,18,17,17,17,16,16,15,15,15,14,14,14,13,13,12,12,12,11,11,11,10,10,9,9,9,9,9,9,9,9,9,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8


.iggy:
 
  
  ld a,(iy+v3) ;add offset from to table
  ld b,a
  add a,a
  add a,b ;*3
  add a,a ;*6
  add a,a ;*12
  ld d,0
  ld e,a ;continue in 16bits
  sla e
  rl d ;*24
  sla e
  rl d ;*48
  sla e
  rl d ;*96
  sla e
  rl d ;*192
  
  ld hl,iggysheertableobject
  add hl,de

  jp .lock
  
  


  
