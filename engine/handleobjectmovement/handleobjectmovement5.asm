;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement5:

;start = 205


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz


  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress5 - 205 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)


;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress5
;  add   hl,de
;  ;substract minimum base adres
;  ld de,205*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress5:

  jp    MovementPattern205     ;red flying koopa (up and down)
  jp    MovementPattern206     ;coin moving underwater
  jp    MovementPattern207     ;rip van fish
  jp    MovementPattern208     ;rip van fish z-bubble
  jp    MovementPattern209     ;air bubble
  jp    MovementPattern210     ;fish (dumb) (up and down)
  jp    MovementPattern211     ;biggerfish (dumb) (left or right)
  jp    MovementPattern212     ;p-balloon
  jp    MovementPattern213     ;spring
  jp    MovementPattern214     ;vineseparator (helper object)
  jp    MovementPattern215     ;ice (helper object)
  jp    MovementPattern216     ;muncher
  jp    MovementPattern217     ;falling platform
  jp    MovementPattern218     ;small ghostring
  jp    MovementPattern219     ;big boo
  jp    MovementPattern220     ;one way door
  jp    MovementPattern221     ;eerie (left right)
  jp    MovementPattern222     ;questionmark animation (pbox)
  jp    MovementPattern223     ;the big boo
  jp    MovementPattern224     ;sheer stairs (helper object)
  jp    MovementPattern225     ;thwimp
  jp    MovementPattern226     ;elevator only
  jp    MovementPattern227     ;ball n chain
  jp    MovementPattern228     ;thwomp
  jp    MovementPattern229     ;dry bones
  jp    MovementPattern230     ;horizontal object mover
  jp    MovementPattern231     ;dry bones special spawn
  jp    MovementPattern232     ;secret box (1up)
  jp    MovementPattern233     ;huge door (special spawn)
  jp    MovementPattern234     ;morton
  jp    MovementPattern235     ;secondary colored block
  jp    MovementPattern236     ;questionmark animation special
;  jp    MovementPattern237     ;cutscene objects (DO NOT USE IN LEVELENGINE!!)



.return:
  
  ret



MovementPattern236:     ;questionmark animation special



  ld a,(ix+v5)
  and a
  jp z,.activate


  call getmarioheighty16
  cp 210
  jp nc,.deactivate ;not yet  

  ld a,(ix+v5)
  dec a
  jp z,.function

  ret

.activate:

  call getmarioheighty16
  cp 200
  ret nc ;not yet
  
  inc (ix+v5)

  ret


.deactivate:

  ld (ix+v5),0
  
  ret


.function:

;see what has to come out
  call objectinteract
  cp 3
  jp z,.releaseobject


;Failsafe. Check the tile around us and see if the box popped by a nearby object. If yes pop anyway
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 204
  jp z,.releaseobject


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3

  ret

.animate:
 
  
.position0:
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex68c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex69  ;color address
  ld    bc,spriteaddrex69c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex70  ;color address ;02
  ld    bc,spriteaddrex70c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex71  ;color address ;03
  ld    bc,spriteaddrex71c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releaseobject:

  ld a,(ix+objectrelease)
  cp 6
  jp z,.releasecoin

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),40
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releasecoin:


  call Playsfx.coin

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),70
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 

  ;ret



MovementPattern235:     ;secondary colored block

;use ix+v7 as the shell does it for hammer brother

  ld a,(ix+v5)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.popup

  ld a,(ix+v7)
  and a
  jp nz,.popbox


  ld c,0
  call platforminteract
  
  call mariointeract
  jp c,.releaseobject
  
 
  ret


.popup:

  call addv2toybject

  inc (ix+v6)
  ld a,(ix+v6)
  cp 2
  ret nz
  
  ld (ix+v2),8
  call addv2toybject
  
  ld (ix+movepat),40

  ret

.releaseobject:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox


.nopopbox:

  ld a,1
  ld (standingonsprite),a

  ld (spriteixvalue),ix
  
  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  push hl
  ld de,8
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  call createbrownblock

  pop hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h  

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v5)
  ld (ix+v2),-4

 ; ld (ix+movepat),40
  ld    hl,spriteaddrex72  ;color address
  ld    bc,spriteaddrex72c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.init:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld (ix+clipping),8
  
  inc (ix+v5)

  ld de,.offsets8

  jp    setcharcolandoffset.offsetsonly

.offsets8:  dw 008,000


MovementPattern234:     ;morton


  ld a,(ix+v4)
  cp 1
  jp z,.moveleftright 
  cp 2
  jp z,.moveup
  cp 3
  jp z,.moveupsidedown
  cp 4
  jp z,.fall
  cp 5
  jp z,.die
  cp 6
  jp z,.destroy
  cp 7
  jp z,.endlevel

  call mariointeract
;  jp c,.setkill


  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  
   
  inc (ix+v4)
  ld (ix+v7),0


  call getmarioleftright
  jp c,.setright

  ld (ix+v3),0
  ld (ix+v1),-2
  ld hl,-8
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret


.destroy:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 60
  ret c
  cp 61
  call z,Playsfx.marioflyrise


  call addv2toybject
  
  ld hl,(mapheightx8)
  ld de,32
  xor a
  sbc hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.setendlevel  
  
  ret

.setendlevel:

  ld (ix+v4),7

  xor a
  ld (time),a ;reset timer  
  
  ld a,2
  ld (playwinmusic),a

  ld hl,350

  ld (ix+v6),l
  ld (ix+v7),h

;make sprite blank
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret

.endlevel:


  ld l,(ix+v6)
  ld h,(ix+v7)
  dec hl
  ld (ix+v6),l
  ld (ix+v7),h
  ld de,0
  xor a
  sbc hl,de
  ret nz
  
 
  ld a,1
  ld (levelend?),a
  ld a,3
  ld (nocontrol),a



  ret



.die:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  cp 32
  jp z,.hit3
  cp 34
  jp z,.hit2
  cp 37
  ret c
 

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex544  ;color address
  ld    bc,spriteaddrex544c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  call    setcharcolandoffset
  
  ld (ix+v4),0
  ld (ix+v7),0

  ret


.hit2:
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex545  ;color address
  ld    bc,spriteaddrex545c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
 
 
.hit3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex546  ;color address
  ld    bc,spriteaddrex546c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset



.setkill:

  call Playsfx.chuckauw

  ld (ix+v7),0
  ld (ix+v4),5

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex547  ;color address
  ld    bc,spriteaddrex547c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  dec (ix+hitpoints)
  jp z,.setdestroy

  ret


.setdestroy:
  
  ld (ix+v4),6
  ld (ix+v2),8

  ret


.setright:

  ld (ix+v3),2
  ld (ix+v1),2
  ld hl,24
  ld (ix+v5),l
  ld (ix+v5+1),h

  ret

.setfallleft:

  ld a,(ix+v1)
  cp 2
  ret z
  
  inc sp
  inc sp
  
.setfall:

  inc (ix+v4)
  ld (ix+v2),0
  ld (ix+objectfp),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex544  ;color address
  ld    bc,spriteaddrex544c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

 ; ret

.setup:


  inc (ix+v4)
  ld (ix+v2),-2


  ret

.setupsidedown:

  inc (ix+v4)
  ld a,(ix+v1)
  neg 
  ld (ix+v1),a
  ret


.fall:

  call mariointeract

  call addv2toybject
  
  call checkobjecttilesfloor
  ret c
  
  ld (ix+v4),0
  ld a,1
  ld (tripscreen),a


  ld a,(mariofloating)
  and a
  ret nz
  
  ld a,10
  ld (mariotransforming),a
  
  call Playsfx.explosion
  
  ret



.moveupsidedown:

  call mariointeract

  ld l,(ix+xobject) ;limit to utmost left
  ld h,(ix+xobject+1)
  ld de,36
  xor a
  sbc hl,de
  call c,.setfallleft


  call getmariodistance
  ld a,c
  cp 5
  jp c,.setfall

  call addv1toxbject

  ld a,(framecounter)
  and 3
  jp z,animatemortonupsidedown


  ret


.moveup:

  call mariointeract

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp nc,.setupsidedown


  call addv2toybject

  ld a,(framecounter)
  and 3
  jp z,animatemortonup


  ret


.moveleftright:

  call mariointeract
  jp c,.setkill


  ld bc,0
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  call objecttilecheckfromrom
  cp 192
  jp nc,.setup


  call addv1toxbject

  ld a,(framecounter)
  and 3
  jp z,animatemortonleftright

  ret



animatemortonupsidedown:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex550  ;color address
  ld    bc,spriteaddrex550c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex551  ;color address
  ld    bc,spriteaddrex551c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex548  ;color address ;02
  ld    bc,spriteaddrex548c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex549  ;color address ;03
  ld    bc,spriteaddrex549c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

  
  


animatemortonup:

    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex540  ;color address
  ld    bc,spriteaddrex540c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex541  ;color address
  ld    bc,spriteaddrex541c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex542  ;color address ;02
  ld    bc,spriteaddrex542c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex543  ;color address ;03
  ld    bc,spriteaddrex543c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

  
  




animatemortonleftright:

    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex536  ;color address
  ld    bc,spriteaddrex536c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex537  ;color address
  ld    bc,spriteaddrex537c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex538  ;color address ;02
  ld    bc,spriteaddrex538c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex539  ;color address ;03
  ld    bc,spriteaddrex539c  ;character address
  ld    de,offsetmorton
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

  
  
  
  
;  ret


MovementPattern233:     ;huge door (special spawn)

  ld a,(ix+v4)
  and a
  jp z,.init


  ld hl,45*8
  ld de,(marioy)
  xor a
  sbc hl,de
  ret c

  ld b,6
  call increasespritecount
  
  ld (ix+movepat),147
  ld (ix+v4),0

  ret

.init:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,24
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  inc (ix+v4)
  ret



MovementPattern232:     ;secret box (1up)


;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobject

  call mariointeract
  call c,handlesecretboxinteract

  ret

handlesecretboxinteract:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox
  
  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a

  ld (ix+movepat),48

  ld b,2
  call increasespritecount
 
  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0
 

;lock the coordinates

  ld a,(ix+yobject)
  and %11111000
  ld (ix+yobject),a
  ld a,(ix+xobject)
  ld h,(ix+xobject+1)
  and %11111000
  ld (ix+xobject),a

  ld a,(ix+objectrelease)
  cp 6
  jp z,.popcoin

  call Playsfx.powerup
  
  ret

.popcoin:

  call Playsfx.coin
  
  ret 




MovementPattern231:     ;dry bones special spawn

  ld a,(ix+v5)
  and a
  jp z,.activate


  call getmarioheighty16
  cp 210
  jp nc,.deactivate ;not yet  

  ld a,(ix+v5)
  dec a
  jp z,MovementPattern229

  ret

.activate:

  call getmarioheighty16
  cp 200
  ret nc ;not yet

  ld b,8
  call increasespritecount
  
  inc (ix+v5)


  ret


.deactivate:

  call removefromsprlst
  ld (ix+amountspr),0


  ld (ix+v5),0

  ret


animatequestionmarkblocks:

  ld    a,animationtilesblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtilescharacteraddr+(8*36)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*36)             ;rotating tile animation 1 character address in ro
  ld a,192
  call .updateblocks   
  ld hl,animationtilescharacteraddr+(8*32)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*32)             ;rotating tile animation 1 character address in ro
  ld a,188
  jp .updateblocks 

.step3:
  ld hl,animationtilescharacteraddr+(8*26)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*26)             ;rotating tile animation 1 character address in ro
  ld a,192  
  call .updateblocks   
  ld hl,animationtilescharacteraddr+(8*22)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*22)             ;rotating tile animation 1 character address in ro
  ld a,188  
  jp .updateblocks 

.step2:
  ld hl,animationtilescharacteraddr+(8*16)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*16)             ;rotating tile animation 1 character address in ro  
  ld a,192  
  call .updateblocks   
  ld hl,animationtilescharacteraddr+(8*12)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*12)             ;rotating tile animation 1 character address in ro
  ld a,188  
  jp .updateblocks 

.step1:
  ld hl,animationtilescharacteraddr+(8*6)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*6)             ;rotating tile animation 1 character address in ro
  ld a,192  
  call .updateblocks   
  ld hl,animationtilescharacteraddr+(8*2)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*2)             ;rotating tile animation 1 character address in ro
  ld a,188  

.updateblocks:
  
  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix32

  ret


;IN a = tilenumber IN = HL tilegfx adress DE = Tilegfx color address Changes all registers including the alternate ones
;b = in case of a loop
updatehorizontalblocks:

  
  exx 

  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  
  ld a,(framecounter)
  and 1
  jp z,.doshit
  
  ld de,$4000
  add hl,de
 ; jp .doshit



 ;ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

  ld c,$98
  call outix8
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0

  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

    ld c,$98
  call outix8

  exx

  ret



;animations for horizontal objects in map 02-11
horzontalblock:                      incbin "../grapx/world2/level2-11c mortons-castle/animation big block.SC2",$0000+7,8*120  ;character table
horzontalblockclr:                   incbin "../grapx/world2/level2-11c mortons-castle/animation big block.SC2",$2000+7,8*120  ;color table


MovementPattern230:     ;horizontal object mover

;use v4 as objecttype
;v5 Global tile position counter
;v7 is the pixel counter which resets at 0
;v3 is used as a temp far to keep track of the lines

;the global tileposition counter checks each layer and adds or substracts the tiles to it

  ld a,(ix+v4)
  and a
  jp z,.init

  call followmario

  call animatequestionmarkblocks ;warning! switches away the general code block

  ld a,(slot.page2rom) ;get map
  out ($a8),a


;kill mario if he manages to land on the spikes
  ld de,0
  ld bc,24
  call objecttilecheck
  cp 15
  call nc,.killmariospike  


;check around mario and act to it
;check the right
  ld de,1
  ld bc,8
  call objecttilecheck
  cp 229
  call z,.kickmarioleft
  ld de,(maplenght)
  add hl,de
  ld a,(hl)
  cp 229
  call z,.kickmarioleft
  ld de,(maplenght)
  add hl,de
  ld a,(hl)
  cp 229
  call z,.kickmarioleft  

;check the left
  ld de,-1
  ld bc,8
  call objecttilecheck
  cp 233
  call z,.kickmarioright
  ld a,(hl)
  cp 175
  call z,.kickmarioright
  ld de,(maplenght)
  add hl,de
  ld a,(hl)
  cp 233
  call z,.kickmarioright
  ld de,(maplenght)
  add hl,de
  ld a,(hl)
  cp 233
  call z,.kickmarioright  

  ;if mario gets pushed inside a block hes dead
  ld de,1
  ld bc,24
  call objecttilecheck
  cp 192
  call nc,.killmario ;mario gets pushed into a hard wall
  ;check if mario managed to get inside a block
  ld a,(hl)
  cp 173
  call z,.pushdown


  ld de,0
  ld bc,32 ;?
  call objecttilecheck
  cp 234
  push af
  call z,.movemario
  pop af
  cp 235
  push af
  call z,.movemario
  pop af
  cp 236
  push af
  call z,.movemario
  pop af
  cp 222
  push af
  call z,.movemario
  pop af
  cp 228
  call z,.movemario


  jp .move

;  ret


.pushdown:

  ld hl,(marioy)
  ld de,8
  add hl,de
  ld (marioy),hl
  ret
  

.killmariospike:

  cp 19
  ret nc

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a
  
  ret


.killmario:

  cp 233
  ret nc

  cp 228
  ret z
  cp 229
  ret z
  cp 230
  ret z
  cp 222
  ret z
  cp 223
  ret z

  ld a,(mariofloating)
  and a
  ret nz

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a
  
  ret


.kickmarioright:

;TODO: compare to V7 and make it lock smooth
  ld hl,(mariox)
  ld a,(ix+v7)
  and 7
  ld e,a
  ld d,0
;  ld de,10

  add hl,de
  ld (mariox),hl


  ret

.kickmarioleft:

;compare to V7 and make it lock somewhat smooth
  ld hl,(mariox)
  ld a,(ix+v7)
  and 7
  ld e,a
  ld d,0

 ; ld de,10

  xor a
  sbc hl,de
  ld (mariox),hl


  ret


.movemario:

  ld a,(ix+v6) ;left or right
  and a
  jp nz,.movemarioleft

  ld hl,(mariox)
  inc hl
  ld (mariox),hl

  ld a,(mariospeed)
  and a
  ret nz

  ld a,1
  ld (marioleftright),a

  ret


.movemarioleft:


  ld hl,(mariox)
  dec hl
  ld (mariox),hl


  ld a,(mariospeed)
  and a
  ret nz


  xor a
  ld (marioleftright),a


  ret



.handletiles:

;the actual moving of the blocks. Should only happen when tiles are required to be updated  
  ld iy,horizontalobjectlist

.loopobjectlist:

  ld a,(iy)
  cp 255
  ret z ;done
  call .handleobject
  ld de,horizontalobjectlistlenght
  add iy,de
  jp .loopobjectlist

;ret

;distance check. If mario is too far out don't do shit
.handleobject:

  ld hl,(marioy)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8  
  ld a,(iy+1)
  sub l
  jp c,.checkminus ;out of bottom
  cp 22
  ret nc ;out of top
  jp .continue
  
.checkminus:

  neg
  cp 22
  ret nc ;out of bottom
  

.continue:


  ld a,(iy+2)
  and a
  jp nz,.handleobjectright

  call writebackbkg
  call writehorizontalobject
;  call writebackbkg
  
  ret


.handleobjectright:

;TODO: when two intersect with each other use writebackbkg 

  ld a,(iy+5)
  and a
  jp nz,.handleobjectrightspecial

  call writebackbkgright
  call writehorizontalobjectright
;  call writebackbkg

  ret

.handleobjectrightspecial:

  call writebackbkg
  call writehorizontalobjectright
;  call writebackbkg

  ret


.animatesteps:

  ld a,(ix+v7)
  and 7
  cp 0
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
  cp 7
  jp z,.step7  
        
  
  ret


.writestep:
;add 40 to each tilestep

;4x push
  push hl
  push de

;3x push
  push hl
  push de

;2x push
  push hl
  push de

;1 x push
  push hl
  push de
  
  push hl
  push de

  ld a,228 ;which block to write to
  call updatehorizontalblocks ;use same code  

  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,229 ;which block to write to
  call updatehorizontalblocks ;use same code  

  push hl
  push de  
  
  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,230 ;which block to write to
  call updatehorizontalblocks ;use same code 

;pop 1
  pop de
  pop hl

  ld bc,8
  add hl,bc
  ex de,hl
  add hl,bc
  ex de,hl
  

  push hl
  push de

  ld a,236;231 ;which block to write to
  call updatehorizontalblocks ;use same code  

  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,174 ;which block to write to
  call updatehorizontalblocks ;use same code  

  push hl
  push de  
  
  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,232 ;which block to write to
  call updatehorizontalblocks ;use same code 

;pop 2
  pop de
  pop hl

  ld bc,3*8
  add hl,bc
  ex de,hl
  add hl,bc
  ex de,hl


  push hl
  push de

  ld a,235 ;which block to write to
  call updatehorizontalblocks ;use same code  

  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,175 ;which block to write to
  call updatehorizontalblocks ;use same code  

  push hl
  push de  
  
  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,231 ;which block to write to
  call updatehorizontalblocks ;use same code 


;pop 3  
  pop de
  pop hl


  ld bc,4*8
  add hl,bc
  ex de,hl
  add hl,bc
  ex de,hl

  push hl
  push de

  ld a,222 ;which block to write to
  call updatehorizontalblocks ;use same code  

  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,233 ;which block to write to
  call updatehorizontalblocks ;use same code  

  push hl
  push de  
  
  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,223 ;which block to write to
  call updatehorizontalblocks ;use same code 


;pop 4  
  pop de
  pop hl

  ld bc,16
  add hl,bc
  ex de,hl
  add hl,bc
  ex de,hl


  push hl
  push de

  ld a,234 ;which block to write to
  call updatehorizontalblocks ;use same code

  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  push hl
  push de

  ld a,173 ;which block to write to
  call updatehorizontalblocks ;use same code


  pop hl
  ld bc,40
  add hl,bc
  ex de,hl
  pop hl
  add hl,bc

  ld a,221 ;which block to write to
  call updatehorizontalblocks ;use same code
  
  ret


.step0:

  ld hl,horzontalblock+(8*0)
  ld de,horzontalblockclr+(8*0)   

  call .writestep

  ld a,(ix+v6)
  and a
  jp nz,.step72

  ret

.step1:

  ld hl,horzontalblock+(8*15)
  ld de,horzontalblockclr+(8*15)   

  jp .writestep

.step2:

  ld hl,horzontalblock+(8*30)
  ld de,horzontalblockclr+(8*30)   

  jp .writestep

.step3:

  ld hl,horzontalblock+(8*45)
  ld de,horzontalblockclr+(8*45)   

  jp .writestep

.step4:

  ld hl,horzontalblock+(8*60)
  ld de,horzontalblockclr+(8*60)   

  jp .writestep

.step5:

  ld hl,horzontalblock+(8*75)
  ld de,horzontalblockclr+(8*75)   

  jp .writestep

.step6:

  ld hl,horzontalblock+(8*90)
  ld de,horzontalblockclr+(8*90)  

  jp .writestep
  
.step7:

  ld hl,horzontalblock+(8*105)
  ld de,horzontalblockclr+(8*105)   

  call .writestep
  
  ld a,(ix+v6)
  and a
  ret nz

  call .turnspecial ;small exclude rules that makes the blocks change their direction way faster
 
  ld a,(ix+v5)
  cp 32
  jp nc,.turn 
  
  inc (ix+v5)
  jp .handletiles

.turn:
  
  ld (ix+v6),1

  ret

.step72:

  
  ld a,(ix+v5)
  and a
  jp z,.turn2

  dec (ix+v5)
  jp .handletiles
  
;  ret

.turnspecial:

  ld hl,135*8
  ld de,(marioy)
  xor a
  sbc hl,de
  ret c

  ld hl,89*8
;  ld de,(marioy)
  xor a
  sbc hl,de
  ret nc
 
  ld a,(ix+v5)
  cp 12
  ret c
  
  inc sp ;change call into jump
  inc sp
  
  
  ld (ix+v6),1


  ret


.turn2:
  
  ld (ix+v5),0
  ld (ix+v6),0
  
  ret


.move:

  ld a,(ix+v6)
  and a
  jp nz,.moveleft

  inc (ix+v7)
  
  jp .animatesteps
  
;  ret


.moveleft:

  dec (ix+v7)
  
  jp .animatesteps  
  
;  ret


.init:

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  ;first we make a mirror copy of the map in page 1
  ld a,(slot.page2rom)
  out ($a8),a
  
  ld hl,$4000 ;start of mapdata
  ld de,$4000+$2000 ;location to copy to
  ld bc,$2000 ;half of the page
  ldir
  
;  call setpage12tohandleobjectmovement
  
  inc (ix+v4)
 ; ld (ix+v5),18
  ret

writebackbkgright:

  ld e,(iy+1)
  ld d,0
  
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  
  ld e,(iy)
  ld d,0
  add hl,de
  
  ld e,(iy+3) ;take offset into account
  ld d,0
  xor a
  sbc hl,de
    
  ld e,(ix+v5)
  ld d,0
  add hl,de
  
  ld a,(maplenght)
  sub e
  ld b,a
  inc b
  and a
  jp nz,writebackbkg.skipchecks
  
  
  ret


writehorizontalobjectright:

  ld e,(iy+1)
  ld d,0
  
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;addglobalposition to posx
  ld e,(iy)
  ld d,0
  add hl,de ;add x

;  ld a,(maplenght)
  ld b,(ix+v5)
;  sub b
;  ret c ;out of map
  ld a,b
  and a
  ret z ;b is 0
  
  ld b,(iy+3)
  sub b
  ret c
  and a
  ret z
  ld b,a
  inc b
  
  push hl
  push bc
  
  ld a,(iy+4)
  ld (ix+v3),a
  
  ld c,a;4
  ld de,.tiledata
  
.bigloop:  
  
  ld a,(de)
  
  push de
  push hl
  push bc
  
.loop:
  
  ld (hl),a
  inc hl 

  djnz .loop
  
  pop bc
  pop hl
  
  ld de,(maplenght)
  add hl,de
  
  pop de
  
  ld a,(ix+v3)
  cp c
  jp nz,.keeptile
  
  inc de ;do not inc when we have rows to handle

.keeptile:
  
  ld a,c
  cp 2
  call z,.inctile
  
  
  dec c
  
  jp nz,.bigloop

  pop bc
  dec b
  ld e,b
  ld d,0
  pop hl
  add hl,de
  push hl
  
  ld de,(maplenght)

  ld b,(ix+v3)
  dec b
  dec b

;attach final row  
  ld a,222
  ld (hl),a
  add hl,de
  
.frontloop:  
  ld a,233
  ld (hl),a
  add hl,de
  djnz .frontloop
;  ld (hl),a
;  add hl,de
  
  ld a,223
  ld (hl),a  
  
  pop hl
  dec hl

  ld b,(ix+v3)
  dec b
  dec b


;attach final row2  
  ld a,235
  ld (hl),a
  add hl,de
 
.frontloop2:  
  ld a,175
  ld (hl),a
  add hl,de
  djnz .frontloop2
  
;  ld (hl),a
;  add hl,de
  
  ld a,231
  ld (hl),a  
  

  ret

.inctile:

  inc de
  ret

.tiledata: db 234,173,221



;IN use iy
writebackbkg:

  ld e,(iy+1)
  ld d,0
  
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

  ld e,(iy)
  ld d,0
  add hl,de ;add x  

  ld a,(maplenght)
  inc a  
  ld b,(ix+v5)
  sub b
  ret c ; b will be zero

  ld a,b
  and a
  ret z

.skipchecks:

  push bc
  push hl
  exx
  pop hl
  pop bc
  ld de,$2000
  add hl,de ;set mirror
  ld c,(iy+4);4 ;4 rows
  exx

.looptotal:

  exx 
  push hl
  exx

  push bc
  push hl

.loopmap:
   exx
   ld a,(hl)
   inc hl
   exx
;exclude the vine tiles from writing back   
   ex af,af'
   ld a,(hl)
   cp 252
   jp nc,.writevine
   ex af,af'

   ld (hl),a
.writevine:    
   inc hl
   
   djnz .loopmap
   
   pop hl
   pop bc
    
  ld de,(maplenght)
  add hl,de

  exx
  pop hl
  ld de,(maplenght)
  add hl,de
  dec c
  exx
  
  jp nz,.looptotal
  


  ret


writehorizontalobject:

  ld e,(iy+1)
  ld d,0
  
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;addglobalposition to posx
  ld a,(ix+v5)
  ld e,(iy)
  ld d,0
  add a,e
  ld e,a
  add hl,de ;add x
  
  ld a,(iy+3)
  add a,e
  ld e,a

  ld a,(maplenght)
  sub e
  ret c ;object is gone
  and a
  ret z
  
  push hl
  
  
  ld a,(maplenght)
;  dec a
  sub e
  ret c ;object is gone
  and a
  ret z
  ld b,a ;write tiles
  
  push de
  exx
  pop de
  exx
  
  ld a,(iy+4)
  ld (ix+v3),a
  
  ld c,a;4 ;4 rows
  ld de,.tiledata
  
.bigloop:  
  
  ld a,(de)
  
  push de
  push bc
  push hl
  
.loop:
  ld (hl),a
  inc hl
  djnz .loop
    
  pop hl
  pop bc
  
  ld de,(maplenght)
  add hl,de ;next row
  
  pop de
  
  ld a,(ix+v3)
  cp c
  jp nz,.keeptile
  
  inc de ;do not inc when we have rows to handle

.keeptile:
  
  ld a,c
  cp 2
  call z,.inctile
  
  dec c
  jp nz,.bigloop
  
  pop hl
  push hl

  ld b,(iy+4) ;get inner part from ram
  dec b
  dec b  

  ld de,(maplenght)
  
  ld a,228
  ld (hl),a
  add hl,de
  
.frontloop:  
  ld a,229
  ld (hl),a
  add hl,de
  djnz .frontloop

;  ld (hl),a
;  add hl,de
  
  
  ld a,230
  ld (hl),a

;  pop de
  pop hl
  inc hl
  
  exx
  push de
  exx  
  pop de
  
  ld a,(maplenght)
  dec a
  sub e
  ret c ;object is gone
  and a
  ret z  
  
  ld b,(iy+4) ;get inner part from ram
  dec b
  dec b
  
  ld de,(maplenght)

  ld a,236
  ld (hl),a
  add hl,de


.frontloop2:  
  ld a,174
  ld (hl),a
  add hl,de
  djnz .frontloop2
  
  
  ld a,232
  ld (hl),a  
  


  ret

.inctile:

  inc de
  ret

.tiledata: db 234,173,221


horizontalobjectlist:
;horx (objectwidth, tiles), hory (position, tiles), direction (0 = left, 1 = right), boundary (no tilecopies allowed), thickness minimum = 4, intersect = object is sharing the same line woth another one (only needed for the right moving one)
db  009,  235,  0,  32-028, 4,  0 ;lowest object
horizontalobjectlistlenght: equ $ - horizontalobjectlist
db  012,  231,  0,  32-028, 4,  0 ;1
db  015,  227,  0,  32-028, 4,  0 ;2
db  000,  223,  1,  32-028, 4,  0 ;3
db  012,  209,  0,  32-028, 4,  0 ;4
db  015,  201,  0,  32-028, 4,  0 ;4
db  000,  191,  1,  32-017, 6,  0 ;4
db  020,  183,  0,  32-026, 4,  0 ;left one always first if oposite
db  000,  183,  1,  32-013, 6,  1 ;4

db  002,  159,  0,  32-032, 4,  0 ;left one always first if oposite
db  000,  159,  1,  32-028, 4,  1 ;4

db  004,  151,  1,  32-009, 4,  0 ;2
db  004,  147,  1,  32-007, 4,  0 ;2
db  004,  143,  1,  32-005, 4,  0 ;2

db  016,  127,  0,  32-028, 4,  0 ;1
db  016,  119,  0,  32-028, 4,  0 ;1
db  018,  115,  0,  32-028, 4,  0 ;1
db  016,  109,  0,  32-028, 6,  0 ;1
db  020,  101,  0,  32-028, 8,  0 ;1
db  018,  095,  0,  32-028, 6,  0 ;1

db  014,  059,  0,  32-032, 4,  0 ;left one always first if oposite
db  000,  059,  1,  32-028, 4,  1 ;4

db  016,  051,  0,  32-028, 4,  0 ;left one always first if oposite
db  024,  041,  0,  32-028, 4,  0 ;left one always first if oposite


db  255 ;end boundary

;how to calculate boundary: 32-(rightmaptilex+horx)


MovementPattern229:     ;dry bones

  ld a,(ix+v4)
  dec a
  jp z,.collapse
  dec a
  jp z,.wait
  dec a
  jp z,.shake
  dec a
  jp z,.riseup  
  dec a
  jp z,.trowbone

  ld a,(ix+objectrelease) ;make sure this guy can trow bones
  and a
  call nz,.handlebonetrow

  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  
  call  checkobjecttilesfloorbelowfeet
  jp c,.wallfound
  
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret

.trowbone:

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe2

  ;stop wait and then trow the bone
  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c

  ld (ix+v4),0 ;reset for now
  ld (ix+v7),0

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld (iy+v4),1

  ld (ix+v5),0 ;clear just to be sure
  ld (ix+v5+1),0

  ret

.nextframe2:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld (iy+v4),1    


    jp .nextframe


.handlebonetrow:

  inc (ix+v7) ;we can use this variable because the collapse code goes before this and nicely resets
  ld a,(ix+v7)
  cp 90
  ret c
  
;set to stop
  ld (ix+v4),5 ;state nr 5
  ld (ix+v7),0

  ;set the sprite to trow
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)



  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  push hl
  push de

  ld    a,(ix+v3)               ;v3 is used as animation variable
  and 2
  jp nz,.trowright  

;left    
  ld    hl,coloraddressDryBoneslefttrow  ;color address
  ld    bc,characteraddressDryBoneslefttrow  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  pop de
  pop hl
  push ix


  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),189                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,263
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,4
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeY),28
  ld (ix+v3),0

  ld    hl,coloraddressbone11  ;color address
  ld    bc,characteraddressbone11  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix ;save adress of main object

  ld (ix+v5),l ;save bone adress for activation
  ld (ix+v5+1),h
  

  ret

.trowright:
  ld    hl,coloraddressDryBonesrighttrow  ;color address
  ld    bc,characteraddressDryBonesrighttrow  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  pop de
  pop hl
  push ix


  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),189                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,263
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,4
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeY),28
  ld (ix+v3),2


  ld    hl,coloraddressbone12  ;color address
  ld    bc,characteraddressbone12  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix ;save adress of main object

  ld (ix+v5),l ;save bone adress for activation
  ld (ix+v5+1),h
  



  ret


.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex528  ;color address
  ld    bc,spriteaddrex528c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex529  ;color address
  ld    bc,spriteaddrex529c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex532  ;color address ;02
  ld    bc,spriteaddrex532c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex533  ;color address ;03
  ld    bc,spriteaddrex533c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.riseup:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  jp z,.animate1 
  cp 10
  ret c

  ld (ix+v7),0
  ld (ix+v4),0 ;next step

  

  ret

.shake:

  ld a,(framecounter)
  and 1
  jp z,.shakeright

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  dec hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  
  ld (ix+v7),0
  inc (ix+v4)
  
  ret


.shakeright:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ret


.wait:

  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  call  checkobjecttilesfloor ;make everything just a little bit nicer



  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  ret c
  
  ld (ix+v7),0
  inc (ix+v4)
  
  ret

.collapse:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animate1
  cp 5
  jp z,.animate2  
  cp 10
  ret c

  ld (ix+v7),0
  inc (ix+v4) ;next step

  ret


.animate1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  cp 2
  jp nc,.animate1right
  

  ld    hl,spriteaddrex530  ;color address
  ld    bc,spriteaddrex530c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset




.animate1right:

  ld    hl,spriteaddrex534  ;color address
  ld    bc,spriteaddrex534c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret

.animate2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  cp 2
  jp nc,.animate2right
  

  ld    hl,spriteaddrex531  ;color address
  ld    bc,spriteaddrex531c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset




.animate2right:

  ld    hl,spriteaddrex535  ;color address
  ld    bc,spriteaddrex535c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret


.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  

  call Playsfx.bonestomp
 ; inc (ix+v4)
  ld (ix+v4),1 ;better to set the variable this way
  ld (ix+v7),0
  

  ret


.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



MovementPattern228:     ;thwomp

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.fall
  dec a
  jp z,.wait
  dec a
  jp z,.gobackup

  call .animate1

  call getmariodistance
  ld a,c
  cp 64
  push bc
  call c,.animate2
  pop bc
  
  ld a,c
  cp 32
  ret nc

  
  call .animate3

  ld (ix+v4),1 ;nextframe
  ld (ix+v2),8

  ret

.gobackup:

  call addv2toybject

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  ret c    
  
  ld (ix+v7),0
  ld (ix+v4),0
  ld (ix+v2),0


  ret


.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  
  inc (ix+v4);nextstep
  ld (ix+v2),-2
  ld (ix+v7),0
  
  call .animate1
  
  ret


.fall:

  call addv2toybject
  

  ld bc,32+8
  ld de,0
  call objecttilecheckfromrom
  cp 191
  ret c
  
  ld a,1
  ld (tripscreen),a
  
  call Playsfx.explosion
  
  inc (ix+v4)
  ld (ix+v7),0

  ret



.animate1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex525 ;color address
  ld    bc,spriteaddrex525c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


  ;ret

.animate2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex526 ;color address
  ld    bc,spriteaddrex526c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


  ;ret

.animate3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex527 ;color address
  ld    bc,spriteaddrex527c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


  ;ret


MovementPattern227:     ;ball n chain


  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,.handlechain1
  cp 3
  jp z,.handlechain2  
  
  call mariointeract

  ld b,2
  ld hl,smallrotatex
  ld de,smallrotatey
  call rotateobject

;check for other objects

  ld l,(ix+offsets+34)
  ld h,(ix+offsets+34+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 227
  call nz,.destroy

  ld l,(ix+offsets+36)
  ld h,(ix+offsets+36+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 227
  call nz,.destroy


  ret

.destroy:

  ld (iy+xobject),0
  ld (iy+xobject+1),0
  ld (iy+yobject),0
  ld (iy+yobject+1),0

.destroyself:

  ld (ix+xobject),0
  ld (ix+xobject+1),0
  ld (ix+yobject),0
  ld (ix+yobject+1),0


  ret




.handlechain1:

  ld l,(ix+offsets+34)
  ld h,(ix+offsets+34+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 227
  call nz,.destroyself


  ld b,2
  ld hl,.x32
  ld de,.y32
  call rotateobject 
  
  ret
  
.handlechain2:

  ld l,(ix+offsets+34)
  ld h,(ix+offsets+34+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 227
  call nz,.destroyself


  ld b,2
  ld hl,.x16
  ld de,.y16
  call rotateobject 
  
  ret  

.x32:
    db 0,0,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0
    db 0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,0,0,-1,0
    db 0,-1,0,0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1
    db -1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,0,0,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,0
    db -1,-1,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0
    db 0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1
    db 0,1,1,0,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,10
.y32:
    db -1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1,1,0
    db 1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1
    db 0,1,1,0,1,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0
    db 1,-1,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1
    db -1,0,-1,-1,0,-1,-1,0,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1,0
    db -1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,0,-1
    db 0,-1,0,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0

.x16:
    db 0,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0
    db 0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0
    db -1,0,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1
    db 0,0,-1,0,-1,0,0,0,0,-1,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1,0,0,-1,0,0
    db 0,-1,0,0,-1,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    db 0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,0,1
    db 0,0,1,0,1,0,0,1,0,0,1,0,1,0,10
.y16:
    db -1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0
    db 0,1,0,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0
    db 0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0
    db 1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0
    db 0,-1,0,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1,0,-1,0,0,-1,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0
    db -1,0,0,-1,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,0
    db 0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0

.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  inc de
  inc de
 ;dec e


  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),218                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  pop hl ;save adress of main object
  ld (ix+offsets+34),l
  ld (ix+offsets+34+1),h

  ld (ix+movepat),227
  ld (ix+v4),2
  ld (ix+deadly),3

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex524 ;color address
  ld    bc,spriteaddrex524c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  push ix
  pop hl


  pop ix

;save adress of object in main object
  ld (ix+offsets+34),l
  ld (ix+offsets+34+1),h

  pop de
  pop hl
  
  inc de
  inc de

  push ix
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),218                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  
  pop hl
;save adress of object in main object
  ld (ix+offsets+34),l
  ld (ix+offsets+34+1),h  

  ld (ix+movepat),227
  ld (ix+v4),3
  ld (ix+deadly),3


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex524 ;color address
  ld    bc,spriteaddrex524c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  push ix
  pop hl


  pop ix


;save adress of object in main object
  ld (ix+offsets+36),l
  ld (ix+offsets+36+1),h


  ld (ix+v4),1
  ret



MovementPattern226:     ;elevator only

  ld c,0
  call platforminteract


  ret



MovementPattern225:     ;thwimp

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.jump

  ld bc,16+8
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.nextframe


  call addv2toybject

  ret


.nextframe:

  inc (ix+v4)
  ld (ix+v7),0
  ld (ix+v1),-2

  ret

.jump:


  inc (ix+v6)
  ld a,(ix+v6)
  cp 30
  ret c
  

  ld e,(ix+v7)
  ld d,0
  ld hl,.jumptable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.setnext
  
  ld (ix+v2),a
  
  call addv2toybject
  call addv1toxbject
  
  inc (ix+v7)

  ret


.setnext:

  call Playsfx.bump

  ld (ix+v6),0

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ld (ix+v7),0
  
  ret



.jumptable: db -8,-8,-8,-7,-7,-6,-6,-5,-5,-5,-5,-4,-4,-4,-3,-2,-1,0,1,2,3,4,4,4,5,5,5,5,6,6,7,7,8,8,8,10


MovementPattern224:     ;sheer stairs (helper object)


  call followmario  

  ld bc,32+8
  ld de,0
  call objecttilecheckfromrom
  cp 183
  jp z,.lockstairs
  cp 184
  jp z,.lockstairs
  ld bc,32
  ld de,0
  call objecttilecheckfromrom  
  cp 179
  jp z,.lock
  cp 180
  jp z,.lock    
  cp 183
  jp z,.lockstairsfrombelow
  cp 184
  jp z,.lockstairsfrombelow
  ld bc,32-8
  ld de,0
  call objecttilecheckfromrom
  cp 179
  jp z,.lockup
  cp 180
  jp z,.lockup 
 

  xor a
  ld (forcetile),a
  ld (mariosheertile),a
  ld (inhibitsprite),a

  ret

.lockstairsfrombelow:

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (marioy),hl


.lockstairs:

  ld a,1
  ld (mariosheertile),a
  ld a,2 ;stairs
  ld (forcetile),a

  call lockmariotile ;eerst mario nog even locken op de tile


  ret

.lockup:

  ld hl,(marioy)
  ld de,8
  xor a
  sbc hl,de
  ld (marioy),hl
  

.lock:

  ld a,1
  ld (forcetile),a

  call lockmariotile ;eerst mario nog even locken op de tile
  
  ret



MovementPattern223:     ;the big boo

  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,.handlepart2

  ld a,(ix+lockdata+1)
  cp 5
  jp z,.killboss
  cp 6
  jp z,.endlevel

  ld a,(ix+v7)
  and a
  jp nz,.handlenextframe
  
  call mariointeract
  call objectinteract
  ld a,(iy+movepat)
  cp 95
  push af
  call z,.setnextframe
  pop af
  cp 96
  push af
  call z,.setnextframe
  pop af
  cp 99
  push af
  call z,.setnextframe
  pop af
  cp 111
  call z,.setnextframe      
 
  call .getrotationdir
;  ld b,2
  call .getspeed
  call rotateobject  
  
  ld a,(framecounter)
  and 3
  call z,.animate
  
  call .moveboo2 

  ret

.smallrotatex:
    db -1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,0,0,-1,0,0,0,0,0
    db 0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1
    db 1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,1,2,1,1,1,1,1,1,1,2
    db 1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0
    db 0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1
    db -1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-2,-1,-1
    db -1,10

.endlevel:


  ld l,(ix+v6)
  ld h,(ix+v7)
  dec hl
  ld (ix+v6),l
  ld (ix+v7),h
  ld de,0
  xor a
  sbc hl,de
  ret nz
  
 
  ld a,1
  ld (levelend?),a
  ld a,3
  ld (nocontrol),a



  ret



.getspeed:

  ld b,2

  ld a,(ix+lockdata)
  cp 30
  ret nc
  inc (ix+lockdata)

  ld b,1

  ret


.setnextframe:

  ld a,(ix+v7)
  and a
  ret nz

  push ix
  push iy
  
  pop ix
  
  call destroyobjectwithcloud
  
  pop ix
  

  dec (ix+lockdata+1)
  jp z,.setdestroyobject

  ld (ix+v7),1
 ; ld (ix+deadly),3
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;make affraid
 
  jp Playsfx.chuckauw
 
 ; ret


.setdestroyobject:

  ld (ix+lockdata+1),5
  ld (ix+v6),0
  ld (ix+v7),0

  call killallenemies

  call Playsfx.chuckauw

  ret

.killboss:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  jp z,.deathsfx
  cp 30
  ret c
  
  ld (ix+v7),30
  
  ld e,(ix+v6)
  ld d,0
  ld hl,.bigboodietable
  add hl,de
  ld a,(hl)
  cp 8
  jp z,.maxspeed
  
  ld (ix+v2),a
  call addv2toybject
  call .moveboo2
  
  inc (ix+v6)


  ret
  
.maxspeed:

  ld (ix+v2),8
  call addv2toybject
  call .moveboo2

  ld hl,(mapheightx8)
  ld de,32
  xor a
  sbc hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.setendlevel

  ret
 
.deathsfx: 
 
   jp Playsfx.bossdies 
  
.bigboodietable:  db -3,-3,-2,-2,-1,-1,0,1,1,2,2,3,3,4,5,6,7,8  
  

.setendlevel:

  inc (ix+lockdata+1)
  
  ld a,2
  ld (playwinmusic),a

  xor a
  ld (time),a
 ; ld (completedfirsttime),a ;always reset this byte upon exit
  
 ; ld a,(completed)
 ; and a
 ; call z,.setfirsttime    

  ld hl,350

  ld (ix+v6),l
  ld (ix+v7),h

  call .makebooblank

  ret
  
;.setfirsttime:
;
;    ld a,1
; ld (completedfirsttime),a
;
;    ret

.handlenextframe:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  jp c,.animatedis

  ld a,(ix+v7)
  cp 30
  jp z,.changedir
  cp 31
  jp z,.resetbooposition
  cp 60
  ret c  
 
  ;ld a,r
  call makerandomnumber
  ld b,a
  call .getrotationdir
  ld a,(ix+v7)
  cp 150
  call c,rotateobject  
  call .moveboo2  
 
  ld a,(ix+v7)
  cp 150
  push af
  call z,Playsfx.magicbeam
  pop af  
  ret c 
  
  ld a,(ix+v7)
  cp 180
  jp c,.animatedis 
  
 ;   ld (ix+deadly),2
  ld (ix+v7),0
  ld (ix+lockdata),0
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;make pissed
 
  
  ret

.animatedis:

  call .animate
  
  ld a,(framecounter)
  and 1
  call z,.makebooblank
  
  ret

.resetbooposition:

  ld hl,104
  ld de,32 ;boo startingcoordinates
 
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),e
  ld (ix+yobject+1),d  
  ld (ix+v5),0
  ld (ix+v5+1),0 ;reset rotation counter too
  
  ret

.changedir:
  
  call .makebooblank  
  
  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a  


  ret

.makebooblank:

  ld l,(ix+objectrelease)
  ld h,(ix+edible)
  push hl
  pop iy

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    (iy+updatespr?),2       ;update sprite (this takes 2 frames)

;make boo dissapear
  ld    de,offsetgone
  call    setcharcolandoffset.offsetsonly  

  push ix
  push iy
  
  pop ix

  ld    de,offsetgone
  call    setcharcolandoffset.offsetsonly   
  
  pop ix
  
  ret 
    

.getrotationdir:

  ld hl,smallrotatex
  ld de,smallrotatey

  ld a,(ix+v6)
  and a
  ret z

  ld hl,.smallrotatex
  ld de,smallrotatey

  ret




.handlepart2:
  
  ld l,(ix+objectrelease)
  ld h,(ix+edible)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 223
  ret z
  

  ld hl,0
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),l
  ld (ix+yobject+1),h  


  ret


.moveboo2:

  ld l,(ix+objectrelease)
  ld h,(ix+edible)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 223
  jp nz,.destroy
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)

  ld (iy+xobject),l
  ld (iy+xobject+1),h


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  ld (iy+yobject),l
  ld (iy+yobject+1),h


  ret

  
.destroy:

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h



  ret



.init:

  ;spawn the second sprite and parse ix into main object
  ;object 210 has 6 sprites that one can be used

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),210                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+movepat),223
  ld (ix+v4),2
  ld (ix+deadly),3
  ld (ix+clipping),6
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex514+(12*16)  ;color address
  ld    bc,spriteaddrex514c+(12*32)  ;character address
  ld    de,offsetsboo2
  ld    a,ingamespritesblock3
  call    setcharcolandoffset
  
  push ix
  pop hl

  pop ix
 
  ld (ix+objectrelease),l
  ld (ix+edible),h

  push hl
  pop iy
  
  push ix
  pop hl

  ld (iy+objectrelease),l
  ld (iy+edible),h  
  

  inc (ix+v4)
  ld (ix+lockdata),0 ;abuse as variable
  ld (ix+lockdata+1),3 ;abuse as variable three hits maximum
  ld (ix+clipping),6  

  ret  

;handle full animation from this point
.animate:

  ld l,(ix+objectrelease)
  ld h,(ix+edible)
  push hl
  pop iy

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    (iy+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp z,.position0 ;left/right
  cp 1
  jp z,.position1 ;right/left
  cp 2
  jp z,.position2 ;left affraid
  cp 3
  jp z,.position3 ;right affraid

  ret

.position0:

  call getmarioleftright
 ; jp nc,.position0
  jp c,.position1


  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex514+(12*16)  ;color address
  ld    bc,spriteaddrex514c+(12*32)  ;character address
  ld    de,offsetsboo2
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix

  ld    hl,spriteaddrex514  ;color address ;03
  ld    bc,spriteaddrex514c  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

.position1:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex518+(12*16)  ;color address
  ld    bc,spriteaddrex518c+(12*32)  ;character address
  ld    de,offsetsboo2right
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex518  ;color address ;03
  ld    bc,spriteaddrex518c  ;character address
  ld    de,offsetbooright
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
.position2:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex517+(12*16)  ;color address
  ld    bc,spriteaddrex517c+(12*32)  ;character address
  ld    de,offsetsboo2a
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex517  ;color address ;03
  ld    bc,spriteaddrex517c  ;character address
  ld    de,offsetbooa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset   

.position3:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex519+(12*16)  ;color address
  ld    bc,spriteaddrex519c+(12*32)  ;character address
  ld    de,offsetsboo2a
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex519  ;color address ;03
  ld    bc,spriteaddrex519c  ;character address
  ld    de,offsetboorighta
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



MovementPattern222:     ;questionmark animation (pbox)

  ld a,(ix+v7)
  and a
  jp z,.init ;spawn the others to prevent spawning bug in ghosthouse


  ld a,(bluepboxactive)
  and a
  jp z,.deactivate

  ld a,(ix+v4)
  and a
  ret nz
  
  
  ;read bkg tiles into RAM
  call getblockcoordinates ;get the object bkgadress 
  inc hl
  push hl

 ; call readanytile
 ;ld a,114
 ; ld (ix+offsets+25),a
 ; inc hl
 ; call readanytile
 ;ld a,115
 ; ld (ix+offsets+26),a    
 ; ld de,(maplenght)
 ; add hl,de
 ; dec hl
 ; call readanytile
 ;ld a,146
 ; ld (ix+offsets+27),a  
 ; inc hl
 ; call readanytile 
 ;ld a,147
 ; ld (ix+offsets+28),a    

  pop hl

  ld a,192
  call writeanytile
  inc hl
  ld a,193
  call writeanytile  
  ld de,(maplenght)
  add hl,de
  dec hl
  ld a,194
  call writeanytile
  inc hl
  ld a,195
  call writeanytile 


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
 ; dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e



;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),57                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld a,r
  ld (ix+offsets+25),a ;save unique ID number

  push ix
  pop hl

  pop ix

  ld (ix+v3),a

;remember that mario can destroy this object so double check it when taking it out!
  ld (ix+v5),l
  ld (ix+v5+1),h


  inc (ix+v4)


  ret


.deactivate:

;  ld a,(ix+v4)
;  and a
;  ret z
  
  
  ;read bkg tiles into RAM
  call getblockcoordinates ;get the object bkgadress 
  inc hl


  ;ld a,(ix+offsets+25)
  ld a,114
  call writeanytile
  inc hl
  ;ld a,(ix+offsets+26)
  ld a,115
  call writeanytile  
  ld de,(maplenght)
  add hl,de
  dec hl
  ;ld a,(ix+offsets+27)
  ld a,146
  call writeanytile
  inc hl
  ;ld a,(ix+offsets+28)
  ld a,147
  call writeanytile 
  
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(ix+v3)
  ld c,a
  ld a,(iy+offsets+25)
  cp c
  ret nz ;compare and see if unique id matches
  
  
  ld (iy+xobject),0
  ld (iy+yobject),0
  ld (iy+xobject+1),0
  ld (iy+yobject+1),0

  ld (ix+v4),0

  ret


.init:

  ld a,(currentlevel)
  cp 35
  jp nz,.skipthis


 ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
 ; dec hl
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e

 ld b,2

.createblocks:

  push bc
  push hl
  push de
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),215                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  

 ;ld a,114
 ; ld (ix+offsets+25),a
 ;ld a,115
 ; ld (ix+offsets+26),a    
 ;ld a,146
 ; ld (ix+offsets+27),a  
 ;ld a,147
 ; ld (ix+offsets+28),a     
  
  
  
  ld (ix+v7),1

  pop ix

  pop de
  pop hl
  pop bc
  
  inc hl
  inc hl
  
  djnz .createblocks  

.skipthis:

  inc (ix+v7)
  ret


MovementPattern221:     ;eerie (left right)/(high up and down)
  ld a,(ix+v4)
  and a
  jp z,.init

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success


  ld a,(framecounter)
  and 7
  call z,.animate

  inc (ix+v7)
  ld e,(ix+v7)
  ld d,0
  ld hl,.updowntable
  ld a,(ix+objectrelease)
  and a
  call nz,.setupdownhigh
  add hl,de
  
  ld a,(hl)
  cp 10
  call z,.resettable

  ld (ix+v2),a
  
  call addv1toxbject
  call addv2toybject


  ret

.updowntablehigh: db 0,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,-3,-3,-3,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,1,1,1,1,1,2,2,2,2,2,3,3,3,2,2,2,2,2,1,1,1,1,1,0,10
.updowntable: db 0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,-1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1,10

.resettable:

  ld (ix+v7),0
  xor a
  ret

.setupdownhigh:

    ld hl,.updowntablehigh
    ret


.maxspeed:

  ld (ix+v2),1
  
  call addv2toybject

  ld a,(framecounter)
  and 1
  ret z
  
  call addv1toxbject

  ret


.animate:

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex520  ;color address
  ld    bc,spriteaddrex520c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex521  ;color address
  ld    bc,spriteaddrex521c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex522  ;color address
  ld    bc,spriteaddrex522c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex523  ;color address
  ld    bc,spriteaddrex523c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;  ret


.init:

  inc (ix+v4)
  call getmarioleftright
  ret nc
  
  ld (ix+v1),2
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a
  ret


MovementPattern220:     ;one way door

  ld a,(currentlevel)
  cp 35
  jp z,.checkotherside

  call getmarioleftright
  ret nc

  call getmariodistance
  ld a,c
  cp 16
  ret c

  call getblockcoordinates ;get the object bkgadress

  ld b,4
  
.writeblocks:  

  ld a,15;200
  call writeanytile
  inc hl
  ld a,216;201
  call writeanytile  
  ld de,(maplenght)
  add hl,de
  dec hl
  ld a,47;202
  call writeanytile
  inc hl
  ld a,217;203
  call writeanytile   
  ld de,(maplenght)
  add hl,de
  dec hl


  djnz .writeblocks

  ld a,1
  ld (tripscreen),a

  call Playsfx.explosion

  jp destroyobject

.checkotherside:

  call getmarioleftright
  ret c
  

  call getmariodistance
  ld a,c
  cp 24
  ret c

  call getblockcoordinates ;get the object bkgadress

  ld b,4
  
.writeblocks2:  

  ld a,218
  call writeanytile
  inc hl
  ld a,219
  call writeanytile  
  ld de,(maplenght)
  add hl,de
  dec hl
  ld a,220
  call writeanytile
  inc hl
  ld a,221
  call writeanytile   
  ld de,(maplenght)
  add hl,de
  dec hl


  djnz .writeblocks2

  ld a,1
  ld (tripscreen),a

  call Playsfx.explosion

  jp destroyobject




movebigboo:

  ld a,(fadeoutscreen?)
  and a
  call z,mariointeract

  call addv1toxbject
  call addv2toybject

;TODO: ARGH They ACCELERATE!!
  call .getsety
  call .getsetx

  call .acceleratex
  call .acceleratey

  ld a,(ix+hitpoints)
  cp 3
  call z,.stopy
  ld a,(ix+v6)
  and a
  call z,.stopx


  ld a,(framecounter)
  and 3
  jp z,.animate

  
  ret


.stopx:

  ld a,(ix+v3)
  cp 2
  jp z,.stopxleft
  
 
  ld hl,.ghostacctable
  ld a,(ix+v7)
  ld e,a
  and a
  jp z,.nospeedx
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1

  dec (ix+v7)
  
  ld (ix+v1),a
  
  ret

.set1:

  ld a,1
  ret

.stopxleft:
 
  ld hl,.ghostacctable
  ld a,(ix+v7)
  ld e,a
  and a
  jp z,.nospeedx
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1  

  dec (ix+v7)
  
    neg
  ld (ix+v1),a
  
  ret

.nostopy:

  ld (ix+hitpoints),0
  ld (ix+v2),0
  ld (ix+objectrelease),0
  ret

.stopy:

  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  xor a
  sbc hl,de ;get y value what needs to be done
  jp c,.stopyleft
  jp z,.nostopy
 
  ld hl,.ghostacctable
  ld a,(ix+objectrelease)
  ld e,a
  and a
  jp z,.nospeedy
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1

  dec (ix+objectrelease)
  
  ld (ix+v2),a
  
  ret


.stopyleft:
 
  ld hl,.ghostacctable
  ld a,(ix+objectrelease)
  ld e,a
  and a
  jp z,.nospeedy
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1  

  dec (ix+objectrelease)
  
    neg
  ld (ix+v2),a
  
  ret


.nospeedx:

  ld (ix+v1),0

  ret

.nospeedy:

  ld (ix+v2),0

  ret


.getsetx:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,(mariox)
  xor a
  sbc hl,de ;get x value what needs to be done
  jp c,.setxmin
  jp z,.setxzero


  ld a,(marioleftrightspr)
  dec a
  jp z,.setxyzero
  
  ld (ix+v6),1
  ld (ix+v3),0 ;left

  ret

.setxmin:

  ld a,(marioleftrightspr)
  and a
  jp z,.setxyzero


  ld (ix+v6),2
  ld (ix+v3),1 ;right

  ret

.setxyzero:

  ld (ix+v2),0
  ld a,(ix+v3)
  cp 2
  jp nc,.setxzero2
  
  inc (ix+v3)
  inc (ix+v3)

.setxzero:

  ld (ix+v1),0
  ld (ix+v6),3 ;stop with table
;  ld (ix+v7),0
;  ld (ix+hitpoints),3 ;stop with table
;  ld (ix+objectrelease),0

  ret  

.setxzero2:

;  ld (ix+v1),0
  ld (ix+v6),0 ;stop with table
;  ld (ix+v7),0
  ld (ix+hitpoints),3 ;stop with table
;  ld (ix+objectrelease),0


  ret


.getsety:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  add hl,de
  ex de,hl
  ld hl,(marioy)
  xor a
  sbc hl,de ;get y value what needs to be done
  jp c,.setymin
  jp z,.setyzero
  
  ld (ix+hitpoints),2

  ret

.setymin:

  ld (ix+hitpoints),1

  ret

.setyzero:

  ld (ix+hitpoints),0
  ld (ix+v2),0

  ret  


.acceleratex:

  ld a,(ix+v6)
  and a
  ret z
  cp 1
  jp z,.accright
  cp 3
  ret z

  ld hl,.ghostacctable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setxspeedmaxleft
  
  ld (ix+v1),a
  
  inc (ix+v7)



  ret


.setxspeedmaxleft:

  ld (ix+v1),1
  ret


.accright:
  ld hl,.ghostacctable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setxspeedmaxright
  
  neg
  ld (ix+v1),a
  
  inc (ix+v7)



  ret


.setxspeedmaxright:

  ld (ix+v1),-1
  ret




.acceleratey:


  ld a,(ix+hitpoints)
  and a
  ret z
  cp 1
  jp z,.accrighty
  cp 3
  ret z

  ld hl,.ghostacctable
  ld e,(ix+objectrelease)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setyspeedmaxleft
  
  ld (ix+v2),a
  
  inc (ix+objectrelease)



  ret


.setyspeedmaxleft:

  ld (ix+v2),1
  ret


.accrighty:
  ld hl,.ghostacctable
  ld e,(ix+objectrelease)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setyspeedmaxright
  
  neg
  ld (ix+v2),a
  
  inc (ix+objectrelease)



  ret


.setyspeedmaxright:

  ld (ix+v2),-1
  ret






.animate:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    (iy+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp z,.position0 ;left
  cp 1
  jp z,.position1 ;right
  cp 2
  jp z,.position2 ;left affraid
  cp 3
  jp z,.position3 ;right affraid

  ret

.position0:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex514+(12*16)  ;color address
  ld    bc,spriteaddrex514c+(12*32)  ;character address
  ld    de,offsetsboo2
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix

  ld    hl,spriteaddrex514  ;color address ;03
  ld    bc,spriteaddrex514c  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

.position1:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex518+(12*16)  ;color address
  ld    bc,spriteaddrex518c+(12*32)  ;character address
  ld    de,offsetsboo2right
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex518  ;color address ;03
  ld    bc,spriteaddrex518c  ;character address
  ld    de,offsetbooright
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
.position2:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex517+(12*16)  ;color address
  ld    bc,spriteaddrex517c+(12*32)  ;character address
  ld    de,offsetsboo2a
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex517  ;color address ;03
  ld    bc,spriteaddrex517c  ;character address
  ld    de,offsetbooa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset   

.position3:

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex519+(12*16)  ;color address
  ld    bc,spriteaddrex519c+(12*32)  ;character address
  ld    de,offsetsboo2a
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  pop ix


  ld    hl,spriteaddrex519  ;color address ;03
  ld    bc,spriteaddrex519c  ;character address
  ld    de,offsetboorighta
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.ghostacctable: db 0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,1,1,0,1,1,0,1,5




MovementPattern219:     ;big boo

  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,.handlepart2
  
;  ld (ix+v1),-1
;  call addv1toxbject
  
  call movebigboo
  
  call .moveboo2 


  ret

.handlepart2:
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 219
  ret z
  

  ld hl,0
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),l
  ld (ix+yobject+1),h  


  ret


.moveboo2:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 219
  jp nz,.destroy
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)

  ld (iy+xobject),l
  ld (iy+xobject+1),h


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  ld (iy+yobject),l
  ld (iy+yobject+1),h


  ret

  
.destroy:

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h



  ret



.init:

  ;spawn the second sprite and parse ix into main object
  ;object 210 has 6 sprites that one can be used

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e



;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),210                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+movepat),219
  ld (ix+v4),2
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex514+(12*16)  ;color address
  ld    bc,spriteaddrex514c+(12*32)  ;character address
  ld    de,offsetsboo2
  ld    a,ingamespritesblock3
  call    setcharcolandoffset
  
  push ix
  pop hl

  pop ix
 
  ld (ix+v5),l
  ld (ix+v5+1),h

  push hl
  pop iy
  
  push ix
  pop hl

  ld (iy+v5),l
  ld (iy+v5+1),h  
  

  inc (ix+v4)

  ret  



MovementPattern218:     ;small ghostring

  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.rotateset
  dec a
  jp z,.rotate
  

  


  ret


.rotateset:

  ld a,(ix+v6)
  ld b,a
 ; ld b,32
  ld hl,.rotatingplatformtablex
  ld de,.rotatingplatformtabley
  call rotateobject

  ld a,(ix+v6)
  ld b,a
 ; ld b,32
  ld hl,.rotatingplatformtablex
  ld de,.rotatingplatformtabley
  call rotateobject

  inc (ix+v4)
  ret


.rotate:

  call mariointeract

  ld b,2
  ld hl,.rotatingplatformtablex
  ld de,.rotatingplatformtabley
  call rotateobject

  ret

.rotatingplatformtablex:
    db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1
    db 1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1
    db 0,1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1
    db -1,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,-1,0
    db 0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1,-1
    db -1,0,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1
    db -1,0,-1,-1,0,-1,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,0,-1
    db 0,-1,0,-1,0,0,-1,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,-1,1,0,0
    db 0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,0,1,0,1,0,1,0,1
    db 0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1
    db 1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,10
.rotatingplatformtabley:
    db 0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,0,1,0,1
    db 0,1,0,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1
    db 1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1
    db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1
    db 1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,0
    db 1,0,1,0,0,1,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1
    db -1,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,-1,0
    db 0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1,-1
    db -1,0,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1
    db -1,0,-1,-1,0,-1,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,0,-1
    db 0,-1,0,-1,0,0,-1,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,-1

.checkdistance:

  call getmarioleftright
  jp nc,destroyobject

  call getmariodistance
  ld a,c
  cp 30
  ret nc


  jp .skipdistancecheck  

.init:
  
  ld a,(currentlevel)
  cp 35
  jp z,.checkdistance
  
  call getmarioleftright
  jp nc,.skipdistancecheck


  ret
;  call getmariodistance
;  ld a,c
;  cp 90
;  ret nc

.skipdistancecheck:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e


;  inc hl
;  inc hl
;  inc hl
;  inc e

  ld a,(ix+v6)
  add a,24;20
  ld (ix+v6),a

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
;one extra ghost
;  push hl
;  push de
  push ix
  push af

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),211                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  inc (ix+v4)
  pop af
  ld (ix+v6),a

  ld a,r
  and 1
  call nz,.set1


  pop ix
 
   
;  pop de
;  pop hl
;/one extra ghost

  inc (ix+v7)
  ld a,(ix+v7)
  cp 8;9
  ret c

  inc (ix+v4)
  inc (ix+v4)
 ; inc (ix+v4)
  ret


.set1:

  ld a,(framecounter)
  and 1
  jp nz,.set2
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex515  ;color address
  ld    bc,spriteaddrex515c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.set2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex516  ;color address
  ld    bc,spriteaddrex516c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;ret



MovementPattern217:     ;falling platform

  ld c,0
  call platforminteract

  ld a,(ix+v4)
  and a
  jp nz,.fall

  call mariointeract
  jp c,.activate

  


  ret

.activate:

  inc (ix+v4)
  ret


.fall:


  ld e,(ix+v7)
  ld d,0
  ld hl,.fallingblocktable
  add hl,de
  ld a,(hl)
  cp 8
  jp z,.maxspeed

  ld (ix+v2),a
  
  call addv2toybject

  inc (ix+v7)

  ret


.maxspeed:

  ld (ix+v2),a
  
    call addv2toybject
    
    ret

.fallingblocktable: db 0,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,4,4,4,5,6,7,8


MovementPattern216:     ;muncher


  call mariointeract


  ld a,(framecounter)
  and 3
  jp z,.animate


  ret

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
;  dec   a
;  jp    z,.position2
;  dec   a
;  jp    z,.position3

.position0:
  ld    hl,spriteaddrex507  ;color address
  ld    bc,spriteaddrex507c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex508  ;color address
  ld    bc,spriteaddrex508c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
;ret



MovementPattern215:     ;ice (helper object)
  
  ld a,(ix+v4)
  and a
  jp z,.init


  call followmariohigh
  
  
  call .accfastslow
  call .accslow
  
  ld a,(mariofloating)
  and a
  ret nz
  
  ld a,(marioslowdownpointer)
  and a
  jp z,.reset
  
  ld a,(ix+v7)
  cp 12;8
  ret nc
  
  call .fetchpointer
  
;  ld a,27
  ld (marioslowdownpointer),a
  
  inc (ix+v7)
  
  ret

.reset:

  ld (ix+v7),0
  ret


.fetchpointer:

  ld a,(ix+v5)
  ld c,a
  push bc

  ld a,(ix+v7) ;pointer was allready fetched once
  cp 2
  pop bc
  ld a,c
  ret nc

  ld a,(marioslowdownpointer)
  ld (ix+v5),a


  ret


.init:

  ld a,(iceactive)
  and a
  jp nz,destroyobject


  inc (ix+v4)

  ld a,1
  ld (iceactive),a

  ret



.accfastslow:

  ld a,(framecounter)
  and 3
  ret nz

 ld a,(mariospeedtablep)
 and a
 ret z
 dec a
 ld (mariospeedtablep),a


  ret


.accslow:

  ld a,(framecounter)
  and 1
  ret nz
  
 ld a,(mariodspeedtablep)
 and a
 ret z
 dec a
 ld (mariodspeedtablep),a
 
  ret



MovementPattern214:     ;vineseparator (helper object)



    ld a,(currentlevel)
    cp 143
    jp z,.invert ;stupid level specific hack


  call getmarioleftright
  jp nc,.right

.left:
  
  xor a
  ld (vineactive),a


  ret

.right:

  ld a,1
  ld (vineactive),a

  ret

  
.invert:
  

  call getmarioleftright
  ret nc
 ; jp nc,.left
 
  jp .left
    
  
 


MovementPattern213:     ;spring

  inc (ix+v4)
  ld a,(ix+v4)
  cp 1
  jp z,.animate1
  cp 2
  jp z,.animate1
  cp 3
  jp z,.animate2 
  cp 4
  jp z,.animate2     
  cp 5
  call z,.animate3
  
  ld a,1
  ld (jumpspeed),a

  ld a,(marioclimbing)
  and a
  jp nz,.deactivate

  call checkblockabovemario
  call checktileabovemario
  jp c,.deactivate


  ld (ix+v5),0

  ld e,(ix+v7)
  ld d,0
  ld hl,.springjumptablelow
  ld a,(Controls)
  bit 4,a
  call nz,.setjumphigh
 
  ld a,(ix+v5)
  and a
  call z,.setv31
 
 
  add hl,de
  ld a,(hl)
  cp 7
  call z,.setv31
  cp 20
  jp z,.deactivate
  ld e,a

  ld hl,(marioy)
;  ld de,10
  xor a
  sbc hl,de
  ld (marioy),hl
  
  inc (ix+v7)

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  ld (mariofloatingpf),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 

  ret

.springjumptablelow:  db 9,8,7,4,3,2,1,1,1,0,0,20,20,20,20,20,20,20,20
.springjumptablehigh: db 16,16,16,12,10,8,8,6,5,4,3,2,2,2,2,1,1,0,0,20

.setjumphigh:

  ld a,(ix+v3)
  and a
  ret nz

  ld (ix+v5),1


  ld hl,.springjumptablehigh

  ret

.setv31:

  ld (ix+v3),1
  ret


.deactivate:

  ld (ix+movepat),93
  ld (ix+v7),0
  ld (ix+v4),0
  ld (ix+v3),0
  xor a
  ld (jumpspeed),a

  ret


.animate1:

  call Playsfx.spring


  xor a
 ; ld (mariospeed),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl
  
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex505  ;color address
  ld    bc,spriteaddrex505c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.animate2:

  xor a
 ; ld (mariospeed),a

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de  
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex506  ;color address
  ld    bc,spriteaddrex506c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.animate3:

  xor a ;release mario
  ld (nocontrol),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex504  ;color address
  ld    bc,spriteaddrex504c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret


;here is the big trick. Mariosprites are written to VRAM BEFORE the eventhandler kicks in.
;this comes out very handy. The mariosprite engine is allready big enough and I need to avoid any more problems
;therefore the diagonal block changes the mariosprite and takes actual control of mario. It reads the running byte
;and this way finds out if the player stops any running. The object checks the tiles and see how mario should change.

;taken from putmariosprite I am not going to recode this again...
;In = is spriteadress in HL set block with this code as well.
writemariosprite2:; there is no more space in ram so I copied this code to this block again...

  push hl

  ;put hero sprite character
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	ld de,448 ;+14 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,$19
  call block12 ;don't call anything from the generalblocks or things will turn bad


  pop hl
  push hl
	ld		c,$98
	call	outix128
;/put hero sprite character


	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,224 ;+14 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  pop hl
	ld de,128
	add hl,de
	ld		c,$98
	call	outix64

  call setpage12tohandleobjectmovement

  ret



balloonuptilecheck:

  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  ld c,a
  jp nc,.checkblock
;  call nc,.resetv2

  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,0;-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom        ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  ld c,a
  jp nc,.checkblock
;  call nc,.resetv2 

  xor a
  ret
  
  
.checkblock:

  ld    a,203                   ;alle blocks (vraagteken, uitroepteken en roterend block)
  cp    c;(hl)                    ;kijk of de huidige tile een block is
  jp    c,.endblocktilescheck


  ld    a,1
  ld    (activatenewblock?),a   ;mario activates a new block
  ld    (blocksactive?),a       ;there is now at least 1 block active, so activate blockhandler
  ld    (blockaddress),hl       ;hl points to tile in map

;PROBLEM: when a object hits the box it will cause the box to malfunction since there is a clipping value
;objectinteract will not do the proper handling since the object is high in the list. If we however disable the clipping
;the shell will no longer kill other object during its fall.
  ld (ix+clipping),0 ;DEBUG will cause bugs after a shell hits a box


.endblocktilescheck:

;      call setpage12tohandleobjectmovement
;  scf
;  ret

  
  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    233+1                     ;softtiles excluden
  ret nc
  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom        ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 233+1
  ret nc

  scf
  ret  



handlepballoon:


  ld a,1
  ld (standingonplatform),a
 
  call addv1toxbject      
  call addv2toybject

  call .movemario

  call .checktimer

  ld a,(Controls)
  bit 2,a
  jp nz,.left
  bit 3,a
  jp nz,.right

  ld a,(ix+v1)
  and a
  call nz,.brakeleftright

.checkupdown:

  ld a,(Controls)
  bit 1,a
  jp nz,.down
  bit 0,a
  jp nz,.up
  
  
  call .brakeupdown
  
  ret
  
  

.checktimer:

  call .animatemarioballoon

  ld a,(mariostate)
  ld c,(ix+v5+1)
  cp c
  jp nz,.deactivate


  ld hl,(marioballoon)
  dec hl
  ld (marioballoon),hl
  
  push hl
  
  ld de,509
  xor a
  sbc hl,de
  call z,.fresh 
  
  pop hl  
  
  
  push hl
  
  ld de,150
  xor a
  sbc hl,de
  call c,.small 
  
  pop hl
  
  ld de,0
  xor a
  sbc hl,de
  ret nz

.deactivate:

  xor a
  ld (nocontrol),a

  xor a
  ld (mariospeed),a
  ld (fallspeed),a
 ; ld a,1
  ld (marionotdown),a

  jp destroyobject

 ; ret


.fresh:

  ld (ix+hitpoints),0
;  jp .animatemarioballoon

.animatemarioballoon:

  ld a,(ix+hitpoints)
  and a
  ret nz

  ld a,(mariostate)
  dec a
  jp z,.resetv3big
  dec a
  jp z,.resetv3fire
  dec a
  jp z,.resetv3flying

  jp .resetv3
;  ld hl,mariosmallballoon
;  ld a,(ix+hitpoints)
;  and a
;  call z,writemariosprite2 

  ;ret


.small:

  ld a,(mariostate)
  dec a
  jp z,.big
  dec a
  jp z,.fire
  dec a
  jp z,.flying  

  ld (ix+hitpoints),1

  inc (ix+v3)
  ld a,(ix+v3)
  cp 14
  jp nc,.resetv3  
  cp 7
  jp nc,.smallfoward


.smallballoon:

  ld hl,mariosmallballoonsmall
  call writemariosprite2

  ret

.smallfoward:

  ld hl,mariosmallballoon
  call writemariosprite2
  
  ret

.resetv3:

  ld (ix+v3),0
  ld hl,mariosmallballoon
  call writemariosprite2

  ret



.big:

  ld (ix+hitpoints),1

  inc (ix+v3)
  ld a,(ix+v3)
  cp 14
  jp nc,.resetv3big  
  cp 7
  jp nc,.bigfoward


.bigballoon:

  ld hl,supermarioballoon
  call writemariosprite2

  ret

.bigfoward:

  ld hl,supermarioballoonsmall
  call writemariosprite2
  
  ret


.resetv3big:

  ld (ix+v3),0
  ld hl,supermarioballoon
  call writemariosprite2

  ret


.fire:

  ld (ix+hitpoints),1

  inc (ix+v3)
  ld a,(ix+v3)
  cp 14
  jp nc,.resetv3fire  
  cp 7
  jp nc,.firefoward


.fireballoon:

  ld hl,shootingmarioballoon
  call writemariosprite2

  ret

.firefoward:

  ld hl,shootingmarioballoonsmall
  call writemariosprite2
  
  ret


.resetv3fire:

  ld (ix+v3),0
  ld hl,shootingmarioballoon
  call writemariosprite2

  ret


.flying:

  ld (ix+hitpoints),1

  inc (ix+v3)
  ld a,(ix+v3)
  cp 14
  jp nc,.resetv3flying  
  cp 7
  jp nc,.flyingfoward


.flyingballoon:

  ld hl,flyingmarioballoon
  call writemariosprite2

  ret

.flyingfoward:

  ld hl,flyingmarioballoonsmall
  call writemariosprite2
  
  ret


.resetv3flying:

  ld (ix+v3),0
  ld hl,flyingmarioballoon
  call writemariosprite2

  ret


.movemario:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld (mariox),hl

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,-8
  add hl,de
  ld (marioy),hl  
  
  ret


.resetv1:

  ld (ix+v1),0
  jp .checkupdown

.resetv2:

  ld (ix+v2),0
  ret


.left:

  ld bc,2
  ld de,-4
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetv1
  ld bc,16
  ld de,-4
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetv1


  ld a,(ix+v1)
  cp -2
  jp z,.checkupdown
  cp -3
  jp z,.brakeleftright
  cp -4
  jp z,.brakeleftright
  cp -5
  jp z,.brakeleftright

  ld a,(framecounter)
  and 3
  jp nz,.checkupdown
  
  dec (ix+v1)

  jp .checkupdown
 ; ret  


.right:

;map boundary
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,24
  add hl,de
  ex de,hl
  ld hl,(maplenghtx8)
  xor a
  sbc hl,de
  jp c,.resetv1


  ld bc,2
  ld de,12
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetv1
  ld bc,16
  ld de,12
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetv1



  ld a,(ix+v1)
  cp 2
  jp z,.checkupdown
  cp 3
  jp nc,.brakeleftright

  ld a,(framecounter)
  and 3
  jp nz,.checkupdown
  
  inc (ix+v1)

  jp .checkupdown
 ; ret  

.up:

;upper map boundary
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  xor a
  sbc hl,de
  jp c,.resetv2

 

  call balloonuptilecheck
  jp c,.resetv2

  ld a,(ix+v2)
  cp -2
  ret z

  ld a,(framecounter)
  and 3
  ret nz
  
  dec (ix+v2)

  ret    
  


.down:

  ld bc,32-8
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetv2


  ld a,(ix+v2)
  cp 1
  ret z

  ld a,(framecounter)
  and 3
  ret nz
  
  inc (ix+v2)

  ret    
  


.brakeupdown:

;upper map boundary
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  xor a
  sbc hl,de
  jp c,.resetv2


  call balloonuptilecheck
  jp c,.resetv2

  ld a,(ix+v2)
  cp -1
  ret z

  ld a,(framecounter)
  and 3
  ret nz

  ld a,(ix+v2)
  bit 7,a
  jp nz,.negatey
  
  dec (ix+v2)

  ret

.negatey:

  inc (ix+v2)

  ret  


.brakeleftright:


  ld a,(framecounter)
  and 3
  ret nz
  
  ld a,(ix+v1)
  bit 7,a
  jp nz,.negate
  
  dec (ix+v1)

  ret

.negate:

  inc (ix+v1)

  ret


MovementPattern212:     ;p-balloon

  ld a,(ix+v6)
  and a
  jp nz,handlepballoon

  ld a,(ix+v4)
  and a
  jp nz,.flying


;see what has to come out
  call objectinteract
  cp 3
  jp z,.releaseobject


;Failsafe. Check the tile around us and see if the box popped by a nearby object. If yes pop anyway
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 204
  jp z,.releaseobject


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3

  ret

.animate:
 
  
.position0:
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex68c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex69  ;color address
  ld    bc,spriteaddrex69c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex70  ;color address ;02
  ld    bc,spriteaddrex70c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex71  ;color address ;03
  ld    bc,spriteaddrex71c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releaseobject:

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  inc (ix+v4)

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,-16
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld (ix+v1),2
  ld (ix+v2),-2

  ld    hl,spriteaddrex503  ;color address
  ld    bc,spriteaddrex503c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



.flying:

  call mariointeract
  jp c,.nextframe

.nevermind: ;very shitty hack quick fix

  ld a,(ix+v4)
  cp 5;15
  jp nc,.flyingright
  
  inc (ix+v4)


  call addv1toxbject
  call addv2toybject

  ret

.flyingright:

;  ld (ix+v1),1

  ld e,(ix+v7)
  ld d,0
  ld hl,pballoonfloattable
  add hl,de
  ld a,(hl)
  cp 10
  call z,.reset
 
  ld (ix+v2),a

  call addv1toxbject
  call addv2toybject

  inc (ix+v7)

  ret


.reset:

  ld (ix+v7),0
  xor a


  ret


.nextframe:

  ld a,(marioyoshi)
  and a
  jp nz,.nevermind ;DO NOT ALLOW ON YOSHI

  ld de,(marioballoon) ;check for zero

  ld hl,510 ;17 seconds
  ld (marioballoon),hl

  xor a
  ld (marioflying),a
  ld (marioflyspecial),a
  
  ex de,hl
  ld de,0
  xor a
  sbc hl,de
  jp nz,destroyobject ;prevent spawning of second helper object
  
  ld a,5
  ld (nocontrol),a
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 ;dec e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),193                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  ld (ix+movepat),212
  ld (ix+v6),1
  ld (ix+v2),-1

  ;store the mariostate
  ld a,(mariostate)
  ld (ix+v5+1),a

  ;get the current mario speedx
  ld a,(mariospeed)
  ld (ix+v1),a
  neg
  ld c,a
  
  ld a,(marioleftright)
  and a
  call z,.setleft
  
  
  call followmario

  pop ix
   
   
  ld hl,mariosmallballoonsmall
  call writemariosprite2 
   
  
  jp destroyobject

;  ret

.setleft:

  ld (ix+v1),c
  ret



pballoonfloattable: db 0,-1,0,-1,-1,-1,-1,-1,-1,-2,-1,-2,-2,-2,-1,-2,-1,-1,-1,-1,-1,-1,0,-1,0,1,0,1,1,1,1,1,1,2,1,2,1,2,1,1,1,1,1,1,0,1,10


MovementPattern211:     ;biggerfish (dumb) (left or right)

  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.die

  call objectinteract
;  cp 4
;  push af
;  call z,.wallfound
;  pop af
  cp 2
  jp z,.otherobjectdeathjump
;  cp 1
;  push af
;  call z,.wallfound
;  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  call checkmariointeractshoot
  jp c,.otherobjectdeathjump

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success


  ld a,(framecounter)
  and 7
  call z,.animate

  inc (ix+v7)
  ld e,(ix+v7)
  ld d,0
  ld hl,.updowntable
  add hl,de
  
  ld a,(hl)
  cp 10
  call z,.resettable

  ld (ix+v2),a
  
  call addv1toxbject
  call addv2toybject


  ret


.updowntable: db 0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,-1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1,10

.resettable:

  ld (ix+v7),0
  xor a
  ret
  

.otherobjectdeathjump:

  ld (ix+v4),2
  ld (ix+v3),0
  ld (ix+v7),0
  ld (ix+v2),0
  ld (ix+deadly),0
  ld (ix+clipping),0
  
  ld (ix+v1),-1
  
  call getmarioleftright
  ret c
  
  ld (ix+v1),1


  ret


.die:

  ld a,(framecounter)
  and 7
  call z,.animatedead


  ld e,(ix+v7)
  ld d,0
  ld hl,.deathtable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.maxspeed


  ld (ix+v2),a
  
  call addv2toybject


  inc (ix+v7)

  ld a,(framecounter)
  and 1
  ret z
  
  call addv1toxbject
  
  ret

.deathtable: db -3,-3,-2,-2,-1,-1,-1,-1,0,-1,0,-1,0,0,0,1,0,0,0,1,0,0,1,0,1,10


.maxspeed:

  ld (ix+v2),1
  
  call addv2toybject

  ld a,(framecounter)
  and 1
  ret z
  
  call addv1toxbject

  ret

.animatedead:

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0d
  dec   a
  jp    z,.position1d
;  dec   a
;  jp    z,.position2
;  dec   a
;  jp    z,.position3

.position0d:
  ld    hl,spriteaddrex501  ;color address
  ld    bc,spriteaddrex501c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1d:
  ld    hl,spriteaddrex502  ;color address
  ld    bc,spriteaddrex502c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;  ret

.animate:

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex497  ;color address
  ld    bc,spriteaddrex497c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex498  ;color address
  ld    bc,spriteaddrex498c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex499  ;color address
  ld    bc,spriteaddrex499c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex500  ;color address
  ld    bc,spriteaddrex500c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;  ret


.init:

  inc (ix+v4)
  call getmarioleftright
  ret nc
  
  ld (ix+v1),1
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a
  ret


MovementPattern210:     ;fish (dumb) (up and down)



  call objectinteract
;  cp 4
;  push af
;  call z,.wallfound
;  pop af
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  push af
;  call z,.wallfound
;  pop af
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success



  ld a,(framecounter)
  and 3
  call z,.animate

  ld a,(ix+v7)
  cp 64
  jp nc,.resetdir
  inc (ix+v7)


  ld (ix+v2),-1

  ld a,(ix+v4)
  and a
  call z,.down

  call addv2toybject

  ret

.down:

  ld (ix+v2),1
  ret

.resetdir:

  ld (ix+v7),0
  ld a,(ix+v4)
  xor 1
  ld (ix+v4),a
  ret


.animate:

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
;  dec   a
;  jp    z,.position2
;  dec   a
;  jp    z,.position3

.position0:
  ld    hl,spriteaddrex253  ;color address
  ld    bc,spriteaddrex253c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex254  ;color address
  ld    bc,spriteaddrex254c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


;  ret



MovementPattern209:     ;air bubble


  ld e,(ix+v7)
  ld d,0
  ld hl,.bubblemovementtable
  add hl,de
  ld a,(hl)
  cp 10
  call z,.reset
  ld (ix+v1),a


  call addv1toxbject
  call addv2toybject

  inc (ix+v7)

  ret

.reset:

  ld (ix+v7),0
  xor a
  ret
  

.bubblemovementtable: db 0,-1,0,1,0,-1,-1,0,1,1,0,-1,-1,-1,0,1,1,1,10

MovementPattern208:     ;rip van fish bubble

  ld a,(ix+v7)
  cp 8
  push af
  call z,.change
  pop af
  cp 22
  jp z,destroyobject  
  cp 16
  push af
  call nc,.pop
  pop af


  inc (ix+v7)

  ld e,(ix+v6)
  ld d,0
  ld hl,.bubblemovementtable
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  
  inc (ix+v6)


  call addv1toxbject
  call addv2toybject



  ret
  
.bubblemovementtable: db 0,-1,0,1,0,-1,-1,0,1,1,0,-1,-1,-1,0,1,1,1,0,0,0,0,0,0,0,0 
  

.change:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex490  ;color address
  ld    bc,spriteaddrex490c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  

.pop:

;  inc (ix+v7)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex491  ;color address
  ld    bc,spriteaddrex491c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  

ripvanfishactive:

  call .animate

  ld a,(ix+v7)
  cp 16
  jp c,.do
  

  call getmariodistancexy
  ;ouput total distance in A. total Y distance in B. Total X distance in C.
  ld (ix+v5),c ;x
  ld (ix+v5+1),b ;y
  ld (ix+v7),0
  
  cp 120
  jp nc,.deactivate
  
  ld (ix+objectfp),0

.do:
  
  inc (ix+v7)
  
  ld (ix+v1),0
  ld (ix+v2),0 ;reset the speeds

  ld a,(ix+v6)
  and a
  jp z,.continue
  dec (ix+v6)

  ld a,(framecounter)
  and 1
  ret z


.continue:

  
  call getmarioleftright
  push af
  call c,.xright
  pop af
  call nc,.xleft

  call getmarioheight
  and a
  push af
  call z,.ydown
  pop af
  call nz,.yup


  call checkobjecttilescheckwall
  jp c,.checky
  
  call addv1toxbject
  
  
.checky:  
  ld a,(ix+v2)
  bit 7,a
  jp z,.checkydown

  
  ld    bc,0                  ;vertical increase to tilecheck
  ld    de,9                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  ret nc
  ld    bc,0                  ;vertical increase to tilecheck
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  ret nc  
  call addv2toybject

  ret

.checkydown:
  call checkobjecttilesflooronly
  ret c
  call addv2toybject
  
  ret  



.xleft:
  
  ld a,(ix+v5)
  and a
  ret z
 
  ld (ix+v1),-1
  dec (ix+v5) 
  ret

.xright:
  
  ld a,(ix+v5)
  and a
  ret z
 
  ld (ix+v1),1
  dec (ix+v5) 
  ret


.yup:
  
  ld a,(ix+v5+1)
  and a
  ret z
  
  ld (ix+v2),-1
  dec (ix+v5+1)  
  ret

.ydown:
  
  ld a,(ix+v5+1)
  and a
  ret z
  
  ld (ix+v2),1
  dec (ix+v5+1)  
  ret

.deactivate:

  ld a,(ix+objectfp)
  cp 64
  jp c,.do

  ld (ix+v4),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v7),0
  ld (ix+v2),1
  ld (ix+v1),0
  ld (ix+v3),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex488  ;color address
  ld    bc,spriteaddrex488c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

 ; ret

.animate:

  ld a,(ix+v1)
  bit 7,a
  jp nz,.animateleft

  jp .animateright


.animateleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0l
  cp 2
  jp    z,.position1l
  cp 4
  ret c


  xor a
  ld (ix+v3),a


.position0l:
  ld    hl,spriteaddrex492  ;color address
  ld    bc,spriteaddrex492c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1l:
  ld    hl,spriteaddrex493  ;color address
  ld    bc,spriteaddrex493c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret



.animateright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 2
  jp    z,.position1
  cp 4
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex494  ;color address
  ld    bc,spriteaddrex494c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex495  ;color address
  ld    bc,spriteaddrex495c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret





MovementPattern207:     ;rip van fish



  call objectinteract
;  cp 4
;  push af
;  call z,.wallfound
;  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  call checkmariointeractshoot
  jp c,.otherobjectshotdeathjump

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success

  ld a,(ix+v4)
  and a
  jp nz,ripvanfishactive

  call getmariodistancexy
  cp 112
  jp c,.activate
  


  ld a,(framecounter)
  and 15
  call z,.makebuble



  call .animate

  call checkobjecttilesflooronly
  ret c
  call addv2toybject


  ret

.otherobjectdeathjump:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex496  ;color address
  ld    bc,spriteaddrex496c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  jp otherobjectdeathjump

.otherobjectshotdeathjump:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex496  ;color address
  ld    bc,spriteaddrex496c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset



  jp otherobjectshotdeathjump

.activate:

  inc (ix+v4)
  ld (ix+v7),100
  ld (ix+v3),0
  ld (ix+v6),35

  ret


.wallfound:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 32
  jp    z,.position1
  cp 42
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex488  ;color address
  ld    bc,spriteaddrex488c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex487  ;color address
  ld    bc,spriteaddrex487c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret

.makebuble:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 ;dec e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),200                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  ld (ix+objectfp),0

  pop ix


  ret




MovementPattern206:     ;coin moving underwater

  

  call mariointeract
  jp c,.pickupcoin


  ld a,(ix+v4)
  cp 50
  call c,.goup
  
;this is the make the coin move really slow  
  ld (ix+v1),0
  ld a,(framecounter)
  and 3
  call z,.moveonepixel  
  
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

;  ld    a,(framecounter)
;  and   3                       ;this sprite animates once every 4 frames
;  call  z,.animate
  call .animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call checkobjecttilescheckwall
  call    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checkobjecttilesfloor
  jp nc,.checkexit
  
  inc (ix+v6)
  ld a,(ix+v6)
  cp 30
  ret c
  
    ld (ix+v6),0
    ld (ix+v7),0
  
  ret


;check if this is a level end. If so remove the coin directly upon contact
.checkexit:


  ld a,(nocontrol)
  cp 3
  jp z,.pickupcoin
  
 
  ld a,(ix+v7)
  cp 2
  ret nc
 
  inc (ix+v7) 
  
  ld (ix+v4),40 ;set bounce
  ld (ix+objectfp),0
  ld (ix+v6),0
  
  

  ret



.moveonepixel:

  ld a,(ix+killshell) ;abuse this variable to push the v1
  ld (ix+v1),a

  ret


.goup:

  call shelluptilecheck.noblockcheck
  jp c,.setfall


  ld a,(ix+v4)
  cp 40
  jp nc,.goupslow

  ld (ix+v2),2
  call subv2toybject
  inc (ix+v4)
  
  xor a
  ld (ix+v2),a
  ret

.goupslow:

  ld (ix+v2),1
  call subv2toybject
  inc (ix+v4)
  
  xor a
  ld (ix+v2),a
  ld (ix+objectfp),a
  ret

.setfall:

  ld (ix+v4),50
  ld (ix+objectfp),a  
  ret

.wallfound:

;  .hardforegroundfound:
  ;change direction
  ld a,(ix+killshell)
;  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ld (ix+killshell),a

  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret

.pickupcoin:

  ld (ix+objectrelease),1

  call handlecoin
  ;put whatever is needed here

  call Playsfx.coin

  jp destroyobjectwithsparkle


MovementPattern205:    ;red flying koopa (up and down)


  call objectinteract
;  cp 4
;  push af
;  call z,.turn
;  pop af
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  push af
;  call z,.turn
;  pop af
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract
  jp c,.nextframe


  ld a,(ix+v4)
  and a
  jp nz,.down


.up:
  

;get the y value  
  ld hl,redflyingkoopatableup
  ld a,(ix+v7)
  ld e,a
  ld d,0
  add hl,de

  inc (ix+v7)

  ld a,(hl)
  cp 10
  jp z,.turn ;end of table reached
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toyobject


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  jp  z,animateflyingkoopa

  ret


.down:
  

;get the y value  
  ld hl,redflyingkoopatabledown
  ld a,(ix+v7)
  ld e,a
  ld d,0
  add hl,de

  inc (ix+v7)

  ld a,(hl)
  cp 10
  jp z,.turn ;end of table reached
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toyobject


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  jp  z,animateflyingkoopa

  ret



.turn:

  ld a,(ix+v4)
  xor 1
  ld (ix+v4),a

  ld (ix+v7),0

  ret


.nextframe:


 
;  ld a,(marioflyslide)
;  dec a
;  jp z,.setkilljump  
 
;  ld a,(movedownsheertile)
;  dec a
;  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),73  ;generic set

  
;framecounter resetten voor constante tussenstukjes
  ld (ix+deathtimer),a
  ld (ix+v4),0
  ld (ix+v1),-1
;  ld (ix+v3),0

  call Playsfx.kickenemy
  call makestarleft

  ret




;  ret

redflyingkoopatableup:    db -1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-1,-2,-1,-2,-1,-2,-1,-2,-2,-3,-2,-3,-2,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-2,-3,-2,-3,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,-0,-1,10
redflyingkoopatabledown:  db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,1,2,1,2,2,3,2,3,2,3,3,3,3,3,3,3,3,3,3,2,3,2,3,2,2,1,2,1,2,1,2,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,10

endHandleObjectMovement5:


