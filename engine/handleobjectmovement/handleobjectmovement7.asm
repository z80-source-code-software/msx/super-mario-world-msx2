;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement7:

;start = 205


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz

  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress7 - 238 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)

;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress7
;  add   hl,de
;  ;substract minimum base adres
;  ld de,238*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress7:

  jp    MovementPattern238     ;maplevel03-01c special object
  jp    MovementPattern239     ;water (mario swim)
  jp    MovementPattern240     ;replicating chuck
  jp    MovementPattern241     ;floating skull
  jp    MovementPattern242     ;blargh
  jp    MovementPattern243     ;piranjaplant2
  jp    MovementPattern244     ;secret box (pbox)
  jp    MovementPattern245     ;ghostblob
  jp    MovementPattern246     ;springboard
  jp    MovementPattern247     ;bill
  jp    MovementPattern248     ;tube up and down
  jp    MovementPattern249     ;tubecoverup for mario exit left out
  jp    MovementPattern250     ;spikey
  jp    MovementPattern251     ;music note block
  jp    MovementPattern252     ;lakitu (pipe)
  jp    MovementPattern253     ;spikeball
  jp    MovementPattern254     ;paraboomb
  jp    MovementPattern255     ;boomb (walking)
  jp    MovementPattern256     ;boomb (upsidedown)
  jp    MovementPattern257     ;silver coin
  jp    MovementPattern258     ;extra watercover
  jp    MovementPattern259     ;porcupuffer
  jp    MovementPattern260     ;dolphin (all types)
  jp    MovementPattern261     ;deadfish
  jp    MovementPattern262     ;spikedown
  jp    MovementPattern263     ;flying bone
  jp    MovementPattern264     ;bony beetle
  jp    MovementPattern265     ;msx powerup
  jp    MovementPattern266     ;reznor (boss)
  jp    MovementPattern267     ;reznor (on platform)
  jp    MovementPattern268     ;reznor (fireball)
;  jp    MovementPattern269     ;magikoopa
;  jp    MovementPattern270     ;magiball



.return:
  
  ret




MovementPattern268:     ;reznor (fireball)

  ld a,(framecounter)
  and 3
  call z,.animate

  call mariointeract

  call addv1toxbject
  call addv2toybject

  ld a,(ix+v4)
  and a
  jp nz,.fallmax

  ld (ix+v2),1
;todo: see where mario is and create a proper Y vector
  call getmariodistance
  ld a,c
  cp 10
  ret nc
  ld a,10
  sub c
  
  ld (ix+v2),a
  ld (ix+v4),1


  ret

.fallmax:

  ld (ix+v2),4
  ret



.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v3)
  ld a,(ix+v3)
  dec a
  jp z,.position0
  dec a
  jp z,.position1
  dec a
  jp z,.position2
  dec a
  ld (ix+v3),0   
  jp z,.position3
 
  ret

.position0:

  ld    hl,coloraddressreznorfireball1  ;color address
  ld    bc,characteraddressreznorfireball1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position1:

  ld    hl,coloraddressreznorfireball2  ;color address
  ld    bc,characteraddressreznorfireball2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position2:

  ld    hl,coloraddressreznorfireball3  ;color address
  ld    bc,characteraddressreznorfireball3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset
  
.position3:

  ld    hl,coloraddressreznorfireball4  ;color address
  ld    bc,characteraddressreznorfireball4;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


MovementPattern267:     ;reznor (on platform)

  ld a,(ix+v6)
  and a
  jp nz,.otherobjectdeathjump

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.turnleft
  dec a
  jp z,.turnright
  dec a
  jp z,.shoot

  inc (ix+v7)
  ld a,(ix+v7)
  cp 90
  jp nc,.setshoot

  
;TODO: wait and do some serious fireball stuff
  call getmarioleftright
  jp c,.setturnright

.setturnleft:
  
  
  ld a,(ix+v3)
  and a
  ret z ;allready left
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 30 ;wait a short while
  ret c

  ld (ix+v4),1 ;tell the engine we are turning left
  ld (ix+v7),0  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressreznorturn  ;color address
  ld    bc,characteraddressreznorturn;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

   
;  ret



.setturnright:

  ld a,(ix+v3)
  and a
  ret nz ;allready right


  inc (ix+v7)
  ld a,(ix+v7)
  cp 15 ;wait a short while
  ret c


  ld (ix+v4),2 ;tell the engine we are turning right
  ld (ix+v7),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressreznorturn  ;color address
  ld    bc,characteraddressreznorturn;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret



.shoot:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30 ;wait a short while
  ret c
  cp 30
  jp z,.spawnfireball
  cp 45
  ret c


;shoot a magic fireball the close the mouth
  ld (ix+v4),0
  ld (ix+v7),0    

  ld a,(ix+v3)
  and a
  jp nz,.spriteright
  jp .spriteleft


  ;ret



.spawnfireball:

  call Playsfx.yoshifireball

  ;object240
  ;create the fireball
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,211
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)

  call  activateobject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld hl,268
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld    hl,coloraddressreznorfireball1  ;color address
  ld    bc,characteraddressreznorfireball1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop iy

  pop ix
  
  ld (iy+v1),-2 ;set direction of fireball
  ld a,(ix+v3)
  and a
  ret z
  ld (iy+v1),2
  
  ret


.setshoot:

;set special handler. open mouth and fire a fireball. reset counter

  ld (ix+v4),3
  ld (ix+v7),0
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(ix+v3)
  and a
  jp nz,.setshootright

  ld    hl,coloraddressreznorleft2  ;color address
  ld    bc,characteraddressreznorleft2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret


.setshootright:

  ld    hl,coloraddressreznorright2  ;color address
  ld    bc,characteraddressreznorright2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret



.turnleft:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3 ;wait a short while
  ret c  


  ld (ix+v3),0 ;left right
  ld (ix+v4),0 ;done
  ld (ix+v7),0  

.spriteleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressreznorleft1  ;color address
  ld    bc,characteraddressreznorleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  
  
  
 ; ret



.turnright:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3 ;wait a short while
  ret c


  ld (ix+v3),1 ;left right
  ld (ix+v4),0 ;done
  ld (ix+v7),0  

.spriteright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressreznorright1  ;color address
  ld    bc,characteraddressreznorright1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  
  
  
 ; ret
 


;BUG: issuing otherobjectdeathjump from here creates wierd bugs...
.otherobjectdeathjump:

  call addv2toybject

  ld hl,.reznordeathtable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  ret z ;done with the jump
  
  ld (ix+v2),a
  inc (ix+v7)


  ret


.reznordeathtable: db -4,-4,-3,-3,-2,-2,-1,-1,-1,-1,0,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,6,6,6,7,7,8,10


MovementPattern266:     ;reznor (boss)
;use offsets +20 and +21 to store the spawned objects. (reznors on the platforms)  
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  call z,.endlevel

  ld c,0
  call platforminteract


  ld hl,smallrotatex
  ld de,smallrotatey
  ld b,1
  call rotateobject

  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy

  call .destroyblocks

  call mariointeract
  call c,.handleinteract

  ld a,(ix+offsets+23) ;check if killed
  and a
  ret nz ;yes?? no longer move it around

;move big reznor along
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,24
  xor a
  sbc hl,de
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ret


.setfadeout:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  ret

.endlevel:



  ld l,(ix+offsets+24)
  ld h,(ix+edible)
  dec hl
  ld (ix+offsets+24),l
  ld (ix+edible),h
  ld de,110
  xor a
  push hl
  sbc hl,de
  pop hl
  jp z,.setfadeout
  ld a,l
  or h
  ret nz




;TODO: build in exit scene  
  ld a,1
  ld (levelend?),a



  ret


.setendlevel:

  ld (ix+v4),2 ;use v4
  
  ld hl,300
  ld (ix+offsets+24),l ;use this as timer.... :(
  ld (ix+edible),h

    xor a
    ld (time),a
  
  ld a,2
  ld (playwinmusic),a

  ret


.destroyblocks:

  ld a,(ix+v3)
  and a
  ret nz ;this is the other spawned platform

  ld a,(ix+v7)
  cp 120
  jp nc,.doit
  
  inc (ix+v7)

  ret

;use v6 to keep track of where the platform destroyer is
.doit:
  
  inc (ix+objectrelease)
  ld a,(ix+objectrelease)
  cp 30
  ret c

  call Playsfx.explosion

  ;set mapwidth
  ld hl,30
  ld a,(ix+v6)
  ld e,a
  ld d,0
  add a,a
  add a,e ;*3
  ld e,a
  xor a
  sbc hl,de
  
  push af

;create a cloud. coordinates are allready set
  push ix
  push de
  push hl

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld de,22
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+movepat),22

  pop hl
  pop de
  pop ix
  
  ex de,hl
  ;get mapadress
  ld hl,$4000
  add hl,de ;x tiles to mapbkgadress
  ld de,32*22 ;mapadress Y for this map
  add hl,de ;add y tiles to it
  
  ;we now have the correct bkg adress in HL
  xor a
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile  
  dec hl
  dec hl
  ld de,32
  add hl,de
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile

  inc (ix+v6)
  ld (ix+objectrelease),0 ;yes this is really lame but im out of variables

  pop af

  jp z,.stop

  ret
  
.stop:

  inc (ix+v3)
  ret



.handleinteract:


  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  ret z

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld hl,(marioy)
  ld de,8
  add hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a  
  
  ld a,(iy+v6)
  and a
  ret nz ;allready set
  
  ld (iy+v6),1 ;tell object it dies!!
  ld (iy+v7),0 ;tell object it dies!!
  ;TODO: keep count of the amount of reznors killed when complete call the endlevel code
  ld (ix+offsets+23),1
  
  ld a,(pillarheight)
  inc a
  ld (pillarheight),a
  
  cp 2
  jp z,.setendlevel ;end level after 2 kills
  
  ret

;spawn other platform and reznor on self
.init:

  xor a
  ld (pillarheight),a ;use this variable as global counter

  inc (ix+v4)

  ;create the rhino
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
  dec de


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,220
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)

  call  activateobject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld (ix+v4),2
  ld (ix+deadly),2
  ld hl,267
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld    hl,coloraddressreznorleft1  ;color address
  ld    bc,characteraddressreznorleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop hl
  
  pop ix

  ld (ix+offsets+20),l ;save reznor ix adress
  ld (ix+offsets+21),h

  ld a,(ix+v3) ;this platform is a spawn
  and a
  ret nz

  ;spawn second elevator
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) 
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8  
  ld de,13
  add hl,de
  ex de,hl
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8  

  push ix

  ld    iy,CustomObject
  ld    (iy),available 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,266
  ld    (iy+5),l                ;set object number
  ld    (iy+6),h                ;set object number

  call  activateobject

  inc (ix+v3)
  
  
  ld hl,125 ;set counter to correct value. Which means the movement is half way
  ld (ix+v5),l
  ld (ix+v5+1),h


  pop ix

  ret



MovementPattern265:     ;msx powerup

  ld a,(ix+v4)
  cp 10
  jp z,.power
  cp 11
  jp z,.setwarpmario
  cp 12
  jp z,.waitforfadeout
  dec a
  jp z,.setsprite


  call mariointeract
  jp c,.pickup


  ld a,(ix+v4) ;activate as soon as mario reaches it
  and a
  ret nz

  ld de,(marioy)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  xor a
  sbc hl,de
  ret c

  call getmariodistancexy
  cp 100
  ret nc
  
  inc (ix+v4)  

  ;clear the objectslist
  ld hl,levelObjectList
  ld (hl),0
  ld bc,fulllenghtlevelObjectList-1
  ld de,levelObjectList+1
  ldir

;  call .killallenemies
;  ret

.killallenemies:

  push ix

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 16

  pop ix

  ret

.handleobject:

  ld l,(ix+movepat)
  ld h,(ix+movepat+1)
  ld de,265
  xor a
  sbc hl,de
  ret z ;I am the msx!!!

  jp destroyobject

.setsprite:

  ld b,12
  call increasespritecount

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  inc (ix+v4)
  ret

.power:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  

;create the sprite above marios head
 ; ld l,(ix+xobject)
 ; ld h,(ix+xobject+1)
    ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec l
 ; ld e,(ix+yobject)
 ; ld d,(ix+yobject+1) 
 ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
  dec de
  dec de
  dec de

  ld a,(mariostate)
  and a
  jp z,.nomoredece

  dec de
  dec de


.nomoredece:

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),47                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+v4),0
  
  pop ix
  
;      ld a,b
;      ld (mariotransforming),a
 
  call Playsfx.oneup
     
     ld a,(lives)
     inc a
     ld (lives),a

  
  ld (ix+v7),0
  inc (ix+v6)
  ld a,(ix+v6)
  cp 11
  ret c
  
  inc (ix+v4)
  
;  jp destroyobject

  ret

.pickup:
  
  ld a,(marioswim)
  and a
  ret nz
  
  
  call removeobjectspat
  call removefromsprlst
  ld (ix+amountspr),0   
  
 ; ld hl,48*8
 ; ld (ix+yobject),l
 ; ld (ix+yobject+1),h
  ld (ix+v4),10
  
  call Playsfx.checkpoint
  
  ret


.setwarpmario:

  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a
    

    call Playsfx.secretdoor
    
    inc (ix+v4)

  xor a
  ld (isPlaying),a
  call stopmusic

    ret
  

.waitforfadeout:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 100
    ret c
    
    
    xor a
    ld (fadeoutscreen?),a    
    
    ld a,1
    ld (levelend?),a
  ;warp mario to the super secret level  
  ld    hl,26*8 -8
  ld    (WMmariox),hl
  ld    hl,3*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationSpeci3
  ld    (CurrentLocation),hl
  ld    a,9
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion
    
    ld a,6
    ld (nocontrol),a
    
    ret
    
    
  

MovementPattern264:     ;bony beetle


  ld a,(ix+v4)
  dec a
  jp z,.wait
  dec a
  jp z,.crumble
  dec a
  jp z,.waitforrespawn
  dec a
  jp z,.shake


  ld a,(ix+v7)
  cp 150
  jp z,.changespike

  inc (ix+v7)

  call objectinteract
  cp 4
  call z,.wallfound
  cp 1
  call z,.wallfound

  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  
  ret



.shake:

  ld a,(framecounter)
  and 1
  ret z
  
  ld a,(ix+xobject)
  xor 1
  ld (ix+xobject),a
 

  inc (ix+v7)
  ld a,(ix+v7)
  cp 7
  ret c

  ld (ix+v4),0 ;reset to normal state
  ld (ix+v7),0

  ret


.waitforrespawn:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 90
  ret c

  inc (ix+v4)
  ld (ix+v7),0
  
  ret


.crumble:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  

  inc (ix+v4)
  ld (ix+v7),0 ;reset counter
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

  ld a,(ix+v3)
  and 2
  jp nz,.changecrumbleright2


  ld    hl,coloraddressbonybeetleleft5  ;color address
  ld    bc,characteraddressbonybeetleleft5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret

.changecrumbleright2:

  ld    hl,coloraddressbonybeetleright5  ;color address
  ld    bc,characteraddressbonybeetleright5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret  

  
  


  ret



.wait:

  call mariointeract ;interact without any jumps
  
   inc (ix+v7)
   ld a,(ix+v7)
   cp 90
   ret c
   
  ld (ix+deadly),1 ;become normal
  ld (ix+v4),0 
  ld (ix+v7),0  ;reset any counters


  ret


.changespike:

  inc (ix+v4)
  ld (ix+v7),0
  ld (ix+deadly),2 ;change to immortal
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

  ld a,(ix+v3)
  and 2
  jp nz,.changespikeright


  ld    hl,coloraddressbonybeetleleft3  ;color address
  ld    bc,characteraddressbonybeetleleft3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret

.changespikeright:

  ld    hl,coloraddressbonybeetleright3  ;color address
  ld    bc,characteraddressbonybeetleright3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret

.nextframe:

  call Playsfx.bonestomp

  ld (ix+v4),2 ;crumble and become nothing
  ld (ix+v7),0 ;reset counter
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

  ld a,(ix+v3)
  and 2
  jp nz,.changecrumbleright


  ld    hl,coloraddressbonybeetleleft4  ;color address
  ld    bc,characteraddressbonybeetleleft4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret

.changecrumbleright:

  ld    hl,coloraddressbonybeetleright4  ;color address
  ld    bc,characteraddressbonybeetleright4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  
  

;  ret  


.wallfound:


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:


  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressbonybeetleleft1  ;color address
  ld    bc,characteraddressbonybeetleleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position1:
  ld    hl,coloraddressbonybeetleleft2  ;color address
  ld    bc,characteraddressbonybeetleleft2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressbonybeetleright1  ;color address ;02
  ld    bc,characteraddressbonybeetleright1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position3:
  ld    hl,coloraddressbonybeetleright2  ;color address ;03
  ld    bc,characteraddressbonybeetleright2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;ret
  




MovementPattern263:     ;flying bone

  ld a,(ix+v4)
  and a
  jp z,.init ;set left or right direction

  call mariointeract

  call addv1toxbject
  
  call .animate


  ret

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

  
  ld    a,(ix+v3)               ;v3 is used as animation variable
  and 2
  jp nz,.animateright


;use framecounter for animation
  ld a,(framecounter)
  and 15
  and a
  jp z,.position0  
  cp 4
  jp z,.position1
  cp 8
  jp z,.position2  
  cp 15
  jp z,.position3  
  
  ret


.position0:
  
  ld    hl,coloraddressbone2  ;color address
  ld    bc,characteraddressbone2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position1:
  
  ld    hl,coloraddressbone3  ;color address
  ld    bc,characteraddressbone3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position2:
  
  ld    hl,coloraddressbone4  ;color address
  ld    bc,characteraddressbone4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset
  
.position3:
  
  ld    hl,coloraddressbone5  ;color address
  ld    bc,characteraddressbone5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset    


;  ret

.animateright:


;use framecounter for animation
  ld a,(framecounter)
  and 15  
  and a
  jp z,.position0r  
  cp 4
  jp z,.position1r
  cp 8
  jp z,.position2r  
  cp 15
  jp z,.position3r  
  
  ret


.position0r:
  
  ld    hl,coloraddressbone5  ;color address
  ld    bc,characteraddressbone5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position1r:
  
  ld    hl,coloraddressbone4  ;color address
  ld    bc,characteraddressbone4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position2r:
  
  ld    hl,coloraddressbone3  ;color address
  ld    bc,characteraddressbone3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset
  
.position3r:
  
  ld    hl,coloraddressbone2  ;color address
  ld    bc,characteraddressbone2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset    


;  ret




.init:

  ld a,(ix+v3)
  and 2
  jp nz,.setright

;set direction left
  ld (ix+v1),-2


  ret


.setright:

  ld (ix+v1),2
  ret



MovementPattern262:     ;spikedown

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.shake
  dec a
  jp z,.fall


  call getmariodistance
  ld a,c
  cp 40
  ret nc


  inc (ix+v4)


  ret


.fall:

  ld a,(waterlevel)
  and a
  call z,.fallfast

  call addv2toybject


  ret

.fallfast:

  ld (ix+v2),4

  ret


.shake:

  ld a,(ix+v7)
  cp 15;5
  jp z,.incv4

  inc (ix+v7)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  
  jp z,.right

  ld (ix+v1),-1
  call addv1toxbject



  ret

.right:

  ld (ix+v1),1
  call addv1toxbject


  ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),a


  ret



MovementPattern261:     ;deadfish

  ld a,(ix+v4)
  and a
  jp z,.init


  call mariointeract

  ld a,(framecounter)
  and 3
  call z,.animate

  call addv1toxbject

  ld e,(ix+v7)
  ld d,0
  ld hl,.deadfishmovementtable
  add hl,de
  ld a,(ix+v3)
  bit 2,a
  jp nz,.handleright
  ld a,(hl)
  cp 10
  jp z,.reset

  neg
  ld (ix+v1),a

  inc (ix+v7)

  ret


.handleright:

  ld a,(hl)
  cp 10
  jp z,.reset

  ld (ix+v1),a

  inc (ix+v7)

  ret


.reset:

  ld (ix+v7),0
  ret

.init:

  inc (ix+v4)
  call getmarioleftright
  ret nc
  
  ld hl,163*8 ;do not spawn beyond this point little estetic upgrade for that particular map
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  ret c
  
  ld a,(currentlevel)
  cp 67
  ret z ;not on this map because of major sprite constraints
  
  
  ld a,(ix+v3)
  xor 4
  ld (ix+v3),a
  ret


.deadfishmovementtable: db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,4,4,3,3,3,3,3,3,2,2,2,2,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,10



.animate:

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  push af ;faster than reloading
  and %00000100
  ld b,a
  pop af
  inc a
  and %00000011
  or b ;restore previous values to keep left right
  ld    (ix+v3),a               ;v3 is used as animation variable
  dec a
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  ;right
  dec a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  ret

.position0:
  ld    hl,coloraddressdeadfishleft1  ;color address
  ld    bc,characteraddressdeadfishleft1  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,coloraddressdeadfishleft2  ;color address
  ld    bc,characteraddressdeadfishleft2  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressdeadfishleft3  ;color address
  ld    bc,characteraddressdeadfishleft3  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,coloraddressdeadfishright1  ;color address
  ld    bc,characteraddressdeadfishright1  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position4:
  ld    hl,coloraddressdeadfishright2  ;color address
  ld    bc,characteraddressdeadfishright2  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position5:
  ld    hl,coloraddressdeadfishright3  ;color address
  ld    bc,characteraddressdeadfishright3  ;character address
  ld    de,offsetboo
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret

  



MovementPattern260:     ;dolphin all versions (all types)

  call addv1toxbject

  ld c,0
  call platforminteract

  call .animate
 

  ld a,(ix+v4)
  dec a
  jp z,.jumpup
  dec a
  jp z,.turndir
  
  call .incv4


  ret


.jumpup:

  ld e,(ix+v7)
  ld d,0
  ld hl,.dolphinjumptable
  ld a,(ix+objectrelease)
  cp 1
  jp z,.handleotherjumptable 
  cp 2
  jp z,.handleotherjumptable    
  add hl,de
  ld a,(hl)
  
  cp 10
  jp z,.incv4
  
  ld (ix+v2),a
  inc (ix+v7)
  
;  call addv1toxbject
  call addv2toybject



  ret

.dolphinjumptable:  db -8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-6,-6,-6,-6,-6,-6,-4,-4,-4,-4,-2,-2,-2,-2,-2,-1,-1,-1,-1,0,1,1,1,1,2,2,2,2,2,4,4,4,4,6,6,6,6,6,6,8,8,8,8,8,8,8,8,4,4,2,2,2,1,1,10 ;y jump table
.dolphinjumptableonedir:  db -8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-6,-6,-6,-6,-6,-6,-4,-4,-4,-4,-2,-2,-2,-2,-2,-1,-1,-1,-1,0,1,1,1,1,2,2,2,2,2,4,4,4,4,6,6,6,6,6,6,8,8,8,8,8,8,8,8,8,8,10 ;y jump table

.handleotherjumptable:

  ld hl,.dolphinjumptableonedir
  add hl,de
  ld a,(hl)
  
  cp 10
  jp z,.reset
  
  ld (ix+v2),a
  inc (ix+v7)
  
;  call addv1toxbject
  call addv2toybject

  ret

.incv4:


  inc (ix+v4)
  ld (ix+v7),0 ;reset timer for next flow


  ret

.turndir:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a
  ld a,(ix+v1)
  neg
  ld (ix+v1),a
 ; jp .reset

.reset:

  call addv1toxbject

  ld (ix+v4),0
  ld (ix+v7),0

  ret

.reset2:


  ld (ix+v4),0
  ld (ix+v7),0

  ret


.animate:

  ld a,(ix+objectrelease)
  cp 3
  jp z,.animateup

;lucky us this foul creature only exsist on 1 map and thus we can simply use the hardcoded coordinates to get the waterheight :)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)  
  ld hl,46*8
  xor a
  sbc hl,de
  jp nc,.animateabovewater


  ld a,(ix+v3)
  bit 1,a
  jp nz,.animateright

;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressdolphinleft12  ;character address
  ld    hl,coloraddressdolphinleft12
  exx
  ld    bc,characteraddressdolphinleft11  ;character address
  ld    hl,coloraddressdolphinleft11  
  exx
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    bc,characteraddressdolphinleft21  ;character address
  ld    hl,coloraddressdolphinleft21   
  exx
  ld    bc,characteraddressdolphinleft22  ;character address
  ld    hl,coloraddressdolphinleft22  
  exx
  cp    7
  jr    c,.watercorrectionfound
  ld    bc,characteraddressdolphinleft12  ;character address
  ld    hl,coloraddressdolphinleft12  
  exx
  ld    bc,characteraddressdolphinleft11  ;character address
  ld    hl,coloraddressdolphinleft11   
  exx
  cp    11
  jr    c,.watercorrectionfound
  ld    bc,characteraddressdolphinleft21  ;character address
  ld    hl,coloraddressdolphinleft21   
  exx
  ld    bc,characteraddressdolphinleft22  ;character address
  ld    hl,coloraddressdolphinleft22  
  exx
.watercorrectionfound:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone
  exx
.evenoddcheckdone:

;  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret

.animateup:
  
  ;lucky us this foul creature only exsist on 1 map and thus we can simply use the hardcoded coordinates to get the waterheight :)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)  
  ld hl,43*8
  xor a
  sbc hl,de
  jp nc,.animateabovewaterup
  
  
;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressdolphinup12  ;character address
  ld    hl,coloraddressdolphinup12
  exx
  ld    bc,characteraddressdolphinup11  ;character address
  ld    hl,coloraddressdolphinup11  
  exx
  cp    3
  jr    c,.watercorrectionfoundup
  cp    15
  jr    z,.watercorrectionfoundup
  ld    bc,characteraddressdolphinup21  ;character address
  ld    hl,coloraddressdolphinup21   
  exx
  ld    bc,characteraddressdolphinup22  ;character address
  ld    hl,coloraddressdolphinup22  
  exx
  cp    7
  jr    c,.watercorrectionfoundup
  ld    bc,characteraddressdolphinup12  ;character address
  ld    hl,coloraddressdolphinup12  
  exx
  ld    bc,characteraddressdolphinup11  ;character address
  ld    hl,coloraddressdolphinup11   
  exx
  cp    11
  jr    c,.watercorrectionfoundup
  ld    bc,characteraddressdolphinup21  ;character address
  ld    hl,coloraddressdolphinup21   
  exx
  ld    bc,characteraddressdolphinup22  ;character address
  ld    hl,coloraddressdolphinup22  
  exx
.watercorrectionfoundup:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone
  exx
.evenoddcheckdoneup:

;  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetkoopa8
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret
  

.animateabovewaterup:

  ld a,(framecounter)
  and 3
  ret nz

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  and 1
  jp z,.position1u

  ld    hl,coloraddressdolphinup1  ;color address
  ld    bc,characteraddressdolphinup1  ;character address
  ld    de,offsetkoopa8
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1u:
  
  ld    hl,coloraddressdolphinup2  ;color address
  ld    bc,characteraddressdolphinup2  ;character address
  ld    de,offsetkoopa8
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animateright:



;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressdolphinright12  ;character address
  ld    hl,coloraddressdolphinright12
  exx
  ld    bc,characteraddressdolphinright11  ;character address
  ld    hl,coloraddressdolphinright11  
  exx
  cp    3
  jr    c,.watercorrectionfound2
  cp    15
  jr    z,.watercorrectionfound2
  ld    bc,characteraddressdolphinright21  ;character address
  ld    hl,coloraddressdolphinright21   
  exx
  ld    bc,characteraddressdolphinright22  ;character address
  ld    hl,coloraddressdolphinright22  
  exx
  cp    7
  jr    c,.watercorrectionfound2
  ld    bc,characteraddressdolphinright12  ;character address
  ld    hl,coloraddressdolphinright12  
  exx
  ld    bc,characteraddressdolphinright11  ;character address
  ld    hl,coloraddressdolphinright11   
  exx
  cp    11
  jr    c,.watercorrectionfound2
  ld    bc,characteraddressdolphinright21  ;character address
  ld    hl,coloraddressdolphinright21   
  exx
  ld    bc,characteraddressdolphinright22  ;character address
  ld    hl,coloraddressdolphinright22  
  exx
.watercorrectionfound2:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone2
  exx
.evenoddcheckdone2:

;  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret

.animateabovewater:

  ld a,(framecounter)
  and 3
  ret nz

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animateabovewaterright

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

 ; ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  jp z,.position1

  ld    hl,coloraddressdolphinleft1  ;color address
  ld    bc,characteraddressdolphinleft1  ;character address
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  
  ld    hl,coloraddressdolphinleft2  ;color address
  ld    bc,characteraddressdolphinleft2  ;character address
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animateabovewaterright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

 ; ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  and 1
  jp z,.position1r
  

  ld    hl,coloraddressdolphinright1  ;color address
  ld    bc,characteraddressdolphinright1  ;character address
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1r:
  
  ld    hl,coloraddressdolphinright2  ;color address
  ld    bc,characteraddressdolphinright2  ;character address
  ld    de,offsetwaterplatform2
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

  


MovementPattern259:     ;porcupuffer
;use v7 as pointer

  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin

  call mariointeract

  call addv1toxbject
  call .animate


  ld hl,(camerax) ;never allow exiting the cameraview
  ld de,64
  add hl,de
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  jp nc,.goright
  
  call getmarioleftright
  jp c,.goright


  ld hl,.porcupufferspeedtable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  
  ld (ix+v1),a

  call getmariodistance
  ld a,c
  cp 80
  jp c,.goleftslow

  ld a,(ix+v1)
  
  cp -5
  ret z

  dec (ix+v7)

  ret

  
.goleftslow:

  ld a,(ix+v1)
  
  cp -5
  jp z,.slowdownleft  
  cp -4
  jp z,.slowdownleft
  cp -3
  ret z

  dec (ix+v7)


  ret

.slowdownleft:

  inc (ix+v7)

  ret



.goright:



  ld hl,.porcupufferspeedtable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  
  ld (ix+v1),a

  call getmariodistance
  ld a,c
  cp 80
  jp c,.gorightslow


  ld a,(ix+v1)
  
  cp 5
  ret z

  inc (ix+v7)


  ret

  

.gorightslow:

  ld a,(ix+v1)
  
  cp 5
  jp z,.slowdownright  
  cp 4
  jp z,.slowdownright
  cp 3
  ret z

  inc (ix+v7)


  ret

.slowdownright:

  dec (ix+v7)

  ret



.porcupufferspeedtable: db -5,-4,-4,-4,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,4,4,4,5


.animate:

  call getmarioleftright
  jp c,.animateright

;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressporcupufferleft12  ;character address
  ld    hl,coloraddressporcupufferleft12
  exx
  ld    bc,characteraddressporcupufferleft11  ;character address
  ld    hl,coloraddressporcupufferleft11  
  exx
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    bc,characteraddressporcupufferleft21  ;character address
  ld    hl,coloraddressporcupufferleft21   
  exx
  ld    bc,characteraddressporcupufferleft22  ;character address
  ld    hl,coloraddressporcupufferleft22  
  exx
  cp    7
  jr    c,.watercorrectionfound
  ld    bc,characteraddressporcupufferleft12  ;character address
  ld    hl,coloraddressporcupufferleft12  
  exx
  ld    bc,characteraddressporcupufferleft11  ;character address
  ld    hl,coloraddressporcupufferleft11   
  exx
  cp    11
  jr    c,.watercorrectionfound
  ld    bc,characteraddressporcupufferleft21  ;character address
  ld    hl,coloraddressporcupufferleft21   
  exx
  ld    bc,characteraddressporcupufferleft22  ;character address
  ld    hl,coloraddressporcupufferleft22  
  exx
.watercorrectionfound:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone
  exx
.evenoddcheckdone:

;  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetporcupuffer
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret
  


.animateright:



;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressporcupufferright12  ;character address
  ld    hl,coloraddressporcupufferright12
  exx
  ld    bc,characteraddressporcupufferright11  ;character address
  ld    hl,coloraddressporcupufferright11  
  exx
  cp    3
  jr    c,.watercorrectionfound2
  cp    15
  jr    z,.watercorrectionfound2
  ld    bc,characteraddressporcupufferright21  ;character address
  ld    hl,coloraddressporcupufferright21   
  exx
  ld    bc,characteraddressporcupufferright22  ;character address
  ld    hl,coloraddressporcupufferright22  
  exx
  cp    7
  jr    c,.watercorrectionfound2
  ld    bc,characteraddressporcupufferright12  ;character address
  ld    hl,coloraddressporcupufferright12  
  exx
  ld    bc,characteraddressporcupufferright11  ;character address
  ld    hl,coloraddressporcupufferright11   
  exx
  cp    11
  jr    c,.watercorrectionfound2
  ld    bc,characteraddressporcupufferright21  ;character address
  ld    hl,coloraddressporcupufferright21   
  exx
  ld    bc,characteraddressporcupufferright22  ;character address
  ld    hl,coloraddressporcupufferright22  
  exx
.watercorrectionfound2:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone2
  exx
.evenoddcheckdone2:

;  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetporcupuffer
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret
  

MovementPattern258:     ;extra watercover

;buffer y coordinates for the first time in order to determine the maximum
  ld a,(ix+v4)
  and a
  jp z,.savey

  ;follow mario x at ALL times
  ld hl,(mariox)
  inc hl
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h



;animate extracover
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,characteraddressextracover2  ;character address
  exx
  ld    bc,characteraddressextracover1  ;character address
  exx
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    bc,characteraddressextracover1  ;character address
  exx
  ld    bc,characteraddressextracover2  ;character address
  exx
  cp    7
  jr    c,.watercorrectionfound
  ld    bc,characteraddressextracover2  ;character address
  exx
  ld    bc,characteraddressextracover1  ;character address
  exx
  cp    11
  jr    c,.watercorrectionfound
  ld    bc,characteraddressextracover1  ;character address
  exx
  ld    bc,characteraddressextracover2  ;character address
  exx
.watercorrectionfound:

  ld    a,(ix+xobject)
  and   1
  jr    z,.evenoddcheckdone
  exx
.evenoddcheckdone:

  ld    hl,coloraddressextracover  ;color address  
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4 
  jp  setcharcolandoffset
;/animate platform

 ; ret
  

.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  dec hl
  dec hl
  dec hl
  dec hl
 
 
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ret


MovementPattern257:     ;silver coin


  ld a,(bluepboxactive)
  and a
  jp z,.turnback


  call mariointeract
  jp c,.pickupcoin


  ld a,(ix+v4)
  cp 5
  jp c,.goup

  
;this is the make the coin move really slow  
  ld (ix+v1),0
  ld a,(framecounter)
  and 3
  call z,.moveonepixel
  
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

;  ld    a,(framecounter)
;  and   3                       ;this sprite animates once every 4 frames
;  call  z,.animate
  call .animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call checkobjecttilescheckwall
  call    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checkobjecttilesfloor
  jp nc,.checkexit
  ret


;change the values back into a spikey
.turnback:

  ;reset all counters and values first
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v2),0
  ld (ix+v1),-1
  ld (ix+v5),0
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+objectrelease),0
  
  ld (ix+sizeY),24
  
  ld (ix+movepat),250
  ld (ix+movepat+1),0
  
  ld (ix+deadly),4
  ld (ix+clipping),1
  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,4
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h  
  


  ret


;check if this is a level end. If so remove the coin directly upon contact
.checkexit:


  ld a,(nocontrol)
  cp 3
  jp z,.pickupcoin
  

  ret



.moveonepixel:

  ld a,(ix+killshell) ;abuse this variable to push the v1
  ld (ix+v1),a

  ret


.goup:

  ld (ix+v2),4
  call subv2toybject
  inc (ix+v4)
  
  xor a
  ld (ix+v2),a
  ret



.wallfound:

;  .hardforegroundfound:
  ;change direction
  ld a,(ix+killshell)
;  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ld (ix+killshell),a

  ret


.animate:
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex609  ;color address
  ld    bc,spriteaddrex609c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex610  ;color address
  ld    bc,spriteaddrex610c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex611  ;color address ;02
  ld    bc,spriteaddrex611c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex612  ;color address ;02
  ld    bc,spriteaddrex612c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


  ret

.pickupcoin:

  ld (ix+objectrelease),10 ;this silver coin is extra worth

  call handlecoin
  ;put whatever is needed here

  call Playsfx.coin

  jp destroyobjectwithsparkle


kickboomb:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

  call .getboombtabledata
  
  
  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret  

      

.otherobjectdeathjump:

  ld (ix+v5),1

  ret
  
;reverse direction of boomb  
.wallfound:


  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a
  
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation
  
  ret




.getboombtabledata:

  ld hl,kickboombtablex
  ld d,0
  ld e,(ix+v7)
  add hl,de
  ld a,(hl)
  and a
  jp z,.turnback

  ld c,a

  ld hl,kickboombtabley
  add hl,de ;get the bounce
  ld a,(hl)
  ld (ix+v2),a

  call addv2toybject

  inc (ix+v7)

  ld a,(ix+v6)
  and a
  jp nz,.right

  
  ld a,c
  neg
  ld (ix+v1),a


  ret

.right:

  ld (ix+v1),c


  ret



.turnback:

  ld (ix+v1),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1
  
  ret


kickboombtablex: db 4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,0
kickboombtabley: db -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,0



holdboomb:

  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150 ;must always be higher than holdgoomba
  push af
  call nc,MovementPattern255.setexplode
  pop af
  cp 220
  jp nc,boombexplode

  ld a,(framecounter)
  and 3
  call z,animateboombupsidedown


;omdraaien
  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell

;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right



  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ret c ;prevent exiting from screen
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeleft

  ret

.right:

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ret



.turnshell:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
;  jp z,.nextframe
  


  ret

.turnshellright:

  
  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,0
  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ret


.followmario:

  ld a,(ix+v6) ;reverse direction
  xor 1
  ld (ix+v6),a
  
  call followmario
  ret


.nextframeleft:


  ld (ix+v7),0
  ld (ix+v6),0 ;left
  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


  jp .nextframe

.nextframeright:

  ld (ix+v7),0
  ld (ix+v6),1 ;right

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


.nextframe:


  ld a,(marioturnshell)
  and a
  ret nz


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a
  
  ld (ix+v7),0
;  ld (ix+deadly),1
  ;ld (ix+deathtimer),0
  ld (ix+objectfp),0
  
  ld (ix+clipping),2

  call Playsfx.kick

;branch to kickup from here if mario is looking up
  ld a,(mariolook)
  and a
  jp nz,.setshootup

  ld (ix+v4),4

  ret


.setshootup:

  ld (ix+v4),2
  
  call objectinteract
  cp 8
  call z,.shootupharder

  
  call getmarioleftright
  jp c,.setshootupleft
  
  ld a,(mariospeed)
  ld (ix+v1),a ;migrate mariospeed fuck mariospeed is always positive!  
  
  
  ret


.shootupharder:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,32
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ret

.setshootupleft:

  ld a,(mariospeed)
  neg
  ld (ix+v1),a



  ret


shootboombup:

  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150 ;must always be higher than holdgoomba
  push af
  call nc,MovementPattern255.setexplode
  pop af
  cp 220
  jp nc,boombexplode

  ld a,(framecounter)
  and 3
  call z,animateboombupsidedown

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  cp 8
  jp z,.nextstepbox
  
  ld a,(ix+v7)
  cp 8
  call nc,.mariointeract ;delay the interact otherwise mario will do strange shit
 
;  call checkmariointeractshoot
;  jp c,MovementPattern197.setshootup ;boomb does not die by the spinning cape 
 

  call addv1toxbject
  call addv2toybject

    
  ld a,(ix+v2)
  ld c,a
  push bc
  neg
  ld (ix+v2),a
  call shelluptilecheck
  pop bc
  ld (ix+v2),c
  jp c,.nextstep


  call .getboombtabledata
  

;todo: check for uptiles
  
;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

;  call  checkobjecttilesfloor
;  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation



  ret


.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.mariointeract:


  call mariointeract
  jp c,MovementPattern256.nextframe

  ret


.getboombtabledata:

  ld a,(ix+v2) ;check for slowdown
  cp -7
  call z,.settablefar


  ld e,(ix+v7)
  ld hl,boombkickuptabley
  ld d,0
  add hl,de
  inc (ix+v7)
  ld a,(hl)
  and a
  jp z,.nextstep
  
  ld (ix+v2),a
  
  
  
  ret


.nextstep:

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v2),0
  ld (ix+v6),0
  ld (ix+v7),0

  ret

.nextstepbox:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v2),0

  ld a,(iy+v7)
  and a
  ret nz

  ld (iy+v7),1

  ret


.settablefar:

  ld (ix+v7),12

  ret

boombkickuptabley: db -10,-10,-10,-10,-10,-10,-10,-10,-9,-9,-8,-7,-6,-4,-2,-1,0



boombcommingdown:

  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150 ;must always be higher than holdgoomba
  push af
  call nc,MovementPattern255.setexplode
  pop af
  cp 220
  jp nc,boombexplode


  ld a,(framecounter)
  and 3
  call z,animateboombupsidedown

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern256.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern256.setshootup ;boomb does not die by the spinning cape 

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

;todo: check for uptiles
  
  call  checksheerobjecttiledown
  jp c,.setbounce
  call checksheerobjecttile ;check for sheer tiles
  jp c,.setbounce
  
  call  checkobjecttilesfloor
  jp nc,.setbounce

  ld a,(ix+standingonspr)
  and a
  jp nz,.setbounce

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ld a,(ix+v2)
  cp 6
  jp z,.maxspeed ;falling is a little bit faster for boomb


  ret
      

.otherobjectdeathjump:

  ld (ix+v5),1

  ret

.maxspeed:

  ld (ix+v2),8

  ret

.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.setbounce:

;use the kick code when it has movement in air

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),0 ;when kicking the boomb flies less
  ld (ix+objectfp),0

;compensate the speed
  ld a,(ix+v1)
  bit 7,a
  call nz,.negate
  and a
  jp z,.bounce
  cp 3
  call z,.set8
  cp 2
  call z,.set15
  cp 1
  call z,.set21


  call getmarioleftright
  jp c,.kickright

  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret

.negate:

  neg

  ret

.set8:

  ld (ix+v7),8

  ret

.set15:

  ld (ix+v7),15

  ret
  
.set21:

  ld (ix+v7),21

  ret


.bounce:

  ld (ix+v4),5
  ld (ix+v6),0 ;use v6 as bouncestep indicator
  ld (ix+v7),0


  ret


;todo
boombbounce:

  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150 ;must always be higher than holdgoomba
  push af
  call nc,MovementPattern255.setexplode
  pop af
  cp 220
  jp nc,boombexplode

  ld a,(framecounter)
  and 3
  call z,animateboombupsidedown


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern256.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern256.setshootup ;boomb does not die by the spinning cape
 

  call addv2toybject

  ld a,(ix+v6)
  and a
  jp z,.step1
  cp 2
  jp z,.step2
  cp 4
  jp z,.step3
  cp 6
  jp z,.end


  call  checkobjecttilesfloor
  jp nc,.nextstep

  ld a,(ix+standingonspr)
  and a
  jp nz,.nextstep

  ret

.step1:

  ld e,(ix+v7)
  ld d,0
  ld hl,boombbouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  ld (ix+v2),a
  ret


.step2:

  ld e,(ix+v7)
  ld d,0
  ld hl,boombbouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret

.step3:

  ld e,(ix+v7)
  ld d,0
  ld hl,boombbouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret


.nextstep:

  inc (ix+v6)
  ld (ix+v7),0
  ld (ix+v2),0
  ld (ix+objectfp),0
  
  ret


.end:

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1

  ret

boombbouncetable: db -4,-3,-2,-2,-1,-1,-1,0,0



  
animateboombupsidedown:
 
  ld a,(ix+hitpoints)
  and a
  ret z
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressboombupsidedownleft1  ;color address
  ld    bc,characteraddressboombupsidedownleft1 ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,coloraddressboombupsidedownleft2  ;color address
  ld    bc,characteraddressboombupsidedownleft2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressboombupsidedownright1  ;color address ;02
  ld    bc,characteraddressboombupsidedownright1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,coloraddressboombupsidedownright2  ;color address ;03
  ld    bc,characteraddressboombupsidedownright2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret



MovementPattern256:     ;boomb (upsidedown)


  ld a,(ix+v4)
  cp 1
  jp z,holdboomb
  cp 4
  jp z,kickboomb
  cp 2
  jp z,shootboombup
  cp 3
  jp z,boombcommingdown
  cp 5
  jp z,boombbounce

  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150 ;must always be higher than holdgoomba
  push af
  call nc,MovementPattern255.setexplode
  pop af
  cp 220
  jp nc,boombexplode

  ld a,(framecounter)
  and 3
  call z,animateboombupsidedown


  call addv1toxbject
  call addv2toybject
;using v4 as status pointer
;1 = hold
;2 = trown up
;3 = comming down
;4 = kicked away
;5 = bouncing

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  call  checkobjecttilesfloor
  ret


.dodeathjump:
  
  call addv1toxbject
  call addv2toybject
 
    ld (ix+deathtimer),0
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 4
  ret c
  
  jp otherobjectdeathjump

.setshootup:

  ld (ix+v4),2
  ld (ix+v7),0
  ld (ix+clipping),2
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret


.nextframe:


  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
;  ld a,(movedownsheertile)
;  dec a
;  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

  


  ld a,(Controls)
  bit 5,a
  jp z,.kick


 ld a,(marioyoshi)
 dec a
 ret z


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a

  ld (ix+v4),1 ;hold
  ld (ix+clipping),0 ;make deadly

  ret

.kick:


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),8 ;when kicking the goomba flies less
  ld (ix+objectfp),0  


  call getmarioleftright
  jp c,.kickright


  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret


boombexplode:

  xor a
  ld (marioshell),a
    ;add a little bit of drama to the whole
  ld a,1
  ld (tripscreen),a
  
  call destroyobjectwithcloud

  call getmariodistancexy
  cp 64
  ret nc
  
  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  jp destroyyoshi



MovementPattern255:     ;boomb (walking)

;no other variable left so we use the offsets+10 byte as a global counter for boomb to set off. then we use ix+hitpoints for the final timing (sprite problems and 8 bit limitations)
  inc (ix+offsets+10)
  ld a,(ix+offsets+10)
  cp 150
  push af
  call nc,.setexplode
  pop af
  cp 220
  jp nc,boombexplode


  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;nextframe2 goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate
  ld    a,(framecounter)
  add   a,(ix+random)
  and 7
  call z,.animate2

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


  ;ret

.setexplode:

  ld a,(ix+hitpoints)
  and a
  ret nz

  inc (ix+hitpoints)
  
  ret

.setshootup:

  call Playsfx.kick

  ld (ix+v4),2
  ld (ix+v2),0
  ld (ix+v7),0
  ld (ix+clipping),2
  ld (ix+deadly),0
  ld hl,256
  ld (ix+movepat),l ;place good movepat here.
  ld (ix+movepat+1),h ;place good movepat here.
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret

.otherobjectdeathjump:

  push ix
  call otherobjectdeathjump
  pop ix


.animateupsidedown:
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  bit 1,a
  jp    z,.position1u

.position0u:
  ld    hl,coloraddressboombupsidedownright1  ;color address
  ld    bc,characteraddressboombupsidedownright1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1u:
  ld    hl,coloraddressboombupsidedownleft1  ;color address
  ld    bc,characteraddressboombupsidedownleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;ret


.wallfound:


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressboomb1left  ;color address
  ld    bc,characteraddressboomb1left  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,coloraddressboomb2left  ;color address
  ld    bc,characteraddressboomb2left  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressboomb1right  ;color address ;02
  ld    bc,characteraddressboomb1right  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,coloraddressboomb2right  ;color address ;03
  ld    bc,characteraddressboomb2right  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret


.animate2:

  ld a,(ix+hitpoints)
  and a
  ret z
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position02
  dec   a
  jp    z,.position12
  dec   a
  jp    z,.position22
  dec   a
  jp    z,.position32

.position02:
  ld    hl,coloraddressboomb3left  ;color address
  ld    bc,characteraddressboomb3left  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position12:
  ld    hl,coloraddressboomb4left  ;color address
  ld    bc,characteraddressboomb4left  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position22:
  ld    hl,coloraddressboomb3right  ;color address ;02
  ld    bc,characteraddressboomb3right  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position32:
  ld    hl,coloraddressboomb4right  ;color address ;03
  ld    bc,characteraddressboomb4right  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret



.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a


  ld (ix+deadly),0
  ld hl,256
  ld (ix+movepat),l ;place good movepat here.
  ld (ix+movepat+1),h ;place good movepat here.
  ld (ix+v1),0 ;stop any movement
  ld (ix+v7),0

  call Playsfx.kickenemy

  jp .animateupsidedown

;  ret




.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret





MovementPattern254:     ;paraboomb



 
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
;  jp z,.animateparachute
  dec a
  dec a
  jp z,.land
  
  call objectinteract
  cp 4
  push af
  call z,.move2tiles
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.move2tiles
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,.destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.otherobjectdeathjump ;nextframe2 goomba does not die by the spinning cape  
  
  call mariointeract
  jp c,.nextframe


  ld a,(framecounter)
  and 7
  call z,.animate

  call addv2toybject

  call .moveparachute

  call checkobjecttilesflooronly
  ret nc
  
  ld (ix+v4),3

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressboomb2  ;color address
  ld    bc,characteraddressboomb2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4 
  jp    setcharcolandoffset

  ;ret


.land:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h
  

  inc (ix+v7)
  ld a,(ix+v7)
  cp 16
  ret c


.transform:


  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  

  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v2),0
  ld (ix+v5),0
  ld (ix+v6),0
  ld (ix+v5+1),0
  ld (ix+movepat),255

  call getmarioleftright
  jp c,.right

  ld (ix+v1),-1

  ret

.right:

  ld (ix+v1),1
  ld (ix+v3),2

  ret  
  

.destroyobjectwithcoin:  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  
  
  jp destroyobjectwithcoin 
 
 
  
.otherobjectdeathjump:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  
  
  jp otherobjectdeathjump


.nextframe:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,.destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,.destroyobjectwithcloud

  ld a,(mariostar)
  and a
  jp nz,.setkilljump

  jp .transform


.destroyobjectwithcloud:

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h


  jp destroyobjectwithcloud


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ret

.move2tiles:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  
  inc hl
  inc hl
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ret



.moveparachute:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld c,(ix+xobject)
  ld b,(ix+xobject+1)  
  
  ld de,16
  xor a
  sbc hl,de
  
  ld (iy+yobject),l
  ld (iy+yobject+1),h
  ld (iy+xobject),c
  ld (iy+xobject+1),b  



  ret



.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 dec de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),197                ;set object number (paragoomba)
  ld    (iy+6),0                ;set object number (paragoomba)

  call  activateobject


  ld (ix+v4),1 ;set parachute
  ld (ix+v3),0
  ld (ix+deadly),0
  ld (ix+clipping),0

  push ix
  pop hl

  push ix
  pop iy

  pop ix
  
  ld (ix+v5),l
  ld (ix+v5+1),h

  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h
 
  
  ld (ix+v4),2
  
  ret
  

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position1

  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,coloraddressboomb3  ;color address
  ld    bc,characteraddressboomb3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:
  ld    hl,coloraddressboomb2  ;color address
  ld    bc,characteraddressboomb2  ;character address
  ld    de,offsetone
  ld    a,ingamespritesblock4 
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressboomb1  ;color address ;02
  ld    bc,characteraddressboomb1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;ret




MovementPattern253:     ;spikeball
  
  call mariointeract


  ld a,(framecounter)
  and 3
  call z,.animate


  call addv1toxbject
  call addv2toybject

  ld a,(ix+v4)
  dec a
  jp z,.fall

  ld e,(ix+v7)
  ld d,0
  ld hl,.spikballtable
  add hl,de
  ld a,(hl)
  and a
  jp z,.incv4
  
  inc (ix+v7)
  ld (ix+v2),a
  

  ret


.animate:


  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp z,.position1

.position0:
  ld    hl,coloraddressspikeball1  ;color address
  ld    bc,characteraddressspikeball1  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.position1:
  ld    hl,coloraddressspikeball2  ;color address
  ld    bc,characteraddressspikeball2  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),0


  ret


.fall:

  ld de,0
  ld bc,32
  call objecttilecheckfromrom
  cp 191
  jp nc,.transform
 
   
  ld e,(ix+v7)
  ld d,0
  ld hl,.falltable
  add hl,de
  ld a,(hl)
  cp 6
  ret z
  
  inc (ix+v7)
  ld (ix+v2),a

  ret
  
.transform:
  
  ld (ix+movepat),250
  ld (ix+v4),0
  ld (ix+v7),0

  ld a,(ix+v1)
  dec a
  ret nz
  
  ld (ix+v3),3


  ret


.falltable: db 1,1,1,2,2,2,3,3,4,4,5,6
.spikballtable: db -4,-4,-4,-3,-3,-3,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-1,0

MovementPattern252:     ;lakitu (pipe)

  call objectinteract
  cp 5
  jp z,destroyobjectwithcoin
  cp 2
  jp z,.otherobjectshotdeathjump  

  call checkmariointeractshoot
  jp c,.otherobjectshotdeathjump
  
  
    call mariointeract
    jp c,destroyobjectwithcloud

    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    jp z,.lookaround
    dec a
    jp z,.preppopup
    dec a
    jp z,.popup
    dec a
    jp z,.wait
    dec a
    jp z,.trowspikey
    dec a
    jp z,.wait2
    dec a
    jp z,.wait3
    dec a
    jp z,.sinkbackdown
    dec a
    jp z,.resetloop
  
  

  ret


.otherobjectshotdeathjump:

  ld a,(ix+amountspr)
  cp 2
  call z,.setspritesto4


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitudying  ;color address
  ld    bc,characteraddresslakitudying   ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  call    setcharcolandoffset
  


  jp otherobjectshotdeathjump



.setspritesto4:

  ld b,4
  call increasespritecount

  ret


;.nextframe:
;
;  jp destroyobjectwithcloud
  ;ret

.sinkbackdown:



  ld a,(ix+v7)
  cp 10
  jp nc,.incv4special
  
  ld (ix+v2),1 ;move at fast steps down
  
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,19
  sub b
  call c,.setzero
  call hidemariosprite

  
  inc (ix+v7)

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,5
  sub b
  call c,.setzero
  call hidemariosprite



  ret

.setzero:
  xor a
  ret



  ret



.wait3:

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 25
  ret c

  jp .incv4

;ret

.wait2:


  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 10
  ret c

  call .incv4

;directly update sprite back here  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe1  ;color address
  ld    bc,characteraddresslakitupipe1  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  

;ret



.wait:

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 15
  ret c

  call .incv4
;directly update sprite here  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe2  ;color address
  ld    bc,characteraddresslakitupipe2  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  
  


;  ret


.trowspikey:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),254              ;set object number
  ld    (iy+6),0                ;set object number

  call  activateobject


  ld (ix+v1),-1
  call getmarioleftright
  call c,.setright

  pop ix

  jp .incv4

  ;ret

.setright:

  ld (ix+v1),1


  ret


.preppopup:

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 7
  jp nc,.incv4
  cp 4
  jp nc,.setemptyadress
  ret


.setemptyadress:

  ld b,4
  call increasespritecount


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe1  ;color address
  ld    bc,characteraddresslakitupipe1left;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  
  ;ret



.popup:


  ld a,(ix+v7)
  cp 10
  jp nc,.incv4refresh

  ld    bc,characteraddresslakitupipe1;use empty char adress ;spriteaddrex590c  ;character address
  ld    (ix+spritecharROM+0),c  ;set character address
  ld    (ix+spritecharROM+1),b 
  
  ld (ix+v2),-1 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  add a,11

  ld bc,0*32
  call unhideobjectsprite

  inc (ix+v7)

;clear the object from vram first
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  push hl
  
  xor a
  call hidemariosprite  
  pop hl


  ld a,(ix+v7)
  dec a
  cp 5
  ret c

  push ix
  pop iy

  ld a,(ix+v7)
  ld b,6
  sub b

  ld bc,2*32
  call unhideobjectsprite


  ret
  
  
  

.resetloop:

  ld (ix+v7),0
  ld (ix+v4),0


  ret

.incv4special:
   
  call removefromsprlstskiptwo ;make sure the sprite gets properly removed from vram
  ld (ix+amountspr),2
   
   
  ld    hl,coloraddresslakitupipe1left  ;color address
  ld    bc,characteraddresslakitupipe1left  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  call    setcharcolandoffset


.incv4refresh:
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
.incv4: 
  ld (ix+v7),0
  inc (ix+v4)
  
  ret

.lookaround:
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 8
  jp z,.position1
  cp 16
  jp z,.position0
  cp 24
  jp z,.position1
  cp 32
  jp z,.position0
  cp 40
  jp z,.incv4   

  ret


.position0:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe1left  ;color address
  ld    bc,characteraddresslakitupipe1left;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe1right  ;color address
  ld    bc,characteraddresslakitupipe1right;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret


.init:

  call getmariodistance
  ld a,c
  cp 135
  ret nc
  
  inc (ix+v4)


  ret
  


MovementPattern251:     ;music note block

  ld c,0
  call platforminteract.objectonly

  ld a,(ix+v1)
  and a
  jp nz,dospring


;use ix+v7 as the shell does it for hammer brother

  ld a,(ix+v5)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.popup
  dec a
  jp z,.wait

  ld a,(ix+v7)
  and a
  jp nz,.popbox
  
  call mariointeract
  jp c,.releaseobject
  
 
  ret

;wait a few frames
.wait:

  inc (ix+v6)
  ld a,(ix+v6)
  cp 4
  ret nz


  dec (ix+v5)
  dec (ix+v5)
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v4),0 

;remove sprite at this point and fully remove from spriteslist too!

  call removeobjectspat
  call removefromsprlst

  ld (ix+amountspr),0
  
  ret

.popup:

  call addv2toybject

  inc (ix+v6)
  ld a,(ix+v6)
  cp 4
  ret nz
  
  ld (ix+clipping),8
  ld (ix+v2),8
  call addv2toybject
  
  inc (ix+v5)
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v4),0 
;restore the boxtile we may need to customize this per map

  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  ;adress in hl
  ld a,124
  call writeanytile
  inc hl
  ld a,125
  call writeanytile
  dec hl
  ld de,(maplenght)
  add hl,de
  ld a,126
  call writeanytile
  inc hl
  ld a,127
  call writeanytile 

  ret

.releaseobject:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox


.nopopbox:


;when the box is not popped mario propably jumps from above and thus the box must set itself to act as a spring
  
  ld a,(springactive)
  and a
  ret nz ;make sure that only 1 box does the job at the same time!!


  ld a,(ix+v5)
  cp 2
  ret nc


  call getmarioheight ;make absolutely sure that mario is not to far below the object!
  cp 16
  ret c


  ld (ix+v1),1
  ld (ix+v4),0
  ld (ix+v5),0
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v3),0

  ld a,1
  ld (springactive),a


;fetch the bkgadress and write back the background

  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  call writetilesfromlistfromrom

;create the sprite in place
  ld b,2
  call increasespritecount

  ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressnotebox  ;color address
  ld    bc,characteraddressnotebox  ;character address
  ld    de,.offsets8
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  push hl
  ld de,8
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  pop hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h  

;  call Playsfx.powerup

;fetch the bkgadress and write back the background

  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  call writetilesfromlistfromrom

;create the sprite in place
  ld b,2
  call increasespritecount

  ld (ix+amountspr),2 ;increase sprite count

  ld (ix+clipping),3 ;make sure any shell or other object jumps up
  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v5)
  ld (ix+v2),-2


  ld    hl,coloraddressnotebox  ;color address
  ld    bc,characteraddressnotebox  ;character address
  ld    de,.offsets8
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

;in some special cases. A powerup can pop out... we directly allow it to pop out
  ld a,(ix+objectrelease)
  and a
  ret z
  
  ld a,(ix+hitpoints) ;object allready came out
  and a
  ret nz
  
  inc (ix+hitpoints)
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),17                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v7),1

  pop ix

  ret



.init:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld (ix+clipping),8
  
  inc (ix+v5)

  ret

.offsets8:  dw 008,000,008,000


dospring:

  inc (ix+v4)
  ld a,(ix+v4)
  cp 1
  jp z,.animate1
  cp 2
  jp z,.animate1
  cp 3
  jp z,.animate2 
  cp 4
  jp z,.animate2     
  cp 5
  jp z,.animate3
  cp 6
  call z,.animate4
  
  ld a,1
  ld (jumpspeed),a

  ld a,(marioclimbing)
  and a
  jp nz,.deactivate

  call checkblockabovemario
  call checktileabovemario
  jp c,.deactivate


  ld (ix+v5),0

  ld e,(ix+v7)
  ld d,0
  ld hl,.springjumptablelow
  ld a,(Controls)
  bit 4,a
  call nz,.setjumphigh
 
  ld a,(ix+v5)
  and a
  call z,.setv31
 
 
  add hl,de
  ld a,(hl)
  cp 7
  call z,.setv31
  cp 20
  jp z,.deactivate
  ld e,a

  ld hl,(marioy)
;  ld de,10
  xor a
  sbc hl,de
  jp c,.nothigher ;mario reached top of the map
  ld (marioy),hl

.nothigher:
  
  inc (ix+v7)

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  ld (mariofloatingpf),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 

  ret

.springjumptablelow:  db 9,8,7,4,3,2,1,1,1,0,0,20,20,20,20,20,20,20,20
.springjumptablehigh: db 16,16,16,12,10,8,8,6,5,4,3,2,2,2,2,1,1,0,0,20

.setjumphigh:

  ld a,(ix+v3)
  and a
  ret nz

  ld (ix+v5),1


  ld hl,.springjumptablehigh

  ret

.setv31:

  ld (ix+v3),1
  ret


.deactivate:

  ld (ix+v7),0
  ld (ix+v6),0
  ld (ix+v5),1
  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v1),0
  xor a
  ld (jumpspeed),a
  ld (springactive),a

;remove sprite at this point and fully remove from spriteslist too!
  ret


.animate1:

  call Playsfx.spring

  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl
  
  ld de,.offsets10

  jp    setcharcolandoffset.offsetsonly  
  
  ret

.offsets8:   dw 008,000,008,000
.offsets10:  dw 010,000,010,000
.offsets12:  dw 012,000,012,000


.animate2:


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de  
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl


  ld de,.offsets12

  jp    setcharcolandoffset.offsetsonly 


  ret




.animate3:





;restore the boxtile we may need to customize this per map

  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  ;adress in hl
  ld a,124
  call writeanytile
  inc hl
  ld a,125
  call writeanytile
  dec hl
  ld de,(maplenght)
  add hl,de
  ld a,126
  call writeanytile
  inc hl
  ld a,127
  call writeanytile 

  ld hl,(marioy) ;make it look a little nicer
  ld de,8
  xor a
  sbc hl,de
  ld (marioy),hl


  ld de,.offsets8

  jp    setcharcolandoffset.offsetsonly 

  ret

.animate4:

  xor a
  ld (nocontrol),a


  call removeobjectspat
  call removefromsprlst

  ld (ix+amountspr),0


  ret



MovementPattern250:     ;spikey

  ld a,(bluepboxactive)
  cp 2
  jp z,.destroyobjectwithcoin
  
  ld a,(ix+v5)
  and a
  jp z,.nodec
  dec (ix+v5)

.nodec:

  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.forcewallfound;.wallfound
  pop af
  cp 3
    jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.otherobjectdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.killmario
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



 ; xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

;green koopa will check for the floor and walk over the ledge. Red and brown ones turn around at the ledge

  call  checkobjecttilesfloor
  ret c

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld a,(ix+v7)
  and a
  ld (ix+v7),0
  ret nz

  xor a
  ld a,(ix+standingonspr)
  dec a
  call z,checkforspritewall
  ret c

  ld a,(ix+standingonspr) ;when standing on sheertiles don't check
  cp 2
  ret nc


.forcewallfound:

  ;use counter if this is going on too much and too long we return
  ld a,(ix+v5)
  add a,6 ;8
  ld (ix+v5),a
  cp 30
  jp nc,.maxforce
  cp 24 ;32
  ret nc


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable
  
  ret

.maxforce:

  ld (ix+v5),30
  ret

.otherobjectdeathjump:

  jp otherobjectdeathjump



  ;ret


.killmario:

  ld a,(movedownsheertile)
  and a
  ret z
  
  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a

;  jp destroyyoshi

  ret


.animate:


  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressspikeyleft1  ;color address
  ld    bc,characteraddressspikeyleft1  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.position1:
  ld    hl,coloraddressspikeyleft2  ;color address
  ld    bc,characteraddressspikeyleft2  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position2:
  ld    hl,coloraddressspikeyright1  ;color address ;02
  ld    bc,characteraddressspikeyright1  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position3:
  ld    hl,coloraddressspikeyright2  ;color address ;03
  ld    bc,characteraddressspikeyright2  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret

.destroyobjectwithcoin:



  call removeobjectspat

  ld (ix+movepat),22
  ld (ix+v3),0
  ld (ix+deadly),0

  call removefromsprlstskipfirst


  ld (ix+amountspr),1
  
  ld    (ix+updatespr?),2

  ;niet alle sprites zijn even groot dus moeten we altijd compenseren voor de cloudhoogte 
  ld a,(ix+sizeY) ;spritsize inladen
  sub 16 ;wat je over wilt houden er vanaf trekken
  ld e,a ;dat willen we kwijt
  ld d,0
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
   ld (ix+sizeY),16
   ld (ix+sizeX),16

  
;meteen de sprite updaten  
  ld    hl,spriteaddrex30  ;color address
  ld    bc,spriteaddrex30c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

;get the tilex and y
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
;  dec e
  inc de


  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),de

  push ix

;TODO: we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56                ;set object number (56 is coin)
  ld    (iy+6),0                ;set object number (56 is coin)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+killshell),1
    ld (ix+v1),1
    ld (ix+v2),2
  ld hl,257
  ld (ix+movepat),l
  ld (ix+movepat+1),h


  pop ix

  ret



MovementPattern249:     ;tubecoverup for mario exit left out

  ld a,(ix+v7)
  cp 1
  jp z,.exitlevel
  cp 2
  jp z,.setendlevel

  ld a,(mariogod)
  and a
  jp nz,.endmariogod

  ld a,(marioduck)
  and a
  ret nz
  
  ld a,(marioleftright)
  and a
  ret nz
  
  ld a,(marioleftrightspr)
  and a
  ret nz

  ld a,(mariospeed)
  and a
  ret z
  
  ld a,(fallspeed)
  and a
  ret nz


  call mariointeract
  jp c,.checkheight

  ret

.endmariogod:

  xor a
  ld (mariogod),a
  ret


.checkheight:

;we cannot check the tiles below mario from here so we simply fetchthe height forcing mario to stand on the ground
  call getmarioheight
  cp 1
  ret nc


;force mario to duck if on yoshi
  ld a,(marioyoshi)
  dec a
  call z,.forceduck



  ld a,4 ;nocontrol mode 4 means we exit through a horizontal tube
  ld (nocontrol),a

  ld (ix+v7),1

  call Playsfx.pipe ;play exit sound

  ret






.exitlevel:


  ld hl,(mariox)
  ld de,12
  xor a
  sbc hl,de
  ex de,hl
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,.negative ;mario is on the left side of the tube so do nothing just yet

  ld a,l
  cp 5
    
  
  jp nc,.setendlevel


;place spritehiding stuff here
.negative:


  ld hl,(mariox)
  dec hl
  ld (mariox),hl

  ;enable the spritehider
  
  ld a,1
  ld (spritehideractive),a

  ret

.setendlevel: 

  xor a
  ld (fallspeed),a

  ld (ix+v7),2


;wait some time, this will make it look smooth  
  inc (ix+v3)
  ld a,(ix+v3)
  cp 10
  ret c



.endlevel:

  call disablescreen

  ld a,1
  ld (levelend?),a

  xor a
  ld (marioduck),a

  ld hl,(mariox)
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  srl h
  rr l ;/16 ;make it small so it fits in one byte easier for us
  ld a,l
  ld (exitx),a

  ret

.forceduck:

  ld a,1
  ld (marioduck),a
 
 
 	ld		a,(VDP_8+15)
  ld    b,a
 
  
  ld a,b
  sub 6
  ld b,a
  
  
    ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a


  ret



;in exx hl adress of tile in VRAM in hl animation gfx adress in ROM
updatetubetiles:
 
  ld b,4 ;all 4pages

.updateloop:
 
 push bc

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
 ; ld   a,8
  call outix48
  pop hl


  push hl ;for the next loop
  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
 ; ld   a,8
  call outix48


  pop hl ;for the next loop
  ld de,$4000
  add hl,de
 
  pop bc

  djnz .updateloop

  ret


animatetubetilesup:

  ld a,(ix+v7)
  and 3
  and a
  jp z,.step1
  dec a
  jp z,.step2
  dec a
  jp z,.step3
  
.step4:

  call getblockcoordinates ;get the object bkgadress

  push hl

  ld a,70
  call writeanytile
  inc hl
  ld a,208
  call writeanytile  
  inc hl  
  ld a,172
  call writeanytile  
  inc hl  
  ld a,210
  call writeanytile  
  inc hl  
  ld a,211
  call writeanytile  
  inc hl  
  ld a,71
  call writeanytile
  
  pop hl

  
    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  push hl

  ld a,72
  call writeanytile
  inc hl
  ld a,212
  call writeanytile  
  inc hl  
  ld a,166
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  ld a,213
  call writeanytile  
  inc hl  
  ld a,65
  call writeanytile
  
  pop hl

    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  
  ld a,66
  call writeanytile
  inc hl
  ld a,214
  call writeanytile  
  inc hl  
  ld a,168
  call writeanytile  
  inc hl  
  ld a,169
  call writeanytile  
  inc hl  
  ld a,215
  call writeanytile  
  inc hl  
  ld a,0
  call writeanytile
   
  ld (ix+hitpoints),1 ;disable step
   
    
  ret

.step1:

  call getblockcoordinates ;get the object bkgadress
 
  push hl
 
  ld a,119
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile 
 
  pop hl
  
    ld de,(maplenght)
    add hl,de

  push hl

  ld a,151
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile

  pop hl

    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  
  ld a,177
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile



  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove1
  ld de,tubemove1clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove1+(6*8)
  ld de,tubemove1clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove1+(12*8)
  ld de,tubemove1clr+(12*8)
  exx
  
  call updatetubetiles

  jp setpage12tohandleobjectmovement

.step2:

  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove2
  ld de,tubemove2clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove2+(6*8)
  ld de,tubemove2clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove2+(12*8)
  ld de,tubemove2clr+(12*8)
  exx
  
  call updatetubetiles

  jp setpage12tohandleobjectmovement


.step3:



  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove3
  ld de,tubemove3clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove3+(6*8)
  ld de,tubemove3clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove3+(12*8)
  ld de,tubemove3clr+(12*8)
  exx
  
  call updatetubetiles


  call setpage12tohandleobjectmovement
  
  jp .step4
  
 ; ret


animatetubetilesdown:

  ld a,(ix+v7)
  and 3
  and a
  jp z,.step1
  dec a
  jp z,.step2
  dec a
  jp z,.step3
  
;ret
.step4:
 
  call getblockcoordinates ;get the object bkgadress 

  ld de,(maplenght)
  xor a
  sbc hl,de

  push hl
  

  xor a
  call writeanytile
  inc hl
  xor a
  call writeanytile
  inc hl
  xor a
  call writeanytile
  inc hl
  xor a
  call writeanytile
  inc hl
  xor a
  call writeanytile
  inc hl
  xor a
  call writeanytile
  inc hl    

  pop hl

  ld de,(maplenght)
  add hl,de


  push hl

  ld a,70
  call writeanytile
  inc hl
  ld a,208
  call writeanytile  
  inc hl  
  ld a,172
  call writeanytile  
  inc hl  
  ld a,210
  call writeanytile  
  inc hl  
  ld a,211
  call writeanytile  
  inc hl  
  ld a,71
  call writeanytile
  
  pop hl

  
    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  push hl

  ld a,72
  call writeanytile
  inc hl
  ld a,212
  call writeanytile  
  inc hl  
  ld a,166
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  ld a,213
  call writeanytile  
  inc hl  
  ld a,65
  call writeanytile
  
  pop hl

    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  
  ld a,66
  call writeanytile
  inc hl
  ld a,214
  call writeanytile  
  inc hl  
  ld a,168
  call writeanytile  
  inc hl  
  ld a,169
  call writeanytile  
  inc hl  
  ld a,215
  call writeanytile  
  inc hl  
  ld a,0
  call writeanytile
    
    
    ld (ix+hitpoints),1 ;disable step  
    
  ret

      


 ; ret

.step1:


 call getblockcoordinates ;get the object bkgadress
 
  push hl
 
  ld a,119
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile 
 
  pop hl
  
    ld de,(maplenght)
    add hl,de

  push hl

  ld a,151
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile

  pop hl

    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  
  ld a,177
  call writeanytile
  inc hl
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  inc a
  call writeanytile




  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove3
  ld de,tubemove3clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove3+(6*8)
  ld de,tubemove3clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove3+(12*8)
  ld de,tubemove3clr+(12*8)
  exx
  
  call updatetubetiles

  jp setpage12tohandleobjectmovement


  
;  ret
  
.step2:

  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove2
  ld de,tubemove2clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove2+(6*8)
  ld de,tubemove2clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove2+(12*8)
  ld de,tubemove2clr+(12*8)
  exx
  
  call updatetubetiles

  jp setpage12tohandleobjectmovement


.step3:

  ld a,animationtilesblock
  call block12
  
  ld hl,$0000+119*8
  exx
  ld hl,tubemove1
  ld de,tubemove1clr
  exx
  
  call updatetubetiles

  ld hl,$0000+151*8
  exx
  ld hl,tubemove1+(6*8)
  ld de,tubemove1clr+(6*8)
  exx
  
  call updatetubetiles

  ld hl,$0000+177*8
  exx
  ld hl,tubemove1+(12*8)
  ld de,tubemove1clr+(12*8)
  exx
  
  call updatetubetiles

  call setpage12tohandleobjectmovement

  jp .step4

 ; ret



MovementPattern248:     ;tube up and down



  ld a,(ix+v3)
  and a
  ld c,0
  jp nz,platforminteract


  ld a,(ix+v6)
  and a
  jp z,.init

;this is done to finish the entire cycle before stopping so that the original tiles are always in place
  call getmariodistance
  cp 64
  call c,.enable

  ld a,(ix+hitpoints)
  and a
  ret nz

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy


  inc (ix+v7)

  ld a,(ix+v4)
  dec a
  jp z,.wait
  dec a
  jp z,.down
  dec a
  jp z,.wait2  

.up:
  
  ld (ix+v2),-2
  
   call addv2toybject

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,2
  sbc hl,de
  ld (iy+yobject),l
  ld (iy+yobject+1),h



  call animatetubetilesup
  
  
  ld a,(ix+v7)
  cp 40
  ret c
  
  ld (ix+v7),0
  inc (ix+v4)


  ret


.down:

  ld (ix+v2),2
  
    call addv2toybject   
  
  call animatetubetilesdown
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,8-1
  sbc hl,de
  ld (iy+yobject),l
  ld (iy+yobject+1),h 
 
  
  ld a,(ix+v7)
  cp 40
  ret c
  
  ld (ix+v7),0
  inc (ix+v4)



  ret

.enable:

  ld (ix+hitpoints),0
  ret

.wait:

  ld a,(ix+v7)
  cp 10
  ret c
  
  inc (ix+v4)
  ld (ix+v7),0


  ret

.wait2:

  ld a,(ix+v7)
  cp 15
  ret c
  
  ld (ix+v4),0
  ld (ix+v7),0


  ret


.init:



  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 dec de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),198                ;set object number (paragoomba)
  ld    (iy+6),0                ;set object number (paragoomba)

  call  activateobject

  ld (ix+v3),1

  push ix
  pop hl

  push ix
  pop iy

  pop ix
  
  ld (ix+v5),l
  ld (ix+v5+1),h

  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h

  inc (ix+v6)
  inc (ix+hitpoints)

  call getblockcoordinates

  push hl
  push hl

  ld a,70
  call writeanytile
  inc hl
  ld a,208
  call writeanytile  
  inc hl  
  ld a,172
  call writeanytile  
  inc hl  
  ld a,210
  call writeanytile  
  inc hl  
  ld a,211
  call writeanytile  
  inc hl  
  ld a,71
  call writeanytile
  
  pop hl

  
    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  push hl

  ld a,72
  call writeanytile
  inc hl
  ld a,212
  call writeanytile  
  inc hl  
  ld a,166
  call writeanytile  
  inc hl  
  inc a
  call writeanytile  
  inc hl  
  ld a,213
  call writeanytile  
  inc hl  
  ld a,65
  call writeanytile
  
  pop hl

    ld de,(maplenght)
    add hl,de
 ;   add hl,de

  
  ld a,66
  call writeanytile
  inc hl
  ld a,214
  call writeanytile  
  inc hl  
  ld a,168
  call writeanytile  
  inc hl  
  ld a,169
  call writeanytile  
  inc hl  
  ld a,215
  call writeanytile  
  inc hl  
  ld a,0
  call writeanytile
   
  pop hl

;clear any tiles above in case you walk back
  ld b,15
  
  ld de,(maplenght)
  xor a
  sbc hl,de
  
.clearloop:

  push hl
  xor a
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile
  inc hl
  call writeanytile
  
  pop hl      
   
  xor a 
  sbc hl,de  
  
  djnz .clearloop  
  
  

  ret




MovementPattern247:     ;bill

  ld a,(ix+objectrelease)
  and a
  jp nz,.superspawn ;spawn many bills. many many bills at once



  ld a,(ix+v4)
  and a
  jp z,.init


  call mariointeract
  jp c,otherobjectdeathjump
  
  
  call addv1toxbject
  call addv2toybject


  ret


.init:

  call Playsfx.explosion

  inc (ix+v4)
  call getmarioleftright
  ret nc
  
  ld (ix+v1),4

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbillright  ;color address
  ld    bc,characteraddressbillright  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;  ret


.superspawn:

  call getmariodistance
  ld a,c
  cp 50
  ret nc

;spawn 5 bills two from the back two from the underside and 1 in front

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  
  ld bc,10
  xor a
  sbc hl,bc

  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
; dec e



;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push hl
  push de
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247              ;set object number ()
  ld    (iy+6),0              ;set object number ()

  call  activateobject  

  ld (ix+v2),-4
  ld (ix+v1),0
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbillup  ;color address
  ld    bc,characteraddressbillup  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset


  pop ix
  pop de
  pop hl
  
  
  ld bc,14
  add hl,bc

  push hl
  push de
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247              ;set object number ()
  ld    (iy+6),0              ;set object number ()

  call  activateobject  

  ld (ix+v2),-4
  ld (ix+v1),0
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbillup  ;color address
  ld    bc,characteraddressbillup  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset


  pop ix
  pop de
  pop hl  
 
  
  inc hl
  inc hl
  inc hl
  inc hl
  ld bc,10
  ex de,hl
  xor a
  sbc hl,bc
  ex de,hl

  push hl
  push de
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247              ;set object number ()
  ld    (iy+6),0              ;set object number ()

  call  activateobject  

  ld (ix+v2),0
  ld (ix+v1),-4
  inc (ix+v4)

;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;
;  ld    hl,coloraddressbillleft  ;color address
;  ld    bc,characteraddressbillleft  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock4
;  call    setcharcolandoffset


  pop ix
  pop de
  pop hl   
  
  inc de
  inc de
  inc de
  inc de
  ld bc,28
  xor a
  sbc hl,bc
  

  push hl
  push de
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247              ;set object number ()
  ld    (iy+6),0              ;set object number ()

  call  activateobject  

  ld (ix+v2),0
  ld (ix+v1),4
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbillright  ;color address
  ld    bc,characteraddressbillright  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  pop ix
  pop de
  pop hl   

  ex de,hl
  ld bc,10
  xor a
  sbc hl,bc
  ex de,hl

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247              ;set object number ()
  ld    (iy+6),0              ;set object number ()

  push ix

  call  activateobject  

  ld (ix+v2),0
  ld (ix+v1),4
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbillright  ;color address
  ld    bc,characteraddressbillright  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  pop ix

  jp destroyobject

;  ret



MovementPattern246:     ;springboard


  ld a,(ix+hitpoints)
  and a
  call nz,.platform ;act as platform if mario was found to much on the right or left

  ld a,(ix+v6)
  and a
  jp z,.checkactive


  jp .animate
  
.continue:  
  
  ld a,1
  ld (jumpspeed),a

  ld a,(marioclimbing)
  and a
  jp nz,.deactivate

  call checkblockabovemario
  call checktileabovemario
  jp c,.deactivate


  ld (ix+v5),0

  ld e,(ix+v7)
  ld d,0
  ld hl,.springjumptablelow
  ld a,(Controls)
  bit 4,a
  call nz,.setjumphigh
 
  ld a,(ix+v5)
  and a
  call z,.setv31
 
 
  add hl,de
  ld a,(hl)
  cp 7
  call z,.setv31
  cp 20
  jp z,.deactivate
  ld e,a

  ld hl,(marioy)
;  ld de,10
  xor a
  sbc hl,de
  ld (marioy),hl
  
  inc (ix+v7)

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  ld (mariofloatingpf),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 

  ret

.springjumptablelow:  db 9,8,7,4,3,2,1,1,1,0,0,20,20,20,20,20,20,20,20
.springjumptablehigh: db 16,16,16,12,10,8,8,6,5,4,3,2,2,2,2,1,1,0,0,20

.animate:

  ld a,(ix+objectrelease)
  and a
  jp nz,.animateright


  inc (ix+v4)
  ld a,(ix+v4)
  cp 1
  jp z,.animate1
  cp 2
  jp z,.animate1
  cp 3
  jp z,.animate2 
  cp 4
  jp z,.animate2     
  cp 5
  jp z,.animate3
  cp 6
  call z,.animate4
  cp 7
  call z,.animate0
  cp 8
  call z,.animate01
  cp 9
  call z,.animate0

  jp .continue

.animateright:


  inc (ix+v4)
  ld a,(ix+v4)
  cp 1
  jp z,.animate1r
  cp 2
  jp z,.animate1r
  cp 3
  jp z,.animate2r 
  cp 4
  jp z,.animate2r     
  cp 5
  jp z,.animate3r
  cp 6
  call z,.animate4r
  cp 7
  call z,.animate0
  cp 8
  call z,.animate01r
  cp 9
  call z,.animate0

  jp .continue




.checkactive:

  call mariointeract ;check interaction
  ret nc
  
  ld a,(fallspeed) ;see if mario is truly falling
  and a
  ret z

  ld a,(ix+objectrelease)
  and a
  jp nz,.checkright
;check distance to axis because that has influence on the springpower...
  ;x coordinaten van de object inlezen
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
;x coordinaten van mario inlezen
   ld de,(mariox)
   xor a ;carry reset
   sbc hl,de ;get distance
   ld a,l
   cp 16
   jp nc,.setplatform
  
  ld (ix+v6),1
  ld (ix+hitpoints),0

  ret



.checkright:

;check a right sided platform
;check distance to axis because that has influence on the springpower...
  ;x coordinaten van de object inlezen
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
;x coordinaten van mario inlezen
   ld de,(mariox)
   xor a ;carry reset
   sbc hl,de ;get distance
   ld a,l
   cp 17
   jp c,.setplatform
  
  ld (ix+v6),1
  ld (ix+hitpoints),0

  ret


.setplatform:
 
  ld (ix+hitpoints),1
  ret


;act as platform
.platform:

  ld c,0
  call platforminteract

  ret



.setjumphigh:

  ld a,(ix+v3)
  and a
  ret nz

  ld (ix+v5),1


  ld hl,.springjumptablehigh

  ret

.setv31:

  ld (ix+v3),1
  ret


.deactivate:

  ld (ix+v7),0
  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v6),0
  xor a
  ld (jumpspeed),a

  ret

.animate0:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex600  ;color address
  ld    bc,spriteaddrex600c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


  

.animate1:

  call Playsfx.spring


  xor a
 ; ld (mariospeed),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl
  
.animate01:  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex601  ;color address
  ld    bc,spriteaddrex601c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animate2:

  xor a
 ; ld (mariospeed),a

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de  
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex602  ;color address
  ld    bc,spriteaddrex602c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animate3:

;  xor a ;release mario
;  ld (nocontrol),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex603  ;color address
  ld    bc,spriteaddrex603c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret

.animate4:

  xor a ;release mario
  ld (nocontrol),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex604  ;color address
  ld    bc,spriteaddrex604c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


  
;right animations
.animate1r:

  call Playsfx.spring


  xor a
 ; ld (mariospeed),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl
  
.animate01r:  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex605  ;color address
  ld    bc,spriteaddrex605c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animate2r:

  xor a
 ; ld (mariospeed),a

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de  
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld (marioy),hl


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex606  ;color address
  ld    bc,spriteaddrex606c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.animate3r:

;  xor a ;release mario
;  ld (nocontrol),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex607  ;color address
  ld    bc,spriteaddrex607c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

;ret

.animate4r:

  xor a ;release mario
  ld (nocontrol),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex608  ;color address
  ld    bc,spriteaddrex608c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset





MovementPattern245:     ;ghostblob


  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.handlepart2



  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  

;===================

  ld a,(framecounter)
  and 7
  call z,.animate



  call mariointeract
  
  call .setymovement
  
  
  call addv1toxbject
  call addv2toybject



  call .movepart2



  ret


.setymovement:

  ld a,(ix+v6)
  and a
  jp nz,.go

;wait until mario is close enough before starting the dive cycle this will make it all look prettier ;)
  call getmariodistance
  ld a,c
  cp 80
  jp c,.initiate

  ret

.go:

  ld e,(ix+v7)
  ld d,0
  ld hl,.ymovetable
  add hl,de
  ld a,(hl)
  cp 5
  ret z

  inc (ix+v7)

  ld (ix+v2),a


  ret

.initiate:

  ld (ix+v6),1
  ret


.ymovetable:  db 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,5




.animate:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    (iy+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  and a
  jp z,.position1


  ld    hl,spriteaddrex598  ;color address
  ld    bc,spriteaddrex598c ;character address
  ld    de,offsetghostblob
  ld    a,ingamespritesblock4
  call    setcharcolandoffset 

  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex598+(12*16)  ;color address
  ld    bc,spriteaddrex598c+(12*32) ;character address
  ld    de,offsetghostblob2
  ld    a,ingamespritesblock4
  call    setcharcolandoffset 

  pop ix

  ret

.position1:


  ld    hl,spriteaddrex599  ;color address
  ld    bc,spriteaddrex599c ;character address
  ld    de,offsetghostblob
  ld    a,ingamespritesblock4
  call    setcharcolandoffset 


  push ix
  push iy
  pop ix

  ld    hl,spriteaddrex599+(12*16)  ;color address
  ld    bc,spriteaddrex599c+(12*32) ;character address
  ld    de,offsetghostblob2
  ld    a,ingamespritesblock4
  call    setcharcolandoffset 

  pop ix

  ret



.movepart2:

  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  ld (iy+xobject),e
  ld (iy+xobject+1),d
  ld (iy+yobject),l
  ld (iy+yobject+1),h 

  ret



.handlepart2:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 245
  ret z
  

  ld hl,0
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),l
  ld (ix+yobject+1),h  


  ret


.init:


  inc (ix+v4)
  
  
  ;spawn the second object
  
  
  ;spawn the second sprite and parse ix into main object
  ;object 210 has 6 sprites that one can be used

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),210                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  
  ld (ix+movepat),245
  
  inc (ix+v4)
  inc (ix+v4)
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressghostblob1+(12*16)  ;color address
  ld    bc,characteraddressghostblob1+(12*32)  ;character address
  ld    de,offsetghostblob2
  ld    a,ingamespritesblock4
  call    setcharcolandoffset


  push ix
  pop hl ;secondary ix
  
  push ix
  pop iy
  

  pop ix


  ld (ix+v5),l
  ld (ix+v5+1),h ;store ix adress  secondary ==> main

  
  push ix
  pop hl

  

  ld (iy+v5),l
  ld (iy+v5+1),h ;store ix adress  in second object main => secondary
  
  

  
  ret




MovementPattern244:     ;secret box (pbox)

  ld a,(bluepboxactive)
  and a
  jp z,.destroyobject

  ld a,(ix+v3)
  and a
  jp z,.init

  ld c,0
  call platforminteract ;make this object a platform

;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobject

  call mariointeract
  call c,.handlesecretboxinteract

  ret

;make box visible
.init:

  ld b,2
  call increasespritecount

  inc (ix+v3)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex81  ;color address
  ld    bc,spriteaddrex81c ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 




 ; ret
.destroyobject:


  ld a,(ix+v3)
  and a
  ret z ;block was never active before
  
  jp destroyobject



  ;ret

.handlesecretboxinteract:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox
  
  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a

  ld (ix+movepat),48

;  ld b,2
;  call increasespritecount
 
  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0
 

;lock the coordinates

  ld a,(ix+yobject)
  and %11111000
  ld (ix+yobject),a
  ld a,(ix+xobject)
  ld h,(ix+xobject+1)
  and %11111000
  ld (ix+xobject),a

  ld a,(ix+objectrelease)
  cp 6
  jp z,.popcoin

  call Playsfx.powerup
  
  ret

.popcoin:

  call Playsfx.coin
  
  ret 



MovementPattern243:     ;piranjaplant2

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.wait ;this is the wait cycle 
  dec a
  jp z,.comedown
  dec a
  jp z,.wait2
  dec a
  jp z,.goup
  dec a
  jp z,.wait3


  ;perform steps after activation
  call getmariodistance
  ld a,c
  cp 60
  jp c,.init
  


  ret

;wait shortly to prevent any sprite bugs
.wait3:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  
  ld (ix+v4),0
  ld (ix+v7),0 ;reset the loop


  ret



.goup:

  call addv2toybject

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17
  jp nc,.gouprest
  

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  call hidemariospritedown




  ret


.gouprest:


  ld a,(ix+v7)
  cp 33
  jp nc,.removesprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,16
  sub b
  call hidemariospritedown

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,16
  sub b
  call hidemariospritedown

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,16
  call hidemariospritedown


  ret


.removesprite:

;clear the object from vram
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite
  
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite    

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite      
  
  
  call removefromsprlst ;remove the sprite from the list
  ld (ix+amountspr),0 ;tell engine that the object has no more sprites


  jp .incv4



.wait2:

  ld a,(framecounter)
  and 3
  call z,.animate

  ld a,(ix+v7)
  cp 40
  jp nc,.incv4

  inc (ix+v7)

  ld (ix+v2),-1

  ret





.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1

.position0:
  ld    hl,spriteaddrex595  ;color address
  ld    bc,spriteaddrex595c ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset 

.position1:
  ld    hl,spriteaddrex596  ;color address
  ld    bc,spriteaddrex596c ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset 

  


.comedown:


  call addv2toybject ;move plant down

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17
  jp nc,.comedownrest

;keep the rest hidden
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite


;selfexplanatory
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 

  push ix
  pop iy

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,2*32
  call unhideobjectspriteupdown

;selfexplanatory
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,4*32
  call unhideobjectspriteupdown



  ret

.comedownrest:

  cp 33
  jp nc,.incv4


;this makes sure the sprite does not flickr per frame
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  push ix
  pop iy

  ld a,16

  ld bc,2*32
  call unhideobjectsprite

;selfexplanatory
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,16
  
  ld bc,4*32
  call unhideobjectsprite


;the rest of the plant
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  ld b,16
  sub b
  ld b,a
  ld a,16
  sub b

  ld bc,0*32
  call unhideobjectspriteupdown


  ret



;yet another lets wait until the sprite update to VRAM is complete routine
.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 2
  ret c
  cp 4
  jp nc,.incv4
  
  ld (ix+v2),1
  
  ld    hl,spriteaddrex595  ;color address
  ld    bc,spriteaddrex595c ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  
 
;  ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),0


  ret

;upload sprites
.init:

  call getmarioheighty16 ;prevent height problems
  cp 40
  ret nc


  inc (ix+v4)
  
  ld b,5
  call increasespritecount

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex595  ;color address
  ld    bc,spriteaddrex593c;use empty char adress
  ld    de,offsetkoopa
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret




hideblarghsprites:

  ld a,(ix+v7)
  cp 11
  jp nc,.hidedown

  ld e,a
  ld d,0
  ld hl,.blarghhidetable
  add hl,de
  ld a,(hl)
  ld (ix+v3),a ;fetch hide value

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v3)

  ld bc,4*32
  call unhideobjectsprite

  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v3)

  ld bc,6*32
  call unhideobjectsprite  
 
  push ix
  pop hl
  ld bc,8
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v3)

  ld bc,8*32
  call unhideobjectsprite 
  

  ret

.blarghhidetable: db 2,4,6,8,10,11,12,13,14,15,16,16,15,14,13,12,10,8,6,4,1,13,11,7,3,0

.hidedown:

  cp 22
  jp nc,.hidehead

  ld e,a
  ld d,0
  ld hl,.blarghhidetable
  add hl,de
  ld a,(hl)
  ld (ix+v3),a ;fetch hide value



  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v3)
  call hidemariosprite

  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v3)
  call hidemariosprite
  
  push ix
  pop hl
  ld bc,8
  call getobjectspriteadress 
  
  ld a,(ix+v3)
  call hidemariosprite  

  

  ret


.hidehead:

  ld e,a
  ld d,0
  ld hl,.blarghhidetable
  add hl,de
  ld a,(hl)
  ld (ix+v3),a ;fetch hide value


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  xor a  
  call hidemariosprite 

  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  xor a  
  call hidemariosprite 
  
  push ix
  pop hl
  ld bc,8
  call getobjectspriteadress 
  
  xor a  
  call hidemariosprite 



  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v3)
  call hidemariosprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v3)
  call hidemariosprite

  ret


MovementPattern242:     ;blargh

  ld a,(ix+v4)
  dec a
  jp z,.preppopup
  dec a
  jp z,.popup ;new experimental code to make blargh look even better
  dec a
  jp z,.lookaround ;look around with the scarry eyes
  dec a
  jp z,.sinkback ;sink back into the lava
  dec a
  jp z,.wait
  dec a
  jp z,.incv4  
  dec a
  jp z,.setsprite
  dec a
  jp z,.attack

  call getmariodistance
  ld a,c
  cp 100;60
  jp c,.activatesprite



  ret


.spawnsplash:

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite ;keep sprite completely hidden

  inc (ix+v4)
  ld (ix+v7),0

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 ; inc e

  push de
  pop bc

;  ld b,e


  push ix

  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc

  push bc
  push hl

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+3),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  pop ix


  ret

.sinkback:

  ld a,(ix+v7)
  cp 9
  jp nc,.spawnsplash
  
  ld (ix+v2),2 ;move at fast steps down
  
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  add a,a
  ld b,a
  ld a,16
  sub b
  call c,.setzero
  call hidemariosprite

  
  inc (ix+v7)


  ret

.setzero:
  xor a
  ret


.lookaround:
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 4
  jp z,.position1
  cp 8
  jp z,.position0
  cp 12
  jp z,.position1
  cp 16
  jp z,.position0
  cp 20
  jp z,.incv4   

  ret



.position0:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex590  ;color address
  ld    bc,spriteaddrex590c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex594  ;color address
  ld    bc,spriteaddrex594c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.preppopup:

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 4
  jp nc,.incv4
  cp 3
  ret c ;wait 2 frames!!!
  
  
  ld    hl,spriteaddrex590  ;color address
  ld    bc,spriteaddrex590c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  
  ;ret

.popup:


  ld a,(ix+v7)
  cp 9
  jp nc,.incv4
  
  ld (ix+v2),-2 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  add a,a

  ld bc,0*32
  call unhideobjectsprite

  inc (ix+v7)

  ret


.incv4:

  ld (ix+v7),0
  inc (ix+v4)

  ret


.setsprite: ;why like this. We first need 2 frames to write the color and character tables + upload them to vram. We change the adress here after the update is complete
;with that move you no longer see the full blargh appearing in the lava making the effect 1000000x more realistic

  inc (ix+v4)

  ld    hl,spriteaddrex591  ;color address
  ld    bc,spriteaddrex591c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret


.attack:


  call hideblarghsprites
  call mariointeract

  ld (ix+v1),-1

  
  call addv1toxbject
  call addv2toybject


  ld a,(ix+v7)
  ld e,a
  ld d,0
  ld hl,.blarghmovetable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.destroyobject

  ld (ix+v2),a
  inc (ix+v7)

  ret

.destroyobject:

  call .spawnsplash
  jp destroyobject


.blarghmovetable: db -2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,0,1,1,1,1,2,2,2,2,3,3,3,4,4,5

.wait:


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite ;keep sprite completely hidden

  call getmariodistance
  ld a,c
  cp 30
  jp c,.activateblargh
  
  ret


.activateblargh:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,20
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  inc (ix+v4)

  ld b,10
  call increasespritecount
  
  call Playsfx.blargh

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex592  ;color address
  ld    bc,spriteaddrex592c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  ret

  


.activatesprite: ;sigh... the blargh eyes come popping out of the lava first then take a few looks around then go back in and spawn a few drops of lava before blargh attacks

  inc (ix+v4)

  ld b,2
  call increasespritecount


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex590  ;color address
  ld    bc,spriteaddrex593c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetblargh
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret



MovementPattern241:     ;floating skull


;spawn the skulls only when close enough this prevents the non spawning bug

  ld c,0
  call platforminteract ;make this object a platform


  ld a,(ix+v4)
  and a
  jp z,.init ;spawn 2 extra skulls
  cp 2
  jp z,.move

;do your thing here :)
  call mariointeract
  jp c,.activate
  
  
  

  ret


.fall:

  call addv2toybject


  ld a,(ix+v3)
  ld hl,.falltable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  ret z
  ld (ix+v2),a

  inc (ix+v3)

  ret

.falltable: db 0,1,1,1,2,2,2,3,3,3,4,5


.move:


  ;animate as well
  call .animate



  ld (ix+v1),1
  
  call addv1toxbject


;check if there is lava. if not fall
  ld bc,40
  ld de,0
  call objecttilecheckfromrom
  cp 180
  jp nz,.fall


  ld (ix+v3),0 ;always reset fallcounter

  ld a,(currentlevel) ;special tilecheck because of animation and tileconstraints for that particular map. maplevel03-04b
  cp 55
  jp z,.checkspecial

;check for special lava
  ld bc,20
  ld de,10
  call objecttilecheckfromrom
  cp 102
  jp z,.rise
  cp 103
  jp z,.rise
  cp 104
  jp z,.rise     

  ld a,(currentlevel) ;only check these tiles on maplevel03-03b
  cp 52
  ret nz

;check for special lava
  ld bc,12
  ld de,10
  call objecttilecheckfromrom  
  cp 156
  jp z,.rise2
  cp 157
  jp z,.rise2  
  cp 76
  jp z,.rise2 

;check twice to get the final rise correct, the z80 will love this ;)
  ld bc,20
  ld de,0
  call objecttilecheckfromrom 
  cp 76
  jp z,.rise2 


  ret

.rise:

  ld (ix+v2),-1
  call addv2toybject  

  ret

.rise2:

  ld (ix+v2),-2
  call addv2toybject  

  ret


.checkspecial:

;check for special lava
  ld bc,20
  ld de,10
  call objecttilecheckfromrom
  cp 120
  jp z,.rise
  cp 121
  jp z,.rise
  cp 122
  jp z,.rise     

;check for special lava
  ld bc,12
  ld de,10
  call objecttilecheckfromrom  
  cp 124
  jp z,.rise2
  cp 125
  jp z,.rise2  
  cp 148
  jp z,.rise2 

;check twice to get the final rise correct, the z80 will love this ;)
  ld bc,20
  ld de,0
  call objecttilecheckfromrom 
  cp 148
  jp z,.rise2 


  ret


.activate:

  inc (ix+v4) ;activate self
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+v6)
  ld d,(ix+v6+1)
  
  push hl
  pop iy
  
  inc (iy+v4)
  
  push de
  pop iy
  
  inc (iy+v4)


  ;tell the other objects to do their thing



  ret



.animate:


  ld a,(framecounter)  
  and 7
  jp z,.position1
  ld a,(framecounter)
  and 3
  jp z,.position0

  
  ret



.position0:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex588  ;color address
  ld    bc,spriteaddrex588c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret


.position1:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex589  ;color address
  ld    bc,spriteaddrex589c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret


.init:

  call getmariodistance ;we prevent spawning bug with this
  ld a,c
  cp 180
  ret nc

  inc (ix+v4)
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
 ; dec e
 ; dec e

  push ix
  push hl
  push de


  
  ld    iy,CustomObject
  ld    (iy),force28
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),239
  ld    (iy+6),0

  call  activateobject
  inc (ix+v4) ;make sure this object does not spawn again!!
  
  push ix
  pop hl
  ld (temphl),hl
  
  
  pop de
  pop hl
  
  inc hl
  inc hl

  ld    iy,CustomObject
  ld    (iy),force30
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),239
  ld    (iy+6),0

  call  activateobject
  inc (ix+v4) ;make sure this object does not spawn again!!
  
  push ix
  pop hl
  ld (tempde),hl  
  
  
  pop ix


;save the ix adresses in each object.
  ;into self
  ld hl,(temphl) ;object1
  ld de,(tempde) ;object2
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld (ix+v6),e
  ld (ix+v6+1),d 

;into object 1
  push hl ;object 1
  pop iy
  push ix ;self
  pop hl
  ld (iy+v5),l ;self
  ld (iy+v5+1),h
  ld (iy+v6),e ;object2
  ld (iy+v6+1),d  
  
;into object 2  
  push de ;object2
  pop iy
  ld de,(temphl) ;object1
  ld (iy+v5),l ;self
  ld (iy+v5+1),h
  ld (iy+v6),e 
  ld (iy+v6+1),d


  ret


MovementPattern240:     ;replicating chuck

  ld a,(ix+v5)
  and a
  jp nz,.becomeevil

  call objectinteract
;  cp 2
;  jp z,otherobjectdeathjump ;replicating chuck is very special MOAHAHAHAHA
  cp 3
  jp z,otherobjectdeathjump


  call mariointeract
  jp c,.nextframe


  ld a,(ix+v4)
  cp 1
  jp z,.spawnchuck1 ;spawn the chucks at the first frames
  cp 5
  jp z,.spawnchuck2
  cp 60
  jp z,.spawnchuck1 ;spawn the chucks at the first frames
  cp 65
  jp z,.spawnchuck2  
  cp 80
  jp z,.transform ;become charging itself
  and a
  jp nz,.count

  call getmariodistance
  ld a,c
  cp 75
  jp c,.activate

  ret

.count:

  inc (ix+v4)
 
  ret


.becomeevil:

  ld a,(ix+v4)
  cp 10
  jp nc,.transform

  call addv1toxbject
  call addv2toybject

  call checkobjecttilesfloor

  inc (ix+v4)

  ret

.transform:

;reset all possible bytes that could cause problems
  ld (ix+v5),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+objectrelease),1
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+movepat),33
  ret


.spawnchuck1:

  ld a,-2

.spawnchuck:

  inc (ix+v4)

;make the first chucky
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 ; dec e
 ; dec e

  push ix
  push af
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),238                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject

  pop af
  ld (ix+v1),a
  ld (ix+v2),-4
  inc (ix+v5) ;tell this new chuck it is a spawn
  
  
  pop ix

  ret

.spawnchuck2:

  ld a,2
  jp .spawnchuck

;  ret 




.activate:

  ld a,1
  ld (tripscreen),a ;trip the screen to make it more awesome ;)

;standing chuck
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v4)

  ld    hl,spriteaddrex582  ;color address
  ld    bc,spriteaddrex582c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;  ret


.nextframe:

  ret



MovementPattern239:    ;water (mario swim)

  ld a,(ix+v4)
  and a
  jp z,.init


  ;follow mario x at ALL times
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(currentlevel)
  cp 53
  call z,.adjusty
  cp 69
  jp z,.setboundaries
  cp 114
  jp z,.setboundaries2
  cp 121
  jp z,.setboundaries3

  
.goswim:

  ld a,(Controls)
  bit 6,a
  call nz,.unmountyoshi


 ; ld a,(ix+deathtimer) ;cheap fix for a nasty bug ;TODO: FIX THIS DAMN BUG!!!
 ; cp 15
 ; ret c

  call mariointeract
  jp c,.setswim


  ld a,(ix+v3)
  dec a
  jp z,.checkcontrols

  
  ret


  
  
.setboundaries3:  

  ld hl,(mariox)
  ld de,218*8
  xor a
  sbc hl,de
  ret c  

  ld hl,(mariox)
  ld de,335*8
  xor a
  sbc hl,de
  ret nc 

  jp .goswim


  
  
.setboundaries2:

  ld hl,(mariox)
  ld de,23*8
  xor a
  sbc hl,de
  ret c  

  ld hl,(mariox)
  ld de,109*8
  xor a
  sbc hl,de
  ret nc 

  jp .goswim



.setboundaries:


  ld hl,(mariox)
  ld de,130*8
  xor a
  sbc hl,de
  ret c  

  ld hl,(mariox)
  ld de,210*8
  xor a
  sbc hl,de
  ret nc 

  jp .goswim

 ; ret

.adjusty:

  ld hl,43*8
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (waterheight),hl  
  
  ld hl,(mariox)
  ld de,71*8
  xor a
  sbc hl,de
  ret c

  ld hl,34*8
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (waterheight),hl 

;cancel water height at end of map
  ld hl,(mariox)
  ld de,330*8
  xor a
  sbc hl,de
  ret c

  ld hl,48*8
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (waterheight),hl 


  ret

;hiero
.init:

  inc (ix+v4)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  ld (waterheight),hl

  ld a,(currentlevel)
  cp 53
  ret nz
  
  ld hl,44*8 ;cheap level specific hack :P
  ld (waterheight),hl

    xor a
    ld (waterlevel),a  

  ret


.unmountyoshi:


  xor a
  ld (marioyoshi),a


  ret


.setswim:
    ;disable jump before trying to swim

  ld a,1 
  ld (marioswim),a
  ld (ix+v3),1 ;set this bit to indicate that mario is not allowed to deswim
    xor a
    ld (mariofloating),a ;VERY important to disable the jumping when mario touches the water or the jump becomes eternal effectively stucking mario
 
 
 
  ld a,(currentlevel) ;set watercurrent for specific map
  cp 6
  call z,.setwatercurrent
  cp 64
  call z,.setwatercurrent 
  cp 81
  call z,.setwatercurrent   
  cp 107
  call z,.setwatercurrent 
  
  xor a
    ld (maxwaterlevel),a
    
    ld a,(marioyoshi)
    dec a
    jp z,.spawnsplash
    
    xor a
    ld (mariospinjump),a


.spawnsplash:

  ld a,(ix+v4)
  cp 1
  ret z

  ld (ix+v4),1
;do not spawn the splash after the jump is done. spawn the splash when the water level is reached.
;do that only once per jump! use v4 for that do not reset v4 to 0
 
;spawn the splash
.spawnsplashset:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  ld e,(ix+v5) ;always at the fixed height of the waterlevel
;  ld d,(ix+v5+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
  dec de

 ; ld b,e


  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),74                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),125

  pop ix


  ld a,(marioleftrightspr)
  ld (marioleftrightsprw),a


  ret


.setwatercurrent:

  ld a,1
    ld (watercurrent),a ;this object causes watercurrent
  ret

.checkcontrols:


  ;do not allow mario to swim up
  ld a,1
  ld (maxwaterlevel),a

;its somewhat complicated. There is no other way :(
  ld a,(marioshell)
  dec a
  jp z,.checkjumpkey

.donecheck:

  ld (ix+v3),0

;do not reset the jumpuppointer if you are not pressing up
  ld a,(Controls)
  bit 0,a
  ret z

  xor a
  ld (mariojumpupp),a
    ld (mariojumprelease),a
      ld (watercurrent),a ;this object causes watercurrent
  ld (marionotdown),a
 ; ld (ix+v3),0
    ld (marioswim),a
  ld (ix+v4),0
  ld (ix+deathtimer),a ;set timer

  ld a,1
  ld (fallspeed),a
  
  jp .spawnsplashset

 ; ret


.checkjumpkey:

  ld a,(Controls)
  bit 4,a
  ret z

  jp .donecheck

updatemaplevel0301canimations:

  ld    a,animationtilesblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtilescharacteraddr+(8*36)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*36)             ;rotating tile animation 1 character address in ro
  call updatemap0301blocks   
  ld hl,animationtiles0301c+(8*18)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*18)             ;rotating tile animation 1 character address in ro
  jp updatemap0301blocks2

.step3:
  ld hl,animationtilescharacteraddr+(8*26)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*26)             ;rotating tile animation 1 character address in ro
  call updatemap0301blocks 
  ld hl,animationtiles0301c+(8*12)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*12)             ;rotating tile animation 1 character address in ro
  jp updatemap0301blocks2

.step2:
  ld hl,animationtilescharacteraddr+(8*16)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*16)             ;rotating tile animation 1 character address in ro  
  call updatemap0301blocks
  ld hl,animationtiles0301c+(8*06)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*06)             ;rotating tile animation 1 character address in ro
  jp updatemap0301blocks2


.step1:
  ld hl,animationtilescharacteraddr+(8*6)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*6)             ;rotating tile animation 1 character address in ro
  call updatemap0301blocks
  ld hl,animationtiles0301c+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*00)             ;rotating tile animation 1 character address in ro
  jp updatemap0301blocks2


updatemap0301blocks2:


  exx 

  
  ld    hl,$4000+177*8
  call .doshit
  ld    hl,$0000+177*8 
  call .doshit
  
;  jp setpage12tohandleobjectmovement
 
 ret 

  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix48
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix48

  ret

 

updatemap0301blocks:
  
  exx 

  
  ld    hl,$4000+192*8
  call .doshit
  ld    hl,$0000+192*8 
  call .doshit

;  jp setpage12tohandleobjectmovement
 
 ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix32

  ret



sinkmassivepillartoend:


.line1:


 ;even worse providing the mapadress manually :P
 
  ld hl,bgrmapaddr+9024
  
;  call getblockcoordinates ;fetch the mapadress in hl
;  ex de,hl  
  ld de,42 ;static X yes! bad coding! i don't give a F*ck
  add hl,de

  jp .estep0


.line2:


 ;even worse providing the mapadress manually :P
 
  ld hl,bgrmapaddr+9216
  
;  call getblockcoordinates ;fetch the mapadress in hl
;  ex de,hl  
  ld de,42 ;static X yes! bad coding! i don't give a F*ck
  add hl,de

;  jp .estep0


  
.estep0: ;black layer
  push hl
  ld b,130 ;size of object
.eloop0:
  ld (hl),0
  inc hl
  djnz .eloop0
  pop hl
;  ret

  ld de,(maplenght)
  add hl,de ;next row

.estep1: ;topline of pillar
  push hl
  ld b,130 ;size of object
  dec b
  ld (hl),160+4
  inc hl
.eloop:
  dec b
  jp z,.eend
  ld (hl),161+4
  inc hl
  dec b
  jp z,.eend
  ld (hl),162+4
  inc hl
  jp .eloop
.eend:
  ld (hl),163+4
  pop hl
  
  ret



.line3:

  ld hl,bgrmapaddr+9408
  
;  call getblockcoordinates ;fetch the mapadress in hl
;  ex de,hl  
  ld de,42 ;static X yes! bad coding! i don't give a F*ck
  add hl,de


.step0: ;black layer
  push hl
  ld b,130 ;size of object
.loop0:
  ld (hl),0
  inc hl
  djnz .loop0
  pop hl
;  ret

  ld de,(maplenght)
  add hl,de ;next row

.step1: ;fix the lava
  ;push hl
  ld b,131 ;size of object
;  dec b
;  ld (hl),160+4
;  inc hl
.loop:
  dec b
;  jp z,.end
  ret z
  ld (hl),177
  inc hl
  dec b
  ;jp z,.end
  ret z
  ld (hl),178
  inc hl
  jp .loop
;.end:
  
;  ret




MovementPattern238:     ;maplevel03-01c special object

  ld a,(ix+v5) ;once this byte is set the entire object ceases to function
  and a
  jp nz,updatemaplevel0301canimations

  ld c,0
  call platforminteract ;WArning: objects that do checktilebelow will also be affected by this platform.

  ld a,(ix+objectrelease)
  and a
  ret nz


  ld a,(ix+v4)
  and a
  jp nz,.go


  call mariointeract
  jp c,.activate

  ret


  
  


.init: ;spawn helper object


  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  inc de
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),236                ;set object number (elevator)
  ld    (iy+6),0                ;set object number (elevator)

  call  activateobject
  
  ld (ix+objectrelease),1
  
  push ix
  pop hl
  
  pop ix

  ld (ix+v6),l
  ld (ix+v7),h

  ret
  



.sinkpillartoend:

  call animatehugeyellowblock ;abuse pillary variable for this

  inc (ix+v5+1)
  
;update mapblocks accordingly. We are lucky that the map is in page 1

  ld a,(slot.page2rom)
  out ($a8),a



  ld a,(ix+v5+1)
  cp 8
  jp c,sinkmassivepillartoend.line1
  cp 16
  jp c,sinkmassivepillartoend.line2  
  cp 23
  jp c,sinkmassivepillartoend.line3

  ld (ix+v5),1
  ld (iy+v5),1

  call setpage12tohandleobjectmovement
  call restoremusicafterpbox

  ret


.go:

  call updatemaplevel0301canimations 
  
  ld l,(ix+v6)
  ld h,(ix+v7)
  push hl
  pop iy
  
  ;TODO: set absolute right border for objects
  
  ;follow mario at all times ;as the platform is fairly huge the platform will take the objects in as well.
  ld hl,(mariox)  
  ld de,148+42*8 ;most left border
  xor a
  sbc hl,de
  jp c,.skipx

  
  ld hl,(mariox) 
;  push hl
  ld de,150
  xor a
  sbc hl,de 
  ld (ix+xobject),l
  ld (ix+xobject+1),h
;  pop hl
;  ld (iy+xobject),l
;  ld (iy+xobject+1),h


 
.skipx:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,200
  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h


  inc (ix+v4)

;wait a few frames before sinking
  ld a,(ix+v4)
  cp 80
  ret c
  
  
  ;sink down each frame
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  inc hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (iy+yobject),l
  ld (iy+yobject+1),h 

  
  ld a,l
  and 7
  ld (pillary),a

  ld a,(ix+v3)
  cp 18
  jp nc,.sinkpillartoend

  call animatehugeyellowblock ;abuse pillary variable for this

;  ld a,(ix+v3)
;  and a
;  jp z,.writefirst

  ld a,(pillary)
  cp 7 
  ret nz

  
.writefirst:  
  
  inc (ix+v3)
  
  call getblockcoordinates ;fetch the mapadress in hl
  ex de,hl  
  ld de,42 ;static X yes! bad coding! i don't give a F*ck
  add hl,de
  
;update mapblocks accordingly. We are lucky that the map is in page 1

  ld a,(slot.page2rom)
  out ($a8),a

  
.step0: ;black layer
  push hl
  ld b,130 ;size of object
.loop0:
  ld (hl),0
  inc hl
  djnz .loop0
  pop hl
;  ret

  ld de,(maplenght)
  add hl,de ;next row

.step1: ;topline of pillar
  push hl
  ld b,130 ;size of object
  dec b
  ld (hl),160+4
  inc hl
.loop:
  dec b
  jp z,.end
  ld (hl),161+4
  inc hl
  dec b
  jp z,.end
  ld (hl),162+4
  inc hl
  jp .loop
.end:
  ld (hl),163+4
  pop hl
;  ret

  ld de,(maplenght)
  add hl,de ;next row


.step2: ;2nd or main line of pillartop:
  push  hl
  ld b,130 ;size of object
  dec b
  ld (hl),160
  inc hl
.loop2:
  dec b
  jp z,.end2
  ld (hl),161
  inc hl
  dec b
  jp z,.end2
  ld (hl),162
  inc hl
  jp .loop2
.end2:
  ld (hl),163
  pop hl
;  ret  
  
  jp setpage12tohandleobjectmovement

;  ret


.activate:

  ld (ix+v4),1

  call Playsfx.explosion

  ld a,1
  ld (tripscreen),a
  ;ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a
  ld (switchmusicpforbox),a  

;cheap fix. Prevent mario from locking on the sides of the tiles so he will fall thru
  ld hl,(marioy)
  ld de,8
  xor a
  sbc hl,de
  ld (marioy),hl
  
  call .init
;  call .writefirst

  ret



animatehugeyellowblock:
  

  ld    a,level19animationsblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 
    ld a,(pillary)
 ;   and 7
  
    exx

  cp 0
  ld hl,animationlvchr19+(8*119)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*119)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles   
  cp 1
  ld hl,animationlvchr19+(8*102)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*102)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles 
  cp 2
  ld hl,animationlvchr19+(8*85)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*85)             ;rotating tile animation 1 character address in ro  
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles
  cp 3
  ld hl,animationlvchr19+(8*68)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*68)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles
  cp 4
  ld hl,animationlvchr19+(8*51)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*51)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles
  cp 5
  ld hl,animationlvchr19+(8*34)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*34)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles
  cp 6
  ld hl,animationlvchr19+(8*17)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*17)             ;rotating tile animation 1 character address in ro
 ;   ld (pillaranimationstep),a
  jp z,.updateyellowpillartiles
  cp 7
  ld hl,animationlvchr19+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in ro
;    ld (pillaranimationstep),a


.updateyellowpillartiles:
  
  exx 

  call .getvrampageadressyellow
;  jp nz,setpage12tohandleobjectmovement
  


  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix136
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix136

  call setpage12tohandleobjectmovement

  ret

;/=================================================================================================


.getvrampageadressyellow:


;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp z,.step1
  jp .step2

;character table at $4000
.step2:
  ld    hl,$4000+160*8
  ret

;character table at $0000
.step1:
  ld    hl,$0000+160*8
  ret


