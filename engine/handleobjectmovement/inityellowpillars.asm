inityellowpillar: ;initialize the pillars, there are multiple pillars, some move up some move down, also init the lenght

  ld a,(currentlevel)
  cp 139
  jp z,inityellowpillar139
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)


  push hl
  
  ld  de,7 +(64 * 8) 
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar1
  
  push hl
  
  ld de,7 +(108 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar2
 
  push hl
 
  ld de,7 +(132 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar3

  push hl
  
  ld de,7 +(156 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar4

  push hl
  
  ld de,7 +(181 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar5 
 
  push hl
  
  ld de,7 +(205 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar6   
  
  push hl
  
  ld de,7 +(266 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar7    

  push hl
  
  ld de,7 +(278 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar8

  push hl
  
  ld de,7 +(326 * 8)
  xor a
  sbc hl,de
  
  pop hl
  
  jp z,.pillar9
  
  
  ret

.pillar9:

  ld a,9
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1
  ld (ix+offsets+1),a

  ld a,10*8
  ld (ix+sizeX),a ;size of object
  
  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret


.pillar8:

  ;;36 tiles deep is the middel pillar or nr 1 the rest is decided by offset
  ld a,9
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a
  ld a,2 ;special upward pillar
  ld (ix+objectrelease),a ;upward pillar

  ld a,19*8
  ld (ix+sizeX),a ;size of object

;  call writenewyellowpillar  
  
  inc (ix+v4)
  ret

.pillar7:

  ;;36 tiles deep is the middel pillar or nr 1 the rest is decided by offset
  ld a,7
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a
  ld a,2 ;special upward pillar
  ld (ix+objectrelease),a ;upward pillar

  ld a,7*8
  ld (ix+sizeX),a ;size of object

;  call writenewyellowpillar  
  
  inc (ix+v4)
  ret


.pillar4:

  ld a,5
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a


  ld a,12*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret

.pillar5:

  ld a,7
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a


  ld a,9*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret

.pillar6:


  ld a,7
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a


  ld a,11*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret


.pillar1:
;pillar 1 decides the first move. all other pillars must synchronise with pillar 1

  ld a,128
  ld (ix+sizeX),a ;size of object

  ld a,1
  ld (ix+v4),a ;end init

  
  jp writenewyellowpillar

;  ret


.pillar2:

  ;;36 tiles deep is the middel pillar or nr 1 the rest is decided by offset
  ld a,9
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a
  ld (ix+objectrelease),a ;upward pillar

  ld a,88
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret

.pillar3:

  ld a,7
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1 ;offset is upwards
  ld (ix+offsets+1),a


  ld a,56
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret

inityellowpillar139:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)


  push hl
  
  ld  de,7 +(50 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar1 

  push hl
  
  ld  de,7 +(66 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar2   

  push hl
  
  ld  de,7 +(90 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar3     
  

  push hl
  
  ld  de,7 +(113 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar4     

  
  push hl
  
  ld  de,7 +(130 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar5
  
  push hl
  
  ld  de,7 +(149 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar6
  
  push hl
  
  ld  de,7 +(173 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar7
  
  push hl
  
  ld  de,7 +(199 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar8
  
  push hl
  
  ld  de,7 +(218 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar9
  
  
  push hl
  
  ld  de,7 +(270 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar10
  
  push hl
  
  ld  de,7 +(288 * 8) 
  xor a
  sbc hl,de
  
  pop hl
 
  jp z,.pillar11     
  
 
    ret
  

.pillar1:
;pillar 1 decides the first move. all other pillars must synchronise with pillar 1

  ld a,7*8
  ld (ix+sizeX),a ;size of object

  ld a,1
  ld (ix+v4),a ;end init

 
  jp writenewyellowpillar

;  ret



.pillar2:

  ld a,4
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,17*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret

.pillar3:

;  ld a,4
;  add a,a
;  add a,a
;  add a,a ;*8 pixel precise offsets
;  ld (ix+offsets),a
;  xor a ;offset is downwards
;  ld (ix+offsets+1),a


  ld a,14*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar4:

  ld a,6
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,10*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar5:

  ld a,2
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  ld a,1
  ld (ix+offsets+1),a


  ld a,10*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar6:

  ld a,6
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,17*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar7:

  ld a,8
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,17*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar8:

  ld a,6
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,10*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar9:

  ld a,8
  add a,a
  add a,a
  add a,a ;*8 pixel precise offsets
  ld (ix+offsets),a
  xor a ;offset is downwards
  ld (ix+offsets+1),a


  ld a,17*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar10:

;  ld a,6
;  add a,a
;  add a,a
;  add a,a ;*8 pixel precise offsets
;  ld (ix+offsets),a
;  xor a ;offset is downwards
;  ld (ix+offsets+1),a


  ld a,7*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret
.pillar11:

;  ld a,6
;  add a,a
;  add a,a
;  add a,a ;*8 pixel precise offsets
;  ld (ix+offsets),a
;  xor a ;offset is downwards
;  ld (ix+offsets+1),a


  ld a,5*8
  ld (ix+sizeX),a ;size of object

  inc (ix+v4)
  
  jp writenewyellowpillar

;  ret


