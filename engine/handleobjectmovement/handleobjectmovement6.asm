;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement6:

;start = 205


  ld a,(pauze)
  dec a
  ret z

;mario sterft alles stop zetten
  ld a,(mariotransforming)
  and a
  ret nz


;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
  ld    l,(ix+movepat)          ;get the movement pattern
  ld    h,(ix+movepat+1)
  ld e,l
  ld d,h ;-----> copy hl to de
  add hl,hl
  add hl,de
  ex de,hl
  ld    hl,movementpatternaddress6
  add   hl,de
  ;substract minimum base adres
  ld de,237*3
  xor a
  sbc hl,de
  jp    (hl)

  ret 
  

movementpatternaddress6:

  jp    MovementPattern237     ;cutscene objects (DO NOT USE IN LEVELENGINE!!)




.return:

  
  ret

  
waitforspace:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp z,.waitforrelease
	
	xor a
	and a
	ret

;keypress release
.waitforrelease:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp nz,.exitcutscene
	
	jp .waitforrelease


.exitcutscene:

;  ld a,1
;  ld (fadeoutscreen?),a
;  ld (endcutscene),a
;  ld a,8
;  ld (fadeoutscreenstep),a

  call cleartekstfromvram ;clear the entire visible part of the VRAM    

  ld (ix+v4),11

  ret  

  
  
cleartekstfromvram: ;clear page 0 and 1 ;added: clear page 2 and 3 as well.

  ;clear entire page with black
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  ld a,16
  out ($9b),a ;36
  xor a
  out ($9b),a
  out ($9b),a
  out ($9b),a
  
  ;256
  ld a,256-16
  out ($9b),a ;40
  ;ld a,1
  xor a
  out ($9b),a
  
  ld hl,212
  ld a,l
  out ($9b),a ;42
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  ;wait for it
  
 ;clear entire page with black
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  ld a,16
  out ($9b),a ;36
  xor a
  out ($9b),a
  out ($9b),a
  ld a,1
  out ($9b),a
  
  ;256
  ld a,256-16
  out ($9b),a ;40
  ;ld a,1
  xor a
  out ($9b),a
  
  ld hl,212
  ld a,l
  out ($9b),a ;42
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  ;wait for it  
  
  
  ret  
  
  
  

waitfor1:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp z,.waitforrelease
	
	xor a
	and a
	ret

;keypress release
.waitforrelease:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp nz,.exitcutscene
	
	jp .waitforrelease


.exitcutscene:

  ld a,1
  ld (fadeoutscreen?),a
  ld (endcutscene),a
  ld a,8
  ld (fadeoutscreenstep),a


  ret




MovementPattern237:     ;cutscene objects (DO NOT USE IN LEVELENGINE!!)


  ld a,(cutscenenr)
  cp 2
  jp z,cutscene2
  cp 3
  jp z,cutscene3
  cp 4
  jp z,cutscene4
  cp 5
  jp z,cutscene5 ;ludwig
  cp 6
  jp z,cutscene6 ;roy
  cp 7
  jp z,cutscene7 ;wendy  
  cp 8
  jp z,cutscene4
  cp 9
  jp z,cutscene8 ;final ever in the game
  cp 10
  jp z,cutscene9 ;monster hall of fame
  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.step1
  dec a
  jp z,.step2


  ret


.step2: ;wait for space

  call .fillblack ;fill with black first (takes highest clock time btw)

  xor a
  ld e,a
  ld a,(cutscenenr)
  dec a
  call z,.set80

  call .trowleftpart
  call .trowrightpart


  call waitfor1

  ret

.set80:

  ld a,80
  ld e,a
  ret

.step1: ;copy in the parts. Fill the middle part of the screen with a 16 px band of blackness then trow the two
;game over parts until they reach te middle of the screen  
  xor a
  ld e,a
  ld a,(cutscenenr)
  dec a
  call z,.set80

  call .fillblack ;fill with black first (takes highest clock time btw)

  call .trowleftpart
  call .trowrightpart

  ld a,(ix+v7)
  cp 100
  jp nc,.setstep2

  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)

  ret


.setstep2:

  inc (ix+v4)


  ret

.trowrightpart:
  
  ld c,(ix+v7)

    ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,48
  add a,e
  out ($9b),a; Xs
  out ($9b),a; Xs

  xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,255-8
  sub c
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,90
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,32
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd


  ret



.trowleftpart:
  
  ld c,(ix+v7)

    ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  add a,e
  out ($9b),a; Xs
  out ($9b),a; Xs

  xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  xor a
  add a,c
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,90
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,32
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd


  ret



.fillblack:

  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  xor a
  out ($9b),a ;36
  out ($9b),a
  ld a,90
  out ($9b),a
  ld a,(sc5page)
  xor 1
  out ($9b),a
  
  ;256
  out ($9b),a ;40
  ld a,1
  out ($9b),a
  
  ld a,16
  out ($9b),a ;42
  xor a
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  call pollvdpcmd

  ret




.init:

;blanc the mario sprite that gets created by the eventhandler
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78 ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 

  inc (ix+v4)

  ret





;include all cutscenes here

  include "handleobjectmovement/cutscene2.asm"


;  ret



endHandleObjectMovement6:


