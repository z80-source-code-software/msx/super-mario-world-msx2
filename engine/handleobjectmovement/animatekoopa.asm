;all the koopa animation routines that are called within the handleobjectmovement routine



animatenakedkoopa:
 
  
  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownnakedkoopa
  cp 3
  jp z,animategreennakedkoopa
  cp 0
  jp z,animatebrownnakedkoopa
  cp 4
  jp z,animatebluenakedkoopa
  cp 2
  jp z,animateyellownakedkoopa
 
  
;  ret




animatebrownnakedkoopa:

  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex107  ;color address
  ld    bc,spriteaddrex107c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex108  ;color address
  ld    bc,spriteaddrex108c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex105  ;color address ;02
  ld    bc,spriteaddrex105c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex106  ;color address ;03
  ld    bc,spriteaddrex106c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animateyellownakedkoopa:

  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex459  ;color address
  ld    bc,spriteaddrex459c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex460  ;color address
  ld    bc,spriteaddrex460c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex457  ;color address ;02
  ld    bc,spriteaddrex457c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex458  ;color address ;03
  ld    bc,spriteaddrex458c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset




animatebluenakedkoopa:
  
;  ret

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex37  ;color address
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex38  ;color address
  ld    bc,spriteaddrex38c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex39  ;color address ;02
  ld    bc,spriteaddrex39c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex40  ;color address ;03
  ld    bc,spriteaddrex40c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret


animategreennakedkoopa:

  ld a,(currentlevel)
  cp 2
  jp z,animategreennakedkoopamap02
  cp 4
  jp z,animategreennakedkoopamap04
 
  
;  ret

animategreennakedkoopamap02:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex129  ;color address
  ld    bc,spriteaddrex129c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex130  ;color address
  ld    bc,spriteaddrex130c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex131  ;color address ;02
  ld    bc,spriteaddrex131c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex132  ;color address ;03
  ld    bc,spriteaddrex132c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret

animategreennakedkoopamap04:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex129  ;color address
  ld    bc,spriteaddrex129c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex130  ;color address
  ld    bc,spriteaddrex130c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex131  ;color address ;02
  ld    bc,spriteaddrex131c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex132  ;color address ;03
  ld    bc,spriteaddrex132c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret





animatenakedkoopaslide:
 
  
  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownnakedkoopaslide
  cp 3
  jp z,animategreennakedkoopaslide
  cp 0
  jp z,animatebrownnakedkoopaslide  
  cp 4
  jp z,animategreennakedkoopaslide
  cp 2
  jp z,animateyellownakedkoopaslide

  
;  ret

animatebrownnakedkoopaslide:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  and a
  jp    z,.position0
  dec   a
  jp    z,.position1

  ret

.position0:
  ld    hl,spriteaddrex103  ;color address
  ld    bc,spriteaddrex103c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex101  ;color address
  ld    bc,spriteaddrex101c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animateyellownakedkoopaslide:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  and a
  jp    z,.position0
  dec   a
  jp    z,.position1

  ret

.position0:
  ld    hl,spriteaddrex455  ;color address
  ld    bc,spriteaddrex455c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex453  ;color address
  ld    bc,spriteaddrex453c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset




animategreennakedkoopaslide:

  ld a,(currentlevel)
  cp 2
  jp z,animategreennakedkoopaslidemap02
  
;  ret

animategreennakedkoopaslidemap02:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  and a
  jp    z,.position0
  dec   a
  jp    z,.position1

  ret

.position0:
  ld    hl,spriteaddrex125  ;color address
  ld    bc,spriteaddrex125c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex127  ;color address
  ld    bc,spriteaddrex127c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  


;ret



animatekoopa:
 
  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownkoopa
  cp 3
  jp z,animategreenkoopa
  cp 0
  jp z,animatebrownkoopa
  cp 4
  jp z,animategreenkoopa


;ret

animatebrownkoopa:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex97  ;color address
  ld    bc,spriteaddrex97c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex98  ;color address
  ld    bc,spriteaddrex98c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex99  ;color address ;02
  ld    bc,spriteaddrex99c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex100  ;color address ;03
  ld    bc,spriteaddrex100c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

animategreenkoopa:
  
  ld a,(currentlevel)
  cp 2
  jp z,animategreenkoopamap02 
  cp 4
  jp z,animategreenkoopamap02 
;  ret



animategreenkoopamap02:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex121  ;color address
  ld    bc,spriteaddrex121c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex122  ;color address
  ld    bc,spriteaddrex122c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex123  ;color address ;02
  ld    bc,spriteaddrex123c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex124  ;color address ;03
  ld    bc,spriteaddrex124c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;ret



turnshellupsidedown:

  ld    (ix+updatespr?),2
  

  ld a,(ix+objectrelease)
  cp 1
  jp z,.turnbrownshellupsidedown
  cp 3
  jp z,.turngreenshellupsidedown
  cp 6
  jp z,.turncavekoopashellupsidedown
  cp 4
  jp z,.turngreenshellupsidedown
  cp 2
  jp z,.turnyellowshellupsidedown



.turnpinkshellupsidedown:
  
;meteen de sprite updaten  
  ld    hl,spriteaddrex36  ;color address
  ld    bc,spriteaddrex36c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.turnyellowshellupsidedown:
  
;meteen de sprite updaten  
  ld    hl,spriteaddrex452  ;color address
  ld    bc,spriteaddrex452c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.turnbrownshellupsidedown:

;meteen de sprite updaten  
  ld    hl,spriteaddrex118  ;color address
  ld    bc,spriteaddrex118c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.turngreenshellupsidedown:

;  ld a,(currentlevel)
;  cp 2
;  jp z,.turngreenshellupsidedownmap02
;
;  ret


.turngreenshellupsidedownmap02:

;meteen de sprite updaten  
  ld    hl,spriteaddrex142  ;color address
  ld    bc,spriteaddrex142c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  

.turncavekoopashellupsidedown:

  ld    hl,spriteaddrex414  ;color address
  ld    bc,spriteaddrex414c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;ret



animatestopshell:

  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownstopshell
  cp 3
  jp z,animategreenstopshell
  cp 6
  jp z,animatecavekoopastopshell
  cp 4
  jp z,animategreenstopshell
  cp 2
  jp z,animateyellowstopshell
  
  

animatepinkstopshell:  


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex10  ;color address
  ld    bc,spriteaddrex10c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp   setcharcolandoffset


animateyellowstopshell:  


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex448  ;color address
  ld    bc,spriteaddrex448c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp   setcharcolandoffset



animatebrownstopshell:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex112  ;color address
  ld    bc,spriteaddrex112c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp   setcharcolandoffset



animategreenstopshell:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex136  ;color address
  ld    bc,spriteaddrex136c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp   setcharcolandoffset


animatecavekoopastopshell:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex415  ;color address
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp   setcharcolandoffset




animateshell:
 


  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownshell
  cp 3
  jp z,animategreenshell
  cp 6
  jp z,animatecavekoopashell
  cp 4
  jp z,animategreenshell
  cp 2
  jp z,animateyellowshell  
  
;  ret

animatepinkshell:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex10  ;color address
  ld    bc,spriteaddrex10c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex11  ;color address
  ld    bc,spriteaddrex11c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex12  ;color address ;02
  ld    bc,spriteaddrex12c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex13  ;color address ;03
  ld    bc,spriteaddrex13c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



animateyellowshell:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex448  ;color address
  ld    bc,spriteaddrex448c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex449  ;color address
  ld    bc,spriteaddrex449c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex450  ;color address ;02
  ld    bc,spriteaddrex450c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex451  ;color address ;03
  ld    bc,spriteaddrex451c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



animatebrownshell:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex112  ;color address
  ld    bc,spriteaddrex112c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex113  ;color address
  ld    bc,spriteaddrex113c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex114  ;color address ;02
  ld    bc,spriteaddrex114c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex115  ;color address ;03
  ld    bc,spriteaddrex115c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  

animategreenshell:

  ld a,(currentlevel)
  cp 2
  jp z,animategreenshellmap02

 ; ret


animategreenshellmap02:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex136  ;color address
  ld    bc,spriteaddrex136c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex137  ;color address
  ld    bc,spriteaddrex137c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex138  ;color address ;02
  ld    bc,spriteaddrex138c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex139  ;color address ;03
  ld    bc,spriteaddrex139c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset




animatecavekoopashell:



  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex415  ;color address
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex416  ;color address
  ld    bc,spriteaddrex416c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex417  ;color address ;02
  ld    bc,spriteaddrex417c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex418  ;color address ;03
  ld    bc,spriteaddrex418c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  

;ret


animateshellupsidedown:


  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownshellupsidedown
  cp 3
  jp z,animategreenshellupsidedown
  cp 6
  jp z,animatecavekoopashellupsidedown
  cp 4
  jp z,animategreenshellupsidedown




animatepinkshellupsidedown:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex36  ;color address
  ld    bc,spriteaddrex36c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset





animatebrownshellupsidedown:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex118  ;color address
  ld    bc,spriteaddrex118c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animategreenshellupsidedown:

  ld a,(currentlevel)
  cp 2
  jp z,animategreenshellupsidedownmap02

  ret


animategreenshellupsidedownmap02:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex142  ;color address
  ld    bc,spriteaddrex142c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;ret



animatecavekoopashellupsidedown:

;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;  
;  .position0:
;  ld    hl,spriteaddrex414  ;color address
;  ld    bc,spriteaddrex414c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock3
;  jp    setcharcolandoffset

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex597  ;color address
  ld    bc,spriteaddrex597c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


;ret




animaterevertkoopa:

  ld a,(ix+objectrelease)
  cp 1
  jp z,revertbrown
  cp 3
  jp z,revertgreen
  cp 4
  jp z,revertgreen   
  cp 6
  jp z,animatecavekoopashellupsidedown


revertpink:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex36  ;color address
  ld    bc,spriteaddrex36c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;;spagethi danger all upsidedown code or any animation that concerns cavekoopa gets bounced to this label.
;revertcave:
;
;
;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;  
;  .position0:
;  ld    hl,spriteaddrex597  ;color address
;  ld    bc,spriteaddrex597c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock4
;  jp    setcharcolandoffset


revertbrown:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex120  ;color address
  ld    bc,spriteaddrex120c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


revertgreen:

  ld a,(currentlevel)
  cp 2
  jp z,revertgreenmap02
  
  ret
  
revertgreenmap02:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
.position0:
  ld    hl,spriteaddrex144  ;color address
  ld    bc,spriteaddrex144c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

  
;ret


animatenakedkoopatransforming:

  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownnakedkoopatransforming
  cp 3
  jp z,animategreennakedkoopatransforming
  cp 0
  jp z,animatebrownnakedkoopatransforming
  cp 4
  jp z,animategreennakedkoopatransforming
  cp 2
  jp z,animateyellownakedkoopatransforming
  
 ; ret

animatebrownnakedkoopatransforming:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex101  ;color address
  ld    bc,spriteaddrex101c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex102  ;color address
  ld    bc,spriteaddrex102c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex103  ;color address ;02
  ld    bc,spriteaddrex103c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex104  ;color address ;03
  ld    bc,spriteaddrex104c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret



animateyellownakedkoopatransforming:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex453  ;color address
  ld    bc,spriteaddrex453c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex454  ;color address
  ld    bc,spriteaddrex454c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex455  ;color address ;02
  ld    bc,spriteaddrex455c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex456  ;color address ;03
  ld    bc,spriteaddrex456c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



;ret



animategreennakedkoopatransforming:


  ld a,(currentlevel)
  cp 2
  jp z,animategreennakedkoopatransformingmap02
  
;  ret

animategreennakedkoopatransformingmap02:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex127  ;color address
  ld    bc,spriteaddrex127c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex128  ;color address
  ld    bc,spriteaddrex128c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex125  ;color address ;02
  ld    bc,spriteaddrex125c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex126  ;color address ;03
  ld    bc,spriteaddrex126c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret
  

animatenakedkoopaupsidedown:

  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownnakedkoopaupsidedown
  cp 3
  jp z,animategreennakedkoopaupsidedown
  cp 0
  jp z,animatebrownnakedkoopaupsidedown
  cp 4
  jp z,animategreennakedkoopaupsidedown  
  cp 2
  jp z,animateyellownakedkoopaupsidedown 
  

  ld (ix+v4),3 ;take action anyway even if the animation is not present!

  ret
  

animatebrownnakedkoopaupsidedown:

  
  ld (ix+v4),3
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position01
  dec   a
  jp    z,.position02
  dec   a
  jp    z,.position03

.position00:
  ld    hl,spriteaddrex110  ;color address
  ld    bc,spriteaddrex110c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position01:
  ld    hl,spriteaddrex110  ;color address
  ld    bc,spriteaddrex110c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position02:
  ld    hl,spriteaddrex109  ;color address ;02
  ld    bc,spriteaddrex109c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position03:
  ld    hl,spriteaddrex109  ;color address ;03
  ld    bc,spriteaddrex109c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret

animateyellownakedkoopaupsidedown:

  
  ld (ix+v4),3
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position01
  dec   a
  jp    z,.position02
  dec   a
  jp    z,.position03

.position00:
  ld    hl,spriteaddrex462  ;color address
  ld    bc,spriteaddrex462c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position01:
  ld    hl,spriteaddrex462  ;color address
  ld    bc,spriteaddrex462c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position02:
  ld    hl,spriteaddrex461  ;color address ;02
  ld    bc,spriteaddrex461c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position03:
  ld    hl,spriteaddrex461  ;color address ;03
  ld    bc,spriteaddrex461c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



;ret




animategreennakedkoopaupsidedown:

  ld a,(currentlevel)
  cp 2
  jp z,animategreennakedkoopaupsidedownmap02
  
;  ret
  
animategreennakedkoopaupsidedownmap02:

    
  ld (ix+v4),3
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position01
  dec   a
  jp    z,.position02
  dec   a
  jp    z,.position03

.position00:
  ld    hl,spriteaddrex134  ;color address
  ld    bc,spriteaddrex134c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position01:
  ld    hl,spriteaddrex134  ;color address
  ld    bc,spriteaddrex134c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position02:
  ld    hl,spriteaddrex133  ;color address ;02
  ld    bc,spriteaddrex133c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position03:
  ld    hl,spriteaddrex133  ;color address ;03
  ld    bc,spriteaddrex133c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret
  

turnkoopaaround:

  inc (ix+v4)
  ld a,(ix+v4)
  cp 4
  jp z,.endturnaround


  ld a,(ix+objectrelease)
  cp 1
  jp z,turnbrownkoopaaround
  cp 3
  jp z,turngreenkoopaaround
  cp 0
  jp z,turnbrownkoopaaround
  cp 4
  jp z,turngreenkoopaaround

  ret

.endturnaround:

  ld (ix+v4),0

  ret


turnbrownkoopaaround:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex116  ;color address
  ld    bc,spriteaddrex116c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex116  ;color address
  ld    bc,spriteaddrex116c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex117  ;color address ;02
  ld    bc,spriteaddrex117c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex117  ;color address ;03
  ld    bc,spriteaddrex117c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

turngreenkoopaaround:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex140  ;color address
  ld    bc,spriteaddrex140c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex140  ;color address
  ld    bc,spriteaddrex140c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex141  ;color address ;02
  ld    bc,spriteaddrex141c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex141  ;color address ;03
  ld    bc,spriteaddrex141c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret


animateturnshell:

.position0:
    ld b,0
    jp .checkcolor
.position2:
    ld b,1

.checkcolor:

  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownturnshell
  cp 3
  jp z,animategreenturnshell
  cp 6
  jp z,animatecavekoopatuenshell
  cp 4
  jp z,animategreenturnshell 
  cp 2
  jp z,animateyellowturnshell 
  

animatepinkturnshell:

  ld a,b
  and a
  jp nz,.position2

.position0:
  ld    hl,spriteaddrex10  ;color address
  ld    bc,spriteaddrex10c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex12  ;color address ;02
  ld    bc,spriteaddrex12c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animateyellowturnshell:

  ld a,b
  and a
  jp nz,.position2

.position0:
  ld    hl,spriteaddrex448  ;color address
  ld    bc,spriteaddrex448c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex450  ;color address ;02
  ld    bc,spriteaddrex450c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


animatebrownturnshell:

  ld a,b
  and a
  jp nz,.position2


.position0:
  ld    hl,spriteaddrex112  ;color address
  ld    bc,spriteaddrex112c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex114  ;color address ;02
  ld    bc,spriteaddrex114c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animategreenturnshell:

  ld a,(currentlevel)
  cp 2
  jp z,animategreenturnshellmap02

  ret


animategreenturnshellmap02:

  
  ld a,b
  and a
  jp nz,.position2


.position0:
  ld    hl,spriteaddrex136  ;color address
  ld    bc,spriteaddrex136c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex138  ;color address ;02
  ld    bc,spriteaddrex138c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



animatecavekoopatuenshell:

  ld a,b
  and a
  jp nz,.position2

.position0:
  ld    hl,spriteaddrex415  ;color address
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex417  ;color address ;02
  ld    bc,spriteaddrex417c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  


animateturnshellupsidedown:

.position0:
    ld b,0
    jp .checkcolor
.position2:
    ld b,1

.checkcolor:

  ld a,(ix+objectrelease)
  cp 1
  jp z,animatebrownturnshellupsidedown
  cp 3
  jp z,animategreenturnshellupsidedown
  cp 4
  jp z,animategreenturnshellupsidedown
  cp 6
  ret z
  

animatepinkturnshellupsidedown:

  ld a,b
  and a
  jp nz,.position2

.position0:
  ld    hl,spriteaddrex36  ;color address
  ld    bc,spriteaddrex36c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex36  ;color address ;02
  ld    bc,spriteaddrex36c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

animatebrownturnshellupsidedown:

  ld a,b
  and a
  jp nz,.position2


.position0:
  ld    hl,spriteaddrex118  ;color address
  ld    bc,spriteaddrex118c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex119  ;color address ;02
  ld    bc,spriteaddrex119c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


animategreenturnshellupsidedown:

  ld a,(currentlevel)
  cp 2
  jp z,animategreenturnshellupsidedownmap02

  ret
  
  
animategreenturnshellupsidedownmap02:


  ld a,b
  and a
  jp nz,.position2


.position0:
  ld    hl,spriteaddrex142  ;color address
  ld    bc,spriteaddrex142c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex143  ;color address ;02
  ld    bc,spriteaddrex143c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
    


