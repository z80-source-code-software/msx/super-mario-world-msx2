;wow block nr 10....

HandleObjectMovement10:

;start = 330


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz

  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress10 - 330 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)


;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress10
;  add   hl,de
;  ;substract minimum base adres
;  ld de,330*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress10:

  jp    MovementPattern330     ;special yellow pillar (maplevel6-07c)
  jp    MovementPattern331     ;special yellow pillar (helper platform)
  jp    MovementPattern332     ;castleflame
  jp    MovementPattern333     ;wooden pillar (down up)
  jp    MovementPattern334     ;wooden pillar (up down)
  jp    MovementPattern335     ;wendy superspike
  jp    MovementPattern336     ;circlesaw (left right)
  jp    MovementPattern337     ;wendy's castle giant moving blocks
  jp    MovementPattern338     ;wendy's castle small moving blocks
  jp    MovementPattern339     ;wendyfireball
  jp    MovementPattern340     ;wendyfireballxl
  jp    MovementPattern341     ;wendy (boss)
  jp    MovementPattern342     ;watercannon
  jp    MovementPattern343     ;ghost many
  jp    MovementPattern344     ;mega mole
  jp    MovementPattern345     ;countplateau
  jp    MovementPattern346     ;lavaplateau
  jp    MovementPattern347     ;scooping chuck
  jp    MovementPattern348     ;chuckball
  jp    MovementPattern349     ;minichuck (from scooping chuck)
  jp    MovementPattern350     ;spike and lava killer  
  jp    MovementPattern351     ;special larry's castle moving platform 
  jp    MovementPattern352     ;mario cutscene helper object
  jp    MovementPattern353     ;mario cutscene helper object (ghost)
  jp    MovementPattern354     ;mario cutscene helper object (ghost cloud)
  jp    MovementPattern355     ;permayoshi
;  jp    MovementPattern356     ;babyyoshi
  
  

.return:
  

  ret

  
killallenemieswendy:  

  push ix
  pop de
  push ix

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 20

  pop ix

  ret

.handleobject:    


    ;not itself
    push ix
    pop hl
    xor a
    sbc hl,de
    ret z
    
    

    call destroyobject

    ret


  
  
MovementPattern355:     ;permayoshi  
  

  ld a,(ix+v3)
  dec a
  jp z,.waitforcrack
  dec a
  jp z,.waitforcrack2 ;wait for yoshi to come out
 ; dec a
 ; jp z,.growyoshi
;  dec a
;  jp z,.yoshi
  
  
  
  ld a,(ix+v4)
  dec a
  jp z,.spawnbaby
  
  
  call getmariodistancexy
  ld a,c
  cp 50
  ret nc
  ld a,b
  cp 50
  ret nc
  
  
  jp .checkforottheryoshi
  
  
;  ret


.waitforcrack:

  ld a,(ix+v1)
  cp 15
  jp z,.begincrack
  
  inc (ix+v1)



  ret

.begincrack:


  ld (ix+v3),2 ;begin the crack
  ld (ix+v1),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex172  ;color address ;02
  ld    bc,spriteaddrex172c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.waitforcrack2:

  ld a,(ix+v1)
  cp 10
  jp z,.beginyoshi
  
  inc (ix+v1)



  ret


  
.beginyoshi:


    ld (ix+v1),0
  

;spawn the debris
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

  ld (hlvar),hl
  ld (bcvar),de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),80               ;set object number (80 = yoshi egg debris)
  ld    (iy+6),0               ;set object number (80 = yoshi egg debris)

  call  activateobject

  ld (ix+deathtimer),0
;  ld (ix+clipping),0
;  ld (ix+movepat),71

  ld hl,(hlvar)
  ld de,(bcvar)


  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),81               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0

 
  pop ix
 

    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddressbabyyoshi  ;color address ;02
  ld    bc,characteraddressbabyyoshi  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  call    setcharcolandoffset

 ; ld (ix+objectrelease),255 ;confirm that yoshi has now spawned and set the objectrelease properly
  
  
  ;    ld (ix+v3),3 ;yoshi comes out
    ld (ix+v4),0
    ld (ix+v3),0
    ld (ix+v2),0
    ld (ix+v1),0 ;reset all important values
  
    ld hl,356 ;change to yoshi baby itself
  
    ld (ix+movepat),l
    ld (ix+movepat+1),h
  
  
  ret

  


.spawnbaby:  
    
  ;copy paste from yoshi egg spawn but new object is yoshi baby
  
  ld (ix+v2),0
  ld (ix+v3),1 ;this indicates that the egg is ready to spawn
  ld (ix+v1),0 ;this is our timer for the hatching process
  ;inc (ix+v4)

  
  
  ret
  
  

.checkforottheryoshi:  
  
  
 ;if there is a yoshi around the egg will refuse to hatch 
  ld (ix+objectrelease),0 ;temp disable yoshi block to prevent self detection
  
  
  push ix
  

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 20

  pop ix  
  
  
  inc (ix+v4) ;yep we have a yoshi
  

    ld (ix+objectrelease),255 ;reenable yoshi blockade
  
  ret
  
  
  
.stop:



    pop ix

  ld (ix+objectrelease),255

    ;nope
    ret
  
  
  
  
  
MovementPattern354:     ;mario cutscene helper object (ghost cloud)  
  
  call addv1toxbject
  
  
  ld (ix+v1),0
  
  ld a,(framecounter)
  and 3
  ret nz
  
  
  ld (ix+v1),-1
  
  
  
  
  ret
  
  
  
  
  
;while very similar there are differences. The ghosthouse uses tiles to move the gate. The tiles are located on the tilemap itself and thus do not need outing to the vdp. I have sorted the tiles in such a fashion
;that the masscopies for the gate are easy. The object is placed @ the doortiles so finding the mapadress is easy peasy
MovementPattern353:     ;mario cutscene helper object (ghost)  
  
    ld a,(ix+v4)
    and a
    call nz,.readkey
      
      
  ld a,(ix+v4)
  and a
  jp z,MovementPattern352.init
    dec a
  jp z,.moveright ;first move
  dec a
  jp z,MovementPattern352.wait
  dec a
  jp z,MovementPattern352.checkunmountyoshi
  dec a
  jp z,MovementPattern352.unmountyoshi ;optional step if mario is on yoshi
  dec a
  jp z,MovementPattern352.setright 
  dec a
  jp z,MovementPattern352.wait
  dec a
  jp z,MovementPattern352.lookup  
  dec a
  jp z,.waitl
  dec a
  jp z,.doorsfx
  dec a
  jp z,.door1
  dec a
  jp z,.wait
  dec a
  jp z,.door2
  dec a
  jp z,.wait
  dec a
  jp z,.door3
  dec a
  jp z,.waitl
  dec a
  jp z,MovementPattern352.finalmove  
  dec a
  jp z,.wait
  dec a
  jp z,.door21
  dec a
  jp z,.wait
  dec a
  jp z,.door11
  dec a
  jp z,.wait
  dec a
  jp z,.door0
  dec a
  jp z,.wait
  dec a
  jp z,MovementPattern352.fadeout ;23
  dec a
  jp z,MovementPattern352.waitl
  dec a
  jp z,MovementPattern352.waitl
  dec a  
  jp z,MovementPattern352.endlevel   


  
  
  ret

.readkey:


;read any key to skip the cutscene
  ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

;nu nog de toetsen goed omdraaien
;om de readout compatible te maken met de oude routine

  ld b,0

  rla
  rl b
  sra a
  sra a
  sra a
  sra a
  sra a
  sra a ;naar links in de carry
  rl b
  sla a
  sla a
  sla a
  sla a
  sla a
  sla a
  srl b
  rra
  srl b
  rra

  
  ld d,a ;even het resultaat wegschrijven
 
;C uitlezen 
  
  ld b,3 ;C
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
  ld c,a

;Z en X uitlezen 
 
  ld b,5 ;Z en X
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A


 ;x in de carry douwen 
  srl a
  srl a
  srl a
  srl a
  srl a
  srl a
  rr d
  ;c in de carry
  srl c
  rr d
  ;Z in de carry
  srl a
  srl a
	rr d
	;eventueel nog een extra key vanaf hier
	xor a ;carry resetten
	
	;laatste verschuiving om alles mooi op zijn plek te krijgen in de bit matrix
	rr d
	
	;resultaat in accumulator gooien
  ld a,d

  cpl

    res 7,a
    and a
    
    jp nz,.endscene
 
    ret

    
.endscene:

    xor a
  ld (Controls),a
  ld (ix+v4),23  

  ret
 
 
.doorsfx:


    call Playsfx.door
    
    jp .incv4

    
    
 
.door11:  

  ;write back the bkg
  
 
    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    
    inc hl
    
    ld a,25 ;door 1 first tile
    ld de,(maplenght)
    
    push hl
    
    
    call .writerow
    
    pop hl
    
    
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl

    

    ld a,25 ;door 1 first tile
    ld de,(maplenght)
    
    
    
    call .writerow    
    
    
;    call .door21
    
    jp .door1


;    ret 
    


.door21:  
  ;write back the bkg
  
 
    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    

    
    ld a,57 ;door 1 first tile
    ld de,(maplenght)
    
    push hl
    
    
    call .writerow
    
    pop hl
    
    
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl
    

    ld a,89 ;door 1 first tile
    ld de,(maplenght)
    
    
    
    call .writerow    
    
    
    jp .door2


;    ret 
  
  
  
  
  
  
  ret
  
  
  
  
.writeempty:

    ld b,6
    ld a,1

.emptyloop:


    call writeanytile
    add hl,de
    
    djnz .emptyloop

    ret
    
.writerow:

    ld b,6 ;repeat so we get 1 row

.writerowloop:

    call writeanytile
    inc a
    add hl,de
    
    djnz .writerowloop
  
    ret
  
.door0:

 

    call Playsfx.explosion
 
    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    
    inc hl
    inc hl
    
    push hl ;save for next row
    
    ld a,109 ;door 1 first tile
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row
    
    push hl

    ld a,115
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row  
    
    push hl
    
    ld a,77
    
    call .writerow
    
    pop hl
    
    inc hl
    
    ld a,83
    
    call .writerow
    
    
    
    
    inc (ix+v4)


    ret




.door1:

    
    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    
    inc hl
    inc hl ;move to the right door is still in boundaries
    
    push hl ;save for next row
    
    ld a,13 ;door 1 first tile
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row
    
    push hl
    
    call .writeempty
    
    pop hl
    
        inc hl
        
    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl
    
    ld a,19
    
    call .writerow
    
    inc (ix+v4)


    ret


.door2: 

    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    
    inc hl ;move to the right but now write earlier
    
    push hl ;save for next row
    
    ld a,45 ;door 1 first tile
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row
    
    push hl
    
    call .writeempty
    
    pop hl
    
        inc hl
        
    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl
    
    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl

    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl    
    
    
    ld a,51
    
    call .writerow
    
    inc (ix+v4)


    ret



.door3:  
 
 

    ld de,0
    ld bc,8
    call objecttilecheckfromrom ;fetch location on map
    
    push hl ;save for next row
    
    ld a,77 ;door 1 first tile
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row
    
    push hl

    ld a,83
    ld de,(maplenght)
    
    call .writerow
    
    pop hl
    
        inc hl ;next row
    
    push hl    
    
    
    call .writeempty
    
    pop hl
    
        inc hl
        
    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl
    
    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl

    push hl
    
    
    call .writeempty
    
    pop hl
    
    inc hl    
    
    push hl
    
    ld a,109
    
    call .writerow
    
    pop hl
    
    inc hl
    
    ld a,115
    
    call .writerow
    
    
    
    
    inc (ix+v4)


    ret

  
  
  
.waitl:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 30
    ret c
    
    jp .incv4
  
  
.wait:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 10
    ret c
    
    jp .incv4
  

;moveright
.moveright:

  ld a,%00001000 ;right
  ld (Controls),a
  
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 25
    ret c
    
    ld a,(marioyoshi)
    and a
    jp z,MovementPattern352.extraframe  
  
    xor a
    ld (Controls),a
  
  
.incv4:

    inc (ix+v4)
    ld (ix+v7),0
    
    ret
 


 
  

;animations for castle gate
castle1animation:                      incbin "../grapx/cutscenes/castle1/animation.SC2",$0000+7,8*32  ;character table
castle1animationclr:                   incbin "../grapx/cutscenes/castle1/animation.SC2",$2000+7,8*32  ;color table


animatecastlegate:

  inc (ix+v5)
  inc (ix+v5)
  ld a,(ix+v5) ;use v5 as variable

  cp 0
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
  cp 7
  jp z,.step7  
  cp 8
  jp z,.step8
  cp 9
  jp z,.step9
  cp 10
  jp z,.step10
  cp 11
  jp z,.step11
  cp 12
  jp z,.step12
  cp 13
  jp z,.step13
  cp 14
  jp z,.step14
;  cp 15
;  jp z,.step15 
  
  
  jp .step15
  
;  ret


.down:

  inc (ix+v5)
  inc (ix+v5)
  ld a,(ix+v5) ;use v5 as variable

  cp 0
  jp z,.step15
  cp 1
  jp z,.step14
  cp 2
  jp z,.step13
  cp 3
  jp z,.step12
  cp 4
  jp z,.step11
  cp 5
  jp z,.step10
  cp 6
  jp z,.step9
  cp 7
  jp z,.step8  
  cp 8
  jp z,.step7
  cp 9
  jp z,.step6
  cp 10
  jp z,.step5
  cp 11
  jp z,.step4
  cp 12
  jp z,.step3
  cp 13
  jp z,.step2
  cp 14
  jp z,.step1
;  cp 15
;  jp z,.step15 
  
  ld (ix+v5),0
  
  jp .step0
  
;  ret

  

.writestep:

;  push hl
;  push de

  ld a,124
  ld b,2
  call updatewendyblocks
  
  

  ret


.step0:

  ld hl,castle1animation+(8*0)
  ld de,castle1animationclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,castle1animation+(8*2)
  ld de,castle1animationclr+(8*2)   

  jp .writestep

.step2:

  ld hl,castle1animation+(8*4)
  ld de,castle1animationclr+(8*4)   

  jp .writestep

.step3:

  ld hl,castle1animation+(8*6)
  ld de,castle1animationclr+(8*6)   

  jp .writestep

.step4:

  ld hl,castle1animation+(8*8)
  ld de,castle1animationclr+(8*8)   

  jp .writestep

.step5:

  ld hl,castle1animation+(8*10)
  ld de,castle1animationclr+(8*10)   

  jp .writestep

.step6:

  ld hl,castle1animation+(8*12)
  ld de,castle1animationclr+(8*12)  

  jp .writestep
  
.step7:

  ld hl,castle1animation+(8*14)
  ld de,castle1animationclr+(8*14)   

  jp .writestep
  
.step8:

  ld hl,castle1animation+(8*16)
  ld de,castle1animationclr+(8*16)   

  jp .writestep
  
.step9:

  ld hl,castle1animation+(8*18)
  ld de,castle1animationclr+(8*18)   

  jp .writestep
  
.step10:

  ld hl,castle1animation+(8*20)
  ld de,castle1animationclr+(8*20)   

  jp .writestep
  
.step11:

  ld hl,castle1animation+(8*22)
  ld de,castle1animationclr+(8*22)   

  jp .writestep
  
.step12:

  ld hl,castle1animation+(8*24)
  ld de,castle1animationclr+(8*24)   

  jp .writestep
  
.step13:

  ld hl,castle1animation+(8*26)
  ld de,castle1animationclr+(8*26)   

  jp .writestep
  
.step14:

  ld hl,castle1animation+(8*28)
  ld de,castle1animationclr+(8*28)   

  jp .writestep  

.step15:

  ld hl,castle1animation+(8*30)
  ld de,castle1animationclr+(8*30)   

    ld (ix+v5),0 ;reset animation counter
  
  jp .writestep  

;basicly it works like this. We overwrite readcontrols and the status bar on the engine during init. Then we have full control over mario while the
;entire engine still runs perfectltly with music and everything. We control the movement of the gate from this object too.


;Standard keyboard mapping/ this is the same for joystick
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		     	Z 	C 		  X   	  right	      left	    down	  up	(keyboard)

MovementPattern352:     ;mario cutscene helper object  
 
    ld a,(ix+v4)
    and a
    call nz,.readkey
 
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.moveright ;first move
  dec a
  jp z,.wait
  dec a
  jp z,.checkunmountyoshi
  dec a
  jp z,.unmountyoshi ;optional step if mario is on yoshi
  dec a
  jp z,.setright
  dec a
  jp z,.waitl
  dec a
  jp z,.lookup
  dec a
  jp z,.waitl  
  dec a
  jp z,.opengatesfx ;this will not be fun. ghosthouse and castle will be different in this one not to mention all the extra sprite and tile operations that need to be done.
  dec a
  jp z,.opengate
  dec a
  jp z,.waitl
  dec a
  jp z,.finalmove
  dec a
  jp z,.closegate
  dec a
  jp z,.writefinaltiles
  dec a
  jp z,.waitl
  dec a
  jp z,.fadeout ;16
  dec a
  jp z,.waitl
  dec a
  jp z,.waitl
  dec a  
  jp z,.endlevel

  
  
  ret

  
.readkey:


;read any key to skip the cutscene
  ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

;nu nog de toetsen goed omdraaien
;om de readout compatible te maken met de oude routine

  ld b,0

  rla
  rl b
  sra a
  sra a
  sra a
  sra a
  sra a
  sra a ;naar links in de carry
  rl b
  sla a
  sla a
  sla a
  sla a
  sla a
  sla a
  srl b
  rra
  srl b
  rra

  
  ld d,a ;even het resultaat wegschrijven
 
;C uitlezen 
  
  ld b,3 ;C
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
  ld c,a

;Z en X uitlezen 
 
  ld b,5 ;Z en X
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A


 ;x in de carry douwen 
  srl a
  srl a
  srl a
  srl a
  srl a
  srl a
  rr d
  ;c in de carry
  srl c
  rr d
  ;Z in de carry
  srl a
  srl a
	rr d
	;eventueel nog een extra key vanaf hier
	xor a ;carry resetten
	
	;laatste verschuiving om alles mooi op zijn plek te krijgen in de bit matrix
	rr d
	
	;resultaat in accumulator gooien
  ld a,d

  cpl

    res 7,a
    and a
    
    jp nz,.endscene
 
    ret

    
.endscene:

    xor a
  ld (Controls),a
  ld (ix+v4),16  

  ret
  
.writefinaltiles:

    call animatecastlegate.step0
    
    jp incv4
  
  
.addtiles:
 
    
    ld de,0
    ld bc,6
    call objecttilecheckfromrom
 
    cp 125 ;tile is allready written here so skip
    ret z
 
 
    ;get the mapadress and write tiles to the locations
    ld a,124
    ld c,1
    ld b,6
    call writemanytile
    ld de,6
    xor a
    sbc hl,de
    ld de,(maplenght)
    add hl,de
    ld a,125
    ld c,1
    ld b,6
    call writemanytile  

    ret

  
.closegate:


    ;place open door code here. Now we have one huge sprite which is this object itself. It doesn't matter it does what it should do. Speed is defined in the infolist
    call .addtiles ;add the background tiles

    call animatecastlegate.down    
    
    call addv2toybject
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 23
    ret c ;after this stop and end

    call Playsfx.explosion

    jp incv4
  
  
.removetiles:


    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    
    ;get the mapadress and write tiles to the locations
    ld a,128
    ld c,1
    ld b,6
    call writemanytile
    ld de,6
    xor a
    sbc hl,de
    ld de,(maplenght)
    add hl,de
    ld a,128
    ld c,1
    ld b,6
    call writemanytile    
    
    
    ret 
  
  
  
.opengate:

    ;place open door code here. Now we have one huge sprite which is this object itself. It doesn't matter it does what it should do. Speed is defined in the infolist
    call .removetiles ;remove the background tiles
    
    
    call animatecastlegate
    
    call addv2toybject
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 23
    ret c ;after this stop and end
    
    

    ld (ix+v2),2 ;reverse gate speed for later


    jp incv4
  
  
.endlevel:

  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a
  
  ret
  
  
.finalmove:


  ld a,%00001000 ;right
  ld (Controls),a
  
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 40
    ret c    

    jp incv4
    
    
.fadeout:  
  
;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
;  call Playsfx.explosion
  
  jp incv4
  
  
.setright:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 20
    ret c ;give it some time otherwise we mis the engine' s checks


    ld a,1
    ld (marioleftrightspr),a
    ld (marioleftright),a


        jp .incv4
        
    
  
.opengatesfx:

    call Playsfx.door


    jp incv4
  
  
  

.lookup:



    ;jump en move to right
    
  ld a,%00000001 ;up
  ld (Controls),a

    ld a,(ix+v7)
    inc (ix+v7)
    cp 30
    ret c
    
    jp .incv4
  

    ;ret

    
  
.waitl:  

    ld a,(ix+v7)
    inc (ix+v7)
    cp 30
    ret c
  
    jp .incv4
    
    
.wait:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 10
    ret c
    
  
    jp .incv4
    
  
  
  
.checkunmountyoshi:


    ld a,(marioyoshi) ;skip if mario is not on yoshi at all
    and a
    jp z,.incv42x


    jp .incv4
    
    
.unmountyoshi:    
    
    

    ;jump en move to right
    
  ld a,%01001000 ;right + jump
  ld (Controls),a

    ld a,(ix+v7)
    inc (ix+v7)
    cp 6
    ret c

  
    jp .incv4
  

    ;ret
  
.extraframe:

    ld a,(ix+v3)
    inc (ix+v3)
    cp 10
    ret c
    
    jp .incv4



;moveright
.moveright:

  ld a,%00001000 ;right
  ld (Controls),a
  
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 35
    ret c
    
    ld a,(marioyoshi)
    and a
    jp z,.extraframe
    

.incv4:  
  
  xor a
  ld (Controls),a 
  
  
  inc (ix+v4)
  ld (ix+v7),0
  
  ret
  
.incv42x:  
  
  xor a
  ld (Controls),a 
  
  
  inc (ix+v4)
  inc (ix+v4)
  ld (ix+v7),0
  
  ret
    

.init:

;store yoshi data for later retrieval
    ld a,(marioyoshi)
    ld (noyoshi),a ;dissallow yoshi store the current value 
 ;   xor a
 ;   ld (marioyoshi),a

    ld a,1
    ld (noyoshi+1),a

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

    ;kill optionsmenu
    ld hl,pollpauze.gooptionsmenu
    ld a,$C9
    ld (hl),a

    ;kill controls
    ld hl,readcontrolsadress
    xor a ;write 3x nop
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    
    ;kill scorebar
    ld hl,scorebaradress
    xor a ;write 3x nop
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    
    ;set lineint to default
    xor a
    ld (lineintheight),a
    
    
    ;reset controls
  ;  xor a
    ld (Controls),a    
    
    inc (ix+v4)
    ret



  
  
  
  
  
  
;first byte is the direction, second byte is the time pixels per frame  
larryplatformmovementtable: db  1,34,4,93,2,10,3,93,2,3,4,93,2,4,4,30,1,21,4,50,8,200
  
;0 = reserved value for next action  
;1 = up
;2 = down
;3 = left
;4 = right
;5 = stand still
  ;8 = end of table
  
  
handlelarryplatform:

    ld a,(ix+v4)
    dec a
    jp z,.up
    dec a
    jp z,.down
    dec a
    jp z,.left
    dec a
    jp z,.right
    dec a
    jp z,.stop
    
    dec a
    dec a
    dec a
    jp z,.destroyobject
    dec a
    jp z,.fall
    
 ;determine action from here since value is 0
 
 
    ld e,(ix+v5) ;pointer
    ld d,0
    ld hl,larryplatformmovementtable
    
    add hl,de
    inc (ix+v5) ;set pointer 
    inc (ix+v5)

    ld a,(hl)
    ld (ix+v4),a ;set direction
    
    inc hl
    
    ld a,(hl)
    ld (ix+v7),a ;set timer
    
    

    ret

    
.fall:

    call addv2toybject

    jp checkobjecttilesfloor
    
    
    
.destroyobject:

    ld (ix+v1),0 ;make sure that the platform doesn' t move mario around
    ld (ix+v2),0

    
    
    ;sit and do nothing. Fall after some time to save sprites
    dec (ix+v7)
    ret nz
    
    
    inc (ix+v4)
    ld (ix+objectfp),0
    
    ret

    
.up:

    ld (ix+v1),0
    ld (ix+v2),-1

    call addv2toybject
    
    ld a,(framecounter) ;move in tiles instead of pixels
    and 7
    ret nz
    
    dec (ix+v7)
    ret nz
    
    ld (ix+v4),0
    
    
    ret
    


.down:

    ld (ix+v1),0
    ld (ix+v2),1

    call addv2toybject
    
    ld a,(framecounter) ;move in tiles instead of pixels
    and 7
    ret nz
    
    dec (ix+v7)
    ret nz
    
    ld (ix+v4),0
    
    
    ret


.left: 

    ld (ix+v1),-1
    ld (ix+v2),0

    call addv1toxbject
    
    ld a,(framecounter) ;move in tiles instead of pixels
    and 7
    ret nz
    
    dec (ix+v7)
    ret nz
    
    ld (ix+v4),0
    
    
    ret


.right:    

    ld (ix+v1),1
    ld (ix+v2),0

    call addv1toxbject
    
    ld a,(framecounter) ;move in tiles instead of pixels
    and 7
    ret nz
    
    dec (ix+v7)
    ret nz
    
    ld (ix+v4),0
    
    
    ret


.stop:    

    ld (ix+v1),0
    ld (ix+v2),0

;    call addv1toxbject
    
    ld a,(framecounter) ;move in tiles instead of pixels
    and 7
    ret nz
    
    dec (ix+v7)
    ret nz
    
    ld (ix+v4),0
    

    ret

  
  
;why not the blocks? Because there are too many sprites and the map is too big for the same tricks. Let mario suffer as he has made us suffer!!  
MovementPattern351:     ;special larry's castle moving platform 
  
  ld c,0
  call platforminteract
  
;so we activate the platform first en then we handle it by constantly reading two bytes from a movement table. This has been done before so nothing new here
    ld a,(ix+v3)
    and a
    jp nz,handlelarryplatform

    ld a,(standingonsprite) ;we get away with this because this is the only platform on the entire map
    dec a
    ret nz
    
    inc (ix+v3)
  
  
  
  ret
  
  
  
MovementPattern350:     ;spike and lava killer  
  
  
    call followmario


    ld de,0
    ld bc,17
    call objecttilecheckfromrom

    ;get mapadress
    cp 15
    call nc,.check2
    cp 175
    call nc,.check3       
    
    ret

.check3:

    cp 179
    ret nc
    
    jp .kill
    
    
.check2:

    cp 23
    ret nc
    
    
.kill:


  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a



    ret    
  
  
  
  
  
handleregion148:

    call .checkkill

    
    ld hl,(mariox)
    xor a
    ld de,1000
    sbc hl,de
    jp nc,.checkregion1
    

   
    
        jp .handleregion0


 ;kill upon spike contact. Very hacky way of solving but yeah I am really done with this shitgame       
        
.checkkill:


    ld de,0
    ld bc,0
    call objecttilecheckfromrom
    ;get mapadress
    cp 77
    call nc,.check2

    ld de,0
    ld bc,17
    call objecttilecheckfromrom

    ;get mapadress
    cp 81
    call nc,.check3    
    
    ld de,0
    ld bc,0
    call objecttilecheckfromrom
    
    ;get mapadress
    cp 85
    call nc,.check4      
    
    ret
    
.check4:

    cp 89
    ret nc
    
    jp .kill
    

.check3:

    cp 85
    ret nc
    
    jp .kill
    
    
.check2:

    cp 81
    ret nc
    
    
    
    

.kill:

 ;   ld a,(ix+v3)
 ;   and a
 ;   ret z ;no kill if motion has stopped

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a



    ret        
        
        
        

.reset:

;do this to prevent gaps from forming into the ground when entering region 1

    ld (ix+v3),0
    ld (ix+v7),0
    ld (ix+v4),0
    ret
    
    
.checkregion1:


    ld de,1500
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp c,.reset

    ld de,2430
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp nc,.checkregion2    
    
    

;    jp .handleregion1



 ;   ret
    
    
.handleregion1:    

    ld iy,wendyspikeadresstable2.region1 ;read the spike table and fetch neede info from there

    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.drop
    dec a
    jp z,.wait ;pillar is down
    dec a
    jp z,.moveup ;move back up
    
    
    
    ld (ix+v4),0


  
  ret

 
 
.checkregion2:

    ld de,3000
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp c,.reset




    ;ret  
 
.handleregion2:    
    
    
    
    
    ld iy,wendyspikeadresstable2.region2 ;read the spike table and fetch neede info from there
  
    
    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.drop
    dec a
    jp z,.wait ;pillar is down
    dec a
    jp z,.moveup ;move back up
    
    
    
    ld (ix+v4),0


  
  ret
  
    
.handleregion0:    
    
    
    
    
    ld iy,wendyspikeadresstable2 ;read the spike table and fetch neede info from there
  
    
    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.drop
    dec a
    jp z,.wait ;pillar is down
    dec a
    jp z,.moveup ;move back up
    
    
    
    ld (ix+v4),0


  
  ret


.moveup:



    dec (ix+v3)
    
    
    
.uploop:
    
    ld c,(iy+ymax) ;maximum depth
;    inc c
;    inc c
    
    ld a,(ix+v3)
    cp c
    jp nc,.skipupmove
    
    call movewendyspike

.skipupmove:     
    
    ;loop through all spikes in region1
    ld de,wendyspikeadresstablesize    
    add iy,de 
    
    ld a,(iy+region)
    cp 5
    jp nz,.uploop
    
    
    ld a,(ix+v3)
    and a
    jp z,incv4
    


    ret   


  
  
  
.drop:

   
    inc (ix+v3)
    inc (ix+v3)
    
    
.droploop: 


    ld c,(iy+ymax) ;maximum depth
;    inc c
;    inc c
    
    ld a,(ix+v3)
    cp c
    jp nc,.skipmove


    call movewendyspike

.skipmove:    
    
    ld de,wendyspikeadresstablesize
    add iy,de 
        
    
    
    ld a,(iy+region)
    cp 5
    jp nz,.droploop
    
 ;   add iy,de 
    
;determine on end
    
    ld c,(iy+ymax) ;maximum depth
    ld c,20 ;maximum depth    
    
    ld a,(ix+v3)
    cp c
    jp z,.incv4
    


    ret  
  
  
  
.wait:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c
    
    jp incv4



    ;ret
  

.incv4:


    ld a,1
    ld (tripscreen),a
    
    call Playsfx.explosion
    
    jp incv4
    
    ;ret
  
  
  
;all the wendy spikes. Handle them one by one each frame

;adress table has a few entries. the bkgadress, maximum movement depth, movement type 0 = downwards 1 = upwards

wendyspikeadresstable2: 

                       dw bgrmapaddr+5754 : db 14,0  ;region 0
                       dw bgrmapaddr+5764 : db 18,0
                       dw bgrmapaddr+5772 : db 20,0
                       dw bgrmapaddr+5790 : db 20,0
                       dw bgrmapaddr+5799 : db 16,0
                       dw bgrmapaddr+5808 : db 12,0
 ;                      dw bgrmapaddr+5764 : db 20,5 ;end of region


.region1:
                       dw bgrmapaddr+5916 : db 18,5  ;region 1
                       dw bgrmapaddr+5934 : db 18,0  
                       dw bgrmapaddr+5970 : db 18,0  
                       dw bgrmapaddr+5979 : db 18,0  
                       dw bgrmapaddr+5989 : db 18,0  
.region2:
                       dw bgrmapaddr+6100 : db 18,5 ;<==== end of region  
                       dw bgrmapaddr+6120 : db 18,0  
                       dw bgrmapaddr+6132 : db 18,0  
                       dw bgrmapaddr+6152 : db 18,0  
                       dw bgrmapaddr+6191 : db 18,0  
   ;                    dw bgrmapaddr+15540 : db 18,1  
                       dw bgrmapaddr+15540 : db 00,5  ;<==== end of region  


  
  
  
  
MovementPattern349:     ;minichuck (from scooping chuck)  
  
  
  call addv1toxbject
  call addv2toybject
  
  call checkobjecttilesfloor
  ret c
  
  
    ld hl,33
    ld (ix+movepat),l
    ld (ix+movepat+1),h  
  
  
  ret
  
  
  
  
MovementPattern348:     ;chuckball  
  
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump  
  
  
  call mariointeract
  jp c,.destroyobject
  
  ld a,(framecounter)
  and 3
  call z,.animate
  
  
  call addv1toxbject
  call addv2toybject

  
  ld a,(ix+v4)
  and a
  jp nz,.bounce
  
  
  call checkobjecttilesfloor
  jp nc,.setbounce
  
  ret


.destroyobject:

    ld a,(mariospinjump)
    dec a
    jp z,destroyobjectwithcloud

    jp otherobjectdeathjump


  ;  ret
  
  
  
.animate:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

    ld a,(ix+v3)
    inc (ix+v3)
    dec a
    jp z,.step1
    dec a
    jp z,.step2
    dec a
    jp z,.step3
    
.step0:

  ld    hl,coloraddresschuckball  ;color address
  ld    bc,characteraddresschuckball   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 


.step1: 

  ld    hl,coloraddresschuckball1  ;color address
  ld    bc,characteraddresschuckball1   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 


.step2:  

  ld    hl,coloraddresschuckball2  ;color address
  ld    bc,characteraddresschuckball2   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 

.step3:    
  
  ld (ix+v3),0
  
  ld    hl,coloraddresschuckball3  ;color address
  ld    bc,characteraddresschuckball3   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 
  
  
  
  
.setbounce:

    inc (ix+v4)
    ld (ix+v7),0

    ret

    
.bounce:

    
    ld a,(ix+v7)
    inc (ix+v7)
    ld e,a
    ld d,0
    ld hl,.bouncetable
    add hl,de
    ld a,(hl)
    and a
    jp z,.endbounce
    
    ld (ix+v2),a



    ret
    
.bouncetable:

    db -8,-6,-5,-4,-4,-3,-3,-2,-2,-1,-1,-1,0

.endbounce:

    ld (ix+objectfp),0
    ld (ix+v2),0
    ld (ix+v4),0
    ld (ix+v7),0
    ret


  
  
MovementPattern347:     ;scooping chuck  
  
;tricky little fella. He is not so difficult to code but we need sprites so I made him a an 8 sprited character. But he spawns those damn balls and I don't want to use another infolist entry for this we are running out of romspace.  
 
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump 
 
  
  call mariointeract
  jp c,.setexplode
  
  ;states:
  ;0 wating
  ;1 looking to side
  ;2 look back
  ;3 scoop 1
  ;4 scoop 2
  ;5 scoop 3 and spawn ball
  ;6 explode and split into mini chucks
  
  
  ld a,(ix+v4)
  dec a
  jp z,.look
  dec a
  jp z,.lookback
  dec a
  jp z,.scoop1
  dec a
  jp z,.scoop2
  dec a
  jp z,.scoop3
  dec a
  jp z,.explode
  
  
  ld a,(ix+v7)
  inc (ix+v7)
  cp 30
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck2  ;color address
  ld    bc,characteraddressscoopingchuck2   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  jp .incv4
  

 ; ret


.spawnchuckball:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  dec hl
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

 
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
;  ld hl,338
  ld    (iy+5),1                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
   
   
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      

    
    ld hl,348
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+v1),-1
    ld (ix+v4),1
    ld (ix+v7),0
    ld (ix+deadly),1
    

  ld    hl,coloraddresschuckball  ;color address
  ld    bc,characteraddresschuckball   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  call   setcharcolandoffset     
   
   

        pop ix



    ret
 
 
 
.look:

    
  ld a,(ix+v7)
  inc (ix+v7)
  cp 30
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck1  ;color address
  ld    bc,characteraddressscoopingchuck1   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  jp .incv4



;ret



.lookback:

  ld a,(ix+v7)
  inc (ix+v7)
  cp 10
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck3  ;color address
  ld    bc,characteraddressscoopingchuck3   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  jp .incv4



;ret


.scoop1:



  ld a,(ix+v7)
  inc (ix+v7)
  cp 10
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck  ;color address
  ld    bc,characteraddressscoopingchuck   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  jp .incv4



;ret


.scoop2:



  ld a,(ix+v7)
  inc (ix+v7)
  cp 10
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck4  ;color address
  ld    bc,characteraddressscoopingchuck4   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  jp .incv4



;ret


.scoop3:




  ld a,(ix+v7)
  inc (ix+v7)
  and a
  jp z,.spawnchuckball
  cp 20
  ret c
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      


  ld    hl,coloraddressscoopingchuck1  ;color address
  ld    bc,characteraddressscoopingchuck1   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call   setcharcolandoffset  
  
  
  
  
  ld (ix+v4),0 ;reset state
  

    ret


.setexplode:

    ld a,(ix+v4)
    cp 5
    jp z,.explode ;if allready exploding ignore


    ld (ix+v4),5

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

 
    push hl
    push de
 
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
;  ld hl,338
  ld    (iy+5),237                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
   
   
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      

    
    ld hl,349
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+v1),-2
    

        pop ix
    
        pop de
        pop hl
    
    
    push ix
    
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
;  ld hl,338
  ld    (iy+5),237                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
   
   
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      

    
    ld hl,349
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+v1),2  
    
    
    pop ix
    
.explode:



    jp destroyobjectwithcloud

   ; ret


.incv4

    inc (ix+v4)
    ld (ix+v7),0



  ret
  
  
  
MovementPattern346:     ;lavaplateau  
  
  
  ld c,0
  call platforminteract ;make this object a platform
  
  ld a,(ix+v4)
  dec a
  jp z,.move
  
  call mariointeract
  ret nc
  
  ld a,(standingonsprite)
  and a
  ret z ;mario must really be standing on this platform or else activation is not allowed
  
  inc (ix+v4) ;start this thing up
    ld (ix+v2),1  
  
  ret
 
  
.move:

    call addv2toybject

    
  ld a,(ix+v7)
  inc (ix+v7)
  cp 17
  jp nc,.destroyobject
  
  inc a
  ld c,a
  push bc
  
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  
  pop bc
  push bc
  
  ld a,16
  sub c
  call hidemariosprite

  
  
  
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  pop bc
  
  ld a,16
  sub c
  call hidemariosprite  
  
  
    ret

    
.destroyobject:

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite  
  
  
    ret
    
    
  

MovementPattern345:     ;countplateau  
  
  ld c,0
  call platforminteract ;make this object a platform
  
  ld a,(ix+v4)
  dec a
  jp z,.move
  dec a
  jp z,.destroy
  
  call mariointeract
  ret nc
  
  ld a,(standingonsprite)
  and a
  ret z ;mario must really be standing on this platform or else activation is not allowed
  
  inc (ix+v4) ;start this thing up
    ld (ix+v1),2  
  
  ret
 
 
.destroy:

    call addv1toxbject
    call addv2toybject

    ld a,(ix+v7)
    inc (ix+v7)
    cp 20 
    ret c    
    
    
    ;perform drop
    
.drop:



  ld e,(ix+v5)
  ld d,0
  ld hl,.droptable
  add hl,de
  ld a,(hl)
  cp 9
  ret z

  ld (ix+v2),a

  inc (ix+v5)

  ret

.droptable: db 0,1,0,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,2,2,2,3,2,3,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,8,9
 
 
 
.move:


    call addv1toxbject
    
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 30 ;1 second unless some idiot forces 50 hz
    ret c

    ld (ix+v7),0 ;reset counter
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)      
    
    
    
    ;animate each second update sprite
    dec (ix+hitpoints)
    ld a,(ix+hitpoints)
    and a
    jp z,.step0
    dec a
    jp z,.step1
    dec a
    jp z,.step2
    dec a
    jp z,.step3
    dec a
    jp z,.step4
    
    
    ret

    
.step0:

    inc (ix+v4) ;set to handle next step


  ld    hl,coloraddresscountplateau  ;color address
  ld    bc,characteraddresscountplateau   ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset  

.step1:  

  ld    hl,coloraddresscountplateau1  ;color address
  ld    bc,characteraddresscountplateau1   ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset 

.step2:    

  ld    hl,coloraddresscountplateau2  ;color address
  ld    bc,characteraddresscountplateau2   ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset 
  
.step3:    

  ld    hl,coloraddresscountplateau3  ;color address
  ld    bc,characteraddresscountplateau3   ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset 

.step4:    

  ld    hl,coloraddresscountplateau4  ;color address
  ld    bc,characteraddresscountplateau4   ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset 

    
    ;ret
  
  
  
  
;use dino rhino code and adapt to this guy. Problem is that mario can stand on this guy so special detection code is needed  
MovementPattern344:     ;megamole  
  
  ld c,0
  call platforminteract
  
  

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  call c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   7                       ;this sprite animates once every 3 frames
  call  z,.animate

    ld a,(ix+v4)
    and a
    jp nz,.jump
  
  
  call objectinteract
  cp 4
;  push af
  jp z,.objectwallfound
;  pop af
  cp 2
  jp z,otherobjectdeathjump
  cp 1
;  push af
  jp z,.objectwallfound
;  pop af


  ld (ix+v7),0 ;set timer

  ld bc,-0 ;check above object and turn if the wall is too high
  ld de,-16
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfound  
  
  
  ld bc,-0 ;check above object and turn if the wall is too high
  ld de,8
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfoundright

.cheaphack:  
  
  
  call checkobjecttilescheckwall
  jp    c,.setjump            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  
  

  ret

  
.setjump:

    ld a,(currentlevel)
    cp 138
    jp z,.wallfound


    inc (ix+v4)
    ld (ix+hitpoints),0 ;reset pointer

    ret
    
.jump:

    ld hl,.jumptable
    ld e,(ix+hitpoints)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.endjump
    
    ld (ix+v2),a
    
    inc (ix+hitpoints)

    ret

    
.endjump:

    ld (ix+v4),0

    ret
    
    
;.jumptable: db -4,-3,-3,-2,-2,-2,-1,-1,-1,-1,0    
.jumptable: db -2,-1,-2,-1,-2,-1,-1,-1,-1,-1,0    
    

  
.handleright:

    ld a,(ix+v1)
    bit 7,a
    ret z
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a

    



    ret
  
  

.objectwallfound:

    ld a,(iy+deadly)
    cp 4
    ret z ;muncher or other extreme deadlies


  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  jp c,.skipchecks
  call checksheerobjecttile ;check for sheer tiles
  jp c,.skipchecks ;sheertile found

  call  checkobjecttilesfloor
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

.skipchecks:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 8
  ret nc ;ignore all objects


.wallfoundright:

    ld a,(currentlevel) ;fuck that this hack will add exception to this map only
    cp 119
    jp z,.cheaphack

    ld a,(ix+v1)
    bit 7,a
    ret nz ;moving left ignore this interaction
  
  
.wallfound:

 ;   call .setjump
;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressmegamole  ;color address
  ld    bc,characteraddressmegamole   ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressmegamole1  ;color address
  ld    bc,characteraddressmegamole1   ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position2:
  ld    hl,coloraddressmegamole2  ;color address
  ld    bc,characteraddressmegamole2   ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position3:
  ld    hl,coloraddressmegamole3  ;color address
  ld    bc,characteraddressmegamole3   ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.nextframe:

    ld a,(standingonsprite)
    and a
    ret nz

    ld (ix+deadly),2
    
    xor a
    ld (movedownsheertile),a
    
    call killmario

    ld (ix+deadly),3

    inc sp
    inc sp
    
  ret
  
  
  
  
  

MovementPattern343:     ;ghost many  
  
 
    ld a,(ix+hitpoints)
    and a
    jp z,.init ;init code to prevent preactivation of the ghost type
 
 
  ld a,(ix+v4)
  dec a
  jp z,handlemanyghosts
  
  
  
  call followmario
  
  ld hl,185*8
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,destroyobject ;remove from map at this point
  
  
  
  ld a,(ix+v7)
  inc (ix+v7)
  cp 150
  ret c
  
  
  ld (ix+v7),0

  
  ld b,9
  
.spawnloop:  
  push bc
  
 ;spawn many ghosts loop this shit 
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8


  
    push hl
    call makerandomnumber
    ld d,0
    ld e,a  
    push de
    call makerandomnumber
    cp 5
    pop de
    pop hl
    call nc,.add
    call c,.substract
    
  
  push hl
  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) 
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

 
    push hl
    call makerandomnumber
    ld d,0
    ld e,a  
    push de
    call makerandomnumber
    cp 5
    pop de
    pop hl
    call nc,.add
    call c,.substract
 
 
 
    ex de,hl
    pop hl
 
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
;  ld hl,338
  ld    (iy+5),1                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
  
  ld hl,343
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v7),0
  inc (ix+v4)
  ld (ix+deadly),2

  
  ld bc,characteraddressghost1left6
  ld hl,coloraddressghost1left6
  push hl
  push bc
  call makerandomnumber
  pop bc
  pop hl
  cp 5
  call c,.settype2

  
  push hl
  push bc
    ld de,0
    ld bc,0
    call objecttilecheckfromrom 

    cp 191
    call nc,destroyobject ;object receives no sprite and will be destroyed  
  pop bc
  pop hl
  
  
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call   setcharcolandoffset 
  
  
  pop ix 
    
  pop bc  
  
    ld a,b
    dec b
    and a
    jp nz,.spawnloop
    
  
  ret

  
  
.settype2:


  ld bc,characteraddressghost1left5
  ld hl,coloraddressghost1left5

  ld (ix+v5),1 ;other type
  

    ret
  
  
.add:

    push af
    add hl,de
    pop af
    
    inc hl
    inc hl
;    inc hl
;    inc hl
    
    
    ret
  
  
.substract:

    xor a
    sbc hl,de
    
    dec hl
    dec hl
;    dec hl
;    dec hl
    
    ret

    
    
.init:


    ld a,(ix+v7)
    inc (ix+v7)
    cp 5
    ret c
    
    
    ld (ix+v7),0
    inc (ix+hitpoints)




    ret
    
    
  

 ;so basicly we spawn alot of ghosts on random positions and they dissapear by themselves. 
handlemanyghosts:




    call mariointeract

    ld a,(ix+v7)
    inc (ix+v7)
    and a
    jp z,.init ;check for interaction and eliminate if so
    cp 5
    jp z,.change ;nice spawn animation sloppy but effective
    cp 10
    jp z,.changefull
    cp 90
    jp z,.change
    cp 95
    jp z,.changegone
    cp 100
    ret c
    
    jp destroyobject



    ret


    
.changegone:    
    

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld bc,characteraddressghost1left6
  ld hl,coloraddressghost1left6
  ld a,(ix+v5)
  dec a
  call z,.settype4
  
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp   setcharcolandoffset   
  
  ;ret    
    
    
    
.changefull:    
    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld bc,characteraddressghost1left2
  ld hl,coloraddressghost1left2
  ld a,(ix+v5)
  dec a
  call z,.settype3
  
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp   setcharcolandoffset   
  
  ;ret
    
    
    
.change:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld bc,characteraddressghost1left4
  ld hl,coloraddressghost1left4
  ld a,(ix+v5)
  dec a
  call z,.settype2
  
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp   setcharcolandoffset   
  
  ;ret

  
.settype2:


  ld bc,characteraddressghost1left3
  ld hl,coloraddressghost1left3
  
  ret
  
.settype3:


  ld bc,characteraddressghost1left1
  ld hl,coloraddressghost1left1
  
  ret  
  
.settype4:


  ld bc,characteraddressghost1left5
  ld hl,coloraddressghost1left5
  
  ret  

  
.init:

  call objectinteract ;check for other ghost nearby and destroy if so
  
  ret nc ;nothing found
  
  ld l,(iy+movepat)
  ld h,(iy+movepat+1)
  
  ld de,343 ;movepat of this thing
  xor a
  sbc hl,de

  ret nz
  
  jp destroyobject
  

    ;ret
  
  
  
MovementPattern342:     ;watercannon  
  

  ld a,(ix+v7)
  inc (ix+v7)
  cp 100
  ret c

  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
  push hl
  call getmarioleftright
  pop hl
  call c,.setright
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc e
 ; dec e
 ; dec e

 
    push hl
    push de
 
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),247                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
  
  
  pop ix

    pop de
    pop hl
    
    push ix
    
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),27                ;set object number (237 mini charging chuck)
  ld    (iy+6),0                ;set object number (237 mini charging chuck)

  call  activateobject
   
   ld (ix+movepat),22
   
    
    pop ix
  
  ld (ix+v7),0
  
  ret
  

  
  
.setright:



    ld de,4
    add hl,de
    ret



  
  
  
 ; ret
  
  

wendybosstable: 
;the row number is the type
;              sprcharadress,                 sprcoladress,             sprcharadress2,               sprcoladress2,        torsoheight
dw      characteraddresswendyfront1,  coloraddresswendyfront1,  characteraddresswendyfront2,  coloraddresswendyfront2     db    26-1  
dw      characteraddresswendyfront3,  coloraddresswendyfront3,  characteraddresswendyfront4,  coloraddresswendyfront4     db    26-1  
dw      characteraddresswendyleft1,   coloraddresswendyleft1,   characteraddresswendyleft2,   coloraddresswendyleft2      db    29-2  
dw      characteraddresswendyright1,  coloraddresswendyright1,  characteraddresswendyright2,  coloraddresswendyright2     db    29-2  
dw      characteraddresswendydoll1,   coloraddresswendydoll1,   characteraddresswendydoll1,   coloraddresswendydoll1      db    26  

;dummy labels replace with real sprites later
/*characteraddresswendyfront1:
coloraddresswendyfront1:
characteraddresswendyfront2:
coloraddresswendyfront2:
characteraddresswendyfront3:
coloraddresswendyfront3:
characteraddresswendyfront4:
coloraddresswendyfront4:
characteraddresswendyleft1:
coloraddresswendyleft1:
characteraddresswendyleft2:
coloraddresswendyleft2:
characteraddresswendyright1:
coloraddresswendyright1:
characteraddresswendyright2:
coloraddresswendyright2:
characteraddresswendydoll1:
coloraddresswendydoll1:
characteraddresswendydoll2:
coloraddresswendydoll2:
characteraddresswendydoll3:
coloraddresswendydoll3:




coloraddresswendydead:
characteraddresswendydead:
characteraddresswendypissed2:
coloraddresswendypissed2:
characteraddresswendypissed1:
coloraddresswendypissed1:*/



lenghtwendybosstable: equ 9

;spritechr1:        equ 0
;spriteclr1:        equ 2
;spritechr2:        equ 4
;spriteclr2:        equ 6
;torsoheight:       equ 8


MovementPattern341:     ;wendy (boss)

  call .fetchiy


  call mariointeract
  jp c,.becomepissed

  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.riseup ;2
  dec a
  jp z,.wait ;3
  dec a
  jp z,.godown ;4
  dec a
  jp z,.ispissed ;5
  dec a
  jp z,.godownpissed ;6
  dec a
  jp z,.prepdie ;7
  dec a
  jp z,.die ;8  
  dec a
  jp z,.setendlevel ;9
  dec a
  jp z,.endlevel ;10


  ld a,(pillaranimationstep)
  and a
  ret nz

  inc (ix+v7)
  ld a,(ix+v7)
  cp 40
  ret c

  call .incv4
  
 
;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call    setcharcolandoffset  


.fullhide:

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite 

  ret


.setfadeout:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  ret

.endlevel:



  ld l,(ix+v5)
  ld h,(ix+v5+1)
  dec hl
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld de,110
  xor a
  push hl
  sbc hl,de
  pop hl
  jp z,.setfadeout
  ld a,l
  or h
  ret nz


  
  ld a,1
  ld (levelend?),a



  ret



.setendlevel:
  
  ;kill the flame. DANGER! we asume the flame is spawned first so this is kinda hackish and will only work on this map.
 ; ld iy,Objectlist
 ; ld (iy+yobject),0
 ; ld (iy+yobject+1),0
  call killallenemieswendy
  
  
  ld hl,300
  ld (ix+v5),l ;use this as timer.... :(
  ld (ix+v5+1),h

  xor a
  ld (time),a

  ld a,2
  ld (playwinmusic),a

  jp .incv4

;  ret


.falldown:

  ld a,(ix+v7)
  cp 22
  jp nc,.incv4


  ld (ix+v2),8
  call addv2toybject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v7)

  ret


.die:

  ld a,(ix+v7)
  cp 16
  jp nc,.falldown

  ld (ix+v2),1
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

;  ld a,(ix+v7)
;  inc a
;  inc a

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,4*32
  call unhideobjectspriteupdown


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 

  push ix
  pop iy

;  ld a,(ix+v7)
;  inc a
;  inc a

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,6*32
  call unhideobjectspriteupdown

  inc (ix+v7)


  ret


.prepdie:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret c

  call Playsfx.bossdies

  call .incv4

  ;make sure to init the color sprite data at first start during init  
  ld hl,coloraddresswendydead
  ld bc,characteraddresswendydead
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset



 ; ret



.godownpissed:


  ld a,(ix+v7)
  cp 16-5
  jp nc,.godownpissed2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret



.godownpissed2:

  ld a,(ix+v7)
  cp 32-6
  jp nc,.restartafterhit
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

.restartafterhit:

  ld a,(ix+objectrelease)
  and a
  jp nz,.restart


  dec (ix+hitpoints)
  jp z,.wendydies
  
  jp .restart
  

.wendydies:

  ld a,1
  ld (pillaranimationstep),a ;use this to kill of the puppet

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

;write dead wendy
  ld hl,coloraddresswendydead
  ld bc,emptysprite95
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call    setcharcolandoffset  

;set right above the underside of the tube
  ld a,20*8
  ld (ix+yobject),a
  ld (ix+yobject+1),0
  
  jp .incv4


;  ret


.ispissed:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  jp nc,.incv4


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

  ld a,(ix+objectrelease)
  and a
  jp nz,.puppet


  ld a,(framecounter)
  and 3
  jp z,.ispissed2

  ld bc,characteraddresswendypissed2
  ld hl,coloraddresswendypissed2
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset


;  ret

.ispissed2:

  ld bc,characteraddresswendypissed1
  ld hl,coloraddresswendypissed1
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset
  
  ;ret


.puppet:

  ld a,(ix+v7)
  cp 3
  ret c


  ld bc,characteraddresswendydoll3
  ld hl,coloraddresswendydoll3
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset
  
  ;ret



.becomepissed:

  ld a,(ix+v4)
  cp 3
;  ret nz ;only when fully up
  jp nz,Playsfx.chuckstomp

  ld a,(iy+torsoheight)
  cp 27
  call nc,.movedown 

  ld a,(ix+objectrelease)
  and a
  jp nz,.crumblepuppet

;  push iy  
  call Playsfx.chuckauw
;  pop iy


  ld (ix+v4),5
  ld (ix+v7),0
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
   
  ld bc,characteraddresswendypissed1
  ld hl,coloraddresswendypissed1
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 

;  ret


.crumblepuppet:

  call Playsfx.chuckstomp ;BUG: some wierd stuff happens here. not good

  ld (ix+v4),5
  ld (ix+v7),0
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
   
  ld bc,characteraddresswendydoll2
  ld hl,coloraddresswendydoll2
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp   setcharcolandoffset 

;  ret




.movedown:



  ld (ix+v2),3
  call addv2toybject
  


  ret


;sink back in
.godown:

  ld a,(iy+torsoheight)
  cp 27
  jp nc,.godownother ;other wait type


  ld a,(ix+v7)
  cp 16-5
  jp nc,.godown2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret

.setzero:

  xor a
  ret


.godown2:

  ld (ix+deadly),0

  ld a,(ix+v7)
  cp 32-6
  jp nc,.restart
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

.godownother:


  ld a,(ix+v7)
  cp 14
  jp nc,.godownother2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-4
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-4
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret

.godownother2:

  ld (ix+deadly),0

  ld a,(ix+v7)
  cp 32-3
  jp nc,.restart
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-4
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-4
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

  

.restart:

  ld (ix+v4),0
  ld (ix+v7),0

  ret


;do nothing do some action then get the fuck out.
.wait:

  ld a,(iy+torsoheight)
  cp 27
  jp nc,.wait2 ;other wait type

  inc (ix+v7)
  ld a,(ix+v7)
  cp 20
  jp nc,.stopwait1


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  jp z,.one
  and 3
  dec a
  ret z
  dec a
  ret z
  dec a
  ret z

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.one:

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr2) ;color data adress
  ld h,(iy+spriteclr2+1)
  ld c,(iy+spritechr2)
  ld b,(iy+spritechr2+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset


;  ret


.stopwait1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call    setcharcolandoffset
  
  jp .incv4
  
  ;ret


.wait2:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 20
  ret c
  cp 40
  jp nc,.incv4

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr2) ;color data adress
  ld h,(iy+spriteclr2+1)
  ld c,(iy+spritechr2)
  ld b,(iy+spritechr2+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset


 ; ret



.fetchiy:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ret


.wendyrandompositiontable:

  ;  x,  y
  db 3*8,  18*8
  db 7*8,  18*8
  db 11*8, 18*8
  db 15*8, 18*8
  db 19*8, 18*8
  db 23*8, 18*8
  db 27*8, 18*8
  db 11*8, 18*8 ;spare row

.init:

  ld (ix+deadly),0 ;to make it a little bit more friendly

 ;pick a random number and decide at which tube wendy comes up. furhermore spawn the puppet wendy and save the iy info somehwere. There are enough unused free positions in ram for that
  call makerandomnumber
  and 7
  inc a
  ld b,a

  ld hl,.wendyrandompositiontable-2
  ld de,2
.getwendypositiondata:
  add hl,de
  djnz .getwendypositiondata

  ld a,(hl)
  ld (ix+xobject),a
  ld (ix+xobject+1),0

  inc hl
  
  ld a,(hl)
  dec a
  ld (ix+yobject),a
  ld (ix+yobject+1),0  
  
  

  call makerandomnumber
  and 3
  inc a
  ld b,a

  ld a,(ix+objectrelease)
  and a
  call nz,.initpuppet

;make sure that a random number is generated and sprites are picked from a table   
;write sprite data in place and directly hide it
 ; ld b,3 ;"random" generated number
  ld iy,wendybosstable-lenghtwendybosstable
  ld de,lenghtwendybosstable
.getwendydata:
  add iy,de
  djnz .getwendydata
  
  push iy
  pop hl
  ld (ix+v5),l ;store data table
  ld (ix+v5+1),h

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  inc (ix+v4)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
;  ld c,(iy+spritechr1)
;  ld b,(iy+spritechr1+1) ;character data adress  
  ld bc,emptysprite95
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock7
  call    setcharcolandoffset


  ld a,(ix+v3)
  and a
  ret nz
 
  inc (ix+v3) ;only do this once


  xor a
  ld (pillaranimationstep),a

;spawn the puppet
  ld hl,80
  ld de,80+16

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object   
  ld hl,336 
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)


  call  activateobject
  ld (ix+objectrelease),1
  ld (ix+v3),1

  pop ix


  ret


.initpuppet:

  ld b,5

  ret


.incv4:
  
  inc (ix+v4)
  ld (ix+v7),0
  ret


.riseup:

  ld a,(ix+v7)
  cp 16
  jp nc,.riseup2

  ld (ix+v2),-1 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  inc a
  inc a
  ld bc,0*32
  call unhideobjectsprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  inc a
  inc a
  ld bc,2*32
  call unhideobjectsprite

  inc (ix+v7)

  ret

.riseup2:

  ld (ix+deadly),1 ;now we become pissed

  call .fetchiy
  ld c,(iy+torsoheight)
  ld a,(ix+v7)
  cp c
  jp nc,.incv4


  ld (ix+v2),-1 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  sub a,16  
  inc a
  inc a
  ld bc,4*32
  call unhideobjectsprite

  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  sub a,16
  inc a
  inc a
  ld bc,6*32
  call unhideobjectsprite

  inc (ix+v7)

  ret


  
  
  
  
  
MovementPattern338:     ;wendy's castle small moving blocks

    ld c,0
    call platforminteract

    
    ;4 states on this one
    ;0 = wait
    ;1 = move left
    ;2 = wait
    ;3 = move right
 
    
    
    ld a,(ix+v4)
    dec a
    jp z,.moveleft
    dec a
    jp z,.wait
    dec a
    jp z,.moveright
    

.wait:    
    
    ld (ix+v1),0
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 50
    ret c
    
    
    inc (ix+v4)
    ld (ix+v7),0
    
    ld a,(ix+v4)
    cp 5
    ret nz
    
    ld (ix+v4),0 ;done so reset loop
;    ld (ix+v7),0

    ret  
  

.moveleft:

    ld (ix+v1),-1

    jp .move
    
.moveright:

    ld (ix+v1),1    

.move:    
    
    call addv1toxbject
    
    ld a,(ix+v7)
    inc (ix+v7)
    cp 104-32
    ret c
    
    inc (ix+v4)
    ld (ix+v7),0
    ret
  
  
  
  
  
MovementPattern339:     ;wendyfireball  
  
    call mariointeract
  

  
    ld a,(ix+v4)
    and a
    jp z,.init  
  
  
    ld a,(framecounter)
    and 3
    call z,.animate
    

    call addv1toxbject
    call addv2toybject
    
    ;move around enum
    
    ld a,(ix+v4)
    dec a
    jp z,.goleft
    dec a
    jp z,.godown
    dec a
    jp z,.godown16
    dec a
    jp z,.goright
    dec a
    jp z,.goright16
    dec a
    jp z,.goup
    dec a
    jp z,.goup16    
    dec a
    jp z,.goleft16
    
  
  
    ret  

    
.goleft:

    ld de,8
    ld bc,24
    call objecttilecheckfromrom  ;check floor

 ;   and a
 ;   
 ;   ret nz
  cp 192
  ret nc
    
    inc (ix+v4)
;    ld (ix+v7),0 ;reset counter
    ld (ix+v1),0 ;next motion
    ld (ix+v2),2 ;next motion
    
    
    
    ret
    
    
    
.godown:

    ld de,16
    ld bc,17
    call objecttilecheckfromrom  ;check floor

 ;   and a
 ;   
 ;   ret nz
  cp 192
  ret nc  
    
    inc (ix+v4) 
    
    ret

.godown16:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 8
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),2 ;next motion
    ld (ix+v2),0 ;next motion


    ret
    
    
    
    
.goright:


    ld de,8
    ld bc,-8
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
  cp 192
  ret nc
    
    
    
    inc (ix+v4)  
    
    ret

    
.goright16:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 6
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),0 ;next motion
    ld (ix+v2),-2 ;next motion


    ret    
    
    
.goup:


    ld de,-17
    ld bc,-8
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
     cp 192
     ret c 
    
    inc (ix+v4)   
    
    ret
    
.goup16:


    ld a,(ix+v7)
    inc (ix+v7)
  ;  cp 11
    cp 15
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),-2 ;next motion
    ld (ix+v2),0 ;next motion


    ret

    ;dirty hack to make it work
    
.goleft16:    
    

    ld a,(ix+v7)
    inc (ix+v7)
    cp 8
    ret nz
    

    ld (ix+v4),1
    ld (ix+v7),0 ;reset counter

    ret    
    
    
    
.init:
 
 
    ;fall down until a solid tile is found so that we can lock onto the wall this makes placing the object alot easier for the mapeditor
 
    call addv2toybject
 
 
    ld de,0
    ld bc,24
    call objecttilecheckfromrom      
    
 ;   and a
 ;   
 ;   ret z
    cp 192
    ret c
  
  
    ld (ix+v2),0 ;stop falling
    
    inc (ix+v4) ;stop check and start main code
    ld (ix+v1),-2 ;we go left
    
    
    ret    
    

  
.animate:
 
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddresswendyfireball  ;color address
  ld    bc,characteraddresswendyfireball  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  
  

.position1:
  ld    hl,coloraddresswendyfireball1  ;color address
  ld    bc,characteraddresswendyfireball1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  

  ret     
  
  
  
  
MovementPattern340:     ;wendyfireballxl  
  
    call mariointeract
 
    ld a,(ix+v4)
    and a
    jp z,.init

    
    ld a,(framecounter)
    and 3
    call z,.animate
    
    
;object has fallen into place now move it around
    ;states are as follow
    ;0 = fall to nearest floor (only for init code)
    ;1 = go left
    ;2 = go left (no tilecheck)
    ;3 = go go down
    ;4 = go go down16
    ;5 = go right
    ;6 = go right16
    ;7 = go up
    ;8 = go up16 ;end of loop

    call addv1toxbject
    call addv2toybject
    
 
    ;enum
    ld a,(ix+v4)
    dec a ;1
    jp z,.goleft
    dec a ;2
    jp z,.goleft32
    dec a ;3
    jp z,.godown
    dec a ;4
    jp z,.godown16
    dec a ;5
    jp z,.goright
    dec a ;6
    jp z,.goright16
    dec a ;7
    jp z,.goup
    dec a ;8
    jp z,.goup16


  
    ret

    
.goleft:    


    ld de,0
    ld bc,32
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
   cp 192
   ret nc
   
   
    
    inc (ix+v4)

    ret

.goleft32:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 16
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),0 ;next motion
    ld (ix+v2),1 ;next motion


    ret
    
    
    
.goright:    

    ld de,24
    ld bc,0
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
   cp 192
   ret nc
   
    
    
    inc (ix+v4)  
    
    
    ret


.goright16:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 20
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),0 ;next motion
    ld (ix+v2),-1 ;next motion


    ret    
    
    


.godown:


   
    ld de,24
    ld bc,32
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
   cp 192
   ret nc
   
    
    
    inc (ix+v4)   

    ret

.godown16:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 24
    ret nz
    

    inc (ix+v4)
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),1 ;next motion
    ld (ix+v2),0 ;next motion


    ret
    
    
    
.goup:    
    
    ld de,0
    ld bc,0
    call objecttilecheckfromrom  ;check floor

  ;  and a
  ;  
  ;  ret nz
   cp 192
   ret nc
   
    
    
    inc (ix+v4)   
 


    ret
 
.goup16:

    ld a,(ix+v7)
    inc (ix+v7)
    cp 24
    ret nz
    

    ld (ix+v4),1 ;restart loop
    ld (ix+v7),0 ;reset counter
    ld (ix+v1),-1 ;next motion
    ld (ix+v2),0 ;next motion


    ret 
 
 
.init:
 
 
    ;fall down until a solid tile is found so that we can lock onto the wall this makes placing the object alot easier for the mapeditor
 
    call addv2toybject
 
 
    ld de,0
    ld bc,32
    call objecttilecheckfromrom      
    
  ;  and a
  ;  
  ;  ret nz
   cp 192
   ret c
   
    
    ld (ix+v2),0 ;stop falling
    
    inc (ix+v4) ;stop check and start main code
    ld (ix+v1),-1 ;we go left
    
    
    ret
    
    
    
.animate:
 
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


    ld a,(ix+v4)
    cp 5
    jp nc,.positionright
    

.positionleft:


    ld a,(ix+v2)
    and a
    jp nz,.position1 ;down left
    
    
    
    
;up 1 and 2 
.position0:


    ld    a,(ix+v3)               ;v3 is used as animation variable
    xor   1
    ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position01

  ld    hl,coloraddresswendyfireballxl  ;color address
  ld    bc,characteraddresswendyfireballxl  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
  
  

.position01:
  ld    hl,coloraddresswendyfireballxl1  ;color address
  ld    bc,characteraddresswendyfireballxl1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset  
  

  
;down 1 and 2 
.position1:


    ld    a,(ix+v3)               ;v3 is used as animation variable
    xor   1
    ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position11

  ld    hl,coloraddresswendyfireballxl2  ;color address
  ld    bc,characteraddresswendyfireballxl2  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
  
  

.position11:
  ld    hl,coloraddresswendyfireballxl3  ;color address
  ld    bc,characteraddresswendyfireballxl3  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset    
  
  
  
  
.positionright:  
  
  


    ld a,(ix+v2)
    and a
    jp nz,.position3 ;down right
    
    
    
    
;up 1 and 2 
.position2:


    ld    a,(ix+v3)               ;v3 is used as animation variable
    xor   1
    ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position21

  ld    hl,coloraddresswendyfireballxlr  ;color address
  ld    bc,characteraddresswendyfireballxlr  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
  
  

.position21:
  ld    hl,coloraddresswendyfireballxlr1  ;color address
  ld    bc,characteraddresswendyfireballxlr1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset  
  

  
;down 1 and 2 
.position3:


    ld    a,(ix+v3)               ;v3 is used as animation variable
    xor   1
    ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position31

  ld    hl,coloraddresswendyfireballxlr2  ;color address
  ld    bc,characteraddresswendyfireballxlr2  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
  
  

.position31:
  ld    hl,coloraddresswendyfireballxlr3  ;color address
  ld    bc,characteraddresswendyfireballxlr3  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset  
  
  
  
  ;ret
  
    
    
    
    
  

  


;IN a = tilenumber IN = HL tilegfx adress DE = Tilegfx color address Changes all registers including the alternate ones
;b = in case of a loop
updatewendyblocks:

  push bc
  exx 
  pop bc ;get bc in exx

  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
 


  
  ld a,(framecounter)
  and 1
  jp z,.doshit
  
  ld de,$4000
  add hl,de
 ; jp .doshit



 ;ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl


  ld c,$98
  push bc
.write1:  
  
  push bc
  call outix8 ;outi modifies bc
  pop bc
  djnz .write1
  pop bc
  
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0

  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

  ld c,$98
    
.write2:   
  push bc
  call outix8
  pop bc
  djnz .write2

  exx

  ret



;animations for horizontal objects in map 03-11c
wendyblock:                      incbin "../grapx/world3/level3-11c/animation.SC2",$0000+7,8*168  ;character table
wendyblockclr:                   incbin "../grapx/world3/level3-11c/animation.SC2",$2000+7,8*168  ;color table

;ret


;move wendy blocks on the screen
;these are vertical blocks that are controlled by 1 object over the entire map.
;scheme flow. Wehave a steady height of the lava. At that height we do not write any tiles
;this height is stored in (objectrelease)
  
  
  
  

MovementPattern337:
;the giant blocks that are moving in wendy' s castle. Will copy paste the code from lemmy' s castle and work them up from there to save lots of time but not ROM space..
  xor a
  ld (forcesetbackground),a

  ld a,(ix+v4)
  and a
  jp z,.init


  call .murdermarioiflava
  call .followmario
;  call animatewendylava
  
  ;do a platforminteract only if mario is within the boundaries of the pillar
  call searchwendyblocktable ;search through the blocktable and write the blocks in place when close enough in screen

 ; call setpage12tohandleobjectmovement
  
  call animatewendyblocks


  ;do movement here
  ld a,(framecounter)
  and 1
  ret z

  ld a,(ix+v3)
  and a
  call nz,.up
  call z,.down
  
  ld hl,(pillary)
  ld a,l
  or h
  jp z,.reverse
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 ;divide by 8 and compare to max value
  ld a,(mapheight)
  ld c,18
  sub c
  srl a ;/2 we use half the (mapheight-16) as our reference pointer
  cp l
  jp c,.reverse  
  
  ret

  

.down:
  
  ld hl,(pillary)
  inc hl
  ld (pillary),hl
  
  ret

.up:

  ld hl,(pillary)
  dec hl
  ld (pillary),hl
  
  ret
  


.reverse:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  
  ret


.followmario: ;only follow for x. we handle Y separately

  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ret

;check lava but check for bkg as well. blocks should crush mario
.murdermarioiflava:


  ld de,0
  ld b,3
  call checkgeneraltilefromhrom
;check downward crush
  cp 040
  jp z,.kill
  cp 041
  jp z,.kill
  cp 042
  jp z,.kill

  ld de,0
  ld b,4
  call checkgeneraltilefromhrom
  cp 130
  jp z,.kill
;check for upward crush  
  cp 216
  jp z,.kill
  cp 216
  jp z,.kill  

  
  cp 173
  jp nc,.check

  ret

.check:

  cp 179
  ret nc

.kill:

  
  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  ret 
  
  
  

.init:

  ;copy pillar buffer to RAM copy from wendypillartable to pillarbkgbuffer in page3 which is not in use.
  ld hl,wendyblocktable
  ld de,pillarbkgbuffer
  ld bc,totallenghtwendyblocktable
  ldir

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

    ld (ix+objectrelease),42 ;set depth  
  
  inc (ix+v4)

  ret



searchwendyblocktable:

  ld iy,pillarbkgbuffer
.searchloop:  
  ld e,(iy)
  ld d,(iy+1) ;fetch x coordinates for this object

  ld a,e
  or d
  ret z ;end of table reached

;do not handle pillar if out of screen!
  push de
  call .getdistance
  pop de
  
  ld (iy+reset),0 ;reset this byte if mario is back in distance of object
  
  ld a,(iy+yposition) ;tile height for each object
  ld hl,(pillary) ;global for pillarheight Y 16bits pixel precise
  
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l  
  ;we now have the height of the object
  add a,l
  ld c,a

;see if object is within mario boundaries. if so adjust the Y coordinates and call the handleplatform interact code.
  push de
  push bc
  push iy
  call .checkmariorange
  pop iy
  pop bc
  pop de

;we check if we are at the edges of the 8 animation steps at that point
;the screen tiles get updated but there is sometimes a small mismatch inbetween frames
;this creates a ugly glitch where it looks as if the platform very quickly changes direction for 1 frame
;we force a screen rebuild after returning from the eventhandler so that the last animation step and tilewrite
;is in perfect sync.
  ld a,(pillary)
  and 7  
  and a
  call z,forcerebuildbackground 
  cp 7
  call z,forcerebuildbackground
;  cp 1
;  jp z,.skipobject  
  cp 2
  jp z,.skipobject ;optimize code a little by only writing the tiles when really needed (still slow as shit on MSX2 (+) )
  cp 3
  jp z,.skipobject
  cp 4
  jp z,.skipobject
  cp 5
  jp z,.skipobject
  cp 6
  jp z,.skipobject  

;skip pillarwriting at these frames to speed things up a little we are seeing screen tears

;we have two types of moving blocks. Here we branch to write the correct type in screen
  ld a,(iy+type)
  and a
  push af
  call z,handlewendypillar0type
  pop af
  dec a
  push af
  call z,handlewendypillar1type
  pop af
;  dec a
;  call z,handlewendypillar2type

.skipobject:

  ld de,wendyblocktablelenght
  add iy,de
  jp .searchloop

;  ret

;problem: When the blocks are going down the black tiles are not written any longer resulting in bad tilebugs
;fix?? perhaps write black bkg once by setting a certain byte? pillarbkgbuffer is unused!
.getdistance:


  ld hl,(mariox)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8  
  and a
  sbc hl,de
  jp c,.negative

  ld de,32
  xor a
  sbc hl,de
  ret c

  jp .stop
  
  
.negative:

  ld de,0
  ex de,hl
  and a
  sbc hl,de

  ld de,32
  xor a
  sbc hl,de
  ret c

.stop:

  ld a,(iy+reset)
  and a
  call z,.destroypillartiles ;destroy only in one frame!

  inc sp
  inc sp
  pop de ;do not forget to pop the byte back!!
  
  
  ld de,wendyblocktablelenght
  add iy,de
  jp .searchloop
  
  ;ret


.destroypillartiles:

  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1

  ld a,(iy+type)
  dec a
  jp z,.destroytype1 ;branch to other pillar type
  dec a
  jp z,.stoperase ;fuck this

;only fetch bkg adress when tiles need destruction
  ld a,(iy+yposition) ;tile height for each object
  ld hl,(pillary) ;global for pillarheight Y 16bits pixel precise
  
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8
  ;we now have the height of the object
  add a,l
  ld c,a
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  ld a,e
  or d
  jp z,.stoperase ;out of map! presearchtable should have a empty byte here any other value will be catched by the following checks
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found  

  ld e,(iy)
  ld d,(iy+1)
  ;Xcoordinate
  add hl,de


;do not even allow this if the pillar is below the lava!!!
  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
;  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;get the mapadress where are we?? 
  
  pop de
  xor a
  sbc hl,de
  exx

  jp c,.stoperase

;erase all lines
.completeline:
  
  ld b,(iy+xsize)
  push hl
.deleteloop:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .deleteloop
  pop hl

  ld de,(maplenght)
  add hl,de ;next row


  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
;  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr ;Y mapadress
  add hl,de

  ;get the mapadress back where are we??   
  pop de
  xor a
  sbc hl,de
  exx
  
  
  jp nc,.completeline


;write back lava
  ld b,(iy+xsize)
  srl b
.deletelavaloop:
  ld a,175
;  call writeanytile
  ld (hl),a
  inc hl
  ld a,176
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .deletelavaloop  



.stoperase:  
  
  ld (iy+reset),1
  
  ret



.destroytype1:

;BUG too many tiles are being cleared!!
; ret
  ld    hl,bgrmapaddr
  ld e,(iy)
  ld d,(iy+1)
  ;Xcoordinate
  add hl,de  ;fetch X keep Y at zero
  
  ld e,25 ;amount of lines to erase

.destroytype1loop:

  push de
  
  ld b,(iy+xsize)
  push hl
.deleteloop2:
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skip ;we are inside a special tile
  cp 213
  jp z,.skip ;we are inside a special tile  
  xor a
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .deleteloop2
.skip:
  pop hl

  ld de,(maplenght)
  add hl,de ;next row

  pop de

  dec e
  
  jp nz,.destroytype1loop

  


;pillar taken out work done
  jp .stoperase

  ;ret


;DO not change DE or BC!!
.checkmariorange:

  ld a,(iy+ignore)
  and a
  jp nz,.unsetignore ;ignore is being unset after 1 frame

  ld a,(iy+type)
  dec a
  ret z ;type 1 has no platform interactions

  ld hl,(mariox)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l   
  xor a
  push hl
  sbc hl,de
  pop hl
  ret c ;exceeds left boundary

  ld c,(iy+xsize)
  ld b,0
  ex de,hl
  add hl,bc
  xor a
  dec hl ;compensate for smoother step off
  sbc hl,de
  ret c; exceeds right boundary

  ;now get the exact coordinates of the objectheight in 16 bits and adjust the elevation to that
  
  ld l,(iy+yposition) ;tile height for each object and multiply this number by 8 then add pillary to it making it pixel precise 
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  ld de,(pillary)
  add hl,de
  ld de,10 ;compensate for pixels in the top layer ;10
  xor a
  sbc hl,de

  push hl
  ld de,(marioy)
  xor a
  sbc hl,de
  call nc,.setheight ;only for pillar type 2
  pop hl

;prevent the object from exiting the map 
  push hl
  ex de,hl
  ld hl,(mapheightx8)
  push de
  ld de,32
  xor a
  sbc hl,de
  pop de  
  pop de
  
  push de
  xor a
  sbc hl,de
  pop hl
  ret c

  ;write result to object
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ;everything good now check if mario is inside the pillar for some stupid reason and if so push him out
  ld de,1
  ld b,3
  call checkgeneraltilefromhrom
  cp 037
  call nc,.pushmarioup
  
  ld c,0
  jp platforminteract
  
;  ret


.setheight:

  ld a,(iy+type)
  cp 2
  ret nz ;only type 2

  push iy
  ld de,wendyblocktablelenght
  add iy,de
  ld (iy+ignore),1 ;tell next object to ignore platform interactions
  pop iy
  ret

.unsetignore:

  ld (iy+ignore),0
  ret


.pushmarioup:

;if a type 1 pillar is above mario and a type 0 below jumping against the type 1 will results in problems so we must check height boundaries too
;  ld a,(standingonsprite)
;  and a
;  ret z
  cp 040
  ret z
  cp 041
  ret z
  cp 042
  ret z

  ld hl,(marioy)
  ld de,8
  xor a
  sbc hl,de
  ld (marioy),hl


  ret


;IN c = true Yposition of pillar. DE = Xcoordinates of pillar
handlewendypillar1type:
;lucky for us type 1 has no exit tiles meaning that the blocks are always in the foreground. 
;I am even happier about the fact that there is absolutely no need to check for mapexit!
;The pillars never exceed the maximum height of the map. This makes clearing quite easy too :)
;Limit the amount of tilewrites to a certain height in the map. This saves alot of writes to RAM
;preventing framedrops on MSX2(+)

;PLAN: each tile is written with call writeanytile. This requires 2xout and is a call with a return.
;Even on the turbo-R this gives a major slowdown. It is more optimal to switch page1 to RAM (lucky for us the map fits in 1 page)
;and do direct tilewrites from code saving us 1200 outs per frame!
  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1
    
  push de
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found
  
  
  pop de
  add hl,de
  ;x adress found. We now have the mapadress
  
  
  push hl
  
  ld de,(maplenght)
  add hl,de
  
  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline0
  cp 213
  jp z,.skipline0
  cp 210
  jp z,.skipline0
  cp 008
  jp z,.skipline0
  
;bottomline
  ld b,(iy+xsize)
.line0:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .line0  

.skipline0:

  pop hl


  push hl

  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline1  
  cp 210
  jp z,.skipline1

  ld a,040
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line1:
  inc hl
  ld a,041
;  call writeanytile
  ld (hl),a
  djnz .line1
  inc hl
  ld a,042
;  call writeanytile
  ld (hl),a

.skipline1:

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de



  push hl

  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline2  
  cp 210
  jp z,.skipline2


  ld a,227
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line2:
  inc hl
  ld a,228
;  call writeanytile
  ld (hl),a
  djnz .line2
  inc hl
  ld a,229
;  call writeanytile
  ld (hl),a

.skipline2:

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de

.writeline3:

  push hl

  ld a,230
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line3:
  inc hl
  ld a,231
;  call writeanytile
  ld (hl),a
  djnz .line3
  inc hl
  ld a,232
;  call writeanytile
  ld (hl),a

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de


  push hl
  exx

  ld hl,(marioy)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8  
  ld de,11
  xor a
  sbc hl,de
  
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,l ;lets say stop at where mario is so that the engine only places the tiles in screen that we see
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress
  pop de
  xor a
  sbc hl,de
  exx

  jp c,.writeline3


  ret


;IN c = true Yposition of pillar. DE = Xcoordinates of pillar
handlewendypillar0type:

  ld a,(ix+objectrelease) ;see if the height is below the lavay, if yes stop!
  cp c
  ret c

  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1

    
  push de
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found
  
  
  pop de
  add hl,de
  ;x adress found. We now have the mapadress
  
;skip black topline when fully upwards as well  
  push hl
  ld hl,(pillary)
  ld de,8
  xor a
  sbc hl,de
  pop hl
  jp c,.skipblacktopline
  
  
;black topline
  push hl
  ld de,(maplenght)
  xor a
  sbc hl,de  

  ld b,(iy+xsize)
.line0:
  ld a,0
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .line0

  pop hl

.skipblacktopline:

  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  cp c
  jp c,.writelavaback ;done so write the old lava back

  push hl

  ld a,037
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line1:
  inc hl
  ld a,038
;  call writeanytile
  ld (hl),a
  djnz .line1
  inc hl
  ld a,039
;  call writeanytile
  ld (hl),a

  pop hl
  ld de,(maplenght)
  add hl,de
  
  
  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  dec a
  cp c
  ret c ;done  
  
  
  push hl

  ld a,221
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line2:
  inc hl
  ld a,222
;  call writeanytile
  ld (hl),a
  djnz .line2
  inc hl
  ld a,223
;  call writeanytile
  ld (hl),a

  pop hl

  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  dec a
  dec a
  cp c
  ret c ;done  


.completeline:

  push hl

  pop hl
  ld de,(maplenght)
  add hl,de


  push hl
  ld a,224
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line3:
  inc hl
  ld a,225
;  call writeanytile
  ld (hl),a
  djnz .line3
  inc hl
  ld a,226
;  call writeanytile
  ld (hl),a
  pop hl


  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;get the mapadress where are we?? 
  
  pop de
  xor a
  sbc hl,de
  exx
  
  jp nc,.completeline

  ld de,(maplenght)
  add hl,de

;write the lava tiles in place
  ld b,(iy+xsize)
  srl b
.line4:
  ld a,173
;  call writeanytile
  ld (hl),a
  inc hl
  ld a,174
;  call writeanytile
  ld (hl),a  
  inc hl
  djnz .line4
  
  


  ret


.writelavaback:

    ret


;copy this table to ram on init so we can write to it
wendyblocktable: ;the blocktable that defines all the blocks used in wendys castle
;all positions are defined in tiles
;xsize must at least be 3!!!!
;type 0 = down up
;type 1 = up down
;other types are unknown to me at the moment


  ;xposition,       type, xsize, yposition, reset, ignore
    dw 054      : db  0,    014,     18,       0,     0
    dw 054      : db  1,    014,     08,       0,     0 ;block 1
    
    dw 087      : db  1,    004,     21,       0,     0 ;block 2
    dw 091      : db  1,    007,     13,       0,     0 
    dw 098      : db  1,    006,     21,       0,     0 
    dw 104      : db  1,    007,     13,       0,     0 
    dw 111      : db  1,    004,     21,       0,     0 
    
    dw 126      : db  1,    004,     13,       0,     0 ;block3

    dw 143      : db  0,    009,     18+8,       0,     0
;    dw 143      : db  1,    009,     08+8,       0,     0 ;block 4    

    dw 184      : db  1,    004,     08+8,       0,     0 ;block 5    
    
    dw 198      : db  0,    019,     18,       0,     0 ;final block
    
    
    dw 000 ;end of table       

totallenghtwendyblocktable: equ $ - wendyblocktable

wendyblocktablelenght:  equ 7
;type:                   equ 2
;xsize:                  equ 3
;yposition:              equ 4
;reset:                  equ 5
;ignore:                 equ 6
;these are allready defined in other code


animatewendyblocks:

  ld a,(pillary)
  and 7
  cp 0  
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
;  cp 7
;  jp z,.step7  
        
  
;  ret

  jp .step7

.writestep:

  push hl
  push de

  ld a,221
  ld b,12
  call updatewendyblocks
  
  pop hl
  ld de,8*12
  add hl,de
  ex de,hl
  pop hl
  ld bc,8*12
  add hl,bc
  
  
  
  ld a,37
  ld b,6
  call updatewendyblocks

  ret


.step0:

  ld hl,wendyblock+(8*0)
  ld de,wendyblockclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,wendyblock+(8*18)
  ld de,wendyblockclr+(8*18)   

  jp .writestep

.step2:

  ld hl,wendyblock+(8*36)
  ld de,wendyblockclr+(8*36)   

  jp .writestep

.step3:

  ld hl,wendyblock+(8*54)
  ld de,wendyblockclr+(8*54)   

  jp .writestep

.step4:

  ld hl,wendyblock+(8*72)
  ld de,wendyblockclr+(8*72)   

  jp .writestep

.step5:

  ld hl,wendyblock+(8*90)
  ld de,wendyblockclr+(8*90)   

  jp .writestep

.step6:

  ld hl,wendyblock+(8*108)
  ld de,wendyblockclr+(8*108)  

  jp .writestep
  
.step7:

  ld hl,wendyblock+(8*126)
  ld de,wendyblockclr+(8*126)   

  jp .writestep
  


  
  
  
  
MovementPattern336:     ;circlesaw (left right)  
  

  ld a,(ix+v4)
  and a
  jp z,.init
  
  
  call mariointeract
  
  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  

  
    ld de,0
    ld bc,16
    call objecttilecheckfromrom
    ;get mapadress
    and a
    call z,.setfall

    
  
  
  call checkobjecttilescheckwall
  call c,.wallfound            ;if wall found then change direction by inverting v1, and set animation  
  
    call .animate ;animate half the thing
  
  ld (ix+objectfp),0
  
  ret

  
.setfall:


    ld (ix+v2),2
    ret
    
  
  

.wallfound:

    ld a,(ix+v1)
    neg
    ld (ix+v1),a
  
  ret
  

  
.animate:
 
 
    ld a,(framecounter)
    and 1
    ret z
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddresscirclesaw  ;color address
  ld    bc,characteraddresscirclesaw  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  
  

.position1:
  ld    hl,coloraddresscirclesaw1  ;color address
  ld    bc,characteraddresscirclesaw1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  

  ret
    

    
.init:

    inc (ix+v4)

    ;nohing special. Just check boundary then set correct movementpattern
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,1130
    xor a
    sbc hl,de
    ret c
    
    
    ld hl,312
    ld (ix+movepat),l
    ld (ix+movepat+1),h

    ld (ix+v5),20 ;left/right
    ld (ix+v6),10  ;up/down


    ret
    
    
  
  
movewendyspikeupsidedown:

    
    
    exx
;    ld hl,(wendyspikeadresstable) ;prepare mapadress we later have to move this out of here and feed the mapadress from the code self
    ld l,(iy)
    ld h,(iy+1)
    ld de,(maplenght)
    exx
    
    ld a,(ix+v3)
    and a
    ret z ;any value lower than 1 is not allowed
   

.getinitialadress: ;get the initital height of the pillar. That is... baseadress + v3
   
        exx
        add hl,de
        exx
        
        dec a
        
        jp nz,.getinitialadress
   
   
   
;line 0
        exx
        ;now write the lines
        
        ld b,6
        xor a
        ld c,1
        push hl
        call writemanytile
        pop hl
        
        add hl,de
        
        
        
        ;write line 1

        ld b,6
        ld a,96
        ld c,0
        push hl
        call writemanytile
        pop hl    
   
        add hl,de
   
        ;write line 2

        ld b,6
        ld a,102
        ld c,0
        push hl
        call writemanytile
        pop hl    
   
        add hl,de
   
        exx
    
    ld b,(ix+v3)
    ld a,14
    sub b
    
    ld b,a
    
    
    ld c,0
    
.writeloop:    
    
    ld a,c
    
    
    and 3 ;prepare for line table
    ld e,a
    ld d,0
    ld hl,.linetable
    add hl,de
    ld a,(hl) ;and there is our tilenumber for writing
    exx
    ld b,6
    ld c,0
    push hl
    call writemanytile
    pop hl
    
    add hl,de ;next row
    exx
    
    inc c
    
    djnz .writeloop    
    
    
    
    
    
    
    
    ret

;linetable      0   1   2   3    
.linetable: db  32, 38, 44, 50  


  
  
movewendyspike:
;read ix+v3 thats how far the pillar in TILES drop. Lower number, les pillar, higher number more pillar. It checks a line table and by that knows at which side of the loop it is.
 
    
    
    ld a,(iy+region)
    dec a
    jp z,movewendyspikeupsidedown
    
    
    
    exx
;    ld hl,(wendyspikeadresstable) ;prepare mapadress we later have to move this out of here and feed the mapadress from the code self
    ld l,(iy)
    ld h,(iy+1)
    ld de,(maplenght)
    exx
    
    ld a,(ix+v3)
    and a
    ret z ;any value lower than 1 is not allowed
    ld b,a ;amount of lines to write till done
    
    ld c,a
    ld a,3
    sub c
    
    ld c,a ;store line number in c

    
.writeloop:    
    
    ld a,c
    
    
    and 3 ;prepare for line table
    ld e,a
    ld d,0
    ld hl,.linetable
    add hl,de
    ld a,(hl) ;and there is our tilenumber for writing
    exx
    ld b,6
    ld c,0
    push hl
    call writemanytile
    pop hl
    
    add hl,de ;next row
    exx
    
    inc c
    
    djnz .writeloop

    
    exx
;add terminator code here. We could make the terminator a little bit more messy but at the same time more bkg friendly. We only write the data that is allowed to write.

    ld c,56 ;start of terminator
    ld b,6 ;6 tiles to write
    push hl
    
.writeterminator:
    
    call readanytile ;get bkg
    cp 68
    jp nc,.nexttile
    
    ld a,c
    call writeanytile
   
.nexttile:
   
   inc hl
   inc c
   
    djnz .writeterminator
   
    pop hl
    add hl,de ;next row
    
    

    push hl
    ld b,6 ;6 tiles to write
    
.writeterminator2:
    
    call readanytile ;get bkg
    cp 68
    jp nc,.nexttile2
    
    ld a,c
    call writeanytile
   
.nexttile2:
   
   inc hl
   inc c
   
    djnz .writeterminator2

    pop hl
    add hl,de ;next row    
    
;and one line of black lines 

    ld b,6 ;6 tiles to write
    
.writeterminator3:
    
    call readanytile ;get bkg
    cp 68
    jp nc,.nexttile3
    
    xor a
    call writeanytile
   
.nexttile3:
   
   inc hl
   
    djnz .writeterminator3

    
    
;write a 4th terminator to eliminate tilebugs at higher speeds    
    
    

    ret

;linetable      0   1   2   3    
.linetable: db  32, 38, 44, 50  
    
    

  
  
MovementPattern335:     ;wendy superspike  
;works as follows writes layers of tiles onto the map and moves the pillar at 8x8 speed. The object itself follows mario around and takes care of the damage that mario receives
;technically the pillars are allready loaded into the tileset. Since there is no background on this map we do not need to save the bkg first so its indentify then perform.
;we use dummy objects to obtain the mapadress for each spike


;pillar is build up out of 36 tiles total. 4 repetetive layers and 2 down layers. We use writemanytile code as the first tile is numbered 32. Each row is 6 tiles wide. SO only 6 calls per frame. Thats doable...

;new problem: now we need to make the code work mapwide. This means @ each frame read all pillars from the wendytable and handle their movement
;the pillars are divided in 3 blocks. Which block is being handled depends on the position of mario


    call followmario

    
;check tiles and kill if needed
    call .checkkill
    
    
    ld a,(currentlevel)
    cp 148
    jp z,handleregion148
;fetch region type 0 = the first one 1 = the large line of pillars 2 = the 3 spikes at the end    
    
    ld hl,(mariox)
    xor a
    ld de,580
    sbc hl,de
    jp nc,.checkregion1
    
    
        jp .handleregion0
    
    
    
   ; ret

   
.checkkill:


    ld de,0
    ld bc,17
    call objecttilecheckfromrom
    ;get mapadress
    cp 32
    jp nc,.check2
    cp 164
    jp z,.kill
    cp 165
    jp z,.kill
    cp 166
    jp z,.kill
    cp 167
    jp z,.kill    
    
    
    
    ret
   
.check2:


    cp 66
    ret nc
    
  ;  jp .kill
    
 ;   ret
    

.kill:

    ld a,(ix+v3)
    and a
    ret z ;no kill if motion has stopped

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a



    ret
    
   
  
.checkregion2:

    ld de,2400
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp c,.reset




    ;ret  
    
.handleregion2:      
;up and down without any breaks
    ld iy,wendyspikeadresstable.region2 ;read the spike table and fetch required info from there

    ld a,(ix+v4)
    and a
    jp z,.wait2
    dec a
    jp z,.drop2
    dec a
    jp z,.wait2 ;pillar is down
    dec a
    jp z,.moveup2 ;move back up
    
    
    
    ld (ix+v4),0


  
  ret

  
  
  
    
.reset:

;do this to prevent gaps from forming into the ground when entering region 1

    ld (ix+v3),0
    ld (ix+v7),0
    ld (ix+v4),0
    ret
    
    
.checkregion1:


    ld de,1145
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp c,.reset

    ld de,2200
    ld hl,(mariox)
    xor a
    sbc hl,de
    jp nc,.checkregion2    
    
    

;    jp .handleregion1



 ;   ret
    
    
.handleregion1:    

    ld iy,wendyspikeadresstable ;read the spike table and fetch neede info from there

    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.drop1
    dec a
    jp z,.wait ;pillar is down
    dec a
    jp z,.moveup1 ;move back up
    
    
    
    ld (ix+v4),0


  
  ret



    
.handleregion0:    
    
    
    
    
    ld iy,wendyspikeadresstable ;read the spike table and fetch neede info from there
  
    
    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.drop
    dec a
    jp z,.wait ;pillar is down
    dec a
    jp z,.moveup ;move back up
    
    
    
    ld (ix+v4),0


  
  ret


.moveup:



    dec (ix+v3)
    call movewendyspike

    ld a,(ix+v3)
    and a
    jp z,incv4
    


    ret

    
    
.moveup2:



    dec (ix+v3)
;    dec (ix+v3)
    
    
    
.uploop2:
    
    
    ;loop through all spikes in region1
    ld de,wendyspikeadresstablesize
    ;skip first region
    add iy,de 
    
    call movewendyspike

    
    ld a,(iy+region)
    cp 5
    jp nz,.uploop2
    
    
    ld a,(ix+v3)
    and a
    jp z,.incv4
    


    ret    
    
    
    
    
.moveup1:



    dec (ix+v3)
    
    
    
.uploop1:
    
    
    ;loop through all spikes in region1
    ld de,wendyspikeadresstablesize
    ;skip first region
    add iy,de 
    
    call movewendyspike

    
    ld a,(iy+region)
    cp 5
    jp nz,.uploop1
    
    
    ld a,(ix+v3)
    and a
    jp z,incv4
    


    ret    

    
.drop2:

   
    inc (ix+v3)
 ;   inc (ix+v3)
    
    
.drop2loop: 
    
    ;loop through all spikes in region1
    ld de,wendyspikeadresstablesize
    ;skip first region
    add iy,de 
    
    call movewendyspike

    
    ld a,(iy+region)
    cp 5
    jp nz,.drop2loop
    
    
    
;determine on end
    
    ld c,(iy+ymax) ;maximum depth
    
    ld a,(ix+v3)
    cp c
    jp z,.incv4
    


    ret      
    


.drop1:

   
    inc (ix+v3)
    inc (ix+v3)
    
    
.drop1loop: 
    
    ;loop through all spikes in region1
    ld de,wendyspikeadresstablesize
    ;skip first region
    add iy,de 
    
    call movewendyspike

    
    ld a,(iy+region)
    cp 5
    jp nz,.drop1loop
    
    
    
;determine on end
    
    ld c,(iy+ymax) ;maximum depth
    
    ld a,(ix+v3)
    cp c
    jp z,.incv4
    


    ret    
  
  
  
  
.drop:


    inc (ix+v3)
    inc (ix+v3)
    call movewendyspike

    ld c,(iy+ymax) ;maximum depth
    
    ld a,(ix+v3)
    cp c
    jp z,.incv4
    


    ret
  
  

.wait2:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    ret c
    
    jp incv4



    ;ret  
  
  
.wait:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c
    
    jp incv4



    ;ret
  

.incv4:


    ld a,1
    ld (tripscreen),a
    
    call Playsfx.explosion
    
    jp incv4
    
    ;ret
  
  
  
;all the wendy spikes. Handle them one by one each frame

;adress table has a few entries. the bkgadress, maximum movement depth, movement type 0 = downwards 1 = upwards

wendyspikeadresstable: dw bgrmapaddr+3496 : db 20,0  ;region 0

                       dw bgrmapaddr+4402 : db 18,0  ;region 1
                       dw bgrmapaddr+4414 : db 18,0  
                       dw bgrmapaddr+4426 : db 18,0  
                       dw bgrmapaddr+4438 : db 18,0  
                      ; dw bgrmapaddr+4446 : db 18,0  
                       dw bgrmapaddr+4453 : db 18,0
.region2:
                       dw bgrmapaddr+4461 : db 18,5 ;<==== end of region  
 
 
                       dw bgrmapaddr+5308 : db 12,0 
                       dw bgrmapaddr+5702 : db 12,1 ;mind the special block 
                       dw bgrmapaddr+5327 : db 12,5 ;<==== end of region  




ymax: equ 2
region: equ 3
wendyspikeadresstablesize: equ 4


  
MovementPattern334:     ;wooden pillar  (up down)
  
  ;waste of rom space but less waste of my time we do the other way differently to prevent spagethi code effects
  
  
    ld a,(ix+v4)
    and a
    jp z,.init
  
  

  
    call getmariodistance
    cp 220
    jp nc,MovementPattern333.restore ;seems to work just fine
    
  
    call mariointeract
    jp c,.pushmarioup
    
    
    ld a,(ix+v4)
    dec a
    jp z,.wait
    dec a
    jp z,.godown
    dec a
    jp z,.wait
    dec a
;    ret z
    jp z,.goup
    
  
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),1
  
  
  
  
  ret

  
.pushmarioup:
;trow mario to the left or right so that he does not get stuck in the new tiles

    ld hl,(mariox)
    ld de,8
    xor a
    sbc hl,de
    ld (mariox),hl

    ret
  
  
  
.goup:
;going down is a tad more complex since we need to write back the bkg tiles
    ld a,(ix+v7)
    inc (ix+v7)
    cp 32
    jp nc,incv4 ;set new speed
    
    call addv2toybject
    
    ld a,(ix+yobject)
    and 7
    cp 0
    ret nz ;only in steps of 8!!
    
    push ix
    pop hl ;get custom data
    ld de,offsets+25
    add hl,de
    ld d,0
    ld e,(ix+v3) ;add entry to it
    add hl,de
    
    ;we now have the table pointer
    
    exx
    
    
    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    ;get mapadress    
    
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    exx
    ld a,(hl)
    exx
    inc hl
    call writeanytile
    
    
    inc (ix+v3) ;shift pointer
    inc (ix+v3) ;shift pointer




    ret
  
  
.godown:

    ld (ix+v2),2
    


    ld a,(ix+v7)
    inc (ix+v7)
    cp 16
    jp nc,.incv4 ;set new speed

    
    call addv2toybject
    
    ld a,(ix+yobject)
    and 7
    cp 7
    ret nz ;cpu saver
    
    ;otherwise write tile in place
    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    ;get mapadress
    
    ld a,208
    ld c,0
    ld b,2
    call writemanytile ;write 1 line


    ret
  
.incv4:

    ld (ix+v3),0 ;mapadress read pointer
    ld (ix+v2),-1 ;new speed upwards
    jp incv4
  
  
.wait:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c
    

    
    jp incv4
    
    ;ret
  
  

/*  
.restore:

;object is out of screen so we need to restore the bkg tiles
    ld l,(ix+v5)
    ld h,(ix+v5+1) ;obtain previously stored mapadress
    
;    ld de,4
;    xor a
;    sbc hl,de ;temp code to see if this works
    
    exx    
    
    push ix
    pop hl ;get restore table
    ld de,offsets+25
    add hl,de
    

    ld b,4;8

    
.restoretileloop:    
    
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    inc hl
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    dec hl
    ld de,(maplenght)
    add hl,de
    exx

    
    djnz .restoretileloop
    
    
    jp destroyobject.objectreturns
    
    
    ;ret
*/
  
  

.init:

    ;displace object 1 tile backwards. so that we can place two of these pillars above each other
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,8
    xor a
    sbc hl,de
    ld (ix+xobject),l
    ld (ix+xobject+1),h


    ;get mapadress above the spike and save the entire bkg thats above the pillar
    
    push ix
    pop hl ;object adress in hl
    ld de,offsets+25
    add hl,de; this is where we save our tiles to
    
    exx
        
    ld de,0
    ld bc,17
    call objecttilecheckfromrom
    ;get initial mapadress and save it
    ld (ix+v5),l
    ld (ix+v5+1),h        

    ld b,4;8 ;64 high
    
.readtileloop:
    
    call readanytile
    ld c,a
    inc hl
    call readanytile
    dec hl
    
    ld de,(maplenght)
    add hl,de ;next row
    
    push bc
    exx
    pop bc
    ld (hl),c
    inc hl
    ld (hl),a
    inc hl
    exx

    djnz .readtileloop

    inc (ix+v4)
    
    ret
    
    
  
  
  
MovementPattern333:     ;wooden pillar  
  
  
    ld a,(ix+v4)
    and a
    jp z,.init
    
    call getmariodistance
    cp 220
    jp nc,.restore ;seems to work just fine

    call mariointeract
    jp c,.pushmarioup
    
    
    ld a,(ix+v4)
    dec a
    jp z,.wait
    dec a
    jp z,.goup
    dec a
    jp z,.wait
    dec a
    jp z,.godown
    
  
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),1
  
  
  
  
  ret

  
.pushmarioup:
;trow mario to the left or right so that he does not get stuck in the new tiles

    ld hl,(mariox)
    ld de,8
    xor a
    sbc hl,de
    ld (mariox),hl

    ret
  
  
  
.godown:
;going down is a tad more complex since we need to write back the bkg tiles
    ld a,(ix+v7)
    inc (ix+v7)
    cp 32
    jp nc,incv4 ;set new speed
    
    call addv2toybject
    
    ld a,(ix+yobject)
    and 7
    cp 0
    ret nz ;only in steps of 8!!
    
    push ix
    pop hl ;get custom data
    ld de,offsets+25
    add hl,de
    ld d,0
    ld e,(ix+v3) ;add entry to it
    add hl,de
    
    ;we now have the table pointer
    
    exx
    
    
    ld de,0
    ld bc,16
    call objecttilecheckfromrom
    ;get mapadress    
    
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    exx
    ld a,(hl)
    exx
    inc hl
    call writeanytile
    
    
    inc (ix+v3) ;shift pointer
    inc (ix+v3) ;shift pointer




    ret
  
  
.goup:

    ld (ix+v2),-2
    


    ld a,(ix+v7)
    inc (ix+v7)
    cp 16
    jp nc,.incv4 ;set new speed

    
    call addv2toybject
    
    ld a,(ix+yobject)
    and 7
    cp 7
    ret nz ;cpu saver
    
    ;otherwise write tile in place
    ld de,0
    ld bc,17
    call objecttilecheckfromrom
    ;get mapadress
    
    ld a,208
    ld c,0
    ld b,2
    call writemanytile ;write 1 line


    ret
  
.incv4:

    ld (ix+v3),0 ;mapadress read pointer
    ld (ix+v2),1 ;new speed downwards
    jp incv4
  
  
.wait:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c
    

    
    jp incv4
    
    ;ret
  
  

  
.restore:

;object is out of screen so we need to restore the bkg tiles
    ld l,(ix+v5)
    ld h,(ix+v5+1) ;obtain previously stored mapadress
    
;    ld de,4
;    xor a
;    sbc hl,de ;temp code to see if this works
    
    exx    
    
    push ix
    pop hl ;get restore table
    ld de,offsets+25
    add hl,de
    

    ld b,4;8

    
.restoretileloop:    
    
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    inc hl
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    dec hl
    ld de,(maplenght)
    add hl,de
    exx

    
    djnz .restoretileloop
    
    
    jp destroyobject.objectreturns
    
    
    ;ret

  
  

.init:



    ;get mapadress above the spike and save the entire bkg thats above the pillar
    
    push ix
    pop hl ;object adress in hl
    ld de,offsets+25
    add hl,de; this is where we save our tiles to
    
    exx
        
    ld de,0
    ld bc,-64+16+1+32
    call objecttilecheckfromrom
    ;get initial mapadress and save it
    ld (ix+v5),l
    ld (ix+v5+1),h        

    ld b,4;8 ;64 high
    
.readtileloop:
    
    call readanytile
    ld c,a
    inc hl
    call readanytile
    dec hl
    
    ld de,(maplenght)
    add hl,de ;next row
    
    push bc
    exx
    pop bc
    ld (hl),c
    inc hl
    ld (hl),a
    inc hl
    exx

    djnz .readtileloop

    inc (ix+v4)
    
    ret
    
  
  
  
  
MovementPattern332:     ;castleflame  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  
  
  call mariointeract
  
    call addv1toxbject
    
  
  

    ld a,(framecounter)
    and 3
    ret nz

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,coloraddressstonekoopafireball  ;color address
  ld    bc,characteraddressstonekoopafireball   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressstonekoopafireball1  ;color address
  ld    bc,characteraddressstonekoopafireball1   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  

    ;ret  
  

.init:


    call Playsfx.yoshifireball


    inc (ix+v4)

    ret

  

  
MovementPattern331:     ;special yellow pillar (helper platform) 
  
  ld a,(ix+v4)
  and a
  jp z,.init ;write black lines on init if pillary is greater than 0 so that the top does not deform
  
  ld c,0
  call platforminteract
  
  ;set coordinates as stated in the pillary animation variable
  
  ld de,(pillary)
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h 
  
;check if the object is nearly out of the map then directly destroy
  ex de,hl
  ld hl,43*8
  xor a
  sbc hl,de
  jp c,.destroyobject ;write final tileline to lava then destroy
  

;TODO: if mario has jumped to the other side check if mario is far away then destroy itself and write only black lines in place
;this to prevent form the player to walk back and find a half deformed platform in place
  
  ld a,(pillarspeed) ;use this part of ram to see if the pillar has been activated
  and a
  ret z ;nope
  
  
  call getmariodistance
  cp 220
  jp nc,.destroyfromscreen ;when the pillar is out of screen we completely remove its tiles and objects in a permanent way
  
  
    ld de,0
    ld bc,1
    call objecttilecheckfromrom
    ;get mapadress
    
;    ld a,(ix+yobject)
;    and 7
;    ret nz
    ld a,(pillaranimationstep)
    cp 7
    ret nz
    
    
;write 3 lines per 8 pixels

;line 0
    push hl

    xor a
    ld b,(ix+v3)
    ld c,1
    call writemanytile

    ld e,(ix+yobject)
    ld d,(ix+yobject+1)
    ld hl,42*8
    xor a
    sbc hl,de
    pop hl
;    jp c,.end ;write limit  
   ret c    
    

    
    ld de,(maplenght)
    add hl,de



    
;line 1
    push hl

    ld a,164
    call writeanytile ;left corner
    inc hl
    
    ld a,(ix+v3) ;tilewidth
    sub a,2
    srl a;/2
    ld b,a
    
.loop:
    ld a,165
    call writeanytile
    inc hl
    ld a,166
    call writeanytile
    inc hl
    
    djnz .loop
    
    ld a,167
    call writeanytile

    ld e,(ix+yobject)
    ld d,(ix+yobject+1)
    ld hl,41*8
    xor a
    sbc hl,de
    pop hl
;    jp c,.end ;write limit 
   ret c 

    ld de,(maplenght)
    add hl,de
    

    

;line 2
    push hl

    ld a,160
    call writeanytile ;left corner
    inc hl
    
    ld a,(ix+v3) ;tilewidth
    sub a,2
    srl a;/2
    ld b,a
    
.loop2:
    ld a,161
    call writeanytile
    inc hl
    ld a,162
    call writeanytile
    inc hl
    
    djnz .loop2
    
    ld a,163
    call writeanytile
    
    ld e,(ix+yobject)
    ld d,(ix+yobject+1)
    ld hl,40*8
    xor a
    sbc hl,de
    pop hl
;    jp c,.end ;write limit 
   ret c 
    
    ld de,(maplenght)
    add hl,de
    
;line 3

    push hl

    ld a,224
    call writeanytile ;left corner
    inc hl
    
    ld a,(ix+v3) ;tilewidth
    dec a
    dec a
    ld b,a
    ld c,1
    ld a,176
    call writemanytile ;much faster
    
    ld a,226
    call writeanytile
    
    pop hl  


    ret
    
    
;  jp forcebuildscreen ;there is no choice but to rebuild the screen. we need to keep everything in correct sync
  ;PROBLEM: if there are multiple objects on screen the screen will be rebuild from each object. We may only rebuild the screen at the end of each frame so this tactic isn' t going to work :(
  
 ; ret

 ;remove completely from screen and wipe out anything thats left
.destroyfromscreen:

    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    ;get mapadress
    
.destructloop:    
    
    push hl
    
    ld b,(ix+v3)
    ld c,1
    xor a
    call writemanytile
    
    pop hl
    
    ld de,(maplenght)
    add hl,de
    
    call readanytile
    cp 179
    jp nz,.destructloop ;keep going until all is gone

    
    ld a,(ix+v3) ;tilewidth
    srl a;/2
    ld b,a
    
.loop4:
    ld a,177
    call writeanytile
    inc hl
    ld a,178
    call writeanytile
    inc hl
    
    djnz .loop4    
    
    
    jp destroyobject
    
    ;ret
    


    
  
.destroyobject:

    ;write tiles
    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    ;get mapadress
    ld a,(ix+v3) ;tilewidth
    srl a;/2
    ld b,a
    
.loop3:
    ld a,177
    call writeanytile
    inc hl
    ld a,178
    call writeanytile
    inc hl
    
    djnz .loop3
    


    jp destroyobject
  
  
.init:

    ld de,0
    ld (ix+sizeX),-8 ;make extremely small for starters
        
    
.checkloop:    
    
    push de
    
    ;check boundaries and add to sizeX
    ld bc,17
    call objecttilecheckfromrom
    pop de
    and a
    push af
    
    ld a,(ix+sizeX)
    add a,8
    ld (ix+sizeX),a
    
    
    ex de,hl
    ld de,8
    add hl,de ;next tile
    ex de,hl

    pop af
    jp nz,.checkloop
    
    
    ;save the primary y coordinates
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld (ix+v5),l
    ld (ix+v5+1),h
    
    
    ;save the sizeX as a tile number
    ld a,(ix+sizeX)
    srl a ;/2
    srl a ;/4
    srl a ;/8
    ld (ix+v3),a
    
    
    inc (ix+v4)
    

;write bogus tiles from top to down    
    
    ld de,(pillary)
    ld a,d
    or e
    ret z ;pillary is still 0
    
    srl e
    srl e
    srl e ;/8
    
    ld a,e
    and a
    ret z ;no difference
    
    
    push de
    
    ld de,0
    ld bc,8
    call objecttilecheckfromrom
    ;get mapadress    
    
    pop de
    
.writenoploop:
    
    push hl
    
    ld b,(ix+v3)
    ld c,1
    xor a
    call writemanytile
    
    pop hl
    
    dec e
    
    ld bc,(maplenght)
    add hl,bc
    
    jp nz,.writenoploop
    
    
    
    ret
  
animatelevel607c:  

  ld    a,animationtilesblock
  call  block12


;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  jr    z,.step4
  ret   ;nz
  
 ;   exx


.step4:
  ld hl,animationtilescharacteraddr+(8*36)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*36)             ;rotating tile animation 1 character address in ro
  call .updateblocks   
  ld hl,animationtiles0301c+(8*18)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*18)             ;rotating tile animation 1 character address in ro  
  jp .updateblocks2   

.step3:
  ld hl,animationtilescharacteraddr+(8*26)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*26)             ;rotating tile animation 1 character address in ro
  call .updateblocks   
  ld hl,animationtiles0301c+(8*12)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*12)             ;rotating tile animation 1 character address in ro  
  jp .updateblocks2

.step2:
  ld hl,animationtilescharacteraddr+(8*16)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*16)             ;rotating tile animation 1 character address in ro  
  call .updateblocks   
  ld hl,animationtiles0301c+(8*06)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*06)             ;rotating tile animation 1 character address in ro  
  jp .updateblocks2

.step1:
  ld hl,animationtilescharacteraddr+(8*6)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*6)             ;rotating tile animation 1 character address in ro
  call .updateblocks   
  ld hl,animationtiles0301c+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0301cclr+(8*00)             ;rotating tile animation 1 character address in ro  
  jp .updateblocks2
 

.updateblocks:
  
  exx 

  
  ld    hl,$4000+192*8
  call .doshit
  ld    hl,$0000+192*8 
  call .doshit

  ret
;  jp setpage12tohandleobjectmovement
 
; ret 


.updateblocks2:
  
  exx 

  
  ld    hl,$4000+177*8
  call .doshit
  ld    hl,$0000+177*8 
  call .doshit

  jp setpage12tohandleobjectmovement



.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix48
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix48

  ret  
  
  
MovementPattern330:     ;special yellow pillar (maplevel6-07c)  

;use this object for any other map on world6 as the lava killer
    ld a,(currentlevel)
    cp 127 ;thats the map we meed this object for
    jp nz,.handlemariolava
    cp 130
    jp nc,.handlemariolava130131

;when pillarspeed = set to 1 then all objects know that the pillar is active
    ld a,(ix+v4)
    and a
    jp z,.init


    call followmario

    ld a,1 ;make sure that the engine knows we are only animating blocks
    ld (animatespecial),a
    
    call animateyellowpillartiles2 
    call animatelevel607c
    
    call .checkstop
    
    ld a,(pillarspeed)
    and a
    jp nz,.movepillar
    
    
    ;check below object and see if we stand on a yellow pillar
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 177
    jp z,.kill
    cp 178
    jp z,.kill
    cp 179
    jp z,.kill
    cp 180
    jp z,.kill    
    cp 164
    ret c
    cp 168
    ret nc
    
    
    ;activate the pillar
    ld a,1
    ld (pillarspeed),a
    
    
    
    ret

    
    
    
    
.init:
;we are using memory which is not cleared on mapload and thus terrible events can ensue.

    xor a
    ld (pillarspeed),a ;initialize memory
    ld de,0
    ld (pillary),de
    inc (ix+v4)
    ret
    
    
    
;check if we reached the next checkpoint then stop    
    
.checkstop:

    ld a,(ix+objectrelease)
    dec a
    jp z,.boundary2
    dec a
    jp z,.boundary3
    dec a
    ret z

;boundary 1

    ld hl,(mariox)
    ld de,100*8
    xor a
    sbc hl,de
    jp nc,.nextboundary


    ld hl,(mariox)
    ld de,80*8
    xor a
    sbc hl,de
    ret c    
    
    inc sp
    inc sp
    
    jp .stop

;    ret



.boundary2:

;boundary 2

    ld hl,(mariox)
    ld de,276*8
    xor a
    sbc hl,de
    jp nc,.nextboundary


    ld hl,(mariox)
    ld de,256*8
    xor a
    sbc hl,de
    ret c    
    
    inc sp
    inc sp
    
    jp .stop

;    ret

.boundary3:

;boundary 3

    ld hl,(mariox)
    ld de,470*8
    xor a
    sbc hl,de
    jp nc,.nextboundary


    ld hl,(mariox)
    ld de,450*8
    xor a
    sbc hl,de
    ret c    
    
    inc sp
    inc sp
    
    jp .stop

;    ret


    
.nextboundary:

    inc (ix+objectrelease)
    ret
    
    
    
.kill:

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a



    ret
    
.handlemariolava:

    cp 119
    jp z,.handlemariolava119
    cp 131
    jp z,.handlespikes
    
    
    call followmario

    ;check below object and see if we hit lava
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 177
    jp z,.kill
    cp 178
    jp z,.kill
    cp 179
    jp z,.kill
    cp 180
    jp z,.kill 
    
    ret


.handlemariolava119:

    call followmario

    ;check below object and see if we hit lava
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 96
    jp z,.kill
    cp 97
    jp z,.kill
    cp 98
    jp z,.kill
    cp 99
    jp z,.kill 
    
    ret

.handlemariolava130131:


    call followmario

    ;check below object and see if we hit lava
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 175
    jp z,.kill
    cp 176
    jp z,.kill
    cp 177
    jp z,.kill
    cp 178
    jp z,.kill 
    
    ret


    
.handlespikes:    


    call followmario

    ;check below object and see if we hit lava
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 19
    jp z,.kill
    cp 20
    jp z,.kill
    cp 21
    jp z,.kill
    cp 22
    jp z,.kill 
    
    ret
    
    
.movepillar:

    ;check below object and see if we hit lava
    ld de,0
    ld bc,32+1
    call objecttilecheckfromrom
    cp 177
    jp z,.kill
    cp 178
    jp z,.kill
    cp 179
    jp z,.kill
    cp 180
    jp z,.kill 



    ld de,(pillary)
    ld hl,150
    xor a
    sbc hl,de
    jp c,.stop ;pillar is fully down reset the code

    ld a,(framecounter)
    and 1
    ret nz ;slow movement down


    ld e,(ix+v7)
    ld d,0
    ld hl,.pillarspeedtable
    add hl,de
    ld a,(hl)
    cp 3
    jp z,.maxspeed
    
    inc (ix+v7)
    ld e,a
    
    ld hl,(pillary)
    add hl,de
    ld (pillary),hl
   
    
    
    ret


    

.maxspeed:

    ld hl,(pillary)
    inc hl
 ;   inc hl
    ld (pillary),hl
   
    
    ret
    
.stop:


    ld (ix+v7),0
    xor a
    ld hl,0
    ld (pillarspeed),a
    ld (pillary),hl
    
    ret
    

.pillarspeedtable: db 0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,1,3;,0,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,3  ;uncomment inchl in maxspeed and remove the 3 to make it move faster  
    
    
  
  
animateyellowpillartiles2:
  

  ld    a,level19animationsblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 
    ld a,(pillary)
    and 7
  
    exx

  cp 0
  ld hl,animationlvchr19+(8*119)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*119)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2   
  cp 1
  ld hl,animationlvchr19+(8*102)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*102)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2 
  cp 2
  ld hl,animationlvchr19+(8*85)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*85)             ;rotating tile animation 1 character address in ro  
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 3
  ld hl,animationlvchr19+(8*68)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*68)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 4
  ld hl,animationlvchr19+(8*51)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*51)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 5
  ld hl,animationlvchr19+(8*34)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*34)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 6
  ld hl,animationlvchr19+(8*17)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*17)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 7
  ld hl,animationlvchr19+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a


updateyellowpillartiles2:
  
  exx 

  call getvrampageadressyellow2
;  jp nz,setpage12tohandleobjectmovement
  


  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix136
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix136

  call setpage12tohandleobjectmovement

  ret

;/=================================================================================================


getvrampageadressyellow2:


;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp z,.step1
  jp .step2

;character table at $4000
.step2:
  ld    hl,$4000+160*8
  ret

;character table at $0000
.step1:
  ld    hl,$0000+160*8
  ret
  
  
