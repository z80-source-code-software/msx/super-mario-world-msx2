;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement9:

;start = 295


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz

  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress9 - 295 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)

;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress9
;  add   hl,de
;  ;substract minimum base adres
;  ld de,295*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress9:

  jp    MovementPattern295     ;maplevel04-06b (helper object)
  jp    MovementPattern296     ;switchbox for maplevel04-06b
  jp    MovementPattern297     ;ludwig (boss)
  jp    MovementPattern298     ;ludwig's fireball
  jp    MovementPattern299     ;ludwig as shell
  jp    MovementPattern300     ;wiggler
  jp    MovementPattern301     ;wiggler (helper objects)
  jp    MovementPattern302     ;wiggler (dying)
  jp    MovementPattern303     ;exploding smiley box (maplevel4-01 specific)
  jp    MovementPattern304     ;seaurchin (all forms)
  jp    MovementPattern305     ;exploding bubble
  jp    MovementPattern306     ;fishin lakitu
  jp    MovementPattern307     ;lakitu cloud
  jp    MovementPattern308     ;color changer (maplevel5-04a)
  jp    MovementPattern309     ;illusion secret1 moving platform
  jp    MovementPattern310     ;iggy platform coverup
  jp    MovementPattern311     ;blocktileanimator2
  jp    MovementPattern312     ;circlesaw
  jp    MovementPattern313     ;circlesaw (inteligent)
  jp    MovementPattern314     ;roys castle moving platform
  jp    MovementPattern315     ;roys castle moving platform (backpart)
  jp    MovementPattern316     ;roys castle spike mario killer
  jp    MovementPattern317     ;stonekoopa
  jp    MovementPattern318     ;water coverupsprite
  jp    MovementPattern319     ;dino rhino (big)
  jp    MovementPattern320     ;dino rhino (small)
  jp    MovementPattern321     ;sheerplatform (left and right)
  jp    MovementPattern322     ;special exit (maplevel6-04b)
  jp    MovementPattern323     ;empty
  jp    MovementPattern324     ;spikey (static)
  jp    MovementPattern325     ;fish (bouncing on ground)
  jp    MovementPattern326     ;ghosthole
  jp    MovementPattern327     ;fishin boo
  jp    MovementPattern328     ;block boo
  jp    MovementPattern329     ;snake boo
;  jp    MovementPattern330     ;special yellow pillar (maplevel6-07c)

;out of ROM. Continue to next ROM block...

.return:
  
  ret

/*  
MovementPattern330:     ;special yellow pillar (maplevel6-07c)  
  
    call followmario
  
  
  
  
  ret
  
  
animateyellowpillartiles2:
  

  ld    a,level19animationsblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 
    ld a,(pillary)
    and 7
  
    exx

  cp 0
  ld hl,animationlvchr19+(8*119)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*119)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2   
  cp 1
  ld hl,animationlvchr19+(8*102)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*102)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2 
  cp 2
  ld hl,animationlvchr19+(8*85)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*85)             ;rotating tile animation 1 character address in ro  
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 3
  ld hl,animationlvchr19+(8*68)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*68)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 4
  ld hl,animationlvchr19+(8*51)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*51)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 5
  ld hl,animationlvchr19+(8*34)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*34)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 6
  ld hl,animationlvchr19+(8*17)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*17)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles2
  cp 7
  ld hl,animationlvchr19+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a


updateyellowpillartiles2:
  
  exx 

  call getvrampageadressyellow2
;  jp nz,setpage12tohandleobjectmovement
  


  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix136
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix136

  call setpage12tohandleobjectmovement

  ret

;/=================================================================================================


getvrampageadressyellow2:


;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp z,.step1
  jp .step2

;character table at $4000
.step2:
  ld    hl,$4000+160*8
  ret

;character table at $0000
.step1:
  ld    hl,$0000+160*8
  ret
  
*/  
  
MovementPattern329:     ;snake boo  
;snake boo is simple. We check v7 and after counting to 16 or less (we will see how it looks) we spawn a new random snakeboo in position which will count to a certain number and destroy itself
;Like that we can never spawn too many snakeboo's problem is that the damn MSX can only handle 32 sprites!! :(
  call mariointeract ;make it deadly


    ld a,(ix+v4)
    and a
    jp nz,.handletail



    inc (ix+v7)
    ld a,(ix+v7)
    cp 4
    call nc,.spawnboo


;works by looking at the v1 and v2 directions
  ld a,(framecounter)
  and 3
  call z,.animate

;call movement
  call addv1toxbject
  call addv2toybject


  ld bc,8
  ld de,10
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1

  ld bc,8
  ld de,-8
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1


  ld bc,0
  ld de,1
  call objecttilecheckfromrom
  cp 192
  call nc,.negatev2

  ld bc,16
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev2

  ld bc,0
  ld de,10
  call objecttilecheckfromrom
  cp 192
  call nc,.negatev1

  ld bc,0
  ld de,-8
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1


  ret

.negatev1:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ret

.negatev2:

  ld a,(ix+v2)
  neg
  ld (ix+v2),a
  ret


.handletail:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c
    
    jp destroyobject
    

    ;ret

  
  
  
.spawnboo:

    ld (ix+v7),0  ;reset timer

    

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; dec de
;  inc de


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,326
  
  ld    (iy+5),l                ;set object number ()
  ld    (iy+6),h                ;set object number ()

  call  activateobject
  
  
  inc (ix+v4)
  
  pop iy
  push iy
  
  ld l,(iy+xobject)
  ld h,(iy+xobject+1)
  ld e,(iy+yobject)
  ld d,(iy+yobject+1)
;copy calling objects coordinates exactly into newly spawned ghost  
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),e
  ld (ix+yobject+1),d  
  
  
  call .setsprite
  
  
  
  pop ix
  
  
  ret
  
;set a random sprite for the spawned object  
  
.setsprite:  
  
  ld a,(iy+v1)
  bit 7,a
  jp z,.setspriteright
  
  call makerandomnumber ;much better random
;  ld a,r
  and 7
  cp 1
  jp z,.type1
  cp 2
  jp z,.type2
  cp 3
  jp z,.type3
  cp 4
  jp z,.type4
  cp 5
  jp z,.type5
  cp 6
  jp z,.type6
  
  jp .type1
  
  
.setspriteright:

  call makerandomnumber ;much better random
;  ld a,r
  and 7
  cp 1
  jp z,.type1r
  cp 2
  jp z,.type2r
  cp 3
  jp z,.type3r
  cp 4
  jp z,.type4r
  cp 5
  jp z,.type5r
  cp 6
  jp z,.type6r
  
  jp .type1r
  
  
  
.type1:  

  ld    hl,coloraddressghost1left1  ;color address ;03
  ld    bc,characteraddressghost1left1  ;character address


  jp .setspritetype

.type2:

  ld    hl,coloraddressghost1left2  ;color address ;03
  ld    bc,characteraddressghost1left2  ;character address

  jp .setspritetype
  
.type3:  

  ld    hl,coloraddressghost2left1  ;color address ;03
  ld    bc,characteraddressghost2left1  ;character address

  jp .setspritetype

.type4:  

  ld    hl,coloraddressghost2left2  ;color address ;03
  ld    bc,characteraddressghost2left2  ;character address

  jp .setspritetype

.type5: 

  ld    hl,coloraddressghost3left1  ;color address ;03
  ld    bc,characteraddressghost3left1  ;character address

  jp .setspritetype
 
.type6:    
 
 
  ld    hl,coloraddressghost3left2  ;color address ;03
  ld    bc,characteraddressghost3left2  ;character address   

  jp .setspritetype

.type1r:  

  ld    hl,coloraddressghost1right1  ;color address ;03
  ld    bc,characteraddressghost1right1  ;character address


  jp .setspritetype

.type2r:

  ld    hl,coloraddressghost1right2  ;color address ;03
  ld    bc,characteraddressghost1right2  ;character address

  jp .setspritetype
  
.type3r:  

  ld    hl,coloraddressghost2right1  ;color address ;03
  ld    bc,characteraddressghost2right1  ;character address

  jp .setspritetype

.type4r:  

  ld    hl,coloraddressghost2right2  ;color address ;03
  ld    bc,characteraddressghost2right2  ;character address

  jp .setspritetype

.type5r: 

  ld    hl,coloraddressghost3right1  ;color address ;03
  ld    bc,characteraddressghost3right1  ;character address

  jp .setspritetype
 
.type6r:    
 
 
  ld    hl,coloraddressghost3right2  ;color address ;03
  ld    bc,characteraddressghost3right2  ;character address     
  
  
.setspritetype:  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 



;  ret  
  

.animate:


    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

    ld a,(ix+v1)
    bit 7,a
    jp z,.position1

.position0: ;left

  ld    hl,spriteaddrex432  ;color address ;03
  ld    bc,spriteaddrex432c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

.position1: ;right
    
  ld    hl,spriteaddrex438  ;color address ;03
  ld    bc,spriteaddrex438c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
;ret

  
MovementPattern328:     ;block boo  

;why platform interact. Because the blocks can deadstop in between tiles. Thus we cannot lock against a bkg tile.
;we could try to stop against a tilelock making the code more complex. however more solid as well. The danger then
;is the offload of the object which is a pain in the ass. Thus we choose for object interaction which can be solid too.


;script is written morrowind style. Meaning that each action directly reacts upon variable change instead of stepwise jumps per frame. risky but sometimes effective
;it eats alot of clockcycles but the hell with that. The engine isn' t really fast anyway. Anyone help? HAHAHAHAHAHA yeah they will.
;Again credits to Grauw for being that one awesome person to help us optimise the most terrible parts of our code. Thank you.

    ld c,0
    call platforminteract

    ld a,(ix+v4)
    and a
    call z,.mariointeract
    

;use v4 as the action variable. 
;0 = idle state
;1 = move left
;2 = move right
;3 = stop

;v7 is the timer and pointer
;1 and 2 use v7 as pointer for the acc table
;3 use v7 as the sprite change timer

;use hitpoints as the Y acc pointer

    
   call .setaccelerate 
   call .accelerate
   call .checkstop 
   call .setstop
   call .doy ;move the y coordinates 
    
    call getmariodistance
    cp 40
    call nc,.checkstopspecial
    
    
  call addv1toxbject
  call addv2toybject

  

;  ld a,(framecounter)
;  and 3
;  jp z,.animate

 jp .animate 
  
  
  ret

.mariointeract:
;handle special interactions when we are a box

    call mariointeract
    call c,.popbox

    ret



;stolen from myself.  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c


  ld a,1
  ld (jumpingupsprite),a
 
  ret  
  
  
  
.checkstopspecial:
;in any case of heavy code faliure check if mario is looking at the back of the boo
    ld a,(ix+v4)
    and a
    ret z
    dec a
    jp z,.checkstopspecialtoleft
    dec a
    jp z,.checkstopspecialtoright
    cp 3
    ret z

    

    ret

.checkstopspecialtoleft:


    call getmarioleftright
    ret nc ;do nothing everything is just fine
    

    ld (ix+v4),3 ;initate stop
    ld (ix+v7),0 ;initate stop    
    
    jp .setstop

;    ret

    
.checkstopspecialtoright:

    call getmarioleftright
    ret c ;do nothing everything is just fine
    
    ld (ix+v4),3 ;initate stop
    ld (ix+v7),0 ;initate stop    
    
    
    jp .setstop

;    ret
    
    
   
   

  
.doy:


   ld a,(ix+v4)
   and a
   ret z
   cp 3
   ret z ;only when moving

  ld a,(framecounter)
  and 3
  ret nz
   
    call getmarioheighty16
    cp 128
    call nc,.setgodown
   
   
    ld e,(ix+hitpoints)
    ld d,0
    ld hl,.ymovementtable
    add hl,de
    ld a,(hl)
    cp 15
    jp z,.resetypointer
    
    inc (ix+hitpoints)
    
    ld (ix+v2),a

    ret
   

.resetypointer:

    ld (ix+hitpoints),0
    
    ret



.setgodown:


    ld (ix+hitpoints),44
    

    ret
    
    
    
  ;looking good but now compensate for the mariodistance  
    
    
.ymovementtable: db 0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,-1,0,-1,-1,-1,-2,-2,-2,-2,-3,-3,-2,-2,-2,-2,-1,-1,-1,0,-1,0,-1,0,1,0,1,0,1,1,1,2,2,2,2,2,3,3,2,2,2,2,1,1,1,0,1,0,1,0,15  
  
.setstop:

    ld a,(ix+v4)
    cp 3
    ret nz
    ;only respond to number 3
    
    ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+deadly),3
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 7
    ret c ;allow the sprites to be updated
    
    
    
    ld (ix+v4),0
    ld (ix+v7),0



    ret
   
  
  
.accelerate:

    ld a,(ix+v4)
    and a
    ret z ;no action
    cp 3
    ret z ; no action

    ld e,(ix+v7)
    ld d,0
    ld hl,.ghostacctable
    add hl,de
    ld a,(hl)
    cp 5
    jp z,.setmaxspeed
    
    inc (ix+v7)
    
    ld c,a ;temp push
    ld a,(ix+v4)
    cp 2
    ld a,c
    call nz,.negate
    
    ld (ix+v1),a

    ret

.negate:
;negate speed

    neg
    
    ret
    
.setmaxspeed:

    ld a,(ix+v4)
    cp 2
    jp z,.setmaxspeedright


    ld (ix+v1),-1
    ret
    
.setmaxspeedright:

    ld (ix+v1),1
    ret

  
  
.checkstop:
;check if mario is looking at the ghost and initiate the stop code

    ld a,(ix+v4)
    and a
    ret z ;no checkstop since we are not in movement at all
    dec a
    dec a
    jp z,.checkstopright
    dec a
    ret z
    
    ld a,(marioleftrightspr)
    and a
    ret z    
    
    ld (ix+v4),3 ;initate stop
    ld (ix+v7),0 ;initate stop
 ;   ld (ix+hitpoints),0 ;initate stop


    ret
  

.checkstopright:

    ld a,(marioleftrightspr)
    and a
    ret nz    
    
    ld (ix+v4),3 ;initate stop
    ld (ix+v7),0 ;initate stop
 ;   ld (ix+hitpoints),0 ;initate stop
    
    
    ret
  
  
  

;check the distance of mario then check direction and begin movement 
.setaccelerate:

    ld a,(ix+v4)
    and a
    ret nz ;no action if another action was previously set


;get all round distance
    call getmariodistancexy
    cp 80
    ret c ;too close no action required yet
    
    call getmarioleftright ;is mario standing left or right from the object?
    jp c,.handleright
    
    ld a,(marioleftrightspr)
    and a
    ret nz

    ;set movement to left here
    ld (ix+v4),1
    ld (ix+v3),0
    ld (ix+v7),0
    ld (ix+hitpoints),0 ;initate y pointer
 
 
    ret

    
    

.handleright:


    ld a,(marioleftrightspr)
    and a
    ret z

    ;set movement to right here
    ld (ix+v4),2
    ld (ix+v3),1 ;tell sprite engine whats going on    
    ld (ix+v7),0
    ld (ix+hitpoints),0 ;initate y pointer

 
    ret
 
 
 
  

.animate:


;most dangerous action off all is to create dealy through the animation engine but I dont care


    ld a,(ix+v4)
    and a
    ret z ;no action

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;  ld a,(ix+v4)
  dec a
  jp z,.position0 ;left
  dec a
  jp z,.position1 ;right
  dec a
;  jp z,.position2 ;left affraid
;  dec a
;  jp z,.position3 ;right affraid
;  dec a
  jp z,.position4 ;block
  
  
  ret

.position0:

    ld a,(ix+v7)
    cp 15
    jp c,.position2


  ld    hl,spriteaddrex432  ;color address ;03
  ld    bc,spriteaddrex432c  ;character address
  ld    de,offsetplatform 
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

.position1:

    ld a,(ix+v7)
    cp 15
    jp c,.position2

    ld (ix+deadly),2
    
    
  ld    hl,spriteaddrex438  ;color address ;03
  ld    bc,spriteaddrex438c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
.position2:

    ld a,(ix+v3)
    and a
    jp z,.position3

    ld (ix+deadly),2
    
    
  ld    hl,coloraddressblockboo1  ;color address ;03
  ld    bc,characteraddressblockboo1  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset   

.position3:

  ld    hl,coloraddressblockboo2  ;color address ;03
  ld    bc,characteraddressblockboo2  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.position4:

    ld a,(ix+v7)
    cp 4
    jp c,.position2


  ld    hl,coloraddressblockboo  ;color address ;03
  ld    bc,characteraddressblockboo  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 
  
  
  

.ghostacctable: db 0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,1,1,0,1,1,0,1,5

 

  
  

;randomly animate left and right alternate between position1 and 2 for both sides  
animatefishinboo:


    ld a,(framecounter)
    and 3
    ret nz
    
    
    ld e,(ix+v4)
    ld d,0
    ld hl,.movementtable
    add hl,de
    ld a,(hl)
    cp 15
    jp z,.resettablecounter

    inc (ix+v4)
    

 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  
;  ld    a,(ix+v3)               ;v3 is used as animation variable
  and a
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  dec   a
  jp    z,.position6
  dec   a
  jp    z,.position7  
  
  
.position0:
  ld    hl,coloraddressfishinboo  ;color address
  ld    bc,characteraddressfishinboo    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    ld (ix+offsets+40),0
    ret
  
  
.position1:
  ld    hl,coloraddressfishinboo1  ;color address
  ld    bc,characteraddressfishinboo1    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset
    ld (ix+offsets+40),0
    ret
  
.position2:
  ld    hl,coloraddressfishinboo2  ;color address
  ld    bc,characteraddressfishinboo2    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset
    ld (ix+offsets+40),5
    ret
    
.position3:
  ld    hl,coloraddressfishinboo3  ;color address
  ld    bc,characteraddressfishinboo3    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset  
    ld (ix+offsets+40),5
    ret
    
.position4:
  ld    hl,coloraddressfishinboo4  ;color address
  ld    bc,characteraddressfishinboo4    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    ld (ix+offsets+40),32
    ret
    
.position5:
  ld    hl,coloraddressfishinboo5  ;color address
  ld    bc,characteraddressfishinboo5    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset
    ld (ix+offsets+40),32
    ret
    
.position6:
  ld    hl,coloraddressfishinboo6  ;color address
  ld    bc,characteraddressfishinboo6    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset
    ld (ix+offsets+40),32-5
    ret
    
.position7:
  ld    hl,coloraddressfishinboo7  ;color address
  ld    bc,characteraddressfishinboo7    ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  call    setcharcolandoffset   
    ld (ix+offsets+40),32-5
    ret
    
  

    ;ret

    
  
  
  
  
.resettablecounter:

    ld (ix+v4),0


    ret
  
  
.movementtable: db 0,1,0,1,2,3,2,3,0,1,0,1,2,3,4,5,4,5,6,7,4,5,15 
 
 
 
 
  
MovementPattern327:     ;fishin boo  

    
    call mariointeract

    ld a,(ix+hitpoints)
    and a
    jp nz,.animateflame;the flame must be handled separately
    


  ld a,(ix+objectrelease)
  and a
  jp z,.init
  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get foreign object

  
  call addv1toxbject
  call addv2toybject

  call acceleratelakitu ;lucky the lakitu is in the same rom position :)
  call .setymovement
  
  call animatefishinboo ;return x offset in de for foreign object

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
    ld e,(ix+offsets+40)
    ld d,0
  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,96
  add hl,de  
  ld (iy+yobject),l
  ld (iy+yobject+1),h  




  ret

  
  
.animateflame:

    ld a,(framecounter)
    and 3
    ret nz

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,coloraddressblueflame  ;color address
  ld    bc,characteraddressblueflame   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressblueflame1  ;color address
  ld    bc,characteraddressblueflame1   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  

    ;ret
  
  
  
.setgodown:  
  
  ld (ix+offsets+38),2  
  
  ret
  
.setgoup:

    ld (ix+offsets+38),40


    ret
  


.setymovement:

;some gimmick code to make this guy look more inteligent

    call getmarioheight;y16 ;use the 8 bit check which is a little bit safer I think
    cp 40
    call c,.setgoup ;go up if mario gets too close in height
    cp 128
    call nc,.setgodown
    


  ld e,(ix+offsets+38)
  ld d,0
  ld hl,.ymovementtable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.resettablecounter

  ld (ix+v2),a
  inc (ix+offsets+38)

  ret

.resettablecounter:

  ld (ix+v2),0
  ld (ix+offsets+38),0

  ret


.ymovementtable:  db 0,0,1,0,0,1,0,0,1,0,1,0,1,1,1,2,1,2,1,2,1,2,2,2,1,2,1,1,2,1,1,0,1,0,1,0,0,1,0,0,-1,0,-1,0,-1,-1,-2,-1,-1,-2,-1,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,5

  
  
  
;spawn the flame
.init:

  inc (ix+v7)
  ld a,(ix+v7) ;cheap bug fix giving eventhandler time to spawn during init
  cp 5
  ret c

;spawn the inner object

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de


  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

;grab object and stop all its code until release  
  ld hl,327
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld (ix+deadly),2
  ld (ix+clipping),0
    inc (ix+hitpoints)  

  ld    hl,coloraddressblueflame  ;color address
  ld    bc,characteraddressblueflame  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix
  
  ld (ix+v5),l
  ld (ix+v5+1),h
    
    inc (ix+objectrelease)
    ret

  
  
  
  
MovementPattern326:     ;ghosthole  

    ld l,(ix+v5)
    ld h,(ix+v5+1)
    push hl
    pop iy ;fetch foreign object
    
;because this object writes tiles into the map it is VERY important to offload it properly
    ld a,(ix+v3)
    and a
    jp nz,.handleright ;handle from central object this includes the movement however check if left object is out of screen


    ld a,(ix+v4)
    and a
    jp z,.init

    ld a,(iy) ;check right object if gone
    jp z,.destroyself    
    
    
    call addv1toxbject ;move left or right
    
    call .checktimer ;see how long we have been moving to th left/right and change direction
    
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,6*8
    add hl,de
    ld (iy+xobject),l
    ld (iy+xobject+1),h
    
    
;handlemoevemtn to left
    ;get mapadress
   
    
  ld bc,8 
  ld de,0;-8
  ld a,(ix+v1)
  bit 7,a ;going right?
  call z,.setmoveright
  call objecttilecheckfromrom

  push hl
  
;write bkg into place    
    xor a
    call writeanytile
    ld de,(maplenght)
    add hl,de
    call writeanytile
  
    pop hl
 

 
    ld de,6
    add hl,de
    ld a,(ix+v1)
  bit 7,a ;going right?
  call z,.sbcmoveright    
    
    
;write back last tile    
    ld a,(ix+hitpoints) ;PROBLEM: tiles are irregular and thus we need to work with a small tilebuffer which we read on each removal
    call writeanytile
    ld de,(maplenght)
    add hl,de
    ld a,7
    call writeanytile
    
    dec (ix+hitpoints)
    ld a,(ix+hitpoints)
    cp 234
    call c,.resettilecounter
    
    ld (iy+hitpoints),a ;pass over to foreign object
  
  
  ret
    
  
  
  
.sbcmoveright:

    ld de,6+6
    xor a
    sbc hl,de
    ret

  
  
.setmoveright:

    ld de,6*8




    ret
  
  
.checktimer:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 60
    ret c
    
    ld (ix+v7),0
    ld a,(ix+v1)
    neg
    ld (ix+v1),a
    
    ret
    
  
  
  
.resettilecounter:  
 
 
    ld (ix+hitpoints),236
    ret
 
  
  
  
.handleright:
  
    ld a,(iy) ;active??
    and a
    jp z,.destroyobjectleft



    ret

    
.destroyself:

;get mapadress to write bkg back
  ld bc,8 
  ld de,-8
  call objecttilecheckfromrom
    
    ld (ix+hitpoints),a
  

  ld b,10
    
  jp .restorebkgtileloop
  
  
    
;last tile to the far left and allign restore code to that    

    
.destroyobjectleft:

;get mapadress to write bkg back
  ld bc,8 
  ld de,-8*8
  call objecttilecheckfromrom
    
    ld (ix+hitpoints),a
  

  ld b,10
  
.restorebkgtileloop:
  
  
    push hl
;write back last tile    
    ld a,(ix+hitpoints)
    call writeanytile
    ld de,(maplenght)
    add hl,de
    ld a,7
    call writeanytile
    
    dec (ix+hitpoints)
    ld a,(ix+hitpoints)
    cp 234
    call c,.resettilecounter  
  
    pop hl
    inc hl
  
    djnz .restorebkgtileloop




    jp destroyobject
    
    
    
  
.init:

;create gap in tiles
  ld bc,8 
  ld de,0;-8
  call objecttilecheckfromrom

  
  ld b,7

  
.createzeroloop:  
  
  push hl
  
;write bkg into place    
    xor a
    call writeanytile
    ld de,(maplenght)
    add hl,de
    call writeanytile
  
    pop hl
    inc hl
    
    djnz .createzeroloop
    

;spawn right part    

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  
  ld de,8-2
  add hl,de
  
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
    inc de
  ;  inc de
  ;  dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  
  ld hl,322
  
  ld    (iy+5),l                 ;set object number
  ld    (iy+6),h                ;set object number
  
  
  call  activateobject
  
  inc (ix+v3)
    ld (ix+hitpoints),234 ;initialize this value
  
;parse ix address into object and vice versa
    pop hl
    push hl
    ld (ix+v5),l
    ld (ix+v5+1),h
  
  

;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressghosthole1  ;color address
  ld    bc,characteraddressghosthole1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
  
  push ix
  pop hl
  
  pop ix  
  
    ld (ix+v5),l
    ld (ix+v5+1),h 
  

    inc (ix+v4)
    ld (ix+hitpoints),236 ;starting tile
  
  
    ret
  
  
  
  
handlefishspecial:

;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe



    call addv1toxbject

    ld a,(framecounter)
    and 3
    call z,.animate
    
    call checkobjecttilescheckwall
    ret nc
    

 ;   call .setjump
;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

    



;    ret


.animate: 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressfish1left  ;color address
  ld    bc,characteraddressfish1left   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressfish2left  ;color address
  ld    bc,characteraddressfish2left   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset    


.position2:
  ld    hl,coloraddressfish1right  ;color address
  ld    bc,characteraddressfish1right  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset 

.position3:
  ld    hl,coloraddressfish2right  ;color address
  ld    bc,characteraddressfish2right   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset     

    ;ret

    
;its a little complicated because mario has different variable usage in the water.
;to prevent too much tampering on the mariointeract routine we simply take care of the hits via
;this route
.nextframe:

  ld a,(marioswim)
  dec a
  jp z,.setkillmario
  
  jp otherobjectdeathjump
  
  
.setkillmario:


  ld a,(mariogod)
  and a
  ret nz

  ld a,(marioyoshi)
  dec a
  jp z,handleyoshihit


  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell

  ret    
  
  
  
  
MovementPattern325:     ;fish (bouncing on ground)  
;From this point on I am seriously starting to believe that the original smw makers have made the game complex on purpose so that no port could ever be made
;anyway when this gut releases from the bubble it bounces on the ground towards mario

    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    dec a
    call z,.jump
    dec a
    jp z,handlefishspecial ;swimming mode

;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,otherobjectdeathjump    
    
    
    call addv1toxbject
    call addv2toybject
    

    ld a,(framecounter)
    and 3
    call z,.animate

    xor a
    ld (ix+v2),a

  ld bc,-8
  ld de,0
  call objecttilecheckfromrom
  cp 104
  jp nc,.setswim    
    
    
    call checkobjecttilesfloor
    ret c
  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 3
    call nc,incv4
    
    call .checkmarioleftright

;we do not do extra wall checks. Its a little bit risky but fuck it anyway    
    ret
    
    
.setswim:  
    
  cp 106
  ret nc
  
  ld (ix+v4),3 ;set to swimming mode
  
  ld (ix+v1),-1
  ld (ix+v3),0
  
  
  
  ret


.checkmarioleftright:

    call getmariodistance ;check distance and react
    ld a,c
    cp 40
    ret c

    call getmarioleftright
    jp c,.handleright

    ;check our direction then follow mario
    
    ld a,(ix+v1)
    bit 7,a
    ret nz
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a
    


    ret
  
.handleright:

    ld a,(ix+v1)
    bit 7,a
    ret z
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a

    



    ret  
  
  
  

.jump:

    ld hl,.jumptable
    ld e,(ix+hitpoints)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.endjump
    
    ld (ix+v2),a
    
    inc (ix+hitpoints)

    ret

    
.endjump:

    ld (ix+v4),1
    ld (ix+hitpoints),0

    ret
    
    
;.jumptable: db -4,-3,-3,-2,-2,-2,-1,-1,-1,-1,0    
.jumptable: db -2,-2,-1,-1,0 
  
  
  
.animate:


 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressfishjump  ;color address
  ld    bc,characteraddressfishjump   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressfishjump1  ;color address
  ld    bc,characteraddressfishjump1   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position2:
  ld    hl,coloraddressfishjump2  ;color address
  ld    bc,characteraddressfishjump2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.position3:
  ld    hl,coloraddressfishjump3  ;color address
  ld    bc,characteraddressfishjump3   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset     

    ;ret
  
  
  
  
  
.init:

    inc (ix+v4)

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    
    call getmarioleftright
    jp c,.setright

    ld (ix+v1),-1
    ld (ix+v3),0
    
    
  
  ld    hl,coloraddressfishjump  ;color address
  ld    bc,characteraddressfishjump   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 




  ;  ret

    
.setright:

    ld (ix+v1),1
    ld (ix+v3),2

  ld    hl,coloraddressfishjump2  ;color address
  ld    bc,characteraddressfishjump2   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 




  ;  ret
    


  
MovementPattern324:     ;spikey (static)  

    call mariointeract


  ;check down
  ld bc,25
  ld de,0
  call objecttilecheckfromrom
  cp 188
;  jp c,.activate
  jp z,otherobjectdeathjump

  
  ;check right
  ld bc,17
  ld de,17
  call objecttilecheckfromrom
  cp 191
  jp c,.activate
  
  ;check left
  ld bc,17
  ld de,-8
  call objecttilecheckfromrom
  cp 191
  ret nc

.activate:  
  
  ld hl,250
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  
  ret
  
  
  
  
MovementPattern323:     ;empty  

    ret
  
  
MovementPattern322:     ;special exit (maplevel6-04b)  
;maplevel6-04b is somewhat tricky the exit has to be super reliable and its one of those annoying elongated maps. Compromise is inevitable.
;this object follows mario around and detects the tiles of the exit tube. Lucky for us each tube is made out exactly the same tiles


  ld a,(ix+v4)
  cp 1
  jp z,.exitlevel
  
  
 call followmario
 
  ld bc,0 ;check tiles to the right
  ld de,0
  call objecttilecheckfromrom
  cp 97
  jp z,.blockmariodown
  cp 98
  jp z,.blockmariodown
  cp 99
  jp z,.blockmariodown  
 
 
  ld bc,24 ;check tiles to the right
  ld de,8
  call objecttilecheckfromrom
  cp 239
  jp z,.blockmario
  cp 30
  jp z,.blockmario  
  cp 62
  jp z,.blockmario
  cp 94
  ret nz

  ld a,(mariogod)
  and a
  call nz,.endmariogod

  ld a,(marioduck)
  and a
  ret nz
  
  ld a,(marioleftright)
  and a
  ret z
  
  ld a,(marioleftrightspr)
  and a
  ret z

  ld a,(mariospeed)
  and a
  ret z
  
  ld a,(fallspeed)
  and a
  ret nz

    jp .checkheight

;push mario back 1 tile
.blockmario:

    ld hl,(mariox)
    ld de,16
    xor a
    sbc hl,de
    ld (mariox),hl


    ret

.blockmariodown:

    ld hl,(marioy)
    ld de,16
    add hl,de
    ld (marioy),hl


    ret
    


.endmariogod:

  xor a
  ld (mariogod),a
  ret


.checkheight:

;check the tiles below mario
 ; call getmarioheight
 ; cp 1
 ; ret nc


;force mario to duck if on yoshi
  ld a,(marioyoshi)
  dec a
  call z,.forceduck



  ld a,4 ;nocontrol mode 4 means we exit through a horizontal tube
  ld (nocontrol),a

  inc (ix+v4)
  ld (ix+v7),0

  call Playsfx.pipe ;play exit sound

  ret



.exitlevel:


  ld hl,(mariox)
  inc hl
  ld (mariox),hl

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17
  ret c

  inc (ix+v3)
  ld a,(ix+v3)
  cp 10
  ret c
    
  
  xor a
  ld (fallspeed),a



.endlevel:

  call disablescreen

  ld a,1
  ld (levelend?),a

  xor a
  ld (marioduck),a

  ld hl,(mariox)
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  srl h
  rr l ;/16 ;make it small so it fits in one byte easier for us
  ld a,l
  ld (exitx),a


  ret

.forceduck:

  ld a,1
  ld (marioduck),a
 
 
 	ld		a,(VDP_8+15)
  ld    b,a
 
  
  ld a,b
  sub 6
  ld b,a
  
  
    ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a


  ret

 

  
  
  
  
makesheerplatform:


  ld a,(ix+v5)
  and a
  jp z,.sety

  call mariointeract
  ret nc


  ld a,specialsheertilesblock
  call block12
  
;first obtain the xcoordinate of mario compared to the object he is standing on  
  ld hl,sheerselecttable
  ld a,(ix+objectrelease)  
  add a,a ;*2
  ld e,a
  ld d,0
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl
  
  push hl

  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)

  xor a
  sbc hl,de
  ld a,h ;check for minus
  and a
  
  ld d,0
  ld e,l ;maximum number is 64
  
  pop hl
  
    jp nz,setpage12tohandleobjectmovement
  
.checkylock:  
  
  ;ld hl,sheertable
  add hl,de
  ld a,(hl) ;fetch the value from table
  
  ld e,a
  ld d,0
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld bc,24
  xor a
  sbc hl,bc
  add hl,de ;add table value to absolute lock

    call setpage12tohandleobjectmovement ;get generalcode back
  
  ld de,8 ;substract base number as compensation
  xor a
  sbc hl,de

  ;lockvalue is now in hl
  ld de,(marioy)
  xor a
  sbc hl,de
  
  ld de,10 ;less tight lock.
  xor a
  sbc hl,de
  ret nc ;if distance to lock is too high return and try again next frame ;debug removal

.lockdirect:
  
;value combination set standingonsprite for platform interaction
;set specialsheertile to make it special
  ld a,1
  ld (standingonsprite),a
  ld a,(ix+objectrelease) ;type of sheertile
  ld (specialsheertile),a

  ld (spriteixvalue),ix


  call readkeydirect

  bit 7,a
  jp nz,.doreset
  bit 4,a
  jp nz,.doreset

  ld a,1
  ld (mariosheerhold),a

  ret
  
  
.doreset:

  xor a
   ld (mariosheerhold),a

  ret


.sety:


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,7
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld (ix+v5),1
  
  ret

    
  
  
  
MovementPattern321:

;make it a platform
    call makesheerplatform



    ld a,(ix+v4)
    and a
    jp z,.wait
    dec a
    jp z,.moveleft
    dec a
    jp z,.wait
    dec a   
    jp z,.moveright
    dec a
    jp z,.wait
    
    ld (ix+v4),1
    ld (ix+v7),0



    ret

.moverightright:
    
    
    ld (ix+v1),1
    ld (ix+v2),1

    jp .move
    
    
    

.moveright:

    ld a,(ix+objectrelease)
    cp 10
    jp z,.moverightright

    ld (ix+v1),1
    ld (ix+v2),-1

    jp .move

.moveleftleft:

    ld (ix+v1),-1
    ld (ix+v2),-1

    jp .move    
    
    
.moveleft:

    ld a,(ix+objectrelease)
    cp 10
    jp z,.moveleftleft

    ld (ix+v1),-1
    ld (ix+v2),1

;    jp .move    
 
.move:

    call addv1toxbject
    call addv2toybject
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 70
    ret c
    
    jp incv4
    
;    ret
  
  
  
  
.wait:

;platform related thinghy
    ld (ix+v1),0
    ld (ix+v2),0

    inc (ix+v7)
    ld a,(ix+v7)
    cp 60
    ret c
    
    jp incv4



  ;  ret
  
  
  
;remember to reset v4 after exiting this codeloop
handlerinoslide:

;handle interaction code from here
    
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

    
  call objectinteract

  cp 2
  jp z,otherobjectdeathjump

    call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    jp z,.slideleft ;first step is always a left slide 
    dec a
    jp z,.fall ;fall down and check for any floor tiles and see what happens
    dec a
    jp z,.lockandcheck
    dec a
    jp z,.slidestop
    dec a
    jp z,.slideright
    dec a
    jp z,.slidestopright    

    ret

.nextframe:

    ld (ix+objectrelease),2
    ld (ix+v4),3
    ld (ix+v7),0

    jp MovementPattern320.nextframe

    ;ret
    
.finish:

    dec (ix+objectrelease)
    ld (ix+v4),0
    ld (ix+v7),0
    ld (ix+v2),0
    ld (ix+v1),-2
    ld (ix+v3),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)



  ld    hl,coloraddressdinorhinosmall  ;color address
  ld    bc,characteraddressdinorhinosmall   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset     
    
    
   ; ret
    
    
.slidestopright:

  call  checkobjecttilesfloor
  jp nc,.stopsliderightfinal

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)    



    ret

    
.stopsliderightfinal:


    ld hl,.stopsliderightable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.finish
    
    inc (ix+v7)
    
    ld (ix+v1),a

    
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

    ret

    
.stopsliderightable:    db 3,3,3,3,3,3,3,3,2,3,2,3,2,2,2,2,1,2,1,2,1,2,1,1,1,1,1,1,0    
    
    
.slideright:



    ld hl,.rhinoslidetable2
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    cp -5
    jp z,.doneslideright
    inc (ix+v7)
    
    push af
    call .adjustsprite
    pop af
    
    neg
    ld (ix+v1),a
    ld (ix+v2),a
    

.doneslideright:

    call addv1toxbject
    call addv2toybject


  ld bc,17 ;check the floor and if reached the ledge we go to next step
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp c,.incv4   
        
    
    ret 

    
.adjustsprite:    
   
  cp -1
  ret nz
   
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)



  ld    hl,coloraddressdinorhinosmall4  ;color address
  ld    bc,characteraddressdinorhinosmall4   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

  ;ret
     
    
    

;lock to tile then check what type of tile it is    
.lockandcheck:    

  ld bc,24 ;check the floor and if reached the ledge we go to next step
  ld de,0
  call objecttilecheckfromrom
  cp 241
  jp nc,.setnextsheerslide ;HACK one time shit code    

    call addv1toxbject ;to prevent ugly stop in frame

    jp .incv4

    ;ret
    
    
.slidestop:

  call  checkobjecttilesfloor
  jp nc,.stopslidefinal

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)    



    ret

    
.stopslidefinal:


    ld hl,.stopslidetable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.finish
    
    inc (ix+v7)
    
    neg
    ld (ix+v1),a

    
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

    ret

.stopslidetable:    db 5,4,4,3,4,3,4,3,4,3,3,3,3,2,3,2,3,2,2,2,2,1,2,1,2,1,2,1,1,1,1,1,1,0 
    
.setnextsheerslide:    
   


    ld (ix+v2),8
    call addv2toybject

    call .incv4
    call .incv4

    ret
 
    
    
.fall:



  call  checkobjecttilesfloor
  jp nc,.incv4    



    call addv1toxbject
    call addv2toybject

    ret
    
    

.slideleft:


    ld hl,.rhinoslidetable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    cp -5
    jp z,.doneslide
    inc (ix+v7)
    
    
    ld (ix+v1),a
    neg
    ld (ix+v2),a
    

.doneslide:

  ld bc,17 ;check the floor and if reached the ledge we go to next step
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp c,.incv4   
    

    call addv1toxbject
    call addv2toybject
    
    
    ret    

.incv4:

    ld (ix+v7),0
    inc (ix+v4)

    ret
  
    
    
    
;lock to tile below first    
.init:

    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,8
    add hl,de
    ld (ix+yobject),l
    ld (ix+yobject+1),h
    

  ld bc,17
  ld de,0
  call objecttilecheckfromrom
  cp 191
  ret c  ;check below and see if we finally found the tile then lock onto the coordinates   

  
;hardcode the slide somewhat to save pain. It only happens on this map anyway
    ;slide left
  
  
    inc (ix+v4)
    ld (ix+v7),0

    
    
    ret
    
.rhinoslidetable:   db 0,0,0,0,0,0,0,0,0,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-2,-1,-2,-2,-2,-2,-2,-2,-3,-3,-3,-4,-3,-4,-4,-3,-4,-4,-5    
.rhinoslidetable2:  db 3,3,3,2,3,2,2,1,1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-2,-1,-2,-2,-2,-2,-2,-2,-3,-3,-3,-4,-3,-4,-4,-3,-4,-4,-5    



handlerinofireball:


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,MovementPattern320.nextframe
    
  
;  pop af
  cp 2
  jp z,otherobjectdeathjump

  
  inc (ix+v7)
  
    ld a,(ix+v7)
    cp 10
    jp z,.spawnfireball
    cp 20
    ret c
    
    
    
    ld (ix+objectrelease),2 ;turn back


    ret

.spawnfireball:

    call Playsfx.yoshifireball

    
    ld a,(ix+offsets+20)
    and 1
    jp nz,.spawnup
    
    ld a,(ix+v3)
    bit 1,a
    jp nz,.spawnright
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
    inc de
  ;  inc de
    dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  ld hl,317
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  inc (ix+v4)
    ld (ix+v1),-3
    ld (ix+deadly),2
    ld (ix+edible),0


;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressstonekoopafireball  ;color address
  ld    bc,characteraddressstonekoopafireball  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
  
  pop ix  
  
  ret

  
.spawnright:  
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
    inc de
  ;  inc de
   ; dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  ld hl,317
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  inc (ix+v4)
    ld (ix+v1),3
    ld (ix+deadly),2
    ld (ix+edible),0
    ld (ix+v3),2

;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressstonekoopafireball2  ;color address
  ld    bc,characteraddressstonekoopafireball2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
  
  pop ix  
  
  ret

.spawnup:  
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
    inc de
  ;  inc de
    dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  ld hl,317
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  inc (ix+v4)
    ld (ix+v1),0
    ld (ix+v2),-3
    ld (ix+deadly),2
    ld (ix+edible),0
    ld (ix+v3),4

;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressstonekoopafireball4  ;color address
  ld    bc,characteraddressstonekoopafireball4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
  
  pop ix  
  
  ret  
  

MovementPattern320:     ;dino rhino (small)  
 
    ld a,(ix+objectrelease)
    and a
    jp z,.init ;one time check if sliding is required
    dec a
    jp z,handlerinoslide
    dec a
    dec a
    jp z,handlerinofireball
 
 
 
    ld a,(ix+v4)
    cp 3
    jp z,.die
 
 
    ld a,(ix+deathtimer)
    cp 128
    jp z,.trowfireball
    
 
 
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   7                       ;this sprite animates once every 3 frames
  call  z,.animate

    ld a,(ix+v4)
    and a
    jp nz,.jump
  
  ;change direction when mario jumps over
  call .checkmarioleftright

    call checkmariointeractshoot
  jp c,otherobjectshotdeathjump
  
  call objectinteract
  cp 4
;  push af
  jp z,.objectwallfound
;  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
;  push af
  jp z,.objectwallfound
;  pop af


  ld (ix+v7),0 ;set timer

  ld bc,-8 ;check above object and turn if the wall is too high
  ld de,-8
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfound  
  
  
  ld a,(ix+v1)
  bit 7,a
  jp nz,.nocheck
  
  ld bc,-8 ;check above object and turn if the wall is too high
  ld de,16
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfound

.nocheck:  
  
  ld bc,0 ;check far to left to prevent hopping though short walls
  ld de,-8
  call objecttilecheckfromrom
  cp 191
  jp nc,.setjump  
  
  
  call checkobjecttilescheckwall
  jp    c,.setjump            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor


  ld bc,32 ;check below object and if these special tiles are found make a small hop
  ld de,0
  call objecttilecheckfromrom
    
  cp 240
  jp z,.setjump
  cp 241
  jp z,.setjump
  

  ret

.trowfireball:

;check for left or right then set sprite and handlecode
    ld a,(ix+v4)
    and a
    ret nz ;do not do this in mid jump
    ld a,(ix+v2)
    and a
    ret nz ;do not do this while falling down


    ld (ix+v7),0
    ld (ix+v4),0

    ld (ix+objectrelease),3 ; handle firetrow


    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
    
    ld a,(ix+v3)
    bit 1,a
    jp nz,.trowfireright

    
    ld a,r
    and 3
    jp z,.trowleftup

      ld (ix+offsets+20),0  
    
  ld    hl,coloraddressdinorhinosmall3  ;color address
  ld    bc,characteraddressdinorhinosmall3   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

  
    ;ret
  

.trowleftup:

    ld (ix+offsets+20),1


  ld    hl,coloraddressdinorhinospitup  ;color address
  ld    bc,characteraddressdinorhinospitup   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

  
    ;ret
  
.trowfireright:


    ld a,r
    and 3
    jp z,.trowrightup

      ld (ix+offsets+20),0     
    

  ld    hl,coloraddressdinorhinosmall7  ;color address
  ld    bc,characteraddressdinorhinosmall7   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 


;    ret


.trowrightup:

    ld (ix+offsets+20),1



  ld    hl,coloraddressdinorhinospitup1  ;color address
  ld    bc,characteraddressdinorhinospitup1   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 


;    ret  
  
.init:

  ld bc,17 ;check above object and turn if the wall is too high
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.startnormal     

  
  ld (ix+objectrelease),1 ;slide mode on
  
  ret
  
.startnormal:  
  
    ld (ix+objectrelease),2

    ret
  
  
.die:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c

    
    jp destroyobject

  ;  ret
  
  
.setjump:

    inc (ix+v4)
    ld (ix+hitpoints),0 ;reset pointer

    ret
    
.jump:

    ld hl,.jumptable
    ld e,(ix+hitpoints)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.endjump
    
    ld (ix+v2),a
    
    inc (ix+hitpoints)

    ret

    
.endjump:

    ld (ix+v4),0

    ret
    
    
.jumptable: db -4,-3,-3,-2,-2,-2,-1,-1,-1,-1,0    
    
  
  
.checkmarioleftright:

    call getmariodistance ;check distance and react
    ld a,c
    cp 80
    ret c

    call getmarioleftright
    jp c,.handleright

    ;check our direction then follow mario
    
    ld a,(ix+v1)
    bit 7,a
    ret nz
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a
    


    ret
  
.handleright:

    ld a,(ix+v1)
    bit 7,a
    ret z
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a

    



    ret
  
  
  

.otherobjectdeathjump:


  jp otherobjectdeathjump



;  jp otherobjectshotdeathjump

.objectwallfound:

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  jp c,.skipchecks
  call checksheerobjecttile ;check for sheer tiles
  jp c,.skipchecks ;sheertile found

  call  checkobjecttilesfloor
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

.skipchecks:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 8
  ret nc ;ignore all objects


.wallfound:

;  ld a,(ix+v2)
;  and a
;  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressdinorhinosmall  ;color address
  ld    bc,characteraddressdinorhinosmall   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressdinorhinosmall1  ;color address
  ld    bc,characteraddressdinorhinosmall1   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position2:
  ld    hl,coloraddressdinorhinosmall4  ;color address
  ld    bc,characteraddressdinorhinosmall4   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position3:
  ld    hl,coloraddressdinorhinosmall5  ;color address
  ld    bc,characteraddressdinorhinosmall5   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


    ld (ix+v4),3
    ld (ix+objectrelease),2

 ;  call removeobjectspat
 ;  call removefromsprlstskiptwo
 ;  ld (ix+amountspr),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)   
   
  ld    hl,coloraddressdinorhinosmall2  ;color address
  ld    bc,characteraddressdinorhinosmall2   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
  

  call Playsfx.kickenemy
;  call makestarleft


  call makestarleft

  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret
  

 
  
MovementPattern319:     ;dino rhino (big)  
  

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   7                       ;this sprite animates once every 3 frames
  call  z,.animate

    ld a,(ix+v4)
    and a
    jp nz,.jump
  
  ;change direction when mario jumps over
  call .checkmarioleftright

    call checkmariointeractshoot
  jp c,otherobjectshotdeathjump
  
  call objectinteract
  cp 4
;  push af
  jp z,.objectwallfound
;  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
;  push af
  jp z,.objectwallfound
;  pop af


  ld (ix+v7),0 ;set timer

  ld bc,-8 ;check above object and turn if the wall is too high
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfound  
  
  
  ld bc,-8 ;check above object and turn if the wall is too high
  ld de,16
  call objecttilecheckfromrom
  cp 191
  jp nc,.wallfound
  
  call checkobjecttilescheckwall
  jp    c,.setjump            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ld bc,32 ;check below object and if these special tiles are found make a small hop
  ld de,0
  call objecttilecheckfromrom
    
  cp 240
  jp z,.setjump
  cp 241
  jp z,.setjump
  

  ret

  
.setjump:

    inc (ix+v4)
    ld (ix+hitpoints),0 ;reset pointer

    ret
    
.jump:

    ld hl,.jumptable
    ld e,(ix+hitpoints)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.endjump
    
    ld (ix+v2),a
    
    inc (ix+hitpoints)

    ret

    
.endjump:

    ld (ix+v4),0

    ret
    
    
.jumptable: db -4,-3,-3,-2,-2,-2,-1,-1,-1,-1,0    
    
  
  
.checkmarioleftright:

    call getmariodistance ;check distance and react
    ld a,c
    cp 80
    ret c

    call getmarioleftright
    jp c,.handleright

    ;check our direction then follow mario
    
    ld a,(ix+v1)
    bit 7,a
    ret nz
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a
    


    ret
  
.handleright:

    ld a,(ix+v1)
    bit 7,a
    ret z
    
    ld a,(ix+v3)
    xor 2
    ld (ix+v3),a
    
    ld a,(ix+v1)
    neg 
    ld (ix+v1),a

    



    ret
  
  
  

.otherobjectdeathjump:


  jp otherobjectdeathjump



;  jp otherobjectshotdeathjump

.objectwallfound:

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  jp c,.skipchecks
  call checksheerobjecttile ;check for sheer tiles
  jp c,.skipchecks ;sheertile found

  call  checkobjecttilesfloor
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

.skipchecks:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 8
  ret nc ;ignore all objects


.wallfound:

;  ld a,(ix+v2)
;  and a
;  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,coloraddressdinorhino  ;color address
  ld    bc,characteraddressdinorhino   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:
  ld    hl,coloraddressdinorhino1  ;color address
  ld    bc,characteraddressdinorhino1   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position2:
  ld    hl,coloraddressdinorhino2  ;color address
  ld    bc,characteraddressdinorhino2   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.position3:
  ld    hl,coloraddressdinorhino3  ;color address
  ld    bc,characteraddressdinorhino3   ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    


.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

;TODO:fix this part

   ld l,(ix+movepat)
   ld h,(ix+movepat+1)
   inc hl
   ld (ix+movepat),l
   ld (ix+movepat+1),h


   call removefromsprlstskiptwo
   call removeobjectspat   
   ld (ix+amountspr),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)   
   
  ld    hl,coloraddressdinorhinosmall  ;color address
  ld    bc,characteraddressdinorhinosmall   ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld (ix+sizeX),16
  ld (ix+sizeY),16
  
  ld (ix+edible),1
  
  call Playsfx.kickenemy
;  call makestarleft


  call makestarleft

;adjust speed in bith directions
  ld a,(ix+v1)
    bit 7,a
    jp z,.setright
  
  ld (ix+v1),-2
  
  
  ret

.setright:

    ld (ix+v1),2
    ret
  
  
.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret
  

  
  
  

MovementPattern318:     ;watercoverupsprite  

;buffer y coordinates for the first time in order to determine the maximum
  ld a,(ix+v4)
  and a
  jp z,.savey


  ;follow mario x at ALL times
  ld hl,(mariox)
  inc hl
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
 
;do all your code stuff, animations etc from this line 
 
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    bc,1                    ;move 1 pixel to the right
  ld    de,00*64                ;DONT use the alternate version for framecounter 15,0,1,2
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    bc,5                    ;move 5 pixels to the right
  ld    de,16*64                ;use the alternate version for framecounter 3,4,5,6
  cp    7
  jr    c,.watercorrectionfound
  ld    bc,9                    ;move 9 pixels to the right
  ld    de,00*64                ;DONT use the alternate version for framecounter 7,8,9,10
  cp    11
  jr    c,.watercorrectionfound
  ld    bc,13                   ;move 13 pixels to the right
  ld    de,16*64                ;use the alternate version for framecounter 11,12,13,14
.watercorrectionfound:

  
  ld    hl,(mariox)
  add   hl,bc                   ;move bc pixels to the right

  ld    a,l
  and   %0000'1111              ;mario x last 4 bits (between 0 and 16)
  ld    l,a
  ld    h,0                     ;hl is now a number from 0 to 15 and represents the sprite we are using
  
  add   hl,hl                   ;*2
  add   hl,hl                   ;*4
  add   hl,hl                   ;*8
  add   hl,hl                   ;*16
  add   hl,hl                   ;*32
  add   hl,hl                   ;*64 (cuz every sprite is 64 bytes)

  add   hl,de                   ;take the alternate version yes or no ?
  

  ld    bc,characteraddresswatercoverupsprite1
  add   hl,bc
  push  hl
  pop   bc
  ld    hl,coloraddresswatercoverupsprite1




  ld    de,offsetwater
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
 
 
 

  ret

.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  dec hl
  dec hl
  dec hl
  dec hl
 
 
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
;hiero kker
;TODO: make a alternate sprite set and define the maximum amount of sprites here!
  ld a,(currentlevel)
  cp 64
  jp z,.bigsprite
  cp 107
  jp z,.bigsprite
  
  ret

  
.bigsprite:  
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 inc de
 inc de
 inc de
 inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),180                ;set object number ;blockwallmap 0 sprites set them after activation
  ld    (iy+6),0                ;set object number

  call  activateobject

  ld hl,258
  ld (ix+movepat),l
  ld (ix+movepat+1),h


  pop ix
  
  ret  
  
  
  
;spit out fireball no init required simply count to some number then trow out fireball and reset state  
MovementPattern317:     ;stonekoopa


  call mariointeract ;bot objects deadly to mario

  ld a,(ix+v4)
  dec a
  jp z,.fireball
  
  inc (ix+objectrelease)
  ld a,(ix+objectrelease)
  cp 100
  ret c
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
    inc de
    inc de
    dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  ld hl,317
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  inc (ix+v4)
    ld (ix+v1),-2
    ld (ix+deadly),2
    ld (ix+edible),0
;    ld (ix+sizeY),16

;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressstonekoopafireball  ;color address
  ld    bc,characteraddressstonekoopafireball  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
  
  pop ix  
  
  
  
  ld (ix+objectrelease),0 ;end
  
  ret

  
  
.fireball:

    call addv1toxbject
    call addv2toybject
    
    ld a,(framecounter)
    and 3
    jp z,.animate

    ret

.animate:    
    
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

    
    
  ld    a,(ix+v3)               ;v3 is used as animation variable
  bit 1,a
  jp nz,.doright
  bit 2,a
  jp nz,.doup
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddressstonekoopafireball  ;color address
  ld    bc,characteraddressstonekoopafireball  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:

  ld    hl,coloraddressstonekoopafireball1  ;color address
  ld    bc,characteraddressstonekoopafireball1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.doright:
  
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  and 1
    jp z,.position3
 
 
.position2:
  ld    hl,coloraddressstonekoopafireball2  ;color address
  ld    bc,characteraddressstonekoopafireball2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position3:

  ld    hl,coloraddressstonekoopafireball3  ;color address
  ld    bc,characteraddressstonekoopafireball3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset   

.doup:


  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  and 1
    jp z,.position5

.position4:
  ld    hl,coloraddressstonekoopafireball4  ;color address
  ld    bc,characteraddressstonekoopafireball4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position5:

  ld    hl,coloraddressstonekoopafireball5  ;color address
  ld    bc,characteraddressstonekoopafireball5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
  
MovementPattern316:     ;roys castle spike mario killer  
  
  
  
  ;follow mario x at ALL times
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  



.killmario:

  ld hl,(marioy)
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  

  ld bc,24
  ld de,0
  call objecttilecheckfromrom
;  ld de,0
;  ld b,1
;  call checkgeneraltilefromhrom
  cp 15
  jp z,.kill
  cp 16
  jp z,.kill
  cp 21
  jp z,.kill
  cp 22
  jp z,.kill
;lava
  cp 175
  jp z,.kill
  cp 176
  jp z,.kill
  
  
  ret


.kill:

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  jp destroyyoshi

 ; ret



  
  
  
roymovementtable:

db 1,10
db 2,2
db 1,2
db 2,2
db 1,10 ;part 1
db 2,2
db 1,10 ;part 2
db 3,3
db 0,5
db 3,3
db 1,10 ;part 3
db 2,2
db 1,2
db 2,2
db 1,2
db 2,2
db 1,40 ;part 4
db 3,5
db 1,5
db 2,5
db 1,5
db 3,5
db 1,5
db 2,5
db 1,5
db 3,5
db 1,5
db 2,4
db 1,5
db 1,16 ;part 5
db 3,50 ;end


;movement type 0 = left 1 = right 2 = up 3 = down
    
  
  

MovementPattern315:     ;roys castle moving platform (backpart)


  ld c,0
  call platforminteract

;  ret ;debug
  
  ;this object should follow the next movement in the memory location. The front object stores 6 tiles in a rotating fashion this objects reads the files back and follows around.
  ld a,(ix+v4)
  and a
  ret z
  
  call handleroyplatformsecondarymovement
  
  
  
  ret 
  

handleroyplatformsecondarymovement:





    ld a,(ix+v7)
    cp 16
    jp c,.move

    ld (ix+v7),0 ;reset counter then handle the tile based stuff 

;wait about 5 cycles until we start doing the stuff for real
    call .wait
    
    
    call handleroymovementtable


;and then read the rest of the table and write the bkg back and that should be it

  ;fetch table from RAM and store movement data
  ;Limit memory usage by resetting the pointer after 25 cycles or else!!!
  ld iy,pillarbkgbuffer
  ld a,(ix+objectrelease) ;get pointer
  ;reset pointer to 0 when max = reached
  cp 25
  call nc,.resettablepointer  
  add a,a ;*2
  add a,a ;*4
  add a,a ;*6
  add a,a ;*8
  ld e,a
  ld d,0
  add iy,de ;set pointer to correct table part
  inc (ix+objectrelease) ;next position

  
  ;read mapadress from table
  ld l,(iy+2)
  ld h,(iy+3)
    
  ld a,(iy+4)
  call writeanytile
  inc hl
  ld a,(iy+5)
  call writeanytile
  dec hl
  ld de,(maplenght)
  add hl,de
  ld a,(iy+6)
  call writeanytile
  inc hl
  ld a,(iy+7)
  call writeanytile    
    


  ret
  
.resettablepointer:

    ld (ix+objectrelease),0
    xor a

    ret
  
  
.move:


  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  
  ret

  
.wait:

    ld a,(ix+offsets+28)
    cp 5
    jp c,.return
    
    ret
    
    
.return:


;write first background tiles hardcoded in from table instead of using 0
    
    ld l,(ix+offsets+10)
    ld h,(ix+offsets+11)
    inc hl
    inc hl
    ld (ix+offsets+10),l
    ld (ix+offsets+11),h   
    
 
    exx
    ld hl,.bkgtiles    
    ld c,(ix+offsets+30) ;read pointer
    ld b,0
    add hl,bc
    ld a,(hl)
    inc hl    
    exx
;    xor a
    call writeanytile
    inc hl
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    dec hl
    ld de,(maplenght)
    add hl,de
    exx
    ld a,(hl)
    inc hl
    exx
    call writeanytile
    inc hl
    exx
    ld a,(hl)
    exx
    call writeanytile
    
    exx
    ld a,c
    add a,4
    ld (ix+offsets+30),a ;and write back position of pointer
    exx
 
 
    inc (ix+offsets+28)
    inc sp
    inc sp
    ret

;background tiles of original state. read out and write them to map
.bkgtiles:

db 52,44,0,43,87,88,55,56,90,0,58,47,0,44,48,43,37,52,37,0


  
;previous code failed. Must not think object oriented. BUt tile oriented! we move the object according to the mapadress and not the XY of the object
;therefore we save the mapadress into the object codespace and move in precounted steps.

;that boils down to basicly
;1: read mapadress which was obtained @init V
;2: read direction from movementtable V
;3: inc mapadress + 16 or to whatever direction we are moving V
;4: move 16 steps to the correct direction V
;5: save bkg and write all data to bkg and table V

moveroyplatform:

    ;we may need to add some blockwall hacks to our object so that mario cannot slide in on it sideways and get stuck inside a wall
  ;cheap hack to help the checktile: lift mario up 1 pixel so that he will not lock to the new spawned block below
  ld a,(ix+v1)
  and a
  call z,.movemarioup

    ld a,(ix+v7)
    cp 16
    jp c,.move

    ld (ix+v7),0 ;reset counter then handle the tile based stuff
       
    
    call handleroymovementtable
    
    

;get current mapadress
    ld l,(ix+offsets+10)
    ld h,(ix+offsets+11)


  ;fetch table from RAM and store movement data
  ld iy,pillarbkgbuffer
  ld a,(ix+objectrelease) ;get pointer
  ;reset pointer to 0 when max = reached
  cp 25
  call nc,.resettablepointer  
  add a,a ;*2
  add a,a ;*4
  add a,a ;*6
  add a,a ;*8
  ld e,a
  ld d,0
  add iy,de ;set pointer to correct table part
  inc (ix+objectrelease) ;next position

  ld a,(ix+offsets+26) ;get movement type and push to table
  ld (iy+1),a ;(iy+0) is reserved
  
  ;push mapadress to table
  ld (iy+2),l
  ld (iy+3),h
  
;read the tiles in the current position before overwriting them then push them to RAM use pillarbkgbuffer for this since this part of the RAM is free on this map 
  push hl
  
  call readanytile
  ld (iy+4),a
  inc hl
  call readanytile
  ld (iy+5),a  
  dec hl
  ld de,(maplenght)
  add hl,de  
  call readanytile
  ld (iy+6),a
  inc hl
  call readanytile
  ld (iy+7),a    
  
  pop hl ;get mapadress back
      
    
    
    
    
 ;write brown block tiles to mapadress 
  
  push hl
  
  ld a,204
  call writeanytile
  inc hl
  ld a,205
  call writeanytile
  dec hl
  ld de,(maplenght)
  add hl,de
  ld a,206
  call writeanytile
  inc hl
  ld a,207
  call writeanytile

  pop hl
    
    ;fetch direction and add it to the bkgadress
    ld a,(ix+offsets+26)
    and a
    call z,.setmoveleft
    dec a
    call z,.setmoveright
    dec a
    call z,.setmoveup
    dec a
    call z,.setmovedown    
    



    ret


.resettablepointer:

    ld (ix+objectrelease),0
    xor a

    ret

.setmoveleft:

    push af
    dec hl
    dec hl
    ld (ix+offsets+10),l
    ld (ix+offsets+11),h
    pop af



    ret

.setmoveright:

    push af
    inc hl
    inc hl
    ld (ix+offsets+10),l
    ld (ix+offsets+11),h
    pop af

    ret

.setmoveup:

    push af
    ld de,(maplenght)
    xor a
    sbc hl,de
    xor a
    sbc hl,de    
    ld (ix+offsets+10),l
    ld (ix+offsets+11),h
    pop af


    ret

.setmovedown:     

    push af
    ld de,(maplenght)
    add hl,de
    add hl,de
    ld (ix+offsets+10),l
    ld (ix+offsets+11),h
    pop af

    
    ret
    
    
.move:

    call addv1toxbject
    call addv2toybject
    
    inc (ix+v7) ;time movement after finising we do the next write
    

    ret

    
.movemarioup:
  
  ld a,(standingonplatform)
  and a
  ret z
  
  ld hl,(marioy)
    dec hl
  ld (marioy),hl       
    
    ret    

    
;read movement type from table and keep reading the same entry until counter reaches 0    
    
handleroymovementtable:    
    
    
    dec (ix+v3) ;count until done
    ret nz

  
  ;use hitpoints for the movementtable pointer. read movement table set speed accordingly then push the direction to the save table
  ld hl,roymovementtable
  ld e,(ix+hitpoints)
  ld d,0
  add hl,de
  ld a,(hl)
;  cp 10
;  jp z,.stop
;    jp z,hang
  ;now read direction and movement
  and a
  call z,.setmoveleft
  dec a
  call z,.setmoveright
  dec a
  call z,.setmoveup
  dec a
  call z,.setmovedown
  
  inc hl
  ld a,(hl)
  ld (ix+v3),a ;another counter which takes care of the steps
  
  inc (ix+hitpoints)
  inc (ix+hitpoints) ;move to next table entry 
  
  
  ret

.setmoveup:

  ld (ix+offsets+26),2 ;store movement type for table storage  
  ld (ix+v1),0
  ld (ix+v2),-1  
  
  ret
  
.setmovedown:  
  
  ld (ix+offsets+26),3 ;store movement type for table storage
  ld (ix+v1),0
  ld (ix+v2),1  
  
  ret  
  
  
.setmoveleft:  
  
  ld (ix+offsets+26),0 ;store movement type for table storage
  ld (ix+v1),-1
  ld (ix+v2),0
  
  ret
    
.setmoveright:  
  
  
  ld (ix+offsets+26),1 ;store movement type for table storage
  ld (ix+v1),1
  ld (ix+v2),0  
  
  ret   
  
.stop:

    inc (ix+v4)
    ret
  


;use objectrelease as memory pointer when reading the iy based tile table
;table data on iy
;1 = movement type 0 = left 1 = right 2 = up 3 = down
;2 = mapbkgadress is a word and thus two bytes
;4 = tile 1
;5 = tile 2 
;etc etc. until tile 4
handleroyinteract:

  call mariointeract
  ret nc
  ld a,(standingonplatform)
  and a
  ret nz

  call getmariodistance
  cp 12
  jp nc,.handlesidetouch

  ld (spriteixvalue),ix
  
  ret



.handlesidetouch:

  call getmarioleftright
  jp c,.sidetouchright

  ld a,1
  ld (touchside),a
  
  ld (spriteixvalue),ix
  ret

.sidetouchright:

  ld a,2
  ld (touchside),a

  ld (spriteixvalue),ix
  ret
  
  
MovementPattern314:     ;roys castle moving platform

  ld c,0
  call platforminteract
  call handleroyinteract
  
  ;v4 sets the state of the platform. The platform moves towards a certain direction.
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,moveroyplatform
  
  ;initiate first when mario jumps on the block cheat using the mariostats we don't need to touch anything
  ;temp setspeedx


  ;432 from the edge
  
  ld de,(mariox)
  ld hl,432
  xor a
  sbc hl,de
  jp c,.start
  
  ret

.start:

    ld a,(fallspeed)
    and a
    ret nz ;wait for jump to complete
   
    ld (ix+v4),2
   
    ;start the backpart as well inc its v4 to tell it that it may begin parse own ix coordinates into it as well
    ld l,(ix+v5)
    ld h,(ix+v5+1)
    push hl
    pop iy
    
    inc (iy+v4)
    
    push ix
    pop hl
    
    ld (iy+v5),l
    ld (iy+v5+1),h
    
   
    ret
  
  

  
.init:


;wait a few frames before spawning
    inc (ix+v7)
    ld a,(ix+v7)
    cp 5
    ret c
    

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

  ;spawn special mario killer assisting object
  push ix
  push hl
  push de
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  
;  ld hl,103
  ld    (iy+5),103                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  ld hl,316
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  pop de
  pop hl
  pop ix
  
  
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld bc,2*5
  xor a
  sbc hl,bc ;move new object to the far left
  inc de
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  
  ld hl,310
  ld    (iy+5),l                 ;set object number
  ld    (iy+6),h                ;set object number
  
  
  call  activateobject

  ;custom object code
  ld hl,315
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  push ix
  pop hl
  push hl
  pop iy ;prepare to sent to foreign object
  
    pop ix
  
    ;save ix adress of foreign object
    ld (ix+v5),l
    ld (ix+v5+1),h
  
    inc (ix+v4) ;set state to activated
    ld (ix+v7),0 ;reset counter
      ld (ix+offsets+26),1 ;store movement type for table storage
    
  ;get mapadress @ current location and start working from there  
  ld de,16
  ld bc,16 ;add y offset
  call objecttilecheckfromrom    
  
  ld (ix+offsets+10),l
  ld (ix+offsets+11),h
    ld de,12
    xor a
    sbc hl,de
  ld (iy+offsets+10),l
  ld (iy+offsets+11),h       
    
    
    ret
  






 
  
MovementPattern313:     ;circlesaw (inteligent)  
  
;this is the maplevel5-07b version of the circular saw. To save ROM space we call a movementpattern change from 312 based on the maplevel
  call mariointeract
  
  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  
  call checkobjecttilesfloor
  jp c,MovementPattern312.animate ;animate the whole thing
  call checkobjecttilescheckwall
  call c,.wallfound            ;if wall found then change direction by inverting v1, and set animation  
  
    call .animate ;animate half the thing
  
  ld (ix+objectfp),0
  
  ret
  

.wallfound:

    ld a,(ix+v1)
    neg
    ld (ix+v1),a


    ret

;animate half a piece    
.animate:    

 
 
    ld a,(framecounter)
    and 1
    ret z
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddresscirclesawhalf  ;color address
  ld    bc,characteraddresscirclesawhalf  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  
  

.position1:
  ld    hl,coloraddresscirclesawhalf1  ;color address
  ld    bc,characteraddresscirclesawhalf1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  

  ret
  
  
  
MovementPattern312:     ;circlesaw  
  
  call mariointeract
  
  call .animate
  
  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.left
  dec a
  jp z,.up
  dec a
  jp z,.right
  dec a
  jp z,.down
  
  
  ret
  
  
.left:


    
    ld a,(ix+v7)
    cp (ix+v5)
    ret c

    inc (ix+v4)
    ld (ix+v7),0
    
    ld (ix+v1),0
    ld (ix+v2),-3

    ret


.up: 

    ld a,(ix+v7)
    cp (ix+v6)
    ret c

    inc (ix+v4)
    ld (ix+v7),0

    ld (ix+v1),3
    ld (ix+v2),0    
    
    ret
    


.right:

    ld a,(ix+v7)
    cp (ix+v5)
    ret c

    inc (ix+v4)
    ld (ix+v7),0

    ld (ix+v1),0
    ld (ix+v2),3    
    
    
    ret


.down:  
  
    ld a,(ix+v7)
    cp (ix+v6)
    ret c

    ld (ix+v4),1
    ld (ix+v7),0

    ld (ix+v1),-3
    ld (ix+v2),0
    
    
    ret  
  
  
  
;  ret

.init:

;check maplevel
    ld a,(currentlevel)
    cp 99
    jp z,.initspecial
    cp 100
    jp nc,.initsuperspecial
    

    ld e,(ix+xobject)
    ld d,(ix+xobject+1)
    ld hl,800
    xor a
    sbc hl,de
    jp c,.inithigh

    ld (ix+v5),20 ;left/right
    ld (ix+v6),10  ;up/down
    inc (ix+v4)


    ret

.inithigh:    
    
    ld (ix+v5),5 ;left/right
    ld (ix+v6),30  ;up/down
    inc (ix+v4)
    
    
    ret

.initspecial:

    ld (ix+v4),0
    ld (ix+v7),0
    ld hl,313
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+sizeY),16


    ret

.initsuperspecial:    

    ld (ix+v4),0
    ld (ix+v7),0
    ld hl,336
    ld (ix+movepat),l
    ld (ix+movepat+1),h

    ret

  
.animate:
 
 
    ld a,(framecounter)
    and 1
    ret z
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddresscirclesaw  ;color address
  ld    bc,characteraddresscirclesaw  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  
  

.position1:
  ld    hl,coloraddresscirclesaw1  ;color address
  ld    bc,characteraddresscirclesaw1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  

  ret
  
  
  
  
  
MovementPattern311:     ;blocktileanimator2

;generalize blocktile animations with this object


  call followmario

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  ld    a,animationtilesblock2
  call  block12


;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtiles0507a+(8*12)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0507aclr+(8*12)             ;rotating tile animation 1 character address in ro
  jp z,.updateblocks   

.step3:
  ld hl,animationtiles0507a+(8*8)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0507aclr+(8*8)             ;rotating tile animation 1 character address in ro
  jp z,.updateblocks 

.step2:
  ld hl,animationtiles0507a+(8*4)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0507aclr+(8*4)             ;rotating tile animation 1 character address in ro  
  jp z,.updateblocks

.step1:
  ld hl,animationtiles0507a+(8*0)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0507aclr+(8*0)             ;rotating tile animation 1 character address in ro

 

.updateblocks:
  
  exx 

  
  ld    hl,$4000+36*8
  call .doshit
  ld    hl,$0000+36*8 
  call .doshit

  jp setpage12tohandleobjectmovement
 
; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix32

  ret

  
MovementPattern310:     ;iggy platform coverup  
  
  ;do nothing just sit there
  ld a,(currentlevel)
  cp 98
  ret nz
  
  ;init completed done
  ld a,(ix+v4)
  and a
  ret nz
  
  ld    hl,coloraddressffcoverup  ;color address
  ld    bc,characteraddressffcoverup  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  
  ;set to this sprite
  
  
  
  ret
  
  
  
MovementPattern309: ;illusion secret1 moving platform


    ld c,0
    call platforminteract


    ld a,(ix+v4)
    and a
    jp z,.init
    cp 2
    jp z,animateplatformwings
    cp 3
    jp z,.move

    call mariointeract
    jp c,.activate
    
    ret

.change:

    ld a,(ix+v5)
    dec a
    jp z,.change2

    ld (ix+v7),0
    ld a,(ix+v2)
    neg
    ld (ix+v2),a
    
    inc (ix+v5) ;set state to 2
    
    ret
 
.change2:

    ld a,(ix+v7)
    cp 255
    ret nz


    ld (ix+v7),0
    ld a,(ix+v2)
    neg
    ld (ix+v2),a
    ret

 
 
.move:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 128
    call nc,.change



    call addv1toxbject
    call addv2toybject

    ld l,(ix+offsets+24)
    ld h,(ix+offsets+25)
    push hl
    pop iy
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,16-2
    xor a
    sbc hl,de
    ld (iy+xobject),l
    ld (iy+xobject+1),h

    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,8
    xor a
    sbc hl,de
    ld (iy+yobject),l
    ld (iy+yobject+1),h    
    
    
    
    
    
    ld l,(ix+offsets+26)
    ld h,(ix+offsets+27)
    push hl
    pop iy
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,16+32-2
    add hl,de
    ld (iy+xobject),l
    ld (iy+xobject+1),h    
    
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,8
    xor a
    sbc hl,de
    ld (iy+yobject),l
    ld (iy+yobject+1),h  
    
    
    
    ret
    

.activate:

    ld (ix+v4),3
    ld (ix+v7),0

    ret
    
    
;spawn the wings    
.init:

;wait a few frames before spawning
    inc (ix+v7)
    ld a,(ix+v7)
    cp 5
    ret c
    

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

  push hl
  push de
  
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  
    dec hl
    dec hl
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject

  ld hl,309
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+deadly),3
    ld (ix+clipping),0
; move 2 pixels to the right
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    inc hl
    inc hl
    ld (ix+xobject),l
    ld (ix+xobject+1),h
    
    
  ld (ix+v4),2
;  call getmarioleftright
;  call c,.setright

    push ix
    pop hl

  pop ix

  ld (ix+offsets+24),l
  ld (ix+offsets+25),h
  
  
  pop de
  pop hl

  ;create second powerup
  push ix

  
    inc hl
    ld bc,5
    add hl,bc
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject

  ld hl,309
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    ld (ix+deadly),3
    ld (ix+clipping),0
    ld (ix+v3),2 ;set right
  ld (ix+v4),2
;  call getmarioleftright
;  call c,.setright

; move 2 pixels to the left
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    dec hl
    dec hl
    ld (ix+xobject),l
    ld (ix+xobject+1),h



    push ix
    pop hl

  pop ix  
  
    ld (ix+offsets+26),l
    ld (ix+offsets+27),h ;push object adress into offsets
  
  
  inc (ix+v4)
    
    ret
    

animateplatformwings:

  ld a,(framecounter)
  and 3
  ret nz
 
.noframecheck: 
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex471  ;color address
  ld    bc,spriteaddrex471c  ;character address
  ld    de,offsetwingcloseleft
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex472  ;color address
  ld    bc,spriteaddrex472c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex473  ;color address ;02
  ld    bc,spriteaddrex473c  ;character address
  ld    de,offsetwingcloseright
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex474  ;color address ;03
  ld    bc,spriteaddrex474c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret
    
    
    

MovementPattern308:     ;color changer (maplevel5-04a)

  ld a,(ix+objectrelease)
  and a
  jp nz,.colorchangetoleft

;v3 0 = left 1 = right
  call getmarioleftright
  jp c,.switchblue



.switchyellow:

  ld a,(ix+v3)
  and a
  ret z
  
  ld (ix+v3),0
  
  ld a,9 ;color number
  ld e,%01110001
  ld d,6;6 
  call switchcolor

  ret



.switchblue:

  ld a,(ix+v3)
  and a
  ret nz
  
  ld (ix+v3),1


  ld a,9 ;color number
  ld e,5
  ld d,3;6 
  call switchcolor

  ret


.colorchangetoleft:

;v3 0 = left 1 = right
  call getmarioleftright
  jp nc,.switchblue

  jp .switchyellow




;code for later
MovementPattern307:     ;lakitu cloud

  ld a,(ix+v4)
  dec a
  jp z,.handleshroom

  call addv1toxbject
  call addv2toybject

  ld a,(ix+v3)
  and a
  jp nz,.ride

  ld a,(ix+objectrelease)
  and a
  ret z

  ld a,(marioyoshi)
  and a
  ret nz ;not allowed when on yoshi

  
;wait a few frames before checking
  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  ret c


  call mariointeract
  jp c,.activate

  ld (ix+v7),15


  ret

.handleshroom:

  call mariointeract
  ret nc
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  inc (iy+v4) ;tell lakitu that all shit broke loose
  
  
  
  jp setextralife


;  ret
  


;grab mario on interact and start movement
.activate:

  ld a,(fallspeed)
  and a
  ret z


;object now Controls mario
    ld a,5
    ld (nocontrol),a
  inc (ix+v3)
;  inc (ix+v4)

;stop all previous actions
  xor a
  ld (mariospeed),a
  ld (marioslide),a

  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (rampjump),a
  ld (uppersheercornerfound),a  
  ld (marioduck),a
  ld (mariolook),a
  ld (mariospinjump),a


  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a  
  
  
  ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a

;TODO: get current cloud speed and adjust table to that  
  call .getspeedx
  call .getspeedy

  ret

;32 wide
.cloudspeedtable: db -2,-2,-2,-2,-2,-2,-1,-2,-1,-2,-1,-1,-1,-1,-1,0,1,1,1,1,2,1,2,1,2,2,2,2,2,2,2,2,5


;get current cloudspeed x and adjust the table to that
.getspeedx:

  ld a,(ix+v1)
  bit 7,a
  jp nz,.getnegativex
  and a
  ret z
  dec a
  jp z,.set1x
  
  ld (ix+v7),31

  ret
  
.getnegativex:

  and a
  ret z
  dec a
  jp z,.set1nx
  
  ld (ix+v7),0

  ret

.set1x:

  ld (ix+v7),19
  
  ret

.set1nx:

  ld (ix+v7),10
  
  ret

;get current cloudspeed y and adjust the table to that
.getspeedy:

  ld a,(ix+v2)
  bit 7,a
  jp nz,.getnegativey
  and a
  ret z
  dec a
  jp z,.set1y
  
  ld (ix+v6),31

  ret
  
.getnegativey:

  and a
  ret z
  dec a
  jp z,.set1ny
  
  ld (ix+v6),0

  ret

.set1y:

  ld (ix+v6),19
  
  ret

.set1ny:

  ld (ix+v6),10
  
  ret


.setspeedx:


  ld hl,.cloudspeedtable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)  
  ld (ix+v1),a
 
  and a
  ret z 

  
  bit 7,a
  jp nz,.increment
  
  dec (ix+v7)
  
  
  ret

.increment:


  inc (ix+v7)
  
  ret


.setspeedy:


  ld hl,.cloudspeedtable
  ld e,(ix+v6)
  ld d,0
  add hl,de
  ld a,(hl) 
  ld (ix+v2),a
 
  and a
  ret z 

  
  bit 7,a
  jp nz,.incrementy  
  
  dec (ix+v6)
  
  
  ret

.incrementy:


  inc (ix+v6)
  
  ret


.ride:

  ld hl,(mapheightx8)
  ld de,48
  xor a
  sbc hl,de
;  ex de,hl
  ld de,(marioy)
  xor a
  sbc hl,de
  jp c,.release


  ld a,1
  ld (standingonplatform),a ;prevent wierd camera exit bugs


;sync object to mario
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  dec hl
  dec hl
  dec hl
  dec hl
  ld (mariox),hl  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,24
  sbc hl,de
  ld (marioy),hl
  
  call .setspeedx
  call .setspeedy

  call .checktile

  ld a,(Controls)
  bit 0,a
  call nz,.up
    ld a,(Controls)
  bit 1,a
  call nz,.down
    ld a,(Controls)
  bit 2,a
  call nz,.left
    ld a,(Controls)
  bit 3,a
  call nz,.right  
    ld a,(Controls)
  bit 4,a
  jp nz,.release
  bit 6,a
  jp nz,.release


  ret

;many checks but they wont bog up the clock because the mariophysics are disabled at this point.
.checktile:

  ld a,(ix+v1)
  bit 7,a
  push af
  call nz,.checkleft
  pop af
  call z,.checkright
  ld a,(ix+v2)
  bit 7,a
  jp nz,.checkup


.checkdown:

  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  cp 191
  jp nc,.endcheckdown
  ld de,8
  ld bc,16
  call objecttilecheckfromrom
  cp 191
  ret c

.endcheckdown:

  ld (ix+v2),0 
  
  ret

.checkup:

  ld hl,(marioy) ;maximum mapheight reached
  ld de,8
  xor a
  sbc hl,de
  jp c,.endcheckup

  ld de,0
  ld bc,-8
  call objecttilecheckfromrom
  cp 191
  ret c

.endcheckup:
  
  ld (ix+v2),0  
  
  ret



.checkright:
  
  ld de,8
  ld bc,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.endcheckright
  ld de,8
  ld bc,8
  call objecttilecheckfromrom
  cp 191
  ret c

.endcheckright:

  
  ld (ix+v1),0
  
  ret


.checkleft:

  ld de,-8
  ld bc,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.endcheckleft
  ld de,-8
  ld bc,8
  call objecttilecheckfromrom
  cp 191
  ret c

.endcheckleft:

  ld (ix+v1),0
  
  ret



.left:

  xor a
  ld (marioleftright),a
  ld (marioleftrightspr),a

  ld a,(ix+v7)
  cp 2
  ret c

  dec (ix+v7)
  dec (ix+v7)


  ret

.right:

  ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a

  ld a,(ix+v7)
  cp 30
  ret nc

  inc (ix+v7)
  inc (ix+v7)


  ret



.up:

  ld a,(ix+v6)
  cp 2
  ret c

  dec (ix+v6)
  dec (ix+v6)


  ret

.down:

  ld a,(ix+v6)
  cp 30
  ret nc

  inc (ix+v6)
  inc (ix+v6)


  ret



.release:

  xor a
  ld (ix+v3),0
  ld (standingonplatform),a

  ld (ix+v7),0

;;we exit through a vertical tube! ;not needed in here since the cloud does not have a release code
;  ld a,(nocontrol)
;  cp 1
;  ret z

  xor a
  ld (nocontrol),a

  ret 




  

acceleratelakitu:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  jp c,.limit ;limit to absolute right of map prevent escape this way 
  

  call getmarioleftright
  jp c,.acceleratemoleright


  ;include sprite updates as wel
.acceleratemoleleft:
  
  call getmariodistance
  cp 160
  jp nc,.setmaxspeedleft

  ld a,(ix+v3)
  and a
  ret z
  
  dec (ix+v3)
  
  ld a,(ix+v3)
  
  ld hl,lakituacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a  


  ret

  
.limit:

  ld (ix+v3),25

  jp .acceleratemoleright
  
;  ret
  
  
.setmaxspeedleft:
  
  ld a,c
  cp 155
  jp c,.set4right
  
  ld (ix+v1),-6

  ret

.set4left:

  ld (ix+v1),-3
  
  ret

.acceleratemoleright:

  call getmariodistance
  cp 160
  jp nc,.setmaxspeedright

  ld a,(ix+v1)
  cp 6
  jp z,.setmaxspeedright

  ld a,(ix+v3)
  cp 41
  ret z
  
  inc (ix+v3)
  
  ld a,(ix+v3)
  
  ld hl,lakituacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a

  ret

.setmaxspeedright:
  
  ld a,c
  cp 155
  jp c,.set4right
  
  ld (ix+v1),6

  ret

.set4right:

  ld (ix+v1),3
  
  ret
  
  

lakituacctable:   db -3,-3,-3,-3,-3,-3,-2,-3,-2,-3,-2,-1,-2,-1,-1,-1,-1,0,-1,0,-1,0,0,0,1,0,1,0,1,1,1,2,1,2,3,2,3,3,3,3,3,3
; lakituacctable:   db -4,-3,-2,-1,-2,-1,-2,-1,-1,-1,0,0,0,1,1,1,2,1,2,1,2,3,4
  



MovementPattern306:     ;fishin lakitu

;  ld (ix+objectrelease),1
  

  call objectinteract
  cp 5
  jp z,.destroyobjectwithcoin
  cp 2
  jp z,.otherobjectshotdeathjump  

  call checkmariointeractshoot
  jp c,.otherobjectshotdeathjump
  
  
    call mariointeract
    jp c,.destroyobjectwithcloud




  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.transformtopissed ;cp2

  ;work for the aggressive guy
    dec a
    jp z,.wait
    dec a
    jp z,.trowspikey
    dec a
    jp z,.wait2

  
.return:  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get foreign object

  
  call addv1toxbject
  call addv2toybject

  call acceleratelakitu
  call .setymovement
  
  call .setspriteleftright

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
;  ld de,8
;  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,12
  add hl,de  
  ld (iy+yobject),l
  ld (iy+yobject+1),h  

;mushroom
  ld a,(ix+v4)
  cp 2
  ret nc ;mushroom is gone so no manupilation of these objects


  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy ;get foreign object 2


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,48
  add hl,de  
  ld (iy+yobject),l
  ld (iy+yobject+1),h 



  ret


.destroyobjectwithcoin:

  ld (ix+objectreturn),1 ;kill permanently

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get foreign object

  ld l,(ix+v1)
  ld h,(ix+v2)
  ld (iy+v1),l
  ld (iy+v2),h
  inc (iy+objectrelease)

;take care of mushroom as well
  ld a,(ix+v4)
  cp 2
  jp nc,destroyobjectwithcoin ;mushroom is gone so no manupilation of these objects


  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy ;get foreign object 2
  
  ld (iy+movepat),20
  ld (iy+movepat+1),0
  ld (iy+v4),100

  jp destroyobjectwithcoin


.destroyobjectwithcloud:

  ld (ix+objectreturn),1 ;kill permanently

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get foreign object

  ld l,(ix+v1)
  ld h,(ix+v2)
  ld (iy+v1),l
  ld (iy+v2),h
  inc (iy+objectrelease)

;take care of mushroom as well
  ld a,(ix+v4)
  cp 2
  jp nc,destroyobjectwithcloud ;mushroom is gone so no manupilation of these objects


  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy ;get foreign object 2
  
  ld (iy+movepat),20
  ld (iy+movepat+1),0
  ld (iy+v4),100

  jp destroyobjectwithcloud

  
.wait2:


  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 10
  ret c

  ld (ix+v4),3
  ld (ix+v7),0

  call .resetspriteleftright
  
  jp .return
  
  ;ret

.trowspikey:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),254              ;set object number
  ld    (iy+6),0                ;set object number

  call  activateobject


  ld (ix+v1),-1
  call getmarioleftright
  call c,.setright

  pop ix

  call .incv4
  jp .return
  ;ret

.setright:

  ld (ix+v1),1

  ret
  
.resetspriteleftright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  call getmarioleftright
  jp c,.resetspriteright

  ld    hl,coloraddresslakitupipe1 ;color address
  ld    bc,characteraddresslakitupipe1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  
  
;ret  

.resetspriteright:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,coloraddresslakitupipe3 ;color address
  ld    bc,characteraddresslakitupipe3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  


;  ret  
  
  
.wait:

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 80
  jp c,.return

  call .incv4
  
  call getmarioleftright
  jp c,.settrowright
  
;directly update sprite here  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitupipe2  ;color address
  ld    bc,characteraddresslakitupipe2  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  call    setcharcolandoffset  
  
  jp .return

;  ret  


.settrowright:

;directly update sprite here  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitutrowright  ;color address
  ld    bc,characteraddresslakitutrowright  ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock6
  call    setcharcolandoffset  
  
  jp .return

;  ret  

  
  
.incv4:


  ld (ix+v7),0
  inc (ix+v4)
  ret
  
  
  
.otherobjectshotdeathjump:

  ld a,(ix+amountspr)
  cp 2
  call z,.setspritesto4

  ld (ix+objectreturn),1 ;kill permanently

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddresslakitudying  ;color address
  ld    bc,characteraddresslakitudying   ;character address
  ld    de,offsetlakitupipe
  ld    a,ingamespritesblock4
  call    setcharcolandoffset
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get foreign object

  ld l,(ix+v1)
  ld h,(ix+v2)
  ld (iy+v1),l
  ld (iy+v2),h
  inc (iy+objectrelease)


;take care of mushroom as well
  ld a,(ix+v4)
  cp 2
  jp nc,otherobjectshotdeathjump ;mushroom is gone so no manupilation of these objects


  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy ;get foreign object 2
  
  ld (iy+movepat),20
  ld (iy+movepat+1),0
  ld (iy+v4),100


  jp otherobjectshotdeathjump

  
.setspritesto4:

  ld b,4
  call increasespritecount

  ret  
  
.setspriteleftright:

  ld a,(ix+v4)
  cp 4
  ret nc ;in a state

  ;ix+offsets+22 0 = left 1 = right sprite active
  
  call getmarioleftright
  jp c,.setspriteright

  ld a,(ix+offsets+22)
  and a
  ret z
  
  ld (ix+offsets+22),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v4)
  cp 2
  jp nc,.leftpissed
  
  ld    hl,coloraddressfishinlakitu ;color address
  ld    bc,characteraddressfishinlakitu  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

  
.leftpissed:

  ld    hl,coloraddresslakitupipe1 ;color address
  ld    bc,characteraddresslakitupipe1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  
  
;ret  

.setspriteright:

  ld a,(ix+offsets+22)
  dec a
  ret z
  
  ld (ix+offsets+22),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v4)
  cp 2
  jp nc,.rightpissed
  
  
  ld    hl,coloraddressfishinlakituright ;color address
  ld    bc,characteraddressfishinlakituright ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.rightpissed:

  ld    hl,coloraddresslakitupipe3 ;color address
  ld    bc,characteraddresslakitupipe3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  


;  ret
  
.transformtopissed:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v4)

  ld    hl,coloraddresslakitupipe1 ;color address
  ld    bc,characteraddresslakitupipe1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset



;  ret
  


.setymovement:

  ld e,(ix+offsets+24)
  ld d,0
  ld hl,.ymovementtable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.resettablecounter

  ld (ix+v2),a
  inc (ix+offsets+24)

  ret

.resettablecounter:

  ld (ix+v2),0
  ld (ix+offsets+24),0

  ret


.ymovementtable:  db 0,0,1,0,0,1,0,0,1,0,1,0,1,1,1,2,1,2,1,2,1,2,2,2,1,2,1,1,2,1,1,0,1,0,1,0,0,1,0,0,-1,0,-1,0,-1,-1,-2,-1,-1,-2,-1,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,5

  
  
  
;spawn cloud and parse ix to lakitu
.init:

  inc (ix+v7)
  ld a,(ix+v7) ;cheap bug fix giving eventhandler time to spawn during init
  cp 5
  ret c

;spawn the inner object

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de

  push hl
  push de


  push ix

  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

;grab object and stop all its code until release  
  ld hl,307
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld (ix+deadly),3
  ld (ix+clipping),0

  ld    hl,coloraddresslakitucloud  ;color address
  ld    bc,characteraddresslakitucloud  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix
  
  ld (ix+v5),l
  ld (ix+v5+1),h

;spawn 1 up
  pop de
  pop hl

;branch init code here to spawn a pissed version of this guy
  ld a,(ix+objectrelease)
  and a
  jp nz,.initpissed
  
  
  push ix
  exx
  pop hl ;save ix for into mushroom
  exx
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),15                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  exx
  ld (ix+v5),l
  ld (ix+v5+1),h
  exx
  
;grab object and stop all its code until release  
  ld hl,307
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  inc (ix+v4)

  push ix
  pop hl

  pop ix
  
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  
  inc (ix+v4)
  ld (ix+v7),0
  
  ret


.initpissed:

  ld (ix+v4),2
  
  ret






MovementPattern305:     ;exploding bubble

;init code and state do not place anything before this!
  ld a,(ix+v4)
  and a
  jp z,.init ;spawn inner object
  dec a
  dec a
  jp z,.setsprite
  dec a
  jp z,.handleobject

  
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy ;get iy
    
 
  call mariointeract
  call c,.changestate
  
  call .checkwallsandfloor



  ld a,(framecounter)
  and 7
  call z,.animate

  call .setymovement


  call addv1toxbject ;move object
  call addv2toybject ;move object

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de  
  ld (iy+yobject),l
  ld (iy+yobject+1),h



  ret


.checkwallsandfloor:


;not good enought yet we need to check a little bit higher too
  ld de,-6
  ld bc,0 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 192
  jp nc,.changestate


  ;check the floor and walls
  ld de,-6
  ld bc,32 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 192
  ret c

.changestate:

  ;change object back to correct form
  ld (iy+deathtimer),0
  ld (iy+objectfp),0
    ld l,(iy+v1)
    ld h,(iy+v2)
  ld (iy+v1),-1
  ld (iy+v2),0
  ld (iy+v3),0
  ld (iy+v4),0
  ld (iy+v7),0
  
  ld (iy+movepat),l
  ld (iy+movepat+1),h

  ld a,l
  cp 16
  call z,.setshroomvars ;cheap hack to make mushroom work


  call Playsfx.chuckclap

  inc sp ;no return to main code
  inc sp

  jp destroyobjectwithcloudnostar
  


;  ret


.setshroomvars:
  
  ld (iy+v4),255



  ret


;foreign object beware!!
.handleobject:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld l,a
  or h
  jp z,destroyobject ;check if original calling object is still instact if not commit suicide

;  push hl 
;  pop iy ;fetch foreign object adress and check if something strange had happened
;  ld l,(iy+movepat)
;  ld h,(iy+movepat+1)
;  ld de,305
;  xor a
;  sbc hl,de
;  jp z,destroyobject
  
  

  ld a,(framecounter)
  and 7
  ret nz
  

;animate

  ld a,(ix+objectrelease)
  dec a
  ret z
  dec a
  ret z
  dec a
  ret z
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a

  jp z,.position1f
  

.position0f:

  ld    hl,coloraddressgoombaupsidedownleft1  ;color address
  ld    bc,characteraddressgoombaupsidedownleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



.position1f:

  ld    hl,coloraddressgoombaupsidedownleft2 ;color address
  ld    bc,characteraddressgoombaupsidedownleft2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;ret


.setfish:

  ld bc,321
  
  ret

.setboomb:

  ld bc,255

  ret


.setmushroom:

  ld bc,14

  ret



.init:

    

  ld c,191
;get objectrelease and set right object
  ld a,(ix+objectrelease)
  exx
  ld b,a ;parse into spawned object
  exx
  dec a
  call z,.setboomb
  dec a
  call z,.setmushroom
  dec a
  call z,.setfish


;spawn the inner object

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc de
;  inc de


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),c                ;set object number ()
  ld    (iy+6),b                ;set object number ()

  call  activateobject
  
;  call .setsprite ;set sprite first
  exx
  ld a,b
  exx
  
  ld (ix+objectrelease),a ;and give it to object

  ld l,(ix+movepat)
  ld h,(ix+movepat+1)
  ld a,l ;cheap shitty hack
  cp 254
  call z,.inchl
  ld (ix+v1),l
  ld (ix+v2),h

;grab object and stop all its code until release  
  ld hl,305
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v4),2

  push ix
  pop iy ;parse main object id into foreign object to prevent stucked sprites
  push ix
  pop hl
 
  
  pop ix

  push ix
  pop de

  ld (iy+v5),e
  ld (iy+v5+1),d
  ld (ix+v5),l
  ld (ix+v5+1),h
  inc (ix+v4)
  
  
  
  
  ret


.inchl:

  inc hl
  ret


.setsprite:

;  exx
;  ld a,b
;  exx
;  
;  ld (ix+objectrelease),a ;and give it to object

  ld l,(ix+v5)
  ld h,(ix+v5+1)

  push hl 
  pop iy ;fetch foreign object adress and check if something strange had happened  
  
  ld l,a
  or h
  jp z,destroyobject ;check if original calling object is still instact if not commit suicide
  
  ld l,(iy+movepat)
  ld h,(iy+movepat+1)
  ld de,305
  xor a
  sbc hl,de
  jp nz,destroyobject


  inc (ix+v4)
  
  ld a,(ix+objectrelease)
  dec a
  jp z,.setboombspr
  dec a
  jp z,.setshroomspr
  dec a
  jp z,.setfishsprite

;  ret

  ld    hl,coloraddressgoombaupsidedownleft1  ;color address
  ld    bc,characteraddressgoombaupsidedownleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  ;ret

.setboombspr:


  ld    hl,coloraddressboombupsidedownleft1  ;color address
  ld    bc,characteraddressboombupsidedownleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

  ;ret

.setshroomspr:

  ld    hl,coloraddressredmushroom  ;color address
  ld    bc,characteraddressredmushroom  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

  ;ret

.setfishsprite:

  ld    hl,coloraddressfish1left  ;color address
  ld    bc,characteraddressfish1left  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

  ;ret
  



.setymovement:

  ld e,(ix+v7)
  ld d,0
  ld hl,.ymovementtable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.resettablecounter

  ld (ix+v2),a
  inc (ix+v7)

  ret

.resettablecounter:

  ld (ix+v2),0
  ld (ix+v7),0

  ret

.ymovementtable:  db 0,0,1,0,0,1,0,0,1,0,1,0,1,1,1,1,2,1,2,1,2,1,2,2,2,1,2,1,1,2,1,1,0,1,0,1,0,0,1,0,0,-1,0,-1,0,-1,-1,-2,-1,-1,-2,-1,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,5


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a

  jp z,.position1
  

.position0:

  ld    hl,coloraddressbubble1  ;color address
  ld    bc,characteraddressbubble1  ;character address
  ld    de,offsetbubble
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.position1:

  ld    hl,coloraddressbubble2 ;color address
  ld    bc,characteraddressbubble2  ;character address
  ld    de,offsetbubble
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


;ret


offsetbubble: dw 000,000, 000,016, 016,000, 016,016


MovementPattern304:     ;seaurchin (all forms)

  call mariointeract ;kill mario

  ld a,(framecounter)
  and 7
  call z,.animate ;animation is the same for all seaurchins
  

  ld a,(ix+objectrelease)
  dec a
  jp z,.handlesidetoside
  dec a
  jp z,.handleround

.handleupdown:


  ld a,(ix+v4)
  and a
  jp z,.movedown ;state going down or up
  dec a
  jp z,.wait
  dec a
  jp z,.moveup
  dec a
  jp z,.wait
  
  
  ld (ix+v4),0 ;reset cycle
  ld (ix+v7),0
  
  ret
  

.wait:
  ;wait
  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  
  jp .incv4

  ;ret
  
.incv4:

    inc (ix+v4)
    ld (ix+v7),0
    
  ret

.movedown:

  ld (ix+v2),1 ;move down
  
  call addv2toybject
  
  
  ;check the floor
  ld de,0
  ld bc,32 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret c

  jp .incv4


;  ret


.moveup:

  ld (ix+v2),-1 ;move down
  
  call addv2toybject
  
  
  ;check the floor
  ld de,0
  ld bc,1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret c

  jp .incv4


;  ret


.handlesidetoside:


  ld a,(ix+v4)
  and a
  jp z,.moveleft ;state going down or up
  dec a
  jp z,.wait
  dec a
  jp z,.moveright
  dec a
  jp z,.wait
  
  
  ld (ix+v4),0 ;reset cycle
  ld (ix+v7),0
  
  ret




.moveleft:

  ld (ix+v1),-1 ;move left
  
  call addv1toxbject
  
  
  ;check the floor
  ld de,-8
  ld bc,1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret c

  jp .incv4


;  ret


.moveright:

  ld (ix+v1),1 ;move right
  
  call addv1toxbject
  
  
  ;check the walls
  ld de,24
  ld bc,1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret c

  jp .incv4


;  ret


;notice: we alwyas start with left movement. Yes I know the original simply crawls along the walls but lets be honest about my motivation at this stage.
;no timers needed and no wait states required this baby moves normally.
.handleround:

  
  ld a,(ix+v4)
  dec a
  jp z,.movedownc
  dec a
  jp z,.moverightc
  dec a
  jp z,.moveupc  

  ld (ix+v4),0

.moveleftc:

  ld (ix+v1),-1 ;move left
  
  call addv1toxbject
  
  
  ;check the floor
  ld de,24
  ld bc,33 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc

  ;check the floor
  ld de,-8
  ld bc,33 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 221
  ret z
  cp 233
  ret z
  cp 234
  ret z  

  jp .incv4


;  ret



.movedownc:

  ld (ix+v2),1 ;move down
  
  call addv2toybject
  
  ld de,32
  ld bc,33 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc


  ld de,32
  ld bc,0 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc


  jp .incv4


;  ret



.moverightc:


  ld (ix+v1),1 ;move right
  
  call addv1toxbject
  
  
  ;check the ceiling
  ld de,24
  ld bc,-1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc

  ;check the ceiling
  ld de,-6
  ld bc,-1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc


  jp .incv4


;  ret



.moveupc:

  ld (ix+v2),-1 ;move up
  
  call addv2toybject
  
  
  ;check the left wall
  ld de,-8
  ld bc,-1 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc

  ;check the left wall
  ld de,-8
  ld bc,30 ;thats about 32 pixels down
  call objecttilecheckfromrom 
  ;compare to hard tiles value
  cp 191
  ret nc


  jp .incv4


;  ret




  ret

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  inc (ix+v3)
  ld a,(ix+v3)
  dec a
  jp z,.position0
  dec a
  jp z,.position1
  dec a
  jp z,.position2
  dec a
  call z,.position1
  ld (ix+v3),0
  
  ret


.position0:

  ld    hl,coloraddressseaurchin1  ;color address
  ld    bc,characteraddressseaurchin1  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.position1:

  ld    hl,coloraddressseaurchin2 ;color address
  ld    bc,characteraddressseaurchin2  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.position2:

  ld    hl,coloraddressseaurchin3  ;color address
  ld    bc,characteraddressseaurchin3  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



;ret


MovementPattern303:

;this is the temp object handle it with a small jump
  ld a,(ix+v4)
  dec a
  jp z,.handlejump ;jump up
  dec a
  jp z,.fall ;fall until hard groud is hit


;wait for it
   call getmariodistance
   ld a,c
   cp 70 
   ret nc
   

;and then... explode
  ld de,0
  ld bc,8
  call objecttilecheckfromrom 
    call crushblockfromram


;spawn the turn objects

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec hl
;  inc hl
  ;this very shitty code is mapspecific but saves us infolist space
;  push hl
;  ld de,52
;  xor a
;  sbc hl,de
;  call z,.setgoomba ;test
;  pop hl
  
  
  
  push hl
  ld de,354
  xor a
  sbc hl,de
  call z,.setnakedkoopa
  pop hl
  push hl
  ld de,360
  xor a
  sbc hl,de
  call z,.setgoomba
  pop hl
  push hl
  ld de,387
  xor a
  sbc hl,de
  call z,.setgoomba
  pop hl 
  push hl
  ld de,391
  xor a
  sbc hl,de
  call z,.setkoopa
  pop hl
  push hl
  ld de,397
  xor a
  sbc hl,de
  call z,.setgoomba
  pop hl
  push hl
  ld de,420
  xor a
  sbc hl,de
  call nc,.setkoopa
  pop hl
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  exx
  ld    (iy+5),l                ;set object number ()
  exx
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ;get movepat
  ld l,(ix+movepat)
  ld h,(ix+movepat+1)
  ld (ix+v5),l ;store original movement pattern
  ld (ix+v5+1),h

;hold object for jump then release  
  ld hl,303
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld (ix+v7),0
  
  inc (ix+v4)
  
  pop ix

  jp destroyobject ;done handle from spawned object


;  ret

.setnakedkoopa:

  exx
  ld l,68
  exx


  ret

.setkoopa:

  exx
  ld l,63
  exx


  ret

.setgoomba:

  exx
  ld l,191
  exx


  ret


.fall:

  call mariointeract ;upon touch  

  call addv2toybject

  call  checkobjecttilesfloor
  ret c

.activate:


;restore original movement pattern
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld (ix+v7),0
  ld (ix+deathtimer),0
  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+objectfp),0
  ld (ix+deathtimer),0
  
  ret


.handlejump:

  call mariointeract

  ld e,(ix+v7)
  ld d,0
  ld hl,.jumptable
  add hl,de
  ld a,(hl)
  and a
  jp z,.incv4 ;done with the jump fix the rest
  ld (ix+v2),a
  
  call addv2toybject
  
  inc (ix+v7)

  ret


.incv4:

  ld (ix+v7),0
  inc (ix+v4)
  ld (ix+deathtimer),0
  ld (ix+objectfp),0

  ret





.jumptable: db -4,-4,-3,-3,-3,-2,-2,-2,-1,-1,-1,-1,0



MovementPattern302:     ;wiggler (dying)

  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 4
  ret c
  

  inc (ix+v4)  
  ld a,(ix+v4)
  cp 5
  jp z,destroyobject



;spawn the turn objects

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  ld c,(ix+v3)
  ld b,(ix+v4)

  push ix
  push bc

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  
  
  pop bc ;parse pissed into objects
  call .setsprite
  call otherobjectdeathjump
  

  pop ix

  ld (ix+v7),0
  
  ret


.setsprite:

  ld a,b
  dec a
  jp z,.sethead


  ld a,c
  and a
  jp nz,.setspritebodypissed


  ld a,r
  and 1
  jp z,.random1


  ld    hl,coloraddresswigglerbody1turnleft1  ;color address
  ld    bc,characteraddresswigglerbody1turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.random1:

  ld    hl,coloraddresswigglerbody1turnright1  ;color address
  ld    bc,characteraddresswigglerbody1turnright1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  

.setspritebodypissed:


  ld a,r
  and 1
  jp z,.random2


  ld    hl,coloraddresswigglerbody2turnleft1  ;color address
  ld    bc,characteraddresswigglerbody2turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.random2:

  ld    hl,coloraddresswigglerbody2turnright1  ;color address
  ld    bc,characteraddresswigglerbody2turnright1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  

.sethead:

  ld a,c
  and a
  jp nz,.setspriteheadpissed

  ld b,4
  call increasespritecount


  ld a,r
  and 1
  jp z,.random3


  ld    hl,coloraddresswigglerhead1turnleft  ;color address
  ld    bc,characteraddresswigglerhead1turnleft  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.random3:

  ld    hl,coloraddresswigglerhead1turnright  ;color address
  ld    bc,characteraddresswigglerhead1turnright  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  

.setspriteheadpissed:

  ld a,r
  and 1
  jp z,.random4


  ld    hl,coloraddresswigglerhead2turnleft  ;color address
  ld    bc,characteraddresswigglerhead2turnleft  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.random4:

  ld    hl,coloraddresswigglerhead2turnright  ;color address
  ld    bc,characteraddresswigglerhead2turnright  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset
  





MovementPattern301:     ;wiggler (helper objects)

  ld a,(ix+v2)
  and a
  jp nz,.handlebodypiece


  inc (ix+v7)
  ld a,(ix+v7)
;  cp wigglerturntime
  ld l,(ix+objectrelease)
  cp l
  jp nc,destroyobject

  call addv1toxbject

  ret

.dec2:

  dec (ix+v7)
  ret


.handlebodypiece:


  ld a,(framecounter)
  and 3
  call z,.animatebodypiece


  dec (ix+v7)
  ld a,(ix+v4)
  and a
  call nz,.dec2
  ld a,(ix+v7)
  and a
  call z,.turn

  inc (ix+hitpoints)
  ld a,(ix+hitpoints)
;  cp wigglerturntime
  ld l,(ix+objectrelease)
  cp l 
  jp z,destroyobject

  call addv1toxbject


  ret

;reached the ledge now turn
.turn:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ld a,(ix+v5)
  xor 1
  ld (ix+v5),a



  ret


.animatebodypiece:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v4)
  and a
  jp nz,.animatepissed


  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  jp z,.position1


  ld    hl,coloraddresswigglerbody1turnleft1  ;color address
  ld    bc,characteraddresswigglerbody1turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.position1:

  ld    hl,coloraddresswigglerbody1turnleft2  ;color address
  ld    bc,characteraddresswigglerbody1turnleft2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.animatepissed:


  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  jp z,.position1p


  ld    hl,coloraddresswigglerbody2turnleft1  ;color address
  ld    bc,characteraddresswigglerbody2turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.position1p:

  ld    hl,coloraddresswigglerbody2turnleft2  ;color address
  ld    bc,characteraddresswigglerbody2turnleft2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



;wigglerturntime:  equ 24
offsetwiggler:          dw 000-20,000, 000-20,000, 016-20,000, 016-20,000


MovementPattern300:     ;wiggler

;  ld (ix+v4),1 ;debug ;=pissed

  ;wiggler can have a few special states. It can become angry which will cause it to follow mario. (use special pattern??) it can turn around which is coding science by itself and it can die....
  
  ;use v5 as directional pointer
  ld a,(ix+v7)
  and a
  jp nz,.wait
 
 
;todo: detect object trown at it and die 
;TODO: being eaten by yoshi causes some nasty problems  
  call objectinteract
  cp 2
  jp z,.otherobjectdeathjump

 
 
  call mariointeract
  call c,.nextframe
  
  ld a,(framecounter)
  and 3
  call z,.animate

  ld a,(tonguehold) ;check if grabbed by yoshi and change state
  and a
  call nz,.checktonguehold


  call addv1toxbject


  ld a,(ix+v5)
  and a
  jp z,.checkleft

.checkright:

  ld de,40
  ld bc,17
  call objecttilecheckfromrom
  cp 192
  jp c,.turn

  ld a,(ix+v4)
  and a
  ret z

;check mario and turn if nario is on the other side

  call getmarioleftright
  jp nc,.turn


  ret

.checkleft:

  ld de,0
  ld bc,17
  call objecttilecheckfromrom
  cp 192
  jp c,.turn

  ld a,(ix+v4)
  and a
  ret z

  call getmarioleftright
  jp c,.turn


  ret


.otherobjectdeathjump:

  ;die by object ;only by the most deadly types
  
  ;simply change to new movementpattern and handle everything from there.
  ld hl,302
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld (ix+v7),5
  ld a,(ix+v4)
  ld (ix+v3),a ;parse into v3 and tell if pissed or not
  ld (ix+v4),0

  call removeobjectspat ;remove sprite from game
  call removefromsprlst; remove the sprite from the engine list
  ld (ix+amountspr),0 ;this will change the object to a spriteless object which means the engine may treat it differently
  ld (ix+edible),0 ;non edible


  ret


;check if yoshi grabbed wiggler. If so turn wiggler into a falling object.
.checktonguehold:

  push ix
  pop hl
  ld de,(tongueiy)
  xor a
  sbc hl,de
  ret nz
;do the change
  ld hl,150 ;use the falling blocks physics from iggys castle
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld (ix+v3),0
  ld (ix+v4),1
  ld (ix+deadly),0 ;make sure that mario doesn't get kicked off yoshi
  
  inc sp ;bail out of the call
  inc sp

  ret

;do nothing until turned around
.wait:

  call mariointeract ;make sure that you can still bounce off but no other interactions can occur at this point

  ld a,(ix+v7)
  inc (ix+v7)
;  cp wigglerturntime-1
  ld l,(ix+objectrelease)
  dec l
  cp l
  ret c

  ld (ix+edible),1 ;make edible again
  ld (ix+v7),0 ;reset timer
  ld b,8
  call increasespritecount
  
  call .animate ;call the animation code so that the correct sprites are set and updated


  ret


.nextframe:

  ld a,(ix+v4) ;state allready changed
  and a
  ret nz
  
  ld (ix+objectrelease),12 ;set turnspeed
  
  ;change state
  inc (ix+v4)
  ld a,(ix+v5)
  dec a
  jp z,.nextframe2 ;check if going left or right
  
  dec (ix+v1)

  ret

.nextframe2:


  inc (ix+v1)
  
  ret



;very special code over here. When wiggler turns remove all his sprites from the spriteslist and make him spriteless. In place spawn 4 objects that do the turning motion.
;the objects have a seperate handling code so perform the turn and wait until finished which is timer based.
.turn:


  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ld a,(ix+v5)
  xor 1
  ld (ix+v5),a

;  ld a,(ix+amountspr)
;  and a
;  ret z ;never ever do this TWICE!!! this piece of code can safely be removed

  call removeobjectspat ;remove sprite from game
  call removefromsprlst; remove the sprite from the engine list
  ld (ix+amountspr),0 ;this will change the object to a spriteless object which means the engine may treat it differently
  ld (ix+edible),0 ;non edible

  inc (ix+v7) ;start turn timer

;spawn the turn objects

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec hl
  inc hl
  
  ld a,(ix+v5)
  and a
  jp nz,.skipinchl
  
  inc hl
  inc hl

.skipinchl:
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push de
  push hl

  exx
  ld c,(ix+v1)
  ld b,(ix+v5) ;push values for parsing
  ld e,(ix+v4)
  ld d,8
  ld l,(ix+objectrelease)
  exx


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,301
   ld (ix+movepat),l
   ld (ix+movepat+1),h
  
  exx
  ld (ix+v1),c
  ld (ix+v5),b ;retrieve values
  ld (ix+v4),e
  ld (ix+objectrelease),l
  ld (ix+edible),0
  exx
  
  call .setturnsprite

  pop ix

  pop hl
  pop de


;invert body part X1 movement
  exx
  ld a,c
  neg
  ld c,a
  exx

  ld b,3


.spawnbodyparts:

  push bc

;spawn the rest of the body parts
  call .increasedecreasex



  push de
  push hl

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,301
   ld (ix+movepat),l
   ld (ix+movepat+1),h
  
  ld (ix+v2),1 ;set as body part this dirty bad way
  exx
  ld (ix+v1),c
  ld (ix+v5),b ;retrieve values
  ld (ix+v4),e
  ld (ix+v7),d
  ld a,d
  add a,8
  ld d,a
  ld (ix+hitpoints),0 ;body parts must dissapear simultaneously
  ld (ix+objectrelease),l  ;parse turntime
  ld (ix+edible),0 ;non edible parts  
  exx
  
  call .setbodypartsprite

  pop ix

  pop hl
  pop de

  pop bc

  djnz .spawnbodyparts

  ret



.setbodypartsprite:

  ld a,(ix+v4)
  and a
  jp nz,.setbodypartspritepissed

  ld    hl,coloraddresswigglerbody1turnleft1  ;color address
  ld    bc,characteraddresswigglerbody1turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.setbodypartspritepissed:


  ld    hl,coloraddresswigglerbody2turnleft1  ;color address
  ld    bc,characteraddresswigglerbody2turnleft1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



;set the next objects coordinates
.increasedecreasex:

  ld a,(ix+v5)
  and a
  jp z,.decreasehl


  inc hl


  ret


.decreasehl:


  dec hl
  ret



;called from foreingh object!
.setturnsprite:

  ld a,(ix+v4)
  and a
  jp z,.increasespr

  ld a,(ix+v5)
  and a
  jp z,.setturnspriteright


  ld    hl,coloraddresswigglerhead2turnleft  ;color address
  ld    bc,characteraddresswigglerhead2turnleft  ;character address
  ld    de,offsetmin4
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.setturnspriteright:

  ld    hl,coloraddresswigglerhead2turnright  ;color address
  ld    bc,characteraddresswigglerhead2turnright  ;character address
  ld    de,offsetmin4
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

;the normal head is two sprites!
.increasespr:

  ld b,4
  call increasespritecount

;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v5)
  and a
  jp z,.setturnspriteright2


  ld    hl,coloraddresswigglerhead1turnleft  ;color address
  ld    bc,characteraddresswigglerhead1turnleft  ;character address
  ld    de,offsetwiggler
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.setturnspriteright2:

  ld    hl,coloraddresswigglerhead1turnright  ;color address
  ld    bc,characteraddresswigglerhead1turnright  ;character address
  ld    de,offsetwiggler
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


;  ret



.animate:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(ix+v4)
  dec a
  jp z,.animatepissed

  ld a,(ix+v5)
  dec a
  jp z,.animateright


   ld a,(ix+v3)
   inc (ix+v3)
   and 3
;   and a
;   jp z,.position0
   dec a
   jp z,.position1
   dec a
   jp z,.position2   
   dec a
   jp z,.position3    
  
  ld    hl,coloraddresswiggler1left  ;color address
  ld    bc,characteraddresswiggler1left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.position1:

  ld    hl,coloraddresswiggler2left  ;color address
  ld    bc,characteraddresswiggler2left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.position2:

  ld    hl,coloraddresswiggler3left  ;color address
  ld    bc,characteraddresswiggler3left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.position3:


  ld    hl,coloraddresswiggler4left  ;color address
  ld    bc,characteraddresswiggler4left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



  ;ret


.animateright:

  ld a,(ix+v3)
   inc (ix+v3)
   and 3
;   and a
;   jp z,.position0
   dec a
   jp z,.position1right
   dec a
   jp z,.position2right   
   dec a
   jp z,.position3right    
  
  ld    hl,coloraddresswiggler1right  ;color address
  ld    bc,characteraddresswiggler1right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.position1right:

  ld    hl,coloraddresswiggler2right  ;color address
  ld    bc,characteraddresswiggler2right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.position2right:

  ld    hl,coloraddresswiggler3right  ;color address
  ld    bc,characteraddresswiggler3right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.position3right:


  ld    hl,coloraddresswiggler4right  ;color address
  ld    bc,characteraddresswiggler4right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



  ;ret


;wiggler is pissed off


.animatepissed:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v5)
  dec a
  jp z,.animatepissedright


   ld a,(ix+v3)
   inc (ix+v3)
   and 3
;   and a
;   jp z,.positionpissed0
   dec a
   jp z,.positionpissed1
   dec a
   jp z,.positionpissed2   
   dec a
   jp z,.positionpissed3    
  
  ld    hl,coloraddresswigglerpissed1left  ;color address
  ld    bc,characteraddresswigglerpissed1left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.positionpissed1:

  ld    hl,coloraddresswigglerpissed2left  ;color address
  ld    bc,characteraddresswigglerpissed2left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.positionpissed2:

  ld    hl,coloraddresswigglerpissed3left  ;color address
  ld    bc,characteraddresswigglerpissed3left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.positionpissed3:


  ld    hl,coloraddresswigglerpissed4left  ;color address
  ld    bc,characteraddresswigglerpissed4left  ;character address
  ld    de,offsetwigglerleft
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



  ;ret


.animatepissedright:

  ld a,(ix+v3)
   inc (ix+v3)
   and 3
;   and a
;   jp z,.positionpissed0
   dec a
   jp z,.positionpissed1right
   dec a
   jp z,.positionpissed2right   
   dec a
   jp z,.positionpissed3right    
  
  ld    hl,coloraddresswigglerpissed1right  ;color address
  ld    bc,characteraddresswigglerpissed1right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.positionpissed1right:

  ld    hl,coloraddresswigglerpissed2right  ;color address
  ld    bc,characteraddresswigglerpissed2right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.positionpissed2right:

  ld    hl,coloraddresswigglerpissed3right  ;color address
  ld    bc,characteraddresswigglerpissed3right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.positionpissed3right:


  ld    hl,coloraddresswigglerpissed4right  ;color address
  ld    bc,characteraddresswigglerpissed4right  ;character address
  ld    de,offsetwigglerright
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



  ;ret





MovementPattern299:     ;ludwig as shell  

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  call c,.makenoise
  
  inc (ix+v2)
  ld a,(ix+v2)
  cp 15
  ret c ;abuse v2 as timer
  cp 80
  jp nc,.return
  
  ld a,(framecounter)
  and 3
  call z,.animate


  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c

  ld (ix+v7),16


  call addv1toxbject
 
  call acceleraterainbowshellludwig

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  xor a
  sbc hl,de
  ret nc ;prevent exiting of outer left map
  
  ld hl,8
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  ret

.makenoise:

  call Playsfx.chuckstomp

  ret

.return:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld hl,297
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ld    hl,coloraddressludwigleft1  ;color address
  ld    bc,characteraddressludwigleft1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  call    setcharcolandoffset  
  
  
  call getmarioleftright
  ret nc
  
  ld (ix+v3),1
  
  ;insert animation here
  ld    hl,coloraddressludwigright1  ;color address
  ld    bc,characteraddressludwigright1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  call    setcharcolandoffset   
  

  ret


.animate:

  ld a,(ix+objectrelease)
  inc a
  and 7
  ld (ix+objectrelease),a
  
  ;insert animation here
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  
  and a
  jp z,.animate0
  cp 1
  jp z,.animate1
  cp 2
  jp z,.animate2
  cp 3
  jp z,.animate0
  cp 4
  jp z,.animate1
  cp 5
  jp z,.animate2
  cp 6
  jp z,.animate0
  cp 7
  jp z,.animate1

  ret
  
.animate0:  
  
  ld    hl,coloraddressludwigshell1  ;color address
  ld    bc,characteraddressludwigshell1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
.animate1:  
  
  ld    hl,coloraddressludwigshell2  ;color address
  ld    bc,characteraddressludwigshell2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
.animate2:  
  
  ld    hl,coloraddressludwigshell3  ;color address
  ld    bc,characteraddressludwigshell3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    
  
;  ret



acceleraterainbowshellludwig:


  call getmarioleftright
  jp c,.acceleratemoleright


.acceleratemoleleft:

  ld a,(ix+v4)
  and a
  ret z
  
  dec (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,rainbowshellacctableludwig
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  

  ret


.acceleratemoleright:

  ld a,(ix+v4)
  cp 22
  ret z
  
  inc (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,rainbowshellacctableludwig
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a



  ret



rainbowshellacctableludwig:   db -4,-3,-2,-1,-2,-1,-2,-1,-1,-1,0,0,0,1,1,1,2,1,2,1,2,3,4

  

  
  
MovementPattern298:     ;ludwig's fireball


  call mariointeract

  call addv1toxbject


  ld a,(framecounter)
  and 7
  and a
  jp z,.animate0
  cp 4
  jp z,.animate1

  ret

.animate0:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld a,(ix+v3)
  and a
  jp nz,.animate0right
  
  ld    hl,coloraddressludwigfireball1left  ;color address
  ld    bc,characteraddressludwigfireball1left  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset

.animate0right:

  ld    hl,coloraddressludwigfireball1right  ;color address
  ld    bc,characteraddressludwigfireball1right  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset




.animate1:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp nz,.animate1right  
  
  
  ld    hl,coloraddressludwigfireball2left  ;color address
  ld    bc,characteraddressludwigfireball2left  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


.animate1right:

  ld    hl,coloraddressludwigfireball2right  ;color address
  ld    bc,characteraddressludwigfireball2right  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



 ; ret


  

MovementPattern297: ;ludwig (boss)
;using v3 as left or right

  call mariointeract
  call c,.nextframe
  

  ld a,(ix+v4)
  dec a
  jp z,.spit ;spit fireball 1
  dec a
  jp z,.walk ;walk towards mario 2
  dec a
  jp z,.jump ;jump towards mario 3
  dec a
  jp z,.wait ;wait 4
  dec a
  jp z,.turnleft ;turn left towards mario 5
  dec a
  jp z,.turnright ;turn right towards mario 6
  dec a
  jp z,.fall ;DIE!!! 7
  dec a
  jp z,.endlevel; the end 8
  

  call getmariodistance
  
  ld a,c
  cp 80
  ret nc


  call .incv4 ;set state to 2


  ret

  
.setfadeout:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  ret

.endlevel:



  ld l,(ix+v5)
  ld h,(ix+v5+1)
  dec hl
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld de,110
  xor a
  push hl
  sbc hl,de
  pop hl
  jp z,.setfadeout
  ld a,l
  or h
  ret nz




;TODO: build in exit scene  
  ld a,1
  ld (levelend?),a



  ret
  
  
.fall:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  cp 40
  jp nc,.setend
  
  call addv2toybject


  ret
  
.setend:

  ld hl,300
  ld (ix+v5),l ;use this as timer.... :(
  ld (ix+v5+1),h

  xor a
  ld (time),a

  ld a,2
  ld (playwinmusic),a

  call .incv4
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigjump1  ;color address ;03
  ld    bc,emptysprite94 ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset  

  ;ret

  
  
  
.nextframe:

  ld a,(ix+v4)
  cp 3
  ret z ;ignore interaction during jump
  cp 6
  ret nc ;no interaction during deathphase!!

  dec (ix+hitpoints)
  jp z,.die
;set animation and go into MovementPattern299
  
  call Playsfx.chuckauw
  
  ld hl,299
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v2),0
  ld (ix+v1),0
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigjump1  ;color address ;03
  ld    bc,characteraddressludwigjump1 ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset   
 
 
;  ret
  
.die: ;hit three times and thus die

  call Playsfx.bossdies

  ld (ix+v4),7
  ld (ix+v7),0
  ld (ix+v2),8
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigjump1  ;color address ;03
  ld    bc,characteraddressludwigjump1 ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
  ;ret
  
  
  
  
.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  ret c
  
  ld (ix+v4),0
  ld (ix+v7),0



  ret
  
  
  
;generic jump code check (move acording to preset v1)  
.jump:

  call addv1toxbject
  
  ld a,(ix+v7)
  inc (ix+v7)
  ld hl,.ludwigjumptable
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.setincv4
  
  ld (ix+v2),a
  call addv2toybject
  

  ret
  
  
.ludwigjumptable:	db -8,-8,-6,-6,-6,-4,-4,-3,-3,-3,-2,-1,-1,0,1,1,2,3,3,3,4,4,6,6,6,8,8,10  


.setincv4:
  
  ld a,1
  ld (tripscreen),a
  
  call Playsfx.explosion

  jp .incv4
  
.turnleft:



  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  jp nc,.setleft



  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigturnleft  ;color address ;03
  ld    bc,characteraddressludwigturnleft ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 



;  ret

  
.turnright:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  jp nc,.setright



  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigturnright  ;color address ;03
  ld    bc,characteraddressludwigturnright  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 



;  ret

.setright:

  ld (ix+v4),2
  ld (ix+v3),1
  ld (ix+v7),0

  ret

.setleft:

  ld (ix+v4),2
  ld (ix+v3),0
  ld (ix+v7),0

  ret
  
.checkleftright:

  ld a,(ix+v3)
  and a
  jp z,.checkright ;check right side because ludwig stands to the left 
  
;checkleft:  
  
  call getmarioleftright
  ret c ;mario is on the right
  
  ;insert code here change state to left 5
  ld (ix+v4),5
  ld (ix+v7),0
  
  
  ret
  
  
  
;check right side  
.checkright:

  call getmarioleftright
  ret nc

  ;insert code here change state to right 6
  ld (ix+v4),6
  ld (ix+v7),0

  ret
  
;change sprite then fix v4
.setjump:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  ld    hl,coloraddressludwigjump2  ;color address ;03
  ld    bc,characteraddressludwigjump2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  call .incv4
  
  ld (ix+v1),-2
  
  ld a,(ix+v3)
  and a
  ret z
  
  ld (ix+v1),2
  
  ret

  
  
;walk towards mario and turn if mario jumps over
.walk:

  call .checkleftright ;check for leftright and turn if mario is on the other side. When turning the state is completely different and thus must be forced


;stop this at a certain moment
  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  jp nc,.setjump



 
  ld a,(framecounter)
  and 7
  jp z,.makestep1
  cp 4
  jp z,.makestep0
 
  ret

.makestep0:
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld a,(ix+v3)
  and a
  jp nz,.makestep0right ;move right if turned
  
  
  ld (ix+v1),-2
  call addv1toxbject
  
  
  ld    hl,coloraddressludwigleft1  ;color address ;03
  ld    bc,characteraddressludwigleft1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.makestep0right:

  ld (ix+v1),2
  call addv1toxbject

  ld    hl,coloraddressludwigright1  ;color address ;03
  ld    bc,characteraddressludwigright1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.makestep1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

  ld a,(ix+v3)
  and a
  jp nz,.makestep1right ;move right if turned
  
  ld    hl,coloraddressludwigleft2  ;color address ;03
  ld    bc,characteraddressludwigleft2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.makestep1right:

  ld    hl,coloraddressludwigright2  ;color address ;03
  ld    bc,characteraddressludwigright2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset



.incv4:

  inc (ix+v4)
  ld (ix+v7),0
  ret



.spit:

  ld a,(ix+v3)
  dec a
  jp z,.spitright

  ;open mouth in several steps then spit fireball
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 2
  jp z,.spit1
  cp 10
  jp z,.spit2
  cp 20
  jp z,.spit3
  cp 30
  jp z,.incv4
  
  
  ret
  

.spit1:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigleftattack1  ;color address ;03
  ld    bc,characteraddressludwigleftattack1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.spit2:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigleftattack2  ;color address ;03
  ld    bc,characteraddressludwigleftattack2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.spit3:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigleftattack3  ;color address ;03
  ld    bc,characteraddressludwigleftattack3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  call    setcharcolandoffset   

  call Playsfx.yoshifireball
;spawn fireball
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),181                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,298
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),-2
   ld (ix+deadly),2
   ld (ix+clipping),0
   ld (ix+v3),0

  pop ix

  ret

;TODO: finish spitleft and copy in right
.spitright:


  ;open mouth in several steps then spit fireball
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 2
  jp z,.spit1r
  cp 10
  jp z,.spit2r
  cp 20
  jp z,.spit3r
  cp 30
  jp z,.incv4
  
  
  ret
  

.spit1r:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigrightattack1  ;color address ;03
  ld    bc,characteraddressludwigrightattack1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.spit2r:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigrightattack2  ;color address ;03
  ld    bc,characteraddressludwigrightattack2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 

.spit3r:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
  
  ld    hl,coloraddressludwigrightattack3  ;color address ;03
  ld    bc,characteraddressludwigrightattack3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock6
  call    setcharcolandoffset   

  call Playsfx.yoshifireball
;spawn fireball
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
 ; dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),181                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,298
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),2
   ld (ix+deadly),2
   ld (ix+clipping),0
   ld (ix+v3),1

  pop ix

  ret


MovementPattern296:     ;switchbox for maplevel04-06b

  ld a,(ix+v7)
  and a
  jp z,.init

  ld a,(ix+v4)
  and a
  jp nz,.handlepop

  call mariointeract
  call c,.handleswitchboxinteract

  ret


.handlepop:

  cp 6
  ret nc ;disable

  inc (ix+v4)

  cp 5
  jp nc,.popdone
  cp 3
  jp nc,.popdown


  ld (ix+v2),-2
  jp addv2toybject

;  ret

.popdown:

  ld (ix+v2),2
  jp addv2toybject 
  
;  ret


.popdone:

  inc (ix+v4)


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex445  ;color address ;03
  ld    bc,spriteaddrex445c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 


;  ret


.handleswitchboxinteract:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox

  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c
  
;set pop mode
  ld (ix+v4),1

  ld a,1
  ld (switchon),a
  
  ret
  

;do a few thing. Make sure that the stairs dissapear. So it looks less buggy. Exchange the stairs for lava.
;This means we need to call in the map and do some things to it
.init:

;make sure we don't see a thing about this
  call getmariodistance
  ld a,c
  cp 80
  ret nc

;get the mapdata
  ld a,(slot.page2rom)
  out ($a8),a
  
  ld hl,$5E26 ;mapadress of our start shit

  ld c,12 ;amount of rows
  
.rowloop:  

  ld b,46 ;x tiles per row

  push hl
    
.xloop:
  ;start adress is correct
  xor a ;bkg
  ld (hl),a
  inc hl
  djnz .xloop

  pop hl
  ld de,(maplenght)
  add hl,de
  
  dec c
  
  jp nz,.rowloop
  
  ;make the upper lava line
  ld b,23 ;x tiles per row

  push hl
    
.xloop2:
  ;start adress is correct
  ld a,175 ;make tile 0 after correct test
  ld (hl),a
  inc hl
  ld a,176 ;make tile 0 after correct test
  ld (hl),a 
  inc hl 
  djnz .xloop2

  pop hl  
  ld de,(maplenght)
  add hl,de
  ;make the under lava line
  ld b,23 ;x tiles per row

  push hl
    
.xloop3:
  ;start adress is correct
  ld a,177 ;make tile 0 after correct test
  ld (hl),a
  inc hl
  ld a,178 ;make tile 0 after correct test
  ld (hl),a 
  inc hl 
  djnz .xloop3

  pop hl  
  ld de,(maplenght)
  add hl,de
;make the lava
  ld c,12 ;amount of rows
  
.rowloop4:  

  ld b,46 ;x tiles per row

  push hl
    
.xloop4:
  ;start adress is correct
  ld a,68 ;make tile 0 after correct test
  ld (hl),a
  inc hl
  djnz .xloop4

  pop hl
  ld de,(maplenght)
  add hl,de
  
  dec c
  
  jp nz,.rowloop4
  

  inc (ix+v7) ;only make this happen once



  ret
  
  
  
;maplevel04-06b tiledata
spikedrop:                      incbin "../grapx/world4/level4-06a/animation.SC2",$0000+7,8*64  ;character table
spikedropclr:                   incbin "../grapx/world4/level4-06a/animation.SC2",$2000+7,8*64  ;color table 


;helper object that brings the entire huge wall down on mario and goes all the way up when the on/off box is pressed.
;interacts with special maplevel04-06b on/off box
MovementPattern295:     ;maplevel04-06b (helper object)

  ld a,(ix+v4)
  and a
  jp z,.init

;reset this byte
  xor a
  ld (forcesetbackground),a
  
  call .followmario
  call .murdermarioiflava
  call animateludwiglava  
  call animate0406block
  
  ld a,(framecounter)
  and 1
  jp z,.skip
  ld hl,(pillary)
  push hl
  ld de,28*8
  xor a
  sbc hl,de
  pop hl ;stop when reaching the bottom
  jp z,.handlesidetouch
  inc hl
  ld a,(switchon)
  and a
  call nz,.dechl  
  ld (pillary),hl

.skip:  
  
;switch page 1 to map
  ld a,(slot.page2rom)
  out ($a8),a

  ld de,(pillary) ;global for pillarheight Y 16bits pixel precise
  
  srl d ;nu moet je hl delen door 8
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8
  ;we now have the height of the object
  
  ;now find the mapadress
  ;----> tabel pointer setten
;  ld d,0
;  ld e,l
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found  

  ld de,13
  add hl,de
  ;X adress found

;write the tiles directly onto the map in ram cannot get faster than this. 
;primary blank line
  push hl
  
  ld b,147
  ld a,232
.line1a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line1a
  
  pop hl
  push hl
  inc hl

  ld b,147
;  ld a,155
.line1b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line1b

;second line  
  pop hl
  ld de,(maplenght)
  add hl,de
  push hl

  ld b,147
  ld a,153 
.line2a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line2a  
 
  pop hl
  push hl
  inc hl

  ld b,147
  inc a
;  ld a,154 
.line2b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line2b   
  
;third line
  pop hl
  ld de,(maplenght)
  add hl,de
  push hl

  ld b,147
  ld a,151 
.line3a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line3a  
 
  pop hl
  push hl
  inc hl

  ld b,147
;  ld a,152 
  inc a
.line3b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line3b  

  pop hl
  ld de,(maplenght)
  add hl,de
  push hl

  ld b,147
  ld a,149 
.line4a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line4a  
 
  pop hl
  push hl
  inc hl

  ld b,147
;  ld a,150 
  inc a
.line4b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line4b 
  
;fifth line

  ld hl,(pillary)
  ld de,28*8
  xor a
  sbc hl,de
  jp nc,.stop

  pop hl
  ld de,(maplenght)
  add hl,de
  push hl

  ld b,147
  ld a,147 
.line5a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line5a  
 
  pop hl
  push hl
  inc hl

  ld b,147
;  ld a,148 
  inc a
.line5b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line5b 

;sixt line

  ld hl,(pillary)
  ld de,26*8
  xor a
  sbc hl,de
  jp nc,.stop

  pop hl
  ld de,(maplenght)
  add hl,de
  push hl

  ld b,147
  xor a
.line6a:
  ld (hl),a
  inc hl
  inc hl
  djnz .line6a
  
  pop hl
  inc hl

  ld b,147
;  xor a
.line6b:
  ld (hl),a
  inc hl
  inc hl
  djnz .line6b



  ret


;return but also push mario back if he gets too far to the right

.handlesidetouch:

  ld hl,(mariox)
  ld de,13*8
  xor a
  sbc hl,de
  ret c
  
  ld hl,(mariox)
  
  ld de,12
  xor a
  sbc hl,de
 
    ld (mariox),hl


  ret



.stop:

  pop hl
  ret

.dechl:

  dec hl
  dec hl

  push hl
  ld hl,(pillary)
  ld de,8*8
  xor a
  sbc hl,de
  pop hl
  jp c,.reverse

  ret

.reverse:

  xor a
  ld (switchon),a
  ret

;check lava but check for bkg as well. blocks should crush mario
.murdermarioiflava:


  ld de,0
  ld b,1
  call checkgeneraltilefromhrom
;check downward crush
  cp 149
  jp z,.kill
  cp 150
  jp z,.kill
  cp 175
  jp z,.kill
  cp 176
  jp z,.kill

  ret

.kill:

  
  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  ret 


.followmario: ;only follow for x. we handle Y separately

  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ret

.init:

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  ld hl,8*8
  ld (pillary),hl

  inc (ix+v4)

  ret


animate0406block:

  ld a,(pillary)
  and 7
  cp 0  
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
;  cp 7
;  jp z,.step7  
        
  
;  ret

  jp .step7

.writestep:


  ld a,147
  ld b,8
  call updateludwigblocks

  ret


.step0:

  ld hl,spikedrop+(8*0)
  ld de,spikedropclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,spikedrop+(8*8)
  ld de,spikedropclr+(8*8)   

  jp .writestep

.step2:

  ld hl,spikedrop+(8*16)
  ld de,spikedropclr+(8*16)   

  jp .writestep

.step3:

  ld hl,spikedrop+(8*24)
  ld de,spikedropclr+(8*24)   

  jp .writestep

.step4:

  ld hl,spikedrop+(8*32)
  ld de,spikedropclr+(8*32)   

  jp .writestep

.step5:

  ld hl,spikedrop+(8*40)
  ld de,spikedropclr+(8*40)   

  jp .writestep

.step6:

  ld hl,spikedrop+(8*48)
  ld de,spikedropclr+(8*48)  

  jp .writestep
  
.step7:

  ld hl,spikedrop+(8*56)
  ld de,spikedropclr+(8*56)   

  jp .writestep
  



;IN a = tilenumber IN = HL tilegfx adress DE = Tilegfx color address Changes all registers including the alternate ones
;b = in case of a loop
updateludwigblocks:

  push bc
  exx 
  pop bc ;get bc in exx

  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
 


  
  ld a,(framecounter)
  and 1
  jp z,.doshit
  
  ld de,$4000
  add hl,de
 ; jp .doshit



 ;ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl


  ld c,$98
  push bc
.write1:  
  
  push bc
  call outix8 ;outi modifies bc
  pop bc
  djnz .write1
  pop bc
  
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0

  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

  ld c,$98
    
.write2:   
  push bc
  call outix8
  pop bc
  djnz .write2

  exx

  ret



animateludwiglava:


  ld    a,animationtilesblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtiles0211a+(8*04)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0211aclr+(8*04)             ;rotating tile animation 1 character address in ro
  ld a,175 
  jp .updateblocks 

.step3:
  ld hl,animationtiles0211a+(8*19)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0211aclr+(8*19)             ;rotating tile animation 1 character address in ro
  ld a,175  
  jp .updateblocks 

.step2:
  ld hl,animationtiles0211a+(8*34)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0211aclr+(8*34)             ;rotating tile animation 1 character address in ro
  ld a,175  
  jp .updateblocks 

.step1:
  ld hl,animationtiles0211a+(8*49)             ;rotating tile animation 1 character address in ro
  ld de,animationtiles0211aclr+(8*49)             ;rotating tile animation 1 character address in ro
  ld a,175     

.updateblocks:

  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,lemmyblock+(8*144)         ;rotating tile animation 1 character address in rom
  ld c,$98
  call outix88
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,lemmyblockclr+(8*144)             ;rotating tile animation 1 character address in rom
    ld c,$98
  call outix88

  ret




