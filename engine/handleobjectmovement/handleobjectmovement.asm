;handle object movement
;well the name says it all :)

;Page1-2 uit de ROM
;WARNING!!: This code is executed solely from page 1-2ROM. This means that if you jump out of this code to any place
;in de engine, you will get unexpected behaviour and most propably a gigantic system crash.
;only return with $ret command!

;end = 79


HandleObjectMovement:


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz


;cheat routine to spawn a yoshi
 ; ld a,(test)
 ; dec a
 ; call z,makeyoshi

 
  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)
 

;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress
;  add   hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress:
  jp    MovementPattern00
  jp    MovementPattern01     ;shell
  jp    MovementPattern02     ;rex
  jp    MovementPattern03     ;rex word klein
  jp    MovementPattern04     ;rex klein
  jp    MovementPattern05     ;rex dood
  jp    MovementPattern06     ;rex alternatief dood 
  jp    MovementPattern07     ;shell knalt weg  
  jp    MovementPattern08     ;piranja plant
  jp    MovementPattern09     ;piranja plant gaat naar beneden
  jp    MovementPattern10     ;piranja plant wacht
  jp    MovementPattern11     ;shellhold
  jp    MovementPattern12     ;shell knalt weg omhoog
  jp    MovementPattern13     ;shell valt naar beneden
  jp    MovementPattern14     ;rotating block, question mark block, exclamation mark block
  jp    MovementPattern15     ;brown block
  jp    MovementPattern16     ;red mushroom
  jp    MovementPattern17     ;fire powerup
  jp    MovementPattern18     ;feather
  jp    MovementPattern19     ;star
  jp    MovementPattern20     ;green mushroom
  jp    MovementPattern21     ;shell stuitert tegen de grond
  jp    MovementPattern22     ;wolkje na een kill
  jp    MovementPattern23     ;schelpje word doodgetikt
  jp    MovementPattern24     ;schelpje word omhooggetikt vanuit 07
  jp    MovementPattern25     ;sliding guy
  jp    MovementPattern26     ;sliding guy walking around
  jp    MovementPattern27     ;any object with one deathsprite dying
  jp    MovementPattern28     ;checkpointbar
  jp    MovementPattern29     ;sterretje bij een kill 
  jp    MovementPattern30     ;kogeltje
  jp    MovementPattern31     ;destroyblockdebris
  jp    MovementPattern32     ;moonpowerup
  jp    MovementPattern33     ;charging chuck (sitting still)
  jp    MovementPattern34     ;charging chuck (running)
  jp    MovementPattern35     ;charging chuck (stunned)
  jp    MovementPattern36     ;charging chuck (jumping)
  jp    MovementPattern37     ;snapping chuck (sitting still)
  jp    MovementPattern38     ;snapping chuck (jumping)
  jp    MovementPattern39     ;questionmark sprite
  jp    MovementPattern40     ;questionmark spawn object
  jp    MovementPattern41     ;checkpointpole
  jp    MovementPattern42     ;exclamationmark
  jp    MovementPattern43     ;mario exit tube coverup
  jp    MovementPattern44     ;mario exit tube coverup pulling mario in
  jp    MovementPattern45     ;mariostar support
  jp    MovementPattern46     ;mariodrop support
  jp    MovementPattern47     ;flying box
  jp    MovementPattern48     ;flying box release object
  jp    MovementPattern49     ;smileybox
  jp    MovementPattern50     ;powerpopup
  jp    MovementPattern51     ;yellowdestructionstar
  jp    MovementPattern52     ;yellowdestructionstar high
  jp    MovementPattern53     ;powerreserve
  jp    MovementPattern54     ;powerreservedrop
  jp    MovementPattern55     ;1upsprite
  jp    MovementPattern56     ;banzai bill
  jp    MovementPattern57     ;banzai bill (part2)
  jp    MovementPattern58     ;banzai bill stardebris
  jp    MovementPattern59     ;banzai bill stardebris high
  jp    MovementPattern60     ;banzai bill destruction debris
  jp    MovementPattern61     ;pinkshell that goes into tube with mario
  jp    MovementPattern62     ;funny mushroom
  jp    MovementPattern63     ;funny balloon
  jp    MovementPattern64     ;tubecoverup for mario comming in down
  jp    MovementPattern65     ;empty
  jp    .return                ;endflagtop relocation marker
  jp    MovementPattern67     ;yoshicoin
  jp    MovementPattern68     ;empty
  jp    MovementPattern69     ;coin (moving)
  jp    MovementPattern70     ;coin release (from box)
  jp    MovementPattern71     ;coin itself (release from box)
  jp    MovementPattern72     ;yoshi egg
  jp    MovementPattern73     ;koopa
  jp    MovementPattern74     ;naked coopa
  jp    MovementPattern75     ;naked coopa (shooting out of shell)
  jp    MovementPattern76     ;naked coopa (transforming)
  jp    MovementPattern77     ;naked coopa (jump in shell)
  jp    MovementPattern78     ;coopa (shell on ground)
  jp    MovementPattern79     ;coopa shell waitstate


  
.return:
  
  ret

timerespawn:

  ld a,(ix+deathtimer)
  cp 150;180
  ret c


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;;shake before respawn
;  ld l,(ix+xobject)
;  ld a,l
;  xor 1
;  ld l,a
;  ld (ix+xobject),l
 
  ld    hl,spriteaddrex415  ;color address
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetshell2
  ld    a,ingamespritesblock3
  call    setcharcolandoffset 
 
 
  ld a,(framecounter)
  and 1
  call z,.position0
  
  
;truly revert
  ld a,(ix+deathtimer)
  cp 180
  ret c


  ld (ix+movepat),180 ;revert back to cavekoopa
  ld (ix+v1),-1
  ld (ix+v3),0
  ld (ix+deadly),1
  ld (ix+deathtimer),0
  ld (ix+clipping),1
  
  xor a
  ld (marioshell),a
  
  inc sp
  inc sp ;return from call 
 

  ret


.position0:
  ld    hl,spriteaddrex415  ;color address
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



MovementPattern00:

  ld a,(framecounter)
  and 7
  ret z


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ret
  
MovementPattern01:              ;shell


  ld (ix+clipping),4 ;always revert to this value


  call objectinteract
  cp 2
  jp z,pinkshelldeathjump
  cp 4
  jp z,pinkshelldeathjump
  cp 3
  jp z,.jumpfromblock
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,pinkshellshotdeathjump


  call mariointeract
  jp c,.nextframe

;  call jumpblockcheck
;  jp c,.jumpfromblock

  ld a,(ix+objectrelease)
  cp 6
  call z,timerespawn

  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ld (ix+v2),0

  jp  checkobjecttilesfloor
;  ret nc
;  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

 ; ret




.jumpfromblock:
 ; ld (ix+clipping),3 ;change clipping to make other boxes jump as well

  ld a,12
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  ld (ix+v1),a
  ld a,8
  ld (ix+deathtimer),a

  ret

  
.nextframe:

  ld (ix+v1),0

  ld a,(marioflyslide)
  dec a
  jp z,pinkshelldeathjump

    ld a,(mariospinjump)
    dec a
    jp z,destroyobjectwithcloud
    
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
    

  ld a,(Controls)
  bit 5,a
  jp nz,.shellhold

.noshellhold:

  ld a,7
  ld (ix+movepat),a
 
  ld (ix+deathtimer),0 

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  
    call Playsfx.kick
  

;schelpje word nu dodelijk

  ld (ix+deadly),1
  ld (ix+clipping),2
  
;vervolgens tikken we het schelpje weg van mario dus zetten we zijn coordinaten aan de hand van de richting  



  call getmarioleftright      ;out c=mario is left of object
  jp nc,.kickright

  
;  ld a,(mariosheertile)
  ld a,(ix+sheertile)
  cp 2
  jp z,.kickupsheertileright



  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,-horshellspeed
  ld (ix+v1),a

  
  ret
  
  
.kickright:

;  ld a,(mariosheertile)
  ld a,(ix+sheertile)
  cp 1
  jp z,.kickupsheertileleft


.continue:

  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld a,horshellspeed
  ld (ix+v1),a


  ret



.shellhold:

;do not allow this item to be picked up when on yoshi

  ld a,(marioyoshi)
  dec a
  jp z,.noshellhold


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a

  ld a,11 ;special movement pattern
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  
  ld (ix+deadly),a              ;niet meer deadly
  ld (ix+clipping),2


  ld (ix+v3),3 ;reset position
  jp animateshell

;  ret


.kickupsheertileleft: 
  
  
;//////// omhoog
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,horshellspeed
  ld (ix+v1),a  
  neg
  ld (ix+v2),a


  ld a,1
  ld (ix+deadly),a
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
 
  
  call locksheertile.left
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret



.kickupsheertileright:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,-horshellspeed
  ld (ix+v1),a
;  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  call locksheertile.right
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 


  ret


horshellspeed: equ 5
vershellspeed: equ 1



;Ok rex is onze meest generieke character. Dit is dus mijn grote copy paste ding bij elk nieuw
;poppetje wat ik in de engine zet

;belangrijk is dat eerst de interacties worden bekeken
;dan die met mario
;dan de movement zowel y als x
;de framecounter word bekeken 
;animaties
;eventuele death etc etc sets

;het mooie is dat dit door iedereen te begrijpen is. Gewoon copy pasten en dingen weglaten of toevoegen
;naargelang wat de AI zou moeten doen
MovementPattern02:              ;rex

  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex00  ;color address
  ld    bc,spriteaddrex00c  ;character address
  ld    de,offsetsrex00
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex01  ;color address
  ld    bc,spriteaddrex01c  ;character address
  ld    de,offsetsrex01
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex02  ;color address ;02
  ld    bc,spriteaddrex02c  ;character address
  ld    de,offsetsrex02
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex03  ;color address ;03
  ld    bc,spriteaddrex03c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),3  ;generic set
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  call MovementPattern03.animate ;animeer
  


;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a


  ld a,16
  ld (ix+sizeY),a

;lower the sprite for better detection
   ld l,(ix+yobject)
   ld h,(ix+yobject+1)
   ld de,16
   add hl,de
   ld (ix+yobject),l
   ld (ix+yobject+1),h


  ld (ix+deadly),0

  call Playsfx.kickenemy

  ret


.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret


MovementPattern03: ;rex is plat aan het worden

    call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success


  ld    a,(ix+deathtimer)
  cp 3                      ;this sprite animates once every 4 frames
  jp  z,.nextframe

;  xor   a                       ;reset vertical movement speed
;  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  


  ret




.animate:
  

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex04  ;color address
  ld    bc,spriteaddrex04c  ;character address
  ld    de,offsetsrex04
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex04  ;color address
  ld    bc,spriteaddrex04c  ;character address
  ld    de,offsetsrex04
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex05  ;color address
  ld    bc,spriteaddrex05c  ;character address
  ld    de,offsetsrex05
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex05  ;color address
  ld    bc,spriteaddrex05c  ;character address
  ld    de,offsetsrex05
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.nextframe:

  
  ld (ix+movepat),4
 
   
;  ;remove 2 sprites from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist

  inc hl
  inc hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0
  inc   hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0   

  
;  ;/remove sprite from spritelist  
  
  ld (ix+amountspr),2 
  ld (ix+deadly),1 ;make deadly again
  
;  ld a,16
;  ld (ix+sizeY),a

;ook de sprite verplaatsen vanwege de offset

;   ld l,(ix+yobject)
;   ld h,(ix+yobject+1)
;   ld de,16
;   add hl,de
;   ld (ix+yobject),l
;   ld (ix+yobject+1),h
  
  
  ;platte rex word sneller
  
  ld a,(ix+v1)
  inc a
  cp 2
  jp nz,.negative
  ld (ix+v1),a
  jp .positive
.negative:  
  ld a,-2
  ld (ix+v1),a
  
.positive:
  
  ;eerst animeren dan pas SPAT kaduk maken
  call MovementPattern04.animate

;spat weghalen
  ld    l,(ix+spataddress)          
  ld    h,(ix+spataddress+1) 
  ld de,8
  add hl,de      
  ld (hl),226
;  inc hl
;  ld (hl),0
  ld de,4
  add hl,de
  ld (hl),226
;  inc hl
;  ld (hl),0
  
  ret


MovementPattern04:              ;rex plat


  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  
  
 
;  call  checksheerobjecttilecorner
;  ret c
  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
 
  
  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:
  
  
 
.hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    .animate

.animate:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex06  ;color address
  ld    bc,spriteaddrex06c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.position1:
  ld    hl,spriteaddrex07  ;color address
  ld    bc,spriteaddrex07c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.position2:
  
  ld    hl,spriteaddrex08  ;color address
  ld    bc,spriteaddrex08c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.position3:
  ld    hl,spriteaddrex09  ;color address
  ld    bc,spriteaddrex09c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  


.nextframe:

  ld (ix+v1),0

  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  xor a
  ld (ix+deathtimer),a

  ld (ix+movepat),5

;sprite zetten 
;  jp MovementPattern05.animate
 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;  ret
  call Playsfx.kickenemy


;rex gaat dood
MovementPattern05:

  ld a,(marioflyslide)
  dec a
  call z,.nextframe 

  ld a,(movedownsheertile)
  dec a
  jp z,.nextframe


  ld    a,(ix+deathtimer)
  cp 10                      ;na 10 frames verwijderen
  call  z,destroyobject 


 ; ret




.animate:
  


  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex18  ;color address ;18
  ld    bc,spriteaddrex18c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex18  ;color address
  ld    bc,spriteaddrex18c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex19  ;color address ;19 
  ld    bc,spriteaddrex19c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex19  ;color address
  ld    bc,spriteaddrex19c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
  
.nextframe:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0

  ld (ix+movepat),6
  
  
  ret
  

  

  
  
MovementPattern06:

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  jp objectdeathjump



MovementPattern07: ;shell knalt weg

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld a,(ix+killshell)
  dec a
  jp z,.ignoreinteract


  call objectinteract  
  cp 2
  jp z,pinkshelldeathjump
  cp 3
  jp z,.jumpup
  cp 5
  jp z,destroyobjectwithcloudnostar

.ignoreinteract:



  call checkmariointeractshoot
  jp c,pinkshelldeathjump


  call mariointeract
  ;jp c,MovementPattern01.nextframe
  jp c,.stopshell


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animateshell


  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call  checkobjecttilesfloor
;  ret c 
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  
  call objectinteract ;check if there is a box wall
  ld a,(iy+movepat)
  cp 47 
  jp z,.wallfound

  
  ret
  
.wallfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
;  ld    a,(ix+v3)               ;v3 is used as animation variable
;  xor   2
;  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    .animate

  call Playsfx.stomp


  jp    animateshell
;  ret




.stopshell:

  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deadly),0
  ld (ix+movepat),21
  ld (ix+clipping),4
    ld (ix+edible),1
  
  call Playsfx.kick
;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;
;  ld    hl,spriteaddrex10  ;color address
;  ld    bc,spriteaddrex10c  ;character address
;  ld    de,offsetshell
;  call    setcharcolandoffset

  call animatestopshell

  call makestarleft
  
  ret


.jumpup:

  ld (ix+v4),0
  ld (ix+movepat),24
  
  ret
  
  


MovementPattern08: ;piranja plant

  call objectinteract
  cp 5
  jp z,destroyobjectwithcloudnostar
  cp 2
  jp z,destroyobjectwithcloudnostar

  call mariointeract
  call c,getspinjump


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


;deze is special omdat de plant heel variabel beweegt daarom lees ik een speedtabelletje uit

;pointer uitlezen
  ld d,0
  ld e,(ix+v4)
  ld hl,piranjaspeedtable
  add hl,de ;pointer geset
  ld a,(hl) ;waarde opgehaald
  ld e,a ;DE bouwen voor de coordinaten
  cp 25
  jp z,.nextframe
  
  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;terugschrijven


  ld a,(ix+v4)
  cp 3
  ret c ;do not animate yet

.animate:
 
  
  ld    (ix+updatespr?),4       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex14  ;color address
  ld    bc,spriteaddrex14c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex15  ;color address
  ld    bc,spriteaddrex15c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex16  ;color address ;02
  ld    bc,spriteaddrex16c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex17  ;color address ;03
  ld    bc,spriteaddrex17c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



  
  ret



.nextframe:

  ld a,(ix+objectrelease)
  and a
  call nz,spawnbullets

  ld a,(ix+movepat)
  inc a
  ld (ix+movepat),a
  
 ; xor a
 ; ld (ix+v4),a

;  ld a,(ix+v4)
;  dec a
;pointer resetten
  xor a
  ld (ix+v4),a

  ret


piranjaspeedtable:  db 6,6,6,6,6,5,5,4,3,4,3,3,2,2,1,2,1,2,1,2,1,1,1,1,0,1,0,1,0,1,0,1,0,0,0,0,25 ;78
piranjaspeedtabledown: db 0,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5



hidepiranjasprite:


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld b,(ix+v7) ;problem
  ld a,18
  sub b
  call c,.zero
;  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld b,(ix+v7)
  ld a,18
  sub b
  call c,.zero  
;  xor a
  call hidemariosprite

  inc (ix+v7)

  ld a,(ix+v7)
  dec a
  cp 18+1
  ret c

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

   
  ld a,(ix+v7) ;19
  sub 18+1
  ld b,a
  ld a,16
  sub b
  call c,.zero
  ;ld a,15
  call hidemariosprite


  ret
  
.zero:
  xor a

  ret


MovementPattern09: ;piranja plant gaat naar beneden

  call objectinteract
  cp 5
  jp z,destroyobjectwithcloudnostar
  cp 2
  jp z,destroyobjectwithcloudnostar


  ld a,(ix+v4)
  cp 65
  call c,.animate
  ld a,(ix+v4)
  cp 64
  call nc,hidepiranjasprite

  call mariointeract
  call c,getspinjump

;deze is special omdat de plant heel variabel beweegt daarom lees ik een speedtabelletje uit


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;pointer uitlezen
  ld d,0
  ld e,(ix+v4)
  ld hl,piranjaspeedtabledown
  add hl,de ;pointer geset
  ld a,(hl) ;waarde opgehaald
  ld e,a ;DE bouwen voor de coordinaten
  cp 5
  jp z,.nextframe
  
  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h ;terugschrijven
  


  ret


.nextframe:

;laatste beweging maken  
;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  add hl,de
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h ;terugschrijven
  
; en klaar is kees
  ld a,(ix+movepat)
  inc a
  ld (ix+movepat),a

  xor a
  ld (ix+deathtimer),a
  
  xor a
  ld (ix+v4),a
  ld (ix+v7),a


  ret


.animate:
 
  
  ld    (ix+updatespr?),4       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex14  ;color address
  ld    bc,spriteaddrex14c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex15  ;color address
  ld    bc,spriteaddrex15c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex16  ;color address ;02
  ld    bc,spriteaddrex16c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex17  ;color address ;03
  ld    bc,spriteaddrex17c  ;character address
  ld    de,offsetpiranjaplant
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



  
  ret




MovementPattern10: ;piranja wachten tot er weer wat mag gebeuren
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite
 
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite  

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite  
  
  
  call getmariodistance
  ld a,c
  cp 32
  ret c

  ld a,(ix+deathtimer)
  
  cp 60
  jp nc,.nextframe
 
  ret


.nextframe:

  ld a,8
  ld (ix+movepat),a
  ret



MovementPattern11: ;shellhold


;what does clipping mean?: 
;0 = the object doesn' t interact 1 = the object acts as a wall to other objects
;2 = the object kills the other objects 3 = the object causes other objects to jump up (boxes etc)
;4 = the object acts as a wall to other objects but kills a shell that is in your hands
;5 = the object kills the other objects and turns them into coins
;6 = This object cannot be destroyed by a level exit
;7 = powerreserve kills an old powerreserve
;10 = special value used by some routines to recognize that the bullet used should now be removed from the game


  call objectinteract
  ld a,(ix+killshell)

  cp 0
  jp z,.nokillshell
  cp 3
  jp z,.nokillshell
  cp 5
  jp z,.nokillshell
  cp 6
  jp z,.nokillshell
  cp 7
  jp z,.nokillshell
  cp 8
  jp z,.nokillshell
  cp 10
  jp z,.nokillshell

  and a
  jp nz,.releaseshellandkill


.nokillshell:


  ld a,(nocontrol)
  cp 4
  call z,.takeshellright
  dec a
  jp z,.takeshell

;schelpje moet tijdelijk van het beeld verdwijnen zodra je omdraait

  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell


;aan de hand van links of rechts de shellspeed van te voren zetten 
  ld a,(watercurrent)
  dec a
  jp z,.checkmarioleftrightsprw


  ld a,(marioleftrightspr)
  dec a
  jp z,.right

.left:

  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ret c ;prevent exiting from the left side of the screen
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  call animateturnshell.position0

;time return to normal if needed
  ld a,(ix+objectrelease)
  cp 6
  call z,timerespawn


  ret

.right:

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframe

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  call animateturnshell.position0

;time return to normal if needed
  ld a,(ix+objectrelease)
  cp 6
  call z,timerespawn

  ret

.checkmarioleftrightsprw:

  ld a,(marioleftrightsprw)
  dec a
  jp z,.right
  
  jp .left



.releaseshellandkill:

;do not kill shell if there is a box in play
      ld a,(iy+movepat)
      cp 47
      jp z,.nokillshell


;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a

  ;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


 
 
  jp pinkshellspecialdeathjump

 


.turnshell:

  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:



  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright
  
  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  jp animateturnshell.position2
  


  ret

.turnshellright:

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,-1
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  jp animateturnshell.position2


;  .position0:
;  ld    hl,spriteaddrex10  ;color address
;  ld    bc,spriteaddrex10c  ;character address
;  ld    de,offsetshell
;  jp    setcharcolandoffset
;
;  .position2:
;  ld    hl,spriteaddrex12  ;color address ;02
;  ld    bc,spriteaddrex12c  ;character address
;  ld    de,offsetshell
;  jp    setcharcolandoffset



;mario takes the shell into a right tube
.takeshellright: 

 ; call findblockwall
 ; ret c

;look at the blockwall placed behind the tube and stop the mariomovement at all times  
  push ix
  ld hl,(mariox)
  ld de,19+16-2
  add hl,de
  ex de,hl
  ld ix,(spriteixvalue)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  pop ix
  xor a
  sbc hl,de
  ret nc
;  jp c,destroyobject


;deactivate this object by hand, firm and direct

  ld    (ix+active?),0          ;our primary sprite is out of active range, set sprite inactive
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
 
  
  ld (ix+movepat),0
  ld (ix+deathtimer),0
  ld (ix+killshell),0

  call removeobjectspat
  call removefromsprlst

;  ;remove sprite from spritelist
;  ld    l,(ix+sprlstadd)
;  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
;  ld    b,(ix+amountspr)        ;amount of sprites
;  .cleanspritesfromspritelist:
;  ld    (hl),0
;  inc   hl
;  djnz  .cleanspritesfromspritelist
;  ;/remove sprite from spritelist


    ld a,(ix+objectrelease)
    ld (marioshelltype),a
  


  ret


;mario takes the shell into a tube
.takeshell:


    ld a,(ix+objectrelease)
    ld (marioshelltype),a

  ld (ix+movepat),61
  ld (ix+v4),0
  ret


  ;spawn a new shell in place that is forced into a new sprite position
 

;  ld l,(ix+xobject)
;  ld h,(ix+xobject+1)
;  srl   h
;  rr    l                       ;/2
;  srl   h
;  rr    l                       ;/4
;  srl   h
;  rr    l                       ;/8
;  dec l
;  ld e,(ix+yobject)
;  ld d,(ix+yobject+1) 
;  srl   d
;  rr    e                       ;/2
;  srl   d
;  rr    e                       ;/4
;  srl   d
;  rr    e                       ;/8
;  inc e
;
;;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;;track of this current object and giant system crashes will become a reality
;  push ix
;
;  ld    iy,CustomObject
;  ld    (iy),force11;12 ;always in the back
;  ld    (iy+1),l                ;set x custom object
;  ld    (iy+2),h
;  ld    (iy+3),e                ;set y custom object
;  ld    (iy+4),51                ;set object number (pinkshell)
;
;  call  activateobject
;  
;  ld (ix+v4),0
;
;  pop ix
;
;
;  jp destroyobject



;ret
  




.nextframe:


;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


  ld a,(marioturnshell)
  and a
  ret nz

  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileleft
  

  jp pattern11rammaproutineleft ;deze aanvraag returned zichzelf spagetthi warning!!


.ignoreleft:


;omhoog trappen
  ld a,(mariolook)
  dec a
  jp z,.nextframe2


  ;kick shell weg
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld a,horshellspeed
  ld (ix+v1),a

  ld (ix+deadly),1
  ld (ix+clipping),2
  
  ld (ix+objectfp),0


  ret




.nextframeright:

;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


;shell mag niet weg terwijl je omdraaid
  ld a,(marioturnshell)
  and a
  ret nz


  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileright


;anders kunnen we dat geintje uithalen in de lucht en tijdens het rennen

  jp pattern11rammaproutineright

  
.ignoreright:  

  ld a,(mariolook)
  dec a
  jp z,.nextframe2right
  
;.skiplookcheckright:



  ;kick shell weg
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,-horshellspeed
  ld (ix+v1),a

  ld (ix+deadly),1
  ld (ix+clipping),2

  ld (ix+objectfp),0


  ret

.nextframe2:

 
 
  ;kick shell weg omhoog
  
  ld a,12 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a

  ld a,(mariospeed)
 ; neg
  ld (ix+v1),a


;  ld a,3
;  ld (ix+deadly),a
 
;bij een mariospeed moet het schelpje een kleine voorsprong krijgen 
  ld a,(mariospeed)
  and a
  ret z
 
  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 
 

 
  ret


.nextframe2right:



  ;kick shell weg
  
  ld a,12 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a
  
  ld a,(mariospeed)
  neg
  ld (ix+v1),a
  

;  ld a,1
;  ld (ix+deadly),a

  ld a,(mariospeed)
  and a
  ret z


  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h



  ret


.nextframespecialleft:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld hl,(mariox)
;  ld de,22
;  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  
  ld a,-horshellspeed
  ld (ix+v1),a

  ld a,1
  ld (ix+deadly),a
  

  
  ret


.nextframespecialright:

  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld hl,(mariox)
  ld de,2
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld a,horshellspeed
  ld (ix+v1),a

  ld a,1
  ld (ix+deadly),a
  


  ret


.kickupsheertileleft: 

  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld a,horshellspeed
  ld (ix+v1),a
  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
 
  
  call locksheertile.left
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret


.kickupsheertileright:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld (ix+deathtimer),0 ;reset any timer related stuff

  ld a,-horshellspeed
  ld (ix+v1),a
;  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  call locksheertile.right
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 


  ret



MovementPattern12: ;shell knalt weg omhoog



  call objectinteract
  and a
  jp nz,.nextframe

.skipnextframe:

  call mariointeract
  jp c,MovementPattern01.nextframe
  

 

  
  ld a,(ix+deathtimer)
  cp 8
 ld b,8 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown


;check upwards twice
  call shelluptilecheck
  jp c,.nextframe3 
 
  
 call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
 call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  call checkobjecttilelr
  jp c,.nextframe2
 

  ret


 
 
;gaan we dropcheck in douwen


.nextframe:

  ld a,(iy+clipping)
  cp 8
  jp z,.nextframe4
  cp 1
  jp z,.skipnextframe
  cp 3
  jp z,.skipnextframe

  ld a,(iy+deadly)
  cp 3
  jp z,.skipnextframe


.nextframe3:

  ld a,13
  ld (ix+movepat),a
  
  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a

  ret


.nextframe4:

  ld a,(ix+deathtimer)
  cp 3
  jp c,.skipnextframe

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld a,13
  ld (ix+movepat),a
  
  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a
  
  ld a,(iy+v7)
  and a
  ret nz
  
  ld (iy+v7),1

  ret

.nextframe2:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:

  call lockobjectx.right

  xor a
  ld (ix+v1),a

  ret


.slowdown:

  
  sub b
  ld d,0
  ld e,a
  ld hl,shellfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe3
  
  
  ld (ix+v2),a

;  call shelluptilecheck
;  jp c,.nextframe
 
  call shelluptilecheck
  jp c,.nextframe3 

  call  subv2toybject
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  

  call checkobjecttilelr
  jp c,.nextframe2




  ret



MovementPattern13: ;shell valt naar beneden



  call checkmariointeractshoot
  jp c,pinkshelldeathjump
 

 call mariointeract
    jp c,.interact
  
  
  ld a,(ix+deathtimer)
  cp 10
; ld b,15 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  call c,.slowdown
  ;reset the clipping after 15 frames. The shell becomes deadly again if it was set to 0
  ;by the blockhandler or any other process
  cp 5
  call z,.setclipping


  call .checkwater

 call  addv1toxbject 
 call  addv2toybject           ;x movement of object, this adds the value in v1 to xobject (16 bits)


;links of rechts een muurtje te pakken 
  call checkobjecttilelr
  jp c,.nextframe3 
 
  jp checktilesmovpattern13
  
.returnfrompage3:   
  
;gaan we dropcheck in douwen

  ret

.nextframe:

  ld (ix+sheertile),0
  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  call lockyobject.forcelock

;check of de shell een vaart heeft zo ja andere routine doen
;  ld a,(ix+v1)
;  and a
;  jp nz,.nextframe2

  ld a,21 ;stuitreren 
  ld (ix+movepat),a
  xor a
;  ld (ix+deadly),a
;  ld (ix+v1),a
  ld (ix+v2),a
  ld (ix+v4),a

  ret


.nextframe2:

  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  ld a,21
  ld (ix+movepat),a
  xor a
  ld (ix+v2),a
  ld (ix+v4),a
 ;  ld (ix+deadly),a
  
  ret

.sheertilefoundright:


  ld (ix+sheertile),2
  jp .sheertile


 
;als we een schuine tile hebben gevonden dan willen we anders locken 
.sheertilefound:

    ld (ix+sheertile),1


.sheertile:

  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  ld a,21
  ld (ix+movepat),a
  xor a
  ld (ix+v2),a
  ld (ix+v4),a

  call locksheertile.left

  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
  call locksheertile.left

  xor a
  ld (ix+v1),a

  ret
 
 
  
  
.nextframe3:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:


  call lockobjectx.right

  xor a
  ld (ix+v1),a
  
  ret  




.slowdown:

;  sub b
  ld d,0
  ld e,a
  ld hl,shellfallacctable
  add hl,de
  ld a,(hl)
  
  
  ld (ix+v2),a
;  call  addv2toybject

.checkwater:
  ;check for a water level
  ld hl,(waterheight)
  xor a
  ld de,0
  sbc hl,de
  ret z ;no waterlevel present
  
  ld de,32;64
  xor a
  sbc hl,de
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  ret nc
  
  ld (ix+v2),1



  ret


.setclipping:

  ld (ix+clipping),2
  ret


.interact:

    
    ld a,(marioyoshi)
    dec a
    ret z
    
    
    jp MovementPattern01.shellhold ;we pakken de shell direct vast maar als we hem niet vast hebben word ie meteen gereleased

;    ret



MovementPattern14:              ;rotating block, question mark block, exclamation mark block
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  

  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    3                       ;4 frames moving upwards nu 3 looks better
  ret   nz
  
  jp destroyobject
  ;ret
  


MovementPattern15:              ;brown block


  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    10                       ;brown block stays 10 frames in play
  ret   nz
  
  ld    (ix+active?),0          ;destroy object
  ld    (ix+yobject),0          ;y object
  ld    (ix+yobject+1),0        ;y object
  
  ret


MovementPattern16: ;red mushroom

  ld a,(ix+v4)
  cp 20
  jp c,.popup

  call objectinteract
  cp 3
  jp z,.jumpupfromblock
  ld a,(iy+movepat)
  cp 173
  call z,.wallfound

;  ld a,2
;  ld b,2
;  push bc
  call mariointeract
;  pop bc
  ld b,2
  jp c,settransform

  ld a,(ix+v4)
  cp 99
  jp nc,.jumpup

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  ret nc

.wallfound:
;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a 

  ret

.jumpupfromblock:
  ld (ix+v4),100

.jumpup:

  
  ld a,(ix+v4)
  sub 100
  ld hl,shroomjumpuptable
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endjump
  
  ld (ix+v1),2
  ld (ix+v2),a
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)
  ld (ix+objectfp),0


  ret


.popup:

  ld a,(ix+v4)
  cp 16
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)  
  ld (ix+objectfp),0

 ret


.nextframe:

  ld (ix+v1),2
  ld (ix+v2),0
  inc (ix+v4)

  ret

.endjump:

  ld (ix+v4),50
  ret


shroomjumpuptable:  db 8,6,4,3,3,2,3,2,3,2,1,0


MovementPattern17: ;fire flower

  ld a,(ix+v4)
  cp 20
  jp c,.popup


  ld a,3
  ld b,a
  push bc
  call mariointeract
  pop bc
  jp c,settransform

  ld (ix+v2),0

  call checkobjecttilesfloor
  
  call addv2toybject


  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

             ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ret

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex24  ;color address
  ld    bc,spriteaddrex24c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex25  ;color address
  ld    bc,spriteaddrex25c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex26  ;color address ;02
  ld    bc,spriteaddrex26c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex27  ;color address ;03
  ld    bc,spriteaddrex27c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret



.popup:

  ld a,(ix+v4)
  cp 16
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)  
  ld (ix+objectfp),0

 ret


.nextframe:

  ld (ix+v1),2
  ld (ix+v2),0
  inc (ix+v4)

  ret


MovementPattern18: ;feather

  ld a,(ix+deathtimer)
  cp 12
  jp c,.popup


  ld a,4
  ld b,a
  push bc
  call mariointeract
  pop bc
  jp c,settransform


;overflow protection
  ld (ix+deathtimer),25


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  call .animate

  ld a,(ix+v3)
  cp 24
  jp z,.resetv3
  ld e,a
  ld d,0
  ld hl,featherspeedlr
  add hl,de

 
  ld a,(ix+v4)
  and a
  jp nz,.invertdir

  ld a,(hl)
  ld (ix+v1),a
  
  ld hl,featherspeedd
  add hl,de
  ld a,(hl)
  ld (ix+v2),a

  ld a,(ix+v3)
  inc a
  ld (ix+v3),a

  ret


.invertdir:

  ld a,(hl)
  neg
  ld (ix+v1),a
  
  ld hl,featherspeedd
  add hl,de
  ld a,(hl)
  ld (ix+v2),a

  ld a,(ix+v3)
  inc a
  ld (ix+v3),a
  
  
  ret

.resetv3:

  ld a,(ix+v4)
  xor %00000001
  ld (ix+v4),a

  xor a
  ld (ix+v3),a

  ret


.animate:
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    a,(ix+v4)               ;v4 is used as animation variable in this case
  and a
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,spriteaddrex22  ;color address ;18 
  ld    bc,spriteaddrex22c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex23  ;color address ;19 
  ld    bc,spriteaddrex23c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.popup:

  ld a,(ix+deathtimer)
  cp 8
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),6
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld (ix+objectfp),0

 ret


.nextframe:

;  ld (ix+v1),6
  ld (ix+v2),2

  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ret




featherspeedlr: db 0,1,1,2,2,2,2,2,2,3,3,4,5,4,3,3,2,2,2,2,2,2,1,1,0 ;snelheid van het veertje wat heen en weer beweegt
featherspeedd:  db 0,1,0,1,0,1,1,1,1,1,1,2,2,1,1,0,0,0,0,0,0,0,0,0,0 ;snelheid waarmee het veertje omlaag beweegt









MovementPattern19: ;star

  ld a,(ix+v4)
  cp 20
  jp c,.popup


  ld a,5
  ld b,a
  push bc
  call mariointeract
  pop bc
  jp c,settransform


  call  .animate




  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  ret nc

;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable





.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex28  ;color address
  ld    bc,spriteaddrex28c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex28  ;color address
  ld    bc,spriteaddrex28c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex29  ;color address ;02
  ld    bc,spriteaddrex29c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex29  ;color address ;03
  ld    bc,spriteaddrex29c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

  ret


.popup:

  ld a,(ix+v4)
  cp 16
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)  
  ld (ix+objectfp),0

 ret


.nextframe:

  ld (ix+v1),2
  ld (ix+v2),0
  inc (ix+v4)

  ret




MovementPattern20: ;green mushroom

  ld a,(ix+v4)
  cp 20
  jp c,.popup
  

  ld a,1 ;one extra life
  ld b,a
  push bc
  call mariointeract
  pop bc
  jp c,setextralife

  ld a,(ix+v7)
  and a
  jp nz,.jumpup

  call objectinteract
  cp 3
  jp z,.jumpupfromblock
  ld a,(iy+movepat)
  cp 173
  call z,.wallfound

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  ret nc

.wallfound:
;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a 

  ret

.popup:

  ld a,(ix+v4)
  cp 16
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)  
  ld (ix+objectfp),0
  ld (ix+v7),0

 ret


.nextframe:

  ld (ix+v1),2
  ld (ix+v2),0
  inc (ix+v4)

  ret

.jumpupfromblock:

  inc (ix+v7)


.jumpup:

  
  ld a,(ix+v7)
  ld hl,shroomjumpuptable
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endjump
  
  ld (ix+v1),2
  ld (ix+v2),a
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v7)
  ld (ix+objectfp),0


  ret

.endjump:

  ld (ix+v7),0
  ret



handleplatformx:


  ;this was saved during the handleobjectmovement  
  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  push hl
  pop iy


  ld d,0
  ld a,(iy+v1)
  bit 7,a
  jp z,.nonegate
  neg 
  ld e,a
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ret

.nonegate:
  ld e,a
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
 ; xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ret

;shell stuitert op de grond na neerkomen met gooien we werken een tabelletje af en wisselen de v1 per zoveel frames
MovementPattern21: ;shell stuitert

;altijd kunnen oppakken no matter what
  call mariointeract
  jp c,.handleinteract


  ld a,(ix+standingonspr) ; Lock the shell to the moving platform
  dec a
  call z,handleplatformx


.notdone:

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a

    ld a,(ix+v1)
    cp 0
    jp nc,.negate
    jp z,.nox
    dec a
    jp z,.nox
    ld (ix+v1),a

  call addv1toxbject
  jp .nox

.negate:

    ld a,(ix+v1)
    jp z,.nox
    inc a
    jp z,.nox
    ld (ix+v1),a
  


.nox:

  ld hl,shellbouncetable
  ld d,0
  ld e,(ix+v4)
  add hl,de


  ld a,(ix+v4)
  cp 13
  jp nc,.done
  cp 1
  jp z,.up
  cp 2
  jp z,.up
  cp 3
  jp z,.up
  cp 4 ;d
  jp z,.down
  cp 5
  jp z,.down
  cp 6
  jp z,.down
  cp 7 ;u
  jp z,.up
  cp 8
  jp z,.up
  cp 9 ;d
  jp z,.down
  cp 10
  jp z,.down
  cp 11 ;u
  jp z,.up
  cp 12 ;d
  jp z,.down



  ret

;shellbouncetable: db 0,3,2,1,1,2,3,2,1,1,2,1,1,0


.up:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call subv2toybject
  
  
  ret

.down:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toybject

  
  ret



.handleinteract:

  ld a,(mariospinjump)
  and a
  jp nz,destroyobjectwithcloud 

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


.done:

  ld (ix+movepat),1
  ld (ix+objectfp),0

;  jp MovementPattern01.nextframe
  
  
  ret





MovementPattern22:
  
.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  
  ld a,(ix+clipping)
  cp 10
  jp nz,destroyobject
  
  ld a,(bulletsingame)
  dec a
  ld (bulletsingame),a 
  
  
  jp destroyobject
  
;  xor a
;  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex30  ;color address
  ld    bc,spriteaddrex30c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex31  ;color address
  ld    bc,spriteaddrex31c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex32  ;color address ;02
  ld    bc,spriteaddrex32c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex33  ;color address ;03
  ld    bc,spriteaddrex33c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position4:
  ld    hl,spriteaddrex34  ;color address ;03
  ld    bc,spriteaddrex34c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.position5:
  ld    hl,spriteaddrex35  ;color address ;03
  ld    bc,spriteaddrex35c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


MovementPattern23:


  xor a
  ld (mariokillshell),a


;TODO: schelpje op zijn kop zetten 
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)



  jp objectdeathjump



  ret

 
MovementPattern24:


  call objectinteract
  cp 2
  jp z,pinkshelldeathjump

  call checkmariointeractshoot
  jp c,pinkshelldeathjump


  call mariointeract
  jp c,MovementPattern01.nextframe


  call .jump


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
 ; call  z,.animate
  call z,animateshell

  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
;  call  checkobjecttilesfloor
;  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret
  
.wallfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
;  ld    a,(ix+v3)               ;v3 is used as animation variable
;  xor   2
;  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    .animate


;  .animate:
; 
;  
;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;
;  ld    a,(ix+v3)               ;v3 is used as animation variable
;  inc a
;  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    z,.position0
;  dec   a
;  jp    z,.position1
;  dec   a
;  jp    z,.position2
;  dec   a
;  jp    z,.position3
;  
;  xor a
;  ld (ix+v3),a
;  
;  .position0:
;  ld    hl,spriteaddrex10  ;color address
;  ld    bc,spriteaddrex10c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock1
;  jp    setcharcolandoffset
;
;  .position1:
;  ld    hl,spriteaddrex11  ;color address
;  ld    bc,spriteaddrex11c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock1
;  jp    setcharcolandoffset
;
;  .position2:
;  ld    hl,spriteaddrex12  ;color address ;02
;  ld    bc,spriteaddrex12c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock1
;  jp    setcharcolandoffset
;
;  .position3:
;  ld    hl,spriteaddrex13  ;color address ;03
;  ld    bc,spriteaddrex13c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock1
;  jp    setcharcolandoffset



.jump:

;  sub b
  ld d,0
  ld e,(ix+v4)
  ld hl,shelljumpuptable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endjump
  
  ld (ix+v2),a


  ld a,(ix+v4)
  inc a
  ld (ix+v4),a



  ret


.endjump:

  ld (ix+objectfp),0
  ld (ix+movepat),7
  ld (ix+v4),0
  ret

shelljumpuptable:   db 8,7,6,5,4,3,2,1,2,1,0
  


  ret


MovementPattern25:    ;sliding guy
;v4 word gebruikt om zijn status bij te houden
  ld a,(currentlevel)
  cp 80
  call z,.checkposition

  call objectinteract ;kans lijkt me klein dat dit nodig is
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,MovementPattern26.nextframe


  call  checksheerslidingguy
  jp c,slidingguyofframp

  call .makespeed

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ret


.checkposition:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,300
  xor a
  sbc hl,de
  ret nc

  ld hl,287
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ret


.makespeed:


;  sub b
  ld d,0
  ld e,(ix+v4)
  ld hl,slidingguytable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.endmakespeed
  
  neg
  ld (ix+v1),a
  neg
  ld (ix+v2),a


  ld a,(ix+v4)
  inc a
  ld (ix+v4),a

.endmakespeed:


  ret

slidingguytable:   db 0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,2,1,2,1,2,2,2,2,2,2,3,3,3,4,3,4,4,3,4,4,5


slidingguyofframp:

  ld (ix+v2),8


    ld a,(ix+v1)
    inc a
    cp -3
    jp z,.nospeed
    ld (ix+v1),a

.nospeed:    

  call  checkobjecttilesfloor
  jp nc,slidingguyfinalslide

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld (ix+v4),0

  ret


slidingguyfinalslide:

;  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success

  ;  sub b
  ld d,0
  ld e,(ix+v4)
  ld hl,slidingguytablefinal
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endslide
  
  neg
  ld (ix+v1),a


  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  
  


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  ret

.endslide:

  ld (ix+movepat),26
;  ld (ix+movepat+1),0
  ld (ix+v4),0 ;v4 resetten gaan we als timertje gebruiken bij het opstaan
  ld (ix+v1),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex37  ;color address
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


MovementPattern26:    ;sliding guy walking around

;hij moet wel dodelijk zijn ook waneer hij wacht
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  ld a,(ix+v4)
  cp 15
  jp nc,.start
  inc a
  ld (ix+v4),a
  ret

.start:

  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex39  ;color address
  ld    bc,spriteaddrex39c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex40  ;color address
  ld    bc,spriteaddrex40c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex37  ;color address ;02
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex38  ;color address ;03
  ld    bc,spriteaddrex38c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.nextframe:
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),27  ;generic set
  
  call Playsfx.kickenemy
 
 ;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a
 
  
;  call MovementPattern27.animate ;animeer
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex41  ;color address ;03
  ld    bc,spriteaddrex41c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret

.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

slidingguytablefinal:   db 4,4,4,4,3,4,3,4,3,4,3,4,3,3,3,3,3,3,3,2,3,2,3,2,3,2,2,2,2,2,1,2,1,2,1,2,1,1,1,1,0


MovementPattern27:    ;any object with one deathsprite dying


  ld a,(ix+deathtimer)
  cp 10
  ret c
  
  
;  call removeobjectspat
  jp destroyobject

;ret

MovementPattern28:    ;checkpointbar

  ld a,(middlereached)
  and a
  jp nz,destroyobject


  call mariointeract
  jp c,.nextframe

  ret

.nextframe:

  ld a,(mariostate)
  and a
  call z,.makemariobig
 
  call Playsfx.checkpoint
  
  ld a,(currentlevel) ;by trowing in the currentlevel the value is way more specific
  ld (middlereached),a
  
  jp destroyobject



;ret

.makemariobig:

  ld a,1
  ld (mariostate),a


  ret


MovementPattern29:    ;sterretje bij een kill

  ld a,(ix+v3)
  inc a
  ld (ix+v3),a
  cp 3
  ret nz

  jp destroyobject


MovementPattern30:    ;kogeltje

;TODO: branch special bonus

    ld a,(mariolaser)
    and a
    jp nz,.setspecial

  ld a,(ix+killshell)
  and a
  jp nz,.wallfound

  ld a,(ix+v4)
  and a
  call z,.setspeed
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld a,(ix+v4)
  and a
  call nz,.bounceup

  call  .animate


  call checkobjecttilescheckwall
  jp c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

;  call  checksheerobjecttiledown
;  ret c
  call checksheerobjecttile ;check for sheer tiles
  jp c,.setbounceup ;sheertile found

  call  checkobjecttilesfloor
  jp nc,.setbounceup
  

  ret

.setspecial:


    ld hl,383
    ld (ix+movepat),l
    ld (ix+movepat+1),h
  
    ret
  
  

.wallfound:


  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+clipping),10
  ld (ix+movepat),22

  ld de,12
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex30  ;color address
  ld    bc,spriteaddrex30c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  dec   a
  jp    z,.position6
  dec   a
  jp    z,.position7


  ld (ix+v3),0

.position0:
  ld    hl,spriteaddrex42  ;color address
  ld    bc,spriteaddrex42c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex46  ;color address
  ld    bc,spriteaddrex46c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex43  ;color address ;02
  ld    bc,spriteaddrex43c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex47  ;color address ;03
  ld    bc,spriteaddrex47c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position4:
  ld    hl,spriteaddrex44  ;color address
  ld    bc,spriteaddrex44c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position5:
  ld    hl,spriteaddrex48  ;color address
  ld    bc,spriteaddrex48c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position6:
  ld    hl,spriteaddrex45  ;color address ;02
  ld    bc,spriteaddrex45c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position7:
  ld    hl,spriteaddrex49  ;color address ;03
  ld    bc,spriteaddrex49c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;.nextframe:  
;  
;  jp z,destroyobjectwithcloud

.setbounceup:


  ld (ix+v4),1 ;bounceup enable

  ret

.bounceup:

  ld (ix+v2),3

  call subv2toybject
  ret


.setspeed:


  ld (ix+v2),7
  ret



MovementPattern31:     ;destroyblockdebris

    ld a,(currentlevel)
    cp 173
    call z,mariointeract

  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  call objectdeathjump

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex50  ;color address
  ld    bc,spriteaddrex50c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex51  ;color address
  ld    bc,spriteaddrex51c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex52  ;color address ;02
  ld    bc,spriteaddrex52c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex53  ;color address ;03
  ld    bc,spriteaddrex53c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position4:
  ld    hl,spriteaddrex54  ;color address ;03
  ld    bc,spriteaddrex54c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.position5:
  ld    hl,spriteaddrex55  ;color address ;03
  ld    bc,spriteaddrex55c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset





MovementPattern32: ;moonpowerup


  ld a,(moonpoweruptaken)
  dec a
  jp z,destroyobject


  call mariointeract
  jp c,.nextframe
  
  
  
  ret



.nextframe:


  ld a,1
  ld (moonpoweruptaken),a
  
  ld a,(lives)
  add a,3
  ld (lives),a
  
  
  call Playsfx.checkpoint
  
  jp destroyobject


;ret


;charging chuck heeft een complexe AI. Hij kijkt om zich heen en charged in jou richting
;dat doet hij totdat je richting is gewijzigd bij een ramp gaat hij springen
MovementPattern33: ;chargin chuck (zit nog stil)


  call objectinteract
;  cp 2
;  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump    
  cp 5
  jp z,.takefire


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

;  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  call  checkobjecttilesfloor
  jp c,.falling


  ld a,(ix+deathtimer)
  cp 15
  jp nc,.setcharge
 

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

  ret

.animate:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;v3 0 = nog niks 1 = links 2 = rechts 4 = bezig met omdraaien



  call getmarioleftright
  call    nc,.setposition1
  
  call getmarioleftright
  call    c,.setposition2

  ld a,(ix+v3)
  and a
  cp 1
  jp z,.position1
  cp 2
  jp z,.position2
  cp 4
  jp z,.position0


.setposition1:

  ld a,(ix+v3)
  cp 1
  ret z

  ld (ix+deathtimer),0
  ld (ix+v3),4

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  cp 2
  ret c
  
  ld (ix+v3),1
  ld (ix+v4),0


  ret

.setposition2:

  ld a,(ix+v3)
  cp 2
  ret z

  ld (ix+deathtimer),0
  ld (ix+v3),4

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  cp 2
  ret c
  
  ld (ix+v3),2
  ld (ix+v4),0




  ret

.position0:
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.position00
  
  ld    hl,spriteaddrex56  ;color address
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.position10  
  
  ld    hl,spriteaddrex59  ;color address
  ld    bc,spriteaddrex59c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.position20  
  
  ld    hl,spriteaddrex60  ;color address ;02
  ld    bc,spriteaddrex60c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position00:

  ld    hl,spriteaddrex580  ;color address
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.position10:

  ld    hl,spriteaddrex581  ;color address
  ld    bc,spriteaddrex581c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  
.position20:

  ld    hl,spriteaddrex580  ;color address
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  
  

.falling:

  ld (ix+deathtimer),0
  ret

.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret

.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a


  ld (ix+v4),0

;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckstomp


  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.setcharge:
  
  ld a,60
  ld (chuckruncount),a

  
  ld a,(ix+v3)
  cp 1
  jp z,.setchargeleft
  cp 2
  jp z,.setchargeright 
  ret

.setchargeleft:
  
  ld (ix+v1),-3
    ld (ix+clipping),2
  ld (ix+movepat),34
  ld (ix+v4),0

  ret

.setchargeright:

  ld (ix+v1),3
    ld (ix+clipping),2
  ld (ix+movepat),34 
  ld (ix+v4),0


  ret



MovementPattern34: ;chargin chuck (is charging now)


  call objectinteract
;  cp 2
;  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump    
  cp 5
  jp z,.takefire


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  call .checkleftright

  ld a,(ix+v4)
  cp 15
  jp nc,.stopcharge

  call Playsfx.chuckrun

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)
 

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

  call checkdestructionblock
;  ret c ;upon destruction ignore the walls to prevent jumping in the proces

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  jp c,.falling
  call checkobjecttilescheckwall
  jp    c,.wallfound            
  ret


.animate:
 
  ld a,(ix+objectrelease)
  and a
  jp nz,.animate2
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex63  ;color address
  ld    bc,spriteaddrex63c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex64  ;color address
  ld    bc,spriteaddrex64c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex65  ;color address ;02
  ld    bc,spriteaddrex65c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex66  ;color address ;03
  ld    bc,spriteaddrex66c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.animate2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position10
  dec   a
  jp    z,.position20
  dec   a
  jp    z,.position30

.position00:
  ld    hl,spriteaddrex584  ;color address
  ld    bc,spriteaddrex584c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position10:
  ld    hl,spriteaddrex585  ;color address
  ld    bc,spriteaddrex585c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position20:
  ld    hl,spriteaddrex586  ;color address ;02
  ld    bc,spriteaddrex586c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position30:
  ld    hl,spriteaddrex587  ;color address ;03
  ld    bc,spriteaddrex587c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset
  


.falling:


  ld (ix+v4),0

  ret


.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret
 
.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
    ld (ix+clipping),1
  ld (ix+movepat),35

  ld    (ix+updatespr?),2 

  ld a,(ix+objectrelease)
  and a
  jp nz,.minibranch

  ld    hl,spriteaddrex67  ;color address ;03
  ld    bc,spriteaddrex67c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckauw

  ret


.minibranch:



  ld    hl,spriteaddrex580  ;color address ;03
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckauw

  ret



.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.wallfound:

;we kijken naar links en naar rechts als mario nog dezelfde kant op staat gaan we springen

  call getmarioleftright
  jp c,.rightjump
  
  ld a,(ix+v1)
  rlca ;is a negatief?
  jp c,.jump
 
  jp .stopcharge
 
  
;  ret
  
.rightjump:


  ld a,(ix+v1)
  rlca ;is a negatief?
  jp nc,.jump

  jp .stopcharge

;  ret

.jump:

;hij moet over muren heen springen!!
;maar alleen als hij jou kant op kijkt.
  ld (ix+v4),0
;  ld (ix+v1),0
  ld (ix+v2),6
  ld (ix+deathtimer),0
    ld (ix+clipping),2
  ld (ix+movepat),36
  ret

.stopcharge:

  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
    ld (ix+clipping),1
  ld (ix+movepat),33
  ret


.checkleftright:


  call getmarioleftright
  jp c,.right
  
  ld a,(ix+v1)
  rlca ;is a negatief?
  ret c
  
  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  
  
.right:


  ld a,(ix+v1)
  rlca ;is a negatief?
  ret nc
  
  ld a,(ix+v4)
  inc a
  ld (ix+v4),a


  ret  

MovementPattern35: ;chargin chuck (is stunned by hit)


  call objectinteract
;  cp 2
;  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump  
  cp 5
  jp z,.takefire


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;stunned blijven zolang hij valt
  call  checkobjecttilesfloor
  ret c

  ld (ix+v1),0
  ld (ix+v2),0

  ld a,(ix+deathtimer)
  cp 10
  jp z,.position3 ;head comes up after a while
  cp 25
  jp nc,.shakehead

  ret

.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret


.shakehead:

  
  ld    (ix+updatespr?),2 

  ld a,(ix+v3)
  inc a
  ld (ix+v3),a
  cp 15
  jp z,.resetmovementpattern
  and 2
  jp z,.position2



.position1:
   
  ld a,(ix+objectrelease)
  and a
  jp nz,.position11 
   
   
  ld    hl,spriteaddrex59  ;color address
  ld    bc,spriteaddrex59c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.position21   
  
  ld    hl,spriteaddrex60  ;color address ;02
  ld    bc,spriteaddrex60c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  

.position3: ;head comes back up
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.position31   
  
  ld    (ix+updatespr?),2 
  ld    hl,spriteaddrex56  ;color address ;02
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 


.position11:

  ld    hl,spriteaddrex581  ;color address
  ld    bc,spriteaddrex581c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.position21:

  ld    hl,spriteaddrex580  ;color address
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position31:

  ld    hl,spriteaddrex580  ;color address
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  
  


.resetmovementpattern:

  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+movepat),33
  ret

.nextframe:
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld a,(ix+hitpoints) ;when stunned chuck cannot be hit again
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
    ld (ix+clipping),1
;  ld (ix+movepat),33

  ld    (ix+updatespr?),2 

  ld a,(ix+objectrelease)
  and a
  jp nz,.minibranch

  ld    hl,spriteaddrex56  ;color address ;03
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 call makestarright
  
   call Playsfx.chuckstomp 
  
  
  ret
  
.minibranch:

  ld    hl,spriteaddrex580  ;color address ;03
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 call makestarright
  
   call Playsfx.chuckstomp 
  
  
  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



MovementPattern36: ;chargin chuck (jumping)


  call objectinteract
;  cp 2
;  jp z,otherobjectdeathjump
  cp 5
  jp z,.takefire


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  ld a,(ix+deathtimer)
  cp 8
 ld b,8 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown

  
;  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)
 

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  .animate



;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c

  call checkobjecttilescheckwallforchuck
  ret c
  
  jp .nowallfound           
  ret


.animate:
 
  ld a,(ix+objectrelease)
  and a
  jp nz,.animatesmall
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex61  ;color address 
  ld    bc,spriteaddrex61c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex61  ;color address
  ld    bc,spriteaddrex61c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex62  ;color address ;02
  ld    bc,spriteaddrex62c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex62  ;color address ;03
  ld    bc,spriteaddrex62c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.animatesmall:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position10
  dec   a
  jp    z,.position20
  dec   a
  jp    z,.position30

.position00:
  ld    hl,spriteaddrex582  ;color address 
  ld    bc,spriteaddrex582c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position10:
  ld    hl,spriteaddrex582  ;color address
  ld    bc,spriteaddrex582c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position20:
  ld    hl,spriteaddrex583  ;color address ;02
  ld    bc,spriteaddrex583c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset

.position30:
  ld    hl,spriteaddrex583  ;color address ;03
  ld    bc,spriteaddrex583c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret

.nowallfound:

  call getmarioleftright
  
  jp c,.setchargeright

  
.setchargeleft:
  
  ld (ix+v1),-3
  ld (ix+v2),0
    ld (ix+clipping),2
  ld (ix+movepat),34
  ld (ix+v4),0

  ret

.setchargeright:

  ld (ix+v1),3
    ld (ix+v2),0
    ld (ix+clipping),2
  ld (ix+movepat),34 
  ld (ix+v4),0


  ret


.donejumping:

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
    ld (ix+clipping),1
  ld (ix+movepat),33 

  ret
 
 
 
.nextframe:
 
 
  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
    ld (ix+clipping),1
  ld (ix+movepat),35

  ld    (ix+updatespr?),2 


  ld a,(ix+objectrelease)
  and a
  jp nz,.dosmall

  ld    hl,spriteaddrex56  ;color address ;03
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 jp makestarright

;ret



.dosmall:

  ld    hl,spriteaddrex580  ;color address ;03
  ld    bc,spriteaddrex580c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

;makestar verneukt de ix waarde be carefull
 jp makestarright

;ret



.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.slowdown:
  
  sub b
  ld d,0
  ld e,a
  ld hl,chuckjumpslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.donejumping
  
  
  ld (ix+v2),a
  call  subv2toybject


  call checkobjecttilescheckwallforchuck
  ret c
  
  jp .nowallfound  

  ret


chuckjumpslowdowntable:   db 6,4,2,1,0



MovementPattern37: ;snapping chuck (sitting still)


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,.takefire


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

;  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  call  checkobjecttilesfloor
  jp c,.falling


  ld a,(ix+deathtimer)
  cp 15
  jp nc,.setjump
 

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate

  ret

.animate:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;v3 0 = nog niks 1 = links 2 = rechts 4 = bezig met omdraaien



  call getmarioleftright
  call    nc,.setposition1
  
  call getmarioleftright
  call    c,.setposition2

  ld a,(ix+v3)
  and a
  cp 1
  jp z,.position1
  cp 2
  jp z,.position2
  cp 4
  jp z,.position0


.setposition1:

  ld a,(ix+v3)
  cp 1
  ret z

  ld (ix+deathtimer),0
  ld (ix+v3),4

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  cp 2
  ret c
  
  ld (ix+v3),1
  ld (ix+v4),0


  ret

.setposition2:

  ld a,(ix+v3)
  cp 2
  ret z

  ld (ix+deathtimer),0
  ld (ix+v3),4

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  cp 2
  ret c
  
  ld (ix+v3),2
  ld (ix+v4),0




  ret

.position0:
  ld    hl,spriteaddrex56  ;color address
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex59  ;color address
  ld    bc,spriteaddrex59c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex60  ;color address ;02
  ld    bc,spriteaddrex60c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.falling:

  ld (ix+deathtimer),0
  ret

.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret

.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+hitpoints),3
  ld (ix+movepat),35

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex67  ;color address ;02
  ld    bc,spriteaddrex67c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call  setcharcolandoffset


;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckauw


  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.setjump:

  call getmarioheight
  cp 48
  ret c
  
  call Playsfx.springjump


  ld a,(ix+v3)
  cp 1
  jp z,.setjumpleft
  cp 2
  jp z,.setjumpright 
  ret

.setjumpleft:
  
  ld (ix+v1),-2
  ld (ix+v2),6
  ld (ix+deathtimer),0
  ld (ix+movepat),38
  ld (ix+v4),0

  ret

.setjumpright:

  ld (ix+v1),2
  ld (ix+v2),6
    ld (ix+deathtimer),0
  ld (ix+movepat),38 
  ld (ix+v4),0


  ret


MovementPattern38: ;snapping chuck (jumping)


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,.takefire


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  ld a,(ix+deathtimer)
  cp 12
 ld b,12 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown

  
;  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)
 

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  .animate
          
  ret


.animate:
 
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex61  ;color address 
  ld    bc,spriteaddrex61c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex61  ;color address
  ld    bc,spriteaddrex61c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex62  ;color address ;02
  ld    bc,spriteaddrex62c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex62  ;color address ;03
  ld    bc,spriteaddrex62c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret
  


.donejumping:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  call    z,.position00
  dec   a
  call    z,.position01
  dec   a
  call    z,.position02
  dec   a
  call    z,.position03

  jp .endjump
 
.position00:
  ld    hl,spriteaddrex57  ;color address 
  ld    bc,spriteaddrex57c  ;character address
  ld    de,offsetchuckclap
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position01:
  ld    hl,spriteaddrex57  ;color address
  ld    bc,spriteaddrex57c  ;character address
  ld    de,offsetchuckclap
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position02:
  ld    hl,spriteaddrex58  ;color address ;02
  ld    bc,spriteaddrex58c  ;character address
  ld    de,offsetchuckclap
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position03:
  ld    hl,spriteaddrex58  ;color address ;03
  ld    bc,spriteaddrex58c  ;character address
  ld    de,offsetchuckclap
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.endjump:

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
  ld (ix+movepat),37 

  call Playsfx.chuckclap

  ret
 
 
 
.nextframe:
 
 
  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+deathtimer),0
  ld (ix+hitpoints),3
  ld (ix+movepat),35

  ld    (ix+updatespr?),2 

  ld    hl,spriteaddrex56  ;color address ;03
  ld    bc,spriteaddrex56c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call    setcharcolandoffset


;makestar verneukt de ix waarde be carefull
 call makestarright


  call Playsfx.chuckauw

  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.slowdown:
  
  sub b
  ld d,0
  ld e,a
  ld hl,chuckjumpslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.donejumping
  
  
  ld (ix+v2),a
  call  subv2toybject 

  ret



MovementPattern39: ;questionmark sprite

  ld a,(currentlevel)
  cp 40
  jp z,.setspecial

;see what has to come out
  call objectinteract
  cp 3
  jp z,.releaseobject


;Failsafe. Check the tile around us and see if the box popped by a nearby object. If yes pop anyway
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 204
  jp z,.releaseobject

  ld a,(currentlevel)
  cp 174 ;special1 does not need animation
  ret z
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3

  ret

.animate:
 
  
.position0:
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex68c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex69  ;color address
  ld    bc,spriteaddrex69c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex70  ;color address ;02
  ld    bc,spriteaddrex70c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex71  ;color address ;03
  ld    bc,spriteaddrex71c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releaseobject:

  ld a,(ix+objectrelease)
  cp 6
  jp z,.releasecoin

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),40
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releasecoin:


  call Playsfx.coin

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),70
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 
 

.setspecial:

  ld (ix+movepat),236
  ret





MovementPattern40:     ;questionmark spawn object

  ld a,(ix+v4)
  and a
  jp nz,.waitforpowerup



;  call subv2toybject
  
  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    2                       ;4 frames moving upwards nu 3 looks better
  ret   nz

  ld (ix+v4),1

  call setspritetobrownblock

.spawnpowerup:

  ld a,(ix+objectrelease)
  cp 1
  call z,.makegreenshroom
  ld a,(ix+objectrelease)
  cp 2
  call z,.makeflower
  ld a,(ix+objectrelease)
  cp 3
  call z,.makefeather
  ld a,(ix+objectrelease)
  cp 4
  call z,.makestar
  ld a,(ix+objectrelease)
  cp 5
  call z,.makeextralife
  ld a,(ix+objectrelease)
  cp 7
  call z,.makegreenyoshiegg
  ld a,(ix+objectrelease)
  cp 8
  call z,.makeclimbingplant
  cp 9
  call z,.makepbox
  cp 10
  call z,.makecointail
  cp 11
  call z,.makepboxgrey
  cp 12
  call z,.makeyoshiwings


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),c                ;set object number (14 is red mushroom)
  ld    (iy+6),b                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v7),1

  pop ix

  ret

.setv2high:

  ld (ix+v2),2
  ret


.makeclimbingplant:


;tell the engine that there is a plant active now
  ld a,1
  ld (vineactive),a
  ld bc,75
  ret


.makeyoshiwings:

  ld bc,286
  ret
  

.makegreenyoshiegg:

  ld bc,59
  ret


.makeextralife:

  ld bc,15
  ret


.makeshroom:

  ld bc,14
  ret


.makepbox:

  ld bc,76
  ret

.makepboxgrey:

  ld bc,77
  ret

.makegreenshroom:

  ld a,(ix+v6)
  and a
  jp nz,.makeshroom

  ld a,(mariostate)
  and a
  jp z,.makeshroom
  
  ld bc,15
  ret


.makeflower:

;always make mushroom first
  ld a,(mariostate)
  and a
  jp z,.makeshroom

  ld bc,16
  ret


.makefeather:

;always make mushroom first
  ld a,(mariostate)
  and a
  jp z,.makeshroom
  
  ld bc,17
  ret


.makestar:

  ld bc,18
  ret

.makecointail:

  ld bc,184
  ret



;wait until the powerup has fully risen from the block then destroy this object
.waitforpowerup:

  inc (ix+v4)
  ld a,(ix+v4)
  cp 50
  jp nc,destroyobject
  ret

;todo: in case of a multicoin situation we need to write back the previous tile. We only change the movepat address and handle the rest from there.




MovementPattern41:     ;checkpointpole


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3
  
 ret

.animate:
 
 
  
.position0:
  ld    hl,spriteaddrex73  ;color address
  ld    bc,spriteaddrex73c  ;character address
  ld    de,offsetcheckpole
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex74  ;color address
  ld    bc,spriteaddrex74c  ;character address
  ld    de,offsetcheckpole
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex75  ;color address ;02
  ld    bc,spriteaddrex75c  ;character address
  ld    de,offsetcheckpole
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex76  ;color address ;03
  ld    bc,spriteaddrex76c  ;character address
  ld    de,offsetcheckpole
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 



  ret






MovementPattern42:     ;exclamationmark

  ld a,(ix+v5)
  and a
  jp z,.init


  call objectinteract
  cp 3
  jp z,.releaseobject

  call getblockcoordinates
  inc hl
  call readanytile
  cp 196 ;this is a main block
  ret z
  
  jp .releaseobject ;the brown block popped without telling the object

;  ret


.releaseobject:

;these blocks do nothing. I could make them even less changeable?
  ld a,(ix+objectrelease)
  cp 2
  jp z,destroyobject
  cp 4
  jp z,destroyobject

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),40
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.init:

  inc (ix+v5)

  ld a,(yellowswitchactive)
  bit 0,a
  call z,.checkyellow
  ld a,(yellowswitchactive)  
  bit 1,a
  call z,.checkgreen
  ld a,(yellowswitchactive)  
  bit 2,a
  call z,.checkred

  ld a,(ix+objectrelease)
  and a
  ret z ;object has been destroyed
  
    ld (ix+v6),1 ;set this to tell that extra lives are not allowed to spawn
  
;then when all the checks are completed check if the tiles in the background match
;if not write the secondary object in place. Do this for each object since there are 4 possible
;colors on each map
  call getblockcoordinates
  inc hl
  call readanytile

  cp 196 ;this is a main block
  jp z,.setfailsafe
  
  ld (ix+movepat),235
  ld (ix+v5),0
;adress of tile is now in a each tilenumber has its own unique tilenanimation per map
  cp 5
  jp z,.write1
  cp 9
  jp z,.write2
  cp 13
  jp z,.write3

  ret

.setfailsafe:

  ld (ix+hitpoints),1 ;this makes sure that conditions are checked properly in case all else fails
  ret


.checkyellow:

  ld a,(ix+objectrelease)
  dec a
  jp z,destroyobject

  ret

.checkgreen:

  ld a,(ix+objectrelease)
  cp 3
  jp z,destroyobject

  ret

.checkred:

  ld a,(ix+objectrelease)
  cp 2
  jp z,destroyobject

  ret
  
.write1:

  ld a,animationtilesblock
  call block12

  ld hl,box04a+(8*0)
  ld de,box04aclr+(8*0)
  
  ld a,(currentlevel)
  cp 19
  call z,.box02a
  
  ld a,05
  call updatetilegfx

  ret

.box02a:

  ld hl,box02a+(8*0)
  ld de,box02aclr+(8*0)

  ret

.write2:
.write3:

  ret



MovementPattern43:     ;mario exit tube coverup

 ; xor a
 ; ld (standingonexit),a

;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  call mariointeract
  jp c,.exitlevel

  ret

;this routine is allowed to be slow. All other things are stopped if the conditions are true
.exitlevel:

  xor a
  ld (standingonexit),a

  ld a,(marioduck)
  and a
  ret z

;mario moet ook stilstaan
  ld a,(mariospeed)
  and a
  ret nz    

;  ld a,(marioblueblock)
;  dec a
;  ret z


;all conditions set disable all the mariocontrols by disabling the physics engine
  ld a,1
  ld (nocontrol),a
  ;tell sprite engine that we are standing on a exit
  ld (standingonexit),a
  
  ld (ix+movepat),44
;  ld b,6
;  call forcetubesprite
   
  ;center mario to the exact position as the tube coverup
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,11
  xor a
  sbc hl,de
  ld (mariox),hl



  call Playsfx.pipe

  ld hl,(mariox)
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  srl h
  rr l ;/16 ;make it small so it fits in one byte easier for us
  ld a,l
  ld (exitx),a
  srl h
  rr l ;/32 ;make it small so it fits in one byte easier for us
  ld a,l
  ld (exitx4),a  


  ret

MovementPattern44: ;mario exit tube coverup pulling mario in

  ld a,(ix+v4)
  cp 31
  jp nc,.endlevel

  ld hl,(marioy)
  inc hl
  ld (marioy),hl
  
  inc (ix+v4)

  ld a,(marioyoshi)
  dec a
  jp z,.hidemarioyoshi

;hide the mario, yoshi and shell sprites

  ld a,(ix+v4)
  cp 17
  jp c,.part1


  ld a,(marioshell)
  and a
  call nz,.hidemarioshellfull


  ld a,(ix+v4)
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  ld c,a
  ld a,32  
  sub c
  call hidemariosprite
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	xor a
	jp hidemariosprite


;ret


.part1:

  ld a,(marioshell)
  and a
  call nz,.hidemarioshell


  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld a,(ix+v4)
  ld c,a 
  ld a,16  
  sub c
  jp hidemariosprite

  
;ret

.hidemarioshell:

;  ld de,marioshell1
;	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
;	add hl,de

  ld hl,(holdingix)
  ld bc,0
  call getobjectspriteadress

  ld a,(ix+v4)
  ld c,a
  ld a,16
  sub c
  call hidemariosprite 


  ret

.hidemarioshellfull:

  ld hl,(holdingix)
  ld bc,0
  call getobjectspriteadress

  xor a  
  call hidemariosprite 


  ret




.hidemarioyoshi:

;yoshi
  ld a,(ix+v4)
  cp 17
  jp c,.part1yoshi


;part2yoshi
  
  ld hl,(yoshiix) ;the body is now completely submerged
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite

  ld hl,(yoshiheadix)
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  ld c,a
  ld a,32
  sub c
  call hidemariosprite 

  
  
.marioonyoshi:  
  ld a,(ix+v4)

  cp 8+16
  jp nc,.part2marioyoshi
  cp 8
  jp nc,.part1marioyoshi


  ret

.part1yoshi:

  ld hl,(yoshiix)
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  ld c,a
  ld a,16
  sub c
  call hidemariosprite 

  jp .marioonyoshi

 ; ret


.part2marioyoshi:

  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  ld c,a
  ld a,32+8-1  
  sub c
  call hidemariosprite
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	xor a
	jp hidemariosprite

  

  
.part1marioyoshi:

  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  ld c,a 
  ld a,16+8-1  
  sub c
  jp hidemariosprite



.endlevel:

;wait just a little bit longer to make it look nicer
  inc (ix+v4)


  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite 


  ld a,(ix+v4)
  cp 40
  ret nz

  ld a,1 ;exit number
  ld (levelend?),a

;reset this global byte
  xor a
  ld (standingonexit),a

  jp destroyobject

;  ret




MovementPattern45:     ;mariostar support

  ld a,(ix+v4)
  cp 3
  jp z,.endstar 


  ld a,(ix+deathtimer)
  cp 150
  call nc,.incv4

  ld (ix+sizeY),32

;always keep the object at mario
  ld hl,(mariox)
  ld de,8
  add hl,de
  ld de,(marioy)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

;make the effective range higher when mario is in a larger state
  
  ld a,(mariostate)
  and a
  call z,.makesmaller
  
  ld (ix+yobject),e
  ld (ix+yobject+1),d


  ret

.makesmaller:

  ex de,hl
  ld de,16
  add hl,de
  ex de,hl
  
  ld (ix+sizeY),16
  
  ret

.endstar:

  ld a,2 ;reset the music
  ld (switchmusicforstar),a

  xor a
  ld (mariostar),a
  jp destroyobject

.incv4:

  ld (ix+deathtimer),0
  inc (ix+v4)
  ret


MovementPattern46:     ;mariodrop support

  ld a,(ix+v4)
  and a
  jp nz,destroyobject
  inc (ix+v4)



  ret



MovementPattern47:     ;flying box

;;always reset the standing bit
;  xor a
;  ld (standingonsprite),a

;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobject
;  call z,handleboxinteract.popbox



  call mariointeract
  call c,handleboxinteract


  call .getv2


  call addv1toxbject
  call .doy

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  and 4
  jp z,.position1
  jp .position0
  
 ret

.animate:
 
 
  
.position0:
  ld    hl,spriteaddrex79  ;color address
  ld    bc,spriteaddrex79c  ;character address
  ld    de,offsetflyingbox1
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex80  ;color address
  ld    bc,spriteaddrex80c  ;character address
  ld    de,offsetflyingbox2
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret

.doy:

  ld a,(ix+v2)
  bit 7,a
  jp nz,.goup

.godown:

  jp addv2toybject

.goup:

  ld a,(ix+v2)
  neg
  ld (ix+v2),a

  jp subv2toybject

.resetv4:

  ld (ix+v4),0
  ld (ix+v2),0
  ret

.getv2:

  ld hl,flyingboxspeedtable
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resetv4
  ld (ix+v2),a
  inc (ix+v4)
  ret



flyingboxspeedtable:  db 1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,10



MovementPattern48:     ;flying box release object

 call createbrownblock


  ld a,(ix+killshell)
  and a
  jp nz,.waitforpowerup



;  call subv2toybject
  
  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    2                       ;4 frames moving upwards nu 3 looks better
  ret   nz

  ld (ix+killshell),1


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;make the brown block
  ld    hl,spriteaddrex72  ;color address
  ld    bc,spriteaddrex72c  ;character address
  ld    de,offsetqblock
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

;  call setspritetobrownblock
;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  ld de,8
;  add hl,de
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h

  ret

.spawnpowerup:

  ld a,(ix+objectrelease)
  cp 1
  call z,.makegreenshroom
  ld a,(ix+objectrelease)
  cp 2
  call z,.makeflower
  ld a,(ix+objectrelease)
  cp 3
  call z,.makefeather
  ld a,(ix+objectrelease)
  cp 4
  call z,.makestar
  ld a,(ix+objectrelease)
  cp 5
  call z,.make1up  
  ld a,(ix+objectrelease)
  cp 6
  jp z,.makecoin



  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),b                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0

  pop ix

  ret

;.setv2high:
;
;  ld (ix+v2),2
;  ret


.makecoin:



  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),71

  pop ix

  ret


.makeshroom:

  ld b,14
  ret



.makegreenshroom:

  ld a,(mariostate)
  and a
  jp z,.makeshroom
  
.make1up:  
  ld b,15
  ret


.makeflower:

;always make mushroom first
  ld a,(mariostate)
  and a
  jp z,.makeshroom

  ld b,16
  ret


.makefeather:

;always make mushroom first
  ld a,(mariostate)
  and a
  jp z,.makeshroom
  
  ld b,17
  ret


.makestar:

  ld b,18
  ret



;wait until the powerup has fully risen from the block then destroy this object
.waitforpowerup:

  inc (ix+v4)
  ld a,(ix+v4)
  cp 5
  jp z,.spawnpowerup
  cp 50
  jp nc,destroyobject
  ret



MovementPattern49:     ;smileybox


  call objectinteract
  cp 3
  jp z,.releaseobject


  ret

.setmulticoin:

  ld hl,281
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  ret

.releaseobject:

  call Playsfx.powerup

  ld b,2
  call increasespritecount

;  ld (ix+amountspr),2 ;increase sprite count


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),40
  ld a,(ix+objectrelease)
  cp 6
  call z,.setmulticoin
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset



;get the mapadress of our block

  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    

  ld d,0
  ld e,l

;compenseer op de offsets    
   inc e
;   inc e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de ;yposition found of block


  
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)

  srl d ;nu moet je hl delen door 8
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    

;  dec e

  add hl,de
  ;now we have the tilex and tiley from where the block is currently at

  ex de,hl






;locate and disable the active block that calls this routine

  xor   a : ld    hl,blocklist+(0*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(1*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(2*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  xor   a : ld    hl,blocklist+(3*blocklenght) : cp    (hl) : call  nz,.activeblockfound
  ret

.activeblockfound:

  inc   hl                      ;type
  ld    a,(hl)
  or    a                       ;question mark
  ret z
  dec   a
  ret z

;fetch the adress on map and see if we have the right block
  inc hl
  inc hl            ;bkgadress
  ld b,(hl)
  inc hl
  ld c,(hl)
  push hl
  ld h,b
  ld l,c  ;-----> transfer hl,bc
  xor a
  sbc hl,de
  pop hl
  ret nz

;set the pointer back to type and change the block into a questionmarkblock if all conditions are correct.
  dec hl
  dec hl   
  dec hl    ;type

;  xor a
  ld (hl),a
  ret




MovementPattern50:     ;powerpopup

  call mariointeract
  call c,Playsfx.powerup
  jp c,.spawnpowerup

  ret


.spawnpowerup:


  ld a,(ix+objectrelease)
  cp 1
  call z,.makeshroom
  cp 2
  call z,.makeflower
  cp 3
  call z,.makefeather
  cp 4
  call z,.makestar
  cp 5
  call z,.makegreenshroom


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl

  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
  dec de

;if mario is big we let the powerpopup spawn a little bit higher to make it look nicer
  ld a,(mariostate)
  and a
  jp z,.skipdece

  dec de
  dec de

.skipdece:

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),b                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v4),100
  
  pop ix

  jp destroyobject
  

.makeshroom:

  ld b,14
  ret



.makegreenshroom:


  
  ld b,15
  ret


.makeflower:

  ld b,16
  ret


.makefeather:

  
  ld b,17
  ret


.makestar:

  ld b,18
  ret



MovementPattern51:     ;yellowdestructionstar

;  ret


  ld a,(ix+v4)
  cp 4
  jp nc,.wait


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)

  ret

.wait:

  ld a,(ix+v4)
  cp 6
  jp z,destroyobject

  inc (ix+v4)

  ret



MovementPattern52:     ;yellowdestructionstar high

;  ret

  ld a,(ix+v4)
  cp 4
  jp nc,.wait


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)


  inc (ix+v4)

  ret


.wait:

  ld a,(ix+v4)
  cp 6
  jp z,destroyobject

  inc (ix+v4)

  ret




MovementPattern53:     ;powerreserve ;Depracated routine only used for reference can later be used for something else

;  call objectinteract
;  cp 7
;  jp z,destroyobject
;
;
;  ld a,(powerreserve)
;  and a
;  jp z,.droppowerup
;
;
;  ld hl,(camerax) ;scherm positie bepalen waar is onze camera
;  
;  sla l
;  rl h ;*2
;  sla l
;  rl h ;*4
;  sla l
;  rl h ;*8
;  
;  
;  ;we missen een stukje informatie in camerax die wij toevoegen door de vdp van de camera uit te lezen
;  ld		a,(VDP_8+19)
;  ld c,a
;  ld b,0
;  xor a
;  sbc hl,bc
;  ld b,h ;transfer hl->bc
;  ld c,l
; 
;  ld de,128
;  add hl,de ;position the sprite to the middle of the screen
; 
; 
;  ld (ix+xobject),l
;  ld (ix+xobject+1),h
;
;
;  ld hl,(cameray) ;scherm positie bepalen waar is onze camera
;  ld h,0
;  
;  
;  sla l
;  rl h ;*2
;  sla l
;  rl h ;*4
;  sla l
;  rl h ;*8
;
;  ld		a,(VDP_8+15) ;verticale offset fixen
;  ld b,0
;  ld c,a
;  add hl,bc
;
;  ld de,10
;  add hl,de
;
;;  ld hl,185
;
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h
;  
;  ret
;
;.droppowerup:
;
;  ld (ix+movepat),54
;  ld (ix+v2),1
;  ret



MovementPattern54:     ;powerreservedrop

  call mariointeract
  jp c,.pickuppower


  ld a,(ix+v4)
  cp 4
  jp z,.setfeatherdrop

  call  addv2toybject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  ld a,(ix+v4)
  cp 1
  jp z,.greenshroom
  cp 2
  jp z,.redshroom
  cp 3
  jp z,.flower
  cp 5
  jp z,.star

  
  ret

.redshroom:
;TODO: update the sprite accordingly
;meteen de sprite updaten  
  ld    hl,spriteaddrex20  ;color address
  ld    bc,spriteaddrex20c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.greenshroom:

  ld    hl,spriteaddrex21  ;color address
  ld    bc,spriteaddrex21c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.flower:

  ld    hl,spriteaddrex26  ;color address
  ld    bc,spriteaddrex26c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.star:

  ld    hl,coloraddressstaryellow  ;color address
  ld    bc,characteraddressstaryellow  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret

.pickuppower:

  ld b,(ix+v4)
  jp settransform


.setfeatherdrop:

  ld (ix+movepat),18
  ld (ix+v2),1
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),25
  ret



MovementPattern55:     ;1upsprite

;rise the sprite for 8 frames
  ld a,(ix+v4)
  cp 16
  jp nc,destroyobject
  
  ld d,0
  ld e,a
  
  ld hl,(marioy)
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

;  ld de,2 ;2 pixels to the right.
;  ld hl,(mariox)
;  add hl,de
;  ld (ix+xobject),l
;  ld (ix+xobject+1),h

  inc (ix+v4)


  ret



MovementPattern56:     ;banzai bill



;initiate the second part and tiles


  ld a,(ix+v4)
  cp 2
  jp z,.begin
  cp 3
  jp z,.moveleft


  call getmarioleftright
  call nc,Playsfx.explosion


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8

  inc de
  inc de
  inc de
  inc de
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force18 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),49                ;set object number (49 is banzai bill part 2)
  ld    (iy+6),0                ;set object number (49 is banzai bill part 2)

  call  activateobject

  push ix
  pop iy

  push ix
  pop hl

  pop ix  

 ld (ix+v5),l
 ld (ix+v5+1),h

  push ix
  pop hl ;get our info
  
  ld (iy+v5),l
  ld (iy+v5+1),h ;write away to new object

 
  inc (ix+v4)
  
  ret
  
  
.begin:


  call getbanzaibillcoordinates
  call putbanzaibill

  inc (ix+v4)
  ld (ix+v3),1

  ret


.moveleft:

  call mariointeract
  jp c,.killbill

.moveleftanyhow:

  call movebanzaibill
  call addv1toxbject

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  ret

.killbill:

;mario must be above banzai bill or else this may not be done
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ex de,hl
  ld de,20
  xor a
  sbc hl,de
  ex de,hl
    
  ld hl,(marioy)
  xor a
  sbc hl,de
  jp nc,.moveleftanyhow

  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld (iy+v3),1

;remove the back part
  call removebanzaibillrom

;add more drama to the death of banzai bill
  ld a,1
  ld (tripscreen),a

;take the sprites out of the sprtlist and screen then spawn a lot of debris in place
  
;  call removeobjectspat

;  call spawnbanzaibilldebris

;kill the sprite and object
  jp destroybanzaibill


getbanzaibillcoordinates:


  ;get the mapposition of banzai bill first based on his coordinates.

  ;set map address y
  ld    l,(ix+yobject)          ;y value of sprite (16 bits)
  ld    h,(ix+yobject+1)        ;y value of sprite (16 bits)

  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  inc l

  ld    a,l
  

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

  ;set map address x
  ld    e,(ix+xobject)          ;x value of sprite (16 bits)
  ld    d,(ix+xobject+1)        ;x value of sprite (16 bits)

  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;/set map address x
  inc e
  inc e
  inc e
;  inc e


  add hl,de
 
  ld (banzaimapadress),hl

  ret


spawnbanzaibilldebris:

 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  dec l
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
  dec de


  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),de

  push ix

;TODO: we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),2
    ld (ix+v2),8

  ld hl,(blockhlstorage) ;hl opslaan
  ld de,(blockbcstorage)

;object 2
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)

;object 3

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
    ld (ix+v2),8

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)

;object 4

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),2


  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)
  
;object 5

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),4
    ld (ix+v2),2


  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)
  

;object 6

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),50                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-4
    ld (ix+v2),2

  pop ix

  call Playsfx.breakblock

  ret


MovementPattern57:     ;banzai bill (part2)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  dec a
  jp z,.createdebris


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  
  push hl
  pop iy
  
  ld a,(iy+movepat)
  cp 56
  jp nz,.destroy
  
  ld l,(iy+xobject)
  ld h,(iy+xobject+1)
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret

.destroy:

  ld hl,0
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),l
  ld (ix+yobject+1),h  

  ret


;we spawn the debris in the second sprite because the first sprite will allready be removed by then.
;leaving 10 sprites for the debris
.createdebris:

  call spawnbanzaibilldebris
;  jp destroyobject
  jp .destroy



MovementPattern58:     ;banzai bill stardebris


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ret




MovementPattern59:     ;banzai bill stardebris high



  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  ret



MovementPattern60:     ;banzai bill destruction debris


  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  call objectdeathjump

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex83  ;color address
  ld    bc,spriteaddrex83c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex84  ;color address
  ld    bc,spriteaddrex84c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex85  ;color address ;02
  ld    bc,spriteaddrex85c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex86  ;color address ;03
  ld    bc,spriteaddrex86c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position4:
  ld    hl,spriteaddrex87  ;color address ;03
  ld    bc,spriteaddrex87c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.position5:
  ld    hl,spriteaddrex88  ;color address ;03
  ld    bc,spriteaddrex88c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset





;ret


MovementPattern61:     ;pinkshell that goes into tube with mario


  ld a,(nocontrol)
  and a
  jp z,.reactivateshell
  
.dorest:  

  ld hl,(mariox)
  ld de,3
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy5
  ld de,15
.bigguy5: 
  
  ld hl,(marioy)
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld a,(ix+v4)
  and a
  call z,.animateshellonce
  

  ld a,(marioblueblock)
  dec a
  jp z,.animateblueblock


  ld a,(mariopbox)
  dec a
  jp z,.animatepbox
  dec a
  jp z,.animatespring
  

 ; ld    hl,spriteaddrex12  ;color address ;02
 ; ld    bc,spriteaddrex12c  ;character address
 ; ld    de,offsetshell
 ; ld    a,ingamespritesblock1
 ; jp    setcharcolandoffset
    ret
 
.animatespring:

  ld    hl,spriteaddrex504  ;color address
  ld    bc,spriteaddrex504c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 


.animateblueblock:


  ld    hl,spriteaddrex214  ;color address
  ld    bc,spriteaddrex214c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 

.animatepbox:


  ld    hl,spriteaddrex321  ;color address
  ld    bc,spriteaddrex321c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 


;ret

.animateshellonce:

;save the ix value to RAM for future reference
  push ix
  pop hl
  ld (holdingix),hl

  ld a,(marioshelltype)
  ld (ix+objectrelease),a  
  
    ld (ix+v3),1 ;set position  
    call animateshell
    ld (ix+v4),1

    
    ret


.reactivateshell:

  call .dorest

  ;spawn a new shell in place that is forced into a new sprite position
 

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number (pinkshell)
  ld    (iy+6),0                ;set object number (pinkshell)

  call  activateobject
  
  
  ld (ix+movepat),11
  ld (ix+clipping),2
  ld (ix+v3),3
  ld (ix+v4),0
  ld a,(marioshelltype)
  ld (ix+objectrelease),a
  xor a
  ld (marioshelltype),a
  call animateshell
 
  ld a,(marioblueblock)
  dec a
  call z,.spawnblueblock

  ld a,(mariopbox)
  dec a
  push af
  call z,.spawnpbox
  pop af
  dec a
  call z,.spawnspring


  pop ix

  jp destroyobject


.spawnblueblock:


  ld hl,350
  ld (ix+v5),l 
  ld (ix+v5+1),h ;set the timer for dissapearance in 16 bit
  ld (ix+movepat),94
  ld (ix+deadly),0
  ld (ix+clipping),2
  ld (ix+objectrelease),2
  

  ret


.spawnpbox:


  ld (ix+movepat),94
  ld (ix+deadly),0
  ld (ix+clipping),0
  ld (ix+objectrelease),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex321  ;color address
  ld    bc,spriteaddrex321c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 

.spawnspring:

  ld (ix+movepat),94
  ld (ix+deadly),3
  ld (ix+clipping),0
  ld (ix+objectrelease),3

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex504  ;color address
  ld    bc,spriteaddrex504c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

 ; ret

;  jp destroyobject

;  ret



MovementPattern62:     ;funny mushroom

  ld a,(funloop)
  and a
  call z,.setfall


;do the steps  
  ld a,(ix+v4)
  cp 1
  jp z,.trowup
  cp 2
  jp z,.falldown
  cp 3
  jp z,.kick
  cp 4
  jp z,.fall
  cp 5
  jp z,.lookup
  cp 6
  jp z,.finish

  ;wait some time, loop up then trow the shroom up
  ld a,(ix+deathtimer)
  cp 60
  ret nz
  
  ld (ix+v4),5
  ld (ix+v3),0


  ret

.finalframe:

;finish it of and kick the shroom away
  ld (ix+v4),3



  ret



.nextframe:

  inc (ix+v4)
  ld (ix+v3),0
  
  ret


.setv4to1:



  ld (ix+v4),1
  ld (ix+v3),0 ;reset the counter
  ld (ix+deathtimer),0
;  ld (ix+v2),9
  
  ret



.lookup:
  
  
  ld a,1
  ld (mariolook),a
  ld a,(ix+v3)
  cp 30
  jp z,.setv4to1
  inc (ix+v3)
  ld (ix+v4),1
  ret
  


.trowup:
  
  inc (ix+v3)
  
  ld a,(ix+v3)
  cp 15
  ret c
  cp 16
  call z,.kickshellanim
  

  
  
  ld a,(ix+v3)
  cp 23
 ld b,23 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown

  ld (ix+v2),9
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ret
  
  
.falldown:

  inc (ix+killshell)

  call .fall
    
  ld a,(ix+killshell)
  cp 19
  jp nc,.finalframe  
    
    
  ret


.kick:


;transform engine een trap geven
      xor a
      ld (mariolook),a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a
      
      ld (ix+v4),6
;      ld (ix+v1),5 
  ret



.finish:

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call  checkobjecttilesfloor
;  ret c 

  
  ret

.kickshellanim:



;transform engine een trap geven
      xor a
  ;    ld (mariolook),a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ret


.setfall:

  ld a,(ix+v4)
  cp 4
  ret z

  ld a,(ix+v2)
  cp 8
  jp z,.nov3reset
  
  
  ld (ix+v3),0
.nov3reset:
  ld (ix+v4),4


  ret


.fall:

  call .accelerate
  call addv2toybject



  ret


.accelerate:
  
  ld d,0
  ld e,(ix+v3)
  ld hl,shellfallacctable
  add hl,de
  ld a,(hl)
  cp 8
  jp z,.setmaxspeed
  
  ld (ix+v2),a
  inc (ix+v3)
  
  ret

.setmaxspeed:

  ld (ix+v2),8
  ret

.slowdown:

  ld a,(ix+v3)
  sub 23
  ld d,0
  ld e,a
  ld hl,shellfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe
  
  
  ld (ix+v2),a
 
;  call shelluptilecheck
;  jp c,.nextframe 

  call  subv2toybject

  ret
  

MovementPattern63:     ;funny balloon


  ld a,(funloop)
  and a
  jp z,destroyobjectwithcloud


;do the steps  
  ld a,(ix+v4)
  cp 1
  jp z,.lookup
  cp 2
  jp z,.letgo


  ;wait some time, loop up then trow the shroom up
  ld a,(ix+deathtimer)
  cp 60
  ret nz
  
  ld (ix+v4),1
  ld (ix+v3),0




  ret

.setv4to2:
  
  xor a
  ld (marioshell),a


  ld (ix+v4),2
  ld (ix+v3),0
  ret



.lookup:
  
  
  ld a,1
  ld (mariolook),a
  ld a,(ix+v3)
  cp 30
  jp z,.setv4to2
  inc (ix+v3)
  ld (ix+v4),1
  ret


.letgo:

  ld a,(ix+v3)
  cp 100
  call z,.resetmariolook
  inc (ix+v3)
  


  call subv2toybject


  ret

.resetmariolook:

  xor a
  ld (mariolook),a
  
  ret


MovementPattern64:     ;tubecoverup for mario comming in up and down


;delay the process for 1 frame so that the sound can be played
  ld a,(ix+v3)
  cp 2
  jp z,.gosmall
  and a
  jp nz,.go

  ld (ix+v3),1

  call Playsfx.pipe ;play exit sound

  ld a,(mariostate)
  and a
  jp z,.gosmall


.go:

  ld a,(ix+v4)
  cp 35
  jp nc,.startlevel

  ld a,(ix+objectrelease)
  dec a
  jp z,.up


;.down:

;  ld hl,(marioy)
;  inc hl
;  ld (marioy),hl

  jp .workupspritedown



.up:

  ld hl,(marioy)
  dec hl
  ld (marioy),hl

  ld a,(marioyoshi)
  and a
  jp nz,.upmarioyoshi

  ld a,(marioshell)
  dec a
  call z,.shellup


  ld a,(ix+v4)
  cp 8
  jp c,.part1
  cp 24
  jp nc,.done


  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
;	add a,8
  sub 8
  call hidemariosprite
  

  jp .done


.shellup:

  ld a,(ix+v4)
  cp 24-1
  ret nc
  cp 8
  jp nc,.shellstep1


  ld hl,(holdingix)
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite 
  


  ret


.shellstep1:

  ld hl,(holdingix)
  push hl 
  pop iy
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  sub 7
  ld bc,0*32
  call unhideobjectsprite 



  ret

  
  
.part1:

  ld a,(ix+v4)
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	add a,8
  call hidemariosprite
  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	xor a
  call hidemariosprite  


  jp .done




.upmarioyoshi:

  ld a,(ix+v4)
  cp 24-1
  jp nc,.done
  cp 8
  jp nc,.part1yoshi



  ld hl,(yoshiix)
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite 

  jp .marioonyoshi
  




.part1yoshi:

  ld hl,(yoshiix)
  push hl 
  pop iy
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  sub 7
  call unhideobjectsprite 

  jp .marioonyoshi


.marioonyoshi:

  ld a,(ix+v4)
  cp 4+16-2
  jp nc,.done
  cp 4
  jp nc,.part1my


;  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite
  

  jp .done
  
  
.part1my:

  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	sub 2
  call hidemariosprite 


;  jp .done


.done:


  inc (ix+v4)

  ret


;for the small mario

.gosmall:

  ld (ix+v3),2
  
  ld a,(ix+v4)
  cp 35-8
  jp nc,.startlevel

  ld a,(ix+objectrelease)
  dec a
  jp z,.up


.workupspritedown:

  ld hl,(marioy)
  inc hl
  ld (marioy),hl

  inc (ix+v4)
 
  ld a,(marioyoshi)
  and a
  jp nz,.handlemarioonyoshi

  ld a,(marioshell)
  and a
  call nz,.hideshelldown  

  ld a,(ix+v4)
  cp 17+8-1
  jp c,.part1downsmall

  
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld c,a
	ld a,32
	sub c
  add a,8
  call hidemariospritedown 
  
  ret


.hideshelldown:


  ld hl,(holdingix)
  push hl 
  pop iy
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  cp 32-8
  ret nc
	ld c,a
	ld a,16
	sub c
	add a,8
  call unhideobjectspriteupdown


  ret


.part1downsmall:

  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld c,a
	ld a,16
	sub c
  add a,8
  call hidemariospritedown 

  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite


  ret


.handlemarioonyoshi:

;yoshi
  ld a,(ix+v4)
  cp 17+8-1
  jp c,.part2yoshi
;  cp 26
;  ret nc


  ld hl,(yoshiheadix)
  push hl 
  pop iy
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
	ld c,a
	ld a,32
	sub c
	add a,8
  call unhideobjectspriteupdown

;mario
  ld a,(ix+v4)
  cp 31
  jp nc,.next
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld c,a
	ld a,31
	sub c
  call hidemariospritedown 

.next:

  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite


  ret
;part2yoshi
 
.part2yoshi: 
  
  ld hl,(yoshiheadix) ;the body is now completely submerged
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite

  ld hl,(yoshiix)
  push hl 
  pop iy
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
	ld c,a
	ld a,16
	sub c
	add a,8
  call unhideobjectspriteupdown 

;take care of mario
  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld c,a
	ld a,16
	sub c
  add a,8+8
  call hidemariospritedown 

  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite


  ret

.hidemariopartonyoshi:

  ld a,(ix+objectrelease)
  and a
  ret nz ;cheap regression fix

  ld a,(mariostate)
  and a
  ret nz

;take care of mario
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  ld a,2
  call hidemariospritedown 

  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite


  ret




;ret



.startlevel:

  ld a,(marioyoshi)
  and a
  call nz,.hidemariopartonyoshi

;wait just a little bit longer to make it look nicer
  inc (ix+v4)

  ld a,(ix+v4)
  cp 40
  ret nz

  xor a
  ld (standingonexit),a
  ld (nocontrol),a
  ld (marioduck),a

  jp destroyobject

;ret


MovementPattern65:     ;empty


  ret


MovementPattern67:     ;yoshicoin

  ld a,(yoshicoinscolected)
  and a
  jp nz,destroyobject

  call mariointeract
  jp c,.pickupcoin
  
;  jp c,destroyobjectwithsparkle ;TEMP


  
.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 8
  jp    z,.position1
  cp 16
  jp    z,.position2
  cp 25
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex89  ;color address
  ld    bc,spriteaddrex89c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex90  ;color address
  ld    bc,spriteaddrex90c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex91  ;color address ;02
  ld    bc,spriteaddrex91c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



  ret


.pickupcoin:

  call Playsfx.yoshicoin

;make sure the coin gets added to the yoshicoin pickup table
  ld hl,yoshicoinpickuptable
.storetableloop:
  ld bc,999
   xor a
   ld e,(hl) ;check if allready containing a value
   inc hl
   ld d,(hl)
   ex de,hl
   sbc hl,bc 
   jp z,.donestoreloop
   ex de,hl
   dec hl
   
   ld bc,0
   ld e,(hl)
   inc hl
   ld d,(hl)
   ex de,hl
   xor a
   sbc hl,bc
   ex de,hl
   inc hl ;check next word not byte

   jp nz,.storetableloop ;do next entry on zero


  ld c,(ix+xobject)
  ld b,(ix+xobject+1)
  srl b 
  rr c   ;/2
  srl b
  rr c ;/4
  srl b
  rr c ;/8 
   
   dec hl
   dec hl
   ld (hl),c
   inc hl
   ld (hl),b
  
.donestoreloop:  

  ld a,1
  ld (scorebaractive),a
  ld (showyoshi),a

  ld a,(yoshicoin)
  inc a
  cp 5
  jp z,.extralife
  ld (yoshicoin),a
  
  jp destroyobjectwithsparkle
  
.extralife:

  ;todo: make sure mario gets extra life
  call Playsfx.oneup
  ld a,(lives)
  inc a
  ld (lives),a
  
  ld a,1
;  ld (yoshicointemp),a
  ld (yoshicoinscolected),a
  
  jp destroyobjectwithsparkle


MovementPattern68:     ;empty

    ret


MovementPattern69:     ;coin

  ld a,(waterlevel)
  and a
  jp nz,.changetospecial

  call mariointeract
  jp c,.pickupcoin


  ld a,(ix+v4)
  cp 5
  jp c,.goup

  
;this is the make the coin move really slow  
  ld (ix+v1),0
  ld a,(framecounter)
  and 3
  call z,.moveonepixel
  
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

;  ld    a,(framecounter)
;  and   3                       ;this sprite animates once every 4 frames
;  call  z,.animate
  call .animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call checkobjecttilescheckwall
  call    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checkobjecttilesfloor
  jp nc,.checkexit
  ret

.changetospecial:

  ld (ix+movepat),206
    ld (ix+v1),1
  ret

;check if this is a level end. If so remove the coin directly upon contact
.checkexit:


  ld a,(nocontrol)
  cp 3
  jp z,.pickupcoin
  

  ret



.moveonepixel:

  ld a,(ix+killshell) ;abuse this variable to push the v1
  ld (ix+v1),a

  ret


.goup:

  ld (ix+v2),4
  call subv2toybject
  inc (ix+v4)
  
  xor a
  ld (ix+v2),a
  ret



.wallfound:

;  .hardforegroundfound:
  ;change direction
  ld a,(ix+killshell)
;  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ld (ix+killshell),a

  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret

.pickupcoin:

  ld (ix+objectrelease),1

  call handlecoin
  ;put whatever is needed here

  call Playsfx.coin

  jp destroyobjectwithsparkle


MovementPattern70: ;coin release


  ld a,(ix+v4)
  and a
  jp nz,.waitforpowerup

  
  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    2                       ;4 frames moving upwards nu 3 looks better
  ret   nz

  ld (ix+v4),1

  call setspritetobrownblock

.spawnpowerup:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
;  inc e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),71

  pop ix

  ret



;wait until the powerup has fully risen from the block then destroy this object
.waitforpowerup:

  inc (ix+v4)
  ld a,(ix+v4)
  cp 15
  jp nc,destroyobject
  ret





MovementPattern71:

  ld (ix+objectrelease),1

  call .animate

  ld a,(ix+v4)
  cp 3
  jp c,.goup
  cp 6 ;8
  jp c,.godown


  call handlecoin
  
  jp destroyobjectwithsparkle


  ;ret

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret


.goup:

  ld (ix+v2),8
  call subv2toybject
  inc (ix+v4)
  ret

.godown:

  call addv2toybject
  inc (ix+v4)
  ret





MovementPattern72: ;yoshi egg

  ld a,(ix+v3)
  dec a
  jp z,.waitforcrack
  dec a
  jp z,.waitforcrack2 ;wait for yoshi to come out
  dec a
  jp z,.growyoshi
;  dec a
;  jp z,.yoshi


  ld a,(ix+v4)
  cp 20
  jp c,.popup

  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  


  ret



.popup:

  ld a,(ix+v4)
  cp 16
  jp nc,.nextframe


  ld (ix+v1),0
  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v4)  
  ld (ix+objectfp),0

 ret


.nextframe:

  ld (ix+v2),0
  ld (ix+v3),1 ;this indicates that the egg is ready to spawn
  ld (ix+v1),0 ;this is our timer for the hatching process
  inc (ix+v4)

  ret



.waitforcrack:

  ld a,(ix+v1)
  cp 15
  jp z,.begincrack
  
  inc (ix+v1)



  ret

.begincrack:


  ld (ix+v3),2 ;begin the crack
  ld (ix+v1),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex172  ;color address ;02
  ld    bc,spriteaddrex172c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.waitforcrack2:

  ld a,(ix+v1)
  cp 10
  jp z,.beginyoshi
  
  inc (ix+v1)



  ret


.stop:


  pop ix
 
 
   ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),15                ;set object number (15 is green mushroom)
  ld    (iy+6),0                ;set object number (15 is green mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v4),20 ;spawn directly without delay

  pop ix




  jp destroyobject
 
 

  


  
.beginyoshi:

    ld (ix+v1),0
  

;spawn the debris
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

  ld (hlvar),hl
  ld (bcvar),de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),80               ;set object number (80 = yoshi egg debris)
  ld    (iy+6),0               ;set object number (80 = yoshi egg debris)

  call  activateobject

  ld (ix+deathtimer),0
;  ld (ix+clipping),0
;  ld (ix+movepat),71

  ld hl,(hlvar)
  ld de,(bcvar)


  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),81               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0

 
  pop ix
 


;detect that yoshi has allready been spawned and is in screen. If a yoshi allready exsist this must branch to a whole different routine. Then we must spawn a 1up and continue as such
  push ix


  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 19
;  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix+objectrelease) : cp 255 : jp z,.stop ;objectnr 20

  pop ix



;TODO: spawn the right type of yoshi from egg based on the objectrelease number
    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex173  ;color address ;02
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  ld (ix+objectrelease),255 ;confirm that yoshi has now spawned and set the objectrelease properly
  
  
      ld (ix+v3),3 ;yoshi comes out
  
  ret



.growyoshi:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v1)               ;v3 is used as animation variable
  inc a
  ld    (ix+v1),a               ;v3 is used as animation variable
  cp 1
  jp    z,.position0
  cp 3
  jp    z,.position1
  cp 5
  jp    z,.position2
  cp 7
  jp    z,.position3
  cp 8
  ret c

  ld (ix+v3),4
  ld (ix+v4),0
  ld (ix+v2),1
  ld (ix+v1),0

  jp .makeyoshi


.position0:
  ld    hl,spriteaddrex173  ;color address
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1: 
  ld    hl,spriteaddrex174  ;color address
  ld    bc,spriteaddrex174c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex173  ;color address ;02
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex174  ;color address ;02
  ld    bc,spriteaddrex174c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
 

;this is a difficult one. yoshi is built out of two sprites so we have to spawn a extra sprite to the object and change this one into the proper body sprite
;another aproach is to higher the spritecount.
.makeyoshi:


;get the coordinates and spawn the head 
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),79               ;set object number (79 = yoshi head)
  ld    (iy+6),0               ;set object number (79 = yoshi head)

  call  activateobject

  ld (ix+deathtimer),0
    ld (ix+v2),1
;  ld (ix+clipping),0
;  ld (ix+movepat),71

;adjust the head with 1 pixel to make it fit correctly
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld (yoshiheadix),ix ;store the adress of the yoshihead so we can do modifications on it from anywhere

  pop ix
 

;set the body sprite correctly  
  ld    hl,spriteaddrex175  ;color address ;02
  ld    bc,spriteaddrex175c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset
  

 ; ret

.yoshi:


  ld (yoshiix),ix ;store the ix value of yoshi which is used by the killroutine
  ld (ix+movepat),105 ;make yoshi passive/active
  ld (ix+v4),0 ;reset this we use this as the activation timer
  ld a,1
  ld (yoshileftright),a ;yoshi is always facing the right on spawn
;do the up and down movements the head has its own animation
;this is not an actual animation but a true movement of the entire object which is faster then updating the sprite each 2th frame.

;  call mariointeract
;  jp c,.activate


  ret



MovementPattern73: ;koopa all colors

  
  ld a,(ix+v5)
  and a
  jp z,.nodec
  dec (ix+v5)

.nodec:

  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.forcewallfound;.wallfound
  pop af
  cp 3
    jp z,.turnupsidedown
;  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.turnupsidedown


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld a,(ix+v4)
  and a
  jp nz,turnkoopaaround



 ; xor   a                       ;reset vertical movement speed
  ld    (ix+v2),0               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animatekoopa

  ld a,(currentlevel) ;to make it run a little bit faster on the allrwady very slow map6 ;)
  cp 6
  jp z,.checktile

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

;green koopa will check for the floor and walk over the ledge. Red and brown ones turn around at the ledge
  
  ld a,(ix+objectrelease)
  cp 0
  jp z,.checktile
  cp 1
  jp z,.checktile
  cp 4
  jp z,.checktile

  call  checkobjecttilesfloor
  ret c

  jp .endcheckfloor

.checktile:

  call  checkobjecttilesfloor
  ret c

  call  checkobjecttilesfloorbelowfeet
  jp c,.wallfound
  

.endcheckfloor:

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld a,(ix+v7)
  and a
  ld (ix+v7),0
  ret nz

  xor a
  ld a,(ix+standingonspr)
  dec a
  call z,checkforspritewall
  ret c

  ld a,(ix+standingonspr) ;when standing on sheertiles don't check
  cp 2
  ret nc

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile

.forcewallfound:

  ;use counter if this is going on too much and too long we return
  ld a,(ix+v5)
  add a,6 ;8
  ld (ix+v5),a
  cp 30
  jp nc,.maxforce
  cp 24 ;32
  ret nc

;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ld (ix+v4),1 ;we turn around

  jp animatekoopa

.maxforce:

  ld (ix+v5),30
  ret

.otherobjectdeathjump:

;first we remove the sprites we no longer need then we set the sprite to the upsidedown shell
;in the end we issue the objectdeathjump

  call removeobjectspat

  ;  ;remove 2 sprites from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist

  inc hl
  inc hl
;  ld    (hl),0
  ld a,(hl)
  dec a
  ld (hl),a
  inc   hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0   

  
;  ;/remove sprite from spritelist  
 
  
  ld (ix+amountspr),2 ;set the new amount of spritenumbers

;save the speed of the hitting object  
  push bc
  
  call turnshellupsidedown

  pop bc

  jp otherobjectdeathjump



  ;ret



.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.otherobjectdeathjump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud

  ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud


  ld (ix+movepat),79  ;we set the movementpattern to a shell but we wait a few frames before the shell truly becomes active
  ld (ix+v4),0

;first we remove the sprites we no longer need then we set the sprite to the upsidedown shell
;in the end we issue the objectdeathjump

  call removeobjectspat

  ;  ;remove 2 sprites from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist

  inc hl
  inc hl
;  ld    (hl),0
  ld a,(hl)
  dec a
  ld (hl),a
  inc   hl
  ld a,(hl)
  dec a
  ld (hl),a
  ;ld    (hl),0   

  
;  ;/remove sprite from spritelist  
  
  ld (ix+amountspr),2 ;set the new amount of spritenumbers
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a


  ld a,16
  ld (ix+sizeY),a

;lower the sprite for better detection
   ld l,(ix+yobject)
   ld h,(ix+yobject+1)
   ld de,16
   add hl,de
   ld (ix+yobject),l
   ld (ix+yobject+1),h


  ld (ix+deadly),0

;make the shell appear
  ld (ix+v3),3
  call animateshell

  call Playsfx.kickenemy


;spawn the naked koopa
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  ld b,(ix+objectrelease)

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix
  push bc

  ld    iy,CustomObject
  ld    (iy),available ;doesn't matter
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),65                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),75 ;set the movementpattern to the naked coopa shooting out
  pop bc
  ld (ix+objectrelease),b ;inherit the objectrelease value


  call getmarioleftright
  jp c,.right

.left:

  ld (ix+v1),4
  ld (ix+v3),1
  
  push ix
  pop iy
  
  pop ix

  ld a,(ix+standingonspr)
  ld (iy+standingonspr),a
  
  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  ld (iy+lockdata),l
  ld (iy+lockdata+1),h
  
  ret


  

.right:

  ld (ix+v1),-4
  ld (ix+v3),0

  push ix
  pop iy

  pop ix

  ld a,(ix+standingonspr)
  ld (iy+standingonspr),a

  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  ld (iy+lockdata),l
  ld (iy+lockdata+1),h
  
  ret


.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


;we turn the sprite upside down and then transform koopa into a upside down shell.
.turnupsidedown:



;first we remove the sprites we no longer need then we set the sprite to the upsidedown shell
;in the end we issue the objectdeathjump

  call removeobjectspat

  ;  ;remove 2 sprites from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist

  inc hl
  inc hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0
  inc   hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0   

  
;  ;/remove sprite from spritelist  
  
  ld (ix+amountspr),2 ;set the new amount of spritenumbers

;save the speed of the hitting object  
  push bc
  
  call turnshellupsidedown

  pop bc


    ld (ix+sizeY),16
    ld (ix+clipping),2 ;becomes deadly to enemies
    ld (ix+deadly),0 ;won't kill mario
    ld (ix+deathtimer),0
    ld (ix+objectfp),0
    ld (ix+v2),8
    ld (ix+movepat),80

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  add hl,de
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)



  ret



MovementPattern74: ;naked koopa

  ld a,(ix+v6)
  and a
  jp z,.nodec
  dec (ix+v6)
.nodec:

  call objectinteract
  cp 4
  jp z,.nextframe
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,MovementPattern26.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animatenakedkoopa


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c

  ld a,(ix+v7)
  and a
  ld (ix+v7),0
  ret nz

  ld a,(ix+objectrelease)
  cp 2
  jp z,.skipledgecheck
  cp 3
  jp z,.skipledgecheck
  
  call checkobjecttilesfloorbelowfeet
  jp c,.wallfound  
  
.skipledgecheck:
  
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret

.maxforce:

  ld (ix+v6),30
  ret

.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


  ;use counter if this is going on too much and too long we return
  ld a,(ix+v6)
  add a,6 ;8
  ld (ix+v6),a
  cp 30
  jp nc,.maxforce
  cp 24 ;32
  ret nc

;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  
  call addv1toxbject
  call addv1toxbject
  
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  jp animatenakedkoopa

.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret

.nextframe:

  ld a,(ix+objectrelease)
  cp 4 ;kicker
  jp z,.nextframe3


  ld (ix+movepat),77
  ld (ix+v4),0
  ld (ix+v2),4

;if koopa falls onto the shell we skip the jumping sequence
  call  checkobjecttilesflooronly
  jp nc,.nextframe2

  ret

.nextframe2:


  ld (ix+v4),6

  ret


.nextframe3:

  push iy
  pop hl
  ld (ix+v5),l ;store shell info 
  ld (ix+v5+1),h

  ld (ix+movepat),193
  ld (ix+v4),0
;  ld (ix+v2),0
;  ld (ix+v1),0

  ret



MovementPattern75: ;naked koopa shooting out of shell

  
;  call objectinteract
;  cp 4
;  call z,.wallfound
;  cp 2
;  jp z,otherobjectdeathjump
;  cp 1
;  call z,.wallfound
;  cp 3
;  jp z,otherobjectdeathjump
;  cp 5
;  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animatenakedkoopaslide

  call brakenakedkoopa

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  
  ;when speed is zero we start the timer to transform this koopa into a walking naked koopa
  ld a,(ix+v1)
  and a
  jp z,.transform
  
  
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld (ix+v1),0
  
  ret
  

.nextframe:
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud
 
  ld (ix+movepat),27  ;generic set
  
  call Playsfx.kickenemy
 
 ;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a
 
  
;  call MovementPattern27.animate ;animeer
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex41  ;color address ;03
  ld    bc,spriteaddrex41c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret

  
.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



.transform:

  ld a,(ix+v3)
  and a
  jp z,.transformright

  ld (ix+v3),0


  ld (ix+movepat),76
  ld (ix+v4),0 ;reset v4 for further use
  ld (ix+deathtimer),0
  
  ret
  

.transformright:

  ld (ix+v3),2


  ld (ix+movepat),76
  ld (ix+v4),0 ;reset v4 for further use
  ld (ix+deathtimer),0
  
  ret




brakenakedkoopa:

  call checkobjecttilesfloorbelowfeet ;do not allow to slide into a ledge
  ret c

;we check if we are near objects, if so we slide on
  call objectinteract
  and a
  ret nz


  ld a,(ix+v3)
  and a
  jp z,.right

.left:


  
  ld hl,nakedkoopaslowdowntableleft
  ld d,0
  ld e,(ix+v4)
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  cp 0
  ret z
  
  inc (ix+v4)

  ret


.right:

  ld hl,nakedkoopaslowdowntableright
  ld d,0
  ld e,(ix+v4)
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  cp 0
  ret z
  
  inc (ix+v4)

  ret


nakedkoopaslowdowntableleft:   db 4,4,4,4,3,3,3,3,2,3,2,3,2,3,2,2,2,2,2,1,2,1,1,1,1,0
nakedkoopaslowdowntableright:  db -4,-4,-4,-4,-3,-3,-3,-3,-2,-3,-2,-3,-2,-3,-2,-2,-2,-2,-2,-1,-2,-1,-1,-1,-1,0



MovementPattern76:

  ld a,(ix+v4)
  dec a
  jp z,.jumpupandtransform
  dec a
  jp z,animatenakedkoopaupsidedown
  dec a
  jp z,.transformfinal


  call objectinteract
;  and a
;  call nz,.resetdeathtimer ;no longer needed because koopas have more brains now!
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,MovementPattern26.nextframe
  
;  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
;  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animatenakedkoopatransforming

  


  ld a,(ix+deathtimer)
  cp 80
  jp nc,.nextframe


  ret



.nextframe:

  ld (ix+v4),1
  ld (ix+deathtimer),0

  ret




;.resetdeathtimer:
;
;  cp 4 ;not if the contact is a shell
;  ret z
;
;  ld (ix+deathtimer),0
;
;
;  ret


.jumpupandtransform:


  ld (ix+v2),4
  call subv2toybject
  
  ld a,(ix+deathtimer)
  cp 3
  ret c
  
  ld (ix+v4),2
  ld (ix+clipping),1


  ret



.transformfinal:



  ld (ix+v1),-1
  ld (ix+v2),0
  ld (ix+movepat),74

;convert the v3 value back to binary system
  ld a,(ix+v3)
  cp 1
  jp nc,.right

  
  ld (ix+v3),1

  ret

.right:


  ld (ix+v3),0

  ret


MovementPattern77:

  ld a,(ix+v4)
  cp 5
  jp nc,.transformshellintocoopa



  call addv1toxbject
  call subv2toybject


  inc (ix+v4)
  
  ret


.transformshellintocoopa:


  call objectinteract

  ld a,(iy+movepat)
  cp 11
  call z,.releaseshell



  ld (iy+v3),0
  ld (iy+v4),0
  ld (iy+v1),1
  ld (iy+v2),0
  ld a,(ix+objectrelease) ;inherit color
  ld (iy+objectrelease),a
  
  ld a,(ix+objectrelease)
  cp 2
  jp z,.transformintorainbowshell
  
  ld (iy+movepat),78 ;change the shell into a coopa again


  jp destroyobject

;  ret

.transformintorainbowshell:


  ld (iy+movepat),195 ;transform into rainbow shell
  ld (iy+v4),10 ;set pointer to middle
  ld (iy+v7),0
  ld (iy+deadly),1
  ld (iy+clipping),2
  ld (iy+objectrelease),5

  jp destroyobject

;  ret  


.releaseshell:

  xor a
  ld (marioshell),a
  ret



MovementPattern78:

;show the little eyes
  call animaterevertkoopa

;we wait a few frames for the little bugger to pop out of its shell otherwise mario may die for no good reason.  
  ld a,(ix+v3)
  and a
  call nz,.wait
  cp 3
  jp z,.spawn
  
  ld a,(ix+v4)
  cp 5
  jp z,.transform

  
  call objectinteract
  cp 2
  jp z,pinkshelldeathjump
  cp 4
  jp z,pinkshelldeathjump
  cp 3
  jp z,pinkshelldeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,pinkshellshotdeathjump


  call mariointeract
  jp c,.nextframe

;shake the shell
  call addv1toxbject
  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  inc (ix+v4)


  ret
  
.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,pinkshelldeathjump 
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud

  ld a,(ix+v3)
  cp 2
  jp c,.wait

.spawn:


  ld (ix+movepat),1  ;we set the movementpattern to a shell


  
;  ;/remove sprite from spritelist  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a



  ld (ix+deadly),0

;make the shell appear
  ld (ix+v3),3
  call animateshell

  call Playsfx.kickenemy


;spawn the naked koopa
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  ld b,(ix+objectrelease)

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix
  push bc

  ld    iy,CustomObject
  ld    (iy),available ;doesn't matter
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),65                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),75 ;set the movementpattern to the naked coopa shooting out
  pop bc
  ld (ix+objectrelease),b ;inherit the objectrelease value


  call getmarioleftright
  jp c,.right

.left:

  ld (ix+v1),4
  ld (ix+v3),1
  
  
  pop ix
  
  ret



.right:

  ld (ix+v1),-4
  ld (ix+v3),0


  pop ix

  ret



.wait:

  inc (ix+v3)
  ret

;transform back into koopa
.transform:


  call removeobjectspat

  ld (ix+v1),-1
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld b,4
  call increasespritecount
 ; ld (ix+amountspr),4
  ld (ix+deadly),1
  ld (ix+movepat),73
  ld (ix+sizeY),32
  ld (ix+clipping),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call animatekoopa
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,16
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ret





MovementPattern79:


  ld a,(ix+v4)
  cp 3
  inc (ix+v4)
  ret c

  ld (ix+v4),0
  ld (ix+movepat),1

  ret

  
    
    
    
    

    
    
    


  
  
  
  
