;wow block nr 11...!!! This madness really has to stop or we will not make it for the SCC ROM!!

HandleObjectMovement11:

;start = 356


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz


  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress11 - 356 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)


;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress11
;  add   hl,de
;  ;substract minimum base adres
;  ld de,356*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress11:

  jp    MovementPattern356     ;babyyoshi
  jp    MovementPattern357     ;endflagbar
  jp    MovementPattern358     ;coin counter (endflagbar object)
  jp    MovementPattern359     ;introcounter
  jp    MovementPattern360     ;star (static)
  jp    MovementPattern361     ;mechakoopa
  jp    MovementPattern362     ;mechakoopa (upside down)
  jp    MovementPattern363     ;stonekoopa (special jumping)
  jp    MovementPattern364     ;questionmark (special light)
  jp    MovementPattern365     ;ninja
  jp    MovementPattern366     ;The final boss!!! (start cycle)
  jp    MovementPattern367     ;The final boss!!! (final fight)
  jp    MovementPattern368     ;firerain
  jp    MovementPattern369     ;firerain (other variation)
  jp    MovementPattern370     ;final object (spawns from dying boss)
  jp    MovementPattern371     ;princess
  jp    MovementPattern372     ;final cutscene helper object
  jp    MovementPattern373     ;final cutscene moving objects
  jp    MovementPattern374     ;special2 exit
  jp    MovementPattern375     ;special2 exit superspecial
  jp    MovementPattern376     ;halloffame
  jp    MovementPattern377     ;rightexit (special3a)
  jp    MovementPattern378     ;bart
  jp    MovementPattern379     ;bartsmoke
  jp    MovementPattern380     ;bartdialogue
  jp    MovementPattern381     ;bartfight
  jp    MovementPattern382     ;superlaser powerup
  jp    MovementPattern383     ;laserbeam
  jp    MovementPattern384     ;giantdoor (super special hack)
  jp    MovementPattern385     ;mariocape (helper object)
  jp    MovementPattern386     ;beta
  
  

.return:
  

  ret


MovementPattern386:     ;beta  
  
  
  ;nothing needed here just follow the screen
  
  
  ld hl,(camerax) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8
  
  
  ;we missen een stukje informatie in camerax die wij toevoegen door de vdp van de camera uit te lezen
  ld		a,(VDP_8+21)
  ld c,a
  ld b,0
  xor a
  sbc hl,bc
  
  ld de,256-16+4
  add hl,de
  
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  
  ;en nu de y coordinaten
  
  ld hl,(cameray) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8

  ld		a,(VDP_8+15) ;verticale offset fixen
  ld b,0
  ld c,a
  add hl,bc
  
  ld de,212-16
  add hl,de
  
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h  
  
  
  
  ret
  
  
  
MovementPattern385:     ;mariocape (helper object)  
  
  
  ld bc,1
  ld de,-8
  call objecttilecheckfromrom
  cp 192 
  call nc,.activateblock
  ld de,3 ;check the right
  add hl,de
  call readanytile
  cp 192 
  call nc,.activateblock  
  
  
  jp destroyobject ;exist for 1 frame then get the hell out
  
 ;   ret
  
  
  
.activateblock:  
  
  
  cp 204
  ret nc
  
  ;check for boundaries on map otherwise the engine could write into page 0 or page 3 --> massive crash [poesje]
  ;push hl
  ld de,$4000
  xor a
  sbc hl,de
  add hl,de
  ;pop hl
  ret c
  
  ;push hl
  ld de,$C000
  xor a
  sbc hl,de
  add hl,de
  ;pop hl
  ret nc
  
  ;push af
  
  ld    a,1
  ld    (activatenewblock?),a   ;mario activates a new block
  ld    (blocksactive?),a       ;there is now at least 1 block active, so activate blockhandler
  ld    (blockaddress),hl       ;hl points to tile in map  
  
  ;pop af
  
  ret
  
  
  
  
MovementPattern384:     ;giantdoor (super special hack)

  ;  call followmario
    ld hl,(marioy)
    ld de,23*8
    xor a
    sbc hl,de
    jp nc,followmario    
    
    
    
    ld hl,21*8 ;x
    ld de,12*8 ;y
    dec hl
    
    ld (ix+xobject),l
    ld (ix+xobject+1),h
    ld (ix+yobject),e
    ld (ix+yobject+1),d

    

  ld a,(ix+v7)
  and a
  jp nz,.waitlonger


  ld a,(ix+v4)
  cp 40
  jp nc,.endlevel
  and a
  jp nz,.fadeout

.checkcontrols:

  ld a,(Controls)
  bit 0,a
  ret z
  
  call mariointeract
  ret nc ;no activation yet
  
  ld a,(mariospeed)
  and a
  ret nz
  
.fadeout:  
  
  ld a,5
  ld (nocontrol),a
  
;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  inc (ix+v4)
  
  ld a,(ix+objectrelease)
  cp 2
  jp z,.handlesecret
  
  call Playsfx.door
  
  ret

.handlesecret:

  ld (ix+v7),1

  xor a
  ld (isPlaying),a
  call stopmusic
  
  jp Playsfx.secretdoor


.endlevel:

  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a

;  ld a,(ix+objectrelease)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  ld a,l
  ld (exitx),a

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4

  ld a,l
  ld (exitx4),a ;delen door 32

  ret

.waitlonger:

  inc (ix+v7)

  cp 120
  ret c
  
  jp .endlevel


;  ret
 
 
 
  
animatelaser:

    ld a,(framecounter)
    and 1
    jp z,.animateposition2


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

    ld a,(ix+v3)
  ;  and a
  ;  jp z,.position0
    dec a
    jp z,.position1
   

  ld    hl,coloraddresslaserbeam  ;color address
  ld    bc,characteraddresslaserbeam  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 

.position1:

  ld    hl,coloraddresslaserbeam2  ;color address
  ld    bc,characteraddresslaserbeam2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


    ret
  
.animateposition2:


    ld a,(ix+v3)
  ;  and a
  ;  jp z,.position0
    dec a
    jp z,.position12
   

  ld    hl,coloraddresslaserbeam1  ;color address
  ld    bc,characteraddresslaserbeam1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 

.position12:

  ld    hl,coloraddresslaserbeam3  ;color address
  ld    bc,characteraddresslaserbeam3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 

  
  
  
MovementPattern383:     ;laserbeam  
  
;ix+v3 will determine the direction and mode of the laserbeam sprite  
;0 = left right, 1 = up down
  
  ld a,(ix+v4)
  and a
  call z,.init

  xor a
  ld (bulletsingame),a
  
  
  call addv1toxbject
  call addv2toybject
  
  call objectinteract
  call c,.destroyotherobject
  
  call animatelaser
  
  ret
  
  
.destroyotherobject:

    ld a,(iy+deadly)
    cp 3
    ret z ;bonuses etc cannot be destroyed
    
    ld a,(iy+amountspr)
    and a
    ret z ;spriteless objects are immutable
    
    push ix
    push iy
    pop ix
    
    call destroyobjectwithcloudnostar
    
    pop ix



    ret
  
  
.init:

    ld hl,(marioy)
    ld de,16-4
    add hl,de
    ld (ix+yobject),l
    ld (ix+yobject+1),h

;reset speed data on init
    ld (ix+v1),0
    ld (ix+v2),0

;  call Playsfx.chuckauw 
  call Playsfx.marioflyrise 
    inc (ix+v4)

    ;read controls and check directions
    ld a,(mariolook)
    dec a
    jp z,.setup
    ld a,(marioduck)
    dec a
    jp z,.setdown
    ld a,(marioleftrightspr)
    and a
    jp z,.setleft
    
    
;setright:

    ld (ix+v3),0
    ld (ix+v1),8

    ret

    
.setleft:

    ld (ix+v3),0
    ld (ix+v1),-8
    
    ret
    
    
 
.setup:

    ld (ix+v3),1
    ld (ix+v2),-8
    
    ret
 

.setdown

    ld (ix+v3),1
    ld (ix+v2),8
    
    ret
  
  
  
MovementPattern382:     ;superlaser powerup  



    ld a,(ix+v4)
    dec a
    jp z,.endlevel
    dec a
    jp z,.waitforfadeout
    dec a
    jp z,.exit ;DONE


  call mariointeract
  ret nc
  
  
  ld a,1
  ld (mariolaser),a
  inc a
  ld (mariostate),a

  inc (ix+v4) ;start level end cycle
  
  call Playsfx.secretdoor
 
  call removeobjectspat
  call removefromsprlst
  ld (ix+amountspr),0  
 

 ret


 
 
.exit:


  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a

 
  ;warp mario to the super secret level  
  ld    hl,31*8 -8
  ld    (WMmariox),hl
  ld    hl,5*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationVaFort
  ld    (CurrentLocation),hl
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion 
 
 
 
  ret
  
  
.waitforfadeout:  
 
 
    ld a,(ix+v7)
    inc (ix+v7)
    cp 60
    ret c  
  
    inc (ix+v4)
    
    ret
  
  
.endlevel:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps


  inc (ix+v4)
  ld (ix+v7),0
  
    ret
 
 
  
  
  
  
  
;superspecial boss!  
MovementPattern381:     ;bartfight  
  
  call mariointeract
  
  ld a,(ix+objectrelease)
  and a
  jp nz,.stayhurt
  
  call objectinteract
  cp 2
  jp z,.hurt
  
  ld a,(ix+hitpoints)
  cp 4
  call c,.spawnfireball2
  
  
  ;no init code that has all been handled before
  ld a,(ix+v4)
  and a
  jp z,.wait ;0
  dec a
  jp z,.spawnbats ;1
  dec a
  jp z,.wait ;2
  dec a
  jp z,.spawnbats ;3
  dec a
  jp z,.loop ;4
  ld (ix+v3),0 ;reset loop counter
  dec a
  jp z,.wait ;5  
  dec a
  jp z,.spawnflyingkoopa ;6
  dec a
  jp z,.wait ;7
  dec a
  jp z,.dropstones ;8
  dec a
  jp z,.wait ;9  
  dec a
  jp z,.dropstones ;10
  dec a
  jp z,.wait ;11
  dec a
  jp z,.spawnparagoomba ;11
  dec a
  jp z,.wait ;12  
  dec a
  jp z,.dropstones ;13
  dec a
  jp z,.wait ;14
  dec a
  jp z,.dropstones ;15
  dec a
  jp z,.wait ;16
  dec a
  jp z,.spawnshell ;17
  dec a
  jp z,.wait ;18
  dec a
  jp z,.spawnfireball ;19
  dec a
  jp z,.loop2 ;20
  dec a
  jp z,.wait ;21
  dec a
  jp z,.wait ;22
  dec a
  jp z,.wait ;23
  dec a
  jp z,.wait ;24
  dec a
  jp z,.wait ;25  
  dec a
  jp z,.spawnshell ;26
  dec a
  jp z,.loop3 ;27
  dec a
  jp z,.wait ;28
  dec a
  jp z,.dropstones ;29
  dec a
  jp z,.wait ;30
  dec a
  jp z,.dropstones ;31
  dec a
  jp z,.wait ;32 
  dec a
  jp z,.spawnshell ;33
  

  ld (ix+v4),28
    ld (ix+v7),0  
  
  ret




    
  

.loop3:

    ld (ix+v4),21
    
    ld a,(ix+hitpoints)
    cp 2
    ret nc
    
    
    ld (ix+v4),28
    ld (ix+v7),0
    
    ret    
  
  
  
.spawnfireball2: ;yeah you thought this was going to be easy righ!!??

    ld a,(framecounter)
    and 31
    ret nz


    ld hl,(marioy)
    ld de,15*8
    xor a
    sbc hl,de
    ret nc ;only spawn in range ;)

    call Playsfx.yoshifireball

  ld hl,25 ;X
  ld e,15 ;Y
  ld bc,82 ;type    


    call .spawnbeast
    
    ld (iy+v1),-4
    ld (iy+deadly),2
    
    jp .incv4

    ;ret



  
  
 
.spawnshell:

  ld hl,2 ;X
  ld e,16 ;Y
  ld bc,1 ;type    


    call .spawnbeast    
     
     
    jp .incv4
    
  ;  ret
    

 
 
  
.spawnparagoomba:


  ld hl,19 ;X
  ld e,5 ;Y
  ld bc,197 ;type    


    call .spawnbeast    
     
     
    jp .incv4
    
  ;  ret


  
  
  
.spawnfireball:

    ld hl,(marioy)
    ld de,14*8
    xor a
    sbc hl,de
    ret nc ;only spawn in range ;)

    call Playsfx.yoshifireball

  ld hl,25 ;X
  ld e,14 ;Y
  ld bc,82 ;type    


    call .spawnbeast
    
    ld (iy+v1),-4
    ld (iy+deadly),2
    
    jp .incv4

    ;ret
  
  
  
.loop2:


    ld (ix+v4),5
    
    ld a,(ix+hitpoints)
    cp 4
    ret nc
    
    
    ld (ix+v4),21
    
    ret
  
  

.dropstones:
  
  ld hl,12 ;X
  ld e,5 ;Y
  ld bc,263 ;type    


    call .spawnbeast

  ld (iy+v4),1
  ld (iy+v6),0
  ld (iy+v7),0
  ld (iy+objectfp),0    
    
    
  ld hl,19 ;X
  ld e,5 ;Y
  ld bc,263 ;type    


    call .spawnbeast    

  ld (iy+v4),1
  ld (iy+v6),0
  ld (iy+v7),0
  ld (iy+objectfp),0       
    
    
    jp .incv4
    
  ;  ret
  
  
  
.hurt:

    dec (ix+hitpoints)
 ;   jp .die ;test debug
    jp z,.die
    
    push ix
    push iy
    pop ix
    
    call destroyobjectwithcloud
    
    pop ix
        
    ld a,1
    ld (tripscreen),a
        
    call Playsfx.chuckauw
    
.stayhurt:    
    inc (ix+objectrelease)
    ld a,(ix+objectrelease)
    cp 20
    ret c
    
    ld (ix+objectrelease),0
    
    
    ret
  

.die:

    call Playsfx.bossdies
    
    call destroyobjectwithcloudnostar
    
;TODO: spawn superlaser bonus    
    ld hl,28 ;X
  ld e,16 ;Y
  ld bc,367 ;type    


    call .spawnbeast

  ld a,2
  ld (playwinmusic),a  
    
    ;destroy the smoke
    ld l,(ix+v5)
    ld h,(ix+v5+1)
    push ix
    push hl
    pop ix
    
    call destroyobject
    
    pop ix
    
    
    ret
  
  

.spawnflyingkoopa:  
  
  
    ld hl,19 ;X
  ld e,5 ;Y
  ld bc,93 ;type    


    call .spawnbeast
    
    jp .incv4
    
    ;ret
  
  
  
.loop:

    ld (ix+v4),0


    inc (ix+v3) ;use as loopcounter
    ld a,(ix+v3)
    cp 2
    ret c
    
    ld (ix+v4),5
    
    ret
  
  
  
.wait:

  inc (ix+v7)
    ld a,(ix+v7)
    cp 80
    ret c

    inc (ix+v4)
    ld (ix+v7),0
    
    
    ret

.spawnbats:


    call Playsfx.bat

  ld hl,12 ;X
  ld e,5 ;Y
  ld bc,170 ;type    


    call .spawnbeast

    inc (iy+v4)
    
  ld hl,19 ;X
  ld e,5 ;Y
  ld bc,170 ;type    


    call .spawnbeast    

        inc (iy+v4)
    
  ;  ret
 
.incv4:

    inc (ix+v4)
    ld (ix+v7),0    

    ret
 
  
.spawnbeast:  
  
  push ix

 ; ld hl,13 ;X
 ; ld e,18 ;Y
 ; d ;type

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),c                ;set object number ()
  ld    (iy+6),b                ;set object number ()

  call  activateobject

    push ix
    pop iy ;for the code
    
  pop ix    
  
  ret
  
MovementPattern380:     ;bartdialogue  
  
  inc (ix+v7)
    ld a,(ix+v7)
    cp 80
    ret c
  
    ld a,(ix+objectrelease)
    and a
    jp nz,destroyobject
  
  
    ld (ix+v7),0    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
    
    inc (ix+v4)
    ld a,(ix+v4)
    dec a
    jp z,.tekst2
    dec a
    jp z,.tekst3  
    dec a
    jp z,.tekst4  
    dec a
    jp z,.tekst5  
    dec a
    jp z,destroyobject
  
  
  ret
  
.tekst2:

  ld    hl,coloraddressbartdialogue1  ;color address
  ld    bc,characteraddressbartdialogue1  ;character address
  ld    de,offsetendbartdialogue
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 



.tekst3: 

  ld    hl,coloraddressbartdialogue2  ;color address
  ld    bc,characteraddressbartdialogue2  ;character address
  ld    de,offsetendbartdialogue
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 



.tekst4:  

  ld    hl,coloraddressbartdialogue3  ;color address
  ld    bc,characteraddressbartdialogue3  ;character address
  ld    de,offsetendbartdialogue
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 




.tekst5:  
 

  ld    hl,coloraddressbartdialogue4  ;color address
  ld    bc,characteraddressbartdialogue4  ;character address
  ld    de,offsetendbartdialogue
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 

  
  
  
  
offsetendbartdialogue: dw 000,000, 000,016, 000,032, 000,048, 000,064, 000,080,  000,096, 000,112
;ja jongens dit is het dan echt. Het laatste baasje. Dit gaat nog wat worden op deze manier pfffffff
;effe in tekst denken. Geen rare splits maar wel wat animaties. Dat peukie moet roken. Dat zijn dan al 7 sprites op een lijn
;Platformpje. hoe....???
;We zitten nogal met wat sprite limits
;Tiles??? nee nooit!!
MovementPattern378:     ;bart  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.waitshort
  dec a
  jp z,.trowquestionmark
  dec a
  jp z,.wait
  dec a
  jp z,.dotekst
  dec a
  jp z,.wait
  dec a
  jp z,.wait
  dec a
  jp z,.wait
  dec a
  jp z,.wait
  dec a
  jp z,.wait
  dec a
  jp z,.startfight
  
  
  
  ret

.startfight:


;drop in platform
  push ix

  ld hl,13
  ld e,18

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),186                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  pop ix  

  ld hl,381
   ld (ix+movepat),l
   ld (ix+movepat+1),h 

   xor a
   ld (nocontrol),a
   
    ld (ix+v4),0;27
    ld (ix+v7),0
   
   
    ret
  
  
.waitshort:

  inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c

    inc (ix+v4)
    ld (ix+v7),0
    
    
    ret  
  
  
.wait:

  inc (ix+v7)
    ld a,(ix+v7)
    cp 80
    ret c

    inc (ix+v4)
    ld (ix+v7),0
    
    
    ret
  
  
  
.trowquestionmark:  
;trow questionmark above mario' s head
  ld hl,2
  ld e,12
  
.trowquestionmarksp: ;so that we can call this from somewhere else
  
  push ix  
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,380
   ld (ix+movepat),l
   ld (ix+movepat+1),h  
  
  
  ld (ix+objectrelease),1
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressquestionmark ;color address
  ld    bc,characteraddressquestionmark  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  pop ix  
  
  inc (ix+v4)
  ld (ix+v7),0
  
  ret
  
  
;spawn the questionmarks on mario's head then trow text
.dotekst:

    ld de,10;7
    ld hl,9+3
    
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),31                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,380
   ld (ix+movepat),l
   ld (ix+movepat+1),h

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressbartdialogue  ;color address
  ld    bc,characteraddressbartdialogue  ;character address
  ld    de,offsetendbartdialogue
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
  ld (ix+v4),0
  ld (ix+v7),0
  
    pop ix 

    inc (ix+v4)


    ret
  
  
  
  
;spawn smoke and maybe init timers and init mario  
.init:


    ld de,14
    ld hl,25
    
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),20                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,379
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+deadly),0
   ld (ix+clipping),3
 ;  ld (ix+v3),0

  ld (ix+deadly),0
 ; ld (ix+sizeY),32


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressbartsmoke  ;color address
  ld    bc,characteraddressbartsmoke  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
  ld (ix+v4),0
  
  push ix
  pop hl
  
    pop ix 

    ld (ix+v5),l
    ld (ix+v5+1),h
    
    inc (ix+v4)
    ld (ix+v7),0
    
    ld a,5
    ld (nocontrol),a
    
    ret
  
  
  
  
MovementPattern379:     ;bartsmoke  
  
  
    ld a,(framecounter)
    and 3
    ret nz
 

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
 
 
    inc (ix+v3)
    ld a,(ix+v3)
    and 3
    dec a
    jp z,.position1
    dec a
    jp z,.position2
    dec a
    jp z,.position3
 
 

 

  ld    hl,coloraddressbartsmoke  ;color address
  ld    bc,characteraddressbartsmoke  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
 
 
 
.position1: 

  ld    hl,coloraddressbartsmoke1  ;color address
  ld    bc,characteraddressbartsmoke1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
 



.position2: 

  ld    hl,coloraddressbartsmoke2  ;color address
  ld    bc,characteraddressbartsmoke2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
 


.position3:  
 

  ld    hl,coloraddressbartsmoke3  ;color address
  ld    bc,characteraddressbartsmoke3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
 
 
  
 ; ret
  
  
  
MovementPattern377     ;rightexit (special3a)


  ld a,(ix+v4)
  dec a
  jp z,.wait
  
  
  call mariointeract
  jp c,.handleinteract
  
  ld (ix+v7),0
  
  
  ret
  
  
.handleinteract:  
 
    xor a
    ld (mariorunning),a
    ld (mariospeed),a
 
 ;make a sound initiate exit and offcourse do nothing
    ld a,5
    ld (nocontrol),a
    
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
  call Playsfx.secretdoor
 
    inc (ix+v4)
    ld (ix+v7),0
 
    ret

.wait:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 40
    ret c 
    
  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a    



    ret
 
  
  
  
  
MovementPattern376:     ;halloffame  
  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  
  
  
  ret
  

.init:

    ld hl,scorebaradress
    xor a
    ld b,9    
.copyloop: ;kill scorebar    
    ld (hl),a
    inc hl
    djnz .copyloop

    
    ;kill camera code
    ld a,1
    ld (staticcamy),a

    call screen5

    ld a,halloffameblock
    call block12
    
    ;write the contents of the shizlle in the VRAM

    ld hl,halloffameaddress

    
    di
  
;fill vram with darkness
;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X
  out ($9b),a
  ld a,2 ;page 2
  out ($9b),a ;Y
  
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;X
  ld a,200
  out ($9b),a
  xor a
  out ($9b),a ;Y 
  
  ld a,255 ;fetch first color byte
  out ($9b),a ;first byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!


  ld de,256/2*200
  dec de ;-1   
    
.upload2:
    ld a,255
  out ($99),a
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload2       
    
  ;/fill vram with darkness  
 

;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X
  ld a,80-16
  out ($9b),a
  ld a,2 ;page 2
  out ($9b),a ;Y
  
  xor a
  out ($9b),a
  ld a,1
  out ($9b),a ;X
  ld a,80
  out ($9b),a
  xor a
  out ($9b),a ;Y 
  
  ld a,(hl) ;fetch first color byte
  out ($9b),a ;first byte
  inc hl ;next byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!


  ld de,256/2*80
  dec de ;-1
  ld c,$99    
    
.upload:
  outi
  ld a,44+128
  out ($99),a
  dec de
  ld a,d
  or e
  jp nz,.upload    

 
  ei
  
  
  
    inc (ix+v4)
    
    call screen4 ;return to correct screenmode

    
    ;inject special custom made isr into engine
    
    call Settempisr ;prevent trouble during copy
    
    ;replace inthandler with other type
    ld hl,specialinthandler
    ld de,InterruptHandler
    ld bc,lenghtspecialinterrupthandler
    ldir

    ld		hl,.isr
	ld		de,$38
	ld		bc,3;6
    di	
	ldir ;activate custom inthandler
    ei
 
    ld a,80-16-16 ;set lineintheight
  ;  xor a
    ld (lineintheight),a
   
;load custom pallete
    ld hl,halloffamepaletteaddress
    ld de,editablepalette
    ld bc,32
    ldir
  
;enable the lineints  
  ld    a,(VDP_0)
  or    %0001'0000          ;set ei1 (enable lineints)
  ld    (VDP_0),a           ;ei1 =0 (which is default at boot) only checks vblankint
  out   ($99),a
  ld    a,0+128
  out   ($99),a  
  
    
    ret

.isr:

  jp InterruptHandler 
    
  
  
  
MovementPattern375:     ;special2 exit superspecial  
  
  
  ld a,(ix+v4)
  dec a
  jp z,.wait
  
  
  call mariointeract
  jp c,.handleinteract
  
  ld (ix+v7),0
  
  
  ret
  
  
.handleinteract:  
 
    ;time this shit
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 100
    ret c
 
 
 ;make a sound initiate exit and offcourse do nothing
    ld a,5
    ld (nocontrol),a
    
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
  call Playsfx.secretdoor
 
    inc (ix+v4)
    ld (ix+v7),0
 
    ret

.wait:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 40
    ret c 
    
  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a    



    ret
 
  
  
  
MovementPattern374:     ;special2 exit  
  
  call mariointeract
  jp c,.endlevel
  
  

  ret
  
.endlevel:  
  
  ld a,3
  ld (nocontrol),a
  
  xor a ;VERY IMPORTANT TO DISABLE THIS!
  ld (bluepboxactive),a
  
  ;save the current coordinates of mario
  ld hl,(mariox)
  ld (hlvar),hl
  
  
  call killallenemies
  
  ;reset the framecounter
  xor a
  ld (framecounter),a
  ld (marioshell),a ;take the shell away!
  ld (marioblueblock),a
  ld (mariopbox),a
  
  ld a,1
  ld (playwinmusic),a  
  
  ld a,(lives)
  ld c,4 ;4 extra lives
  add a,c
  ld (lives),a
  
  jp destroyobject
  
    ;ret
  
  
MovementPattern373:     ;final cutscene moving objects  
  
  
  ld a,(ix+v4)
  and a
  ret z
  cp 10
  jp z,.handleprincess
  dec a
  jp z,.handleyoshi ;1
  dec a
  jp z,.yoshiegg ;2
  dec a
  jp z,.yoshieggwait ;3
  dec a
  jp z,.hatchyoshiegg ;4
  
  ;control from main object?? that could be dangerous. well see
  
  
  ret

.spawnthankyou:


    ld de,16
    ld hl,10
    
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),148                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,373
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+deadly),0
   ld (ix+clipping),3
 ;  ld (ix+v3),0

  ld (ix+deadly),0
 ; ld (ix+sizeY),32


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressthankyou  ;color address
  ld    bc,characteraddressthankyou  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
  ld (ix+v4),0
  
    pop ix 

    ret
  
  
.handleprincess:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 80
    ret c    
    cp 80
    call z,.spawnthankyou

    ld (ix+v7),100 ;cheap hack
    
    
    ld a,(framecounter)
    and 3
    ret nz

    ld l,(ix+hitpoints)
    ld h,(ix+objectrelease)
    
    

    push ix
    push hl
    pop ix

    ld a,(ix+v3)
    xor 1
    ld (ix+v3),a
    

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
    
    jp z,.animatepr12
 
  
  ld    hl,coloraddressprincesssitting2  ;color address
  ld    bc,characteraddressprincesssitting2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
 
 
 
 
    pop ix
    ret 
 
    

.animatepr12:


  ld    hl,coloraddressprincesssitting3  ;color address
  ld    bc,characteraddressprincesssitting3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 



    pop ix
    ret


    ret
  
  
  
  
.hatchyoshiegg:


    ld a,(ix+xobject)
    xor 1
    ld (ix+xobject),a

    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c      


;spawn the debris
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

  ld (hlvar),hl
  ld (bcvar),de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),80               ;set object number (80 = yoshi egg debris)
  ld    (iy+6),0               ;set object number (80 = yoshi egg debris)

  call  activateobject

  ld (ix+deathtimer),0
;  ld (ix+clipping),0
;  ld (ix+movepat),71

  ld hl,(hlvar)
  ld de,(bcvar)


  ld    iy,CustomObject
  ld    (iy),available ;use any sprite
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),81               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0

  
  pop ix  
 
;spawn baby yoshi nothing fancy 

 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddressbabyyoshi4  ;color address ;02
  ld    bc,characteraddressbabyyoshi4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  call    setcharcolandoffset


    ld (ix+v4),0
    ld (ix+v3),2
    ld (ix+v2),0
    ld (ix+v1),0 ;reset all important values
  
    ld hl,356 ;change to yoshi baby itself
  
    ld (ix+movepat),l
    ld (ix+movepat+1),h
   

  
  ret
  
  
  
.yoshieggwait:

  call .bounceegg 


    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c  
  
    ld a,(ix+v3)
    and a
    ret nz
    
    inc (ix+v4)
    ld (ix+v7),0
  
  

    ret

  
  
  
.yoshiegg:

   
  call .bounceegg 
   
  call addv1toxbject


    inc (ix+v7)
    ld a,(ix+v7)
    cp 40
    ret c
    
   ; ld (ix+v4),0
    inc (ix+v4)
    ld (ix+v7),0
  

    ret
 
.bounceegg: 
 
  ld e,(ix+v3)
  ld d,0
  ld hl,.eggbouncetable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resetv7
  
  ld (ix+v2),a
  call addv2toybject

  inc (ix+v3)

  ret

.resetv7:

  ld (ix+v3),0
  ret

.eggbouncetable: db -1,0,-2,-2,0,2,2,0,1,10  
  
  
  
.handleyoshi:


  call .animateyoshiwalk
  call addv1toxbject
  
  ld l,(ix+v5) ;head
  ld h,(ix+v5+1)
  push hl
  pop iy
  
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,10
    xor a
    sbc hl,de
    ld (iy+xobject),l
    ld (iy+xobject+1),h
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,16
    xor a
    sbc hl,de    
    ld (iy+yobject),l
    ld (iy+yobject+1),h

  ld l,(ix+hitpoints) ;princess
  ld h,(ix+objectrelease)
  push hl
  pop iy    
    
    
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,4
    xor a
    sbc hl,de
    ld (iy+xobject),l
    ld (iy+xobject+1),h
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,30
    xor a
    sbc hl,de    
    ld (iy+yobject),l
    ld (iy+yobject+1),h    
    
    ld a,(framecounter)
    and 3
    call z,.animatepricess
    
  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 60
    ret c
    
    ld (ix+v4),10 ;animate princess pfffff
    ld (ix+v7),0

    ret

    
    
.animatepricess:

    push ix
    push iy
    pop ix

    ld a,(ix+v3)
    xor 1
    ld (ix+v3),a
    

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
    
    jp z,.animatepr1
 
  
  ld    hl,coloraddressprincesssitting  ;color address
  ld    bc,characteraddressprincesssitting  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
 
 
 
 
    pop ix
    ret 
 
    

.animatepr1:


  ld    hl,coloraddressprincesssitting1  ;color address
  ld    bc,characteraddressprincesssitting1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 



    pop ix
    ret
    
 
.animateyoshiwalk:

    inc (ix+v3)
    ld a,(ix+v3)
    and 3
    dec a
    jp z,.normalleft2
    dec a
    jp z,.normalleft3
    

 
 
.normalleft1:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex183  ;color address
  ld    bc,spriteaddrex183c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.normalleft2:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex184  ;color address
  ld    bc,spriteaddrex184c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.normalleft3:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex185  ;color address
  ld    bc,spriteaddrex185c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset   
 
 
MovementPattern372:     ;final cutscene helper object 
 
 
    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    jp z,.walkleft
    dec a
    jp z,.animatepeace
    dec a
    jp z,.waitforspace
 
 
 
 ret


.animatepeace:

    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 80
    ret c

    
   ;nice try Daemos but mario here is the actual hero so you have to change the sprite in another way. 
    
  ld a,1
  ld (mariopeace),a



    jp .incv4

;    ret
 

 
.waitforspace:


    ;wait for the player to press space then exit.
    
    
    call waitforspacebar





    ret
 

 
 
 
 
 

.incv4:


    ld (ix+v7),0
    inc (ix+v4)
    ret
 

.spawnyoshi:



;spawn the princess


;spawn head and parse data into next object. yes shitty but thats the way yoshi was build. 

    push ix

    
    ld de,22
    ld hl,30
;spawn final cutscene object    
;  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,358
  ld    (iy+5),l                ;set object number ()
  ld    (iy+6),h                ;set object number ()

  call  activateobject
  
  
    ld hl,373
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    push ix
    
  ld b,8
  call increasespritecount
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressprincesssitting  ;color address
  ld    bc,characteraddressprincesssitting   ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset      
    
    
    
    
    
    ld de,22
    ld hl,30
;spawn final cutscene object    
;  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,358
  ld    (iy+5),l                ;set object number ()
  ld    (iy+6),h                ;set object number ()

  call  activateobject
  
  
    ld hl,373
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    push ix
    
  ld b,2
  call increasespritecount
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressyoshieheadnormalleft  ;color address
  ld    bc,characteraddressyoshieheadnormalleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock1
  call    setcharcolandoffset     



    ld de,22
    ld hl,30
;spawn final cutscene object    
;  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,358
  ld    (iy+5),l                ;set object number ()
  ld    (iy+6),h                ;set object number ()

  call  activateobject

    ld hl,373
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    pop hl ;push head data into object
    ld (ix+v5),l
    ld (ix+v5+1),h
    
    pop hl ;push the princess into the object
    ld (ix+hitpoints),l
    ld (ix+objectrelease),h
    
    
    ld (ix+v1),-3
  ld (ix+v4),1    
  
  ld b,2
  call increasespritecount
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressyoshiebodywalk1left  ;color address
  ld    bc,characteraddressyoshiebodywalk1left  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 
  
  
  
    pop ix 
    
    ret
 
 
.spawnegg:


    push ix

    
    ld de,22
    ld hl,30
;spawn final cutscene object    
;  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,358
  ld    (iy+5),l                ;set object number ()
  ld    (iy+6),h                ;set object number ()

  call  activateobject
  
  
    ld hl,373
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    ld (ix+v4),2
    ld (ix+v1),-3    
    
    
  ld b,2
  call increasespritecount
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex552 ;color address
  ld    bc,spriteaddrex552c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  pop ix
  

    ret
 
 
 
.walkleft:


    ld a,%00000100
    ld (Controls),a

    inc (ix+v7)
    ld a,(ix+v7)
    cp 16
    jp z,.spawnyoshi
    cp 32
    jp z,.spawnegg
    cp 80
    ret c
    
    ld a,1
    ld (marioleftright),a
    ld (marioleftrightspr),a

    
    xor a
    ld (Controls),a
    
    jp .incv4
    
  ;  ret
 
 
.init:

    ld hl,ReadControls
;    ld a,(hl)
;    ld (ix+v5),a ;store what was there.
    ld a,$C9 
    ld (hl),a ;cripple controls. let this object handle it
    ld hl,pollpauze
    ld (hl),a ;no menus nothing
    ld hl,readcontrolsadress.disablemariofun
    xor a
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    
    ld a,1
    ld (mariostate),a
    

    ;position mario to different position
    ld hl,30*8
    ld (mariox),hl

    xor a
    ld (marioleftright),a
    ld (marioleftrightspr),a
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c ;wait a small amount of time so that the screen can come up and such
    
    inc (ix+v4)
    ld (ix+v7),0


    ret
 
 
 
 
waitforspacebar:

    ld a,(ix+v7)
    and a
    jp nz,.exitcutscene


  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp z,.exitcutscene
	
	xor a
	and a
	ret



.exitcutscene:

  ld a,1
  ld (fadeoutscreen?),a
;  ld (endcutscene),a
  ld a,8
  ld (fadeoutscreenstep),a

  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  ret c
  
  xor a
  ld (fadeoutscreen?),a
 
  ;ld a,(ix+v5)
  ld a,$C3
  ld hl,ReadControls
  ld (hl),a ;restore controls 
 
 
  ld a,1
  ld (levelend?),a
  
  ret  
 
 
MovementPattern371:     ;princess 
 
 
    ld a,(ix+v4)
    dec a
    jp z,.spawnthankyou
    dec a
    jp z,.setendlevel
    dec a
    jp z,.endlevel
    dec a
    jp z,.waitforfadeout
    dec a
    jp z,.exit ;DONE
 

    call addv2toybject    
    ld (ix+v2),0
    call checkobjecttilesfloor
    ret c
 
 
    ;doe iets
    inc (ix+v4)

 ret

 
 
.setendlevel:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 150
    ret c
    
    ld (ix+v7),0
    inc (ix+v4)
    
    
    ret
 
 
.exit:


  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a
  
  ;DONE WE ARE DONE!!!!! THATS IT THE FINAL BOSS IS FINISHED!!!! JAAAAAAAAAAAAAAAAAAA KKER JAAAAAAAAAAAAAAAAAAAAAAAAAAHAHAHAHHAHAAAAAAAAAAAAAAAA!!!!!
  ;MESSAGE TO THE HATERS: FUCK YOU. YEAH FUCK YOU!!
  
  ret
  
  
.waitforfadeout:  
 
 
    ld a,(ix+v7)
    inc (ix+v7)
    cp 60
    ret c  
  
    inc (ix+v4)
    
    ret
  
  
.endlevel:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps


  inc (ix+v4)
  ld (ix+v7),0
  
    ret
 
 
 
.spawnthankyou:

    ld de,20+1
    ld hl,14+1
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),148                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,371
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+deadly),0
   ld (ix+clipping),3
 ;  ld (ix+v3),0

  ld (ix+deadly),0
 ; ld (ix+sizeY),32


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressthankyou  ;color address
  ld    bc,characteraddressthankyou  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
  ld (ix+v4),2
  
    pop ix 


    inc (ix+v4)

    ret
 
 
 
MovementPattern370:     ;final object (spawns from dying boss)  
  

  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 25
  ret c  
  
  xor a
  ld (marioleftright),a
  ld (marioleftrightspr),a
  
  ld hl,(mariox)
  ld de,16*8
  xor a
  sbc hl,de
  call c,.setmarioright
  
  
  ld a,5
  ld (nocontrol),a
  xor a
  ld (mariospeed),a
  ld a,1
  ld (mariogod),a ;make mario immortal just in case there is some shit going on 
  
  ld a,(ix+v4)
  and a
  jp z,.spawnprincess
  
  
  
  
  jp destroyobject

  
.setmarioright:

    ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a    
  
  
  
.spawnprincess:


    
    ld de,1
    ld hl,14
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),148                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,371
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+deadly),0
 ;  ld (ix+clipping),0
 ;  ld (ix+v3),0

  ld (ix+deadly),0
  ld (ix+sizeY),32


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressprincess  ;color address
  ld    bc,characteraddressprincess  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
  
    pop ix  
  
    inc (ix+v4)  
  
  
    ret
  
  
  
checkleftboundary:  
 
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,16
    xor a
    sbc hl,de
    ret nc
 
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,24*8
    xor a
    sbc hl,de
    ret c 
 
    ld hl,(mariox)
    ld de,8
    xor a
    sbc hl,de
    ret nc 
  
    ;cheap shitty hack to prevent mario from cheating
    ld hl,(marioy)
    ld de,24*8
    xor a
    sbc hl,de
    ret c ;check y

    
  ld a,(mariotransforming)
  cp 10
  ret z
  
  xor a
  ld (mariotransformp),a
  ld (marioflying),a
  ld (marioflyspecial),a  

  
  ld a,10
  ld (mariotransforming),a
  
  ld a,(mariostate)
  and a
  call nz,Playsfx.powerdown

;  xor a ;reset carry
    ret
  
  

MovementPattern367:     ;The final boss!!! (final fight)   
 
 
 call checkleftboundary
 call mariointeract
 
 
  call objectinteract ;when hit by a shell react
  cp 2
  call z,.sethurt
;  cp 4
;  call z,.sethurt
 
 
 
 
 
;direction for sprite is stored in bit 7 of v3  
;subcounter 1 v5
;subcounter 2 objectrelease
;local counter v7
;global stat v4
  
;pissed of level 0  
  ld a,(ix+v4)
  dec a
  jp z,.setrun ;1
  dec a
  jp z,.run ;2
  dec a
  jp z,.stop ;3
  dec a
  jp z,.spit ;4
  dec a
  jp z,.wait ;5
  dec a
  jp z,.trow ;6
  dec a
  jp z,.pauze ;7
  dec a
  jp z,.hurt ;8  
;pissed off level 0
;pissed off level 1
  dec a
  jp z,.setjump ;9
  dec a
  jp z,.jump ;10
  dec a
  jp z,.fall ;11
  dec a
  jp z,.wait2 ;12
  dec a
  jp z,.trow ;13
  dec a
  jp z,.wait3 ;14
;pissed off level 1
;pissed off level 2
  dec a
  jp z,.setslide ;15
  dec a
  jp z,.slideshell ;16
  dec a
  jp z,.wait4 ;17
  dec a
  jp z,.scream ;18
  dec a
  jp z,.scream2 ;19
;pissed off level 2  
;pissed off level 3
  dec a
  jp z,.runtoright ;20
  dec a
  jp z,.setlevitate ;21
  dec a
  jp z,.setlevitate2 ;22
  dec a
  jp z,.levitateup ;23
  dec a
  jp z,.levitate ;24
;pissed off level 3
;final death scene
    dec a
    jp z,.die
  
  

  ret ;invalid state simply returns nothing this should never happen as it will break the game. 


.die:

    ld a,1
    ld (gamecompleted),a ;set level as completed will be saved into the global vars

    ld de,1
    ld hl,1
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),30                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,370
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+deadly),0
 ;  ld (ix+clipping),0
 ;  ld (ix+v3),0

 
  pop ix  

  ld (ix+deadly),0
  
  ld a,2
  ld (playwinmusic),a
  call Playsfx.bossdies
  
  
 ; killallenemies:

  push ix

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 20

  pop ix

    call otherobjectdeathjump   
  
  
  ret

.handleobject:

;cannot be destroyed on level exit
    ld a,(ix+movepat+1)
    and a
    ret nz
    
    

;367 and 370 must live on!    and I select this out with a dirty trick!
    
    
  jp destroyobjectwithcloud
  



   ; ret
  
  
  
.spawnobjects:    
  
  
  
  ld hl,15
  ld de,1
    

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),204                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,369
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),-4
   ld (ix+v2),4
   ld (ix+deadly),2
 ;  ld (ix+clipping),0
 ;  ld (ix+v3),0

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressfireballsheer  ;color address
  ld    bc,characteraddressfireballsheer  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  
 
 
 
  pop ix  
  
  inc (ix+v5) ;count with secondary counter and spawn a boomb when needed
  ld a,(ix+v5)
  cp 10
  ret c
  
  
  ld (ix+v5),0
  
  ld hl,15
  ld de,1
    
    ld a,1
    ld (tripscreen),a
    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),195                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
 
 
  pop ix    
  
  
  
  ret
  
  
.levitate:

    ld a,(framecounter)
    and 3
    call z,.animatelevitation
    
    ld a,(ix+v7)
    cp 15
    push af
    call z,.spawnobjects
    pop af
    cp 30
    call z,.spawnobjects
    
    
    
    call addv2toybject

    ld hl,.levitationtable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    
    cp 5
    jp z,.resetv7
    
    ld (ix+v2),a

    inc (ix+v7)

    ret    


.resetv7:

    ld (ix+v7),0
    ret
    
    
.levitationtable: db 0,1,1,1,2,2,2,3,3,4,3,3,2,2,2,1,1,1,0,-1,-1,-1,-2,-2,-2,-3,-3,-4,-3,-3,-2,-2,-2,-1,-1,-1,0,5    
    
  

.levitateup:

    ld a,(framecounter)
    and 3
    call z,.animatelevitation

    call addv2toybject

    ld hl,.levitateuptable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    
    and a
    jp z,.incv4
    
    ld (ix+v2),a

    inc (ix+v7)

    ret
  

.levitateuptable: db -8,-8,-8,-6,-6,-6,-6,-6,-4,-4,-4,-4,-3,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0  
  
  
  
.animatelevitation:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

    ld a,(ix+v3)
    xor 1
    ld (ix+v3),a
    
    jp z,.levitateanim2
     
     
  
  ld    hl,coloraddressbossfloat  ;color address
  ld    bc,characteraddressbossfloat  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset     
  
  
.levitateanim2:

  ld    hl,coloraddressbossfloat1  ;color address
  ld    bc,characteraddressbossfloat1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


    ;ret
  
  
  
.setlevitate2:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c
    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  
  
  ld    hl,coloraddressbossspitleft1  ;color address
  ld    bc,characteraddressbossspitleft1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset   
  
  ld (ix+v3),0
  
  jp .incv4    
    
    
   ; ret

    
  
  
  
.setlevitate:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c
    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  
  
  ld    hl,coloraddressbossspitleft  ;color address
  ld    bc,characteraddressbossspitleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset   
  
  jp .incv4    
    
    
   ; ret
  


.runtoright: 


    ld (ix+v1),4 ;debug cycle hack 
    ld a,(ix+v3) ;debug cycle hack 
    or %10000000 ;debug cycle hack
    ld (ix+v3),a ;debug cycle hack
    
  ld a,(framecounter)
  and 3
  call nz,animatebossrun
  
  
  call addv1toxbject
  
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,26*8
  xor a
  sbc hl,de
  ret c
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  
  
  ld    hl,coloraddressbossrunleft  ;color address
  ld    bc,characteraddressbossrunleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset   
  
  jp .incv4
  
  ;ret
  
  
  
.scream2:

    
    inc (ix+v7)
    ld a,(ix+v7)
    and a
    jp z,.tripscreen
    cp 10
    jp z,.tripscreen
    cp 20
    jp z,.tripscreen
    cp 30
    jp z,.tripscreen
    cp 50
    ret c
        
    
    ld (ix+v5),0
    ld (ix+v4),15 ;reset cycle
    ld (ix+v7),0
    
   ; jp .incv4

    ret

.tripscreen:

    ld a,1
    ld (tripscreen),a
    
    ret


  
  
.scream:


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
     
     
    call getmarioleftright
    jp c,.screamright

  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.spitl1
    dec a
    jp z,.spitl2
    dec a
    jp z,.screaml3
  
    ret
  

.screaml3:  
  
  
  call .incv4
  
  ld    hl,coloraddressbossspitleft2  ;color address
  ld    bc,characteraddressbossspitleft2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  


  call Playsfx.yoshifireball

  
  ld a,(ix+edible)
  cp 3
  jp nc,.summonboomb


    ;drop fireballs from the sky simply drop them like rain. Spawn a lot of them. Start at the left side of the map
    
  ld hl,1
  ld de,1
    

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),204                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,368
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v2),1
   ld (ix+deadly),2
 ;  ld (ix+clipping),0
 ;  ld (ix+v3),0

  pop ix


  
  ret  
  
  
.summonboomb:


    ld de,1
    ld hl,8
    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),255                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

 
 ;   ld de,1
 ;   ld hl,25 
 
 
 ; ld    iy,CustomObject
 ; ld    (iy),available ;always in the back
 ; ld    (iy+1),l                ;set x custom object
 ; ld    (iy+2),h
 ; ld    (iy+3),e                ;set y custom object
 ; ld    (iy+4),d                ;set y custom object
 ; ld    (iy+5),255                ;set object number ()
 ; ld    (iy+6),0                ;set object number ()

 ; call  activateobject  
  
  

  pop ix


  ld (ix+edible),0
  
  
  
  ret     
    




 ;   ret
  
  
.screamright:



  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.spitr1
    dec a
    jp z,.spitr2
    dec a
    jp z,.screamr3
  
    ret
  

.screamr3:  
 
  call .incv4 
 
  
  ld    hl,coloraddressbossspitright2  ;color address
  ld    bc,characteraddressbossspitright2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  

  

  
  
  call Playsfx.yoshifireball
;spawn fireball
    ;drop fireballs from the sky simply drop them like rain. Spawn a lot of them. Start at the left side of the map
 
 
  ld a,(ix+edible)
  cp 3
  jp nc,.summonboomb 
 
 
  ld hl,1
  ld de,1
    

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),204                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,368
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v2),1
   ld (ix+deadly),2
 ;  ld (ix+clipping),0
 ;  ld (ix+v3),0

  pop ix

;    ld (ix+v7),0
  
  ret  
  
  
 ;   ret

  

  
.wait4:


    ;wait for a short while then set the scream
    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c
    
        ld (ix+v5),0
    
    
    inc (ix+edible) ;count
    
    
        jp .incv4
    

  ;  ret
  
  
  
  
.setslide:


    ld (ix+v5),10
    ld (ix+v3),0
    ld (ix+v7),0
    ld (ix+sizeY),24
    inc (ix+v4)
    
    ld (ix+deathtimer),0
    ld (ix+objectfp),0



    ret
  
  
.setstand:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

      
     
     
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,16
    xor a
    sbc hl,de
    ld (ix+yobject),l
    ld (ix+yobject+1),h
    


    ld (ix+sizeY),40
    ld (ix+v7),0
    inc (ix+v4)
    ld (ix+v5),10

    call getmarioleftright
    jp c,.setstandright

    
  ld    hl,coloraddressbossrunleft  ;color address
  ld    bc,characteraddressbossrunleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
    


  ;  ret


    
.setstandright:



  ld    hl,coloraddressbossrunright  ;color address
  ld    bc,characteraddressbossrunright  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  
    


  ;  ret
    
    
  
.slideshell:

  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  jp nc,.setstand
  
  
  
  
  ld a,(framecounter)
  and 7
  call z,.animatebossshell

  call addv1toxbject
  call addv2toybject

  call acceleratebossshell
  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ret



.animatebossshell:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

    inc (ix+v3)
    ld a,(ix+v3)
    and 3
    dec a
    jp z,.shellposition1
    dec a
    jp z,.shellposition2
    dec a
    jp z,.shellposition3


  ld    hl,coloraddressbossshell ;color address
  ld    bc,characteraddressbossshell  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
;    ret
  
  
.shellposition1: 

  ld    hl,coloraddressbossshell1 ;color address
  ld    bc,characteraddressbossshell1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
;    ret


.shellposition2:  

  ld    hl,coloraddressbossshell2 ;color address
  ld    bc,characteraddressbossshell2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
;    ret

.shellposition3:  

  ld    hl,coloraddressbossshell3 ;color address
  ld    bc,characteraddressbossshell3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
;    ret  
  
  

.wallfound:

  ;change direction
  ld    a,(ix+v5)               ;v1 is movement speed and direction
  cp 11
  ret z
  jp c,.toright

  call getmarioleftright
  ret nc


  ld (ix+v1),-4
  ld (ix+v5),1

  call Playsfx.stomp

  ret

.toright:

  call getmarioleftright
  ret c


  ld (ix+v1),4
  ld (ix+v5),21

  ret






  
  
  
  
.wait2:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c
    
  ;  ld (ix+v4),13
    ld (ix+v5),0
  ;  ld (ix+v7),0

    jp .incv4


   ; ret
  
.wait3:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c
    
    ld (ix+v4),9
    ld (ix+v5),10 ;use as pointer now
    ld (ix+v7),0
    ret
  


.fall:

  ;  call addv1toxbject
    call addv2toybject

    
    ld (ix+v2),0
    
    call checkobjecttilesfloor
    
    ret c

    call Playsfx.explosion
    
    ld a,1
    ld (tripscreen),a
    
   ;spawn deadly debris 

   
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
 ; dec hl
 ; dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e   
   ld a,e
   
   
   push ix
   
  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),a

;we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),2
    ld (ix+v2),1
    ld (ix+deadly),2

  ld hl,(blockhlstorage) ;hl opslaan
  ld a,(blockbcstorage)
  ld b,a

;object 2
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-4
        ld (ix+deadly),2

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld a,(blockbcstorage)
  ld b,a

;object 3

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
    ld (ix+v2),-1
    ld (ix+deadly),2
    
    
  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld a,(blockbcstorage)
  ld b,a

;object 4

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),4
    ld (ix+deadly),2  
    
    pop ix
    
    jp .incv4

   ; ret
  
.setjump:

    call getmarioleftright
    jp c,.setjumpright


    ;so basicly this is how it works. We first take the x coordinates of the object. substract mario's coordinates from them.
    ;bowsers movement speed is at a fixed 4 pixels per frame so we divide. the result by 4 that is the total travel distance towards mario.
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,(mariox)
    xor a
    sbc hl,de
    
    srl   h
    rr    l                       ;/2
    srl   h
    rr    l                       ;/4    
      
    ld (ix+v7),l
    ld (ix+v4),10 ;jump
    ld (ix+v1),-4

    ;set sprite to jump
    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbossjump  ;color address
  ld    bc,characteraddressbossjump  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset     
    
    

   ; ret

    
.setjumpright:


    ;so basicly this is how it works. We first take the x coordinates of the object. substract mario's coordinates from them.
    ;bowsers movement speed is at a fixed 4 pixels per frame so we divide. the result by 4 that is the total travel distance towards mario.
    
    ld e,(ix+xobject)
    ld d,(ix+xobject+1)
    ld hl,(mariox)
    xor a
    sbc hl,de
    
    srl   h
    rr    l                       ;/2
    srl   h
    rr    l                       ;/4 
    
  ;  srl   h
  ;  rr    l                       ;/8
    
    
  ;  ld (ix+v3),l      
    ld (ix+v7),l
    ld (ix+v4),10 ;jump
    ld (ix+v1),4

    ;set sprite to jump
    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressbossjump1  ;color address
  ld    bc,characteraddressbossjump1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset
    
    

.jump:


    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,16
    xor a
    sbc hl,de
    jp c,.setfall
  
  
  call addv1toxbject
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,32
  xor a
  sbc hl,de
  call nc,addv2toybject    ;secure against exiting the screen

    

    ld e,(ix+v7)
    ld d,0
    ld hl,.bossjumptable
    add hl,de
    ld a,(hl)
    and a
    jp z,.setfall
    
    dec (ix+v7)

    ld (ix+v2),a
    

    ret
  
.setfall:
    ;nasty hack to prevent screen exit bug
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    xor a
    ld de,18
    sbc hl,de
    jp nc,.continue
    
    
    ld hl,17
    ld (ix+xobject),l
    ld (ix+xobject+1),h

.continue:
    inc (ix+v4)
    ld (ix+objectfp),0
    ld (ix+deathtimer),0

    ld (ix+v7),0
  
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  
  
  ld    hl,coloraddressbossfall  ;color address
  ld    bc,characteraddressbossfall  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset    
    
    

   ; ret
  
  
;only jump up handle the rest through tilechecks  
.bossjumptable:  db 0,-1,-1,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,-3,-3,-3,-3,-3,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-6,-6,-6,-6,-6,-6
  
  
  
.animatebowserhurt:

    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

    ld a,(ix+v3)
    xor 1
    ld (ix+v3),a
    
    jp z,.animatehurt2
    
    
  ld    hl,coloraddressbosspissed  ;color address
  ld    bc,characteraddressbosspissed  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  ;ret
    

.animatehurt2:

    

  ld    hl,coloraddressbosspissed1  ;color address
  ld    bc,characteraddressbosspissed1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  ;ret
  
  
  
  
.hurt:

    
    ld a,(framecounter)
    and 3
    call .animatebowserhurt

    ld a,(ix+hitpoints)
    and a
    jp z,.setdie ;goodbye bowser    
    
    
    ;fall first if we must
    call addv2toyobject
    ld (ix+v2),0
    call checkobjecttilesfloor
    ret c    
    
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 50
    ret c
    
    
    
    ;then reset status en set to run towards mario for a exact certain time
    ld (ix+v4),2
    ld (ix+v5),0
    ld (ix+objectrelease),0
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    
    srl   h
    rr    l                       ;/2
    srl   h
    rr    l                       ;/4    
    
    dec l
    dec l
    dec l
 ;   dec l
    ld a,52
    sub l
    ld (ix+v7),a ;works
    
    ld (ix+v3),0
    ld (ix+v1),-4

    
 ;   ld a,(ix+v4)
 ;   cp 9 ;check anger state and jump correctly
 ;   call nc,.setstate9
    
    
    ld a,(ix+hitpoints)
 ;   and a
 ;   jp z,.setdie ;goodbye bowser
    cp 3
    jp c,.setstate20    
    cp 5
    jp c,.setstate15    
    cp 7
    jp c,.setstate9


    ret

    
.setdie:

    ld (ix+v4),25
    ld (ix+v7),0
    ld (ix+objectfp),0
    ld (ix+deathtimer),0
    ld (ix+v2),0
    

    ret
    
    
.setstate20:

    ;final state of anger
    
    ld (ix+v3),128
    ld (ix+v1),4
    ld (ix+v4),20
    ld (ix+v7),0
    ld (ix+v5),0
    ld (ix+objectrelease),0
    ld (ix+edible),0
    
    
    ret    
    
    
.setstate15:
    ;next state of anger
    
    ld (ix+v4),15
    ld (ix+v7),0
    ld (ix+v5),0
    ld (ix+objectrelease),0
    
    
    ret




.setstate9: 
    ;next state of anger
    
    ld (ix+v4),9
    ld (ix+v7),0
    ld (ix+v5),0
    ld (ix+objectrelease),0
    
    
    ret

    
    
.sethurt:

    ld a,(ix+v4)
;    cp 10
;    ret z
;    cp 11
;    ret z ;cannot be hurt while being in the air (perhaps fix this so that it actually can be done??)
    cp 15
    ret z ;imutable as shell
    cp 16
    ret z
    
    ld a,(marioshell)
    and a
    ret nz
    
    push ix
    push iy
    pop ix
    
    
    ;call pinkshelldeathjump
    call otherobjectdeathjump
    
    pop ix


   ; call otherobjectdeathjump
   ;hmmmz there is a bug in my favor here. The shell cannot hit bowser when sliding over the ground. Lucky me. no fix
   
   ld (ix+v3),0
   ld (ix+v4),8
   ld (ix+v7),0 ;problem. What to do after the hit.
    call Playsfx.chuckauw

    dec (ix+hitpoints)
    
    ld (ix+objectfp),0
    ld (ix+deathtimer),0
    
    
    ret
    
  
  
.pauze:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c
    
    
    ;check for pissedness level and jump to correct status ;do the same in wait
    
    ;ld aaaaa
    
    ld (ix+v4),1
    ld (ix+v5),0
    ld (ix+v7),0 ;reset all counters
    ld (ix+objectrelease),0 ;reset all counters
    


    ;reset cycle branch off from here

    ret
  
  
  
  
.trow:


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
     
     
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,128
    xor a
    sbc hl,de
    jp c,.trowright

  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.trowl1
    dec a
    jp z,.trowl2
    dec a
    jp z,.trowl3
  
    ret
  

.trowl1: 

  ld    hl,coloraddressbosstrowleft  ;color address
  ld    bc,characteraddressbosstrowleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.trowl2: 

  ld    hl,coloraddressbosstrowleft1  ;color address
  ld    bc,characteraddressbosstrowleft1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset

.trowl3:  
  
  
  call .incv4
  
  ld    hl,coloraddressbossrunleft  ;color address
  ld    bc,characteraddressbossrunleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  


;  call Playsfx.yoshifireball
;spawn shell
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
  dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,7
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),-horshellspeed
   ld (ix+deadly),1
   ld (ix+clipping),2
   ld (ix+v3),0

  pop ix


  
  ret  
  
  
 ;   ret
  
  
.trowright:



  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.trowr1
    dec a
    jp z,.trowr2
    dec a
    jp z,.trowr3
  
    ret
  

.trowr1: 

  ld    hl,coloraddressbosstrowright  ;color address
  ld    bc,characteraddressbosstrowright  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.trowr2: 

  ld    hl,coloraddressbosstrowright1  ;color address
  ld    bc,characteraddressbosstrowright1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset

.trowr3:  
 
  call .incv4 
 
  
  ld    hl,coloraddressbossrunright  ;color address
  ld    bc,characteraddressbossrunright  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  
  
;  call Playsfx.yoshifireball
;spawn shell
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),1                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,7
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),horshellspeed
   ld (ix+deadly),1
   ld (ix+clipping),2
   ld (ix+v3),0

  pop ix

 ;   ld (ix+v7),0  
  
  
  ret  
  
  
 ;   ret  
  
.wait:

   inc (ix+v7)
   ld a,(ix+v7)
   cp 25
   ret c
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,128
    xor a
    sbc hl,de
    call c,animatebossrunright.position0  
  
    jp .jump1
    
    
    call animatebossrun.position0
  
  
.jump1:
    
    
    ld (ix+v4),1
    ld (ix+v5),0
    ld (ix+v7),0

    ret
  
  
  
  
  
.incv4:

    
    inc (ix+v4)
    ld (ix+v7),0
  
  
  ret

.setspitup:
    
    inc (ix+v5)
    ld (ix+v7),0
    ret

  
.spit:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
     
     
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,128
    xor a
    sbc hl,de
    jp c,.spitright

  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.spitl1
    dec a
    jp z,.spitl2
    dec a
    jp z,.spitl3
  
    ret
  

.spitl1: 

  ld    hl,coloraddressbossspitleft  ;color address
  ld    bc,characteraddressbossspitleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.spitl2: 

  ld    hl,coloraddressbossspitleft1  ;color address
  ld    bc,characteraddressbossspitleft1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset

.spitl3:  
  
  
  call .incv4
  
  ld    hl,coloraddressbossspitleft2  ;color address
  ld    bc,characteraddressbossspitleft2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  


  call Playsfx.yoshifireball
;spawn fireball
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),181                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,298
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),-3
   ld (ix+deadly),2
   ld (ix+clipping),0
   ld (ix+v3),0

  pop ix

;    ld (ix+v7),0
  
  ret  
  
  
 ;   ret
  
  
.spitright:



  
    inc (ix+v7)
    ld a,(ix+v7)
    cp 10
    jp nc,.setspitup
  
  
  
    ld a,(ix+v5)
    and a
    jp z,.spitr1
    dec a
    jp z,.spitr2
    dec a
    jp z,.spitr3
  
    ret
  

.spitr1: 

  ld    hl,coloraddressbossspitright  ;color address
  ld    bc,characteraddressbossspitright  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.spitr2: 

  ld    hl,coloraddressbossspitright1  ;color address
  ld    bc,characteraddressbossspitright1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset

.spitr3:  
 
  call .incv4 
 
  
  ld    hl,coloraddressbossspitright2  ;color address
  ld    bc,characteraddressbossspitright2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  
  
  call Playsfx.yoshifireball
;spawn fireball
  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
 ;dec e


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),181                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,298
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),3
   ld (ix+deadly),2
   ld (ix+clipping),0
   ld (ix+v3),1

  pop ix

 ;   ld (ix+v7),0  
  
  
  ret  
  
  
 ;   ret  
 ;   ret


  
  
.stop:

    inc (ix+v5)
    ld a,(ix+v5)
    cp 3
    jp nc,.go4 ;next cycle
    ld (ix+v4),1

    ret

    
.go4:

    inc (ix+objectrelease)
    ld a,(ix+objectrelease)
    cp 3
    jp z,.settrow


    ld (ix+v4),4
    ld (ix+v5),0
    ld (ix+v7),0
    ret
    
  
.settrow:

    ld (ix+v4),6
    ld (ix+v5),0
    ld (ix+v7),0
  
    ret
  
  
.run:  
  
  ld a,(framecounter)
  and 3
  call nz,animatebossrun
  
  call addv1toxbject
  
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 52
  ret c
  
  
  
  jp .incv4
  
  
  
 ; ret
  
  
  
;determine position of mario on map then run towards his position.
.setrun:

    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,128
    xor a
    sbc hl,de
    jp c,.setrunright

   ; call getmarioleftright
   ; jp c,.setrunright

    ld (ix+v3),0
    ld (ix+v1),-4
    
    jp .incv4
    
   ; ret
  
.setrunright:

    ld (ix+v3),128
    ld (ix+v1),4

    jp .incv4

   ; ret
  

  
animatebossrun:


     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

    inc (ix+v3)
    ld a,(ix+v3)
    bit 7,a
    jp nz,animatebossrunright

    and 3 ;discard all non important info
    dec a
    jp z,.position1
    dec a
    jp z,.position2
    dec a
    jp z,.position3
    
    
.position0:    
    
  ld    hl,coloraddressbossrunleft  ;color address
  ld    bc,characteraddressbossrunleft  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position1:

  ld    hl,coloraddressbossrunleft1  ;color address
  ld    bc,characteraddressbossrunleft1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position2:  


  ld    hl,coloraddressbossrunleft2  ;color address
  ld    bc,characteraddressbossrunleft2 ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position3:    
    
  ld    hl,coloraddressbossrunleft3  ;color address
  ld    bc,characteraddressbossrunleft3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   
    
    
    
    
    

animatebossrunright:


    and 3 ;discard all non important info
    dec a
    jp z,.position1
    dec a
    jp z,.position2
    dec a
    jp z,.position3
    
    
.position0:    
    
  ld    hl,coloraddressbossrunright  ;color address
  ld    bc,characteraddressbossrunright  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position1:

  ld    hl,coloraddressbossrunright1  ;color address
  ld    bc,characteraddressbossrunright1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position2:  


  ld    hl,coloraddressbossrunright2  ;color address
  ld    bc,characteraddressbossrunright2 ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   


.position3:    
    
  ld    hl,coloraddressbossrunright3  ;color address
  ld    bc,characteraddressbossrunright3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset   
    


  
;  ret
  
  
MovementPattern366:     ;The final boss!!!  

    ;kill camera code
    ld a,1
    ld (staticcamy),a


    ld a,(gamecompleted)
    and a
    jp nz,.donothing   
 
 
 ;lets finish this monstrous piece of F*ck!!!
  ld a,(ix+v4)
  dec a
  jp z,.trowtext
  dec a
  jp z,.tekstloop ;do it like this or we will not be able to time the tekst output
  dec a
  jp z,.wait
  dec a
  jp z,.startbattle
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  ret c
  
  
.incv4:

    inc (ix+v4)
    ld (ix+v7),0
    
  
  ret

  
.donothing:

    ld a,(cutscenenr)
    cp 10
    ret z ;do NOT do this during the final cutscene


    ld hl,160
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    xor a
    ld (nocontrol),a
  ld (ix+deadly),3
  
  ld (ix+v2),-9
  call addv2toybject
  
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
    
  ld    hl,coloraddressbossdead  ;color address
  ld    bc,characteraddressbossdead  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset  

  ld hl,3
  ld e,22;trow questionmark above mario' s head

  call MovementPattern378.trowquestionmarksp
  
    ret

  
.wait:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  ret c

    jp .incv4
  
  
    ;ret
  

.startbattle:

;give controls to player

    xor a
    ld (nocontrol),a

;remove tekst
    xor a
    ld hl,480+$4000 ;mapadress to trow tiles @
    ld b,128
    ld c,1
    call writemanytile


    ld hl,367
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+v3),0
    ;ld (ix+v4),25 ; for ending 1 for the real start
    ld (ix+v4),1
    ld (ix+v5),0
    ld (ix+v7),0
    ld (ix+objectrelease),0 ;use as sub sub counter

    ret
  
  
.trowtext:

    ;great is that we do not need to exit from this cycle since nothing happens on the screen.
    
    ld hl,480+$4000 ;mapadress to trow tiles @
    
    ld (ix+v5),l
    ld (ix+v5+1),h
    
    ld a,32 ;first tile
    
    ld (ix+v3),a
    
    ld b,128
    
    ld (ix+objectrelease),b
    
    inc (ix+v4)
    
    
.tekstloop:
    
    ld a,(ix+v3)
    ld l,(ix+v5)
    ld h,(ix+v5+1)
    
        call writeanytile
        inc a
        inc hl
        
    ld (ix+v3),a    
    ld (ix+v5),l
    ld (ix+v5+1),h        
        
        
        dec (ix+objectrelease)
        ld a,(ix+objectrelease)
        and a
        ret nz
        
        
        jp .incv4
        
    
    
    
    
    
  


 
 
  

MovementPattern365:     ;ninja  
  
  
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectdeathjump ;nextframe2 mecha does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  
  
    ld a,(ix+v4)
    dec a
    jp z,.setjumpup
    dec a
    jp z,.jumpup
    dec a
    jp z,.falldown
    dec a
    jp z,.reset
    
    ;jumping sequence
 
 
    inc (ix+v7)
    ld a,(ix+v7)
    cp 50
    ret c
    
.incv4:    
    
    inc (ix+v4)
    ld (ix+v7),0
    ld (ix+objectfp),0
    ld (ix+deathtimer),0
  
  ret
  

.setjumpup:

;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressninja1  ;color address
  ld    bc,characteraddressninja1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  call    setcharcolandoffset      

    jp incv4

  ;  ret


.jumpup:


    
    ld hl,.jumptable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    ld a,(hl)
    and a
    jp z,.incv4
    
    ld (ix+v2),a
    call addv2toybject
    
    
    inc (ix+v7)
    
    ret

.falldown:

    call addv2toybject

    ld (ix+v2),0
    
    call checkobjecttilesfloor
    
    ret c

    jp incv4
    

   ; ret

.reset:

    ld (ix+v4),0
    ld (ix+v7),0
;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressninja  ;color address
  ld    bc,characteraddressninja  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset 
  
    ret
  
  
.jumptable: db -4,-4,-4,-4,-3,-3,-3,-3,-2,-2,-2,-2,-1,-1,-1,-1,0  
  
  
  
  
.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,otherobjectdeathjump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud  
 
 jp otherobjectdeathjump
 
 ;ret
 
  
  
  
MovementPattern364:     ;questionmark (special light)  
  
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.active
  
  
  ;see what has to come out
  call objectinteract
  cp 3
  jp z,.releaseobject
  
  
  ret
  

.releaseobject: ;or to be better called activate but I don't care because no one will ever look into the source code anyway

    ;so what do we do here. We transform into a small spriteless object that follows mario around and buggers around with the pallette.

  call removeobjectspat
  call removefromsprlst
  ld (ix+amountspr),0    
    ;make this object spriteless
    inc (ix+v4)

    ret


.init:

;block is allready popped
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 204
  jp z,destroyobject


    call getmariodistance
    ld a,c
    cp 70
    ret nc
    
    
    inc (ix+v4) ;activate map specific crap
    

    ;only init once and never ever again!

    ld a,(switchon)
    dec a
    ret z    
    
    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,7 ;no we need to diffle a little bit around with these numbers
  ld (fadeoutscreenstep),a

    ld a,1
    ld (switchon),a
    
    ld hl,currentpalette
    ld de,editablepalette
    ld bc,32
    ldir
    
    
    ret

    
    
    
.active:


    call followmario
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 50
    ret c
    
    ;write back the palette data
    
  ld    hl,editablepalette
  ld    de,currentpalette
  ld bc,32
  ldir
 ; call  SetPalette
;pulse the fadeout    
  ld a,1
  ld (fadeoutscreen?),a
  ld a,7 ;no we need to diffle a little bit around with these numbers
  ld (fadeoutscreenstep),a    
    
    
    ld (ix+v7),0 ;reset timer
    
    ret
    
  
MovementPattern368: ;firerain

    call mariointeract

    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    dec a
    jp z,.animateflameout
    
    
    call addv1toxbject
    call addv2toybject

    
    ld a,(framecounter)
    and 3
    call z,.animate
    
  ld bc,16
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp nc,.flameout    
    
    
    
    
   ;swing the flames from side to side 
    ld hl,.firetable
    ld e,(ix+v7)
    ld d,0
    add hl,de
    
    ld a,(hl)
    cp 5
    jp z,.resetfiretable
    
    ld (ix+v1),a
    inc (ix+v7)
    
    

    ret

.resetfiretable:

    ld (ix+v7),0
    
    ret

    
    
.animateflameout:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    
    ld a,(ix+v3)
    inc (ix+v3)
    dec a
    jp z,.out1
    dec a
    jp z,.out2
    dec a
    jp z,.out3
    dec a
    jp z,.out4
    dec a
    jp z,.out5
    dec a
    jp z,destroyobject

  ld    hl,coloraddressbossfireballx  ;color address
  ld    bc,characteraddressbossfireballx  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
    
    
.out1:

  ld    hl,coloraddressbossfireballx1  ;color address
  ld    bc,characteraddressbossfireballx1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.out2:

  ld    hl,coloraddressbossfireballx2  ;color address
  ld    bc,characteraddressbossfireballx2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out3:

  ld    hl,coloraddressbossfireballx3  ;color address
  ld    bc,characteraddressbossfireballx3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out4:

  ld    hl,coloraddressbossfireballx4  ;color address
  ld    bc,characteraddressbossfireballx4  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out5:

  ld    hl,coloraddressbossfireballx5  ;color address
  ld    bc,characteraddressbossfireballx5  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
    
    
    

.flameout:

    ld (ix+deadly),0
    ld (ix+v3),0
    inc (ix+v4)
    
    ret
    
    
    
.firetable: db -1,-1,-2,-1,-1,0,1,1,2,1,1,0,5    
 
 
.animate:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    
    
    inc (ix+v3)
    ld a,(ix+v3)
    and 3
    dec a
    jp z,.position1
    dec a
    jp z,.position2
    dec a
    jp z,.position3
 
 
 
 
  ld    hl,coloraddressbossfireball  ;color address
  ld    bc,characteraddressbossfireball  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset     
    

.position1:

 
  ld    hl,coloraddressbossfireball2  ;color address
  ld    bc,characteraddressbossfireball2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  



.position2: 

  ld    hl,coloraddressbossfireball1  ;color address
  ld    bc,characteraddressbossfireball1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset  


.position3:    
    
  ld    hl,coloraddressbossfireball3  ;color address
  ld    bc,characteraddressbossfireball3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset      
    
    
 ;   ret
 
 
 
 
 
  
;spawn a bunch of these things. Do not chain but simply spawn a bunch!!  
.init:

  ld b,5 ;7
  
  ld hl,3 ;1+2
  ld de,2    
  
.spawnloop:


  push ix
  push hl
  push de
  push bc

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h 
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),204                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld hl,368
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v2),1
   ld (ix+deadly),2
    inc (ix+v4) ;no init
        ld (ix+v1),0 ;no initial x speed
   
  pop bc 
  pop de
  pop hl
  pop ix    

 
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl
    inc hl;
    inc hl;
    inc de
 
    djnz .spawnloop
    
    inc (ix+v4)

    ld (ix+v1),0

    ret  

    

  
MovementPattern369:     ;firerain (other variation)  
  

    call mariointeract

    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    dec a
    jp z,.animateflameout
    
    ld a,(framecounter)
    and 3
    call z,.animate
    
    
    call addv1toxbject
    call addv2toybject

    
  ld bc,16
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp nc,.flameout    
    

    ret

    
.animate:

    
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


     ld a,(ix+v3)
     xor 1
     ld (ix+v3),a
     jp z,.position1
     

  ld    hl,coloraddressfireballsheer  ;color address
  ld    bc,characteraddressfireballsheer  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
    

    
.position1:

  ld    hl,coloraddressfireballsheer1  ;color address
  ld    bc,characteraddressfireballsheer1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset     
    
    
.animateflameout:

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    
    ld a,(ix+v3)
    inc (ix+v3)
    dec a
    jp z,.out1
    dec a
    jp z,.out2
    dec a
    jp z,.out3
    dec a
    jp z,.out4
    dec a
    jp z,.out5
    dec a
    jp z,destroyobject

  ld    hl,coloraddressbossfireballx  ;color address
  ld    bc,characteraddressbossfireballx  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
    
    
.out1:

  ld    hl,coloraddressbossfireballx1  ;color address
  ld    bc,characteraddressbossfireballx1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 


.out2:

  ld    hl,coloraddressbossfireballx2  ;color address
  ld    bc,characteraddressbossfireballx2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out3:

  ld    hl,coloraddressbossfireballx3  ;color address
  ld    bc,characteraddressbossfireballx3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out4:

  ld    hl,coloraddressbossfireballx4  ;color address
  ld    bc,characteraddressbossfireballx4  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
.out5:

  ld    hl,coloraddressbossfireballx5  ;color address
  ld    bc,characteraddressbossfireballx5  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  jp    setcharcolandoffset 
  
  
    
    
    

.flameout:

    ld (ix+deadly),0
    ld (ix+v3),0
    inc (ix+v4)
    
    ret
    

 
 
 
  
;spawn a bunch of these things. Do not chain but simply spawn a bunch!!  
.init:

  ld b,5
  
  ld hl,19 ;+4
  ld de,5    
  
.spawnloop:


  push ix
  push hl
  push de
  push bc

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h 
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),204                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call makerandomnumber
  cp 3;6
  jp nc,.skip
  
  call  activateobject

  ld hl,369
   ld (ix+movepat),l
   ld (ix+movepat+1),h
   ld (ix+v1),-4
   ld (ix+v2),4
   ld (ix+deadly),2
    inc (ix+v4) ;no init

     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressfireballsheer  ;color address
  ld    bc,characteraddressfireballsheer  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset     
    
    
.skip:    
    
  pop bc 
  pop de
  pop hl
  pop ix    

  
  ld a,r ;generate "random" number
  bit 0,a
  jp z,.addless
 
    inc hl
    inc hl
    inc hl
    inc hl

    inc de
    inc de
    inc de
    inc de
 
    djnz .spawnloop
    
    inc (ix+v4)

  ;  ld (ix+v1),0

    ret    

    
.addless:

    inc hl
    inc hl
    inc hl
    inc hl

    inc de
   ; inc de
   ; inc de
  ;  inc de
 
    djnz .spawnloop
    
    inc (ix+v4)

  ;  ld (ix+v1),0

    ret        
    
      
    
    
    
MovementPattern363:     ;stonekoopa (special jumping)  
  
 ;yes they actually added this bizar variety of enemy in the very last submap of bowsers castle. I am seriously considering suicide at this point.
 ;use v4 as left right


  call mariointeract ;bot objects deadly to mario


  inc (ix+objectrelease)
  ld a,(ix+objectrelease)
  cp 110
  jp nc,.trowfire
  cp 100
  ret nc
  cp 49
  jp z,.setjump ;yes this is very shitty but I stopped caring. If it works then it works.
  cp 50
  jp nc,.jump
;  cp 100
;  ret c
    ret

    
.trowfire:    

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ;  inc de
    inc de
    dec hl
  
  ;spawn special mario killer assisting object
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object

  ld    (iy+5),1                 ;set object number
  ld    (iy+6),0                ;set object number
  
  
  call  activateobject
  
  pop iy
  push iy
  
  ld hl,317
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  inc (ix+v4)
    ld (ix+v1),-4
    ld (ix+deadly),2
    ld (ix+edible),0
;    ld (ix+sizeY),16

    ;test
    ;ld (ix+v3),2 ;works



;set sprite 
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressstonekoopafireball  ;color address
  ld    bc,characteraddressstonekoopafireball  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset    
    
    
    ld a,(iy+v4)
    dec a
    call z,.setspriteright
    
  
  pop ix  
  
  
  
  ld (ix+objectrelease),0 ;end reset cycle
  
  ret


.setspriteright:


    ld (ix+v1),4
    
    ;set it more to the right
   call addv1toxbject       
   call addv1toxbject       
    
    
    ld (ix+v3),2 ;set spritetype for fireball right
  ld    hl,coloraddressstonekoopafireball2  ;color address
  ld    bc,characteraddressstonekoopafireball2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset 
  
  ;ret
  
  

  
  
  
.setjump:

    ;lift sprite up about 4 pixels to make it look better
    ld l,(ix+yobject)
    ld h,(ix+yobject+1)
    ld de,8
    xor a
    sbc hl,de
    ld (ix+yobject),l
    ld (ix+yobject+1),h
    


    call getmarioleftright
    jp c,.setjumpright

    ld (ix+v1),-2 ;set left
    ld (ix+v4),0 ;set left
    ld (ix+v7),0
    
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressjumpingstonekoopa1  ;color address
  ld    bc,characteraddressjumpingstonekoopa1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

  ;ret

.setjumpright:


    ld (ix+v1),2 ;set right
    ld (ix+v4),1 ;set right
    ld (ix+v7),0
    
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressjumpingstonekoopa3  ;color address
  ld    bc,characteraddressjumpingstonekoopa3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
    
    ;ret
    
    

.jump:
  

  
  
  call addv1toxbject
  call addv2toybject
  
  
  ld e,(ix+v7)
  ld d,0
  ld hl,.jumptable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.fall
  
  ld (ix+v2),a
  
  inc (ix+v7)
  
  ret

  
.fall:


  ld bc,24
  ld de,0
  call objecttilecheckfromrom
  cp 191
  ret c
  
  ld (ix+v1),0
  ld (ix+v2),0
  
  ld (ix+objectrelease),100 ;stop! and trow fireball
  
 ;lock to tile and change back sprite 
  ld a,(ix+yobject)
  and %11111000
  ld (ix+yobject),a
  
  ld a,(ix+v4)
  dec a
  jp z,.setlockright
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressjumpingstonekoopa  ;color address
  ld    bc,characteraddressjumpingstonekoopa  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset  
  
  
  ;ret
  
.setlockright:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)    
  ld    hl,coloraddressjumpingstonekoopa2  ;color address
  ld    bc,characteraddressjumpingstonekoopa2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
    
    ;ret
  
  
  
  
.jumptable: db -8,-8,-8,-6,-6,-4,-4,-4,-4,-4,-3,-3,-3,-2,-2,-1,-1,-1,0,1,1,1,2,2,2,2,3,3,3,3,4,5
  
  
  

  
acceleratebossshell:


  call getmarioleftright
  jp c,.acceleratemoleright


.acceleratemoleleft:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
    ld de,12
    xor a
    sbc hl,de 
    jp c,.setv1good


  ld a,(ix+v5)
  and a
  ret z
  
  dec (ix+v5)
  
  ld a,(ix+v5)
  
  ld hl,bossshellacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a

  ret


.setv1good:

    ld (ix+v1),1
    ld (ix+v5),12


  ret

  
  
.acceleratemoleright:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
    ld de,12
    xor a
    sbc hl,de 
    jp c,.setv1good


  ld a,(ix+v5)
  cp 22
  ret z
  
  inc (ix+v5)
  
  ld a,(ix+v5)
  
  ld hl,bossshellacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a

  ret



bossshellacctable:   db -4,-3,-2,-1,-2,-1,-2,-1,-1,-1,0,0,0,1,1,1,2,1,2,1,2,3,4    
  
  
  
  
  
  
  


kickmecha:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
 ; cp 1
 ; jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 3 frames
  call  z,MovementPattern362.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

  call .getmechatabledata
  
  
  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret  

      
;Depcarated??
;.otherobjectdeathjump:
;
;  ld (ix+v5),1
;
;  ret
  
;reverse direction of mecha  
.wallfound:


  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a
  
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation
  
  ret




.getmechatabledata:

  ld hl,kickmechatablex
  ld d,0
  ld e,(ix+v7)
  add hl,de
  ld a,(hl)
  and a
  jp z,.turnback

  ld c,a

  ld hl,kickmechatabley
  add hl,de ;get the bounce
  ld a,(hl)
  ld (ix+v2),a

  call addv2toybject

  inc (ix+v7)

  ld a,(ix+v6)
  and a
  jp nz,.right

  
  ld a,c
  neg
  ld (ix+v1),a


  ret

.right:

  ld (ix+v1),c


  ret



.turnback:

  ld (ix+v1),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1
  
  ret


kickmechatablex: db 6,6,6,6,5,5,5,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,0
kickmechatabley: db -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,0



holdmecha:

  inc (ix+v7)
  ld a,(ix+v7)
  ;cp 250
  cp 229
  call nc,.turnback


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern362.animate


;omdraaien
  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell

;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right



    ld (ix+v3),1
  
  ld hl,(mariox)
  ld de,18
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeleft

  ret

.right:

  ld (ix+v3),0

  ld hl,(mariox)
  ld de,10
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ret



.turnshell:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
;  ld a,(Controls)
;  bit 5,a
;  jp z,.nextframe
  ld (ix+v3),0  


  ret

.turnshellright:

  
  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,0
  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v3),1    
  
  
  ret



.turnback:

 ;shake the damn thing so we know he about to come back
    ld hl,(mariox)
    ld a,l
    xor 1
   ld l,a
    ld (mariox),hl
 
    ld a,(ix+v7)
    cp 250
    ret c


  ld (ix+deadly),1
  ld hl,361
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v1),-1
  ld (ix+v3),0 ;reset animation

  
  ld (ix+sizeY),24
  
  xor a
  ld (marioshell),a

  ret

.followmario:

  ld a,(ix+v6) ;reverse direction
  xor 1
  ld (ix+v6),a
  
  call followmario
  ret


.nextframeleft:


  ld (ix+v7),0
  ld (ix+v6),0 ;left
  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


  jp .nextframe

.nextframeright:

  ld (ix+v7),0
  ld (ix+v6),1 ;right

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


.nextframe:


  ld a,(marioturnshell)
  and a
  ret nz


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a
  
  ld (ix+v7),0
;  ld (ix+deadly),1
  ;ld (ix+deathtimer),0
  ld (ix+objectfp),0
  
  ld (ix+clipping),2

  call Playsfx.kick

;branch to kickup from here if mario is looking up
  ld a,(mariolook)
  and a
  jp nz,.setshootup

  ld (ix+v4),4

  ret


.setshootup:

  ld (ix+v4),2
  
  call objectinteract
  cp 8
  call z,.shootupharder

  
  call getmarioleftright
  jp c,.setshootupleft
  
  ld a,(mariospeed)
  ld (ix+v1),a ;migrate mariospeed fuck mariospeed is always positive!  
  
  
  ret


.shootupharder:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,32
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ret

.setshootupleft:

  ld a,(mariospeed)
  neg
  ld (ix+v1),a



  ret


shootmechaup:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  cp 8
  jp z,.nextstepbox
  
  ld a,(ix+v7)
  cp 8
  call nc,.mariointeract ;delay the interact otherwise mario will do strange shit
 
;  call checkmariointeractshoot
;  jp c,MovementPattern362.setshootup ;mecha does not die by the spinning cape 
 
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern362.animate

  call addv1toxbject
  call addv2toybject

    
  ld a,(ix+v2)
  ld c,a
  push bc
  neg
  ld (ix+v2),a
  call shelluptilecheck
  pop bc
  ld (ix+v2),c
  jp c,.nextstep


  call .getmechatabledata
  

;todo: check for uptiles
  
;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

;  call  checkobjecttilesfloor
;  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation



  ret


.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.mariointeract:


  call mariointeract
  jp c,MovementPattern362.nextframe

  ret


.getmechatabledata:

  ld a,(ix+v2) ;check for slowdown
  cp -7
  call z,.settablefar


  ld e,(ix+v7)
  ld hl,mechakickuptabley
  ld d,0
  add hl,de
  inc (ix+v7)
  ld a,(hl)
  and a
  jp z,.nextstep
  
  ld (ix+v2),a
  
  
  
  ret


.nextstep:

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v2),0
  ld (ix+v6),0
  ld (ix+v7),0

  ret

.nextstepbox:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v2),0

  ld a,(iy+v7)
  and a
  ret nz

  ld (iy+v7),1

  ret


.settablefar:

  ld (ix+v7),12

  ret

mechakickuptabley: db -10,-10,-10,-10,-10,-10,-10,-10,-9,-9,-8,-7,-6,-4,-2,-1,0



mechacommingdown:

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern362.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern362.setshootup ;mecha does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern362.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

;todo: check for uptiles
  
  call  checksheerobjecttiledown
  jp c,.setbounce
  call checksheerobjecttile ;check for sheer tiles
  jp c,.setbounce
  
  call  checkobjecttilesfloor
  jp nc,.setbounce

  ld a,(ix+standingonspr)
  and a
  jp nz,.setbounce

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ld a,(ix+v2)
  cp 6
  jp z,.maxspeed ;falling is a little bit faster for mecha


  ret
      

.otherobjectdeathjump:

  ld (ix+v5),1

  ret

.maxspeed:

  ld (ix+v2),8

  ret

.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.setbounce:

;use the kick code when it has movement in air

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),0 ;when kicking the mecha flies less
  ld (ix+objectfp),0

;compensate the speed
  ld a,(ix+v1)
  bit 7,a
  call nz,.negate
  and a
  jp z,.bounce
  cp 3
  call z,.set8
  cp 2
  call z,.set15
  cp 1
  call z,.set21


  call getmarioleftright
  jp c,.kickright

  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret

.negate:

  neg

  ret

.set8:

  ld (ix+v7),8

  ret

.set15:

  ld (ix+v7),15

  ret
  
.set21:

  ld (ix+v7),21

  ret


.bounce:

  ld (ix+v4),5
  ld (ix+v6),0 ;use v6 as bouncestep indicator
  ld (ix+v7),0


  ret


;todo
mechabounce:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern362.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern362.setshootup ;mecha does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern362.animate

  call addv2toybject

  ld a,(ix+v6)
  and a
  jp z,.step1
  cp 2
  jp z,.step2
  cp 4
  jp z,.step3
  cp 6
  jp z,.end


  call  checkobjecttilesfloor
  jp nc,.nextstep

  ld a,(ix+standingonspr)
  and a
  jp nz,.nextstep

  ret

.step1:

  ld e,(ix+v7)
  ld d,0
  ld hl,mechabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  ld (ix+v2),a
  ret


.step2:

  ld e,(ix+v7)
  ld d,0
  ld hl,mechabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret

.step3:

  ld e,(ix+v7)
  ld d,0
  ld hl,mechabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret


.nextstep:

  inc (ix+v6)
  ld (ix+v7),0
  ld (ix+v2),0
  ld (ix+objectfp),0
  
  ret


.end:

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1

  ret

mechabouncetable: db -4,-3,-2,-2,-1,-1,-1,0,0


MovementPattern362:     ;mecha (upsidedown)

;  ld a,(ix+v5)
;  and a
;  jp nz,.dodeathjump


  ld a,(ix+v4)
  cp 1
  jp z,holdmecha
  cp 4
  jp z,kickmecha
  cp 2
  jp z,shootmechaup
  cp 3
  jp z,mechacommingdown
  cp 5
  jp z,mechabounce

  inc (ix+v7)
  ld a,(ix+v7)
;  cp 251 ;must always be higher than holdmecha
  cp 220 ;must always be higher than holdmecha
  jp nc,.turnback


  call addv1toxbject
  call addv2toybject
;using v4 as status pointer
;1 = hold
;2 = trown up
;3 = comming down
;4 = kicked away
;5 = bouncing

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.kickdirect
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;mecha does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe



  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

  call  checkobjecttilesfloor
  
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation


  ret


.dodeathjump:
  
  call addv1toxbject
  call addv2toybject
 
    ld (ix+deathtimer),0
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 4
  ret c
  
  jp otherobjectdeathjump

.setshootup:

  ld (ix+v4),2
  ld (ix+v7),0
  ld (ix+clipping),2
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret


.nextframe:


  ld a,(marioflyslide)
  dec a
  jp z,MovementPattern361.setkilljump  
 
;  ld a,(movedownsheertile)
;  dec a
;  call z,setrampjump  
   
;  ld a,(mariospinjump)
;  dec a
;  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

  


  ld a,(Controls)
  bit 5,a
  jp z,.kick


 ld a,(marioyoshi)
 dec a
 ret z


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a

  ld (ix+v4),1 ;hold
  ld (ix+clipping),0 ;make deadly

  ret

.kick:


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

.kickdirect: ;kick around without the use of mario

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),8 ;when kicking the mecha flies less
  ld (ix+objectfp),0  


  call getmarioleftright
  jp c,.kickright


  ld (ix+v3),0
  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v3),1
  ld (ix+v6),0
  ret



.turnback:


 ;shake the damn thing so we know he about to come back
 ld a,(ix+xobject)
 ld h,(ix+xobject+1)
    xor 1
   ld l,a
   ld (ix+xobject),l
   ld (ix+xobject+1),h
 
    ld a,(ix+v7)
    cp 251
    ret c

  ld (ix+deadly),1
  ld (ix+clipping),1
  
  ld hl,361
  ld (ix+movepat),l
  ld (ix+movepat+1),h  
  
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v1),-1
  ld (ix+v3),0 ;reset animation

  
  ;lift target a bit of the ground to prevent clipping and fallthrough
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,10
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  ld (ix+sizeY),24

  ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor 1
  jp    nz,.position1u


.position0u:
  ld    hl,coloraddressmechakoopadown  ;color address
  ld    bc,characteraddressmechakoopadown  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position1u:
  ld    hl,coloraddressmechakoopadown1  ;color address
  ld    bc,characteraddressmechakoopadown1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

;ret



MovementPattern361:     ;mechakoopa

  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;nextframe2 mecha does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
 ; ret c
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.setshootup:

  call Playsfx.kick

  ld (ix+v4),2
  ld (ix+v2),0
  ld (ix+v7),0
  ld (ix+clipping),2
  ld (ix+deadly),0
  ld hl,362
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret

.otherobjectdeathjump:

  push ix
  call otherobjectdeathjump
  pop ix


.animateupsidedown:
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor 1
  jp    nz,.position1u


.position0u:
  ld    hl,coloraddressmechakoopadown  ;color address
  ld    bc,characteraddressmechakoopadown  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position1u:
  ld    hl,coloraddressmechakoopadown1  ;color address
  ld    bc,characteraddressmechakoopadown1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

;ret


.wallfound:


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v1)
  ld e,a
  ld    a,(ix+v3)               ;v3 is used as animation variable
;  xor   1
    inc a
    and 3
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:

    ld a,e
    and a
    jp s,.position0r


  ld    hl,coloraddressmechakooparight1  ;color address
  ld    bc,characteraddressmechakooparight1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
  
  
.position1:

    ld a,e
    and a
    jp s,.position1r


  ld    hl,coloraddressmechakooparight2  ;color address
  ld    bc,characteraddressmechakooparight2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position2:

    ld a,e
    and a
    jp s,.position2r


  ld    hl,coloraddressmechakooparight3  ;color address
  ld    bc,characteraddressmechakooparight3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position3:

    ld a,e
    and a
    jp s,.position3r


  ld    hl,coloraddressmechakooparight4  ;color address
  ld    bc,characteraddressmechakooparight4  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

  
  
.position0r:  
  
  ld    hl,coloraddressmechakoopaleft1  ;color address
  ld    bc,characteraddressmechakoopaleft1  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset    
  
 
.position1r:  
  
  ld    hl,coloraddressmechakoopaleft2  ;color address
  ld    bc,characteraddressmechakoopaleft2  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset   

.position2r:  
  
  ld    hl,coloraddressmechakoopaleft3  ;color address
  ld    bc,characteraddressmechakoopaleft3  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset   
  
.position3r:  
  
  ld    hl,coloraddressmechakoopaleft4  ;color address
  ld    bc,characteraddressmechakoopaleft4  ;character address
  ld    de,offsetmine
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset     
 
  
.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
 ; ld a,(mariospinjump)
 ; dec a
 ; jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a


  ld (ix+deadly),0

  ld hl,362
 ; ld hl,1
  ld (ix+movepat),l
  ld (ix+movepat+1),h  
  
  
  ld a,(ix+v1) ;cheap hack to transfer movement to next route
  and a
  ld (ix+v3),0
  call s,.setright
  
  
  ld (ix+v1),0 ;stop any movement
  ld (ix+v7),0

  call Playsfx.kickenemy

  ld (ix+sizeY),16 ;reduce size to 16
  
  jp .animateupsidedown

 ; ret

.setright:


  ld (ix+v3),1

    ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret
  
  
  
  
  
  
  
  
;end of goomba ripoff code  
  
MovementPattern360:     ;star (static)


  ld a,5
  ld b,a
  push bc
  call mariointeract
  pop bc
  jp c,settransform  
  
  
  ret
  
  
MovementPattern359:     ;introcounter  
  
  
  
    ld a,(ix+v4)
    and a
    jp z,.init ;handle from here better for our RAM usage
    dec a
    dec a
    jp z,.endlevel
    dec a
    jp z,.waitforfadeout
    dec a
    jp z,.exit
  
  
    ld l,(ix+v5)
    ld h,(ix+v5+1)
    dec hl
    ld (ix+v5),l
    ld (ix+v5+1),h
    
    ld a,l
    or h
    ret nz
  
;stop the music player or else the switching on the loader will cause a hang
  xor a
  ld (musicactive),a  
;    call Settempisr  ;stop inthandler so that fmplayer shuts up!
    call stopmusic ;shut fmchip up
    
  
    inc (ix+v4)
  
  ret

.exit:


  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a
  
  
  ret
  
  
.waitforfadeout:  
 
 
    ld a,(ix+v7)
    inc (ix+v7)
    cp 60
    ret c  
  
    inc (ix+v4)
    
    ret
  
  
.endlevel:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps


  inc (ix+v4)
  ld (ix+v7),0
  
    ret
  
  
  
.init:

    ;kill optionsmenu
    ld hl,pollpauze.gooptionsmenu
    ld a,$C9
    ld (hl),a

    ;kill controls
    ld hl,readcontrolsadress
    xor a ;write 3x nop
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    
    ;kill scorebar
    ld hl,scorebaradress
    xor a ;write 3x nop
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a
    inc hl
    ld (hl),a 
    
    ;set lineint to default
    xor a
    ld (lineintheight),a


    ld hl,13*30 ;each frame is 1/30thof a second so we count x seconds x 30 frames
    ld (ix+v5),l
    ld (ix+v5+1),h
    
    inc (ix+v4) ;init done now start counting
    
    ret

  
  
  
  
MovementPattern358:     ;coin counter (endflagbar object)  
  
  
  ld a,(spawn1up)
  and a
  jp nz,.spawn1up
  
  
  ld a,(endcoins)
  and a
  ret z ;done object will be cleared by loader
  
  ld a,(endlevelbywin)
  cp 2
  ret nz
  
  ld a,1
  ld (scorebaractive),a ;activate scorebar
  ld (showcoin),a
  
  ld a,(framecounter) ;slow down
  and 3
  ret z
  
  
  ld a,(endcoins)
  dec a
  ld (endcoins),a
  
  ld a,(gold)
  inc a
  ld (gold),a
  cp 100
  jp nc,.resetandgive1up

    call Playsfx.coin
  
  ret


.resetandgive1up:

  call Playsfx.oneup
  
  ld a,(lives)
  inc a
  ld (lives),a
  xor a
  ld (gold),a
  
  ret  
  
  
.spawn1up: 

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
  dec de
  dec de
  dec de
  dec de

  
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available            ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),15               ;set object number (pinkshell)
  ld    (iy+6),0               ;set object number (pinkshell)

  call  activateobject
  
    ld (ix+v4),20
      inc (ix+v7)

  pop ix  
  
    xor a
    ld (spawn1up),a
  
 
    ret
 
  
MovementPattern357:     ;endflagbar  
  
  ld a,(ix+v7)
  and a
  jp z,.storey
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;  ld a,(currentlevel) ;skip all checks since that is allowed
;  cp 77
;  jp z,.skipchecks
;
  ld hl,(mariox)
  ld de,8 ;16 was 16 but that was just a little bit too tight
  add hl,de
  ex de,hl
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,.endlevely

.skipchecks:

  call mariointeract
  jp c,.endleveltouched


;v3 as timer

  ld a,(ix+deathtimer)
  cp 55
  jp nc,.setmoveupordown


  ld a,(ix+v4)
  and a
  jp nz,.moveup

  call addv2toybject


  ret

  ;check if mario is very far below the object or very far high and react accordingly
.endlevely:

  ld de,(mariox)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,.skipchecks ;passed way beyond checkpoint


  
  ;check for higher above pole
    ld hl,(marioy)
    ld e,(ix+v5)
    ld d,(ix+v5+1) ;retrieve primary ycoordinates
    
  ld a,(currentlevel) ;do not check the higher part of the pole
  cp 77
  jp nz,.skipy0check  
  
    
    xor a
    sbc hl,de
    jp c,.skipchecks ;mario is higher than the pole

.skipy0check:

    
    ex de,hl
    ld de,20*8
    add hl,de
    ld de,(marioy)
    xor a
    sbc hl,de
    jp c,.skipchecks
    

    ;mario is within range so end level


    jp .endlevel
  
  
  
.setmoveupordown:

  ld (ix+deathtimer),0
  ld a,(ix+v4)
  xor %00000001 ;invert
  ld (ix+v4),a
  ret



.moveup:


  call subv2toybject

  call mariointeract
  jp c,.endlevel

  ret

.endleveltouched:

  call Playsfx.checkpoint ;play sound that confirms that you touched the checkpoint bar

;retrieve height and compare to original height base amount of coins on that

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
 
  
  ld e,(ix+v5) 
  ld d,(ix+v5+1)
    
    xor a
    sbc hl,de ;we now have the difference in hl
    
    ;divide by 2
  srl h
  rr l   ;/2
  ld a,55 ;max coins
  sub l

  ld (endcoins),a ;store coins you get @ level end

  
;no return  

.endlevel:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  ld a,l
  ld (exitx),a

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4

  ld a,l
  ld (exitx4),a

  ld a,3
  ld (nocontrol),a
  
  xor a ;VERY IMPORTANT TO DISABLE THIS!
  ld (bluepboxactive),a
  
  ;save the current coordinates of mario
  ld hl,(mariox)
  ld (hlvar),hl
  
  call killallenemies
  
  call .checkobjects
  
  
  ;reset the framecounter
  xor a
  ld (framecounter),a
  ld (marioshell),a ;take the shell away!
  ld (marioblueblock),a
  ld (mariopbox),a
 ; ld (completedfirsttime),a ;always reset this byte upon exit
  
 ; ld a,(completed)
 ; and a
 ; call z,.setfirsttime  
  
  
  ld a,1
  ld (playwinmusic),a

  

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld (ix+movepat),113
  ld (ix+movepat+1),0
  
;spawn a little cloud and pop it

  ;spawn a new shell in place that is forced into a new sprite position
 

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push hl
  push de
  
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force20            ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),46               ;set object number (pinkshell)
  ld    (iy+6),0               ;set object number (pinkshell)

  call  activateobject
  
  call destroyobjectwithcloudnostar

  pop ix

  
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  ld de,15
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
   
  ld b,12
  call forcetubesprite


  pop de
  pop hl

  ;spawn the coins counter
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available            ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,349
  ld    (iy+5),l               ;set object number (pinkshell)
  ld    (iy+6),h               ;set object number (pinkshell)

  call  activateobject
  
  pop ix
  
  
  jp animatelevelexit

  
;  ret

;.setfirsttime:
;
;    ld a,1
; ld (completedfirsttime),a
;
;    ret
 

;check if mario is holding a shell and if so give him a extra life
.checkobjects:  
  
  ld a,(marioshell)
  and a
  ret z
  
  ld a,1
  ld (spawn1up),a
   
 
    ret 



;store the coordinates which will be used for the max height
.storey:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
 
  
  ld (ix+v5),l
  ld (ix+v5+1),h

  ld (ix+v7),1
  ld (ix+deathtimer),0

  ret
  
  
  

babyyoshieatobject:

      ld a,(ix+offsets+20)
        and a
        jp nz,.growyoshi
      

    ld a,(marioshell)
    and a
    call nz,holdbabyyoshi ;in case mario is holding the baby yoshi handle its movement
    
    ;eat object
    ld l,(ix+offsets+15)
    ld h,(ix+offsets+16)
    push hl
    pop iy
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld e,(ix+yobject)
    ld d,(ix+yobject+1)
    
    ld (iy+xobject),l
    ld (iy+xobject+1),h
    ld (iy+yobject),e
    ld (iy+yobject+1),d    
    
    

    ld a,(ix+offsets+13)
    cp 10
    jp nc,.reset
    cp 8
    call z,.eat

    
    call .animate


    
    
    ld a,(ix+hitpoints)
    cp 5
    jp nc,.transform
    
    
    ret

    
.eat:

    ;eat object
    ld l,(ix+offsets+15)
    ld h,(ix+offsets+16)
    
    push ix
    
    push hl
    pop ix
    
    call destroyobject
    
    pop ix
    
    inc (ix+hitpoints)

    
 ;set right yoshi's at right map   
    ld a,(currentlevel)
    cp 156
    jp z,.settype1 ;code this after the star world maps are in
    cp 157
    jp z,.settype3 ;code this after the star world maps are in
    cp 158
    jp z,.settype2 ;code this after the star world maps are in    
    cp 159
    jp z,.settype1 ;code this after the star world maps are in 
    cp 160
    jp z,.settype2 ;code this after the star world maps are in 

    xor a
    ld (yoshitype),a ;transfer object characteristics into yoshi type
    
    
    
    ret
    

.settype1: ;red

    ld a,1
    ld (yoshitype),a ;transfer object characteristics into yoshi type    
    ret

.settype2: ;yellow

    ld a,2
    ld (yoshitype),a ;transfer object characteristics into yoshi type    
    ret
    
.settype3: ;blue

    ld a,3
    ld (yoshitype),a ;transfer object characteristics into yoshi type    
    ret

    
    
.transform:



      ld (ix+offsets+20),1 ;yoshi comes out    
        ld (ix+v1),0
    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex173  ;color address ;02
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  
  
    ld a,(marioshell)
    and a
    ret z
  
   ;transform engine een trap geven
      xor a
        ld (marioshell),a
   
   
  
  ret



.growyoshi:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v1)               ;v3 is used as animation variable
  inc a
  ld    (ix+v1),a               ;v3 is used as animation variable
  cp 1
  jp    z,.position0a
  cp 3
  jp    z,.position1a
  cp 5
  jp    z,.position2a
  cp 7
  jp    z,.position3a
  cp 8
  ret c

  ld (ix+v3),4
  ld (ix+v4),0
  ld (ix+v2),1
  ld (ix+v1),0

  jp .makeyoshi


.position0a:
  ld    hl,spriteaddrex173  ;color address
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1a: 
  ld    hl,spriteaddrex174  ;color address
  ld    bc,spriteaddrex174c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2a:
  ld    hl,spriteaddrex173  ;color address ;02
  ld    bc,spriteaddrex173c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3a:
  ld    hl,spriteaddrex174  ;color address ;02
  ld    bc,spriteaddrex174c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
 

;this is a difficult one. yoshi is built out of two sprites so we have to spawn a extra sprite to the object and change this one into the proper body sprite
;another aproach is to higher the spritecount.
.makeyoshi:


;get the coordinates and spawn the head 
 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  dec e
  inc hl

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),79               ;set object number (79 = yoshi head)
  ld    (iy+6),0               ;set object number (79 = yoshi head)

  call  activateobject

  ld (ix+deathtimer),0
    ld (ix+v2),1
;  ld (ix+clipping),0
;  ld (ix+movepat),71

;adjust the head with 1 pixel to make it fit correctly
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld (yoshiheadix),ix ;store the adress of the yoshihead so we can do modifications on it from anywhere

  pop ix
 

;set the body sprite correctly  
  ld    hl,spriteaddrex175  ;color address ;02
  ld    bc,spriteaddrex175c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset
  

 ; ret

.yoshi:


  ld (yoshiix),ix ;store the ix value of yoshi which is used by the killroutine
  ld (ix+movepat),105 ;make yoshi passive/active
  ld (ix+movepat+1),0
  ld (ix+v4),0 ;reset this we use this as the activation timer
  ld a,1
  ld (yoshileftright),a ;yoshi is always facing the right on spawn
;do the up and down movements the head has its own animation
;this is not an actual animation but a true movement of the entire object which is faster then updating the sprite each 2th frame.

;  call mariointeract
;  jp c,.activate


  ret

    
    
    
.reset:


    ld (ix+offsets+12),0
    ld (ix+offsets+13),0
    ret
    

.animate:
    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(ix+v3)
  and 2
  jp nz,.right
  
  
 
    
.left: 



    ld a,(ix+offsets+13)
    inc (ix+offsets+13)
    cp 5
    jp nc,.position1
    




.position0:

  ld    hl,coloraddressbabyyoshi1  ;color address
  ld    bc,characteraddressbabyyoshi1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset
    

.position1:

  ld    hl,coloraddressbabyyoshi2  ;color address
  ld    bc,characteraddressbabyyoshi2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset    
    
    
    
.right:    
   
   
    ld a,(ix+offsets+13)
    inc (ix+offsets+13)
    cp 5
    jp nc,.position1r   
   
   
.position0r:

  ld    hl,coloraddressbabyyoshi5  ;color address
  ld    bc,characteraddressbabyyoshi5  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position1r:

  ld    hl,coloraddressbabyyoshi6  ;color address
  ld    bc,characteraddressbabyyoshi7  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

;    ret  
  
  
  
  
kickbabyyoshi:


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 3 frames
  call  z,MovementPattern356.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

  call .getbabyyoshitabledata
  
  
  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret  

      
;Depcarated??
;.otherobjectdeathjump:
;
;  ld (ix+v5),1
;
;  ret
  
;reverse direction of babyyoshi  
.wallfound:


  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a
  
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation
  
  ret




.getbabyyoshitabledata:

  ld hl,kickbabyyoshitablex
  ld d,0
  ld e,(ix+v7)
  add hl,de
  ld a,(hl)
  and a
  jp z,.turnback

  ld c,a

  ld hl,kickbabyyoshitabley
  add hl,de ;get the bounce
  ld a,(hl)
  ld (ix+v2),a

  call addv2toybject

  inc (ix+v7)

  
    push bc
    call changeyoshidir
    pop bc
    
  
  ld a,(ix+v6)
  and a
  jp nz,.right

  
  ld a,c
  neg
  ld (ix+v1),a


  ret

.right:

    push bc
    call changeyoshidirright
    pop bc
    

  ld (ix+v1),c


  ret



.turnback:

  ld (ix+v1),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),3
  
  ret


kickbabyyoshitablex: db 4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,0
kickbabyyoshitabley: db -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,0  
 
 
changeyoshidir:  
  
  ld    a,(ix+v3)               ;v3 is used as animation variable
  ld c,%00000010
  or c
  
  ld c,a ;store result
  ld a,(ix+v3) ;compare
  sub c ;change

  ld a,c ;retrieve value
  
    ld    (ix+v3),a               ;v3 is used as animation variable
  
  call nz,MovementPattern356.animate ;it changed so directly apply change

  
  ret

changeyoshidirright:  
  
  ld    a,(ix+v3)               ;v3 is used as animation variable
  ld c,%11111101
  and c
  
  
  ld c,a ;store result
  ld a,(ix+v3) ;compare
  sub c ;change

  ld a,c ;retrieve value
  
    ld    (ix+v3),a               ;v3 is used as animation variable
  
  call nz,MovementPattern356.animate ;it changed so directly apply change
  
  ret
 
 
  
holdbabyyoshi:



  ld    a,(framecounter)
  add   a,(ix+random)
  and   15                       ;this sprite animates once every 31 frames
  call  z,MovementPattern356.animate


;omdraaien
  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell

;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right

    call changeyoshidir
  

  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeleft

  ret


    
  
  
.right:


    call changeyoshidirright

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ret



.turnshell:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
;  jp z,.nextframe
  


  ret

.turnshellright:


  
  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,0
  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ret





.followmario:

  ld a,(ix+v6) ;reverse direction
  xor 1
  ld (ix+v6),a
  
  call followmario
  ret


.nextframeleft:


  ld (ix+v7),0
  ld (ix+v6),0 ;left
  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


  jp .nextframe

.nextframeright:

  ld (ix+v7),0
  ld (ix+v6),1 ;right

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


.nextframe:


  ld a,(marioturnshell)
  and a
  ret nz


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a
  
  ld (ix+v7),0
;  ld (ix+deadly),1
  ;ld (ix+deathtimer),0
  ld (ix+objectfp),0
  
  ld (ix+clipping),2

  call Playsfx.kick

;branch to kickup from here if mario is looking up
  ld a,(mariolook)
  and a
  jp nz,.setshootup

  ld (ix+v4),4

  ret


.setshootup:

  ld (ix+v4),2
  
  call objectinteract
  cp 8
  call z,.shootupharder

  
  call getmarioleftright
  jp c,.setshootupleft
  
  ld a,(mariospeed)
  ld (ix+v1),a ;migrate mariospeed fuck mariospeed is always positive!  
  
  
  ret


.shootupharder:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,32
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ret

.setshootupleft:

  ld a,(mariospeed)
  neg
  ld (ix+v1),a



  ret


shootbabyyoshiup:

  
  ld a,(ix+v7)
  cp 8
  call nc,.mariointeract ;delay the interact otherwise mario will do strange shit
 
;  call checkmariointeractshoot
;  jp c,MovementPattern197.setshootup ;babyyoshi does not die by the spinning cape 
 
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
;  call  z,MovementPattern197.animate

  call addv1toxbject
  call addv2toybject

    
  ld a,(ix+v2)
  ld c,a
  push bc
  neg
  ld (ix+v2),a
  call shelluptilecheck
  pop bc
  ld (ix+v2),c
  jp c,.nextstep


  call .getbabyyoshitabledata
  

;todo: check for uptiles
  
;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

;  call  checkobjecttilesfloor
;  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation



  ret


.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.mariointeract:


  call mariointeract
  jp c,MovementPattern356.nextframe

  ret


.getbabyyoshitabledata:

  ld a,(ix+v2) ;check for slowdown
  cp -7
  call z,.settablefar


  ld e,(ix+v7)
  ld hl,babyyoshikickuptabley
  ld d,0
  add hl,de
  inc (ix+v7)
  ld a,(hl)
  and a
  jp z,.nextstep
  
  ld (ix+v2),a
  
  
  
  ret


.nextstep:

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v2),0
  ld (ix+v6),0
  ld (ix+v7),0

  ret

.nextstepbox:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v2),0

  ld a,(iy+v7)
  and a
  ret nz

  ld (iy+v7),1

  ret


.settablefar:

  ld (ix+v7),12

  ret

babyyoshikickuptabley: db -10,-10,-10,-10,-10,-10,-10,-10,-9,-9,-8,-7,-6,-4,-2,-1,0



babyyoshicommingdown:

  
  call mariointeract
  jp c,MovementPattern356.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern356.setshootup ;babyyoshi does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern356.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

;todo: check for uptiles
  
  call  checksheerobjecttiledown
  jp c,.setbounce
  call checksheerobjecttile ;check for sheer tiles
  jp c,.setbounce
  
  call  checkobjecttilesfloor
  jp nc,.setbounce

  ld a,(ix+standingonspr)
  and a
  jp nz,.setbounce

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ld a,(ix+v2)
  cp 6
  jp z,.maxspeed ;falling is a little bit faster for babyyoshi


  ret
      

.otherobjectdeathjump:

  ld (ix+v5),1

  ret

.maxspeed:

  ld (ix+v2),8

  ret

.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.setbounce:

;use the kick code when it has movement in air

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),0 ;when kicking the babyyoshi flies less
  ld (ix+objectfp),0

;compensate the speed
  ld a,(ix+v1)
  bit 7,a
  call nz,.negate
  and a
  jp z,.bounce
  cp 3
  call z,.set8
  cp 2
  call z,.set15
  cp 1
  call z,.set21


  call getmarioleftright
  jp c,.kickright

  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret

.negate:

  neg

  ret

.set8:

  ld (ix+v7),8

  ret

.set15:

  ld (ix+v7),15

  ret
  
.set21:

  ld (ix+v7),21

  ret


.bounce:

  ld (ix+v4),5
  ld (ix+v6),0 ;use v6 as bouncestep indicator
  ld (ix+v7),0


  ret


;todo
babyyoshibounce:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern356.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern356.setshootup ;babyyoshi does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern356.animate

  call addv2toybject

  ld a,(ix+v6)
  and a
  jp z,.step1
  cp 2
  jp z,.step2
  cp 4
  jp z,.step3
  cp 6
  jp z,.end


  call  checkobjecttilesfloor
  jp nc,.nextstep

  ld a,(ix+standingonspr)
  and a
  jp nz,.nextstep

  ret

.step1:

  ld e,(ix+v7)
  ld d,0
  ld hl,babyyoshibouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  ld (ix+v2),a
  ret


.step2:

  ld e,(ix+v7)
  ld d,0
  ld hl,babyyoshibouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret

.step3:

  ld e,(ix+v7)
  ld d,0
  ld hl,babyyoshibouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret


.nextstep:

  inc (ix+v6)
  ld (ix+v7),0
  ld (ix+v2),0
  ld (ix+objectfp),0
  
  ret


.end:

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1

  ret

babyyoshibouncetable: db -4,-3,-2,-2,-1,-1,-1,0,0






  
MovementPattern356:     ;babyyoshi  
  ;code copy pasted from goomba
 
  ld a,(ix+offsets+12)
  and a
  jp nz,babyyoshieatobject
 
 
  call objectinteractspecial
  jp c,.seteat

.noeat:  
  

  ld a,(ix+v4)
  cp 1
  jp z,holdbabyyoshi
  cp 4
  jp z,kickbabyyoshi
  cp 2
  jp z,shootbabyyoshiup
  cp 3
  jp z,babyyoshicommingdown
  cp 5
  jp z,babyyoshibounce



  call addv1toxbject
  call addv2toybject
;using v4 as status pointer
;1 = hold
;2 = trown up
;3 = comming down
;4 = kicked away
;5 = bouncing

  call objectinteract
  cp 1
  jp z,.kickdirect




  call checkmariointeractshoot
  jp c,.setshootup ;goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe



  ld    a,(framecounter)
  add   a,(ix+random)
  and   15                       ;this sprite animates once every 31 frames
  call  z,.animate


;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

  call  checkobjecttilesfloor
  
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation


  ret

.seteat:


    ld a,(iy+edible)
    and a
    jp z,.noeat ;prevent eating of non edible objects


    ld (ix+offsets+12),1 ;yoshi is gonna eat someone

    push iy
    pop hl
    
    ld (ix+offsets+15),l
    ld (ix+offsets+16),h
    
    ;save object data
    
    ld (iy+movepat),0
    ld (iy+movepat+1),0 ;freeze object
    
    
    ret
  
  

.dodeathjump:
  
  call addv1toxbject
  call addv2toybject
 
    ld (ix+deathtimer),0
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 4
  ret c
  
  jp otherobjectdeathjump

.setshootup:

  ld (ix+v4),2
  ld (ix+v7),0
  ld (ix+clipping),2
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret


.nextframe:



  ld a,(Controls)
  bit 5,a
  jp z,.kick


 ld a,(marioyoshi)
 dec a
 ret z


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a

  ld (ix+v4),1 ;hold
  ld (ix+clipping),0 ;make deadly

  ret

.kick:


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

.kickdirect: ;kick around without the use of mario

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),8 ;when kicking the goomba flies less
  ld (ix+objectfp),0  


  call getmarioleftright
  jp c,.kickright


  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret



.turnback:

  ld (ix+deadly),1
  ld (ix+clipping),1
;  ld (ix+movepat),196 ;this should be a loose yoshi baby
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v1),-1
  ld (ix+v3),0 ;reset animation


  ret


.animate:

    
 ; ld a,(ix+offsets+12)
 ; and a
 ; ret nz
    
    
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0u
  dec   a
  jp    z,.position1u
  dec   a
  jp    z,.position2u
  dec   a
  jp    z,.position3u

.position0u:
  ld    hl,coloraddressbabyyoshi  ;color address
  ld    bc,characteraddressbabyyoshi  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position1u:
  ld    hl,coloraddressbabyyoshi3  ;color address
  ld    bc,characteraddressbabyyoshi3  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position2u:
  ld    hl,coloraddressbabyyoshi4  ;color address ;02
  ld    bc,characteraddressbabyyoshi4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

.position3u:
  ld    hl,coloraddressbabyyoshi7  ;color address ;03
  ld    bc,characteraddressbabyyoshi7  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock7
  jp    setcharcolandoffset

;ret

  

 
  
