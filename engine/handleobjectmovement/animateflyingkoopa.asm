;merge with animatekoopa when done!!


animateflyingkoopa:

  
  ld a,(ix+objectrelease)
  cp 0
  jp z,animateredflyingkoopa
  cp 3
  jp z,animategreenflyingkoopa




;ret



animategreenflyingkoopa:

  ld a,(currentlevel)
  cp 4
  jp z,animategreenflyingkoopalevel04
  


;  ret


animategreenflyingkoopalevel04:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex228  ;color address
  ld    bc,spriteaddrex228c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex229  ;color address
  ld    bc,spriteaddrex229c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex231  ;color address ;02
  ld    bc,spriteaddrex231c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex232  ;color address ;03
  ld    bc,spriteaddrex232c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
  
  ;ret






animateredflyingkoopa:



  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex222  ;color address
  ld    bc,spriteaddrex222c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex223  ;color address
  ld    bc,spriteaddrex223c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex225  ;color address ;02
  ld    bc,spriteaddrex225c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex226  ;color address ;03
  ld    bc,spriteaddrex226c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset




 ; ret