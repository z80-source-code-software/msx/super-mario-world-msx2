cutscene9:

    ld a,cutsceneblock
    call block12

    ;get data
    ld l,(ix+v5)
    ld h,(ix+v5+1)
    ld e,(ix+v1)
    ld d,(ix+v2)    
    
    
;write the tekst at the designated area
    ld a,(ix+objectrelease)
    dec a
    jp z,.writecredits
    dec a
    jp z,.writecredits
    ld (ix+objectrelease),0
    
    call setpage12tohandleobjectmovement


;write per screen a character per 2 frames. That way we get. 1 sprite + text
    ld a,(ix+v4)
    and a
    jp z,.init
    dec a
    jp z,.char1
    dec a
    jp z,.stopcharacter

    dec a
    jp z,.char2
    dec a
    jp z,.stopcharacter    
    
    dec a
    jp z,.char3
    dec a
    jp z,.stopcharacter
    
    dec a
    jp z,.char4
    dec a
    jp z,.stopcharacter
    
    dec a
    jp z,.char5
    dec a
    jp z,.stopcharacter

    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen
    
    dec a
    jp z,.char6
    dec a
    jp z,.stopcharacter   
    
    dec a
    jp z,.char7
    dec a
    jp z,.stopcharacter      

    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen    
    
    dec a
    jp z,.char8
    dec a
    jp z,.stopcharacter
    
    dec a
    jp z,.char9
    dec a
    jp z,.stopcharacter
 
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen  
 
    dec a
    jp z,.char10
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char11
    dec a
    jp z,.stopcharacter 
 
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen     
    
    dec a
    jp z,.char12
    dec a
    jp z,.incv4 
    dec a
    jp z,.incv4     
    dec a
    jp z,.stopcharacter ;keep longer active
    
    dec a
    jp z,.char13
    dec a
    jp z,.stopcharacter
    
    dec a
    jp z,.char14
    dec a
    jp z,.stopcharacter 
    
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen     
    
    dec a
    jp z,.char15
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char16
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char17
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char18
    dec a
    jp z,.stopcharacter 
    
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen            
    
    dec a
    jp z,.char19
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char20
 ;   dec a
 ;   jp z,.stopcharacter 
    
    dec a
    jp z,.char21
    dec a
    jp z,.stopcharacter 
    
    dec a
    jp z,.char22
    dec a
    jp z,.incv4
    dec a
    jp z,.incv4  
    dec a
    jp z,.incv4
    dec a
    jp z,.incv4      
    dec a
    jp z,.stopcharacter     
    
    dec a
    jp z,.char23
    dec a
    jp z,.stopcharacter              
    
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen            
    
    dec a
    jp z,.char24
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char25
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char26
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char27
   ; dec a
   ; jp z,.stopcharacter              
    
    dec a
    jp z,.char28
    dec a
    jp z,.stopcharacter              
    
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen       
    
    dec a
    jp z,.char29
  ;  dec a
  ;  jp z,.stopcharacter              
    
    dec a
    jp z,.char30
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char31
    dec a
    jp z,.stopcharacter              
    
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen        
    
    dec a
    jp z,.char32   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char33   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char34   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char35   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char36   
    dec a
    jp z,.stopcharacter              
               
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen                 
    
    dec a
    jp z,.char37   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char38   
    dec a
    jp z,.stopcharacter              

    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen     
    
    dec a
    jp z,.char39   
    dec a
    jp z,.stopcharacter              
    
    dec a
    jp z,.char40   
    dec a
    jp z,.stopcharacter              
                 
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen 
    
    dec a
    jp z,.char41   
    dec a
    jp z,.stopcharacter              
                 
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen 
    
    dec a
    jp z,.char42   
    dec a
    jp z,.stopcharacter              
           
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen    
    
    dec a
    jp z,.char43   
    dec a
    jp z,.stopcharacter              
            
    ;next screen 
    dec a
    jp z,waitforspace8
    ;next screen                  

    push af
    ld a,cutsceneblock
    call block12 
    pop af
 
    dec a
    jp z,.writepalette

    
    dec a
    jp z,.writeendbkg
    dec a
    jp z,.writeendbkg    
    
    
    dec a
    jp z,.fixbrokencode
    dec a
    jp z,waitfor1
    
    ret
 
.fixbrokencode:

     ;kill the spriteforce code
    ld hl,activateobject.forcesprite
    ld a,%00101000
    ld (hl),a   

    inc (ix+v4)
    
    ret
 
.writepalette:

  ld hl,endbkgpalettadress
  ld de,currentpalette
  ld bc,32
  ldir
  ld hl,currentpalette
  xor a
  ld (hl),a ;make color 0 black
  call SetPalette    

    inc (ix+v4)

    ret

 
.writeendbkg:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  xor a
  out ($9b),a ;Xd
  out ($9b),a ;Xd
  
  ld a,50
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  xor a
  out ($9b),a ;Xw
  ld a,1
  out ($9b),a ;Xw

;PROBLEM: writing back the entire background takes too much time. Thus we need to step it down a little
  ld a,125
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
   
  inc (ix+v4) 
   
  ret 
 
 
    
.char43:


    ld hl,endtext.bowser
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,6 ;per tile Y
    ld hl,15; per tile X

    ld bc,357
    
    call .placechar
    
    inc (ix+v4)
    
   
    ret  
   
.char42:


    ld hl,endtext.mechakoopa
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,15; per tile X

    ld bc,352
    
    call .placechar
    
    inc (ix+v4)
    
   
    ret  
    
.char41:


    ld hl,endtext.reznor
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,15; per tile X

    ld bc,266
    
    call .placechar
    
    inc (ix+v4)
    
   
    ret  
  

.char40:


    ld hl,endtext.fishbone
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,15; per tile X

    ld bc,262
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
    
   
.char39:


    ld hl,endtext.ballchain
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,10; per tile X

    ld bc,220
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
    
    
.char38:


    ld hl,endtext.grinder
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,15; per tile X

    ld bc,308
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
  
.char37:


    ld hl,endtext.thwomp
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,10 ;per tile Y
    ld hl,5; per tile X

    ld bc,221
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
   
.char36:


    ld hl,endtext.thwimp
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,14 ;per tile Y
    ld hl,20; per tile X

    ld bc,218
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
   
.char35:


    ld hl,endtext.hothead
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,18 ;per tile Y
    ld hl,5; per tile X

    ld bc,335
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
   
.char34:


    ld hl,endtext.bonybeetle
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,20; per tile X

    ld bc,264
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
   
.char33:


    ld hl,endtext.lilsparky
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,6 ;per tile Y
    ld hl,15; per tile X

    ld bc,334
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
     
.char32:


    ld hl,endtext.drybones
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,222
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
    
.char31:


    ld hl,endtext.eerie
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,25; per tile X

    ld bc,324
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char30:


    ld hl,endtext.fishinboo
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,15; per tile X

    ld bc,323
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char29:


    ld hl,endtext.boobuddy
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,179
    
    call .placechar
    
  ;  inc (ix+v4)
   call .stopcharacter
   
    ret  
  
.char28:


    ld hl,endtext.torpedoted
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,20 ;per tile Y
    ld hl,20; per tile X

    ld bc,361
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char27:


    ld hl,endtext.ripvanfish
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,5; per tile X

    ld bc,199
    
    call .placechar
    
   ; inc (ix+v4)
    call .stopcharacter
    
    ret  
  
.char26:


    ld hl,endtext.urchin
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,2 ;per tile Y
    ld hl,23; per tile X

    ld bc,297
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char25:


    ld hl,endtext.porcupuffer
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,15; per tile X

    ld bc,257
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char24:


    ld hl,endtext.blurp
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,203
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
  
.char23:


    ld hl,endtext.blargg
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,17 ;per tile Y
    ld hl,20; per tile X

    ld bc,360
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
     
.char22:

    ld hl,endtext.buzzybeetle
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,14 ;per tile Y
    ld hl,7; per tile X

    ld bc,169
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
          
.char21:

    ld hl,endtext.swooper
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,20; per tile X

    ld bc,170
    
    call .placechar
    
    inc (ix+v4)
    
    ret  
            
.char20:

    ld hl,endtext.spiketop
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,10; per tile X

    ld bc,174
    
    call .placechar
    
 ;   inc (ix+v4)
    
    call .stopcharacter
    
    ret  
 
.char19:

    ld hl,endtext.koopa
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,063
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
   
.char18:

    ld hl,endtext.dinotorch
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,19 ;per tile Y
    ld hl,20; per tile X

    ld bc,313
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
       
.char17:

    ld hl,endtext.dinorhino
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,7; per tile X

    ld bc,312
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
    
     
.char16:

    ld hl,endtext.megamole
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,18; per tile X

    ld bc,318
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
    
   
.char15:

    ld hl,endtext.rex
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,002
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
    
 
.char14:

    ld hl,endtext.bill
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,20 ;per tile Y
    ld hl,15; per tile X

    ld bc,247
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
 
.char13:

    ld hl,endtext.monty
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,10 ;per tile Y
    ld hl,20; per tile X

    ld bc,359
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
   
.char12:

    ld hl,endtext.pokey
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,10 ;per tile Y
    ld hl,10; per tile X

    ld bc,117
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
    
.char11:

    ld hl,endtext.sumo
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,20; per tile X

    ld bc,288
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
      
.char10:

    ld hl,endtext.chuck
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,7; per tile X

    ld bc,025
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
    
.char9:

    ld hl,endtext.volcano
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,20 ;per tile Y
    ld hl,10; per tile X

    ld bc,158
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret  
 
 
 
.char8:

    ld hl,endtext.jumpingpirahnja
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,008
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret        
    
.char7:

    ld hl,endtext.superkoopa
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,10 ;per tile Y
    ld hl,20; per tile X

    ld bc,157
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret    
    
    
.char6:

    ld hl,endtext.brother
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile Y
    ld hl,5; per tile X

    ld bc,192
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret     
    

.char5:

    ld hl,endtext.wiggler
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,17 ;per tile Y
    ld hl,20; per tile X

    ld bc,295
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret    
    
    
.char4:

    ld hl,endtext.spiny
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,15 ;per tile Y
    ld hl,5; per tile X

    ld bc,319
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret    
    
    
.char2:

    ld hl,endtext.lakitu
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,10 ;per tile Y
    ld hl,25; per tile X

    ld bc,304
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret      
    
    
.char1:

    ld hl,endtext.paragoomba
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,7 ;per tile Y
    ld hl,11; per tile X

    ld bc,197
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret    
    
    

.char3:

    ld hl,endtext.parabomb
    ld (ix+v5),l
    ld (ix+v5+1),h
    ;make this generic. So that we can read out character data somehow
    ld de,5 ;per tile
    ld hl,2

    ld bc,255
    
    call .placechar
    
    inc (ix+v4)
    
    
    ret

    
.incv4:

    inc (ix+v4)
    ld (ix+v7),0
    ret
    

.clearscr:

  call clearvrampage0and1cs ;clear the entire visible part of the VRAM


    ret
    
    
.placechar:    
    
    push de
    push hl
    
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),c                ;set object number ()
  ld    (iy+6),b                ;set object number ()

  call  activateobject
 
;TODO: There has to be a way to call the objects init code so that all sprites and objects are being spawn 
 ; ld (ix+movepat),0
 ; ld (ix+movepat+1),0
  ;deadly and clipping could be used for this purpose call back after 1 frame
  push ix
  pop hl

    ld (ix+v7),6 ;cheap hack
    ld (ix+v3),1
  
    ld a,(ix+sizeY)  
  
  pop ix

  ld (ix+deadly),l
  ld (ix+clipping),h  
  
    ld (ix+objectrelease),1 ;command engine to write text
    
    pop hl
    pop de
    
    sla l
    sla l
    sla l
    
    sla e
    sla e
    sla e ;*8
    
    add a,e
    ld e,18
    add a,e
    ld e,a
    
    ld (ix+v1),l
    ld (ix+v2),e
 
  
    ret
    
    
    
    
.stopcharacter:
 
 ;fetch ix from foreign object and stop it from doing any action
 
    ld l,(ix+deadly)
    ld h,(ix+clipping)
    push hl
    pop iy
    ld (iy+movepat),0
    ld (iy+movepat+1),0
 
    inc (ix+v4)
 
    ret
 
 
.writecredits:

    
  call writetexttoactivevram


    inc (ix+objectrelease)

    ret    
    

    
.init:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 1
    jp z,.killsprite
    cp 100
    ret c ;wait a whil so the music can start simultaniously
    
    ld (ix+v7),0

    ;kill the spriteforce code
    ld hl,activateobject.forcesprite
 ;   ld a,(hl)
;    ld (ix+v3),a ;store information for later REMEMBER TO PUT IT BACK!!!!!
    ld a,%00011000
    ld (hl),a




  inc (ix+v4)
    ld (ix+clipping),20 ;imutable
  

    ld (ix+objectrelease),1 ;write tekst
    ld hl,endtext.cast
    ld de,$1040  
    ld (ix+v5),l
    ld (ix+v5+1),h
    ld (ix+v1),e
    ld (ix+v2),d


    ret

.killsprite:    
    ;make mario spriteless
  call removeobjectspat
  call removefromsprlst
  ld (ix+amountspr),0      
  
  ret
    

endtext:
.cast: db "cast of characters",0
.parabomb: db "parabomb",0    
.paragoomba: db "paragoomba",0    
.lakitu: db "lakitu",0    
.spiny: db "spiny",0    
.wiggler: db "wiggler",0    
.brother: db "amazing brother",0    
.superkoopa: db "superkoopa",0    
.jumpingpirahnja: db "jumping pirahnja",0    
.volcano: db "volcano lotus",0    
.chuck: db "chuck",0    
.sumo: db "sumo brother",0    
.pokey: db "pokey",0    
.monty: db "monty mole",0    
.bill: db "bill",0    
.rex: db "rex",0    
.megamole: db "megamole",0        
.dinorhino: db "dinorhino",0    
.dinotorch: db "dinotorch",0    
.koopa: db "koopa",0    
.spiketop: db "spiketop",0    
.swooper: db "swooper",0    
.buzzybeetle: db "buzzy beetle",0    
.blargg: db "blargg",0    
.blurp: db "blurp",0    
.porcupuffer: db "porcu puffer",0    
.urchin: db "urchin",0    
.ripvanfish: db "rip van fish",0    
.torpedoted: db "torpedo ted",0    
.boobuddy: db "boobuddy",0    
.fishinboo: db "fishinboo",0    
.eerie: db "eerie",0    
.bigboo: db "the big boo",0    
.drybones: db "dry bones",0    
.lilsparky: db "little sparky",0    
.bonybeetle: db "bony beetle",0    
.hothead: db "hothead",0    
.thwomp: db "thwomp",0    
.thwimp: db "thwimp",0    
.ballchain: db "ball n, chain",0    
.grinder: db "grinder",0    
.fishbone: db "fishbone",0    
.reznor: db "reznor",0    
.mechakoopa: db "mechakoopa",0    
.bowser: db "bowser",0    








waitforspace8:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp z,.waitforrelease
	
	xor a
	and a
	ret

;keypress release
.waitforrelease:

  ld b,8

  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	bit 0,a
	jp nz,.exitcutscene
	
	jp .waitforrelease


.exitcutscene:

;kill all objects
    
  push ix

  ld    ix,Objectlist+(lenghtobjectlist*00) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 1
  ld    ix,Objectlist+(lenghtobjectlist*01) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 2
  ld    ix,Objectlist+(lenghtobjectlist*02) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 3
  ld    ix,Objectlist+(lenghtobjectlist*03) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 4
  ld    ix,Objectlist+(lenghtobjectlist*04) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 5
  ld    ix,Objectlist+(lenghtobjectlist*05) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 6
  ld    ix,Objectlist+(lenghtobjectlist*06) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 7
  ld    ix,Objectlist+(lenghtobjectlist*07) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 8
  ld    ix,Objectlist+(lenghtobjectlist*08) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 9
  ld    ix,Objectlist+(lenghtobjectlist*09) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 10
  ld    ix,Objectlist+(lenghtobjectlist*10) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 11
  ld    ix,Objectlist+(lenghtobjectlist*11) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 12
  ld    ix,Objectlist+(lenghtobjectlist*12) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 13
  ld    ix,Objectlist+(lenghtobjectlist*13) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 14
  ld    ix,Objectlist+(lenghtobjectlist*14) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 15
  ld    ix,Objectlist+(lenghtobjectlist*15) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 16
  ld    ix,Objectlist+(lenghtobjectlist*16) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 17
  ld    ix,Objectlist+(lenghtobjectlist*17) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 18
  ld    ix,Objectlist+(lenghtobjectlist*18) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 19
  ld    ix,Objectlist+(lenghtobjectlist*19) : ld a,(ix) : or a : call nz,.handleobject ;objectnr 20

  pop ix

    
    ld a,cutsceneblock
    call block12

  call clearvisiblevram12 ;clear the entire visible part of the VRAM 
  
  
 ; halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
 ; halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt  
  
  call setpage12tohandleobjectmovement
  
  inc (ix+v4)

  ret 

.handleobject:

    ld a,(ix+amountspr)
    and a
    ret z

    jp destroyobject


;final cutscene after killing bowser
cutscene8:

;so when we write the tekst to the vram we need to write al the tekst and do this for 2 frames. and for each tekst so we need some pointer and repeat. pfffff
    ;pull variables back from ram
    ld e,(ix+v1)
    ld d,(ix+v2)
    ld l,(ix+v5)
    ld h,(ix+v5+1)

    ld a,cutsceneblock
    call block12    
    

;one screen can hold about 22 lines of text

    ld a,(ix+v4)
    cp 10
    jp z,waitforspace
    cp 11
    jp z,.waitforfadeout
    cp 12
    jp z,waitfor1
    cp 13
    ret z ;do nothing
    and a
    jp z,.init
    
    dec a
    jp z,.writecredits    
    dec a ;2 frames
    jp z,.writecredits


    ;get next line of text and fetch the linewidth
    ;react on the carry bit and return to next screen state
    call getnextline
    ld (ix+v5),l
    ld (ix+v5+1),h     
    jp c,.stop
   
    
    ld a,d
    add a,8
    ld d,a
    
    ld (ix+v2),d
    
    
    
;Problem. Fonts are proportional so we need to grab the width of one line with some special code so that we can decide the X coordinate

    ld e,0
    call getlastfontcoordinate 
    exx
    ld a,128
    srl e
    sub e
    ld e,a
    

    ld (ix+v1),e    

    exx
    
    ld (ix+v4),1
    
    
    
    ret

    
.done:


    ret
    
    
.stop:

    ld (ix+v4),10


    ret
    
 
.writecredits:

    
  call writetexttoactivevram


    inc (ix+v4)

    ret


.waitforfadeout:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 20
    ret c
    
    ld (ix+v7),0
    ld (ix+v4),0

    
    jp .nextscreen
    

    ;ret
    
    
    
.init:

    ld hl,35*8 ;move mario out of the screen
    ld (ix+xobject),l
    ld (ix+xobject+1),h


    inc (ix+v7)
    ld a,(ix+v7)
    cp 100
    ret c ;wait a whil so the music can start simultaniously


    ;store two pointers in RAM. @which coordinates are we and... @ which point of the credits are we??
    ;v1 + v2 and v5 + v6 are excellent for these things.

    ld hl,credits
    ld (ix+v5),l
    ld (ix+v5+1),h    
    
    
    ;place mario on some more conventient spot and spawn a cute yoshi for more effects and maybe a princess. who knows
    ld hl,15
    ld (ix+xobject),l
    ld (ix+xobject+1),h
    
   
   
   call setpage12tohandleobjectmovement
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex568 ;color address
  ld    bc,spriteaddrex568c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset    

 ;spawn a yoshi because he is cute 
    ld de,10+1
    ld hl,26+1
;spawn final cutscene object    
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),228                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  
    ld (ix+v4),13
  
  ld b,8
  call increasespritecount
  
  
     ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,coloraddressprincess  ;color address
  ld    bc,characteraddressprincess  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock8
  call    setcharcolandoffset 
  
    pop ix   
  
  
  
  
    ld a,cutsceneblock
    call block12    
    
.nextscreen:   
    
    
    ld l,(ix+v5)
    ld h,(ix+v5+1)
 ;   ld de,$105B
    ld de,$1000
    
    ld (ix+v2),d

 
    ld e,0
    call getlastfontcoordinate 
    exx
    ld a,128
    srl e
    sub e
    ld e,a
    

    ld (ix+v1),e    

    exx 
 

    inc (ix+v4)
    ld (ix+v7),0
    
    ret
    
    
credits: db "programming",0
db " ",0
db "daemos",0
db "norakomi",0
db " ",0
db "additional programming",0
db "and tooling",0
db " ",0
db "grauw",0
db "pedseven",0
db "artrag",0
db "team shadow",0
db "guillian",0
db "nyyrikki",0
db " ",0
db "maps",0
db " ",0
db "norakomi",0
db "hydragon",0
db "daemos",0
db "grauw",0
db "poesje",0
db "some guy from spain",0,1
;db " ",0
db "sfx",0
db " ",0
db "norakomi",0
db "daemos",0
db "artrag",0
db " ",0
db "gfx",0
db " ",0
db "norakomi",0
db "daemos",0
db "grauw",0
db " ",0
db "music",0
db " ",0
db "norakomi",0
db "daemos",0
db " ",0
db "advice technical assistance",0
db " ",0
db "grauw",0
db "guillian",0,1
;db " ",0
db "testing",0
db " ",0
db "poesje",0
db "hydragon",0
db " ",0
db "special hardware provisioning",0
db " ",0
db "meits",0
db "supersoniqs",0
db "edoz",0
db "hydragon",0,1
db " ",0
db "special thanks",0
db " ",0
db "everyone who send",0 
db "me sweet words",0
db "thank you all for",0
db "your support",0
db "could not have",0 
db "done it without you guys",0
db "hey msx.org thank",0
db "you for your support",0
db " ",0
db "special message",0
db " ",0
db "to the idiot on",0
db "youtube whining about",0
db "the camera not being perfect.",0
db "go grab the source and",0
db "fix it yourself!",0,2
 

getnextline:

    ld a,(hl)  
    and a
    jp z,.exit
        inc hl
    jp getnextline

.exit:

    inc hl

    ld a,(hl) ;check for stop byte
    cp 1
    jp z,.exitspecial  
    cp 2
    jp z,.done
    
    ret

.exitspecial:


    inc hl
    scf 
    ret
 
.done:


    ld (ix+v4),12
    
    inc sp
    inc sp
    
    ret
 
 
 
;fetch the last coordinate after reading a single line of text. Does not write anything to the screen. changes e
getlastfontcoordinate:

  push de

  ld de,levelbackgroundtileslist
  call convertstringtonumber ;convert the strings into numbers

  pop de
  
;precall  
  
  ld hl,levelbackgroundtileslist
    
  push hl
  push de
  exx
  pop de
  pop hl
  ld a,(maxrowcount)
  ld c,a
;  ld c,.maxrowcount
  ld b,e
  exx
  
;preloop 

.puttext:
  
  exx
  ld a,(hl)
  cp $BF
  call z,.setspace
  exx
  
  add a,a
  add a,a
  ld e,a
  ld d,0
  ld hl,fontadress 
  add hl,de
  
  exx
  push de
  exx
  pop de

  inc hl ;next byte
  

  ld b,3
.upload:

  inc hl
  
  djnz .upload

  ld b,7
.loadfont:  

  push bc

  dec hl
  dec hl
  dec hl
  dec hl
  ld de,32*4
  add hl,de
  
  ld b,4
.upload2:

  inc hl
  
  djnz .upload2

  pop bc
  
  djnz .loadfont

  exx
  ld a,e
  add a,8
  ld e,a
 
  ld a,(hl)
  cp $BF
  jp z,.space
 
  push hl
  push de
  push bc
 
  ld c,e ;store shift value in c
 
  ld a,(hl)
  ld e,a
  ld d,0
  ld hl,fontsizemap
  add hl,de
  ld a,(hl)
  dec a
  ld e,a
  
  ld a,c
  sub e
 
;PROBLEM!!: the last bit is lost in the VDP! so it is all or nothing
  bit 0,a
  call nz,.inca
 
 
  pop bc
  pop de
  pop hl
 
  ld e,a ;store final result

.space:
 
  dec c
  call z,.nextrow
 
 
  inc hl
  ld a,(hl)
  exx
  cp 255
  ret z
  jp .puttext


 ; ret

.nextrow:

  ld c,1 ;keep c reset until a space apears
  ld a,(hl)
  cp $BF
  ret nz 
  
  ld a,(maxrowcount)
  ld c,a
  ;ld c,.maxrowcount ;reset rowcounter
  ld a,d
  add a,8 ;shift to next fontposition
  ld d,a

  ld e,b

  ret

.maxrowcount: equ 25 ;maximum amount of characters per row

.inca:

  inc a
  ret

.setspace:

  ld a,31
  ret 
 

;write any text from a predefined fontmap into the vram. needs conversion from a string first
;autoreads the converted string from RAM
;e X coordinate d Y coordinate
;in HL adress of textstring
writetexttoactivevram:

  push de

  ld de,levelbackgroundtileslist
  call convertstringtonumber ;convert the strings into numbers

  pop de
  
;precall  
  
  ld hl,levelbackgroundtileslist
    
  push hl
  push de
  exx
  pop de
  pop hl
  ld a,(maxrowcount)
  ld c,a
;  ld c,.maxrowcount
  ld b,e
  exx
  
;preloop 

.puttext:
  
  exx
  ld a,(hl)
  cp $BF
  call z,.setspace
  exx
  
  add a,a
  add a,a
  ld e,a
  ld d,0
  ld hl,fontadress 
  add hl,de
  
;set command register
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  exx
  push de
  exx
  pop de
  
  ld a,e
  out ($9b),a
  xor a
  out ($9b),a ;X = 16
  ld a,d
  out ($9b),a
  ld a,(sc5page)
  xor 1
;  ld a,2
  out ($9b),a ;Y = 16
  
  ld a,8
  out ($9b),a
  xor a
  out ($9b),a ;8 in size
  ld a,8
  out ($9b),a
  xor a
  out ($9b),a ;font = 8*8
  
  ld a,(hl) ;fetch first color byte
  out ($9b),a ;first byte
  inc hl ;next byte
  
  xor a
  out ($9b),a ;setup
  
  ld a,%11110000
  out ($9b),a ;go!!  

  ld b,3
.upload:
  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  inc hl
  
  djnz .upload

  ld b,7
.loadfont:  

  push bc

  dec hl
  dec hl
  dec hl
  dec hl
  ld de,32*4
  add hl,de
  
  ld b,4
.upload2:
  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  inc hl
  
  djnz .upload2

  pop bc
  
  djnz .loadfont
  ei

  exx
  ld a,e
  add a,8
  ld e,a
 
  ld a,(hl)
  cp $BF
  jp z,.space
 
  push hl
  push de
  push bc
 
  ld c,e ;store shift value in c
 
  ld a,(hl)
  ld e,a
  ld d,0
  ld hl,fontsizemap
  add hl,de
  ld a,(hl)
  dec a
  ld e,a
  
  ld a,c
  sub e
 
;PROBLEM!!: the last bit is lost in the VDP! so it is all or nothing
  bit 0,a
  call nz,.inca
 
 
  pop bc
  pop de
  pop hl
 
  ld e,a ;store final result

.space:
 
  dec c
  call z,.nextrow
 
 
  inc hl
  ld a,(hl)
  exx
  cp 255
  ret z
  jp .puttext


 ; ret

.nextrow:

  ld c,1 ;keep c reset until a space apears
  ld a,(hl)
  cp $BF
  ret nz 
  
  ld a,(maxrowcount)
  ld c,a
  ;ld c,.maxrowcount ;reset rowcounter
  ld a,d
  add a,8 ;shift to next fontposition
  ld d,a

  ld e,b

  ret

.maxrowcount: equ 25 ;maximum amount of characters per row

.inca:

  inc a
  ret

.setspace:

  ld a,31
  ret
 
 
 
;cutscene7:
cutscene6:


  ld a,(ix+objectrelease)
  dec a ;1
  jp z,handleboomthing
  dec a ;2
  jp z,handlecutsceneegg
  dec a ;3
  jp z,handlefuse
  dec a ;4
  jp z,.handlequestionmark
  dec a ;5
  jp z,handlepfewcloud  
  
;mario our main hero does all the stuff
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy  
    
  ld a,(ix+v4)
  and a
  jp z,cutscene2.init
  dec a ;step1 walk towards the left of screen
  dec a
  jp z,cutscene2.step2 ;put egg down
  dec a
  jp z,cutscene2.step3 ;jump onto explosion thingy
  dec a
  jp z,cutscene2.step4 ;bounce back and duck down
  dec a
  jp z,.spawnquestionmark
  dec a
  jp z,.wait
  dec a
  jp z,.step3 ;walk to the right
  dec a
  jp z,.spawnquestionmarks
  dec a
  jp z,.step5 ;wait for fuse and blow everything up!
  dec a
  jp z,cutscene2.step6 ;collapse castle
  dec a
  jp z,cutscene2.step7 ;trow the flag
  dec a
  jp z,.step11 ;trow the pfew cloud
  dec a
  jp z,cutscene2.trowtext ;trow text line by line  
  dec a
  jp z,waitfor1
  dec a
  ret z
  
  ;walk towards the left for a while
  ld e,(ix+v7)
  ld d,0
  ld hl,cutscenemovetable
  add hl,de
  ld a,(hl)
  neg
  and a
  jp z,cutscene2.setstep2
  
  ld (ix+v1),a
  call addv1toxbject

  call cutscene2.moveegg
  
  call cutscene2.animatewalkcycle
 

  inc (ix+v7)


  ret

.step11:

  call Playsfx.swim 

  push ix

  ld hl,24
  ld e,10+2

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),5
  ld (ix+v1),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex576 ;color address
  ld    bc,spriteaddrex576c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  pop ix  
  
  inc (ix+v4)
  ld (ix+v7),0

  ret  
  
  
.step5:;blow the castle up

  ld a,(ix+v7)
  and a
  call z,cutscene2.tripscreen ;BUG: the music totally hangs. Has something to do with the lineints for sure!
  cp 11
  jp nc,cutscene2.setstep6special
  
  ld (ix+v6),25
  
  inc (ix+v7)

  call cutscene2.writecastleexplosion


  ld a,(framecounter)
  and 1
  jp z,cutscene2.changebkgcolor


  ld hl,currentpalette
  call SetPalette


  ret  
  
  
.wait:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c
    
    inc (ix+v4)
    ld (ix+v7),0
    
    
    ret
  
  
  
;walk towards the right
.step3:

  ld a,(ix+v7)
  cp 38
  jp nc,.setstep4

  ld (ix+v1),3
  call addv1toxbject

  call cutscene3.animatewalkcycle

  inc (ix+v7)


  ret  

  
.setstep4:
    
    
    ld (ix+v7),0
    inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariofried ;color address
  ld    bc,characteraddressmariofried  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    
    

;    ret
  
  
  
.handlequestionmark:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 15
    ret c
    
    jp destroyobject


    ret

.spawnquestionmarks:

    inc (ix+v7)
    ld a,(ix+v7)
    cp 5
    jp z,.spawn1
    cp 10
    jp z,.spawn2
    cp 15
    jp z,.spawn3
    cp 20
    jp z,.spawn4
    cp 35
    ret c
    
    call Playsfx.explosion
    
    ld (ix+v7),0 ;next step
    inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariofried1 ;color address
  ld    bc,characteraddressmariofried1  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset     
    
    
;    ret
    
.spawn1:

    push ix
    ld hl,20
    ld e,9
    jp .doit

.spawn2:

    push ix
    ld hl,22
    ld e,8
    jp .doit
    
.spawn3:

    push ix
    ld hl,24
    ld e,8
    jp .doit
    
.spawn4:

    push ix
    ld hl,26
    ld e,9
    jp .doit    
    
    
.spawnquestionmark:


    inc (ix+v7)
    ld a,(ix+v7)
    cp 30
    ret c

  ;call Playsfx.explosion 
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex568 ;color address
  ld    bc,spriteaddrex568c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset 

    ld (ix+v7),0
    inc (ix+v4)

  push ix

  ld hl,8
  ld e,8

.doit:  
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),4

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressquestionmark ;color address
  ld    bc,characteraddressquestionmark  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  pop ix


    ret



  
  
  
handlerocket:

    call addv2toybject
    
    inc (ix+v7)
    ld a,(ix+v7)
    cp 25
    push af
    call z,addv1toxbject
    pop af    
    cp 50
    push af
    call z,addv1toxbject
    pop af
    cp 70
    jp nc,.destroyobjectwithcloudnostar
    
    
    
    ld a,(framecounter)
    and 3
    jp z,.animate

    ret

.animate:    
    
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddressflyingcastle ;color address
  ld    bc,characteraddressflyingcastle  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:

  ld    hl,coloraddressflyingcastle1 ;color address
  ld    bc,characteraddressflyingcastle1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset


    ret

.destroyobjectwithcloudnostar:

;  xor a
;  ld (sfxplaying),a
    call Playsfx.spinjumpstomp


    jp destroyobjectwithcloudnostar


handlecastleflame:


    call addv2toybject

    ld a,(framecounter)
    and 3
    jp z,.animate

    ret

.animate:    
    
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
    jp z,.position1
 
 
.position0:
  ld    hl,coloraddresscastleflame  ;color address
  ld    bc,characteraddresscastleflame  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset    

.position1:

  ld    hl,coloraddresscastleflame1  ;color address
  ld    bc,characteraddresscastleflame1  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  



spawncastleflame:
 
  push ix

  ld hl,13+10
  ld e,12

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),4
  ld (ix+v2),-1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  inc hl
  inc hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddresscastleflame ;color address
  ld    bc,characteraddresscastleflame  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  pop ix
  
  ret



;TODO: separate cutscenes into different files
;ludwig's castle defeat
cutscene5:


  ld a,(ix+objectrelease)
  dec a ;1
  jp z,handleboomthing
  dec a ;2
  jp z,handlecutsceneegg
  dec a ;3
  jp z,handlefuse
  dec a
  jp z,handlecastleflame
  dec a
  jp z,handlerocket
  dec a
  ret z
  
;mario our main hero does all the stuff
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy  
    
  ld a,(ix+v4)
  and a
  jp z,cutscene2.init
  dec a ;step1 walk towards the left of screen
  dec a
  jp z,cutscene2.step2 ;put egg down
  dec a
  jp z,cutscene2.step3 ;jump onto explosion thingy
  dec a
  jp z,cutscene2.step4 ;bounce back and duck down
  dec a
  jp z,cutscene2.step5 ;wait for fuse and blow everything up!
  dec a
  jp z,.step6 ;launch castle out of screen
  dec a
  jp z,.step7 ;drop the castle back on the mountain
  dec a
  jp z,cutscene2.trowtext ;trow text line by line  
  dec a
  jp z,waitfor1
  dec a
  ret z
  
  ;walk towards the left for a while
  ld e,(ix+v7)
  ld d,0
  ld hl,cutscenemovetable
  add hl,de
  ld a,(hl)
  neg
  and a
  jp z,cutscene2.setstep2
  
  ld (ix+v1),a
  call addv1toxbject

  call cutscene2.moveegg
  
  call cutscene2.animatewalkcycle
 

  inc (ix+v7)


  ret


.endcutscene:

  ld a,(ix+v7)
 ; call .writeflag

  inc (ix+v6)
  ld a,(ix+v6)
  cp 3
  ret c

  inc (ix+v4)
  ld (ix+v6),0
  ld (ix+v7),0

  ret

.spawnrocket:

  push ix

  ld hl,13+4
  ld e,2

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),5
  ld (ix+v1),-1  
  ld (ix+v2),1  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressflyingcastle ;color address
  ld    bc,characteraddressflyingcastle  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  pop ix
  
  ret

  
  

.step7: 
  
  inc (ix+v7)
  ld a,(ix+v7)

  cp 16
  jp z,.spawnrocket
  cp 20
  push af
  call z,.setmariolookup1
  pop af
  cp 36
  push af
  call z,.setmariolookup2
  pop af  
  cp 80+16
  jp z,.writerubbish
  cp 80+16+1
  jp z,.writerubbish  
  cp 128
  jp nc,.endcutscene



  ret

.setmariolookup2:  
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariolooking1 ;color address
  ld    bc,characteraddressmariolooking1  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
  
  ;ret    
  
  
.setmariolookup1:  
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariolooking ;color address
  ld    bc,characteraddressmariolooking  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock6
  jp    setcharcolandoffset  
  
  
  ;ret  
  
.setmariolookup:  

;trow questionmark above mario' s head
  push ix

  ld hl,8
  ld e,8

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),6

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressquestionmark ;color address
  ld    bc,characteraddressquestionmark  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock6
  call    setcharcolandoffset 

  pop ix



  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariolookingup ;color address
  ld    bc,characteraddressmariolookingup  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset  
  
  
  ;ret
  

.setstep7:


  ld (ix+v7),0
  inc (ix+v4)
  ld (ix+v6),0

  ret

;Use this command to write the plaster
.writerubbish:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192+48
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,64
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-8-24
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80+16-10
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,8
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,8
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM +TIMP
  ei
  out ($9b),a ;command 

  call pollvdpcmd


  ret


.step6:

;first make smokecloud then start the launch
  ld a,(ix+v3)
  cp 15 
  jp nc,.skipsmoke

  call .putupsmokecloud

  inc (ix+v3) 

  ret
.skipsmoke:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 64+32+8+16+2
  jp nc,.setstep7
  cp 64+32+8
  jp nc,.removesmoke
  cp 64+32+6
  jp nc,.writebackcastlebkg
  cp 16
  push af
  call z,spawncastleflame
  pop af
  cp 32
  call z,.setmariolookup

;    cp 128
;    jp nc,hang

  call .writebackcastlebkg
  
  call .blitcastle

  ld a,15
  call .putupsmokecloud

  ld (ix+v6),16

  ld a,(framecounter)
  and 3
  ret nz
  
    ld a,1
    ld (tripscreen),a ;shake!!
  
  ret


.removesmoke:

  call .writebackcastlebkg2
  
 ; call .writerubbish ;fix rubish
  
  ld a,(ix+v6)
  and a
  ret z
  dec (ix+v6)
  call .putupsmokecloud


  ret



.putupsmokecloud:

  ld c,a

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  
  ld a,(sc5page)
  add a,a ;2
  add a,a ;4
  add a,a ;8
  add a,a ;16
  add a,a ;32
  add a,a ;64 
  ld b,a
  
  xor a
  add a,b
  out ($9b),a; Xs
  out ($9b),a; Xs

  ld a,112
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40-8-1
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,96+16-1
  sub c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,64
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,1
;  ld a,16
  add a,c
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP;HMMM
  ei
  out ($9b),a ;command 

  call pollvdpcmd
  
  ret


;.set2:
;    
;    ld c,24
;    
;    ret


.writebackcastlebkg:

;  ld c,0

;  ld a,(ix+v7)
;  cp 31
;  call nc,.set2

    ld c,(ix+v7)

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

;xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,16
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

;PROBLEM: writing back the entire background takes too much time. Thus we need to step it down a little
  ld a,96
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret
  

.writebackcastlebkg2:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32-16
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16+24
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-16
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48+24
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,64+8
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

;PROBLEM: writing back the entire background takes too much time. Thus we need to step it down a little
  ld a,32+16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret

.setzero:

    xor a
    ret

.setc:

    ld a,(ix+v7)
    sub 64-32+8
    ld c,a
    
    ret

;by far the worst MSX code ever written. It works thats what matters    
.setup:

    ld a,(ix+v7)
    sub 32


    ret
    

.blitcastle:

  ld c,(ix+v7)
  
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,(ix+v7)
  cp 64-32+1
  ld a,0
  call nc,.setup
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48
  sub c
  call c,.setzero
  sub 16
  call c,.setzero
  add a,16
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld c,0
  ld a,(ix+v7)
  cp 47-32+6
  call nc,.setc
  ld a,64
  sub c
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret



;set in two frames
.setstep6:


;    call Playsfx.yoshifireball ;problem: scc replayer is not active on the cutscene engine

  ld hl,currentpalette
  call SetPalette ;write palette once more to prevent any stucking

  call .writebackcastle

  dec (ix+v6) ;wait two frames
  ret nz

  inc (ix+v4)
  ld (ix+v6),0
  ld (ix+v7),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex568 ;color address
  ld    bc,spriteaddrex568c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset 

;  call Playsfx.yoshifireball ;problem: scc replayer is not active on the cutscene engine
  
  ret

.writebackcastle:
 
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,65-32
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,32
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  ;halt
  
  ret


;lemmy's castle
cutscene4:

  ld a,(ix+objectrelease)
  dec a ;1
  dec a ;2
  jp z,handlecutsceneegg
  dec a ;3
  jp z,handleshakecastle
  dec a ;4
  jp z,demolishcastle  ;the order of spawning DOES matter!!
  dec a ;5
  jp z,handlecastlesmoke  
  dec a ;6
  jp z,trowflag
  dec a
  ret z ;hammer in hands of mario and black cover sprite on left of screen ;update from here


;mario our main hero does all the stuff
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy  
    
  ld a,(ix+v4)
  and a
  push af
  call z,cutscene3.init
  pop af
  jp z,.init
  dec a ;step1 walk towards the left of screen
  dec a
  jp z,cutscene2.step2 ;put egg down
  dec a
  jp z,.walkoutofscreen
  dec a
  jp z,.fetchhammer
  dec a
  jp z,.walktoright ;walk back towards the castel with hammer in hand
  dec a
  jp z,cutscene3.setstep8 ;destroy castle
  dec a
  jp z,.goaroundandsmash  ;animate mario jumping around and smash things 
  dec a
  jp z,.wait ;wait a little bit or else we get artefacts
  dec a
  jp z,cutscene2.trowtext 
  dec a
  jp z,waitfor1  
   

  ;walk towards the left for a while
  ld e,(ix+v7)
  ld d,0
  ld hl,cutscenemovetable
  add hl,de
  ld a,(hl)
  neg
  and a
  jp z,cutscene2.setstep2
  
  
  ld (ix+v1),a
  call addv1toxbject

  call cutscene2.moveegg
  
  call cutscene2.animatewalkcycle
 

  inc (ix+v7)


  ret

.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  ret c
  
  jp .nextstep


.animatemariospin:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  jp z,.animate4
  cp 6
  jp z,.animate3  
  cp 4
  jp z,.animate2
  cp 2
  jp z,.animate1
  ret

.animate1:

  ld    hl,coloraddressmariohammer4 ;color address
  ld    bc,characteraddressmariohammer4  ;character address
  ld    de,offsetshell  
  call .animatehammer


  ld    hl,coloraddressmariowalk1 ;color address
  ld    bc,characteraddressmariowalk1  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate2:

  ld    hl,coloraddressmariohammer3 ;color address
  ld    bc,characteraddressmariohammer3  ;character address
  ld    de,offsetkooparight  
  call .animatehammer

  ld    hl,coloraddressmariofront ;color address
  ld    bc,characteraddressmariofront  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate3:

  ld    hl,coloraddressmariohammer2 ;color address
  ld    bc,characteraddressmariohammer2  ;character address
  ld    de,offsetsupercape2l    
  call .animatehammer

  ld    hl,coloraddressmariowalk1r ;color address
  ld    bc,characteraddressmariowalk1r  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate4:

  ld    hl,coloraddressmariohammer3 ;color address
  ld    bc,characteraddressmariohammer3  ;character address
  ld    de,offsetkooparight  
  call .animatehammer

  ld    hl,coloraddressmarioback ;color address
  ld    bc,characteraddressmarioback  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret


.goaroundandsmash:

  inc (ix+v7)
  
  call .moveleft

  call .animatemariospin

  ;move the hammer along
  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  
  push hl
  pop iy
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  ld de,8
  sbc hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h


  ld a,(ix+v7)
  cp 20
  jp z,.turn
  cp 40
  jp z,.turn  
  cp 60
  jp z,.turn    
  cp 82
  jp z,.nextstepspecial   
  
  
  ld a,(framecounter)
  and 7
  ret nz
  
;spawn debris and play sound

  call Playsfx.breakblock

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  
  push ix
  
  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),23                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  ld (ix+deathtimer),0

  ld (ix+v1),2
  
  ld a,r
  and 1
  call z,.negate

  pop ix
   
  
  ret

.negate:

    ld (ix+v1),-2
    ret



.nextstepspecial:

;put hammer sprite back in position
  ld    hl,coloraddressmariohammer4 ;color address
  ld    bc,characteraddressmariohammer4  ;character address
  ld    de,offsetshell  
  call .animatehammer

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariowalk1r ;color address
  ld    bc,characteraddressmariowalk1r  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset


  jp .nextstep
  


.turn:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  ret
  
.moveleft:

  ld a,(ix+v3)
  and a
  jp z,.moveright

  ld (ix+v1),-3
  call addv1toxbject

  ret  

.moveright:

  ld (ix+v1),3
  call addv1toxbject

  ret


.animatehammer:

;jump into hammer code and execute sprite updates
  push ix
  
  push de
;get hammer adress
  ld e,(ix+offsets+20)
  ld d,(ix+offsets+21)
  push de
  pop ix 
  pop de 

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;  ld    hl,coloraddressmarioback ;color address
;  ld    bc,characteraddressmarioback  ;character address
;  ld    de,offsetshell
  ld    a,ingamespritesblock5
  call    setcharcolandoffset


  pop ix

  ret


    

.walktoright:

  ld a,(ix+v7)
  cp 15
  push af
  call z,.removecoverupsprite
  pop af
  cp 45
  jp nc,.nextstep

  ld (ix+v1),3
  call addv1toxbject

  call cutscene3.animatewalkcycle


  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret c ;hammer follows after a few frames this to prevent it from exiting the coverup sprite on the left

  ;make the hammer follow mario
  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  push hl
  pop iy
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  ld de,8
  sbc hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h


  ret


.removecoverupsprite:


  ld l,(ix+offsets+18)
  ld h,(ix+offsets+19)
  push hl
  pop iy
  
  
  ld (iy+yobject),0
  ld (iy+yobject+1),0



  ret


;spawn hammer
.fetchhammer:

  push ix

  ld hl,2
  ld e,10

  ld    iy,CustomObject
  ld    (iy),force19 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),7

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmariohammer4 ;color address
  ld    bc,characteraddressmariohammer4  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix

;store ix adress somewhere (global or local)
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  

  inc (ix+v4)

  ret


;spawn the cover sprite (not the hammer)
.init:

  push ix

  ld hl,2
  ld e,10

  ld    iy,CustomObject
  ld    (iy),force00 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),7

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressblack ;color address
  ld    bc,characteraddressblack  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix

;just pick a adress in RAM that is not used during this scene
  ld (ix+offsets+18),l
  ld (ix+offsets+19),h ;save this for removal
  


  ret


.walkoutofscreen:

  ld a,(ix+v7)
  cp 14
  jp nc,.nextstep

  ld (ix+v1),-2
  call addv1toxbject

  call cutscene2.animatewalkcycle

  inc (ix+v7)


  ret


.nextstep:


  inc (ix+v4)
  ld (ix+v7),0

  ret



;morton's castle
cutscene7:
cutscene3:

  ld a,(ix+objectrelease)
  dec a ;1
  dec a ;2
  jp z,handlecutsceneegg
  dec a ;3
  jp z,handleshakecastle
  dec a ;4
  jp z,demolishcastle  ;the order of spawning DOES matter!!
  dec a ;5
  jp z,handlecastlesmoke  
  dec a ;6
  jp z,trowflag
  dec a ;7
  jp z,handlepfewcloud
  

;mario our main hero does all the stuff
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy  
    
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a ;step1 walk towards the left of screen
  dec a
  jp z,cutscene2.step2 ;put egg down
  dec a
  jp z,.step3 ;spagethi ends here ;walk towards the right
  dec a
  jp z,.step4 ;jump against castle
  dec a
  jp z,.step5 ;fall back
  dec a
  jp z,.step6 ;slide and walk back to the right
  dec a
  jp z,.step3 ;walk back to right
  dec a
  jp z,.step4
  dec a
  jp z,.step5
  dec a
  jp z,.step6
  dec a
  jp z,.step3 ;repeat until step6 and return
  dec a
  jp z,.step7 ;jump up really high
  dec a
  jp z,.step8 ;make some jumps
  dec a
  jp z,.step9 ;jump left
  dec a
  jp z,.step10 ;walk away
  dec a
  jp z,cutscene2.trowtext ;wait then finish the cutscene  
  dec a
  jp z,.step11
  dec a ;TODO: make the bubble that mario trows in the end. see step11
  jp z,waitfor1

  ;walk towards the left for a while
  ld e,(ix+v7)
  ld d,0
  ld hl,cutscenemovetable
  add hl,de
  ld a,(hl)
  neg
  and a
  jp z,cutscene2.setstep2
  
  
  ld (ix+v1),a
  call addv1toxbject

  call cutscene2.moveegg
  
  call cutscene2.animatewalkcycle
 

  inc (ix+v7)


  ret



.step11:

  call Playsfx.swim 

  push ix

  ld hl,17-4
  ld e,10+2

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),7
  ld (ix+v1),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex576 ;color address
  ld    bc,spriteaddrex576c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset

  pop ix  
  
  inc (ix+v4)

  ret


.setstep11:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld (ix+v7),0
  inc (ix+v4)
  ld (ix+v6),0
  jp .animate1



.step10:


  ld e,(ix+v7)
  ld d,0
  ld hl,.step10table
  add hl,de
  ld a,(hl)
  and a
  jp z,.setstep11
  
  
  ld (ix+v1),a
  
  call addv1toxbject
  
  call .animatewalkcycleleft
  
  
  inc (ix+v7)
  
  ret

  
.step10table: db -3,-3,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,-1,-0


.animatewalkcycleleft:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  cp 5
  jp nc,.animate3l  
  cp 3
  jp nc,.animate2l
  cp 1
  jp nc,.animate1l
  ret

.animate1l:

  ld    hl,spriteaddrex557 ;color address
  ld    bc,spriteaddrex557c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate2l:

  ld    hl,spriteaddrex574 ;color address
  ld    bc,spriteaddrex574c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate3l:

  ld    hl,spriteaddrex575 ;color address
  ld    bc,spriteaddrex575c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret



.setstep10:

  ld (ix+v7),0 
  inc (ix+v4)
  ret


.step9:

  ld (ix+v1),-2

  ld e,(ix+v7)
  ld d,0
  ld hl,.step9table
  add hl,de
  ld a,(hl)
  and a
  jp z,.setstep10
  
  
  ld (ix+v2),a
  
  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  
  ret

  
.step9table: db -3,-3,-3,-2,-2,-2,-1,1,2,2,2,3,3,3,4,4,5,6,0



.setstep9:

  ld (ix+v7),0
  inc (ix+v4)
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex573 ;color address
  ld    bc,spriteaddrex573c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset
 

;  ret


.step8:

  ld a,(ix+v6)
  cp 3
  jp z,.setstep9

  ld e,(ix+v7)
  ld d,0
  ld hl,.step8table
  add hl,de
  ld a,(hl)
  and a
  jp z,.resetv7
  
  
  ld (ix+v2),a
  
  call addv2toybject
  
  inc (ix+v7)
  
  ret


.resetv7:
  
;  xor a
;  ld (sfxplaying),a
  call Playsfx.breakblock
  
  inc (ix+v6)
  ld (ix+v7),0
  ret


.step8table: db -3,-3,-2,-2,-1,1,1,2,2,3,3,4,4,5,0


.setstep8:

  ld (ix+v7),0
  inc (ix+v4)
  
  call Playsfx.breakblock  
  
  ;create smoke and castle demolisher

  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78 ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  ld (ix+objectrelease),4 ;smoke

  pop ix   


  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78 ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  ld (ix+objectrelease),5 ;demolisher

  pop ix 
  
  
  ret


.step7:

  
  ld e,(ix+v7)
  ld d,0
  ld hl,.step7movetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.setstep8
  ld (ix+v2),a  
  dec a
  call z,.animatefall
  

  ld (ix+v1),2
  
  call addv1toxbject
  call addv2toybject
  
  inc (ix+v7)
  
  ret

.step7movetable: db -8,-8,-7,-6,-6,-5,-5,-4,-4,-3,-3,-2,-2,-1,-1,-1,1,1,2,2,0


.animatefall:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex560 ;color address
  ld    bc,spriteaddrex560c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset
 

;  ret


.resetstep3:

  inc (ix+v4)
  ld (ix+v7),15
  
  
  ret


.step6: ;slide and walk back to right

  ld e,(ix+v7)
  ld d,0
  ld hl,.step6table
  add hl,de
  ld a,(hl)
  and a
  jp z,.resetstep3
  
  ld (ix+v1),a
  
  call addv1toxbject  
  
  inc (ix+v7)

  ret


.step6table: db -4,-4,-3,-3,-3,-2,-2,-2,-1,-1,-1,-1,0

.step5: ;fall back

  ld (ix+v1),-4
  
  ld e,(ix+v7)
  ld d,0
  ld hl,.step5table
  add hl,de
  ld a,(hl)
  and a
  jp z,.setstep6
  
  ld (ix+v2),a
  

  call addv2toybject
  call addv1toxbject
 
  inc (ix+v7)
  
  ret


.setstep6:

  ld (ix+v7),0
  inc (ix+v4) 

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex572 ;color address
  ld    bc,spriteaddrex572c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset
 

;  ret



.step5table: db 1,1,2,2,2,3,3,6,7,0


.setstep5:

  ld (ix+v7),0
  inc (ix+v4)
 
  call Playsfx.explosion 
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex560 ;color address
  ld    bc,spriteaddrex560c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset


  push ix
  call makestarleft
  pop ix
  
  ;shake the castle initiate an object that takes care of this

  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78 ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  ld (ix+objectrelease),3 ;castle object

  pop ix   
  
  
  ret

;ret  
  



.step4: ;jump against castle

  ld e,(ix+v7)
  ld d,0
  ld hl,.step4table
  add hl,de
  ld a,(hl)
  and a
  jp z,.setstep5
  cp -3
  call z,.animateside
  
  
  ld (ix+v2),a
  ld (ix+v1),2
  call addv2toybject
  call addv1toxbject
 
  inc (ix+v7)


  ret


.animateside:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex571 ;color address
  ld    bc,spriteaddrex571c  ;character address
  ld    de,offsetmariosidel
  ld    a,supermariospritesblock2
  jp    setcharcolandoffset

;ret



.step4table: db -8,-8,-7,-6,-6,-5,-3,-2,-1,0


;walk towards the right
.step3:

  ld a,(ix+v7)
  cp 30
  jp nc,.setstep4

  ld (ix+v1),3
  call addv1toxbject

  call .animatewalkcycle

  inc (ix+v7)


  ret


.setstep4:

  inc (ix+v4)
  ld (ix+v7),0

  xor a
  ld (sfxplaying),a
  call Playsfx.jump

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex559 ;color address
  ld    bc,spriteaddrex559c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


 ; ret


.animatewalkcycle:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  cp 5
  jp nc,.animate3  
  cp 3
  jp nc,.animate2
  cp 1
  jp nc,.animate1
  ret

.animate1:

  ld    hl,spriteaddrex568 ;color address
  ld    bc,spriteaddrex568c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate2:

  ld    hl,spriteaddrex569 ;color address
  ld    bc,spriteaddrex569c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate3:

  ld    hl,spriteaddrex570 ;color address
  ld    bc,spriteaddrex570c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret
    




;spawn all other objects required to be on screen
.init:


;spawn the egg

  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex552 ;color address
  ld    bc,spriteaddrex552c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix

  ld (ix+v5),l
  ld (ix+v5+1),h ;store egg adress

  inc (ix+v4)

  ret



handlepfewcloud:

  call addv1toxbject
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 
   
  ld a,(ix+v7)
  inc (ix+v7)
  cp 4
  jp z,.step2
  cp 8
  jp z,.step3
  cp 12
  jp z,.step4
  cp 16
  jp z,destroyobject

  ret

.step2:


  ld    hl,spriteaddrex577 ;color address
  ld    bc,spriteaddrex577c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret

.step3:

  ld    hl,spriteaddrex578 ;color address
  ld    bc,spriteaddrex578c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret



.step4:

  ld    hl,spriteaddrex579 ;color address
  ld    bc,spriteaddrex579c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset


 ; ret




handlecastlesmoke:

  ld a,(ix+v7)
  cp 64
  jp nc,.removesmoke
  inc (ix+v7)  
  cp 15
  ld a,15
  jp nc,cutscene2.putupsmokecloud


  ld a,(ix+v7)

  call cutscene2.putupsmokecloud

  ret


.removesmoke:

  ld a,(ix+v4)
  inc (ix+v4)  
  cp 17
  jp nc,.destroyobject
  cp 15
  ld a,0
  jp nc,cutscene2.writerubbish



  call cutscene2.writebackcastlebkg2
  call cutscene2.writerubbish
  
  ld a,(ix+v4)
  ld c,a
  ld a,15
  sub c  
  
  call cutscene2.putupsmokecloud
  

  ret  


.destroyobject:


;spawn the egg

  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),6

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78 ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  pop ix

  jp destroyobject
  


trowflag:

  ld c,24
  call cutscene2.writebackcastlebkg2
 
  ld a,(ix+v7)
  call cutscene2.writeflag

  call cutscene2.writerubbish ;write rubish first then write flag

  ld a,(ix+v7)
  cp 17
  jp z,.destroyobject


  inc (ix+v7)

  ret

.destroyobject:

  call cutscene2.writeflag
  call cutscene2.writerubbish ;write rubish first then write flag

  ld a,(ix+v4)
  inc (ix+v4)
  and a
  ret z

  jp destroyobject



demolishcastle:

  ld a,(ix+v7)
  cp 64
  jp nc,destroyobject
  ld c,a
  call cutscene2.blitcastle  
  call .trowcastlebkgline
  
  
  inc (ix+v7)

  ret

.trowcastlebkgline:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32;+48
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16-2
  add a,c
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4;+48
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48-2
  add a,c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,2
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret




handleshakecastle:

  ld a,(ix+v7)
  and a ;0
  call z,shakecastleleft
  dec a ;1
  call z,shakecastleright
  dec a ;2
  call z,shakecastleleft
  dec a ;3
  call z,shakecastleright
  dec a ;4
  call z,setcastleback
  dec a ;5
  call z,setcastleback
  dec a
  jp z,destroyobject
  
  inc (ix+v7)
  


  ret


setcastleback:

  call writecastlebkglines
  ld c,0
  call movecastle

  ret

shakecastleleft:

  call writecastlebkglines
  ld c,-2
  call movecastle

  ret

shakecastleright:

  call writecastlebkglines
  ld c,2
  call movecastle

  ret


movecastle:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
;  add a,c
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  add a,c
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,64
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP;HMMM
  ei
  out ($9b),a ;command 
  
;  call pollvdpcmd

  ret




writecastlebkglines:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32-4+48
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16;-24;+24
;  add a,c
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-4+48
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48;-24-8-2
;  add a,c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,8;48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,64
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32-4
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16;-24;+24
;  add a,c
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-4
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48;-24-8-2
;  add a,c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,8;48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,64
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 

  call pollvdpcmd  
  
  ret
  




;iggy's castle
cutscene2:

  ld a,(ix+objectrelease)
  dec a ;1
  jp z,handleboomthing
  dec a ;2
  jp z,handlecutsceneegg
  dec a ;3
  jp z,handlefuse
  
  
;mario our main hero does all the stuff
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy  
    
  ld a,(ix+v4)
  and a
  jp z,.init
  dec a ;step1 walk towards the left of screen
  dec a
  jp z,.step2 ;put egg down
  dec a
  jp z,.step3 ;jump onto explosion thingy
  dec a
  jp z,.step4 ;bounce back and duck down
  dec a
  jp z,.step5 ;wait for fuse and blow everything up!
  dec a
  jp z,.step6 ;collapse castle
  dec a
  jp z,.step7 ;trow the flag
  dec a
  jp z,.trowtext ;trow text line by line  
  dec a
  jp z,waitfor1
  dec a
  ret z
  
  ;walk towards the left for a while
  ld e,(ix+v7)
  ld d,0
  ld hl,cutscenemovetable
  add hl,de
  ld a,(hl)
  neg
  and a
  jp z,.setstep2
  
  ld (ix+v1),a
  call addv1toxbject

  call .moveegg
  
  call .animatewalkcycle
 

  inc (ix+v7)


  ret

;trow a new line of text each frame
.trowtext:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 16
  ret c
  cp 18 ;write the textline 2 frames
  jp z,.nexttextline


  ld a,(ix+v6)
  call .writetextline

  ret

.nexttextline:

  ld (ix+v7),0
  inc (ix+v6)
  ld a,(ix+v6)
  cp 9
  ret c
  
  inc (ix+v4) ;done wait for userinteract

  ret


;write a line of text
.writetextline:

  add a,a ;2
  add a,a ;4
  add a,a ;8
  ld c,a

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,$16
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,$90
  add a,c
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,$16;-4
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,$90
  add a,c  
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,256-$22+4
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,8
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 

  ret


.endcutscene:

  ld a,(ix+v7)
  call .writeflag

  inc (ix+v6)
  ld a,(ix+v6)
  cp 3
  ret c

  inc (ix+v4)
  ld (ix+v6),0
  ld (ix+v7),0

  ret


.step7: 

  ld c,24
  call .writebackcastlebkg2
 
  ld a,(ix+v7)
  call .writeflag

  call .writerubbish ;write rubish first then write flag

  ld a,(ix+v7)
  cp 17
  jp nc,.endcutscene

  inc (ix+v7)

  ret

.writeflag:

  ld c,a

 ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,128
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,112
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,160+32-4+2
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80+6+16
  sub c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,16
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,1
  add a,c
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM +TIMP
  ei
  out ($9b),a ;command 

  call pollvdpcmd


  ret

.setstep7:

;  xor a
;  ld (sfxplaying),a
;  call Playsfx.jump


  ld (ix+v7),0
  inc (ix+v4)
  ld (ix+v6),0

  ret


.writerubbish:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,64
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-8
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80+16
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,64
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM +TIMP
  ei
  out ($9b),a ;command 

  call pollvdpcmd


  ret


.step6:

;TODO: first make the smoke come up then proceed with this step use v3 first
  ld a,(ix+v3)
  cp 15 
  jp nc,.skipsmoke

  call .putupsmokecloud

  inc (ix+v3) 

  ret
.skipsmoke:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 64+16+2
  jp nc,.setstep7
  cp 64
  jp nc,.removesmoke


  call .writebackcastlebkg
  
  call .blitcastle

  ld a,15
  call .putupsmokecloud

  ld (ix+v6),16


  ret


.removesmoke:

  call .writebackcastlebkg2
  
  call .writerubbish ;fix rubish
  
  ld a,(ix+v6)
  and a
  ret z
  dec (ix+v6)
  call .putupsmokecloud


  ret



.putupsmokecloud:

  ld c,a

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  
  ld a,(sc5page)
  add a,a ;2
  add a,a ;4
  add a,a ;8
  add a,a ;16
  add a,a ;32
  add a,a ;64 
  ld b,a
  
  xor a
  add a,b
  out ($9b),a; Xs
  out ($9b),a; Xs

  ld a,112
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40-8-1
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,96+16-1
  sub c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,64
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,1
;  ld a,16
  add a,c
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP;HMMM
  ei
  out ($9b),a ;command 

  call pollvdpcmd
  
  ret


.set2:
    
    ld c,24
    
    ret


.writebackcastlebkg:

  ld c,0

  ld a,(ix+v7)
  cp 31
  call nc,.set2


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16;+24
  add a,c
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48;+24
  add a,c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

;PROBLEM: writing back the entire background takes too much time. Thus we need to step it down a little
  ld a,32;64
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret
  

.writebackcastlebkg2:


  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,164+4-32-16
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,48-16+24
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,164+4-16
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48+24
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,64+8
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

;PROBLEM: writing back the entire background takes too much time. Thus we need to step it down a little
  ld a,32+16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret




.blitcastle:

  ld c,(ix+v7)

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,48
  add a,c
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,64
  sub c
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP;HMMM
  ei
  out ($9b),a ;command 
  
  call pollvdpcmd
  
  ret



;set in two frames
.setstep6:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex568 ;color address
  ld    bc,spriteaddrex568c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  call    setcharcolandoffset 

.setstep6special:  

  ld hl,currentpalette
  call SetPalette ;write palette once more to prevent any stucking

  call .writebackcastle

  dec (ix+v6) ;wait two frames
  ret nz

  inc (ix+v4)
  ld (ix+v6),0
  ld (ix+v7),0



  ret

.writebackcastle:
 
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,65-32
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,32
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
  ;halt
  
  ret


.step5:;blow the castle up

  ld a,(ix+v7)
  and a
  call z,.tripscreen ;BUG: the music totally hangs. Has something to do with the lineints for sure!
  cp 11
  jp nc,.setstep6
  
  ld (ix+v6),25
  
  inc (ix+v7)

  call .writecastleexplosion


  ld a,(framecounter)
  and 1
  jp z,.changebkgcolor


  ld hl,currentpalette
  call SetPalette


  ret

.tripscreen:

  ld a,1
  ld (tripscreen),a
  ret

.changebkgcolor:

  
  ld a,1 ;color number
  ld e,255
  ld d,255
  call switchcolor
  
  ld a,14 ;color number
  ld e,0
  ld d,255
  call switchcolor


  ret


.writecastleexplosion:
  
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,192
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,80
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,128+40
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,80
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,48
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,32
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  ei
  out ($9b),a ;command 
  
 ; halt
  
  ret
  



.step4:

  ld e,(ix+v7)
  ld d,0
  ld hl,.bouncebacktable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.setstep5
  
  ld (ix+v2),a
  call addv1toxbject
  call addv2toybject


  inc (ix+v7)


  ret


.bouncebacktable: db -3,-3,-2,-2,-1,-1,0,1,1,2,2,2,3,3,4,5,10



.setstep5:


  call Playsfx.explosion

  ld (ix+v6),0
  ld (ix+v7),0
  inc (ix+v4)


  ret



.setstep4:

  ld l,(ix+offsets+25)
  ld h,(ix+offsets+26)
  push hl
  pop iy
  
  call Playsfx.breakblock

  ld (iy+v4),1 ;activate boom thingy

  ld (ix+v1),-1
  ld (ix+v7),0
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex561 ;color address
  ld    bc,spriteaddrex561c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset

;  ret

.setspritefall:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex560 ;color address
  ld    bc,spriteaddrex560c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset 


.step3:


  ld e,(ix+v7)
  ld d,0  
  ld hl,.jumpfalltable
  add hl,de
  ld a,(hl)
  and a
  push af
  call z,.setspritefall
  pop af
  cp 10
  jp z,.setstep4
  
  ld (ix+v2),a
  call addv2toybject
  ld (ix+v1),2
  call addv1toxbject
  
  
  inc (ix+v7)
  ret 

.jumpfalltable: db -6,-5,-5,-4,-4,-2,-2,-2,-1,-1,-1,0,1,2,3,3,4,4,5,10 

.step2:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c ;wait a few cycles
  cp 16
  jp z,.puteggdown
  cp 20
  jp z,.comebackup
  cp 30
  jp nc,.setstep3

  ret

.setstep3:


  call copythnxtoactivepage ;make this happen twice

  dec (ix+v6)
  ret nz

  ld (ix+v7),0

  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld a,(cutscenenr)
  cp 2
  jp z,.continue
  cp 5
  jp z,.continue
  cp 6
  jp z,.continue
  
  ret

.continue:  
  
  xor a
  ld (sfxplaying),a
  call Playsfx.jump

  ld    hl,spriteaddrex559 ;color address
  ld    bc,spriteaddrex559c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset  
  
;  ret

.comebackup:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex557 ;color address
  ld    bc,spriteaddrex557c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret


.puteggdown:

  ld (iy+v5),1 ;tell the egg to start bouncing
  
  ld hl,96
  ld (iy+yobject),l
  ld (iy+yobject+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex556 ;color address
  ld    bc,spriteaddrex556c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret


.setstep2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    (ix+v6),2       ;update swsprite (this takes 2 frames)

  inc (ix+v4)

  ld (ix+v7),0 ;reset timer

.animate0:

  ld    hl,spriteaddrex553 ;color address
  ld    bc,spriteaddrex553c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset

 ; ret


.animatewalkcycle:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  cp 3
  jp z,.animate2
  cp 6
  jp z,.animate1
  ret

.animate1:

  ld    hl,spriteaddrex554 ;color address
  ld    bc,spriteaddrex554c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret

.animate2:

  ld    hl,spriteaddrex555 ;color address
  ld    bc,spriteaddrex555c  ;character address
  ld    de,offsetkoopa
  ld    a,supermariospritesblock
  jp    setcharcolandoffset


;  ret


.moveegg:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,10
  xor a
  sbc hl,de
;  ld de,6
;  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,6
  add hl,de
  ld (iy+yobject),l
  ld (iy+yobject+1),h 
  
  ret


;spawn all other objects required to be on screen
.init:

;spawn the egg

  push ix

  ld hl,17
  ld e,10

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),2

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex552 ;color address
  ld    bc,spriteaddrex552c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  push ix
  pop hl

  pop ix

  ld (ix+v5),l
  ld (ix+v5+1),h ;store egg adress
;boom thing
  push ix

  ld hl,11
  ld e,12

  ld    iy,CustomObject
  ld    (iy),force28 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

;  ld (ix+objectrelease),1

  push ix
  pop hl

  pop ix

  ld (ix+offsets+25),l
  ld (ix+offsets+26),h ;save this object too

  inc (ix+v4)
  
  ld (ix+v1),-2
  
  ret




handlefuse:


  inc (ix+v7)
  ld a,(ix+v7)
  cp 14
  jp nc,destroyobject

  call removefusepart

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v7) ;use ix+v7 in this exeptional case so that we are sure that this is absolutely the first frame
  and 1
  jp nz,.animate1 

  call addv1toxbject


  ld    hl,spriteaddrex562 ;color address
  ld    bc,spriteaddrex562c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset 


.animate1:

  ld    hl,spriteaddrex563 ;color address
  ld    bc,spriteaddrex563c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset 

;do this with HMMM
removefusepart:
  
  ld a,(ix+xobject)
  and %11110000
  add a,8
  ld c,a
  ld a,(ix+yobject)
  and %11110000
  add a,16
  ld b,a ;y
  
  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,c
  sub 32
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,b
  sub 16
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,c
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,b
  out ($9b),a ;Yd  
  ld a,(sc5page)
  xor 1
  out ($9b),a ;Yd  

  ld a,16
  out ($9b),a ;Xw
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a 
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%11010000 ;HMMM
  out ($9b),a ;command  

  ei




  ret



handleboomthing:

  ld a,(ix+v4)
  and a
  ret z
  dec a
  dec a
  ret z ;after setting the newest position ;TODO: spawn the fuse and write the fuse code
  
  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex558 ;color address
  ld    bc,spriteaddrex558c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset  
  
  push ix

  ld hl,13
  ld e,12

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),229                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject

  ld (ix+objectrelease),3
  ld (ix+v1),8

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex562 ;color address
  ld    bc,spriteaddrex562c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  call    setcharcolandoffset 

  pop ix
  
  ret

copythnxtoactivepage:

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,240;thnx balloon 1
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ;xor a
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,32+1
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,64+8
  out ($9b),a ;Yd  
  ld a,(sc5page) ;active page
  xor 1
  out ($9b),a ;Yd  

  ld a,32
  out ($9b),a ;Xw  
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000  ;LMMM + TIMP
  ei
  out ($9b),a ;command  

  call pollvdpcmd


;do second part

  ;set command register
  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  ld a,240;thnx balloon 1
  out ($9b),a; Xs
  xor a
  out ($9b),a; Xs

  ld a,16
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,32+16+1
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,64+8
  out ($9b),a ;Yd  
  ld a,(sc5page) ;active page
  xor 1
  out ($9b),a ;Yd  

  ld a,32
  out ($9b),a ;Xw  
  xor a
  out ($9b),a ;Xw

  ld a,16
  out ($9b),a ;Yw
  xor a
  out ($9b),a ;Yw
  
  out ($9b),a ;color
  
  out ($9b),a ;dir
  
  ld a,%10011000 ;LMMM + TIMP
  ei
  out ($9b),a ;command  



  ret


  
handlecutsceneegg:

  ld a,(ix+v4)
  and a
  jp z,.init

  ld a,(ix+v5)
  and a
  ret z
  
  ld e,(ix+v7)
  ld d,0
  ld hl,.eggbouncetable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resetv7
  
  ld (ix+v2),a
  call addv2toybject

  inc (ix+v7)

  ret

.resetv7:

  ld (ix+v7),0
  ret

.eggbouncetable: db -1,0,-2,-2,0,2,2,0,1,10

.init:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,6
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,6
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  
  inc (ix+v4)


  ret

cutscenemovetable: db 1,1,1,1,2,1,2,1,2,2,3,2,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,2,3,2,3,2,2,2,2,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,0

