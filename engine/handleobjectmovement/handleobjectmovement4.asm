;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement4:

;start = 169


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz

  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress4 - 169 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)



;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress4
;  add   hl,de
;  ;substract minimum base adres
;  ld de,169*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress4:
  jp    MovementPattern169     ;spuugplant
  jp    MovementPattern170     ;spuugplant (bullet)
  jp    MovementPattern171     ;chuck (trowing)
  jp    MovementPattern172     ;baseball
  jp    MovementPattern173     ;tubecoverup for mario exit right out
  jp    MovementPattern174     ;tubecoverup for mario exit (upwards)
  jp    MovementPattern175     ;tic tac toe
  jp    MovementPattern176     ;powerup static
  jp    MovementPattern177     ;diagonal block (left and right)
  jp    MovementPattern178     ;diagonal block (helper)
  jp    MovementPattern179     ;mariocrusher (helper)
  jp    MovementPattern180     ;cavekoopa
  jp    MovementPattern181     ;bat (flying turd)
  jp    MovementPattern182     ;spiketop
  jp    MovementPattern183     ;football chuck
  jp    MovementPattern184     ;football
  jp    MovementPattern185     ;small ghost
  jp    MovementPattern186     ;pbox spawn
  jp    MovementPattern187     ;vinehelper ghost house (helper object)
  jp    MovementPattern188     ;coin tail object
  jp    MovementPattern189     ;small ghost (following mario)
  jp    MovementPattern190     ;moving platform (path dependant)
  jp    MovementPattern191     ;switchbox
  jp    MovementPattern192     ;green flying koopa (horizontal only)
  jp    MovementPattern193     ;blue koopa kicking shell away
  jp    MovementPattern194     ;fuzzy
  jp    MovementPattern195     ;rainbow shell
  jp    MovementPattern196     ;goomba
  jp    MovementPattern197     ;goomba (upsidedown)
  jp    MovementPattern198     ;hammer brother
  jp    MovementPattern199     ;hammer
  jp    MovementPattern200     ;truwall (for koopa)
  jp    MovementPattern201     ;goomba (winged)
  jp    MovementPattern202     ;changing powerup
  jp    MovementPattern203     ;paragoomba
  jp    MovementPattern204     ;empty



.return:
  
  ret



MovementPattern204:     ;empty

  ret



MovementPattern203:     ;paragoomba

  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  jp z,.animateparachute
  dec a
  dec a
  jp z,.land
  
  call objectinteract
  cp 4
  push af
  call z,.move2tiles
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.move2tiles
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,.destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.otherobjectdeathjump ;nextframe2 goomba does not die by the spinning cape  
  
  call mariointeract
  jp c,.nextframe


  ld a,(framecounter)
  and 7
  call z,.animate

  call addv2toybject

  call .moveparachute

  call checkobjecttilesflooronly
  ret nc
  
  ld (ix+v4),3

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex482  ;color address
  ld    bc,spriteaddrex482c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3 
  jp    setcharcolandoffset

  ;ret


.land:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h
  

  inc (ix+v7)
  ld a,(ix+v7)
  cp 16
  ret c


.transform:

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  

  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v2),0
  ld (ix+v5),0
  ld (ix+v6),0
  ld (ix+v5+1),0
  ld (ix+movepat),196

  call getmarioleftright
  jp c,.right

  ld (ix+v1),-1

  ret

.right:

  ld (ix+v1),1
  ld (ix+v3),2

  ret  
  

.destroyobjectwithcoin:  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  
  
  jp destroyobjectwithcoin 
 
 
  
.otherobjectdeathjump:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h  
  
  jp otherobjectdeathjump


.nextframe:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,.destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,.destroyobjectwithcloud

  ld a,(mariostar)
  and a
  jp nz,.setkilljump

  jp .transform


.destroyobjectwithcloud:

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h


  jp destroyobjectwithcloud


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6

  ld hl,0
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ret

.move2tiles:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  
  inc hl
  inc hl
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ret



.moveparachute:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld c,(ix+xobject)
  ld b,(ix+xobject+1)  
  
  ld de,16
  xor a
  sbc hl,de
  
  ld (iy+yobject),l
  ld (iy+yobject+1),h
  ld (iy+xobject),c
  ld (iy+xobject+1),b  



  ret



.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 dec de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),197                ;set object number (paragoomba)
  ld    (iy+6),0                ;set object number (paragoomba)

  call  activateobject


  ld (ix+v4),1 ;set parachute
  ld (ix+v3),0
  ld (ix+deadly),0
  ld (ix+clipping),0

  push ix
  pop hl

  push ix
  pop iy

  pop ix
  
  ld (ix+v5),l
  ld (ix+v5+1),h

  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h
 
  
  ld (ix+v4),2
  
  ret
  


.animateparachute:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy)
  and a
  jp z,destroyobject ;prevent chute from stucking in screen.
  

 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(iy+v3)               ;v3 is used as animation variable
  ;inc  a
  and a
  jp    z,.position0p
  dec   a
  jp    z,.position1p
  dec   a
  jp    z,.position2p
  dec   a
  jp    z,.position1p

  
  xor a
  ld (ix+v3),a
  
.position0p:
  ld    hl,spriteaddrex485  ;color address
  ld    bc,spriteaddrex485c  ;character address
  ld    de,offsetsparaleft
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1p:
  ld    hl,spriteaddrex484  ;color address
  ld    bc,spriteaddrex484c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3 
  jp    setcharcolandoffset

.position2p:
  ld    hl,spriteaddrex486  ;color address ;02
  ld    bc,spriteaddrex486c  ;character address
  ld    de,offsetspararight
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position1

  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex481  ;color address
  ld    bc,spriteaddrex481c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex482  ;color address
  ld    bc,spriteaddrex482c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3 
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex483  ;color address ;02
  ld    bc,spriteaddrex483c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;ret


powerupjumptable: db -6,-6,-5,-4,-4,-4,-4,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,0


handleboxinteractch:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox


.nopopbox:

  call getmariodistance
  cp 12
  jp nc,.handlesidetouch


  ld a,1
  ld (standingonsprite),a

  ld (spriteixvalue),ix
  
  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a

  ld (ix+v5),1
  
  call Playsfx.powerup  
  
  ld a,(currentlevel)
  cp 174 ;special1
  ret nz
  
;special stuff  
  ld a,3
  ld (nocontrol),a
  
  xor a ;VERY IMPORTANT TO DISABLE THIS!
  ld (bluepboxactive),a
  
  ;save the current coordinates of mario
  ld hl,(mariox)
  ld (hlvar),hl
  
  ;reset the framecounter
  xor a
  ld (framecounter),a
  ld (marioshell),a ;take the shell away!
  ld (marioblueblock),a
  ld (mariopbox),a
  
  ld a,1
  ld (playwinmusic),a

  ld (ix+v7),0
  ;ld (ix+v7),0
  
  
  ret 



.handlesidetouch:

  call getmarioleftright
  jp c,.sidetouchright

  ld a,1
  ld (touchside),a
  
  ld (spriteixvalue),ix
  ret

.sidetouchright:

  ld a,2
  ld (touchside),a

  ld (spriteixvalue),ix
  ret


handleboxinteractforobjectch:

    ld a,(currentlevel)
    cp 174
    ret z

;the info from the previous object is saved in iy   

;check if it is the shell going upwards

  ld a,(iy+movepat)
  cp 12
  jp z,.interact
  cp 95
  jp z,.interact
  
  ret


.interact:

  
;we check if the previous object is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  inc hl
  inc hl
  inc hl  
  xor a
  sbc hl,de
  ret c

  ld (ix+v5),1  
  
  call Playsfx.powerup
  
  ret 





MovementPattern202:     ;changing powerup

  ld a,(ix+hitpoints)
  inc a
  inc a
  ld (ix+objectrelease),a
  push af

  call .animate
  call .checkanimation

  pop af
  ld b,a
  
;todo: set stuck code here  
  ld a,(ix+v5)
  and a
  jp z,.waitforinteract
  dec a
  jp z,.poppowerup
  
  
  push bc
  call mariointeract
  pop bc
  jp c,settransform

  call addv1toxbject
  call addv2toybject

  ld a,(currentlevel)
  cp 53 ;this map gives me no choice... :(
  jp z,.special

  ld a,(ix+v4)
  and a
  jp nz,.checkfloor


  ld e,(ix+v7)
  ld d,0
  ld hl,powerupjumptable
  add hl,de
  ld a,(hl)
  and a
  call z,.resetv7

  ld (ix+v2),a


  call checkobjecttilescheckwall
  call c,.wallfound
 
  inc (ix+v7)
  ld (ix+deadly),3

  ret

.special:

  ld (ix+deadly),3

  call checkobjecttilescheckwall
  call c,.wallfound

  jp .checkfloor
  ;ret


.poppowerup:
  
  ld (ix+v2),-1
  call addv2toybject
  
  ld a,(currentlevel)
  cp 174
  jp z,.poppowerupspecial ;cheap level specific hack :P
  
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 36;24
  ret c
  
  ld (ix+v1),2
  ld (ix+v2),0
  ld (ix+v7),0
  inc (ix+v5)
 
  
  ret
  
.poppowerupspecial:
  
  ld (ix+v2),-1
  call addv2toybject
  
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 24
  ret c
  
  ld (ix+v1),2
  ld (ix+v2),0
  ld (ix+v7),0
  inc (ix+v5)
 
  
  ret



.waitforinteract:


  ;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobjectch
;  call z,handleboxinteract.popbox



  call mariointeract
  call c,handleboxinteractch




  ret



.wallfound:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ret

.checkanimation:

  inc (ix+v6)
  ld a,(ix+v6)
  cp 30
  ret c

  ld a,(ix+hitpoints)
  inc a
  and 3
  ld (ix+hitpoints),a

  ld (ix+v6),0

  ret

.checkfloor:

  call checkobjecttilesfloor
  call nc,.resetv74

  call checkobjecttilescheckwall
  call c,.wallfound

  ret


.resetv74:

  ld (ix+v4),0
  ld (ix+v7),0
  xor a
  ret


.resetv7:

  ld (ix+objectfp),0
  ld (ix+v7),0 
  ld (ix+v4),1
  xor a
  ret



.animate:


  ld a,(ix+hitpoints)
  and a
  jp z,.animatemushroom
  dec a
  jp z,.animateflower
  dec a
  jp z,.animatefeather
  dec a
  jp z,.animatestar




  ret


.animatemushroom:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex20  ;color address ;18 
  ld    bc,spriteaddrex20c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.animatestar:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex28  ;color address
  ld    bc,spriteaddrex28c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex28  ;color address
  ld    bc,spriteaddrex28c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex29  ;color address ;02
  ld    bc,spriteaddrex29c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex29  ;color address ;03
  ld    bc,spriteaddrex29c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.animatefeather:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex22  ;color address ;18 
  ld    bc,spriteaddrex22c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.animateflower:
 
  ld a,(framecounter)
  and 3
  ret nz
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0f
  dec   a
  jp    z,.position1f
  dec   a
  jp    z,.position2f
  dec   a
  jp    z,.position3f
  
  xor a
  ld (ix+v3),a
  
.position0f:
  ld    hl,spriteaddrex24  ;color address
  ld    bc,spriteaddrex24c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position1f:
  ld    hl,spriteaddrex25  ;color address
  ld    bc,spriteaddrex25c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2f:
  ld    hl,spriteaddrex26  ;color address ;02
  ld    bc,spriteaddrex26c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3f:
  ld    hl,spriteaddrex27  ;color address ;03
  ld    bc,spriteaddrex27c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;ret


wingedgoombajumpsteps:
  
  call shelluptilecheck.noblockcheck
  jp c,.reset
  
  inc (ix+hitpoints)
  ld a,(ix+objectrelease)
  dec a
  jp z,.step1
  dec a
  jp z,.wait
  dec a
  jp z,.step1
  dec a
  jp z,.wait  
  dec a
  jp z,.step1
  dec a
  jp z,.wait  
  dec a
  jp z,.step2    
  dec a
  jp z,.reset   
  

  ld a,(ix+hitpoints)
  cp 35
  ret c
  
  call  checkobjecttilesflooronly
  ret nc  
  

  inc (ix+objectrelease) ;set first step
  ld (ix+hitpoints),0

  ret

.step2:

  ld hl,wgoombajumptablelarge
  jp .jump


.step1:

  ld hl,wgoombajumptablesmall

.jump:
  ld (ix+objectfp),0

  ld a,(ix+hitpoints)
  dec a
  ld e,a
  ld d,0
 ; ld hl,wgoombajumptablesmall
  add hl,de
  ld a,(hl)
  and a
  jp z,.next
  
  ld (ix+v2),a
  
  ret




.reset:

  ld (ix+hitpoints),0 ;temp reset
  ld (ix+objectrelease),0



  ret

.next:

  ld (ix+hitpoints),0
  inc (ix+objectrelease)


  ret

.wait:

  ld a,(ix+hitpoints)
  cp 6
  ret c
  
  jp .next


;  ret


wgoombajumptablesmall: db -3,-2,-2,-2,-1,0
wgoombajumptablelarge: db -6,-6,-5,-4,-4,-4,-4,-3,-3,-3,-2,-2,-2,-1,0


MovementPattern201:     ;goomba (winged)
;hitpoints is the main timer
;objectrelease is the stepcounter


  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,.animatehammerwings


  call objectinteract
  cp 4
  push af
  call z,MovementPattern196.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,MovementPattern196.wallfound
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,.destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;nextframe2 goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call wingedgoombajumpsteps  
  
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  call .movegoombawings

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern196.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,MovementPattern196.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.setshootup:

  call .clearwings

  jp MovementPattern196.setshootup 

.animatehammerwings:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld a,(iy+objectrelease)
  and a
;  ret z
  jp z,.resetposition
  cp 7
  jp z,animatehammerwings.noframecheck
    


  jp animatehammerwings

.resetposition:

  ld a,(framecounter)
  and 15
  ret nz


    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex472  ;color address
  ld    bc,spriteaddrex472c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex472  ;color address
  ld    bc,spriteaddrex472c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex474  ;color address ;02
  ld    bc,spriteaddrex474c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex474  ;color address ;03
  ld    bc,spriteaddrex474c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


  


.movegoombawings:

  ld hl,(mapheightx8)
  ld de,16
  xor a
  sbc hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.setallzero


  ld l,(ix+v5) ;left
  ld h,(ix+v5+1)
  push hl
  pop iy
 
  ld a,(iy+movepat)
  cp 201
  jp nz,.setallzero 
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) 
 
  ld de,8
  xor a
  sbc hl,de
  push hl
  pop bc
 
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)

  
  ld de,12
  xor a
  sbc hl,de
  
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b
  
  ld l,(ix+v6) ;right
  ld h,(ix+v7)
  push hl
  pop iy  
  
  ld a,(iy+movepat)
  cp 201
  jp nz,.setallzero  
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  
  ld de,12
  add hl,de
  
  
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b  
  
  ret

.setallzero:

  ld bc,0
  ld hl,0
  
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+yobject),c
  ld (ix+yobject+1),b

  ld e,(ix+v6) ;right
  ld d,(ix+v7)
  push de
  pop iy  
 
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b    
  
  ld e,(ix+v5) ;left
  ld d,(ix+v5+1)
  push de
  pop iy  

  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b  
  
  
  ret

.destroyobjectwithcoin:

  call .clearwings
  
  jp destroyobjectwithcoin
  


.otherobjectdeathjump:

  call .clearwings

  push ix
  call otherobjectdeathjump
  pop ix

  ret


.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,.destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,.destroyobjectwithcloud

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a

    call .clearwings


  ld (ix+movepat),196
  ld (ix+v7),0
  ld (ix+objectfp),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v4),0


  call Playsfx.kickenemy

  jp makestarleft


.destroyobjectwithcloud:

  call .clearwings
  jp destroyobjectwithcloud


.clearwings:
;clear the wings
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld (iy+xobject),0
  ld (iy+xobject+1),0
  ld (iy+yobject),0
  ld (iy+yobject+1),0


  ld l,(ix+v6)
  ld h,(ix+v7)
  push hl
  pop iy
  
  ld (iy+xobject),0
  ld (iy+xobject+1),0
  ld (iy+yobject),0
  ld (iy+yobject+1),0

  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
 
  jp .clearwings
  
  ;ret


.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),195                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),2 ;set wings
  ld (ix+v3),0
  ld (ix+deadly),0
  ld (ix+clipping),0

  call animatehammerwings.noframecheck

  push ix
  pop hl

  push ix
  pop iy

  pop ix

  ld (ix+v5),l
  ld (ix+v5+1),h

  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h
  

  pop de
  pop hl



;second wing
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),195                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),2 ;set wings
  ld (ix+v3),2
  ld (ix+deadly),0
  ld (ix+clipping),0

  call animatehammerwings.noframecheck

  push ix
  pop hl

  push ix
  pop iy


  pop ix

  ld (ix+v6),l
  ld (ix+v7),h

  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h

  
  inc (ix+v4)

  ld (ix+hitpoints),0 ;use this as timer


  ret



MovementPattern200:     ;truwall (for koopa)

  call objectinteract
  
  ld a,(iy+movepat)
  cp 73
  jp nz,.nextcheck
  
  ld (iy+v7),1
  
  ret
.nextcheck:
  cp 74
  ret nz
  
  ld (iy+v7),1
  



  ret



MovementPattern199:     ;hammer  


  ld a,(ix+v4)
  and a
  jp z,.init


  ld a,(framecounter)
  and 3
  call z,.animate


  call mariointeract


  call addv1toxbject
  call addv2toybject


  ld e,(ix+v7)
  ld d,0
  ld hl,hammerfalltable
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.maxspeed

  ld (ix+v2),a
  inc (ix+v7)


  ret

.maxspeed:

  ld (ix+v2),8

  ret



.init:

  inc (ix+v4)

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v5+1)
  and a
  jp nz,.right

  ld (ix+v1),-1

  ld    hl,spriteaddrex477  ;color address
  ld    bc,spriteaddrex477c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

 ; ret

.right:

  ld (ix+v1),1

  ld    hl,spriteaddrex478  ;color address
  ld    bc,spriteaddrex478c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

 ; ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v3)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  and 3
  jp    z,.position0u
  cp 1
  jp    z,.position1u
  cp 2
  jp    z,.position2u
  cp 3
  jp    z,.position3u

.position0u:
  ld    hl,spriteaddrex477  ;color address
  ld    bc,spriteaddrex477c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1u:
  ld    hl,spriteaddrex478  ;color address
  ld    bc,spriteaddrex478c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2u:
  ld    hl,spriteaddrex479  ;color address ;02
  ld    bc,spriteaddrex479c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3u:
  ld    hl,spriteaddrex480  ;color address ;03
  ld    bc,spriteaddrex480c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
  ;ret


hammerfalltable: db -2,-2,-1,0,1,1,1,2,2,2,2,3,3,4,4,5,6,7,8,10


MovementPattern198:     ;hammer brother
;using v4 to identify the part
;0 = startpart
;1 = platform
;2 = wings
;3 = hammer brother
;4 = main pattern movement control
;5 = check object


  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,animatehammerwings
  cp 3
  jp z,handlehammerbrother

  
  ld a,(ix+v7)
  and a
  call nz,.handlepopboxanimation
  
  ld c,0
  call platforminteract
  
  call mariointeract
  call c,handleboxinteract2
 

;all global control above this line

  ld a,(ix+v4)
  cp 4
  ret nz

;all main object Controls below this line

  call .fetchb

;  ld b,5;6 ;fetch this from a table
  
  
  ld hl,rotatehammerrx
  ld de,rotatehammerry
  
  ld a,(ix+v6)
  and a
  call nz,.left
  
  
  call rotateobject

  ld a,(ix+hitpoints)
  and a
  call nz,.changedir


  jp movehammerobjects

 ; ret

.left:

  ld hl,rotatehammerlx
  ld de,rotatehammerly  
  
  ret
  


.changedir:

  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a
  
  ld (ix+v5),0
  ld (ix+v5+1),0
  
  ld (ix+hitpoints),0
  
  ret


.fetchb:

  ld a,(ix+v5)
  cp 225
  jp nc,.bdec
;  ld a,(ix+v5)
  cp 30
  jp nc,.bmax
  
  ld e,a
  ld d,0
  ld hl,.acctable
  add hl,de
  ld a,(hl)
  
  ld b,a


  ret

.bdec:

  ld e,225
  sub e

  ld e,a
  ld d,0
  ld hl,.acctable+30
  xor a
  sbc hl,de
  
  ld a,(hl)
  
  ld b,a

  ret

  
.bmax:

    ld b,6
    ret
 
.acctable: db 1,2,1,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,5,5,5  

.handlepopboxanimation:

  ld a,(ix+v7)
  cp 2
  jp z,.step1
  cp 3
  jp z,.step1
  cp 4
  jp z,.reset


  ld (ix+v2),-4
  
  call addv2toybject
  inc (ix+v7)

  ret

.step1:

  ld (ix+v2),2
  
  call addv2toybject
  inc (ix+v7)

  ret

.reset:

  ld (ix+v7),0


  ret





.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force20 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),192                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),3
  ld (ix+v3),0
  ld (ix+deadly),1

  ld b,8
  call increasespritecount

  push ix
  pop hl


  pop ix
  
  
  ld (ix+offsets+22),l ;save ix adress in main control object
  ld (ix+offsets+23),h
  
  pop de
  pop hl

  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force31 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),192                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),2;set wings

  push ix
  pop hl


  pop ix
  
  
  ld (ix+offsets+18),l ;save ix adress in main control object
  ld (ix+offsets+19),h

  pop de
  pop hl



  push hl
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force08 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),192                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),2;set wings
  ld (ix+v3),2

  push ix
  pop hl


  pop ix
  
  
  
  ld (ix+offsets+20),l ;save ix adress in main control object
  ld (ix+offsets+21),h

  pop de
  pop hl  

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force29 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),192                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+v4),1;set platform
  ld (ix+v3),0


  push ix
  pop hl


  pop ix
  
  
  ld (ix+offsets+16),l ;save ix adress in main control object
  ld (ix+offsets+17),h



  ld (ix+v4),4
  ld (ix+v3),0
  ld (ix+objectfp),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v2),0
  ld (ix+v1),0
  ld (ix+v7),0
  ld (ix+v6),1
 
   ld (ix+hitpoints),0
 
  
;  exx
;  ld c,0
;  ld hl,0
;  ld de,0
;  exx
  
  ret


animatehammerwings:


  ld a,(framecounter)
  and 3
  ret nz
 
.noframecheck: 
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex471  ;color address
  ld    bc,spriteaddrex471c  ;character address
  ld    de,offsetwingcloseleft
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex472  ;color address
  ld    bc,spriteaddrex472c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex473  ;color address ;02
  ld    bc,spriteaddrex473c  ;character address
  ld    de,offsetwingcloseright
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex474  ;color address ;03
  ld    bc,spriteaddrex474c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret


handlehammerbrother:
;use v5 as timer here



  call mariointeract
  jp c,.otherobjectdeathjump


  ld a,(framecounter)
  and 7
  jp z,.animate
  
  
  ld a,(ix+v7)
  and a
  jp nz,.otherobjectdeathjump
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 30
  ret c
  
  ;spawn the hammers

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),192                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+objectfp),0
;  ld (ix+v4),0
 ; ld (ix+v3),0
  ld (ix+deadly),2
  ld (ix+movepat),199
  
  push ix
  pop iy

  pop ix

  ld (ix+v5),0
  ld a,(ix+v5+1)
  xor 1
  ld (ix+v5+1),a
  
  
  ld (iy+v5+1),a
  
  ret
  
.otherobjectdeathjump:


  ld (ix+v6),1 ;tells the main object that this one is over


  ld (ix+objectfp),0

     ld    (ix+updatespr?),2
  ld (ix+objectreturn),1


  ld (ix+deathtimer),0
  ld (ix+movepat),6

  ld (ix+clipping),0

  ld a,b ;snelheid van wat het object had geraakt
  ;moet nog 3 vanaf
  rlca
  jp c,.left
 
  ld (ix+v1),3
 ret
  
  
;  ret
  
.left:

    ld (ix+v1),-3
   ret
    
   
  
  
  
.animate:  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,spriteaddrex475  ;color address
  ld    bc,spriteaddrex475c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex476  ;color address
  ld    bc,spriteaddrex476c  ;character address
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;ret





movehammerobjects:

  

  ld l,(ix+offsets+18)
  ld h,(ix+offsets+19)
  
  
  push hl
  pop iy
  
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld c,(ix+yobject)
  ld b,(ix+yobject+1)
 
  ld a,(iy+movepat)
  cp 198
  call nz,.moveallobjectsoutofscreen
 
  
  
  push hl

  xor a
  ld de,16-4
  sbc hl,de
  
  
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b

  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  
  push hl
  pop iy 

  pop hl

  ld a,(iy+movepat)
  cp 198
  call nz,.moveallobjectsoutofscreenright 


  push hl

  ld de,32-4
  add hl,de

  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b



  ld l,(ix+offsets+16)
  ld h,(ix+offsets+17)
  
  push hl
  pop iy

  pop hl 
  push hl
  
  ld de,16
  add hl,de
  
  ld a,(ix+v2)
  ld (iy+v2),a ;do for platforminteract
  ld a,(ix+v1)
  ld (iy+v1),a
  
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b



  pop hl
  push hl

  ld l,(ix+offsets+22) ;hammerbrother
  ld h,(ix+offsets+23)
  
  push hl
  pop iy 

  ld a,(iy+v6)
  and a
  call nz,.disablebrotherchecks

  pop hl 

  ld a,(ix+offsets+25)
  and a
  jp nz,.checkkillhammer

  ld (iy+xobject),l
  ld (iy+xobject+1),h
  
  push bc
  pop hl
  
  ld de,32
  xor a
  sbc hl,de
  
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ;bc =0?
  push bc
  pop hl
  ld de,0
  xor a
  sbc hl,de
  jp z,.killhammer

  
  


; parse info on v7 into hammerbrother
  ld a,(ix+v7)
  ld (iy+v7),a
  
  and a
  jp nz,.disablebrotherchecks
  
  ld l,(ix+offsets+16)
  ld h,(ix+offsets+17)
  
  push ix
  
  push hl
  pop ix

  ld a,(ix+v7)
  ld (iy+v7),a    

  pop ix
 
  and a
  jp nz,.disablebrotherchecks 
  

  ret

.checkkillhammer:
  ;bc =0?
  push bc
  pop hl
  ld de,0
  xor a
  sbc hl,de
  jp z,.killhammer

  ret

.killhammer:

  ld (ix+movepat),0
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v6),1
  ld (ix+v7),0
  
;  exx
;  ld c,0
;  ld hl,0
;  exx
  

  jp z,destroyobject.objectreturns

 ; ret



.disablebrotherchecks:

  ld (ix+offsets+25),1


  ret


.moveallobjectsoutofscreen:

  ld hl,0
  ld bc,0
  
  ret
  
.moveallobjectsoutofscreenright:
  
  push iy
  
  ld l,(ix+offsets+18) ;left wing
  ld h,(ix+offsets+19)
  
  
  push hl
  pop iy


  ld hl,0
  ld bc,0
 
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  ld (iy+yobject),c
  ld (iy+yobject+1),b

 
  pop iy
  
  ret

  
  

handleboxinteract2:


;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  ret z
  
.popbox:


  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

  ld a,1
  ld (jumpingupsprite),a
  
  ld (ix+v7),1
 
  ret


handleboxinteractforobject2:


;the info from the previous object is saved in iy   


.interact:

  
;we check if the previous object is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  inc hl
  inc hl
  inc hl  
  xor a
  sbc hl,de
  ret c

  ld (ix+v7),1 ;pop the box
  
  


  ret




kickgoomba:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
 ; cp 1
 ; jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 3 frames
  call  z,MovementPattern197.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

  call .getgoombatabledata
  
  
  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret  

      
;Depcarated??
;.otherobjectdeathjump:
;
;  ld (ix+v5),1
;
;  ret
  
;reverse direction of goomba  
.wallfound:


  ld a,(ix+v6)
  xor 1
  ld (ix+v6),a
  
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation
  
  ret




.getgoombatabledata:

  ld hl,kickgoombatablex
  ld d,0
  ld e,(ix+v7)
  add hl,de
  ld a,(hl)
  and a
  jp z,.turnback

  ld c,a

  ld hl,kickgoombatabley
  add hl,de ;get the bounce
  ld a,(hl)
  ld (ix+v2),a

  call addv2toybject

  inc (ix+v7)

  ld a,(ix+v6)
  and a
  jp nz,.right

  
  ld a,c
  neg
  ld (ix+v1),a


  ret

.right:

  ld (ix+v1),c


  ret



.turnback:

  ld (ix+v1),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1
  
  ret


kickgoombatablex: db 4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,0
kickgoombatabley: db -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,0



holdgoomba:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 250
  jp nc,.turnback


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern197.animate


;omdraaien
  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell

;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right


  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ret c ;prevent exiting from the left side of the screen
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
;  ld hl,(mariox)
;  ld de,10
;  xor a
;  sbc hl,de
;  ld (ix+xobject),l
;  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeleft

  ret

.right:

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ret



.turnshell:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
;  jp z,.nextframe
  


  ret

.turnshellright:

  
  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,0
  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ret



.turnback:

  ld (ix+deadly),1
  ld (ix+movepat),196
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v1),-1
  ld (ix+v3),0 ;reset animation

  xor a
  ld (marioshell),a

  ret

.followmario:

  ld a,(ix+v6) ;reverse direction
  xor 1
  ld (ix+v6),a
  
  call followmario
  ret


.nextframeleft:


  ld (ix+v7),0
  ld (ix+v6),0 ;left
  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


  jp .nextframe

.nextframeright:

  ld (ix+v7),0
  ld (ix+v6),1 ;right

  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 191
  call nc,.followmario


.nextframe:


  ld a,(marioturnshell)
  and a
  ret nz


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a
  
  ld (ix+v7),0
;  ld (ix+deadly),1
  ;ld (ix+deathtimer),0
  ld (ix+objectfp),0
  
  ld (ix+clipping),2

  call Playsfx.kick

;branch to kickup from here if mario is looking up
  ld a,(mariolook)
  and a
  jp nz,.setshootup

  ld (ix+v4),4

  ret


.setshootup:

  ld (ix+v4),2
  
  call objectinteract
  cp 8
  call z,.shootupharder

  
  call getmarioleftright
  jp c,.setshootupleft
  
  ld a,(mariospeed)
  ld (ix+v1),a ;migrate mariospeed fuck mariospeed is always positive!  
  
  
  ret


.shootupharder:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,32
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ret

.setshootupleft:

  ld a,(mariospeed)
  neg
  ld (ix+v1),a



  ret


shootgoombaup:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  cp 8
  jp z,.nextstepbox
  
  ld a,(ix+v7)
  cp 8
  call nc,.mariointeract ;delay the interact otherwise mario will do strange shit
 
;  call checkmariointeractshoot
;  jp c,MovementPattern197.setshootup ;goomba does not die by the spinning cape 
 
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern197.animate

  call addv1toxbject
  call addv2toybject

    
  ld a,(ix+v2)
  ld c,a
  push bc
  neg
  ld (ix+v2),a
  call shelluptilecheck
  pop bc
  ld (ix+v2),c
  jp c,.nextstep


  call .getgoombatabledata
  

;todo: check for uptiles
  
;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

;  call  checkobjecttilesfloor
;  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation



  ret


.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.mariointeract:


  call mariointeract
  jp c,MovementPattern197.nextframe

  ret


.getgoombatabledata:

  ld a,(ix+v2) ;check for slowdown
  cp -7
  call z,.settablefar


  ld e,(ix+v7)
  ld hl,goombakickuptabley
  ld d,0
  add hl,de
  inc (ix+v7)
  ld a,(hl)
  and a
  jp z,.nextstep
  
  ld (ix+v2),a
  
  
  
  ret


.nextstep:

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v2),0
  ld (ix+v6),0
  ld (ix+v7),0

  ret

.nextstepbox:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v4),3
  ;ld (ix+clipping),1 ;temp
  ld (ix+objectfp),0 
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+v2),0

  ld a,(iy+v7)
  and a
  ret nz

  ld (iy+v7),1

  ret


.settablefar:

  ld (ix+v7),12

  ret

goombakickuptabley: db -10,-10,-10,-10,-10,-10,-10,-10,-9,-9,-8,-7,-6,-4,-2,-1,0



goombacommingdown:

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern197.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern197.setshootup ;goomba does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern197.animate

  call addv1toxbject
  call addv2toybject

  ld (ix+v2),0

;todo: check for uptiles
  
  call  checksheerobjecttiledown
  jp c,.setbounce
  call checksheerobjecttile ;check for sheer tiles
  jp c,.setbounce
  
  call  checkobjecttilesfloor
  jp nc,.setbounce

  ld a,(ix+standingonspr)
  and a
  jp nz,.setbounce

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ld a,(ix+v2)
  cp 6
  jp z,.maxspeed ;falling is a little bit faster for goomba


  ret
      

.otherobjectdeathjump:

  ld (ix+v5),1

  ret

.maxspeed:

  ld (ix+v2),8

  ret

.wallfound:

  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a ;reverse animation

  ld a,(ix+v1)
  neg
  ld (ix+v1),a

  ret


.setbounce:

;use the kick code when it has movement in air

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),0 ;when kicking the goomba flies less
  ld (ix+objectfp),0

;compensate the speed
  ld a,(ix+v1)
  bit 7,a
  call nz,.negate
  and a
  jp z,.bounce
  cp 3
  call z,.set8
  cp 2
  call z,.set15
  cp 1
  call z,.set21


  call getmarioleftright
  jp c,.kickright

  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret

.negate:

  neg

  ret

.set8:

  ld (ix+v7),8

  ret

.set15:

  ld (ix+v7),15

  ret
  
.set21:

  ld (ix+v7),21

  ret


.bounce:

  ld (ix+v4),5
  ld (ix+v6),0 ;use v6 as bouncestep indicator
  ld (ix+v7),0


  ret


;todo
goombabounce:


  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  jp z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call mariointeract
  jp c,MovementPattern197.nextframe  


  call checkmariointeractshoot
  jp c,MovementPattern197.setshootup ;goomba does not die by the spinning cape
 
  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,MovementPattern197.animate

  call addv2toybject

  ld a,(ix+v6)
  and a
  jp z,.step1
  cp 2
  jp z,.step2
  cp 4
  jp z,.step3
  cp 6
  jp z,.end


  call  checkobjecttilesfloor
  jp nc,.nextstep

  ld a,(ix+standingonspr)
  and a
  jp nz,.nextstep

  ret

.step1:

  ld e,(ix+v7)
  ld d,0
  ld hl,goombabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  ld (ix+v2),a
  ret


.step2:

  ld e,(ix+v7)
  ld d,0
  ld hl,goombabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret

.step3:

  ld e,(ix+v7)
  ld d,0
  ld hl,goombabouncetable
  add hl,de
  ld a,(hl)
  and a
  jp z,.nextstep

  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  inc (ix+v7)
  ld (ix+v2),a
  ret


.nextstep:

  inc (ix+v6)
  ld (ix+v7),0
  ld (ix+v2),0
  ld (ix+objectfp),0
  
  ret


.end:

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+clipping),1

  ret

goombabouncetable: db -4,-3,-2,-2,-1,-1,-1,0,0


MovementPattern197:     ;goomba (upsidedown)

;  ld a,(ix+v5)
;  and a
;  jp nz,.dodeathjump


  ld a,(ix+v4)
  cp 1
  jp z,holdgoomba
  cp 4
  jp z,kickgoomba
  cp 2
  jp z,shootgoombaup
  cp 3
  jp z,goombacommingdown
  cp 5
  jp z,goombabounce

  inc (ix+v7)
  ld a,(ix+v7)
  cp 251 ;must always be higher than holdgoomba
  jp nc,.turnback


  call addv1toxbject
  call addv2toybject
;using v4 as status pointer
;1 = hold
;2 = trown up
;3 = comming down
;4 = kicked away
;5 = bouncing

  call objectinteract
;  cp 4
;  jp z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  jp z,.kickdirect
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe



  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

  call  checkobjecttilesfloor
  
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation


  ret


.dodeathjump:
  
  call addv1toxbject
  call addv2toybject
 
    ld (ix+deathtimer),0
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 4
  ret c
  
  jp otherobjectdeathjump

.setshootup:

  ld (ix+v4),2
  ld (ix+v7),0
  ld (ix+clipping),2
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret


.nextframe:


  ld a,(marioflyslide)
  dec a
  jp z,MovementPattern196.setkilljump  
 
;  ld a,(movedownsheertile)
;  dec a
;  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

  


  ld a,(Controls)
  bit 5,a
  jp z,.kick


 ld a,(marioyoshi)
 dec a
 ret z


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a

  ld (ix+v4),1 ;hold
  ld (ix+clipping),0 ;make deadly

  ret

.kick:


;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

.kickdirect: ;kick around without the use of mario

  ld (ix+v4),4 ;kick
  ld (ix+clipping),2 ;make deadly
  ld (ix+v7),8 ;when kicking the goomba flies less
  ld (ix+objectfp),0  


  call getmarioleftright
  jp c,.kickright


  ld (ix+v6),1

  ret 
  

.kickright:

  ld (ix+v6),0
  ret



.turnback:

  ld (ix+deadly),1
  ld (ix+clipping),1
  ld (ix+movepat),196
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v1),-1
  ld (ix+v3),0 ;reset animation


  ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0u
  dec   a
  jp    z,.position1u
  dec   a
  jp    z,.position2u
  dec   a
  jp    z,.position3u

.position0u:
  ld    hl,spriteaddrex467  ;color address
  ld    bc,spriteaddrex467c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1u:
  ld    hl,spriteaddrex468  ;color address
  ld    bc,spriteaddrex468c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2u:
  ld    hl,spriteaddrex469  ;color address ;02
  ld    bc,spriteaddrex469c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3u:
  ld    hl,spriteaddrex470  ;color address ;03
  ld    bc,spriteaddrex470c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret



MovementPattern196:     ;goomba

  call objectinteract
  cp 4
  push af
  call z,.wallfound
  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  push af
  call z,.wallfound
  pop af
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,.setshootup ;nextframe2 goomba does not die by the spinning cape


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.setshootup:

  call Playsfx.kick

  ld (ix+v4),2
  ld (ix+v2),0
  ld (ix+v7),0
  ld (ix+clipping),2
  ld (ix+deadly),0
  ld (ix+movepat),197
  
  call getmarioleftright
  jp c,.setshootupright

  ld (ix+v1),2

  ret

.setshootupright:

  ld (ix+v1),-2

  ret

.otherobjectdeathjump:

  push ix
  call otherobjectdeathjump
  pop ix


.animateupsidedown:
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
;  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0u
  dec   a
  jp    z,.position1u
  dec   a
  jp    z,.position2u
  dec   a
  jp    z,.position3u

.position0u:
  ld    hl,spriteaddrex467  ;color address
  ld    bc,spriteaddrex467c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1u:
  ld    hl,spriteaddrex468  ;color address
  ld    bc,spriteaddrex468c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2u:
  ld    hl,spriteaddrex469  ;color address ;02
  ld    bc,spriteaddrex469c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3u:
  ld    hl,spriteaddrex470  ;color address ;03
  ld    bc,spriteaddrex470c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret


.wallfound:


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex463  ;color address
  ld    bc,spriteaddrex463c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex464  ;color address
  ld    bc,spriteaddrex464c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex465  ;color address ;02
  ld    bc,spriteaddrex465c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex466  ;color address ;03
  ld    bc,spriteaddrex466c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a


  ld (ix+deadly),0
  ld (ix+movepat),197
  ld (ix+v1),0 ;stop any movement
  ld (ix+v7),0

  call Playsfx.kickenemy

  jp .animateupsidedown

;  ret




.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



MovementPattern195:     ;rainbow shell
;good news is that the objectrelease doesn't matter so we can use the animation pointer per frame :)
  call objectinteract
  cp 3
  jp z,otherobjectdeathjump
;  cp 5
;  jp z,destroyobjectwithcoin

;  call checkmariointeractshoot
;  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  ld a,(framecounter)
  and 3
  call z,.animate


  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c

  ld (ix+v7),16


  call addv1toxbject
  call addv2toybject

  call acceleraterainbowshell
  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  ret


.nextframe:

  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
  ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud

  

  ret


.wallfound:

  ;change direction
  ld    a,(ix+v4)               ;v1 is movement speed and direction
  cp 11
  ret z
  jp c,.toright

  call getmarioleftright
  ret nc


  ld (ix+v1),-4
  ld (ix+v4),1

  call Playsfx.stomp

  ret

.toright:

  call getmarioleftright
  ret c


  ld (ix+v1),4
  ld (ix+v4),21

  ret


.animate:

  ld a,(ix+objectrelease)
  inc a
  and 7
  ld (ix+objectrelease),a
  
  call animateshell
  
  ret



acceleraterainbowshell:


  call getmarioleftright
  jp c,.acceleratemoleright


.acceleratemoleleft:

  ld a,(ix+v4)
  and a
  ret z
  
  dec (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,rainbowshellacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a



  ret


.acceleratemoleright:

  ld a,(ix+v4)
  cp 22
  ret z
  
  inc (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,rainbowshellacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a



  ret



rainbowshellacctable:   db -4,-3,-2,-1,-2,-1,-2,-1,-1,-1,0,0,0,1,1,1,2,1,2,1,2,3,4

  


MovementPattern194:     ;fuzzy


  ld a,(ix+v4)
  and a
  jp z,.init


  call mariointeract

  ld a,(framecounter)
  and 3
  jp z,.animate  


  ld a,(ix+v4)
  cp 2
  jp z,.rotatesmall
  cp 3
  ret z
  cp 4
  jp z,.moveoctal
  cp 5
  jp z,.movetetra

  ld b,4
  ld hl,smallrotatex
  ld de,smallrotatey
  call rotateobject  


  ret

.movetetra: ;for maplevel6-03a

  call addv1toxbject
  call addv2toybject

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 1+8
  jp z,.step3
  cp 1+8+8
  jp z,.step5
  cp 1+8+8+8
  jp z,.step7
  cp 1+8+8+8+8
  jp z,.reset
  
  
    ret

.moveoctal:


  call addv1toxbject
  call addv2toybject

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 1+8
  jp z,.step2
  cp 1+8+8
  jp z,.step3
  cp 1+8+8+8
  jp z,.step4
  cp 1+8+8+8+8
  jp z,.step5
  cp 1+8+8+8+8+8
  jp z,.step6
  cp 1+8+8+8+8+8+8
  jp z,.step7
  cp 1+8+8+8+8+8+8+8
  jp z,.step8
  cp 1+8+8+8+8+8+8+8+8
  jp z,.reset


  ret


.step1:

  ld (ix+v1),4
  ld (ix+v2),0

  ret

.step2:

  ld (ix+v1),4
  ld (ix+v2),4

  ret
  
.step3:

  ld (ix+v1),0
  ld (ix+v2),4

  ret

.step4:

  ld (ix+v1),-4
  ld (ix+v2),4

  ret

.step5:

  ld (ix+v1),-4
  ld (ix+v2),0

  ret

.step6:

  ld (ix+v1),-4
  ld (ix+v2),-4

  ret

.step7:

  ld (ix+v1),0
  ld (ix+v2),-4

  ret

.step8:

  ld (ix+v1),4
  ld (ix+v2),-4

  ret


.reset:

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v7),0

  ret


.rotatesmall:

  ld b,6
  ld hl,verysmallrotatex
  ld de,verysmallrotatey
  call rotateobject 



  ret



.init:

  inc (ix+v4)

  ld a,(currentlevel) ;cheesbride 3
  cp 77
  jp z,.setrotatesmall
  cp 78
  jp z,.setrotatesmall
  cp 117
  jp z,.setmovetetra

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,2616+7
  xor a
  sbc hl,de
  jp z,.centreandprep


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,2615
  xor a
  sbc hl,de
  jp c,.setrotatesmall

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,3000
  xor a
  sbc hl,de
  jp nc,.setoctalmovement


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,2630
  xor a
  sbc hl,de
  jp nc,.setnomovement



  ret

.setmovetetra:

  ld (ix+v4),5
  ld (ix+v7),0
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  ret

.setoctalmovement:

  ld (ix+v4),4
  ld (ix+v7),0
  ret


.setnomovement:

  ld (ix+v4),3
  ret


.setrotatesmall:

  ld (ix+v4),2
  ret



.centreandprep:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  
  ld hl,125
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  ret
  



.animate:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and 1
  jp z,.position1

  ld    hl,spriteaddrex446  ;color address
  ld    bc,spriteaddrex446c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position1:

  ld    hl,spriteaddrex447  ;color address
  ld    bc,spriteaddrex447c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

;ret  


MovementPattern193:     ;blue koopa kicking shell away


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)


  ld a,(ix+v4)
  cp 1
  jp z,.kickleft
  cp 2
  jp z,.endkick
  cp 3
  jp z,.kickright
 
  
  ld a,(ix+v3)
  and 2
  jp nz,.setkickright
 
 
;  ret
 
.setkickleft:

  ld (ix+v4),1
  ld (ix+v7),0
  
  ld de,16
  xor a
  sbc hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex37  ;color address
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;  ret
 
  
.kickleft:
  
  ld a,(ix+v7)
  cp 15
  inc (ix+v7)
  ret c
  
  
   ld a,7
  ld (iy+movepat),a
 
  ld (iy+deathtimer),0 

  xor   a                       ;reset vertical movement speed
  ld    (iy+v2),a               ;v2 is vertical movement speed and direction 
  
    call Playsfx.kick
  

;schelpje word nu dodelijk

  ld (iy+deadly),1
  ld (iy+clipping),2

  ld a,-horshellspeed
  ld (iy+v1),a

  ld (ix+v4),2
  ld (ix+v7),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex38  ;color address
  ld    bc,spriteaddrex38c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp makestarleft

  
 ; ret
 
.endkick:

  ld a,(ix+v7)
  cp 10
  inc (ix+v7)
  ret c

    ld (ix+v4),0
    ld (ix+v7),0    
    ld (ix+movepat),74
    
    ret
 

.setkickright:

  ld (ix+v4),3
  ld (ix+v7),0

  ld de,16
  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex39  ;color address
  ld    bc,spriteaddrex39c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;  ret

.kickright:
  
  ld a,(ix+v7)
  cp 15
  inc (ix+v7)
  ret c
  
  
   ld a,7
  ld (iy+movepat),a
 
  ld (iy+deathtimer),0 

  xor   a                       ;reset vertical movement speed
  ld    (iy+v2),a               ;v2 is vertical movement speed and direction 
  
    call Playsfx.kick
  

;schelpje word nu dodelijk

  ld (iy+deadly),1
  ld (iy+clipping),2

  ld a,horshellspeed
  ld (iy+v1),a

  ld (ix+v4),2
  ld (ix+v7),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex40  ;color address
  ld    bc,spriteaddrex40c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp makestarright
  
 ; ret


.nextframe:
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),27  ;generic set
  
  call Playsfx.kickenemy
 
 ;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a
 
  
;  call MovementPattern27.animate ;animeer
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex41  ;color address ;03
  ld    bc,spriteaddrex41c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



MovementPattern192:     ;green flying koopa (horizontal only)


  call objectinteract
;  cp 4
;  call z,.turn
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  call z,.turn
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

  call addv1toxbject

;  push hl
  call mariointeract
;  pop hl
  jp c,.nextframe


;get the y value  
  ld hl,greenflyingkoopatabley
  ld a,(ix+v4)
  ld e,a
  ld d,0
  add hl,de


  inc (ix+v4)

  ld a,(hl)
  cp 10
  jp z,.resetv4
  ld (ix+v2),a
  call addv2toyobject

;  ld a,(ix+v4)
;  cp 4
;  ret c ;skip the first frames to do correct turning


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  jp  z,animateflyingkoopa

  ret
  
.resetv4:

  ld (ix+v4),0
  ret


.nextframe:

   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),73  ;generic set

  ld (ix+v1),-1

;framecounter resetten voor constante tussenstukjes
  ld (ix+deathtimer),a
  ld (ix+v4),0
;  ld (ix+v3),0

  call Playsfx.kickenemy
  call makestarleft

  ret




;  ret

greenflyingkoopatabley:  db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,10




MovementPattern191:     ;switchbox

  ld a,(ix+v4)
  and a
  jp nz,.handlepop


;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleswitchboxinteractforobject
;  call z,handleboxinteract.popbox

  call mariointeract
  call c,handleswitchboxinteract

  ret


.handlepop:

  cp 6
  ret nc ;disable

  inc (ix+v4)

  cp 5
  jp nc,.popdone
  cp 3
  jp nc,.popdown


  ld (ix+v2),-2
  jp addv2toybject

;  ret

.popdown:

  ld (ix+v2),2
  jp addv2toybject 
  
;  ret


.popdone:

  inc (ix+v4)


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld    hl,spriteaddrex445  ;color address ;03
  ld    bc,spriteaddrex445c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 


;  ret


handleswitchboxinteract:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox

  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c
  
;set pop mode
  ld (ix+v4),1

  ld a,1
  ld (switchon),a
  
  ret



handleswitchboxinteractforobject:


;the info from the previous object is saved in iy   

;check if it is the shell going upwards

  ld a,(iy+movepat)
  cp 12
  jp z,.interact
  cp 95
  jp z,.interact
  
  ret


.interact:

  
;we check if the previous object is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  inc hl
  inc hl
  inc hl  
  xor a
  sbc hl,de
  ret c

;set pop mode
  ld (ix+v4),1

  ld a,1
  ld (switchon),a


  ret




MovementPattern190:     ;moving platform (path dependant)

  ld c,0
  call platforminteract


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8
  srl h
  rr l ;/16
  srl h
  rr l ;/32
  ld a,l
  cp 93
  jp nc,route5  
  cp 62
  jp nc,route2
  
  
  ld b,3
  ld hl,smallrotatex
  ld de,smallrotatey
  call rotateobject


  ret

route5:

 ; ld c,0
 ; call platforminteract

  ld a,(ix+v4)
  and a
  jp nz,.go

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  push de
  ld de,16
  add hl,de
  pop de
  xor a
  sbc hl,de
  ret nc
  
  call mariointeract
  ret nc
  
  ld (ix+v4),1

  
  ret
  

.go:

;  ld c,0
;  call platforminteract

  call addv1toxbject
  call addv2toybject

  ld a,(ix+v6)
  cp 3
  jp z,objectfall

  inc (ix+v7)

  ld a,(ix+v4)
  cp 2
  call z,objectfall

  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 48+1
  jp z,.step2
  cp 48+12+1
  jp z,.step3
  cp 48+12+1+24
  jp z,.reset

  ret
 
.step1: 

  ld (ix+v4),1
  ld (ix+v1),2
  ld (ix+v2),0
  
  ret


.step2:

  ld (ix+v1),2
  ld (ix+v2),-1
  
  ret
  
   
.step3: 

  inc (ix+v6)
  ld (ix+deathtimer),0
  inc (ix+v4)
  ret


.reset:

    ld (ix+v7),0
    ret


route2:

;make it a platform
;  ld c,0
;  call platforminteract

  call addv1toxbject
  call addv2toybject


;use v5 as 16 bit timer

  inc (ix+v7)

  ld a,(ix+v4)
  dec a
  jp z,route3
  dec a
  jp z,route4

 
  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 24+1
  jp z,.step2
  cp 24+24+1
  jp z,.step3
  cp 24+24+24+1
  jp z,.step4
  cp 24+24+24+24+1
  jp z,.step5
  cp 24+24+24+24+24+1
  jp z,.step6
  cp 24+24+24+24+24+24+1
  jp z,.step7
  cp 24+24+24+24+24+24+24+1
  jp z,.step8
  cp 24+24+24+24+24+24+24+24+1
  jp z,.reset          
    
 

  ret


.step1:

  ld (ix+v1),2
  ld (ix+v2),-2
  ret


.step2:

  ld (ix+v1),0
  ld (ix+v2),-2
  ret

.step3:

  ld (ix+v1),-2
  ld (ix+v2),-2
  ret

.step4:

  ld (ix+v1),-2
  ld (ix+v2),0
  ret

.step5:

  ld (ix+v1),-2
  ld (ix+v2),2
  ret

.step6:

  ld (ix+v1),0
  ld (ix+v2),2
  ret

.step7:

  ld (ix+v1),2
  ld (ix+v2),2
  ret

.step8:

  ld (ix+v1),2
  ld (ix+v2),0
  ret

.reset:

  ld (ix+v7),0
  ld (ix+v1),0
  ld (ix+v2),0
  

  ld a,(switchon)
  and a
  ret z
  
  
  ld (ix+v4),1
  xor a
  ld (switchon),a
  
  
  ret



route3:

  
  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 24+1
  jp z,.step2
  cp 24+72+1-24
  jp z,.step3
  cp 24+72+8+1-24
  jp z,.step4
  cp 24+72+8+24+1-24
  jp z,.step5
  cp 24+72+8+24+8+1-24
  jp z,.step6
  cp 24+72+8+24+8+24+1-24
  jp z,.step7  
  cp 24+72+8+24+8+24+24+1-24
  jp z,.step8 
  cp 24+72+8+24+8+24+24+24+1-24
  jp z,.step9
  
  
  
  cp 24+72+8+24+8+24+24+24+8+1-24
  jp z,.step10
  cp 24+72+8+24+8+24+24+24+8+1+1-24
  jp nc,objectfall 
  
  ret



.step1:

  ld (ix+v1),2
  ld (ix+v2),2
  ret


.step2:

  ld (ix+v1),2
  ld (ix+v2),0
  ret

.step3:

  ld (ix+v1),2
  ld (ix+v2),-2
  ret

.step4:

  ld (ix+v1),0
  ld (ix+v2),-2
  ret

.step5:

  ld (ix+v1),2
  ld (ix+v2),-2
  ret

.step6:

  ld (ix+v1),2
  ld (ix+v2),0
  
  ret

.step7:

  ld (ix+v1),2
  ld (ix+v2),2
  
  ld a,(switchon)
  and a
  ret z
  
  inc (ix+v4)  
  ld (ix+v7),0
  xor a
  ld (switchon),a
  
  
  ret


.step8:


  ld (ix+v1),2
  ld (ix+v2),0
  ret


.step9:

  ld (ix+v1),2
  ld (ix+v2),2
  ret

.step10:


  ld (ix+deathtimer),0

  
  ret


route4:


  ld a,(ix+v7)
  cp 1
  jp z,.step1
  cp 24+1
  jp z,.step2
  cp 24+90+1
  jp z,.step3
  cp 24+90+8+1
  jp z,.step4
  cp 24+90+8+1+1
  jp nc,objectfall

  
  ret
  
.step1:

  ld (ix+v1),2
  ld (ix+v2),-2
  ret


.step2:

  ld (ix+v1),2
  ld (ix+v2),0
  ret

.step3:

  ld (ix+v1),2
  ld (ix+v2),2
  ret

.step4:

  ld (ix+deathtimer),0
  ret



MovementPattern189:     ;small ghost (following mario)

  ld a,(fadeoutscreen?)
  and a
  call z,mariointeract

  call addv1toxbject
  call addv2toybject

  call .getsety
  call .getsetx

  call .acceleratex
  call .acceleratey

  ld a,(ix+v5)
  cp 3
  call z,.stopy
  ld a,(ix+v6)
  and a
  call z,.stopx


  ld a,(framecounter)
  and 3
  jp z,.animate

  
  ret


.stopx:

  ld a,(ix+v3)
  cp 2
  jp z,.stopxleft
  
 
  ld hl,ghostacctable
  ld a,(ix+v7)
  ld e,a
  and a
  jp z,.nospeedx
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1

  dec (ix+v7)
  
  ld (ix+v1),a
  
  ret

.set1:

  ld a,1
  ret

.stopxleft:
 
  ld hl,ghostacctable
  ld a,(ix+v7)
  ld e,a
  and a
  jp z,.nospeedx
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1  

  dec (ix+v7)
  
    neg
  ld (ix+v1),a
  
  ret

.nostopy:

  ld (ix+v5),0
  ld (ix+v2),0
  ld (ix+v5+1),0
  ret

.stopy:

  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  xor a
  sbc hl,de ;get y value what needs to be done
  jp c,.stopyleft
  jp z,.nostopy
 
  ld hl,ghostacctable
  ld a,(ix+v5+1)
  ld e,a
  and a
  jp z,.nospeedy
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1

  dec (ix+v5+1)
  
  ld (ix+v2),a
  
  ret


.stopyleft:
 
  ld hl,ghostacctable
  ld a,(ix+v5+1)
  ld e,a
  and a
  jp z,.nospeedy
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.set1  

  dec (ix+v5+1)
  
    neg
  ld (ix+v2),a
  
  ret


.nospeedx:

  ld (ix+v1),0

  ret

.nospeedy:

  ld (ix+v2),0

  ret


.getsetx:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,(mariox)
  xor a
  sbc hl,de ;get x value what needs to be done
  jp c,.setxmin
  jp z,.setxzero


  ld a,(marioleftrightspr)
  dec a
  jp z,.setxyzero
  
  ld (ix+v6),1
  ld (ix+v3),0 ;left

  ret

.setxmin:

  ld a,(marioleftrightspr)
  and a
  jp z,.setxyzero


  ld (ix+v6),2
  ld (ix+v3),1 ;right

  ret

.setxyzero:

  ld (ix+v2),0
  ld a,(ix+v3)
  cp 2
  jp nc,.setxzero2
  
  inc (ix+v3)
  inc (ix+v3)

.setxzero:

  ld (ix+v1),0
  ld (ix+v6),3 ;stop with table
;  ld (ix+v7),0
;  ld (ix+v5),3 ;stop with table
;  ld (ix+v5+1),0

  ret  

.setxzero2:

;  ld (ix+v1),0
  ld (ix+v6),0 ;stop with table
;  ld (ix+v7),0
  ld (ix+v5),3 ;stop with table
;  ld (ix+v5+1),0


  ret


.getsety:

  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  xor a
  sbc hl,de ;get y value what needs to be done
  jp c,.setymin
  jp z,.setyzero
  
  ld (ix+v5),2

  ret

.setymin:

  ld (ix+v5),1

  ret

.setyzero:

  ld (ix+v5),0
  ld (ix+v2),0

  ret  


.acceleratex:

  ld a,(ix+v6)
  and a
  ret z
  cp 1
  jp z,.accright
  cp 3
  ret z

  ld hl,ghostacctable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setxspeedmaxleft
  
  ld (ix+v1),a
  
  inc (ix+v7)



  ret


.setxspeedmaxleft:

  ld (ix+v1),1
  ret


.accright:
  ld hl,ghostacctable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setxspeedmaxright
  
  neg
  ld (ix+v1),a
  
  inc (ix+v7)



  ret


.setxspeedmaxright:

  ld (ix+v1),-1
  ret




.acceleratey:


  ld a,(ix+v5)
  and a
  ret z
  cp 1
  jp z,.accrighty
  cp 3
  ret z

  ld hl,ghostacctable
  ld e,(ix+v5+1)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setyspeedmaxleft
  
  ld (ix+v2),a
  
  inc (ix+v5+1)



  ret


.setyspeedmaxleft:

  ld (ix+v2),1
  ret


.accrighty:
  ld hl,ghostacctable
  ld e,(ix+v5+1)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setyspeedmaxright
  
  neg
  ld (ix+v2),a
  
  inc (ix+v5+1)



  ret


.setyspeedmaxright:

  ld (ix+v2),-1
  ret






.animate:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp z,.position0 ;left
  cp 1
  jp z,.position1 ;right
  cp 2
  jp z,.position2 ;left affraid
  cp 3
  jp z,.position3 ;right affraid

  ret

.position0:

  ld    hl,spriteaddrex432  ;color address ;03
  ld    bc,spriteaddrex432c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

.position1:

  ld    hl,spriteaddrex438  ;color address ;03
  ld    bc,spriteaddrex438c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
.position2:

  ld    hl,spriteaddrex433  ;color address ;03
  ld    bc,spriteaddrex433c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset   

.position3:

  ld    hl,spriteaddrex439  ;color address ;03
  ld    bc,spriteaddrex439c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


ghostacctable: db 0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,1,1,0,1,1,0,1,5


MovementPattern188:     ;coin tail object

  ld a,(ix+v4)
  and a
  jp z,.init
  
  ld a,(ix+v7)
  and a
  jp nz,.timer
  
  ret

.timer:

  call .animate

  call addv1toxbject
  call addv2toybject

  ld a,(ix+v7)
  inc (ix+v7)
  cp 9
  ret c

  call .makerandomxy

;check if we are at the wall and if so end the routine
  ld bc,0
  ld de,-8
  call objecttilecheckfromrom
  cp 191
  jp nc,.end

  ld bc,0
  ld de,8
  call objecttilecheckfromrom
  cp 191
  jp nc,.end


  ld a,(ix+v7)
  cp 200
  ret c


.end:

  push ix
  call restoremusicafterpbox
  pop ix

  jp destroyobject

;  ret



.init:

  ld a,1
  ld (switchmusicpforbox),a


  ld (ix+v7),1
  inc (ix+v4)

  ret


.makerandomxy:

;each 8 pixels of movement
  ld a,(ix+v7)
  and 15
  ret nz
 
  ;spawn a coin on each previuos position
  call .spawncoin
 
  
  ld a,r
  and 1
  jp z,.up
  
  ld a,r
  and 1
  jp z,.right

.left:

  ld a,(ix+v1) ;not allowed to return
  cp 1
  jp z,.right
  
  ld (ix+v1),-1
  ld (ix+v2),0
  
  ret

.up:

  ld a,r
  and 1
;  jp z,.down
  
  ld (ix+v1),0
  ld (ix+v2),-1
  ret
  
.down:

  ld (ix+v1),0
  ld (ix+v2),-1
  
  ld a,(ix+v7)
  cp 100
  ret c

  ld (ix+v1),0
  ld (ix+v2),1
  ret
  
.right:

  ld a,(ix+v1) ;not allowed to return
  cp -1
  jp z,.left


    ld (ix+v1),1
    ld (ix+v2),0
    ret
  

.spawncoin:

  

  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8




  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),78                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject


  pop ix


  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret



MovementPattern187:     ;vinehelper ghost house (helper object)

  call getmariodistance
  ld a,c
  cp 24
  jp nc,.setvineinactive

    ld a,1
    ld (vineactive),a
    
    ret



.setvineinactive:
  
    xor a
    ld (vineactive),a


  ret




MovementPattern186:     ;pbox spawn


  ld a,(ix+v7)
  cp 20
  jp c,.popup


  ld (ix+v7),0
  ld (ix+movepat),93
  
  ret

.popup:

  ld a,(ix+v7)
  cp 16
  jp nc,.nextframe


  ld (ix+v2),1
  
  call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v7)  
  ld (ix+objectfp),0

 ret


.nextframe:

  ld (ix+v2),0
  inc (ix+v7)

  ret

MovementPattern185:     ;small ghost

  ld a,(ix+v4)
  and a
  jp z,.init

  ld a,(fadeoutscreen?)
  and a
  call z,mariointeract


  ld a,(mariospeed)
  and a
  call z,.turn



  call addv1toxbject
  call addv2toybject
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,25*8
  xor a
  sbc hl,de
  call c,.negate 
 
  

;if flying too low negate y movement
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,53*8
  xor a
  sbc hl,de
  ret c
  
.negate: 

  ld a,(ix+v2)
  neg 
  ld (ix+v2),a


  ret


.turn:

  call getmariodistance
  cp 50
  ret c


  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  
  xor a
  sbc hl,de
  
  jp nc,.turnleft

  ld a,(ix+v1)
  and 3
  neg
  ld (ix+v1),a


  ld a,(ix+v3)
  cp 1
  jp z,.type1
  cp 2
  jp z,.type2
  cp 3
  jp z,.type3
  cp 4
  jp z,.type4
  cp 5
  jp z,.type5
  cp 6
  jp z,.type6
  cp 7
  jp z,.type1  


  ret

.turnleft:


  ld a,(ix+v1)
  and 3
  ld (ix+v1),a


  ld a,(ix+v3)
  cp 1
  jp z,.type1r
  cp 2
  jp z,.type2r
  cp 3
  jp z,.type3r
  cp 4
  jp z,.type4r
  cp 5
  jp z,.type5r
  cp 6
  jp z,.type6r 
  cp 7
  jp z,.type1r  

  
  ret


;randomize v1 and 2 and sprite type
.init:


  ld a,r
  and 3
 ; inc a
  neg
  ld (ix+v1),a
  call makerandomnumberforghost
  ld (ix+v2),a  
  ld a,r
  and 7
  or 1; prevent number 0
  ld (ix+v3),a
  cp 1
  jp z,.type1
  cp 2
  jp z,.type2
  cp 3
  jp z,.type3
  cp 4
  jp z,.type4
  cp 5
  jp z,.type5
  cp 6
  jp z,.type6
  
.type1:  

  ld    hl,spriteaddrex432  ;color address ;03
  ld    bc,spriteaddrex432c  ;character address


  jp .setspritetype

.type2:

  ld    hl,spriteaddrex433  ;color address ;03
  ld    bc,spriteaddrex433c  ;character address

  jp .setspritetype
  
.type3:  

  ld    hl,spriteaddrex434  ;color address ;03
  ld    bc,spriteaddrex434c  ;character address

  jp .setspritetype

.type4:  

  ld    hl,spriteaddrex435  ;color address ;03
  ld    bc,spriteaddrex435c  ;character address

  jp .setspritetype

.type5: 

  ld    hl,spriteaddrex436  ;color address ;03
  ld    bc,spriteaddrex436c  ;character address

  jp .setspritetype
 
.type6:    
 
 
  ld    hl,spriteaddrex437  ;color address ;03
  ld    bc,spriteaddrex437c  ;character address   

  jp .setspritetype

.type1r:  

  ld    hl,spriteaddrex438  ;color address ;03
  ld    bc,spriteaddrex438c  ;character address


  jp .setspritetype

.type2r:

  ld    hl,spriteaddrex439  ;color address ;03
  ld    bc,spriteaddrex439c  ;character address

  jp .setspritetype
  
.type3r:  

  ld    hl,spriteaddrex440  ;color address ;03
  ld    bc,spriteaddrex440c  ;character address

  jp .setspritetype

.type4r:  

  ld    hl,spriteaddrex441  ;color address ;03
  ld    bc,spriteaddrex441c  ;character address

  jp .setspritetype

.type5r: 

  ld    hl,spriteaddrex442  ;color address ;03
  ld    bc,spriteaddrex442c  ;character address

  jp .setspritetype
 
.type6r:    
 
 
  ld    hl,spriteaddrex443  ;color address ;03
  ld    bc,spriteaddrex443c  ;character address     
  
  
.setspritetype:  
  
  ld (ix+v4),1
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 



;  ret


makerandomnumberforghost:

  ld b,0

  ld a,r
  ld b,a
  ld a,r
  nop
  add a,b
  ld b,a
  ld a,r
  add a,b
  ld b,a
  nop : nop : 
  nop : nop : 
  ld a,r
  add a,b
  ld b,a
  nop
  ld a,r
  add a,b

  ld hl,randomtableghost
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)


  ret
  


randomtableghost:  db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20
                     db 1,-1,1,2,-1,1,-1,-2,1,-2,3,-2,1,-1,-1,2,-2,-2,1,-1 ;20

  



MovementPattern184:     ;football

  ld a,(ix+v5)
  and a
  jp z,.countdeath


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin

  call mariointeract
  jp c,otherobjectdeathjump


  call checkobjecttilescheckwall
  call c,.changedir


  ld a,(ix+v4)
  cp 1
  jp z,.fall

  ld a,(ix+v6)
  cp 1
  jp z,.bouncelow
  cp 2
  jp z,.bouncehigh


  call getmarioheight
  ld (ix+v6),1
  cp 30
  jp nc,.sethighbounce

.bouncelow:

  ld hl,bouncetable
  jp .bounce
  
.bouncehigh:

  ld hl,bouncehightable   

.bounce:

  ld a,(ix+v7)
;  ld hl,bouncetable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.setfall
  
  ld (ix+v2),a
 
 
  call addv1toxbject 
  call addv2toybject 
  
  inc (ix+v7)
  
  ret


.sethighbounce:

  ld (ix+v6),2
  
  ret

.setfall:

  ld (ix+v4),1
  ld (ix+v6),0
  ld (ix+v7),0
  ld (ix+objectfp),0
  ret


.fall:

  call addv1toxbject
  call addv2toybject

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  


  call checkobjecttilesfloor
  ret c
  
  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp nz,.position1

  ld    hl,spriteaddrex430  ;color address ;03
  ld    bc,spriteaddrex430c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 


  ;ret

.position1:


  ld    hl,spriteaddrex431  ;color address ;03
  ld    bc,spriteaddrex431c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 

;ret

.changedir:

 
    ld a,(ix+v1)
    neg
    ld (ix+v1),a


  ret

;prevent stucking
.countdeath:

  inc (ix+hitpoints)
  ld a,(ix+hitpoints)
  cp 20
  ret c

  jp destroyobject
  
  ;ret




bouncetable: db -4,-4,-3,-3,-3,-2,-2,-2,-1,-1,0
bouncehightable: db -7,-7,-7,-6,-6,-5,-5,-4,-3,-3,-3,-2,-2,-2,-1,-1,0


MovementPattern183:     ;football chuck


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,.takefire
  cp 3
  jp z,otherobjectdeathjump


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  ;V4
  ;0 = normal waiting for something to happen
  ;1 = about to trow football
  ;2 = trowing football

  ;using v7 at all times

  ld a,(ix+v4)
  cp 1
  jp z,.wait
  cp 2
  jp z,.resetanimation




  ;add counter for football spawn and kick
  inc (ix+v7)
  ld a,(ix+v7)
  cp 75
  jp z,.setfootball



  call getmarioleftright
  call    nc,.setleft
  
  call getmarioleftright
  call    c,.setright

  ret


.resetanimation:


   ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

   ld (ix+v4),0
   ld (ix+v7),0
   

  ld a,(ix+v3) ;1 = right 0 = left
  and a
  jp nz,.resetanimationright


  ld    hl,spriteaddrex395  ;color address ;03
  ld    bc,spriteaddrex395c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  


;  ret 


.resetanimationright:


  ld    hl,spriteaddrex400  ;color address ;03
  ld    bc,spriteaddrex400c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
 
  ;ret


.setleft:

  ld a,(ix+v3) ;1 = right 0 = left
  and a
  ret z
  
  ld (ix+v3),0


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex395  ;color address ;03
  ld    bc,spriteaddrex395c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  


;  ret 

  

.setright:

  ld a,(ix+v3) ;1 = right 0 = left
  and a
  ret nz
  
  ld (ix+v3),1


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex400  ;color address ;03
  ld    bc,spriteaddrex400c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  


;  ret 

.animatekick:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(ix+v3) ;1 = right 0 = left
  and a
  jp nz,.animatekickright
  
  ld    hl,spriteaddrex64  ;color address ;03
  ld    bc,spriteaddrex64c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  


;  ret 


.animatekickright:



  ld    hl,spriteaddrex66  ;color address ;03
  ld    bc,spriteaddrex66c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  


;  ret 






.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 10
  ret c
  cp 10
  jp z,.animatekick
  cp 11
  jp z,.animatekick  
  
  
  ld (ix+v4),2
  ld (ix+v7),0

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  
;activate the football  
  push hl
  pop iy 
  
;  ld (iy+movepat),184
  ld (iy+v5),1
  ld (iy+edible),1
 
  ret


.setfootball:

  ld (ix+v4),1
  ld (ix+v7),0

;TODO: yayayaya spawn the freaking football
  ld a,(ix+v3)
  and a
  jp nz,.setfootballright


  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de



  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),176                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject

  push ix
  pop hl


  pop ix
;save the football ix adress
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret 



.setfootballright:

  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  inc hl
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de



  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),176                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject

  ld (ix+v1),2


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex431  ;color address ;03
  ld    bc,spriteaddrex431c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset 


  push ix
  pop hl


  pop ix
;save the football ix adress
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret 


.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret

.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+hitpoints),3
  ld (ix+movepat),35

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex67  ;color address ;02
  ld    bc,spriteaddrex67c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call  setcharcolandoffset


;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckauw


  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret



MovementPattern182:     ;spiketop

  call objectinteract
;  cp 4
;  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
;  cp 5
;  jp z,destroyobjectwithcoin

  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump



  call mariointeract

  ld a,(ix+v4)
  cp 1
  jp z,.turndown
  cp 2
  jp z,.down
  cp 3
  jp z,.turnright
  cp 4
  jp z,.right
  cp 5
  jp z,.turnup
  cp 6
  jp z,.up
  cp 7
  jp z,.turnleft
  cp 8
  jp z,.turnleftup
  cp 9
  jp z,.turnleftdown
  
  ;directions:
  ;V4 = 0 LEFT
  ;V4 = 1 turning to down
  ;V4 = 2 DOWN
  ;V4 = 3 turning to right
  ;V4 = 4 RIGHT
  ;V4 = 5 turning to up (from down)
  ;V4 = 6 UP
  ;V4 = 7 turning to left
  ;V4 = 8 turning to up (from left)
  ;V4 = 9 turning to left (from up)
  
  
  ;V7 = timer during the turns
  
  ld (ix+v1),-1
  
  call addv1toxbject
  
  call  checkobjecttilesfloor
  jp c,.changedir

;or check for walls on  the left
  ld bc,15
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp nc,.setdirup



  ld a,(framecounter) ;animate spike top
  and 3
  ret nz
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp z,.position1left


  ld    hl,spriteaddrex422  ;color address ;03
  ld    bc,spriteaddrex422c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position1left:


  ld    hl,spriteaddrex423  ;color address ;03
  ld    bc,spriteaddrex423c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
 
  
;  ret

.setdirup:

  ld (ix+v4),8
  ld (ix+v7),0


  ret


.down:

  ld a,(framecounter) ;animate spike top
  and 3
  call z,.animatedown

  
  ld (ix+v2),1
  
  call addv2toybject  

  ld bc,16
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp nc,.changedirdownleft


 
  ld bc,16
  ld    de,16                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.checknext

  ret

.checknext:

  ld bc,1
  ld    de,16                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.changedir

  ret


.changedirdownleft:

  ld (ix+v4),9
  ld (ix+v7),0


  ret

.animatedown:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp z,.position1down


  ld    hl,spriteaddrex425  ;color address ;03
  ld    bc,spriteaddrex425c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position1down:


  ld    hl,spriteaddrex426  ;color address ;03
  ld    bc,spriteaddrex426c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
 
  
;  ret



.right:

  ld a,(framecounter) ;animate spike top
  and 3
  call z,.animateright


  ld (ix+v1),1
  call addv1toxbject


  ld bc,0
  ld    de,8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.checknextright


  ret

.checknextright:

  ld bc,0
  ld    de,-7                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.changedir
 
  ret  



.animateright:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp z,.position1right


  ld    hl,spriteaddrex509  ;color address ;03
  ld    bc,spriteaddrex509c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position1right:


  ld    hl,spriteaddrex510  ;color address ;03
  ld    bc,spriteaddrex510c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
 
  
;  ret



.up:


  ld a,(framecounter) ;animate spike top
  and 3
  call z,.animateup


  ld (ix+v2),-1
  call addv2toybject


  ld bc,1 ;changed to 1 makes the difference on some maps
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.checknextup 
  
  ret 

.checknextup:

  ld bc,16
  ld    de,-8                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom
  cp 191  
  jp c,.changedir 

  ret


.animateup:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp z,.position1up


  ld    hl,spriteaddrex428  ;color address ;03
  ld    bc,spriteaddrex428c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position1up:


  ld    hl,spriteaddrex429  ;color address ;03
  ld    bc,spriteaddrex429c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
 
  
;  ret




.changedir:

  ld a,(ix+v4);,2
  inc a
;  inc a
  ld (ix+v4),a
  ld (ix+v7),0
  ret

.resetdir:

  ld (ix+v4),0
  ld (ix+v7),0
  ret

.turnleftdown:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturndown
  cp 5
  ret c

  ld (ix+v4),0
  ld (ix+v7),0
  
  ret
  

.turnleftup:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturnleft
  cp 5
  ret c

  ld (ix+v4),6
  ld (ix+v7),0
  
  ret




.turndown:
 

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturndown
  cp 5
  ret c

  jp .changedir


.animateturndown:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex424  ;color address ;03
  ld    bc,spriteaddrex424c  ;character address
  ld    de,offsetspiketurndown
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.turnright:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturnright  
  cp 5
  ret c

  jp .changedir

.animateturnright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex511  ;color address ;03
  ld    bc,spriteaddrex511c  ;character address
  ld    de,offsetspiketurnupsd
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.turnup:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturnup   
  cp 5
  ret c

  jp .changedir

.animateturnup:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex512  ;color address ;03
  ld    bc,spriteaddrex512c  ;character address
  ld    de,offsetspiketurnup
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.turnleft:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,.animateturnleft
  cp 5
  ret c

  jp .resetdir


.animateturnleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex427  ;color address ;03
  ld    bc,spriteaddrex427c  ;character address
  ld    de,offsetspiketurnleft
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


MovementPattern181:     ;bat (flying turd)


  call objectinteract
  cp 2
  jp z,.otherobjectdeathjump
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin

  call mariointeract
  jp c,.otherobjectdeathjump

;fly the stupid thing
  ld a,(ix+v4)
  dec a
  jp z,.fly



  call getmariodistance
  ld a,c
  cp 40
  jp c,.activate

  ret


.otherobjectdeathjump:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex421  ;color address ;03
  ld    bc,spriteaddrex421c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  jp otherobjectdeathjump


.activate:

  ld (ix+v4),1
;addsfx here
  call Playsfx.bat

  ret


.fly:

    ld a,(currentlevel)
    cp 139 ;map with very high ceiling
    jp z,.flydeep

  ld a,(framecounter)
  and 3
  call z,.animate


  ld a,(ix+v7)
  ld hl,batflytablex
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.flymax
  
  ld (ix+v1),a
  
  ld a,(ix+v7)
  ld hl,batflytabley
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  
  ld (ix+v2),a  

  inc (ix+v7)

.flymax:

  call addv2toybject
  call addv1toxbject

  ret

  
  
.flydeep:  

  ld a,(framecounter)
  and 3
  call z,.animate


  ld a,(ix+v7)
  ld hl,batflytablex
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.flymax
  
  ld (ix+v1),a
  
  ld a,(ix+v7)
  ld hl,batflytableymax
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  
  ld (ix+v2),a  

  inc (ix+v7)
  
  jp .flymax
  
  
.animate:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  and a
  jp z,.position1


  ld    hl,spriteaddrex419  ;color address ;03
  ld    bc,spriteaddrex419c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position1:


  ld    hl,spriteaddrex420  ;color address ;03
  ld    bc,spriteaddrex420c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  

;ret

batflytablex: db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,-2,-3,-3,-3,-3,-3,-3,-3,-3,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,5
batflytabley: db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, 3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,1,1,1,1,0,-1,-1,-1,-1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-3
batflytableymax: db 28,10,8,8,8,5,5,5,5,5,4,4,4,4,4, 4,4,4,4,4,4,4,3,3,3,3,2,2,2,2,2,2,2,2,2,2,1,1,1,1,0,-1,-1,-1,-1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-3


MovementPattern180:     ;cavekoopa




  call checkmariointeractshoot
  jp c,.otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   7                       ;this sprite animates once every 3 frames
  call  z,.animate

  call objectinteract
  cp 4
;  push af
  jp z,.objectwallfound
;  pop af
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
;  push af
  jp z,.objectwallfound
;  pop af
  cp 3
  jp z,.otherobjectdeathjump
;  cp 5
;  jp z,destroyobjectwithcoin


  ld (ix+v7),0 ;set timer

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  
  call  checkobjecttilesfloor


  ret


.otherobjectdeathjump:


    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex414  ;color address ;03
  ld    bc,spriteaddrex414c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  jp otherobjectdeathjump


.otherobjectshotdeathjump:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex597  ;color address ;03
  ld    bc,spriteaddrex597c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock4
  call    setcharcolandoffset


;;save the speed of the hitting object  
;  push bc
;  
;  call turnshellupsidedown
;
;  pop bc


    ld (ix+sizeY),16
    ld (ix+clipping),2 ;becomes deadly to enemies
    ld (ix+deadly),0 ;won't kill mario
    ld (ix+deathtimer),0
    ld (ix+objectfp),0
    ld (ix+v2),8
    ld (ix+movepat),80
    ld (ix+v7),0

;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  ld de,16
;  add hl,de
;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)



  ret



;  jp otherobjectshotdeathjump

.objectwallfound:

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  jp c,.skipchecks
  call checksheerobjecttile ;check for sheer tiles
  jp c,.skipchecks ;sheertile found

  call  checkobjecttilesfloor
;  ret c
;  call checkobjecttilescheckwall
;  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

.skipchecks:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 8
  ret nc ;ignore all objects


.wallfound:

;  ld a,(ix+v2)
;  and a
;  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex410  ;color address
  ld    bc,spriteaddrex410c  ;character address
  ld    de,offsetsrex00
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex411  ;color address
  ld    bc,spriteaddrex411c  ;character address
  ld    de,offsetsrex01
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex412  ;color address ;02
  ld    bc,spriteaddrex412c  ;character address
  ld    de,offsetsrex02
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex413  ;color address ;03
  ld    bc,spriteaddrex413c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.nextframe:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),1  ;generic set
; 

;use this value for respawn purposes
  xor a
  ld (ix+deathtimer),a


  ld (ix+deadly),0

  call Playsfx.kickenemy
;  call makestarleft

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex415  ;color address ;03
  ld    bc,spriteaddrex415c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  call makestarleft

  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex414  ;color address ;03
  ld    bc,spriteaddrex414c  ;character address
  ld    de,offsetsrex03
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
  

  ;ret


MovementPattern179:     ;mariocrusher (helper)


  call followmario

;  ld a,(mariocoordinates+1)
;  cp 5
;  jp c,.killmario
;  cp 10
;  call c,.setforcewalk
  
  
  ld a,(mariofloating)
  and a
  jp nz,.checkjump


  ld de,0
  ld bc,16
  call objecttilecheckfromrom
  cp 97
  jp z,.killmario
  cp 98
  jp z,.killmario
  cp 99
  jp z,.killmario
  cp 176
  jp z,.killmario
  cp 232
  jp z,.killmario
  cp 240
  jp z,.killmario
  cp 208
  jp z,.killmario
  cp 209
  jp z,.killmario
  
  ret

.checkjump:

  ld a,(mariostate)
  and a
  jp nz,.big

  ld de,0
  ld bc,8-2
  call objecttilecheckfromrom
  cp 232
  jp z,.fall
  cp 240
  jp z,.fall
  cp 208
  jp z,.fall
  cp 209
  jp z,.fall  
  ld de,8
  ld bc,8-2
  call objecttilecheckfromrom
  cp 232
  jp z,.fall
  cp 240
  jp z,.fall
  cp 208
  jp z,.fall
  cp 209
  jp z,.fall 


  ret


.big:

  ld de,0
  ld bc,0
  call objecttilecheckfromrom
  cp 232
  jp z,.fall
  cp 240
  jp z,.fall
  cp 208
  jp z,.fall
  cp 209
  jp z,.fall  
  ld de,8
  ld bc,0
  call objecttilecheckfromrom
  cp 232
  jp z,.fall
  cp 240
  jp z,.fall
  cp 208
  jp z,.fall
  cp 209
  jp z,.fall 


  ret



.killmario:

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a
  
  ret


.fall:

  ld a,1
  ld (marionotdown),a


  ret

;.setforcewalk:
;
;    ld a,1
;    ld (marioforcewalk),a
;    ret
  
  

MovementPattern178:     ;diagonal block (helper)

  ;follow mario x at ALL times
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld hl,(marioy)
  ld de,32 ;adjust y coordinates of object for the tiledetection code
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld a,(ix+v7)
  dec a
  jp z,.specialstate


  call objectinteract
  ld a,(iy+movepat)
  cp 177  
  jp z,.setspecialstate

  ret


.resetstate:

  ld (ix+v5),0
  ld (ix+v5+1),0
  ld (ix+v7),0
  xor a
  ld (nocontrol),a

  ret


.setspecialstate:

  push iy
  pop hl
  ld (ix+v5),l
  ld (ix+v5+1),h ;save these values for later
;Danger: if this object gets overwritten or deleted by the engine during the handling of the special state
;wierd things may happen. The engine should not be doing that. If wierd bugs occur something may be wrong with
;the eventhandler.


.specialstate:

  ld a,(marionotdown)
  dec a
  jp z,.resetstate ;simply returning can stuck mario for eternity


  ld a,(marioyoshi)
  and a
  jp nz,.resetstate
  
  ld a,(marioshell)
  and a
  jp nz,.resetstate

;  ld a,(mariorunning)
;  and a
;  ret nz
  ld a,(mariospeed)
  cp 4
  ret c



;retrieve other objects ix value for reference
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld a,(iy+objectrelease)
  dec a
  jp z,.handleright



  ld a,(Controls)
  bit 5,a
  jp z,.resetstate
  bit 3,a
  jp z,.resetstate


  ld (ix+v7),1
   ld a,5 ;bogus number to block the mariophysics engine but prevent the levelloader from doing anything strange
   ld (nocontrol),a
  

;trow mario up the wall
  ld hl,(marioy)
  dec hl
  dec hl
  dec hl
  dec hl
  ld (marioy),hl


  ;lock mario to the object so you really see him running up the walls
  ld l,(iy+xobject)
  ld h,(iy+xobject+1)
  dec hl
  ld (mariox),hl

;check if there is background on the right. If yes. abort the running. Mario has reached the topS
  ld de,16
  ld bc,0
  call objecttilecheckfromrom
  cp 191
  jp c,.resetstate

 
  ;animate mario this means write into the mario sprite in VRAM. fetch the correct sprite etc. then animate

  
  ld a,(mariostate)
  and a
  jp z,animatewallmariosmall
  cp 1
  jp z,animatewallmariobig
  cp 2
  jp z,animatewallmariofire
  cp 3
  jp z,animatewallmarioflying

;if we cannot determine the state leave it

  ret



.handleright:



  ld a,(Controls)
  bit 5,a
  jp z,.resetstate
  bit 2,a
  jp z,.resetstate


  ld (ix+v7),1
   ld a,5 ;bogus number to block the mariophysics engine but prevent the levelloader from doing anything strange
   ld (nocontrol),a

;trow mario up the wall
  ld hl,(marioy)
  dec hl
  dec hl
  dec hl
  dec hl
  ld (marioy),hl


  ;lock mario to the object so you really see him running up the walls
  ld l,(iy+xobject)
  ld h,(iy+xobject+1)
  inc hl
  inc hl
;  inc hl
;  inc hl
  ld (mariox),hl

;adjust object. This is a cheap hack. I am not going to recode the objecttilecheck for this one little thingy
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

;check if there is background on the left. If yes. abort the running. Mario has reached the top
  ld de,1
  ld bc,0
  call objecttilecheckfromrom
  cp 191
  jp c,.resetstate

 
  ;animate mario this means write into the mario sprite in VRAM. fetch the correct sprite etc. then animate

  
  ld a,(mariostate)
  and a
  jp z,animatewallmariosmallright
  cp 1
  jp z,animatewallmariobigright
  cp 2
  jp z,animatewallmariofireright
  cp 3
  jp z,animatewallmarioflyingright

;if we cannot determine the state leave it



  ret




animatewallmariosmall:

  ld a,(mariowalktable)
  
  cp 1
  jp c,.runleft1

  
  ld hl,spriteadrrunningupleft2
  jp writemariosprite

;  ret


.runleft1:

  ld hl,spriteadrrunningupleft1
  jp writemariosprite
  
;  ret



animatewallmariobig:


  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret


.mariospriterunleft1:

  ld hl,spriteadrrunningupbigright1
  jp writemariosprite


.mariospriterunleft2:

  ld hl,spriteadrrunningupbigright2
  jp writemariosprite

.mariospriterunleft3:

  ld hl,spriteadrrunningupbigright3
  jp writemariosprite



animatewallmariofire:


  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret


.mariospriterunleft1:

  ld hl,spriteadrrunningupfireleft1
  jp writemariosprite


.mariospriterunleft2:

  ld hl,spriteadrrunningupfireleft2
  jp writemariosprite

.mariospriterunleft3:

  ld hl,spriteadrrunningupfireleft3
  jp writemariosprite





animatewallmarioflying:




  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret


.mariospriterunleft1:

  ld hl,spriteadrrunningupflyingleft1
  jp writemariosprite


.mariospriterunleft2:

  ld hl,spriteadrrunningupflyingleft2
  jp writemariosprite

.mariospriterunleft3:

  ld hl,spriteadrrunningupflyingleft3
  jp writemariosprite




 ; ret


;right

animatewallmariosmallright:

  ld a,(mariowalktable)
  
  cp 1
  jp c,.runright1

  
  ld hl,spriteadrrunningupright2
  jp writemariosprite

;  ret


.runright1:

  ld hl,spriteadrrunningupright1
  jp writemariosprite
  
;  ret



animatewallmariobigright:


  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunleft1
  
  cp 1
  jp z,.mariospriterunleft2
  
  cp 2
  jp z,.mariospriterunleft3
  ret


.mariospriterunleft1:

  ld hl,spriteadrrunningupbigleft1
  jp writemariosprite


.mariospriterunleft2:

  ld hl,spriteadrrunningupbigleft2
  jp writemariosprite

.mariospriterunleft3:

  ld hl,spriteadrrunningupbigleft3
  jp writemariosprite



animatewallmariofireright:


  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunright1
  
  cp 1
  jp z,.mariospriterunright2
  
  cp 2
  jp z,.mariospriterunright3
  ret


.mariospriterunright1:

  ld hl,spriteadrrunningupfireright1
  jp writemariosprite


.mariospriterunright2:

  ld hl,spriteadrrunningupfireright2
  jp writemariosprite

.mariospriterunright3:

  ld hl,spriteadrrunningupfireright3
  jp writemariosprite





animatewallmarioflyingright:




  ld a,(mariowalktable)
  
  cp 0
  jp z,.mariospriterunright1
  
  cp 1
  jp z,.mariospriterunright2
  
  cp 2
  jp z,.mariospriterunright3
  ret


.mariospriterunright1:

  ld hl,spriteadrrunningupflyingright1
  jp writemariosprite


.mariospriterunright2:

  ld hl,spriteadrrunningupflyingright2
  jp writemariosprite

.mariospriterunright3:

  ld hl,spriteadrrunningupflyingright3
  jp writemariosprite




 ; ret


;here is the big trick. Mariosprites are written to VRAM BEFORE the eventhandler kicks in.
;this comes out very handy. The mariosprite engine is allready big enough and I need to avoid any more problems
;therefore the diagonal block changes the mariosprite and takes actual control of mario. It reads the running byte
;and this way finds out if the player stops any running. The object checks the tiles and see how mario should change.

;ret

;taken from putmariosprite I am not going to recode this again...
;In = is spriteadress in HL set block with this code as well.
writemariosprite:

  push hl

  ;put hero sprite character
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	ld de,448 ;+14 sprites verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  ld a,$19
  call block12 ;don't call anything from the generalblocks or things will turn bad


  pop hl
  push hl
	ld		c,$98
	call	outix128
;/put hero sprite character


	ld		hl,(invissprcoltableaddress)		;sprite color table in VRAM ($17400)
	ld de,224 ;+14 color tables verder
	add hl,de
	ld		a,1
	call	SetVdp_Write

  pop hl
	ld de,128
	add hl,de
	ld		c,$98
	call	outix64

  ;retrieve mario spat data from RAM and apply coordinate corrections.

;vertical
  ld    a,(spat+56)
  add a,16
  ld    (spat+56),a
  ld    a,(spat+60)
  add a,16
  ld    (spat+60),a


  ld a,(mariostate)
  and a
  jp z,.adjustsmall

  ld a,(marioleftright)
  and a
  jp z,.adjustleftbig

;correct the head
;horizontal
  ld    a,(spat+57)
  sub 16+4
  ld    (spat+57),a
  ld    a,(spat+61)
  sub 16+4
  ld    (spat+61),a
  
  ld    a,(spat+65)
  sub 4
  ld    (spat+65),a
  ld    a,(spat+69)  
  sub 4
  ld    (spat+69),a  
  
  
;vertical
;  ld    a,(spat+56)
;  add a,16
;  ld    (spat+56),a
;  ld    a,(spat+60)
;  add a,16
;  ld    (spat+60),a


  jp .continue


.adjustleftbig:



;correct the head
;horizontal
  ld    a,(spat+57)
  sub 8;16+4-16+4
  ld    (spat+57),a
  ld    a,(spat+61)
  sub 8;16+4-16+4
  ld    (spat+61),a
  
  ld    a,(spat+65)
  sub -8;4-16+4
  ld    (spat+65),a
  ld    a,(spat+69)  
  sub -8;4-16+4
  ld    (spat+69),a  




  jp .continue


.adjustsmall:

;correct the head
;horizontal
  ld    a,(spat+57)
  sub 16-8+4
  ld    (spat+57),a
  ld    a,(spat+61)
  sub 16-8+4
  ld    (spat+61),a
  
  ld    a,(spat+65)
  add a,8-4
  ld    (spat+65),a
  ld    a,(spat+69)  
  add a,8-4
  ld    (spat+69),a  
  
  
;vertical
;  ld    a,(spat+56)
;  add a,16
;  ld    (spat+56),a
;  ld    a,(spat+60)
;  add a,16
;  ld    (spat+60),a

  
.continue:


	ld		hl,(invisspratttableaddress)		;sprite attribute table in VRAM ($17600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,spat			;sprite attribute table
	ld    de,56
	add   hl,de
  ld    c,$98
	call	outix16 ;only write the SPAT values for MARIO!


  call setpage12tohandleobjectmovement

  ret



MovementPattern177:     ;diagonal block (left and right)


;  ld (ix+standingonspr),0

  ;this movement pattern is recognized by the helper object DO NOT CHANGE!!
  ;you can however add any code in here and use it with another object

;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  ld de,16
;  xor a
;  push hl
;  sbc hl,de
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h
;  ld c,0
;  call platforminteract ;we now set the interaction byte for all active objects that interact with the platform
;  pop hl
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h


;  call mariointeract
;  jp c,handleboxinteract.handlesidetouch


  ret


MovementPattern176:     ;powerup static





  ld a,(ix+objectrelease)
  ld b,a

  push bc
  
  cp 3
  call z,.animate
  

  call mariointeract
  pop bc
  jp c,settransform

  ret


.animate:


    
  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animateforreal

             ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ret

.animateforreal:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc  a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex24  ;color address
  ld    bc,spriteaddrex24c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex25  ;color address
  ld    bc,spriteaddrex25c  ;character address
  ld    de,offsetshell 
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex26  ;color address ;02
  ld    bc,spriteaddrex26c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex27  ;color address ;03
  ld    bc,spriteaddrex27c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret




MovementPattern171:     ;chuck (trowing)

  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,.takefire
  cp 3
  jp z,otherobjectdeathjump


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 

  ld a,(ix+v4)
  dec a
  jp z,.jump
  dec a
  jp z,.dotrow


  call  checkobjecttilesfloor
  ret c

  call getmarioheight
  cp 48
  call nc,.setjump

.dotrow:

;TODO: add trowing routine
  inc (ix+v7)


  ld a,(ix+v7)
  cp 15
  jp z,.frame1
  cp 17
  jp z,.frame2
  cp 19
  jp z,.frame3
  cp 21
  jp z,.frame4
  cp 25
  ret c

  ;reset to default sprite
  ld (ix+v7),0
  ld (ix+v4),0
 
   ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  call getmarioleftright
  jp c,.right
 
 
  ld    hl,spriteaddrex395  ;color address ;02
  ld    bc,spriteaddrex395c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset
  
  
.right:
  
  ld    hl,spriteaddrex400  ;color address ;02
  ld    bc,spriteaddrex400c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset
  
  

.jump:

;TODO: add jump sequence
  
  ld e,(ix+v7)
  ld hl,.jumptable
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.endjump
  
  ld (ix+v2),a
  
  inc (ix+v7)
  
  jp addv2toybject
  

  
;  ret
  

.endjump:

;end jump sequence  
  ld (ix+v4),2
  ld (ix+v7),14

  ret

.jumptable: db -3,-3,-3,-3,-2,-2,-2,-1,-1,-1,-1,-1,0,-1,0,-1,0,-1,0,5
;  ret


.frame1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call getmarioleftright
  jp c,.frame1r

  ld    hl,spriteaddrex396  ;color address ;02
  ld    bc,spriteaddrex396c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset

.frame1r:
  
  ld    hl,spriteaddrex401  ;color address ;02
  ld    bc,spriteaddrex401c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset


.frame2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call getmarioleftright
  jp c,.frame2r


  ld    hl,spriteaddrex397  ;color address ;02
  ld    bc,spriteaddrex397c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset

.frame2r:
  
  ld    hl,spriteaddrex402  ;color address ;02
  ld    bc,spriteaddrex402c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset


.frame3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call getmarioleftright
  jp c,.frame3r

  ld    hl,spriteaddrex398  ;color address ;02
  ld    bc,spriteaddrex398c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset


.frame3r:
  
  ld    hl,spriteaddrex403  ;color address ;02
  ld    bc,spriteaddrex403c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset

.setv1right:

  ld a,3

  ret

.frame4:


  call getmarioleftright
  ld a,-3
  call c,.setv1right


;spawn the ball

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de 


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),161              ;set object number (baseball)
  ld    (iy+6),0              ;set object number (baseball)

  push af

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  pop af
  ld (ix+v1),a

  pop ix

  call getmarioleftright
  jp c,.frame4r


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex399  ;color address ;02
  ld    bc,spriteaddrex399c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset

.frame4r:
  
  ld    hl,spriteaddrex404  ;color address ;02
  ld    bc,spriteaddrex404c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset


.setjump:

  ld a,(ix+v7)
  and a
  ret nz ;wait until chuck is finished trowing any ball

  ld (ix+v4),1 ;set to jumping state
  ld (ix+v7),0 ;use this counter again

  ret


.takefire:

  ld a,(ix+hitpoints)
  dec a
  jp z,otherobjectdeathjump
  ld (ix+hitpoints),a

  ret

.nextframe:

 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
 
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+hitpoints),3
  ld (ix+movepat),35

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex67  ;color address ;02
  ld    bc,spriteaddrex67c  ;character address
  ld    de,offsetchuck
  ld    a,ingamespritesblock1
  call  setcharcolandoffset


;makestar verneukt de ix waarde be carefull
 call makestarright

  call Playsfx.chuckauw


  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
  ret




MovementPattern170:     ;spuugplant (bullet)

  call mariointeract

  ld a,(ix+v4)
  dec a
  jp z,.movedown

  ;launch bullet up check which type it is
  ld e,(ix+v7)
  ld hl,spuugplantbullettable1
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setmovedown
  
  ld (ix+v2),a

  call addv2toybject
  call addv1toxbject
  
  inc (ix+v7)


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3


.position0:
  ld    hl,spriteaddrex393  ;color address
  ld    bc,spriteaddrex393c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1:
.position3:
.position2:
  ld    hl,spriteaddrex394  ;color address
  ld    bc,spriteaddrex394c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.movedown:

  ;launch bullet up check which type it is
  ld e,(ix+v7)
  ld hl,spuugplantbullettable2
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  call z,.resettable
  
  ld (ix+v1),a

  call addv2toybject
  call addv1toxbject
  
  inc (ix+v7)

  jp .animate
  
  ;ret

.setmovedown:

  inc (ix+v4)
  ld (ix+v7),0
  ld (ix+v2),1

  ret

.resettable:

  ld (ix+v7),0
  xor a
  ret

spuugplantbullettable1: db -3,-3,-3,-3,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,0,-1,0,1,0,1,0,1,5
spuugplantbullettable2: db 0,1,0,1,0,1,0,1,0,-1,0,-1,0,-1,0,-1,5


MovementPattern169:     ;spuugplant

  call mariointeract

  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump



  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld (ix+v2),0

  call  checkobjecttilesfloor
  ret c


  inc (ix+v4)

  ld a,(ix+v4)
  cp 105+20+20
  jp nc,.reset
  cp 66+20+20
  jp nc,.frame3
  cp 65+20+20
  jp nc,.frame2
  cp 35+20+20+20
  jp nc,.frame1
  


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 3 frames
  jp  z,.animate

  ret

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
;  jp    z,.position1
  dec   a
  jp    z,.position2
;  dec   a
;  jp    z,.position3

.position0:
  ld    hl,spriteaddrex389  ;color address
  ld    bc,spriteaddrex389c  ;character address
  ld    de,offsetspuugplant
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex390  ;color address
  ld    bc,spriteaddrex390c  ;character address
  ld    de,offsetspuugplant
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



.frame1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position01
  dec   a
;  jp    z,.position1
  dec   a
  jp    z,.position21
;  dec   a
;  jp    z,.position3

.position01:
  ld    hl,spriteaddrex389  ;color address
  ld    bc,spriteaddrex389c  ;character address
  ld    de,offsetspuugplant
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position21:
  ld    hl,spriteaddrex392  ;color address
  ld    bc,spriteaddrex392c  ;character address
  ld    de,offsetspuugplant
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

   


  ret


.frame2:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;spawn the little bullets

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e

  push hl ;we need these two later
  push de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),159                ;set object number (spuugplant bullet)
  ld    (iy+6),0                ;set object number (spuugplant bullet)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v1),-1

  pop ix

  pop de
  pop hl

  push hl
  push de
  
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),159                ;set object number (spuugplant bullet)
  ld    (iy+6),0                ;set object number (spuugplant bullet)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v1),-2

  pop ix

  pop de
  pop hl

  push hl
  push de
  
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),159                ;set object number (spuugplant bullet)
  ld    (iy+6),0                ;set object number (spuugplant bullet)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v1),1

  pop ix
 
  pop de
  pop hl
  
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),159                ;set object number (spuugplant bullet)
  ld    (iy+6),0                ;set object number (spuugplant bullet)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v1),2

  pop ix  


.frame3:

  ld    hl,spriteaddrex391  ;color address
  ld    bc,spriteaddrex391c  ;character address
  ld    de,offsetspuugplant
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


 ; ret

.reset:

  ld (ix+v4),0

  ret


;  ret


MovementPattern175: ;tic tac toe


  ld a,(ix+v3)
  and a
  jp z,.init
  cp 5
  ret z


  ld a,(ix+v1)
  and a
  jp nz,.setturnred

;see what has to come out
  call objectinteract
  cp 3
  jp z,.releaseobject


;Failsafe. Check the tile around us and see if the box popped by a nearby object. If yes pop anyway
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 204
  jp z,.releaseobject


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3

  ret

.animate:
 
  
.position0:
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex68c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex69  ;color address
  ld    bc,spriteaddrex69c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex70  ;color address ;02
  ld    bc,spriteaddrex70c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex71  ;color address ;03
  ld    bc,spriteaddrex71c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.releaseobject:

  call makerandomnumber
  cp 6
  jp c,.setturnred

  call Playsfx.error
  
    push ix
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop ix
  
  ld b,204
  call createanyblock
 
  
  ld (ix+xobject),255
  ld (ix+xobject+1),255
 
  pop ix
  push ix

  ld l,(ix+v6)
  ld h,(ix+v6+1)
  push hl
  pop ix

  ld b,204
  call createanyblock
  
  ld (ix+xobject),255
  ld (ix+xobject+1),255

  pop ix

  ld b,204
  call createanyblock

  jp destroyobject

;  ret

;test

.setturnred:

  inc (ix+v1)
  
  ld a,(ix+v1)
  cp 5
  ret c

;  ret

;.turnred:


  ld (ix+v3),5

  ld b,209
  call createanyblock
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex409  ;color address
  ld    bc,spriteaddrex409c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

;two reds?
  push ix

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  
  push hl
  pop ix
  
  ld a,(ix+v3)
  cp 5
  pop ix
  ret nz
  
  push ix
  
  ld l,(ix+v6)
  ld h,(ix+v6+1)
  
  push hl
  pop ix
  
  ld a,(ix+v3)
  cp 5
  pop ix
  ret nz
  
;  ret

  ld b,204
  call createanyblock 

;reset the values and spawn the powerup :)
  ld (ix+v4),0
  ld (ix+v1),0
  ld (ix+v3),0
  ld (ix+v6),0

  call Playsfx.powerup

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),40
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;ret

;make the other two
.init:
 
  
  push ix
  exx
  pop hl ;block 1
  exx
  
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  
  ld de,6
  add hl,de
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  push de
  push hl


;create second block
  push ix


  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),163                ;set object number (make clone of self)
  ld    (iy+6),0                ;set object number (make clone of self)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v3),2

  push ix
  exx
  pop de ;block 2
  exx


  pop ix


  pop hl
  ld de,6
  add hl,de
  
  pop de

;create third block


  push ix

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),163                ;set object number (make clone of self)
  ld    (iy+6),0                ;set object number (make clone of self)

  call  activateobject


  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+v3),3

  push ix
  exx
  pop bc ;third block
  exx


  pop ix


  push ix
  exx
  push hl ;add to first block
  pop ix
  ld (ix+v5),e
  ld (ix+v5+1),d
  ld (ix+v6),c
  ld (ix+v6+1),b
  push de;add to second block
  pop ix
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld (ix+v6),c
  ld (ix+v6+1),b
  push bc;add to third block
  pop ix
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld (ix+v6),e
  ld (ix+v6+1),d
  exx
  pop ix

  inc (ix+v3)

  ret




MovementPattern174:     ;tubecoverup for mario exit (upwards)

  ld a,(ix+v7)
  and a
  jp nz,exitlevelup


  call mariointeract
  jp c,.exitlevel

  ret

;this routine is allowed to be slow. All other things are stopped if the conditions are true
.exitlevel:

  ld a,(mariolook)
  and a
  ret z


  xor a
  ld (standingonexit),a
  ld (mariolook),a

;all conditions set disable all the mariocontrols by disabling the physics engine
  ld a,1
  ld (nocontrol),a
  ;tell sprite engine that we are standing on a exit
  ld (standingonexit),a
  ld (marioduck),a
   
  ;center mario to the exact position as the tube coverup
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,11-8
  xor a
  sbc hl,de
  ld (mariox),hl


  ;fix the height
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,4
  xor a
  sbc hl,de
  ld (marioy),hl

  call Playsfx.pipe

  inc (ix+v7)


  ret


exitlevelup:


  ld a,(ix+v4)
  cp 31
  jp nc,.endlevel

  ld hl,(marioy)
  dec hl
  ld (marioy),hl
  
  inc (ix+v4)

;hide the mario, yoshi and shell sprites

  ld a,(marioyoshi)
  dec a
  jp z,.hidemarioyoshi


  ld a,(marioshell)
  and a
  call nz,.hidemarioshell



  ld a,(ix+v4)
  cp 8-1
  jp c,.part1


  ld a,(ix+v4)
  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  sub 4+1
  call hidemariospritedown
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	xor a
	jp hidemariospritedown


;ret


.part1:


  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	ld a,(ix+v4)
  add a,8+4-1
  jp hidemariospritedown

  
;ret


.hidemarioshell:


  ld hl,(holdingix)
  ld bc,0
  call getobjectspriteadress

  ld a,(ix+v4)
  cp 5
  ret c ;prevent feeding the wrong value into the call
  sub 4
  call hidemariospritedown 


  ret



.hidemarioyoshi:

 ;       jp .marioonyoshi ;temp debug

;yoshi
;  ld a,(ix+v4)
;  cp 17
;  jp c,.part1yoshi


;part2yoshi
  
  ld hl,(yoshiheadix) ;the body is now completely submerged
  ld bc,0
  call getobjectspriteadress
  
  xor a
  call hidemariosprite

  ld hl,(yoshiix)
  ld bc,0
  call getobjectspriteadress
  
  ld a,(ix+v4)
  cp 5
  jp c,.nope
  sub 4
  call hidemariospritedown 

.nope:  



  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  ld a,(ix+v4)
  call hidemariospritedown
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
	xor a
	jp hidemariospritedown



 

.endlevel:

;wait just a little bit longer to make it look nicer
  inc (ix+v4)


  ld de,mariosprite2
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite
  ld de,mariosprite1
	ld		hl,(invissprchatableaddress)		;sprite character table in VRAM ($17800)
	add hl,de
  xor a
  call hidemariosprite 


  ld a,(ix+v4)
  cp 40
  ret nz

  ld a,1 ;exit number
  ld (levelend?),a

  jp destroyobject

;ret



MovementPattern173:     ;tubecoverup for mario exit right out

  ld a,(ix+v7)
  cp 1
  jp z,.exitlevel
  cp 2
  jp z,.setendlevel

  ld a,(mariogod)
  and a
  jp nz,.endmariogod

  ld a,(marioduck)
  and a
  ret nz
  
  ld a,(marioleftright)
  and a
  ret z
  
  ld a,(marioleftrightspr)
  and a
  ret z

  ld a,(mariospeed)
  and a
  ret z
  
  ld a,(fallspeed)
  and a
  ret nz


  call mariointeract
  jp c,.checkheight

  ret

.endmariogod:

  xor a
  ld (mariogod),a
  ret


.checkheight:

;we cannot check the tiles below mario from here so we simply fetchthe height forcing mario to stand on the ground
  call getmarioheight
  cp 1
  ret nc

;store the y info because of some stupid bug
;  ld hl,(marioy)
;
;  ld (ix+v1),l
;  ld (ix+v2),h


;force mario to duck if on yoshi
  ld a,(marioyoshi)
  dec a
  call z,.forceduck



  ld a,4 ;nocontrol mode 4 means we exit through a horizontal tube
  ld (nocontrol),a

  ld (ix+v7),1

  call Playsfx.pipe ;play exit sound

  ret






.exitlevel:


  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,.negative ;mario is on the left side of the tube so do nothing just yet

  ld a,l
  cp 5
    
  
  jp nc,.setendlevel


;place spritehiding stuff here
;hiero
.negative:


;get the y coordinates back
 ; ld l,(ix+v1)
 ; ld h,(ix+v2)
 ; ld (marioy),hl


  ld hl,(mariox)
  inc hl
  ld (mariox),hl

  ;enable the spritehider
  
  ld a,1
  ld (spritehideractive),a

  ret

.setendlevel: 




  xor a
  ld (fallspeed),a

  ld (ix+v7),2


;wait some time, this will make it look smooth  
  inc (ix+v3)
  ld a,(ix+v3)
  cp 10
  ret c



.endlevel:

  call disablescreen

  ld a,1
  ld (levelend?),a

  xor a
  ld (marioduck),a

  ld hl,(mariox)
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  srl h
  rr l ;/16 ;make it small so it fits in one byte easier for us
  ld a,l
  ld (exitx),a


  ret

.forceduck:

  ld a,1
  ld (marioduck),a
 
 
 	ld		a,(VDP_8+15)
  ld    b,a
 
  
  ld a,b
  sub 6
  ld b,a
  
  
    ld    a,(mariocoordinates)            ;y
  add   a,b                             ;add vert scroll offset
  ld    (spat+56),a
  ld    (spat+60),a
  add   a,16
  ld    (spat+64),a
  ld    (spat+68),a


  ret





MovementPattern172:     ;baseball



  call mariointeract
  
  call addv1toxbject
  call addv2toybject


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 3 frames
  call  z,.animate

  
  ld a,(ix+v4)
  dec a
  jp z,.fall
  
  
  call getmariodistance
  cp 32
  jp c,.setfall


  ret


.setfall:
 
  inc (ix+v4)
 
  ret


.fall:

  ld e,(ix+v7)
  ld hl,baseballfalltable
  ld d,0
  add hl,de
  ld a,(hl)
  cp 6
  ret z
  
  ld (ix+v2),a
  
  inc (ix+v7)


  ret

.animate:

   ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v3)

  ld a,(ix+v3)
  dec a
  jp z,.frame1
  dec a
  jp z,.frame2
  dec a
  jp z,.frame3
  dec a
  jp z,.frame4
 
  ld (ix+v3),0

.frame1:

  ld    hl,spriteaddrex405  ;color address ;02
  ld    bc,spriteaddrex405c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset

.frame2:

  ld    hl,spriteaddrex406  ;color address ;02
  ld    bc,spriteaddrex406c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset
  
.frame3:

  ld    hl,spriteaddrex407  ;color address ;02
  ld    bc,spriteaddrex407c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset
  
.frame4:

  ld    hl,spriteaddrex408  ;color address ;02
  ld    bc,spriteaddrex408c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp  setcharcolandoffset    

  


baseballfalltable:  db 1,1,1,2,2,3,3,4,5,6



