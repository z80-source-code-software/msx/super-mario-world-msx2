
;next block of objectmovements

HandleObjectMovement2:

;start = 80


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz


  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress2 - 80 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)



;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress2
;  add   hl,de
;  ;substract minimum base adres
;  ld de,80*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress2:
  jp    MovementPattern80     ;shell upside down flyup after kick (similar to movement pattern 12 with different rules)
  jp    MovementPattern81     ;shell upside down fall (similar to movement pattern 13 with different rules)
  jp    MovementPattern82     ;shell upside down hold (similar to movement pattern 11 with different rules)
  jp    MovementPattern83     ;shell upside down (similar to movement pattern 1 with different rules)
  jp    MovementPattern84     ;shell upside down bounce (similar to movement pattern 21 with different rules)
  jp    MovementPattern85     ;shell upside down flyup (similar to movement pattern 12 with different rules)
  jp    MovementPattern86     ;montymole spawn
  jp    MovementPattern87     ;montymole spawn (jump up)
  jp    MovementPattern88     ;montymole spawn (falling down)
  jp    MovementPattern89     ;destroygrounddebris
  jp    MovementPattern90     ;montymole
  jp    MovementPattern91     ;sparkle
  jp    MovementPattern92     ;climbing plant
  jp    MovementPattern93     ;p-box
  jp    MovementPattern94     ;p-box (hold)
  jp    MovementPattern95     ;p-box (shoot up)
  jp    MovementPattern96     ;p-box (fall)
  jp    MovementPattern97     ;p-box (bounce)
  jp    MovementPattern98     ;p-box (flat)
  jp    MovementPattern99     ;p-box (kick)
  jp    MovementPattern100    ;coin (static)
  jp    MovementPattern101    ;yoshi head (passive)
  jp    MovementPattern102    ;feather1+2
  jp    MovementPattern103    ;activeyoshibody
  jp    MovementPattern104    ;activeyoshihead
  jp    MovementPattern105    ;yoshi body (passive)
  jp    MovementPattern106    ;yoshi body (beserk)
  jp    MovementPattern107    ;yoshi head (beserk)
  jp    MovementPattern108    ;red flame sidewards
  jp    MovementPattern109    ;pbox (blue-static)
  jp    MovementPattern110    ;blue rotating block
  jp    MovementPattern111    ;blue rotating block (shooting away)
  jp    MovementPattern112    ;flying box (alternate movement)
  jp    MovementPattern113    ;mariolevelexit coverupsprite
  jp    MovementPattern114    ;red flying koopa (from left to right)
  jp    MovementPattern115    ;flat elevator
  jp    MovementPattern116    ;green flying koopa ()
  jp    MovementPattern117    ;special green starbox
  jp    MovementPattern118    ;rotating platform
  jp    MovementPattern119    ;expanding smileybox gfx updater
  jp    MovementPattern120    ;expanding smileybox platform
  jp    MovementPattern121    ;lava (mario killer)


  
  
.return:
  
  ret


MovementPattern121:    ;lava (mario killer)

  ;follow mario x at ALL times
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ld a,(currentlevel)
  cp 51
  jp z,.checktile
  cp 52
  jp z,.checktile
  cp 55
  jp z,.checktile
  cp 86
  jp z,.killmario
  cp 170
  jp z,.killmario  
  
  call mariointeract
  ret nc

.kill:

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  jp destroyyoshi

 ; ret



.checktile:

  ld hl,(marioy)
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld bc,48
  ld de,0
  call objecttilecheckfromrom
  cp 180
  jp z,.kill


  ret


.killmario:

  ld hl,(marioy)
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  

  ld bc,24
  ld de,0
  call objecttilecheckfromrom
;  ld de,0
;  ld b,1
;  call checkgeneraltilefromhrom
  cp 36
  jp z,.kill
  cp 37
  jp z,.kill
  cp 38
  jp z,.kill
  cp 39
  jp z,.kill

  ret




MovementPattern120:    ;expanding smileybox platform
;TODO: change the expansion on the basis of objectrelease number 0 or 1

  ld c,0
  call platforminteract ;make this object a platform
  
  ld a,(ix+v3)
  and a
  jp z,.setbuffer

;limit the distance to inscreen to prevent spawning bugs  
  call getmariodistance
  cp 120
  ret nc


  ld a,(ix+offsets)
  dec a
  jp z,verticalexpandbox


  ld a,(shrinkingboxcounter)
  bit 7,a
  jp nz,.step1
  cp 1
  jp z,.step1
  cp 3
  jp z,.step2
  cp 5
  jp z,.step3
  cp 7
  jp z,.step4
  cp 9
  jp z,.step5
  cp 11
  jp z,.step6
  cp 13
  jp z,.step7
  cp 15
  jp z,.step8
  cp 16
  jp nc,.step9

  ret


.checkhorver:

  ;use ix+offsetts (object is spriteless anyway) as counter do horizontal twice. vertical twice then switch back to horizontal

  call changeslidingboxgfx
  
  ld (ix+offsets),1
  ld (ix+offsets+5),0
  

  ret


.step1:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld b,(ix+hitpoints)

  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+sizeX),b

  ld a,(ix+objectrelease)
  and a
  ret z

  ld (ix+offsets+5),1 ;just set this to 1 so we know we passed this step
  
  ret

.step2:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,4
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 8
  
  ld (ix+sizeX),a
  
  ret


.step3:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,8
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 16
  
  ld (ix+sizeX),a
  
  ret

.step4:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,12
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 24
  
  ld (ix+sizeX),a
  
  ret

.step5:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,16
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 32
  
  ld (ix+sizeX),a
  
  ret

.step6:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,20
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 40
  
  ld (ix+sizeX),a
  
  ret


.step7:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,24
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 48
  
  ld (ix+sizeX),a
  
  ret


.step8:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,28
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 56
  
  ld (ix+sizeX),a
  
  ret


.step9:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld a,(ix+hitpoints)

  ld de,32
  add hl,de


  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  sub 64
  
  ld (ix+sizeX),a
 
  ld a,(ix+offsets+5) ;we passed step 1 so we change into vertical mode
  dec a
  jp z,.checkhorver
  
  ret


.setbuffer:
;store the data in a internal buffer for the first time so that we can always refer to these initial values


  ld (ix+v3),1

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld a,(ix+sizeX)

  ld (ix+v5),l
  ld (ix+v5+1),h
  ld (ix+hitpoints),a
  
  ld (ix+offsets),0

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld (ix+offsets+1),l
  ld (ix+offsets+2),h

  
  ret



verticalexpandbox:

  call mariointeract
  call c,.setsidetouch


  ld a,(shrinkingboxcounter)
  bit 7,a
  jp nz,.step1
  cp 1
  jp z,.step1
  cp 3
  jp z,.step2
  cp 5
  jp z,.step3
  cp 7
  jp z,.step4
  cp 9
  jp z,.step5
  cp 11
  jp z,.step6
  cp 13
  jp z,.step7
  cp 15
  jp z,.step8
  cp 16
  jp nc,.step9

  ret


.setsidetouch:

  ld a,(standingonsprite)
  dec a
  ret z

  ld a,1
  ld (touchside),a
  ret


.checkhorver:

  ;use ix+offsetts (object is spriteless anyway) as counter do horizontal twice. vertical twice then switch back to horizontal

  call changeslidingboxgfx
  
  ld (ix+offsets),0
  ld (ix+offsets+5),0
  

  ret


;set 32 pixels higher and make the width constant. Delevel and set yobject properly
.step1:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld de,32
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,32
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,80 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
 
  ld a,(ix+objectrelease)
  and a
  ret z
 
 
   ld (ix+offsets+5),1 ;just set this to 1 so we know we passed this step
 
  
  ret

.step2:


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,28
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,72 ;height *2+16


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step3:


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,24
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,64 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step4:

  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,20
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,56 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step5:


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,16
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,48 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step6:


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,12
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,40 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step7:


  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,08
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,32 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step8:

  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  ld de,04
  xor a
  sbc hl,de ;y -16
  
  ld b,16
  
  ld c,24 ;height *2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  ret




.step9:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld de,32
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h



  ld l,(ix+offsets+1)
  ld h,(ix+offsets+2)
  
  ld b,16
  
  ld c,16 ;height *2+2


  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ld (ix+sizeX),b
  ld (ix+sizeY),c
  
  
  ld a,(ix+offsets+5) ;we passed step 1 so we change into vertical mode
  dec a
  jp z,.checkhorver
  
  
  ret


  




MovementPattern119:    ;expanding smileybox gfx updater




  ld a,(shrinkingboxcounter)
  cp 1
  call z,.step1
  ld a,(shrinkingboxcounter)
  cp 3
  call z,.step2
  ld a,(shrinkingboxcounter)
  cp 5
  call z,.step3
  ld a,(shrinkingboxcounter)
  cp 7
  call z,.step4
  ld a,(shrinkingboxcounter)
  cp 9
  call z,.step5
  ld a,(shrinkingboxcounter)
  cp 11
  call z,.step6
  ld a,(shrinkingboxcounter)
  cp 13
  call z,.step7
  ld a,(shrinkingboxcounter)
  cp 15
  call z,.step8
  ld a,(shrinkingboxcounter)
  cp 17
  call z,.step9

  ld a,(shrinkingboxcounter)
  cp 40
  call z,.reset
  
    

  ld a,(shrinkingboxv4)
  dec a
  jp z,.expand



 ; ret

.shrink:

  ld a,(shrinkingboxcounter)
  inc a
  ld (shrinkingboxcounter),a


  ret


.expand:

  ld a,(shrinkingboxcounter)
  dec a
  cp -40
  jp z,.reset0
  ld (shrinkingboxcounter),a

  ret

.reset0:

  xor a
  ld (shrinkingboxv4),a


  ret


.reset:

  ld a,1
  ld (shrinkingboxv4),a

 ; xor a
 ; ld (shrinkingboxcounter),a


  ret 


.step1:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*00)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*00)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*00)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*00)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical change

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*00)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*00)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*00)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*00)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*00)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*00)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*00)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*00)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  


  call setpage12tohandleobjectmovement
 

  ret

.step2:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*20)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*20)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*20)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*20)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*20)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;verticall movement


;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*20)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*20)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*20)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*20)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*20)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*20)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*20)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*20)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret


.step3:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*40)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*40)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*40)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*40)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*40)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*40)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*40)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*40)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical



;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*40)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*40)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*40)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*40)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*40)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*40)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*40)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*40)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret


.step4:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*60)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*60)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*60)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*60)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*60)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical


;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*60)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*60)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*60)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*60)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*60)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*60)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*60)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*60)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret


.step5:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*80)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*80)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*80)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*80)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*80)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*80)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*80)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*80)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical



;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*80)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*80)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*80)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*80)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*80)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*80)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*80)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*80)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret


.step6:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*100)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*100)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*100)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*100)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*100)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*100)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*100)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*100)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical



;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*100)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*100)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*100)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*100)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*100)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*100)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*100)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*100)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret



.step7:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*120)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*120)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*120)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*120)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*120)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*120)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*120)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*120)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical


;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*120)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*120)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*120)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*120)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*120)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*120)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*120)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*120)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret


.step8:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*140)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*140)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*140)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*140)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*140)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*140)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*140)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*140)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    


;vertical


;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*140)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*140)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*140)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*140)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*140)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*140)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*140)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*140)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    


  call setpage12tohandleobjectmovement
 

  ret

.step9:


  ld    a,animationtilesblock
  call  block12

  ld c,$98

;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*160)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*160)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*160)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+108*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddr+(8*160)         ;rotating tile animation 1 character address in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*160)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*160)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*160)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+108*8                                ;vram address of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddr+(8*160)         ;rotating tile animation 1 color address in rom
  ld    b,8*20
  otir    

;vertical


;write character data
  ;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*160)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*160)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*160)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+128*8                                ;vram addrvess of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  ld    hl,shiftingboxcharacteraddrv+(8*160)         ;rotating tile animation 1 character addrvess in rom
  ld    b,8*20
  otir

;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*160)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir
  ;page 2
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*160)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 3
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*160)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir  
  ;page 4
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+128*8                                ;vram addrvess of colortile to write to
  call  SetVdp_Write                                  ;set vdp to write at color of tile 106
  ld    hl,shiftingboxcoloraddrv+(8*160)         ;rotating tile animation 1 color addrvess in rom
  ld    b,8*20
  otir    



  call setpage12tohandleobjectmovement
 

  ret



MovementPattern118: ;rotating platform
;v4 = 1 move clockwise
;v4 = 2 move counterclockwise

  ld c,0
  call platforminteract ;make this object a platform

  ld a,(ix+objectrelease)
  and a
  jp nz,.handlestaticplatform

  call mariointeract
  jp c,.accrotateplatform ;accelerate

  ld a,(framecounter)
  and 16
  call z,.animate
  

  ld a,(ix+v4)
  and a
  jp nz,.decrotateplatform

  ret

.handlestaticplatform:


  ld a,(framecounter)
  and 3
  call z,.animatestatic

  ld hl,rotatingplatformtablex
  ld de,rotatingplatformtabley
  ld hl,smallrotatex
  ld de,smallrotatey
  ld b,2
  call rotateobject

  ret


.animatestatic:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+hitpoints)               ;v3 is used as animation variable
  xor   1
  ld    (ix+hitpoints),a               ;v3 is used as animation variable
  jp    z,.position0s
  dec   a
  jp    z,.position1s


.position0s:
  ld    hl,spriteaddrex235  ;color address
  ld    bc,spriteaddrex235c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.position1s:
  ld    hl,spriteaddrex236  ;color address
  ld    bc,spriteaddrex236c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
  
  ;ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    hl,spriteaddrex235  ;color address
  ld    bc,spriteaddrex235c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.setv4left:
 
 
  ;formula max distance limit counterclockwise movement = (500 - distancecrossed) + lenghtrotationspeed
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld hl,500
  xor a
  sbc hl,de
 
  ; hl = (500 - distancecrossed)
  ld de,lenghtrotationspeed
  add hl,de
 
  ld (ix+counter),l
  ld (ix+counter+1),h
 

  ld (ix+v4),2

;compensate for the change of direction
  ld e,(ix+v5)
  ld d,(ix+v5+1)

  dec de ;cclockwise
  
  ld (ix+v5),e
  ld (ix+v5+1),d



  jp .donesetv4



.decrotateplatform:


;get the rotation speed

  ld e,(ix+v3)
  ld hl,rotationspeed
  ld d,0
  xor a
  add hl,de
  ld a,(hl)
  ld b,a
  cp 0
  jp z,.donedec
  dec (ix+v3)

  jp .rotateplatform

.donedec:

  ld (ix+v4),0

  ret


.accrotateplatform:


  ld a,(standingonsprite) ;mario is not standing on the platform yet
  and a
  ret z

  ld a,(framecounter)
  and 3
  jp nz,.continue


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+hitpoints)               ;v3 is used as animation variable
  xor   1
  ld    (ix+hitpoints),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,spriteaddrex235  ;color address
  ld    bc,spriteaddrex235c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  jp .continue

.position1:
  ld    hl,spriteaddrex236  ;color address
  ld    bc,spriteaddrex236c  ;character address
  ld    de,offsetplatform
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

.continue:

  ld a,(ix+v4)
  and a
  jp z,.donechecklimit
  cp 2
  jp z,.checklimitleft

.checklimitright:
;see if we have reached the right outer limit
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld l,(ix+counter)
  ld h,(ix+counter+1)
  xor a
  sbc hl,de
  jp c,.decrotateplatform
  
  jp .donechecklimit


.checklimitleft:

  ;see if we have reached the left outer limit
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+counter)
  ld d,(ix+counter+1)
  xor a
  sbc hl,de
  jp c,.decrotateplatform
  


.donechecklimit:

  ld a,(ix+v4)
  and a
  jp nz,.donesetv4 ;set this only once so check the value ahead
  
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld hl,250
  xor a
  sbc hl,de
  jp c,.setv4left
  
  ;clockwise rotation maximum = ((250-distance crossed)*2)-lenghtrotationspeed+distance crossed
  ;hl = (250-distance crossed)
  sla l
  rl h ;hl*2
  ;hl = ((250-distance crossed)*2)
  add hl,de
  ;hl = ((250-distance crossed)*2) +distance crossed
  ld de,lenghtrotationspeed
  xor a
  sbc hl,de
  ld (ix+counter),l
  ld (ix+counter+1),h  

  ld (ix+v4),1 

  ld e,(ix+v5)
  ld d,(ix+v5+1)

  inc de ;clockwise
  
  ld (ix+v5),e
  ld (ix+v5+1),d


.donesetv4:
  

;get the rotation speed

  ld e,(ix+v3)
  ld hl,rotationspeed
  ld d,0
  add hl,de
  ld a,(hl)
  ld b,a
  cp 5
  jp z,.rotateplatform
  cp 0
  call z,.setone
  inc (ix+v3)
  


.rotateplatform:

  ld a,(ix+v4)
  cp 2
  jp z,.rotateplatformleft



.rotateplatformright:


;reset the v1 and v2 values
  push bc

  ld (ix+v1),0
  ld (ix+v2),0
  ld bc,0
  ld (bcvar),bc


  pop bc

.rotationloop:


  ld e,(ix+v5)
  ld d,(ix+v5+1)

  ld hl,rotatingplatformtablex
;  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  call z,.resetloop
  push bc ;save the b value
  ld bc,(bcvar)
  add a,b
  ld b,a
    ld (bcvar),bc
  ld a,(hl)
  ld (ix+v1),a
  push de
  call addv1toxbject
  pop de
  ld hl,rotatingplatformtabley
;  ld d,0
  add hl,de
  ld a,(hl)
  ld bc,(bcvar)
  add a,c
  ld c,a
    ld (bcvar),bc
  ld a,(hl)
  ld (ix+v2),a
  push de
  call addv2toyobject
  pop de 

  inc de ;clockwise
  
  ld (ix+v5),e
  ld (ix+v5+1),d


  pop bc
  
  djnz .rotationloop

  ld bc,(bcvar)
  ld (ix+v1),b
  ld (ix+v2),c
 
  ret




.rotateplatformleft:


;reset the v1 and v2 values
  push bc

  ld (ix+v1),0
  ld (ix+v2),0
  ld bc,0
  ld (bcvar),bc


  pop bc

.rotationloopleft:


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld de,0
  xor a
  sbc hl,de
  jp z,.resetloopleft

  ex de,hl

;  ld e,(ix+v5)
;  ld d,(ix+v5+1)

  ld hl,rotatingplatformtablex
;  ld d,0
  add hl,de
  ld a,(hl)
;  cp 10
;  call z,.resetloopleft
  push bc ;save the b value
  ld bc,(bcvar)
  add a,b
  ld b,a
    ld (bcvar),bc
  ld a,(hl)
  neg
  ld (ix+v1),a
  push de
  call addv1toxbject
  pop de
  ld hl,rotatingplatformtabley
;  ld d,0
  add hl,de
  ld a,(hl)
  ld bc,(bcvar)
  add a,c
  ld c,a
    ld (bcvar),bc
  ld a,(hl)
  neg
  ld (ix+v2),a
  push de
  call addv2toyobject
  pop de 

  dec de ;counterclockwise
  
  ld (ix+v5),e
  ld (ix+v5+1),d


  pop bc
  
  djnz .rotationloopleft

  ld bc,(bcvar)
  
  ld a,b
  neg
  ld b,a
  
  ld a,c
  neg
  ld c,a
  
  ld (ix+v1),b
  ld (ix+v2),c
 
  ret

  

.resetloopleft:


  ld de,499

  ld (ix+v5),e
  ld (ix+v5+1),d

;handle first x bit as compensation
  ld a,1
  neg
  ld (ix+v1),a
  call addv1toxbject

  ld a,b
  neg
  ld (ix+v1),a


  ret



.resetloop:

  ld (ix+v5),0
  ld (ix+v5+1),0

  ld hl,rotatingplatformtablex
  ld de,0
  ld a,(hl)

  ret

.setone:
  
  ld b,1


  ret


rotationspeed:           db 0,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,2,2,2,2,2,2,3,3,3,3,4,4,4,4,5
lenghtrotationspeed: equ $-rotationspeed

;rotation table made possible by NYYRIKKI
rotatingplatformtablex:
    db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
    db 1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,0
    db 1,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,0,1,0,1,0,1,0
    db 0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,-1,0
    db 0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1
    db 0,-1,0,0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,-1,0,-1,-1,0
    db -1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1
    db -1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,0,-1,-1,-1,0,-1,0
    db -1,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,0,-1,0,-1,0,-1,0,0,-1,0,0,-1,0
    db 0,0,-1,0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,-1,1,0,0,0,0,0,0,0
    db 0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,0,1,0,0,1,0
    db 1,0,1,0,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1
    db 1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1
    db 1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,10
rotatingplatformtabley:
    db 0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0
    db 1,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0
    db 1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1
    db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
    db 1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
    db 1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,0,1,1,1
    db 0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,0,1,0,1,0,1,0,0,1,0
    db 0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,-1,0,0,0,0
    db 0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,0,-1,0,0,0,-1,0,0,-1,0,0,-1,0,-1,0,-1,0
    db 0,-1,0,-1,0,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,0,-1,-1,0,-1,0,-1,-1,-1,0,-1,-1,0,-1,-1,-1
    db -1,0,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
    db -1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1
    db -1,-1,-1,-1,0,-1,-1,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,-1,0,-1,-1,0,-1,-1,-1,0,-1,0,-1,-1,0
    db -1,-1,0,-1,0,-1,-1,0,-1,0,-1,0,-1,0,-1,0,0,-1,0,-1,0,-1,0,0,-1,0,0,-1,0,0,0,-1
    db 0,0,0,-1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,-1

;table was rendered in MSXbasic using the following basic script:
;
;predrawing of the circle to check results
;10 SCREEN 5
;20 AP=ATN(1)*2:AT=AP*4
;30 SPRITE$(0)="@"
;40 FOR I=0 TO 255
;50 X=-SIN(-AT/256*I)*80
;60 Y=-SIN(-AT/256*I+AP)*80
;70 PUT SPRITE 0,(X+128,Y+106),8
;80 IF INKEY$>"" THEN END
;90 NEXT I:GOTO 40
;
;
;10 ' To Excel
;20 DEFINT X,Y
;30 OPEN "XY.CSV" FOR OUTPUT AS #1
;40 AP=ATN(1)*2:AT=AP*4
;50 FOR I=0 TO 499
;60 X=-SIN(-AT/500*I)*80
;70 Y=-SIN(-AT/500*I+AP)*80
;80 PRINT#1,X;",";Y
;90 NEXT I:CLOSE

;start = 84

MovementPattern84:

  ld (ix+hitpoints),0

;altijd kunnen oppakken no matter what
  call mariointeract
  jp c,.handleinteract


.notdone:

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a

    ld a,(ix+v1)
    cp 0
    jp nc,.negate
    jp z,.nox
    dec a
    jp z,.nox
    ld (ix+v1),a

  call addv1toxbject
  jp .nox

.negate:

    ld a,(ix+v1)
    jp z,.nox
    inc a
    jp z,.nox
    ld (ix+v1),a
  


.nox:

  ld hl,shellbouncetable
  ld d,0
  ld e,(ix+v4)
  add hl,de


  ld a,(ix+v4)
  cp 13
  jp nc,.done
  cp 1
  jp z,.up
  cp 2
  jp z,.up
  cp 3
  jp z,.up
  cp 4 ;d
  jp z,.down
  cp 5
  jp z,.down
  cp 6
  jp z,.down
  cp 7 ;u
  jp z,.up
  cp 8
  jp z,.up
  cp 9 ;d
  jp z,.down
  cp 10
  jp z,.down
  cp 11 ;u
  jp z,.up
  cp 12 ;d
  jp z,.down



  ret

;shellbouncetable: db 0,3,2,1,1,2,3,2,1,1,2,1,1,0


.up:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call subv2toybject
  
  
  ret

.down:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toybject

  
  ret



.handleinteract:

  ld a,(mariospinjump)
  and a
  jp nz,destroyobjectwithcloud 
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud


.done:

  ld (ix+movepat),83
  ld (ix+objectfp),0
  ld (ix+v1),1 ;for the shaking

;  jp MovementPattern01.nextframe
  
  
  ret



MovementPattern85:


  ld (ix+hitpoints),0

  call objectinteract
  cp 1
  jp z,.nextframe



;  call mariointeract
;  jp c,MovementPattern01.nextframe
  

 

  
  ld a,(ix+deathtimer)
  cp 8
 ld b,8 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown


;check upwards twice
  call shelluptilecheck
  jp c,.nextframe 
 
  
 call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
 call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  call checkobjecttilelr
  jp c,.nextframe2
 

  ret


 
 
;gaan we dropcheck in douwen


.nextframe:

  ld a,81
  ld (ix+movepat),a
  
  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a

  ret

.nextframe2:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:

  call lockobjectx.right

  xor a
  ld (ix+v1),a

  ret


.slowdown:

  
  sub b
  ld d,0
  ld e,a
  ld hl,shelludfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe
  
  
  ld (ix+v2),a

;  call shelluptilecheck
;  jp c,.nextframe
 
  call shelluptilecheck
  jp c,.nextframe 

  call  subv2toybject
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  

  call checkobjecttilelr
  jp c,.nextframe2




  ret




MovementPattern86:

  call getmariodistance
  cp 50
  call c,.activate

  ld a,(ix+v4)
  and a
  ret z


  ld a,(ix+v1)
  cp 50
  inc (ix+v1)
  jp nc,.nextframe


  call animatemontyrelease

  ret

.nextframe:

  ld (ix+v1),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+movepat),87
  ld (ix+deathtimer),0
  ld (ix+deadly),1
  ld (ix+edible),1
  ld (ix+clipping),1
  ld (ix+objectreturn),1

;make the hole
  ld a,(ix+objectrelease)
  and a
  call z,makemontyhole
  ld a,(ix+objectrelease)
  cp 2
  call z,makemontyhole

;spawn the debris

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


  push ix

  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),de
  
;TODO: we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),2
    ld (ix+v2),8
    ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  ld de,(blockbcstorage)

;object 2
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
        ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)

;object 3

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
    ld (ix+v2),8
        ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)

;object 4

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),2
        ld (ix+movepat),89

  pop ix


  call Playsfx.breakblock

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex153  ;color address ;02
  ld    bc,spriteaddrex153c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret


.activate:

  ld (ix+v4),1


  ret



animatemontyrelease:

  
  ld a,(ix+objectrelease)
  cp 1
  jp z,animateflatmontyrelease
  cp 3
  jp z,animateflatmontyrelease

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 8
  jp    z,.position1
  cp 16
  jp    z,.position2
  cp 25
  ret c
 

  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex145  ;color address
  ld    bc,spriteaddrex145c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex146  ;color address
  ld    bc,spriteaddrex146c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex147  ;color address ;02
  ld    bc,spriteaddrex147c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


 ; ret

animateflatmontyrelease:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 8
  jp    z,.position1
  cp 16
  jp    z,.position2
  cp 25
  ret c
 

  xor a
  ld (ix+v3),a

 
.position0:
  ld    hl,spriteaddrex148  ;color address
  ld    bc,spriteaddrex148c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex149  ;color address
  ld    bc,spriteaddrex149c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex148  ;color address ;02
  ld    bc,spriteaddrex148c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret




MovementPattern87:

  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


 
  call mariointeract
  jp c,.killmontey
  
  
  ld a,(ix+deathtimer)
  cp 8
 ld b,8 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown


 call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

 

  ret
 
;gaan we dropcheck in douwen


.nextframe:

  ld (ix+movepat),88
  
  ld (ix+deathtimer),0
  ld (ix+v2),0

  ret


.slowdown:

  
  sub b
  ld d,0
  ld e,a
  ld hl,shellfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe
  
  ld (ix+v2),a

  call  subv2toybject

  ret

.killmontey:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud
  
    ld (ix+deadly),0
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex154  ;color address
  ld    bc,spriteaddrex154c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  call Playsfx.kickenemy

;  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
    call makestarleft
  
  ret


MovementPattern88:


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump
 

 call mariointeract
 jp c,.killmontey
  
  ld a,(ix+deathtimer)
  cp 10
; ld b,15 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  call c,.slowdown

 call  addv2toybject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
 

  jp checktilesmovpattern88
  
.returnfrompage3: 
  
;gaan we dropcheck in douwen

  ret

.nextframe:

;prevent sprite stucking
  call objectinteract
  and a
  jp nz,.moveonetile


  ld a,(ix+yobject)
  and %11111000
  ld (ix+yobject),a
  
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),11 ;middle of the acceleration pointer
  ld (ix+movepat),90
  ld (ix+deathtimer),0


  ret



.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ld (ix+v2),0

  
  ret

.slowdown:

;  sub b
  ld d,0
  ld e,a
  ld hl,shellfallacctable
  add hl,de
  ld a,(hl)
  
  
  ld (ix+v2),a
;  call  addv2toybject


  ret

.killmontey:


 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud
  
  ld (ix+deadly),0
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex154  ;color address
  ld    bc,spriteaddrex154c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  call Playsfx.kickenemy

;  ret

.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
    call makestarleft
  
  ret




MovementPattern89:



  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  call objectdeathjump

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2


  
  xor a
  ld (ix+v3),a
  
.position0:
  ld    hl,spriteaddrex150  ;color address
  ld    bc,spriteaddrex150c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex151  ;color address
  ld    bc,spriteaddrex151c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex152  ;color address ;02
  ld    bc,spriteaddrex152c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;ret




MovementPattern90:

  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.killmontey
  
  call setspriteleftright ;orient the sprite at mario all the time

  call acceleratemole ;set the speed and slide accelerate when required
  
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  
  ld a,(ix+deathtimer)
  cp 50
  jp nc,.goup

.return:

 
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

.skipfall:

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  


  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animatemole


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret

;make a little jump once in a while
.goup:

;following mole does not jump
  ld a,(ix+objectrelease)
  cp 2
  jp z,.return
  cp 3
  jp z,.return

  ld (ix+v2),3
  call subv2toybject
  
  ld a,(ix+deathtimer)
  cp 55
  jp nc,.resetgoup
  
  ld  (ix+objectfp),0
  
  
  jp .skipfall

.resetgoup:

  ld (ix+deathtimer),0
  ld (ix+v2),0
  ld (ix+objectfp),0
  
  jp .skipfall



.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  call addv1toxbject ;prevent objectstucking

  ld (ix+v4),11

  jp animatemole

.killmontey:


  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
  ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud
  
  
  ld (ix+deadly),0

  call Playsfx.kickenemy

;  ret

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  cp 2
  jp c,.left
  

  ld    hl,spriteaddrex168  ;color address
  ld    bc,spriteaddrex168c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset
  jp .setkilljump

.left:
  ld    hl,spriteaddrex167  ;color address
  ld    bc,spriteaddrex167c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  
    call makestarleft
  
  ret

.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret



animatemole:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex154  ;color address
  ld    bc,spriteaddrex154c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex155  ;color address
  ld    bc,spriteaddrex155c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex156  ;color address ;02
  ld    bc,spriteaddrex156c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex157  ;color address ;03
  ld    bc,spriteaddrex157c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 



acceleratemole:

  ld a,(ix+objectrelease)
  cp 2
  jp c,.acceleratemolestatic


  call getmarioleftright
  jp c,.acceleratemoleright


.acceleratemoleleft:

  ld a,(ix+v4)
  and a
  ret z
  
  dec (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,moleacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a



  ret


.acceleratemoleright:

  ld a,(ix+v4)
  cp 22
  ret z
  
  inc (ix+v4)
  
  ld a,(ix+v4)
  
  ld hl,moleacctable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a



  ret


.acceleratemolestatic:


  ld a,(ix+v3)
  cp 2
  jp c,.acceleratemoleleft
  jp .acceleratemoleright
  
  ;ret



moleacctable:   db -3,-2,-2,-1,-2,-1,-2,-1,-1,-1,0,0,0,1,1,1,2,1,2,1,2,2,3


setspriteleftright:

  ld a,(ix+objectrelease)
  cp 2
  ret c


  call getmarioleftright
  jp c,.setright


  ld a,(ix+v3)
  cp 2
  ret c

  ld (ix+v3),0



  ret

.setright:


  ld a,(ix+v3)
  cp 1
  ret nc

  ld (ix+v3),2


  ret

MovementPattern91:

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 2
  jp    z,.position1
  cp 4
  jp    z,.position2
  cp 6
  jp    z,.position3
  cp 8
  jp    z,.position4
  cp 10
  jp    z,.position5
  cp 12
  jp    z,.position6
  cp 14
  jp    z,.position7
  cp 16
  jp    z,.position8
  cp 18
  ret c

  jp destroyobject
 

.position0:
  ld    hl,spriteaddrex158  ;color address
  ld    bc,spriteaddrex158c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1 
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex159  ;color address
  ld    bc,spriteaddrex159c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex160  ;color address ;02
  ld    bc,spriteaddrex160c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex161  ;color address ;02
  ld    bc,spriteaddrex161c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.position4:
  ld    hl,spriteaddrex162  ;color address
  ld    bc,spriteaddrex162c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position5:
  ld    hl,spriteaddrex163  ;color address
  ld    bc,spriteaddrex163c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position6:
  ld    hl,spriteaddrex164  ;color address ;02
  ld    bc,spriteaddrex164c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position7:
  ld    hl,spriteaddrex165  ;color address ;02
  ld    bc,spriteaddrex165c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position8:
  ld    hl,spriteaddrex166  ;color address ;02
  ld    bc,spriteaddrex166c  ;character address
  ld    de,offsetsparkle
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

;  ret






MovementPattern92:


  call subv2toybject


  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate
 
;we must wait 4 frames before we add another piece of plant  
  inc (ix+v4)
  ld a,(ix+v4)
  cp 9
  ret c

;  call makevine

  call checkforsolid
  jp c,.destroyobject

  call makevine

  ret
  
.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,spriteaddrex169  ;color address
  ld    bc,spriteaddrex169c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex170  ;color address
  ld    bc,spriteaddrex170c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret


.destroyobject:

  call makevine ;make the last piece

  ld (ix+v5),1

  jp destroyobject

 

MovementPattern93:

  ld a,(ix+v7)
  and a
  jp nz,.spawnfirst


  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox


;ret

  call objectinteract
  cp 3
  jp z,.jumpfromblock

  call checkmariointeractshoot
  jp c,.setjumpup


  call mariointeract
  jp c,.nextframe


  call  checkobjecttilesfloor
  ret nc
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret


.spawnfirst:

  ld (ix+movepat),186
  ret


.setjumpup:

  ld (ix+movepat),95
  ld (ix+deathtimer),0
  ld (ix+v4),0
  ld (ix+v2),8
  
  call getmarioleftright
  jp nc,.setjumpupright
  
  ld (ix+v1),-2
  
  call makestarleft
      call Playsfx.kick
  ret

.setjumpupright:

  ld (ix+v1),2
    call makestarright
    call Playsfx.kick
  ret
  

.jumpfromblock:

  ld a,95
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  ld (ix+v1),a
  ld a,8
  ld (ix+deathtimer),a
  

  ret

  
.nextframe:

  ld (ix+v1),0

  ld a,(marioshell) ;if holding something react as normal
  dec a
  jp z,.noshellhold

  ld a,(Controls)
  bit 5,a
  jp nz,.shellhold

.noshellhold:

;if it is a blue rotating box we kick it away
  ld a,(ix+objectrelease)
  cp 2
  jp z,.kickbox
  cp 3
  jp z,.setspring

  ld a,(ix+standingonspr)
  and a
  jp nz,.skipfloorchecks

;only if the switch is on the floor
  call checkobjecttilesflooronly
  ret nc

.skipfloorchecks:

  ld (ix+movepat),98
  ld (ix+edible),0
  ld (ix+v4),0

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  
  ret


.setspring:

;only if the switch is on the floor
  call checkobjecttilesflooronly
  ret nc

  ld a,(fallspeed)
  and a
  jp z,.shellhold

  ld (ix+movepat),213
  ld (ix+edible),0
  ld (ix+v4),0
  ld (ix+v3),0
  ld (ix+v5),0
  ld (ix+v7),0

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  
    ld a,5
    ld (nocontrol),a
    
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,28
  xor a
  sbc hl,de  
  ld (marioy),hl    
    
  
  ret

.kickbox:

  ld (ix+movepat),111
  ld (ix+clipping),2  
  ld (ix+v4),0

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 


  call getmarioleftright      ;out c=mario is left of object
  jp nc,.kickright


.kickleft:

  ld (ix+v1),-horshellspeed

  ret

.kickright:
  
  ld (ix+v1),horshellspeed
  
  ret



.shellhold:

 ;do not allow while mario is on yoshi
 
 ld a,(marioyoshi)
 dec a
; jp z,.noshellhold
  ret z


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a


  ld a,1
  ld (mariopbox),a
 
  ld a,(ix+objectrelease)
  cp 2
  call z,.makebluebox
  cp 3
  call z,.makespring

  ld a,94 ;special movement pattern
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ret

.makebluebox:

  xor a
  ld (mariopbox),a
  ld a,1
  ld (marioblueblock),a
  
  ret

.makespring:

  ld a,2
  ld (mariopbox),a
  
  ret


MovementPattern94:


  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox

  ld a,(nocontrol)
  cp 4
  jp z,.takeshellright
  dec a
  jp z,.takeshell

  ld (ix+hitpoints),0
  ld a,(mariospeed)
  cp 2
  call nc,.setfastslowdown


;schelpje moet tijdelijk van het beeld verdwijnen zodra je omdraait

  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell


;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right



  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ret

.right:

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframe

  ret


.setfastslowdown:

;not underwater
  ld a,(waterlevel)
  and a
  ret nz

  ld (ix+hitpoints),1


  ret


.turnshell:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright
  


  ret

.turnshellright:

  
  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,0
  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ret

;mario takes the shell into a right tube
.takeshellright: 


 ; call findblockwall
 ; ret c

;look at the blockwall placed behind the tube and stop the mariomovement at all times  
;  push ix
;  ld hl,(mariox)
;  ld de,19+16-2
;  add hl,de
;  ex de,hl
;  ld ix,(spriteixvalue)
;  ld l,(ix+xobject)
;  ld h,(ix+xobject+1)
;  pop ix
;  xor a
;  sbc hl,de
;  ret nc

;deactivate this object by hand, firm and direct

  ld    (ix+active?),0          ;our primary sprite is out of active range, set sprite inactive
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
 
  
  ld (ix+movepat),0
  ld (ix+deathtimer),0
  ld (ix+killshell),0

  call removeobjectspat
  call removefromsprlst


;  ;remove sprite from spritelist
;  ld    l,(ix+sprlstadd)
;  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
;  ld    b,(ix+amountspr)        ;amount of sprites
;  .cleanspritesfromspritelist:
;  ld    (hl),0
;  inc   hl
;  djnz  .cleanspritesfromspritelist
;  ;/remove sprite from spritelist


  ret


;mario takes the shell into a tube
.takeshell:


  ld (ix+movepat),61
  ld (ix+v4),0
  ret

  ;spawn a new shell in place that is forced into a new sprite position
 

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force12 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),51                ;set object number (pinkshell)
  ld    (iy+6),0                ;set object number (pinkshell)

  call  activateobject
  
  ld (ix+v4),0

  pop ix


  jp destroyobject



;ret
  
.nextframe:


;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


  ld a,(marioturnshell)
  and a
  ret nz

  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileleft
  

  jp pattern94rammaproutineleft ;deze aanvraag returned zichzelf spagetthi warning!!


.ignoreleft:


;omhoog trappen
  ld a,(mariolook)
  dec a
  jp z,.nextframe2


  ;kick shell weg
  
  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld a,horshellspeed
  ld (ix+v1),a
  
  ld (ix+objectfp),0

  ld (ix+v4),0
  ld (ix+v3),1
;  ld (ix+hitpoints),0

  ret




.nextframeright:

;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


;shell mag niet weg terwijl je omdraaid
  ld a,(marioturnshell)
  and a
  ret nz


  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileright


;anders kunnen we dat geintje uithalen in de lucht en tijdens het rennen

  jp pattern94rammaproutineright

  
.ignoreright:  

  ld a,(mariolook)
  dec a
  jp z,.nextframe2right
  
;.skiplookcheckright:



  ;kick shell weg
  
  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a

  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,-horshellspeed
  ld (ix+v1),a

  ld (ix+objectfp),0
  ld (ix+v4),0
    ld (ix+v3),0
 ;     ld (ix+hitpoints),0

  ret

.nextframe2:

 
 
  ;kick shell weg omhoog
  
  ld a,95  ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a

  ld a,(mariospeed)
  ld (ix+v1),a
 
;bij een mariospeed moet het schelpje een kleine voorsprong krijgen 
  ld a,(mariospeed)
  and a
  ret z
 
  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 
 

 
  ret


.nextframe2right:



  ;kick shell weg
  
  ld a,95 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a
  
  ld a,(mariospeed)
  neg
  ld (ix+v1),a
  
  ld a,(mariospeed)
  and a
  ret z


  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h



  ret


.nextframespecialleft:


  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  ld hl,(mariox)
;  ld de,22
;  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  
  ld a,-horshellspeed
  ld (ix+v1),a
    ld (ix+v4),0
      ld (ix+v3),0
        ld (ix+hitpoints),1

  
  ret


.nextframespecialright:

  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  ld hl,(mariox)
  ld de,2
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld a,horshellspeed
  ld (ix+v1),a
    ld (ix+v4),0
      ld (ix+v3),1
        ld (ix+hitpoints),1
  
  ret


.kickupsheertileleft: 

  
  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  xor a
  ld (mariopbox),a


  ld a,horshellspeed
  ld (ix+v1),a
  neg
  ld (ix+v2),a

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
   ld (ix+v4),0
     ld (ix+v3),1
 ;      ld (ix+hitpoints),0
  
  call locksheertile.left
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret


.kickupsheertileright:


  ld a,99 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  xor a
  ld (mariopbox),a

  ld a,-horshellspeed
  ld (ix+v1),a
  ld (ix+v2),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
    ld (ix+v4),0
      ld (ix+v3),0
 ;       ld (ix+hitpoints),0
  
  call locksheertile.right
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 


  ret



MovementPattern95: ;flyup p-box


  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox


;  call mariointeract
;  jp c,MovementPattern93.nextframe
  


  
  ld a,(ix+deathtimer)
  cp 8
 ld b,8 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown


;check upwards twice
  call shelluptilecheck
  jp c,.nextframe3 
 
 
  call objectinteract
  jp c,.checkbox
  
  
.continue: 
  
 call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
 call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  call checkobjecttilelr
  jp c,.nextframe2
 

  ret

.checkbox:

  ld a,(iy+clipping)
  cp 8
  jp z,.nextframe4  
  ld a,(iy+movepat)
  cp 47
  jp z,.nextframe3
  cp 48
  jp z,.nextframe3


  jp .continue

.nextframe4:

  ld a,(ix+deathtimer)
  cp 3
  jp c,.continue


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld a,96
  ld (ix+movepat),a

  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a
  
  ld a,(iy+v7)
  and a
  ret nz
  
  ld (iy+v7),1

  ret
 
 
;gaan we dropcheck in douwen


.nextframe3:

  ld a,96
  ld (ix+movepat),a
  
  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a

  ret

.nextframe2:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:

  call lockobjectx.right

  xor a
  ld (ix+v1),a

  ret


.slowdown:

  
  sub b
  ld d,0
  ld e,a
  ld hl,shellfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe3
  
  
  ld (ix+v2),a

;  call shelluptilecheck
;  jp c,.nextframe
 
  call shelluptilecheck
  jp c,.nextframe3 

  call  subv2toybject
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  

  call checkobjecttilelr
  jp c,.nextframe2




  ret




MovementPattern96: ;fall


  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox



  call checkmariointeractshoot
  jp c,.setjumpup
 

 call mariointeract
 ; jp c,MovementPattern01.nextframe
  jp c,MovementPattern93.shellhold ;we pakken de shell direct vast maar als we hem niet vast hebben word ie meteen gereleased
  
  ld a,(ix+deathtimer)
  cp 10
; ld b,15 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  call c,.slowdown


  call .checkwater

 call  addv1toxbject 
 call  addv2toybject           ;x movement of object, this adds the value in v1 to xobject (16 bits)


;links of rechts een muurtje te pakken 
  call checkobjecttilelr
  jp c,.nextframe3 

  ld a,(ix+standingonspr)
  and a
  jp nz,.nextframe
 

  jp checktilesmovpattern96
  
.returnfrompage3: 
  
;gaan we dropcheck in douwen

  ret


.setjumpup:

  ld (ix+movepat),95
  ld (ix+deathtimer),0
  ld (ix+v4),0
  ld (ix+v2),8
  
  call getmarioleftright
  jp nc,.setjumpupright
  
  ld (ix+v1),-2
  
    call makestarleft
      call Playsfx.kick
  ret

.setjumpupright:

  ld (ix+v1),2
  
    call makestarright
      call Playsfx.kick
  ret


.nextframe:

  ld (ix+sheertile),0

  call lockyobject.forcelock

;check of de shell een vaart heeft zo ja andere routine doen
;  ld a,(ix+v1)
;  and a
;  jp nz,.nextframe2

  ld a,97 ;stuiteren 
  ld (ix+movepat),a
  xor a
 ; ld (ix+v1),a
  ld (ix+v2),a
  ld (ix+v4),a

  ret


.nextframe2:


  ld a,97
  ld (ix+movepat),a
  xor a
  ld (ix+v2),a
  ld (ix+v4),a
  
  ret

.sheertilefoundright:


  ld (ix+sheertile),2
  jp .sheertile


 
;als we een schuine tile hebben gevonden dan willen we anders locken 
.sheertilefound:

    ld (ix+sheertile),1


.sheertile:


  ld a,97
  ld (ix+movepat),a
  xor a
  ld (ix+v2),a
  ld (ix+v4),a

  call locksheertile.left

  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
  call locksheertile.left

  xor a
  ld (ix+v1),a

  ret
 
 
  
  
.nextframe3:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:


  call lockobjectx.right

  xor a
  ld (ix+v1),a
  
  ret  




.slowdown:

;  sub b
  ld d,0
  ld e,a
  ld hl,shellfallacctable
  add hl,de
  ld a,(hl)
  
  
  ld (ix+v2),a
;  call  addv2toybject

.checkwater:

  ld a,(waterlevel)
  and a
  jp nz,.slow

  ;check for a water level
  ld hl,(waterheight)
  xor a
  ld de,0
  sbc hl,de
  ret z ;no waterlevel present
  
  ld de,32;64
  xor a
  sbc hl,de
  
  
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  ret nc
  
  ld (ix+v2),1


  ret

.slow:

  ld (ix+v2),1

  ret


MovementPattern97: ;bounce


  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox


;altijd kunnen oppakken no matter what
  call mariointeract
  jp c,.handleinteract


.notdone:

  ld a,(ix+v4)
  inc a
  ld (ix+v4),a

    ld a,(ix+v1)
    cp 0
    jp nc,.negate
    jp z,.nox
    dec a
    jp z,.nox
    ld (ix+v1),a

  call addv1toxbject
  jp .nox

.negate:

    ld a,(ix+v1)
    jp z,.nox
    inc a
    jp z,.nox
    ld (ix+v1),a
  


.nox:

  ld hl,shellbouncetable
  ld d,0
  ld e,(ix+v4)
  add hl,de


  ld a,(ix+v4)
  cp 13
  jp nc,.done
  cp 1
  jp z,.up
  cp 2
  jp z,.up
  cp 3
  jp z,.up
  cp 4 ;d
  jp z,.down
  cp 5
  jp z,.down
  cp 6
  jp z,.down
  cp 7 ;u
  jp z,.up
  cp 8
  jp z,.up
  cp 9 ;d
  jp z,.down
  cp 10
  jp z,.down
  cp 11 ;u
  jp z,.up
  cp 12 ;d
  jp z,.down


  ret

;shellbouncetable: db 0,3,2,1,1,2,3,2,1,1,2,1,1,0


.up:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call subv2toybject
  
  ret

.down:
  
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toybject
  
  ret



.handleinteract:

;  call checkmariointeractshoot
;  ret c

.done:

  ld (ix+movepat),93
  ld (ix+objectfp),0

;  jp MovementPattern01.nextframe
  
  
  ret




MovementPattern98:

  ld a,(ix+v4)
  and a
  jp nz,.timer


;trip the screen
  ld a,1
  ld (tripscreen),a

  ld a,(bluepboxactive)
  dec a
  call z,.setspecial
  
  ld a,1
  ld (bluepboxactive),a 
  ld (switchmusicpforbox),a   


  ld hl,300;900 ;10 sec
  ld (pboxcounter),hl
  
  ld (ix+v4),1
 
  ld a,(ix+objectrelease)
  dec a
  jp z,.setgrey
 
  ld a,(doublepboxactive)
  and a  
  call z,turnbrownblocksintocoin ;not twice

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex171  ;color address
  ld    bc,spriteaddrex171c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.setspecial:
    
    ld a,1
    ld (doublepboxactive),a

    ret
 
  
.setgrey:

  ld a,2
  ld (bluepboxactive),a  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresspboxgreyjumpedon  ;color address
  ld    bc,characteraddresspboxgreyjumpedon  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock4
  jp    setcharcolandoffset  

.timer:

  inc (ix+v4)
  ld a,(ix+v4)

  cp 25
  ret c

  jp destroyobject


;ret



MovementPattern99:

  ld a,(ix+objectrelease)
  cp 2
  call z,animatebluebox


;  ld a,(mariospeed)
;  cp 2
;  call nc,.setfastslowdown


  call checkmariointeractshoot
  jp c,.setjumpup

;in case of a blue rotating box
  call mariointeract
  jp c,.kickbox

.nobluebox:

  ld a,(ix+hitpoints)
  and a
  call z,.slowdownfast
  ld a,(ix+hitpoints)
  dec a
  call z,.slowdownslow


  ld a,(ix+v3)
  and a
  call z,.negatespeed

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ;after all that we check if both speed have stopped so we can revert back to the proper state
  
 ld a,(ix+v1)
 ld b,a
 ld a,(ix+v2)
 or b
 jp z,.nextframe
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  ret c


  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
;  call  checkobjecttilesfloor
;  ret c 
  call  checkobjecttilesfloor

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  
  call objectinteract ;check if there is a box wall
  ld a,(iy+movepat)
  cp 47 
  jp z,.wallfound
  
  ret
 
.setjumpup:

  ld (ix+movepat),95
  ld (ix+deathtimer),0
  ld (ix+v4),0
  ld (ix+v2),8
  
  call getmarioleftright
  jp nc,.setjumpupright
  
  ld (ix+v1),-2
  
    call makestarleft
      call Playsfx.kick
  ret

.setjumpupright:

  ld (ix+v1),2
  
    call makestarright
      call Playsfx.kick
  ret   

.setfastslowdown:

  ld (ix+hitpoints),1


  ret

.nextframe:

  ld (ix+movepat),93
  ld (ix+hitpoints),0
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  ld (ix+objectfp),0
  
  ret

 
 
.negatespeed:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ret

  
  
.wallfound:


  ld (ix+v4),15
  ld (ix+v1),0
  ret

 

.slowdownfast:

  ld e,(ix+v4)
  ld d,0
  ld hl,slowpboxdownfasttable
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  and a
  ret z

  ld (ix+v1),2

  call checkobjecttilesflooronly
  ret nc


  ld e,(ix+v4)
  ld d,0
  ld hl,slowpboxdownfasttable
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  and a
  ret z
  
  inc (ix+v4)
  
  ret



.slowdownslow:

  ld e,(ix+v4)
  ld d,0
  ld hl,slowpboxdownslowtable
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  and a
  ret z


;  ld (ix+v1),6

  call checkobjecttilesflooronly
  ret nc


  ld e,(ix+v4)
  ld d,0
  ld hl,slowpboxdownslowtable
  add hl,de
  ld a,(hl)
  ld (ix+v1),a
  and a
  ret z
  
  inc (ix+v4)
  
  ret


.kickbox:

  ld a,(ix+objectrelease)
  cp 2
  jp nz,.nobluebox


  ld (ix+movepat),111
  ld (ix+clipping),2
  ld (ix+v4),0

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 


  call getmarioleftright      ;out c=mario is left of object
  jp nc,.kickright


.kickleft:

  ld (ix+v1),-horshellspeed

  ret

.kickright:
  
  ld (ix+v1),horshellspeed
  
  ret



slowpboxdownfasttable:  db 2,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0
slowpboxdownslowtable:  db 5,4,4,4,3,4,3,3,3,2,3,2,2,1,1,0




MovementPattern100: ;coin (static)

  ld a,(ix+v7)
  and a
  jp nz,.changebacktobrownblock

  ld a,(doublepboxactive)
  and a
  jp nz,.turnintobrownblock ;in any case a blue pbox was pressed and a silver pbox at the same time
  
  ld a,(bluepboxactive)
  dec a
  jp z,.turnintobrownblock


.continue:

  call mariointeract
  jp c,.pickupcoin
  

  call .animate


  ret




.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret

.pickupcoin:

  call handlecoin

  ;put whatever is needed here

  call Playsfx.coin


  jp destroyobjectwithsparkle

;ret


.turnintobrownblock:
  
  call transformintobrownblock

  jp destroyobject.objectreturns

;ret

;when this coin got spawned out of a brown block we must turn it back into one
.changebacktobrownblock:

  ld a,(bluepboxactive)
  and a
  jp nz,.continue

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  call createbrownblock

  jp destroyobject

;  ret
 


MovementPattern101: ;yoshi head (passive)


  ld a,(marioyoshi)
  dec a
  jp z,.yoshiactive

  ld a,(yoshileftright)
  dec a
  jp z,.right


  ld a,(yoshifilled)
  and a
  jp nz,.handleswallowleft

;take care of the openmouth animation  
  ld a,(ix+v4)
  cp 45
  call z,.openmouthleft
  ld a,(ix+v4)
  cp 60
  call z,.closemouthleft
  
  inc (ix+v4)
  
  ret


.openmouthleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex188  ;color address
  ld    bc,spriteaddrex188c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.closemouthleft:

  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex187  ;color address
  ld    bc,spriteaddrex187c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.right:

  ld a,(yoshifilled)
  and a
  jp nz,.handleswallowright


  ld a,(ix+v4)
  cp 45
  call z,.openmouthright
  ld a,(ix+v4)
  cp 60
  call z,.closemouthright
  
  inc (ix+v4)

  ret

.openmouthright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex179  ;color address
  ld    bc,spriteaddrex179c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.closemouthright:

  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex176  ;color address
  ld    bc,spriteaddrex176c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset




;  ret



.handleswallowleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(yoshiswallow)
  inc a
  ld (yoshiswallow),a
  cp 15
  jp nc,.resetswallow
  cp 10
  jp nc,.swallowleft3
  cp 5
  jp nc,.swallowleft2
  
.swallowleft1:

  ld    hl,spriteaddrex203  ;color address
  ld    bc,spriteaddrex203c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.swallowleft2:

  ld    hl,spriteaddrex204  ;color address
  ld    bc,spriteaddrex204c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.swallowleft3:

  ld    hl,spriteaddrex205  ;color address
  ld    bc,spriteaddrex205c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  



.handleswallowright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(yoshiswallow)
  inc a
  ld (yoshiswallow),a
  cp 15
  jp nc,.resetswallow
  cp 10
  jp nc,.swallowright3
  cp 5
  jp nc,.swallowright2
  
.swallowright1:

  ld    hl,spriteaddrex206  ;color address
  ld    bc,spriteaddrex206c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.swallowright2:

  ld    hl,spriteaddrex207  ;color address
  ld    bc,spriteaddrex207c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.swallowright3:

  ld    hl,spriteaddrex208  ;color address
  ld    bc,spriteaddrex208c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  



.resetswallow:

  xor a
  ld (yoshiswallow),a
  ld (yoshifilled),a
  
  ret




.yoshiactive:


  ld (ix+movepat),104
  ld (ix+clipping),0
  
  ld a,(yoshileftright)
  and a
  jp nz,.closemouthright
  
  jp .closemouthleft ;close the mouth or bad things will happen ;)


;  ret


MovementPattern102:     ;destroyblockdebris

  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  jp objectdeathjump


MovementPattern103:  ;activeyoshibody

  ld a,(yoshitype)
  cp 4
  call z,.setflying ;permament blue bonus yoshi
;  cp 2
;  call z,.setstomp

  xor a
  ld (marioshell),a ;CANNOT INTERACT WITH SHELL


;detect sprite inhibition
  ld a,(nosprite)
  dec a
  jp z,.inhibitsprite



;check if mario is still on yoshi then handle the right movement from there
  ld a,(marioyoshi)
  and a
  jp z,.deactivate

    
;must compensate for the leftright differences between both statics
  ld a,(marioleftrightspr)
  dec a
  jp z,.yoshibodyright
  
  ld hl,(mariox)
;  ld de,7
;  xor a
;  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 
  
  jp .bodyxdone

.yoshibodyright:

  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

.bodyxdone:
  
  ld hl,(marioy)
  
  ld de,0
  xor a
  sbc hl,de
  dec hl
  jp z,.noadd
  inc hl
  
  ld de,16
  xor a
  add hl,de
  
.noadd:
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h



;handle tube entering

  ld a,(nocontrol)
  cp 1
  jp z,.turnonce

.actnormal:
;handle turning

  ld a,(marioturnyoshi)
  and a
  jp nz,.turn


;handle ducking
  ld a,(marioduck)
  dec a
  jp z,.sit

;handle jumping

  ld a,(mariofloating)
  dec a
  jp z,.jump  
  

.normal:

  ld a,(marioleftrightspr)
  dec a
  jp z,.normalright


.normalleft:

  ;we are not moving
  ld a,(mariospeed)
  cp 0
  jp z,.normalleft1

  ld a,(mariofloating)
  and a
  jp nz,.normalleft1


  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.runleft


  ld a,(Controls)
  bit 5,a
  jp z,.yoshiwalkleftslow
  
.yoshiwalkleftfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.normalleft3

  cp 1
  jp nc,.normalleft2
  
  cp 0
  jp nc,.normalleft1  

.yoshiwalkleftslow:
  
  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  
  cp 6
  call nc,.resetmariowalktable


  cp 4
  jp nc,.normalleft3

  cp 2
  jp nc,.normalleft2

  cp 0
  jp nc,.normalleft1


.runleft:

  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.normalleft1
  
  cp 1
  jp z,.normalleft2
  
  cp 2
  jp z,.normalleft3
  ret

  
  
.setstomp:

  ld a,1
  ld (yoshistomp),a
  ret

  

.inhibitsprite:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  

  

.normalleft1:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex183  ;color address
  ld    bc,spriteaddrex183c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.normalleft2:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex184  ;color address
  ld    bc,spriteaddrex184c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.normalleft3:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex185  ;color address
  ld    bc,spriteaddrex185c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  




.normalright:

  ;we are not moving
  ld a,(mariospeed)
  cp 0
  jp z,.normalright1

  ld a,(mariofloating)
  and a
  jp nz,.normalright1


  ;renne wij op max speed?
  ld a,(mariorunning)
  dec a
  jp nz,.runright


  ld a,(Controls)
  bit 5,a
  jp z,.yoshiwalkrightslow
  
.yoshiwalkrightfast:   

  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  cp 3
  call nc,.resetmariowalktable


  cp 2
  jp nc,.normalright3

  cp 1
  jp nc,.normalright2
  
  cp 0
  jp nc,.normalright1  

.yoshiwalkrightslow:
  
  ld hl,mariowalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  
  cp 6
  call nc,.resetmariowalktable


  cp 4
  jp nc,.normalright3

  cp 2
  jp nc,.normalright2

  cp 0
  jp nc,.normalright1


.runright:

  ld a,(mariowalktable)
  inc a
  ld (mariowalktable),a
  
  cp 3
  call nc,.resetmariowalktable
  
  cp 0
  jp z,.normalright1
  
  cp 1
  jp z,.normalright2
  
  cp 2
  jp z,.normalright3
  ret



.normalright1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex180  ;color address
  ld    bc,spriteaddrex180c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.normalright2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex181  ;color address
  ld    bc,spriteaddrex181c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.normalright3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex182  ;color address
  ld    bc,spriteaddrex182c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret

.setflying:

  ld a,1
  ld (yoshiflying),a
  ret

;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetmariowalktable:

  ld hl,mariowalktable
  xor a
  ld (hl),a
  ret


;only issue turn once. So that the spritehider can do its job
.turnonce:

  ld a,(yoshispecial)
  dec a
  jp z,.actnormal

  ld a,(ix+v7)
  and a
  jp nz,.turnonceleftright
  
  inc (ix+v7)

.turn:

  ld a,(marioleftrightspr)
  dec a
  jp z,.turnleft
  
.turnright:


;compensate for left and right offsets
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex191  ;color address
  ld    bc,spriteaddrex191c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
    


.turnleft:  

;compensate for left and right offsets
  ld hl,(mariox)
;  ld de,7
;  xor a
;  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex190  ;color address
  ld    bc,spriteaddrex190c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.turnonceleftright:


  ld a,(marioleftrightspr)
  dec a
  jp z,.turnleftonce
  

.turnrightonce:


;compensate for left and right offsets
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret
  
.turnleftonce:

;compensate for left and right offsets
  ld hl,(mariox)
;  ld de,7
;  xor a
;  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

    ret
  


.deactivate:

  xor a
  ld (yoshiswallow),a
  ld (yoshiflying),a
  ld (yoshistomp),a

  ld (ix+movepat),105 ;105
  ld (ix+clipping),1 

;directly adjust the height of the head as well
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)


  push ix
  ld ix,(yoshiheadix)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  pop ix

;set the v1 speed value based on the current speed of mario

  ld a,(marioleftright)
  dec a
  jp z,.right

  ld a,(mariospeed)
  neg
  ld (ix+v1),a

;set the left right status of yoshi based on the last status of mario
  xor a
  ld (yoshileftright),a

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ;set the body sprite correctly  
  ld    hl,spriteaddrex189  ;color address ;02
  ld    bc,spriteaddrex189c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.right:
  
  ld a,(mariospeed)
  ld (ix+v1),a
  
  ld a,1
  ld (yoshileftright),a
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ;set the body sprite correctly  
  ld    hl,spriteaddrex175  ;color address ;02
  ld    bc,spriteaddrex175c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.sit:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 

  ld a,(marioleftright)
  dec a
  jp z,.sitright

  
  ld    hl,spriteaddrex189  ;color address
  ld    bc,spriteaddrex189c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.sitright:

  ld    hl,spriteaddrex175  ;color address
  ld    bc,spriteaddrex175c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
    


.jump:

  ld a,(marionotdown)
  dec a
  jp z,.normal


  ld a,(marioleftrightspr)
  dec a
  jp z,.jumpright

.jumpleft:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex194  ;color address
  ld    bc,spriteaddrex194c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.jumpright:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex186  ;color address
  ld    bc,spriteaddrex186c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
   



MovementPattern104:    ;activeyoshihead

  ld a,(nosprite)
  dec a
  jp z,.inhibitsprite


;check if mario is still on yoshi then handle the right movement from there
  ld a,(marioyoshi)
  and a
  jp z,.deactivate


;handle tube entering

  ld a,(nocontrol)
  cp 1
  jp z,.turnonce


.actnormal:

  ld a,(marioturnyoshi)
  and a
  jp nz,.turn


;get the left right and adjust to that

  ld a,(marioleftrightspr)
  dec a
  jp z,.right

  ld hl,(mariox)
  ld de,9
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
 
  ld a,(yoshifilled)
  and a
  jp nz,.swallowleft1
 
 
  ld a,(yoshiswallow)
  and a
  jp nz,.handleswallowleft
 
 
  ld a,(marioshot)
  and a
  jp nz,.tongueleft
 
  
  ld    hl,spriteaddrex187  ;color address
  ld    bc,spriteaddrex187c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 


  jp .done


.inhibitsprite:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.right:

  ld hl,(mariox)
  ld de,16
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld a,(yoshifilled)
  and a
  jp nz,.swallowright1

  ld a,(yoshiswallow)
  and a
  jp nz,.handleswallowright


  ld a,(marioshot)
  and a
  jp nz,.tongueright
 
  
  ld    hl,spriteaddrex176  ;color address
  ld    bc,spriteaddrex176c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset
  
  
.done:

  ld a,(marioduck)
  dec a
  jp z,.duck

.adjusty:

;  ld de,13
  ld de,0
  jp .bigguy
.duck: 
  ld de,8
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  
  ;check if de = -1
  push hl
  ld hl,(marioy)
  ld de,0
  xor a
  sbc hl,de
  pop hl
  jp z,.setmin1
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ret

.setmin1:

  ld hl,-1
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  ret



.tongueleft:

  ld a,(marioshot)
  cp 10
  jp nc,.tongueleft3
  cp 5
  jp nc,.tongueleft2
  
.tongueleft1:

  ld    hl,spriteaddrex197  ;color address
  ld    bc,spriteaddrex197c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  jp .done

.tongueleft2:

  ld    hl,spriteaddrex198  ;color address
  ld    bc,spriteaddrex198c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  jp .done

.tongueleft3:

  ld    hl,spriteaddrex199  ;color address
  ld    bc,spriteaddrex199c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset  

  jp .done


.tongueright:


  ld a,(marioshot)
  cp 10
  jp nc,.tongueright3
  cp 5
  jp nc,.tongueright2
  
.tongueright1:

  ld    hl,spriteaddrex202  ;color address
  ld    bc,spriteaddrex202c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  jp .done

.tongueright2:

  ld    hl,spriteaddrex201  ;color address
  ld    bc,spriteaddrex201c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  jp .done

.tongueright3:

  ld    hl,spriteaddrex200  ;color address
  ld    bc,spriteaddrex200c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  call    setcharcolandoffset  

  jp .done



.handleswallowleft:

  ld a,(yoshiswallow)
  inc a
  ld (yoshiswallow),a
  cp 15
  jp nc,.resetswallow
  cp 10
  jp nc,.swallowleft3
  cp 5
  jp nc,.swallowleft2
  
.swallowleft1:

  ld    hl,spriteaddrex203  ;color address
  ld    bc,spriteaddrex203c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp .done

.swallowleft2:

  ld    hl,spriteaddrex204  ;color address
  ld    bc,spriteaddrex204c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp .done

.swallowleft3:

  ld    hl,spriteaddrex205  ;color address
  ld    bc,spriteaddrex205c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset  

  jp .done


.handleswallowright:

  ld a,(yoshiswallow)
  inc a
  ld (yoshiswallow),a
  cp 15
  jp nc,.resetswallow
  cp 10
  jp nc,.swallowright3
  cp 5
  jp nc,.swallowright2
  
.swallowright1:

  ld    hl,spriteaddrex206  ;color address
  ld    bc,spriteaddrex206c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp .done

.swallowright2:

  ld    hl,spriteaddrex207  ;color address
  ld    bc,spriteaddrex207c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp .done

.swallowright3:

  ld    hl,spriteaddrex208  ;color address
  ld    bc,spriteaddrex208c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset  

  jp .done


.resetswallow:

  xor a
  ld (yoshiswallow),a
  
  jp .done


.turnonce:

  ld a,(yoshispecial)
  dec a
  jp z,.actnormal


  ld a,(ix+v7)
  and a
  jp nz,.handleturnonce

  inc (ix+v7)


.turn:

  ld a,(marioleftrightspr)
  dec a
  jp z,.turnleft
  
.turnright:


  ld hl,(mariox)
  ld de,14
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex193  ;color address
  ld    bc,spriteaddrex193c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset
    
  jp .adjusty


.turnleft:  

  ld hl,(mariox)
  ld de,7
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex192  ;color address
  ld    bc,spriteaddrex192c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset

  jp .adjusty


.handleturnonce:

  ld a,(marioleftrightspr)
  dec a
  jp z,.turnleftonce


.turnrightonce:

  ld hl,(mariox)
  ld de,14
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
    
  jp .adjusty


.turnleftonce:

  ld hl,(mariox)
  ld de,7
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  jp .adjusty



.deactivate:

  ld (ix+movepat),101
  ld (ix+clipping),1


  ld a,(marioleftright)
  dec a
  jp z,.rightd

  ld hl,(mariox)
  ld de,2
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex187  ;color address
  ld    bc,spriteaddrex187c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset 

.rightd:

  ld hl,(mariox)
  ld de,16
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex176  ;color address
  ld    bc,spriteaddrex176c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret




MovementPattern105:    ;yoshi body (passive)



  call mariointeract
  jp c,.activate


.noactivate:


;if yoshi is standing on a floor we only animate him and thats it
  call checkobjecttilesflooronly ;BUG!!
  jp c,animatepassiveyoshi


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)


  ;prevent yoshi from getting out of screen
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  call c,.lockyoshileft


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld c,(ix+xobject)
  ld b,(ix+xobject+1)

;adjust the head
  push ix
  ld ix,(yoshiheadix)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h

;do the x as well discriminate for left and right in this particular case 
  ld h,b
  ld l,c
  
  ld a,(yoshileftright)
  dec a
  jp z,.right
  
  ld de,9
  xor a
  sbc hl,de
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  jp .done


.right:
  
  ld de,9
  add hl,de
   
  ld (ix+xobject),l
  ld (ix+xobject+1),h


.done:
  
  pop ix


  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  


  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

  ld (ix+v1),0

  jp animatepassiveyoshi


.lockyoshileft:

  ld hl,16
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  ret



.activate:

;do not allow mounting while mario has a object in hands
  ld a,(marioshell)
  dec a
  jp z,.noactivate


  ld a,(fallspeed)
  and a
  jp z,.noactivate

  ld a,1
  ld (marioyoshi),a
  ld (ix+movepat),103
  ld (ix+clipping),0
;  xor a
;  ld (mariospinjump),a ;ALLOW this. This bug is present on the SNES as well

  ld a,sccyoshimount
  jp ReplayerInit

;  ret


animatepassiveyoshi:
 
  ld (ix+v2),1
  ld c,(ix+xobject) ;get the xobject coordinates before adjusting
  ld b,(ix+xobject+1)
 

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  cp 1
  jp    z,.position0
  cp 3
  jp    z,.position1
  cp 4
  ret c

  ld (ix+v3),0
  ret


.position0:

  call subv2toybject

;adjust the head
  push ix
  ld ix,(yoshiheadix)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  jp .adjustx


.position1:

  call addv2toybject
 

;adjust the head
  push ix
  ld ix,(yoshiheadix)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h

;adjust the xcoordinates
.adjustx:

  ;do the x as well discriminate for left and right in this particular case 
  ld h,b
  ld l,c
  
  ld a,(yoshileftright)
  dec a
  jp z,.right
  
  ld de,9
  xor a
  sbc hl,de
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  pop ix

  ret


.right:
  
  ld de,9
  add hl,de
   
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  pop ix

  ret



MovementPattern106:    ;yoshi body (beserk)


  call mariointeract
  jp c,.activate

;this lable is required since an activation may not always result in sucess (multiple factors involved) then we must continue the routine or else bad stuff is bound to happen
.noactive:

  call .normal ;animate the body




;if yoshi is standing on a floor we only animate him and thats it
;  call  checkobjecttilesflooronly
;  jp c,animatepassiveyoshi

;  call animatepassiveyoshi


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld c,(ix+xobject)
  ld b,(ix+xobject+1)

;adjust the head
  push ix
  ld ix,(yoshiheadix)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h

;do the x as well discriminate for left and right in this particular case 
  ld h,b
  ld l,c
  
  ld a,(yoshileftright)
  dec a
  jp z,.right
  
  ld de,9
  xor a
  sbc hl,de
  
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  jp .done


.right:
  
  ld de,9
  add hl,de
   
  ld (ix+xobject),l
  ld (ix+xobject+1),h


.done:
  
  pop ix

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  jp    c,.wallfound            ;do not try to walk out of left side of map


  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:

;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction

  ld a,(yoshileftright)
  xor %00000001
  ld (yoshileftright),a


;make sure that the head responds correctly to the directional change
  push ix
  
  ld ix,(yoshiheadix)
  
  ld (ix+v4),15
  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)

  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  
  pop ix

 ; jp animatepassiveyoshi
  ret ;animate from here


.activate:

;do not allow mounting while mario has a object in hands
  ld a,(marioshell)
  dec a
  jp z,.noactive


  ld a,(fallspeed)
  and a
  jp z,.noactive

  ld a,1
  ld (marioyoshi),a
  ld (ix+movepat),103

  ret



;deze code reset de mariowalktable om buggy bewegingen te voorkomen
.resetyoshiwalktable:

  ld hl,yoshiwalktable
  xor a
  ld (hl),a
  ret  





.normal:

  ld a,(yoshileftright)
  dec a
  jp z,.normalright


.normalleft:

  ld hl,yoshiwalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  
  cp 6
  call nc,.resetyoshiwalktable


  cp 4
  jp nc,.normalleft3

  cp 2
  jp nc,.normalleft2

  cp 0
  jp nc,.normalleft1

  

.normalleft1:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex183  ;color address
  ld    bc,spriteaddrex183c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.normalleft2:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex184  ;color address
  ld    bc,spriteaddrex184c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.normalleft3:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex185  ;color address
  ld    bc,spriteaddrex185c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  




.normalright:

  
  ld hl,yoshiwalktable
  ld a,(hl)
  inc a
  ld (hl),a
  
  
  cp 6
  call nc,.resetyoshiwalktable


  cp 4
  jp nc,.normalright3

  cp 2
  jp nc,.normalright2

  cp 0
  jp nc,.normalright1



.normalright1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex180  ;color address
  ld    bc,spriteaddrex180c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.normalright2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex181  ;color address
  ld    bc,spriteaddrex181c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset
  
.normalright3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex182  ;color address
  ld    bc,spriteaddrex182c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret




MovementPattern107:    ;yoshi head (beserk)


    

  ld a,(marioyoshi)
  dec a
  jp z,.yoshiactive

  ld a,(yoshileftright)
  dec a
  jp z,.right


;take care of the pain animation  
  ld a,(ix+v4)
  cp 15
  call z,.openmouthleft
  ld a,(ix+v4)
  cp 30
  call z,.closemouthleft
  ld a,(ix+v4)
  cp 31
  call z,.resetv4
  
  inc (ix+v4)
  
  ret


.openmouthleft:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex195  ;color address
  ld    bc,spriteaddrex195c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.closemouthleft:

  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex187  ;color address
  ld    bc,spriteaddrex187c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.right:


  ld a,(ix+v4)
  cp 15
  call z,.openmouthright
  ld a,(ix+v4)
  cp 30
  call z,.closemouthright
  ld a,(ix+v4)
  cp 31
  call z,.resetv4
  
  inc (ix+v4)

  ret

.openmouthright:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex196  ;color address
  ld    bc,spriteaddrex196c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


.closemouthright:

  ld (ix+v4),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  
  ld    hl,spriteaddrex176  ;color address
  ld    bc,spriteaddrex176c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset




.resetv4:

    ld (ix+v4),0
    ret




;  ret


.yoshiactive:


  ld (ix+movepat),104
  ld (ix+clipping),0
  


;  ret




MovementPattern108:    ;red flame sidewards


    ld a,(currentlevel)
    cp 178
    call z,mariointeract
;the movement of this object is somewhat complicated because it acts as light. The faster you move, the faster the object moves away from you. It always moves in correlation with you


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)
  

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v1)
  bit 7,a
  jp nz,.right

  
.left:

  ld a,(framecounter)
  and 1
  jp z,.position1

.position0:
  ld    hl,spriteaddrex209  ;color address
  ld    bc,spriteaddrex209c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex210  ;color address
  ld    bc,spriteaddrex210c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.right:

  ld a,(framecounter)
  and 1
  jp z,.position3


.position2:
  ld    hl,spriteaddrex211  ;color address ;02
  ld    bc,spriteaddrex211c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex212  ;color address ;02
  ld    bc,spriteaddrex212c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset



 ; ret



MovementPattern109:    ;pbox (blue-static)

  ld a,(ix+v4)
  cp 8
  jp z,.activate
  and a
  jp nz,.fall
  
  

;ret

  call objectinteract
  cp 3
  jp z,.activatewithdelay

  call checkmariointeractshoot
  jp c,.activate


  call mariointeract
  jp c,.activate


;  call  checkobjecttilesfloor
;  ret nc
;  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret


.activate:

  call addv2toybject
  ld (ix+movepat),93
  ret


.fall:

  inc (ix+v4)
  call addv2toybject
  

  ret


.activatewithdelay:

;  call addv2toybject
    ld (ix+v2),2
  ld (ix+v4),1
  ret


blueblockinteract:


  ld a,(mariotransforming)
  cp 10
  ret z ;Disable interactions on dying to prevent deathloops

  ;check if mario within x range of object
  ld  e,(ix+xobject)
  ld  d,(ix+xobject+1)
  ld  hl,-20
  add hl,de
  
  ld  de,(mariox)
  or  a  
  sbc hl,de
  
  ret nc            ;left border not found, no collision

  ld  a,(ix+sizeX)
  add a,9+8
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

  ret nc            ;right border not found, no collision
  ;/check if mario within x range of object

  ;check if mario within y range of object
  ld  e,(ix+yobject)
  ld  d,(ix+yobject+1)
  ld  hl,-28
  add hl,de
  
  ld  de,(marioy)
  or  a  
  sbc hl,de
  
  ret nc            ;top border not found, no collision

  ld b,28;15 ;normal state


  ld  a,(ix+sizeY)
  add a,b
  ld  d,0
  ld  e,a
  or  a  
  add hl,de

  ret nc            ;bottom border not found, no collision
  ;/check if mario within x range of object

  ;na de return is het voor de eventhandler belangrijk om te weten of er iets met het object hoort te gebeuren
; we geven hier dus een carry bit uit  

.setcarry:  

  scf
  ret





MovementPattern110:    ;blue rotating block


  ld bc,16
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp c,.destroyobject
  
  call blueblockinteract  
  jp c,.handlegrab


  ret

.destroyobject:

  ld (ix+v4),1 ;no sprite update
  call .placenewblockbelow

  jp destroyobject



.handlegrab:

;Problem: if too many objects are active the new block will refuse to spawn.
;thus we must check if a new block can spawn. and if not wait until so
  ;search empty space in objectlst
  ld    b,1                     ;object number in objectlist
  ld    hl,Objectlist
  ld    de,lenghtobjectlist
  
.searchemptyplace:
  ld    a,(hl)
  or    a
  jr    z,.emptyplacefound      ;there is an empty place in the Object list, put object here
  add   hl,de                   ;next object
  inc   b                       ;increase object number
  jp    .searchemptyplace

.emptyplacefound:
  ld    a,AmountofPossibleActiveObjects
  sub   a,b
  ret   c                       ;this is the overflow check, if carry then no more empty space

  ld a,(marioyoshi)
  dec a
  ret z


;not with shell
  ld a,(marioshell)
  dec a
  ret z


;fallspeed must be 0 you are standing on the thing
  ld a,(fallspeed)
  and a
  ret nz

  ld a,(marioduck)
  and a
  ret z

;c must be pressed 
  ld a,(Controls)
  bit 5,a
  ret z
 

  
  ld a,1
  ld (marioshell),a
  ld (marioblueblock),a
  
  ld hl,350
;  ld (ix+counter),l 
;  ld (ix+counter+1),h ;set the timer for dissapearance in 16 bit
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  ld (ix+movepat),94
  ld (ix+v4),0
  ld (ix+deadly),0
  ld (ix+clipping),2
  
  ld b,2
  call increasespritecount 

  
  call makebkgblock

.placenewblockbelow:

;  ld a,(currentlevel) ;for map 03-01b
;  cp x
;  jp z,.checkspecial

  ld bc,16+16
  ld de,0
  call objecttilecheckfromrom
  cp 224
  jp z,.makeblock
  cp 200
  jp z,.makeblock
  cp 213
  jp z,.makeblock ;map03-01b

  ret


;.checkspecial:
;
;  ld bc,16+16
;  ld de,0
;  call objecttilecheckfromrom
;  cp 213
;  jp z,.makeblock
;  
;  ret
  


.makeblock:
 
;spawn a new blue block underneath. This new block will check itself so no problem

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  inc de


  push ix
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                  ;set y custom object
  ld    (iy+4),d                  ;set y custom object
  ld    (iy+5),87                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject

  pop ix
 
;set the sprite
  ld a,(ix+v4)
  and a
  ret nz ;do not allow this if v4 is set
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex214  ;color address
  ld    bc,spriteaddrex214c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset  
  
 ; ret




MovementPattern111:    ;blue rotating block (shooting away)


  call objectinteract  
  cp 2
  jp z,.destroybluebox
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,pinkshelldeathjump


  call mariointeract
 ; jp c,.destroybluebox


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

;  ld    a,(framecounter)
;  and   3                       ;this sprite animates once every 4 frames
;  call  z,animatebluebox

  call animatebluebox


  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  call  checkobjecttilesfloor
;  ret c 
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  
  call objectinteract ;check if there is a box wall
  ld a,(iy+movepat)
  cp 47 
  jp z,.wallfound
  
  ret
  
.wallfound:

.destroybluebox:

  
;spawn the debris

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

  ld a,e


  push ix

  ld (blockhlstorage),hl ;hl opslaan
  ld (blockbcstorage),de

;TODO: we moeten 4 objecten spawnen met allemaal een eigen waarde. Deze objecten moeten direct een deathjump doen
;object 1
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),2
    ld (ix+v2),8
    ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  ld de,(blockbcstorage)
  
;object 2
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
        ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)


;object 3

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e               ;set y custom object
  ld    (iy+4),d               ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),-2
    ld (ix+v2),8
        ld (ix+movepat),89

  ld hl,(blockhlstorage) ;hl opslaan
  inc hl
  ld de,(blockbcstorage)


;object 4

    ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),23                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
    ld (ix+v1),2
        ld (ix+movepat),89

  pop ix


  call Playsfx.breakblock






  jp destroyobjectwithcloudnostar
  

    ret

;TODO: add the right sprites
animatebluebox:

;count 30 seconds before removal from map
;  ld l,(ix+counter)
;  ld h,(ix+counter+1)
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld de,1
  xor a
  sbc hl,de
  jp z,.killblock
;  ld (ix+counter),l
;  ld (ix+counter+1),h
  ld (ix+v5),l
  ld (ix+v5+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    a,(framecounter)
  and   3
  jp    z,.position2 
  and   1                       ;this sprite animates once every 4 frames
  jp    z,.position1
  and   2
  jp    z,.position0  

.position0:
  ld    hl,spriteaddrex215  ;color address
  ld    bc,spriteaddrex215c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset  


.position1: 
  ld    hl,spriteaddrex216  ;color address
  ld    bc,spriteaddrex216c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset 
  
.position2: 
  ld    hl,spriteaddrex214  ;color address
  ld    bc,spriteaddrex214c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



.killblock:

  ld a,(marioshell)
  dec a
  jp z,.killblock2

  jp destroyobjectwithcloudnostar


 ;   ret

.killblock2:

  ld l,(ix+movepat+1)
  ld a,(ix+movepat)
  xor l
  cp 94
  jp nz,destroyobjectwithcloudnostar 

  xor a
  ld (marioshell),a
      ld (marioblueblock),a

    jp destroyobjectwithcloudnostar




MovementPattern112:    ;flying box (alternate movement)

;always reset the standing bit
;  xor a
;  ld (standingonsprite),a


;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobject
;  call z,handleboxinteract.popbox



  call mariointeract
  call c,handleboxinteract


  call .getv1
  call .getv2


  call addv1toxbject
  call .doy


  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  and 4
  jp z,.position1
  jp .position0
  
 ret

.animate:
 
 
  
.position0:
  ld    hl,spriteaddrex79  ;color address
  ld    bc,spriteaddrex79c  ;character address
  ld    de,offsetflyingbox1
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex80  ;color address
  ld    bc,spriteaddrex80c  ;character address
  ld    de,offsetflyingbox2
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret

.doy:

  ld a,(ix+v2)
  bit 7,a
  jp nz,.goup

.godown:

  jp addv2toybject

.goup:

  ld a,(ix+v2)
  neg
  ld (ix+v2),a

  jp subv2toybject

.resetv4:

  ld (ix+v4),0
  ld (ix+v2),0
  ret

.resetv3:

  ld (ix+v3),0
  ld (ix+v1),0
  ret


.getv2:

  ld hl,flyingbox2speedtable
  ld e,(ix+v4)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resetv4
  ld (ix+v2),a
  inc (ix+v4)
  ret

.getv1:

  ld hl,horflyingbox2speedtable
  ld e,(ix+v3)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resetv3
  ld (ix+v1),a
  inc (ix+v3)
  ret



flyingbox2speedtable:  db 1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,10
horflyingbox2speedtable:  db 1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,10




MovementPattern113:    ;mariolevelexit coverupsprite

  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
;  ex de,hl
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  jp nc,destroyobject



  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call animatelevelexit


  ld hl,(marioy)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  xor a
  sbc hl,de
  jp c,.fixy


  ld hl,(marioy)
  ld de,16
  xor a
  sbc hl,de
  dec hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ret


.fixy:

  ld (ix+yobject),e
  ld (ix+yobject+1),d
  
  ret



MovementPattern114:    ;red flying koopa (from left to right)


  call objectinteract
  cp 4
  push af
  call z,.turn
  pop af
  cp 2
  jp z,otherobjectdeathjump
  cp 1
  push af
  call z,.turn
  pop af
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump



;get the y value  
  ld hl,redflyingkoopatabley
  ld a,(ix+v4)
  ld e,a
  ld d,0
  add hl,de
  
  ld (hlvar),hl


  ld hl,redflyingkoopatable
  ld a,(ix+v4)
  ld e,a
  ld d,0
  add hl,de
;value fetched from the table

 


  push hl
  call mariointeract
  pop hl
  jp c,.nextframe



  ld a,(hl)
  cp 10
  jp z,.turn ;end of table reached
  
  inc (ix+v4)

  ld a,(ix+v3)
  bit 1,a
  jp z,.negate
  
  
  ld a,(hl)
  ld (ix+v1),a
  call addv1toxbject
  
  
  jp .nonegate


.negate:

  ld a,(hl)
  neg
  ld (ix+v1),a
  call addv1toxbject


.nonegate:


  ld hl,(hlvar) ;fetch y value
  ld a,(hl)
  ld (ix+v2),a
  call addv2toyobject

  ld a,(ix+v4)
  cp 4
  ret c ;skip the first frames to do correct turning


  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  jp  z,animateflyingkoopa

  ret


.turn:

  ld (ix+v4),0

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
;  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1


.position0:
  ld    hl,spriteaddrex224  ;color address
  ld    bc,spriteaddrex224c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex227  ;color address
  ld    bc,spriteaddrex227c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.nextframe:


 
;  ld a,(marioflyslide)
;  dec a
;  jp z,.setkilljump  
 
;  ld a,(movedownsheertile)
;  dec a
;  call z,setrampjump  
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),73  ;generic set

;migrate the speed into koopa
  ld a,(ix+v3)
  bit 1,a
  jp z,.negate2
  
  
  ld (ix+v1),1
  
  
  jp .nonegate2


.negate2:

  ld (ix+v1),-1


.nonegate2:

  

;framecounter resetten voor constante tussenstukjes
  ld (ix+deathtimer),a
  ld (ix+v4),0
;  ld (ix+v3),0

  call Playsfx.kickenemy
  call makestarleft

  ret




;  ret

redflyingkoopatable:  db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,2,1,2,1,2,1,2,2,3,2,3,2,3,3,3,3,3,3,3,3,3,3,2,3,2,3,2,2,1,2,1,2,1,2,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,10
redflyingkoopatabley:  db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,0,-1,10





MovementPattern115:    ;flat elevator ;objectrelease 0 = horizontal 1 = vertical

  ld c,0
  call platforminteract ;we now set the interaction byte for all active objects that interact with the platform

;  ret ;disable movement by uncommenting this line

;temp routine to see if the flying around works
  ld hl,redflyingkoopatable
  ld a,(ix+v4)
  ld e,a
  ld d,0
  add hl,de
;value fetched from the table

  ld a,(hl)
  cp 10
  jp z,.turn ;end of table reached
  
  inc (ix+v4)

  ld a,(ix+objectrelease)
  dec a
  jp z,.vertical

.horizontal:

  ld a,(ix+v3)
  bit 1,a
  jp z,.negate
  
  
  ld a,(hl)
  ld (ix+v1),a
  call addv1toxbject
  
  
  jp .nonegate


.negate:

  ld a,(hl)
  neg
  ld (ix+v1),a
  call addv1toxbject


.nonegate:

  ld a,(ix+v4)
  cp 4
  ret c ;skip the first frames to do correct turning




  ret


.vertical:

  ld a,(ix+v3)
  bit 1,a
  jp z,.negate2
  
  
  ld a,(hl)
  ld (ix+v2),a
  call addv2toyobject
  
  
  jp .nonegate2


.negate2:

  ld a,(hl)
  neg
  ld (ix+v2),a
  call addv2toyobject


.nonegate2:

  ld a,(ix+v4)
  cp 4
  ret c ;skip the first frames to do correct turning




  ret


.turn:

  ld (ix+v4),0

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable
  
  ret



;TODO: use V5 and V5+1 as jump counters
MovementPattern116:    ;green flying koopa




  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  jp z,.otherobjectdeathjump
  cp 1
  call z,.wallfound
;  cp 3
;    jp z,.turnupsidedown
;  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin



  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toyobject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld a,(ix+v4)
  and a
  jp nz,turnkoopaaround



  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  add   a,(ix+random)
  and   3                       ;this sprite animates once every 4 frames
  call  z,animateflyingkoopa

  ld a,(ix+v5)
  dec a
  jp z,.jumpup

;  inc (ix+v5)

  call  checksheerobjecttiledown
  ret c
  call checksheerobjecttile ;check for sheer tiles
  ret c ;sheertile found
  

  call  checkobjecttilesfloor
  ret c
  call nc,.setjumpup

  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.wallfound:


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  ld (ix+v4),1 ;we turn around

  jp animateflyingkoopa

.otherobjectdeathjump:

;first we remove the sprites we no longer need then we set the sprite to the upsidedown shell
;in the end we issue the objectdeathjump

  call removeobjectspat

  ;  ;remove 2 sprites from spritelist
  ld    l,(ix+sprlstadd)
  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist

  inc hl
  inc hl
;  ld    (hl),0
  ld a,(hl)
  dec a
  ld (hl),a
  inc   hl
  ld a,(hl)
  dec a
  ld (hl),a
;  ld    (hl),0   

  
;  ;/remove sprite from spritelist  
 
  
  ld (ix+amountspr),2 ;set the new amount of spritenumbers

;save the speed of the hitting object  
  push bc
  
  call turnshellupsidedown

  pop bc

  jp otherobjectdeathjump



  ;ret



.nextframe:


 
   
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud

    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud


  ld (ix+movepat),73  ;generic set

;migrate the speed into koopa
  ld a,(ix+v3)
  bit 1,a
  jp z,.negate2
  
  
  ld (ix+v1),1
  
  
  jp .nonegate2


.negate2:

  ld (ix+v1),-1


.nonegate2:

  

;framecounter resetten voor constante tussenstukjes
  ld (ix+deathtimer),a
  ld (ix+v4),0
;  ld (ix+v3),0

  call Playsfx.kickenemy
  call makestarleft

  ret

;ret

.setjumpup:

  ld (ix+v5),1

;no ret we directly commence the jump as soon as we hit ground
 ; ret


.jumpup:
 
  
  ld e,(ix+v5+1)
  ld d,0
  ld hl,koopajumptable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endjump
  
  ld (ix+v2),a
  
  
  inc (ix+v5+1)

  ld (ix+objectfp),0



  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret


.endjump:


  call  checkobjecttilesfloor
  ret c

  ld (ix+v5),0
  ld (ix+v5+1),0

  ret



koopajumptable: db -5,-4,-4,-3,-4,-3,-2,-2,-2,-1,-2,-1,-2,-1,-1,-1,0



MovementPattern117:    ;special green starbox



;in case the box gets hit by a shell
  call objectinteract
  cp 2
  call z,handleboxinteractforobject
;  call z,handleboxinteract.popbox



  call mariointeract
  call c,handlegreenboxinteract




  ret


handlegreenboxinteract:

;is mario jumping against the box? if so handle this
  ld a,(marionotdown)
  xor %00000001
  ld b,a
  ld a,(mariofloating)
  and b
  jp nz,.popbox

  ret
  
.popbox:

  ld a,(fallspeed)
  and a
  ret nz

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  inc hl
  inc hl
  inc hl
  inc hl
  xor a
  sbc hl,de
  ret c

;  call Playsfx.coin

  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),70
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 

 
  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0

  ld a,(gold)
  cp 30
  call nc,.set1up

  ld a,(ix+objectrelease)
  cp 6
  jp z,.popcoin

  call Playsfx.powerup
  
  ret

.popcoin:

  call Playsfx.coin
  
  ret 

.set1up:

  ld (ix+objectrelease),5
  ld (ix+movepat),40

  ret


handlegreenboxinteractforobject:


;the info from the previous object is saved in iy   

;check if it is the shell going upwards

  ld a,(iy+movepat)
  cp 12
  jp z,.interact
  cp 95
  jp z,.interact
  
  ret


.interact:

  
;we check if the previous object is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  inc hl
  inc hl
  inc hl  
  xor a
  sbc hl,de
  ret c
  
  ld b,2
  call increasespritecount

 ; ld (ix+amountspr),2 ;increase sprite count

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+movepat),70
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 
 
  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0

  ld a,(ix+objectrelease)
  cp 6
  jp z,.popcoin

  call Playsfx.powerup


  ret

.popcoin:

  call Playsfx.coin
  
  ret 


;adresses that are not general are put here




MovementPattern80:

  ld (ix+hitpoints),0

  call objectinteract
  and a
  jp nz,.nextframe

.skipnextframe:

;  call mariointeract
;  jp c,MovementPattern01.nextframe
  

 

  
  ld a,(ix+deathtimer)
  cp 4
 ld b,4 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  jp nc,.slowdown


;check upwards twice
  call shelluptilecheck
  jp c,.nextframe3
 
  
 call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
 call  subv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)



  call checkobjecttilelr
  jp c,.nextframe2
 

  ret


 
 
;gaan we dropcheck in douwen


.nextframe:

  ld a,(iy+deadly)
  cp 3
  jp nz,.skipnextframe

.nextframe3:

  ld a,81
  ld (ix+movepat),a
  
  xor a
  ld (ix+deathtimer),a
  ld (ix+v2),a

  ret

.nextframe2:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:

  call lockobjectx.right

  xor a
  ld (ix+v1),a

  ret


.slowdown:

  
  sub b
  ld d,0
  ld e,a
  ld hl,shelludfallslowdowntable
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.nextframe3
  
  
  ld (ix+v2),a

;  call shelluptilecheck
;  jp c,.nextframe
 
  call shelluptilecheck
  jp c,.nextframe3 

  call  subv2toybject
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  

  call checkobjecttilelr
  jp c,.nextframe2




  ret






MovementPattern81:

  ld (ix+hitpoints),0


  call checkmariointeractshoot
  jp c,pinkshelldeathjump
 

 call mariointeract
    jp c,.interact

  
  ld a,(ix+deathtimer)
  cp 10
; ld b,15 ;resultaat opslaan zodat we alleen maar de cp telkens hoeven te wijzigen
  call c,.slowdown
  ;reset the clipping after 15 frames. The shell becomes deadly again if it was set to 0
  ;by the blockhandler or any other process
  cp 5
  call z,.setclipping

 call  addv1toxbject 
 call  addv2toybject           ;x movement of object, this adds the value in v1 to xobject (16 bits)


;links of rechts een muurtje te pakken 
  call checkobjecttilelr
  jp c,.nextframe3 
 
  ld a,(ix+standingonspr)
  and a
  jp nz,.objectspritelock


  jp checktilesmovpattern81
  
.returnfrompage3: 
  
;gaan we dropcheck in douwen

  ret


.objectspritelock:

  call objectspritelock ;lock
  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  ld (ix+movepat),84
  xor a
  ld (ix+v2),a
  ld (ix+v4),a
  
;  ld l,(ix+yobject)
;  ld h,(ix+yobject+1)
;  ld de,8
;  add hl,de
;  ld (ix+yobject),l
;  ld (ix+yobject+1),h
 
  xor a
  ld (ix+v1),a  

 
  ret


.interact:

  ld a,(marioyoshi)
  dec a
  ret z
  


  jp MovementPattern83.shellhold ;we pakken de shell direct vast maar als we hem niet vast hebben word ie meteen gereleased

;ret

.nextframe:

  ld (ix+sheertile),0
  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  call lockyobject.forcelock

;check of de shell een vaart heeft zo ja andere routine doen
;  ld a,(ix+v1)
;  and a
;  jp nz,.nextframe2

  ld (ix+movepat),84
  xor a
;  ld (ix+deadly),a
;  ld (ix+v1),a
  ld (ix+v2),a
  ld (ix+v4),a

  ret


.nextframe2:

  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  ld (ix+movepat),84
  xor a
  ld (ix+v2),a
  ld (ix+v4),a
 ;  ld (ix+deadly),a
  
  ret

.sheertilefoundright:


  ld (ix+sheertile),2
  jp .sheertile


 
;als we een schuine tile hebben gevonden dan willen we anders locken 
.sheertilefound:

    ld (ix+sheertile),1


.sheertile:

  ld (ix+clipping),4 ;status terugzetten anders krijg je vage bugs

  ld (ix+movepat),84
  xor a
  ld (ix+v2),a
  ld (ix+v4),a

  call locksheertile.left

  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
  call locksheertile.left

  xor a
  ld (ix+v1),a

  ret
 
 
  
  
.nextframe3:


  ld a,(ix+v1)
  rlca
  jp nc,.lockright

  call lockobjectx.left

  xor a
  ld (ix+v1),a

  ret 
  
.lockright:


  call lockobjectx.right

  xor a
  ld (ix+v1),a
  
  ret  




.slowdown:

;  sub b
  ld d,0
  ld e,a
  ld hl,shellfallacctable
  add hl,de
  ld a,(hl)
  
  
  ld (ix+v2),a
;  call  addv2toybject


  ret


.setclipping:

  ld (ix+clipping),2
  ret



MovementPattern82:

  inc (ix+hitpoints)


;what does clipping mean?: 
;0 = the object doesn' t interact 1 = the object acts as a wall to other objects
;2 = the object kills the other objects 3 = the object causes other objects to jump up (boxes etc)
;4 = the object acts as a wall to other objects but kills a shell that is in your hands
;5 = the object kills the other objects and turns them into coins
;6 = This object cannot be destroyed by a level exit
;7 = powerreserve kills an old powerreserve
;10 = special value used by some routines to recognize that the bullet used should now be removed from the game


  call objectinteract
  ld a,(ix+killshell)

  cp 0
  jp z,.nokillshell
  cp 3
  jp z,.nokillshell
  cp 5
  jp z,.nokillshell
  cp 6
  jp z,.nokillshell
  cp 7
  jp z,.nokillshell
  cp 10
  jp z,.nokillshell

  and a
  jp nz,.releaseshellandkill


.nokillshell:


  ld a,(nocontrol)
  cp 4
  call z,.takeshellright
  dec a
  jp z,.takeshell


;  call objectinteract
;  cp 1
;  jp z,.releaseshellandkill
;  cp 2
;  jp z,.releaseshellandkill
;  cp 4
;  jp z,.releaseshellandkill

;schelpje moet tijdelijk van het beeld verdwijnen zodra je omdraait

  ld a,(marioshell)
  ld c,a
  ld a,(marioturnshell)
  and a
  jp nz,.turnshell


;aan de hand van links of rechts de shellspeed van te voren zetten 

  ld a,(marioleftrightspr)
  dec a
  jp z,.right



  ld hl,(mariox)
  ld de,10
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,(marioduck)
  dec a
  jp z,.duck

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy
.duck: 
  ld de,15
.bigguy:  

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright

  ld a,(ix+hitpoints)
  cp 150
  jp nc,reverttokoopainhand


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  jp animateturnshellupsidedown.position0

  ret

.right:

  ld hl,(mariox)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld a,(marioduck)
  dec a
  jp z,.duck2

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy2
.duck2: 
  ld de,15
.bigguy2:   

  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframe

  ld a,(ix+hitpoints)
  cp 150
  jp nc,reverttokoopainhand

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  jp animateturnshellupsidedown.position0

  ret

.releaseshellandkill:

;do not kill shell if there is a box in play
      ld a,(iy+movepat)
      cp 47
      jp z,.nokillshell


;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a

  ;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


 
 
  jp pinkshellspecialdeathjump

 


.turnshell:

  ld a,(marioleftrightspr)
  dec a
  jp z,.turnshellright

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellrightaftertimer

.turnshellleftaftertimer:



  
  ld hl,(mariox)
  ld de,7
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
 
  
  ld a,(marioduck)
  dec a
  jp z,.duck3

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy3
.duck3: 
  ld de,15
.bigguy3: 
 
 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld a,(Controls)
  bit 5,a
  jp z,.nextframeright
  
  
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
  jp animateturnshellupsidedown.position2
  


  ret

.turnshellright:

  ld a,(marioturnshell)
  cp 3
  jp nc,.turnshellleftaftertimer

.turnshellrightaftertimer:

  ld hl,(mariox)
  ld de,-1
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  

  ld a,(marioduck)
  dec a
  jp z,.duck4

  ld a,(mariostate)
  and a
  ld de,13
  jp nz,.bigguy4
.duck4: 
  ld de,15
.bigguy4: 
  
  ld hl,(marioy)
;  ld de,15
;  xor a
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  jp animateturnshellupsidedown.position2


;  .position0:
;  ld    hl,spriteaddrex10  ;color address
;  ld    bc,spriteaddrex10c  ;character address
;  ld    de,offsetshell
;  jp    setcharcolandoffset
;
;  .position2:
;  ld    hl,spriteaddrex12  ;color address ;02
;  ld    bc,spriteaddrex12c  ;character address
;  ld    de,offsetshell
;  jp    setcharcolandoffset



;mario takes the shell into a right tube
.takeshellright: 

;  call findblockwall
;  ret c

;look at the blockwall placed behind the tube and stop the mariomovement at all times  
  push ix
  ld hl,(mariox)
  ld de,19+16-2
  add hl,de
  ex de,hl
  ld ix,(spriteixvalue)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  pop ix
  xor a
  sbc hl,de
  ret nc
;  jp c,destroyobject


;deactivate this object by hand, firm and direct

  ld    (ix+active?),0          ;our primary sprite is out of active range, set sprite inactive
  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
 
  
  ld (ix+movepat),0
  ld (ix+deathtimer),0
  ld (ix+killshell),0

  call removeobjectspat
  call removefromsprlst

;  ;remove sprite from spritelist
;  ld    l,(ix+sprlstadd)
;  ld    h,(ix+sprlstadd+1)      ;hl->address of sprite in spritelist
;  ld    b,(ix+amountspr)        ;amount of sprites
;  .cleanspritesfromspritelist:
;  ld    (hl),0
;  inc   hl
;  djnz  .cleanspritesfromspritelist
;  ;/remove sprite from spritelist



  


  ret


;mario takes the shell into a tube
.takeshell:



;ret
  ;spawn a new shell in place that is forced into a new sprite position
 

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force12 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),51                ;set object number (pinkshell)
  ld    (iy+6),0                ;set object number (pinkshell)

  call  activateobject
  
  ld (ix+v4),0

  pop ix


  jp destroyobject



;ret
  




.nextframe:


;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


  ld a,(marioturnshell)
  and a
  ret nz

  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileleft
  
  jp pattern82rammaproutineleft ;deze aanvraag returned zichzelf spagetthi warning!!


.ignoreleft:


;omhoog trappen
  ld a,(mariolook)
  dec a
  jp z,.nextframe2


  ;kick shell weg
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld a,horshellspeed
  ld (ix+v1),a

  ld (ix+deadly),1
  ld (ix+clipping),2
  
  ld (ix+objectfp),0


  ret




.nextframeright:

;transform engine een trap geven anders blijf je in een state hangen
      xor a
      ld (mariotransformp),a    
      ld (mariopickupshell),a


;shell mag niet weg terwijl je omdraaid
  ld a,(marioturnshell)
  and a
  ret nz


  ld a,(mariosheertile)
  and a
  jp nz,.kickupsheertileright


;anders kunnen we dat geintje uithalen in de lucht en tijdens het rennen

  jp pattern82rammaproutineright

  
.ignoreright:  

  ld a,(mariolook)
  dec a
  jp z,.nextframe2right
  
;.skiplookcheckright:



  ;kick shell weg
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a

  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,-horshellspeed
  ld (ix+v1),a

  ld (ix+deadly),1
  ld (ix+clipping),2

  ld (ix+objectfp),0


  ret

.nextframe2:

 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  call animateturnshellupsidedown.position0
 
  ;kick shell weg omhoog
  
  ld a,85 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a

  ld a,(mariospeed)
 ; neg
  ld (ix+v1),a


;  ld a,3
;  ld (ix+deadly),a
 
;bij een mariospeed moet het schelpje een kleine voorsprong krijgen 
  ld a,(mariospeed)
  and a
  ret z
 
  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 
  
  ret


.nextframe2right:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  call animateturnshellupsidedown.position0

  ;kick shell weg
  
  ld a,85 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  xor a
  ld (ix+v1),a
  ld (ix+deathtimer),a

  ld a,10
  ld (ix+v2),a
  
  ld a,(mariospeed)
  neg
  ld (ix+v1),a
  

;  ld a,1
;  ld (ix+deadly),a

  ld a,(mariospeed)
  and a
  ret z


  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ret


.nextframespecialleft:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld hl,(mariox)
;  ld de,22
;  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  
  ld a,-horshellspeed
  ld (ix+v1),a

  ld a,1
  ld (ix+deadly),a
  

  
  ret


.nextframespecialright:

  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld hl,(mariox)
  ld de,2
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld hl,(marioy)
  ld de,12
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ld a,horshellspeed
  ld (ix+v1),a

  ld a,1
  ld (ix+deadly),a
  


  ret


.kickupsheertileleft: 

  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,horshellspeed
  ld (ix+v1),a
  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
 
  
  call locksheertile.left
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret


.kickupsheertileright:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,-horshellspeed
  ld (ix+v1),a
;  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  call locksheertile.right
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 


  ret

;koopa has reverted in player's hand ouch
reverttokoopainhand:

  ld a,(ix+hitpoints)
  cp 170
  jp c,animaterevertkoopa

  ld a,(ix+objectrelease)
  cp 6
  jp z,reverttocavekoopa

  call removeobjectspat

  ld (ix+v1),-1
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  
  ld b,4
  call increasespritecount
  
;  ld (ix+amountspr),4
  ld (ix+deadly),1
  ld (ix+movepat),73
  ld (ix+sizeY),32
  ld (ix+clipping),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call animatekoopa
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,14
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  xor a
  ld (marioshell),a
  
  ret



reverttokoopa:

  ld a,(ix+hitpoints)
  cp 170
  jp c,animaterevertkoopa

  call removeobjectspat

  ld (ix+v1),-1
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  
  ld b,4
  call increasespritecount
  ;ld (ix+amountspr),4
  ld (ix+deadly),1
  ld (ix+movepat),73
  ld (ix+sizeY),32
  ld (ix+clipping),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call animatekoopa
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,16
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (marioshell),a

  ret

reverttocavekoopa:


  ld a,(ix+hitpoints)
  cp 170
  jp c,animaterevertkoopa


  ld (ix+v1),-1
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+deathtimer),0
  

  ld (ix+deadly),1
  ld (ix+movepat),180
  ld (ix+clipping),1

;  call animatekoopa
  xor a
    ld (marioshell),a

  ret



reverttokoopashaking:


  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  
  call addv1toxbject

  ld a,(ix+objectrelease) ;check if cave koopa
  cp 6
  jp z,reverttocavekoopa


  jp reverttokoopa


MovementPattern83:



  ld (ix+clipping),4 ;always revert to this value

  ld a,(ix+standingonspr)
  and a
  jp nz,.nofloorcheck


;do not revert to koopa if the shell is standing on the ledge
  call checkobjecttilesfloorbelowfeet
  call c,.resethitpoints


.nofloorcheck:

  ld a,(ix+hitpoints)
  cp 150
  call nc,reverttokoopashaking
  inc (ix+hitpoints)



  call objectinteract
  cp 2
  jp z,pinkshelldeathjump
  cp 4
  jp z,pinkshelldeathjump
  cp 3
  jp z,.jumpfromblock
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,pinkshellshotdeathjump


  call mariointeract
  jp c,.nextframe

;  call jumpblockcheck
;  jp c,.jumpfromblock

  call  checkobjecttilesfloor
  ret nc
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret

.resethitpoints:

  ld (ix+hitpoints),0
  ret


.jumpfromblock:

  ld a,80 
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  ld (ix+v1),a
  ld a,8
  ld (ix+deathtimer),a
  

;schelpje word nu dodelijk
;  ld a,1
;  ld (ix+deadly),a

  ret

  
.nextframe:

  ld (ix+v1),0

  ld a,(marioflyslide)
  dec a
  jp z,pinkshelldeathjump

    ld a,(mariospinjump)
    dec a
    jp z,destroyobjectwithcloud
    
      ld a,(marioyoshi)
  dec a
  jp z,destroyobjectwithcloud

  ld a,(Controls)
  bit 5,a
  jp nz,.shellhold

  ld a,7
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction 
  
    call Playsfx.kick
  

;schelpje word nu dodelijk

  ld (ix+deadly),1
  ld (ix+clipping),2
  
;vervolgens tikken we het schelpje weg van mario dus zetten we zijn coordinaten aan de hand van de richting  



  call getmarioleftright      ;out c=mario is left of object
  jp nc,.kickright

  
;  ld a,(mariosheertile)
  ld a,(ix+sheertile)
  cp 2
  jp z,.kickupsheertileright



  ld hl,(mariox)
  ld de,16
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ld a,-horshellspeed
  ld (ix+v1),a

  
  ret
  
  
.kickright:

;  ld a,(mariosheertile)
  ld a,(ix+sheertile)
  cp 1
  jp z,.kickupsheertileleft


.continue:

  ld hl,(mariox)
  ld de,22
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h 

  ld a,horshellspeed
  ld (ix+v1),a


  ret

 

.shellhold:


;als mario al een schelpje heeft dan mag het niet meer 
  ld a,(marioshell)
  dec a
  ret z

;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariopickupshell),a


  ld a,82 ;special movement pattern
  ld (ix+movepat),a

  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  
  ld (ix+deadly),a              ;niet meer deadly
  ld (ix+clipping),2
  
  ;sprite nog goed zetten just in case het is wel een afwezig. Spagetthi versus code space??
;  jp MovementPattern07.position3 ;WIP

  ld (ix+v3),3 ;reset position
  jp animateshellupsidedown

;  ret


.kickupsheertileleft: 
  
  
;//////// omhoog
  
  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,horshellspeed
  ld (ix+v1),a  
  neg
  ld (ix+v2),a


  ld a,1
  ld (ix+deadly),a
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
 
 
  
  call locksheertile.left
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 

  ret



.kickupsheertileright:


  ld a,7 ;special movement pattern
  ld (ix+movepat),a
;transform engine een trap geven
      xor a
      ld (mariotransformp),a    
      ld a,1
      ld (mariokickshell),a


  ld a,-horshellspeed
  ld (ix+v1),a
;  neg
  ld (ix+v2),a

  ld a,1
  ld (ix+deadly),a
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,8
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  call locksheertile.right
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits) 


  ret
  
    
  



