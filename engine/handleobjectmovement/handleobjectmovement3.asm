;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement3:

;start = 122


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  jp nz,keepcoverup


  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress3 - 122 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)

;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress3
;  add   hl,de
;  ;substract minimum base adres
;  ld de,122*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress3:
  jp    MovementPattern122    ;empty
  jp    MovementPattern123    ;empty
  jp    MovementPattern124    ;fish (jumping up from water)
  jp    MovementPattern125    ;watersplash
  jp    MovementPattern126    ;blockwall (for mario right)
  jp    MovementPattern127    ;blockwall (for mario left)
  jp    MovementPattern128    ;brownblockdestroyer
  jp    MovementPattern129    ;blockwallformap
  jp    MovementPattern130    ;giant watermine
  jp    MovementPattern131    ;waterplatform
  jp    MovementPattern132    ;tube inxit (right)
  jp    MovementPattern133     ;sheertileplatform
  jp    MovementPattern134     ;cactuscat main body
  jp    MovementPattern135     ;cactuscat bodypart
  jp    MovementPattern136     ;multicoinbox
  jp    MovementPattern137     ;coin release multibox
  jp    MovementPattern138     ;fish (dumb)
  jp    MovementPattern139     ;fish (ADHD)
  jp    MovementPattern140     ;fence support object for marioclimbing
  jp    MovementPattern141     ;climbing koopa (horizontal)
  jp    MovementPattern142     ;climbing koopa  (vertical)
  jp    MovementPattern143     ;blockwall (16x16)
  jp    MovementPattern144     ;flame
  jp    MovementPattern145     ;flamesplash
  jp    MovementPattern146     ;bar
  jp    MovementPattern147     ;door
  jp    MovementPattern148     ;static camerascroller
  jp    MovementPattern149     ;giant iggy block
  jp    MovementPattern150     ;falling block
  jp    MovementPattern151     ;iggy platform rotation device
  jp    MovementPattern152     ;iggy
  jp    MovementPattern153     ;iggy ball
  jp    MovementPattern154     ;parrallax scroller
  jp    MovementPattern155     ;coin special spawn
  jp    MovementPattern156     ;yellow switch
  jp    MovementPattern157     ;birds at yoshi's house, picks random color at start
  jp    MovementPattern158     ;yoshis fireplace
  jp    MovementPattern159     ;apple
  jp    MovementPattern160     ;yoshi houseexit  
  jp    MovementPattern161     ;yellow pillar  
  jp    MovementPattern162     ;blocktileanimator  
  jp    MovementPattern163     ;water (without level or current) 
  jp    MovementPattern164     ;superkoopa (walking)
  jp    MovementPattern165     ;superkoopa (cape)
  jp    MovementPattern166     ;superkoopa (flying straight)
  jp    MovementPattern167     ;superkoopa (dying)
  jp    MovementPattern168     ;superkoopa (flying with curve)

  
  
.return:
  
  ret


MovementPattern168:     ;superkoopa (flying with curve)


;handle world interactions
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  ld e,(ix+v7)
  ld hl,superkoopacurve
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.noyadd
  
  ld (ix+v2),a
  
  inc (ix+v7)

.noyadd:

  call  addv2toybject
  call  addv1toxbject


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    z,.position0
  cp 2
  jp    z,.position1
  cp 4
  jp    z,.position2
  cp 6
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 10
  ret c


  xor a
  ld (ix+v3),a


.position1:
  
  ld a,(ix+v7)
  dec a
  jp z,.position1r
  
  ld    hl,spriteaddrex377  ;color address
  ld    bc,spriteaddrex377c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position2:
  
  ld a,(ix+v7)
  dec a
  jp z,.position2r
  
  ld    hl,spriteaddrex378  ;color address
  ld    bc,spriteaddrex378c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.position1r:
  
  ld    hl,spriteaddrex381  ;color address
  ld    bc,spriteaddrex381c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position2r:

  ld    hl,spriteaddrex382  ;color address
  ld    bc,spriteaddrex382c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.nextframe:
 
;  ld a,(marioflyslide)
;  dec a
;  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),167;superkoopa (dying) ;27  ;generic set
  ld (ix+v3),0
  ld (ix+v4),0 ;reset counters and variables
  
  call Playsfx.kickenemy
 
 ;spat weghalen
   call removeobjectspat
   call removefromsprlstskiptwo
   ld (ix+amountspr),2

;framecounter resetten voor constante tussenstukjes
 ; xor a
  ld (ix+deathtimer),0

  ret

superkoopacurve: db 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,1,1,1,1,1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-3,5 



MovementPattern167:     ;superkoopa (dying)

  call addv1toxbject
  
  call objectdeathjump


  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 31 frames
  call  z,.animate


  ret

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  

  ret

.position2:
.position0:

  ld a,(ix+v1)
  sla a
  jp nc,.position0r

  ld    hl,spriteaddrex385  ;color address ;03
  ld    bc,spriteaddrex385c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position3:
.position1:

  ld a,(ix+v1)
  sla a
  jp nc,.position1r

  ld    hl,spriteaddrex386  ;color address ;03
  ld    bc,spriteaddrex386c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position0r:
  ld    hl,spriteaddrex387  ;color address ;03
  ld    bc,spriteaddrex387c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position1r:

  ld    hl,spriteaddrex388  ;color address ;03
  ld    bc,spriteaddrex388c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


;  ret


MovementPattern166:     ;superkoopa (flying straight)

;handle world interactions
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call objectinteract
;  cp 4
;  call z,.wallfound
  cp 2
  jp z,otherobjectdeathjump
;  cp 1
;  call z,.wallfound
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call  addv1toxbject

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    z,.position0
  cp 2
  jp    z,.position1
  cp 4
  jp    z,.position2
  cp 6
  jp    z,.position3
  cp 8
  jp    z,.position4
  cp 10
  ret c


  xor a
  ld (ix+v3),a


.position1:
  
  ld a,(ix+v7)
  dec a
  jp z,.position1r
  
  ld    hl,spriteaddrex377  ;color address
  ld    bc,spriteaddrex377c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position2:
  
  ld a,(ix+v7)
  dec a
  jp z,.position2r
  
  ld    hl,spriteaddrex378  ;color address
  ld    bc,spriteaddrex378c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:

  ld a,(ix+objectrelease)
  dec a
  jp z,.position1

  
  ld a,(ix+v7)
  dec a
  jp z,.position3r
  
  ld    hl,spriteaddrex379  ;color address
  ld    bc,spriteaddrex379c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position4:

  ld a,(ix+objectrelease)
  dec a
  jp z,.position2
  
  ld a,(ix+v7)
  dec a
  jp z,.position4r
  
  ld    hl,spriteaddrex380  ;color address
  ld    bc,spriteaddrex380c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.position1r:
  
  ld    hl,spriteaddrex381  ;color address
  ld    bc,spriteaddrex381c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position2r:

  ld    hl,spriteaddrex382  ;color address
  ld    bc,spriteaddrex382c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
.position3r:
  
  ld    hl,spriteaddrex383  ;color address
  ld    bc,spriteaddrex383c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
    
.position4r:

  ld    hl,spriteaddrex384  ;color address
  ld    bc,spriteaddrex384c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.nextframe:
 
;  ld a,(marioflyslide)
;  dec a
;  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),167;superkoopa (dying) ;27  ;generic set
  ld (ix+v3),0
  ld (ix+v4),0 ;reset counters and variables
  ld (ix+objectreturn),1
  
  call Playsfx.kickenemy
 
 ;spat weghalen
   call removeobjectspat
   call removefromsprlstskiptwo
   ld (ix+amountspr),2

;framecounter resetten voor constante tussenstukjes
 ; xor a
  ld (ix+deathtimer),0

  ld a,(ix+objectrelease) ;no feather variant
  dec a
  ret z

  ld (ix+movepat),26;superkoopa (walking around)
  ld (ix+v1),-1
  ld (ix+v3),2
  ld (ix+v4),15

;spawn the feather
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),17                ;set object number (17 is feather)
  ld    (iy+6),0                ;set object number (17 is feather)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0

  call makestarleft

  pop ix

  ret


MovementPattern165:     ;superkoopa (cape)

  ;check if koopa is still there, in very rare cases koopa may become inactive and the cape remains in the map
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+hitpoints)
  cp 18
  jp nz,destroyobject
  
 ; ld a,(iy+active?)
 ; and a
 ; jp z,destroyobject
  


  ;animate the cape


  ;wait a few ticks until setting the position to 1
  ld a,(ix+v4)
  cp 20
  jp c,.position0
  
  
  
.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
;  jp    z,.position0
  cp 2
  jp    z,.position1
  cp 4
  jp    z,.position2
  cp 6
  jp    z,.position3
  cp 8
  jp    z,.position4
  cp 10
  ret c


  xor a
  ld (ix+v3),a


.position1:

  ld a,(ix+v1)
  dec a
  jp z,.position1r
  
  ld    hl,spriteaddrex368  ;color address
  ld    bc,spriteaddrex368c  ;character address
  ld    de,offsetsupercape
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position0:
  
  inc (ix+v4)
  
  ld a,(ix+v1)
  dec a
  jp z,.position0r
  
  ld    hl,spriteaddrex367  ;color address
  ld    bc,spriteaddrex367c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position2:
  
  ld a,(ix+v1)
  dec a
  jp z,.position2r
  
  ld    hl,spriteaddrex369  ;color address
  ld    bc,spriteaddrex369c  ;character address
  ld    de,offsetsupercape
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position3:

  ld a,(iy+objectrelease)
  dec a
  jp z,.position1

  
  ld a,(ix+v1)
  dec a
  jp z,.position3r
  
  ld    hl,spriteaddrex370  ;color address
  ld    bc,spriteaddrex370c  ;character address
  ld    de,offsetsupercape
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position4:

  ld a,(iy+objectrelease)
  dec a
  jp z,.position2

  
  ld a,(ix+v1)
  dec a
  jp z,.position4r
  
  ld    hl,spriteaddrex371  ;color address
  ld    bc,spriteaddrex371c  ;character address
  ld    de,offsetsupercape
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.position0r:
  
  ld    hl,spriteaddrex372  ;color address
  ld    bc,spriteaddrex372c  ;character address
  ld    de,offsetsupercape2r
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
.position1r:
  
  ld    hl,spriteaddrex373  ;color address
  ld    bc,spriteaddrex373c  ;character address
  ld    de,offsetsupercaper
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
.position2r:

  ld    hl,spriteaddrex374  ;color address
  ld    bc,spriteaddrex374c  ;character address
  ld    de,offsetsupercaper
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
.position3r:
  
  ld    hl,spriteaddrex375  ;color address
  ld    bc,spriteaddrex375c  ;character address
  ld    de,offsetsupercaper
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
    
.position4r:

  ld    hl,spriteaddrex376  ;color address
  ld    bc,spriteaddrex376c  ;character address
  ld    de,offsetsupercaper
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


  ret

superkoopajumpuptable: db -4,-4,-3,-3,-2,-2,-2,-1,-1,-1,0,1,2,2,1,2,1,2,1,1,1,0,-1,0,5


MovementPattern164:     ;superkoopa (walking)

  ld a,(ix+v4)
  and a
  jp z,.spawncape ;init the cape first and init direction

  
;hij moet wel dodelijk zijn ook waneer hij wacht
  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call objectinteract
  cp 4
  call z,.wallfound
  cp 2
  call z,.removecape
  jp z,otherobjectdeathjump
  cp 1
  call z,.wallfound
  cp 3
  call z,.removecape
  jp z,otherobjectdeathjump
  cp 5
  call z,.removecape  
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  call c,.removecape
  jp c,otherobjectshotdeathjump
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ;fetch cape data into IY
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ld a,(iy+hitpoints)
  cp 19
  jp nz,destroyobject ;cape is corrupted somehow

;add coordinate data to cape as well
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8
  add hl,de
  ld (iy+xobject),l
  ld (iy+xobject+1),h

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld (iy+yobject),l
  ld (iy+yobject+1),h

;make the jump after a while
  ld a,(ix+v6)
  cp 40
  jp nc,.jumpup
  inc (ix+v6)


  xor   a                       ;reset vertical movement speed
  ld    (ix+v2),a               ;v2 is vertical movement speed and direction  

  ld    a,(framecounter)
  and   3                       ;this sprite animates once every 4 frames
  call  z,.animate


;  call  checksheerobjecttiledown
;  ret c
;  call checksheerobjecttile ;check for sheer tiles
;  ret c ;sheertile found

  call  checkobjecttilesfloor
  ret c
  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation
  ret

.jumpup:

  ld a,(ix+v7)
  and a
  call z,.speedup

;TODO: Make table for the jump and handle this jump properly
  ld e,(ix+v7)
  ld hl,superkoopajumpuptable
  ld d,0
  add hl,de
  
  ld a,(hl)
  cp 5
  jp z,.changetoflyingkoopa
  ld (ix+v2),a
  
  call addv2toybject

  inc (ix+v7)


  ret

.speedup:

;make speed increase working both ways
  ld a,(ix+v1)
  ld b,a
  rlca
  sbc a,a
  ccf
  adc a,b
  ld (ix+v1),a

  ret

.changetoflyingkoopa:

  call .removecape

  ld (ix+movepat),166
  ld (ix+v2),0
  
  ld b,4
  call increasespritecount
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

.position1start:
  
  ld a,(ix+v1) ;check if going left or right on the basis of speed
  sla a
  jp nc,.position1rstart
  
  ld    hl,spriteaddrex377  ;color address
  ld    bc,spriteaddrex377c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

.position1rstart:
  
  ld (ix+v7),1
  
  ld    hl,spriteaddrex381  ;color address
  ld    bc,spriteaddrex381c  ;character address
  ld    de,offsetflyingkoopa
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  

;ret

.wallfound:

  ld a,(ix+v2)
  and a
  jp nz,.moveonetile


;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v1)               ;v1 is movement speed and direction
  neg
  ld    (ix+v1),a               ;v1 is movement speed and direction
  ;change sprite character color and offsets (change between left and right)
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable

  call .changecape

.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex39  ;color address
  ld    bc,spriteaddrex39c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex40  ;color address
  ld    bc,spriteaddrex40c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:  
  ld    hl,spriteaddrex37  ;color address ;02
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex38  ;color address ;03
  ld    bc,spriteaddrex38c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.nextframe:

  call .removecape
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),167;superkoopa (dying) ;27  ;generic set
  ld (ix+v3),0
  ld (ix+v4),0 ;reset counters and variables
  ld (ix+objectreturn),1
  
  call Playsfx.kickenemy
 
 ;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
 ; xor a
  ld (ix+deathtimer),0

  ld a,(ix+objectrelease) ;no feather variant
  dec a
  ret z

  ld (ix+movepat),26;superkoopa (walking around)
  ld (ix+v1),-1
  ld (ix+v3),2
  ld (ix+v4),15

;spawn the feather
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),force30 ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),17                ;set object number (17 is feather)
  ld    (iy+6),0                ;set object number (17 is feather)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0

  call makestarleft

  pop ix

  ret


  
;  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;
;  ld    hl,spriteaddrex41  ;color address ;03
;  ld    bc,spriteaddrex41c  ;character address
;  ld    de,offsetshell
;  ld    a,ingamespritesblock1
;  jp    setcharcolandoffset






.removecape:

  push af
  ;fetch cape data into IY
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  
  ;remove the cape as well
  push ix
  push hl
  pop ix
  call destroyobject
  pop ix

  pop af

  ret

.changecape:

  ;fetch cape data into IY
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+v1)
  xor 1
  ld (iy+v1),a


  ret


.moveonetile:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ret


.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6 


  ret


  ;spawn the cape


.spawncape:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  
  inc hl ;allign object to correct width
  
  ld e,(ix+yobject) ;I wonder why we are not using BC for this...
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
 ; dec e

  inc de

  ;ld b,e
;--
  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),155                ;set object number (155 is supercoopa cape)
  ld    (iy+6),0                ;set object number (155 is supercoopa cape)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),165
  
  
  push ix
  pop hl ;transfer object adress
    

  pop ix
;--
  inc (ix+v4)

;store second object adress for control
  ld (ix+v5),l
  ld (ix+v5+1),h

  push hl
  pop iy ;trow word into iy for later reference

  push ix
  pop hl ;do the same on the cape, the cape itself should check for the presence of the object

  ld (iy+v5),l
  ld (iy+v5+1),h

  ld (ix+v6),0 ;just to be sure

  call getmarioleftright
  jp c,.setright


  ret

.setright:
  
  ld (iy+v1),1  
  ld (ix+v1),2
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   2
  ld    (ix+v3),a               ;v3 is used as animation variable
  
  ret 

  
  
MovementPattern163:     ;water (without level or current)

  ld a,(nocontrol)
  dec a
  ret z

  ld a,(framecounter)
  and 31
  call z,.makebubble


  ;follow mario x at ALL times
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  
  ld a,1
  ld (marioswim),a ;set mario to swimming

  ld a,(currentlevel)
  cp 66
  jp z,.checkspikes
  cp 171
  jp z,.checkspikes 
    ret

.checkspikes:    
  
  ld hl,(marioy)
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld a,(currentlevel)
  cp 171
  jp nz,.skipleftchecks
  
;cheap hack for left spikes  
  ld bc,16
  ld de,0
  call objecttilecheckfromrom
;  cp 107
;  jp z,.kill
  cp 109
  jp z,.kill
;  cp 139
;  jp z,.kill
  cp 140
  jp z,.kill  

.skipleftchecks:  
  
  ld bc,32
  ld de,0
  call objecttilecheckfromrom
  cp 103
  jp c,.nextcheck
  cp 105
  jp nc,.nextcheck

  jp .kill

.nextcheck:  
  ld bc,16
  ld a,(mariostate)
  and a
  call nz,.sethigh
  ld de,0
  call objecttilecheckfromrom
  cp 137
  ret c
  cp 138
  ret nc
  
.kill:
  
  ld a,10
  ld (mariotransforming),a
  

  ret

.sethigh:

  ld bc,8

  ret

.makebubble:

  ld a,r
  and 1
  ret z

  ld hl,(mariox)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
;  inc hl
  
  ld de,(marioy)
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc e
 ;dec e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),201                ;set object number ()
  ld    (iy+6),0                ;set object number ()

  call  activateobject
  ld (ix+objectfp),0

  pop ix


  ret
  
  
MovementPattern162:     ;blocktileanimator 

  call followmario

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  call yellowpillarhandler

  call animateyellowpillartiles

  ld    a,animationtilesblock
  call  block12


;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtilescharacteraddr+(8*36)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*36)             ;rotating tile animation 1 character address in ro
  jp z,updateblocks   

.step3:
  ld hl,animationtilescharacteraddr+(8*26)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*26)             ;rotating tile animation 1 character address in ro
  jp z,updateblocks 

.step2:
  ld hl,animationtilescharacteraddr+(8*16)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*16)             ;rotating tile animation 1 character address in ro  
  jp z,updateblocks

.step1:
  ld hl,animationtilescharacteraddr+(8*6)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*6)             ;rotating tile animation 1 character address in ro

 

updateblocks:
  
  exx 

  
  ld    hl,$4000+192*8
  call .doshit
  ld    hl,$0000+192*8 
  call .doshit

  jp setpage12tohandleobjectmovement
 
; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix32

  ret



yellowpillarhandler:
  ld a,(ix+v4)
  and a
  call z,.init

;  ld a,(framecounter) ;slow everything down. This causes the annoying timing bug to come back... :(
;  and 7
;  ret z

;use v3 for one of 6 phases of the movement
  ld a,(pillarmovementstep)
;  ld a,(ix+v3)
  dec a
  jp z,.step1Moveup
  dec a
  jp z,.step2SlowDownWhileUp
  dec a
  jp z,.step3SpeedUpWhileDown
  dec a
  jp z,.step4MoveDown
  dec a
  jp z,.step5SlowDownWhileDown
  dec a
  jp z,.step6SpeedUpWhileUp
  ret

.step1Moveup:


  ld hl,(pillary)
  dec hl
  ld a,(framecounter)
  and 3
;  jp z,.nomove
  ld (pillary),hl


.nomove:

  inc (ix+v2) ;move 100 pixels up
  ld a,(ix+v2)
  cp 97
  ret c
  
  ld (ix+v2),0 ;after 100 pixels moved up reset v2 and go to step 2

    ld a,(pillarmovementstep)
    inc a
    ld (pillarmovementstep),a

  ;inc (ix+v3) ;go to step 2 (step2SlowDownWhileUp)
  ret
  
.step2SlowDownWhileUp:

  inc (ix+v5)
  ld a,(ix+v5) ;points to yellowpillartable

  ld hl,yellowpillartable
  ld d,0
  ld e,a
  add hl,de
  
  ld a,(hl)
  cp 5
  jp nz,.notdone 
 
    ld a,(pillarmovementstep)
    inc a
    ld (pillarmovementstep),a

  ret
 
.notdone: 
 
  ld d,0
  ld e,a
  
  ld hl,(pillary)

  xor a
  sbc hl,de

  ld (pillary),hl

  ret
 
  
.step3SpeedUpWhileDown:

  dec (ix+v5)
  ld a,(ix+v5) ;points to yellowpillartable

  ld hl,yellowpillartable
  ld d,0
  ld e,a
  add hl,de
  
  ld a,(hl)
  cp 5
  jp nz,.notdone2
    
    ld a,(pillarmovementstep)
    inc a
    ld (pillarmovementstep),a
  ret

.notdone2:

  ld d,0
  ld e,a
  
    ld hl,(pillary)

  add hl,de

  ld (pillary),hl
  ret


.step4MoveDown:
  
   ld hl,(pillary)
  inc hl
    ld a,(framecounter)
  and 3
;  jp z,.nomove2
    ld (pillary),hl
.nomove2:  
  
  inc (ix+v2) ;move 100 pixels up
  ld a,(ix+v2)
  cp 97
  ret c
  
  ld a,(pillaranimationstep)
  cp 0
  ret nz ;wait for the right moment
   
  ld (ix+v2),0 ;after 100 pixels moved up reset v2 and go to step 2

    ld a,(pillarmovementstep)
    inc a
    ld (pillarmovementstep),a

  ret
  
.step5SlowDownWhileDown:

  inc (ix+v5)
  ld a,(ix+v5) ;points to yellowpillartable

  ld hl,yellowpillartable
  ld d,0
  ld e,a
  add hl,de
  
  ld a,(hl)
  cp 5
  jp nz,.notdone3
  
    ld a,(pillarmovementstep)
    inc a
    ld (pillarmovementstep),a
 ; inc (ix+v3) ;go to step 6 (step6SpeedUpWhileUp)
  ret

.notdone3:
  ld d,0
  ld e,a
  
    ld hl,(pillary)

  add hl,de

  ld (pillary),hl
  
  ret
  

.step6SpeedUpWhileUp:

  dec (ix+v5)
  ld a,(ix+v5) ;points to yellowpillartable

  ld hl,yellowpillartable
  ld d,0
  ld e,a
  add hl,de
  
  ld a,(hl)
  cp 5
  jp nz,.notdone4

  xor a
  ld (pillarspeed),a 
 
   ld a,1
   ld (pillarmovementstep),a 
  ret

.notdone4:

  ld d,0
  ld e,a

  ld hl,(pillary)

  xor a
  sbc hl,de

  ld (pillary),hl

  ret



.init:

   ld a,1
   ld (pillarmovementstep),a 
   ld (ix+v2),0
;TODO: set starting coordinate pillary
   ld hl,36*8
   ld (pillary),hl
   
   
   inc (ix+v4)
   ret
  




MovementPattern161:     ;yellow pillar

  ld a,(ix+v4)
  and a
;  jp z,inityellowpillar
  jp z,callsetblocktilesfromrom

  call moveyellowpillar ;move the object, no vram write is done here.

  call checkyellowpillartiles ;writes tiles on the map in ram, which will be written to vram next frame to the mirror page

;  call animateyellowpillartiles ;which page is active in 2 frames

  ld a,(ix+objectrelease)
  and a
  jp nz,.handlesolid


  ld c,0
  call platforminteract.objectonly ;check for objects only

  call mariointeract
  ret nc

  ;make sure that mario cannot crawl in the corners of the pillar
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16
  xor a
  sbc hl,de
  ld de,(marioy)
  xor a
  sbc hl,de
  call c,handlesolidspriteinteract

  
  ld a,1
  ld (standingonsprite),a

  ld (spriteixvalue),ix

  ret

.handlesolid:

  call mariointeract
  call c,handlesolidspriteinteract

;TODO: CRUSH MARIO!!
  ld a,(pillarmovementstep)
  cp 6
  ret nz
;hiero
  ld a,(mariofloating)
  dec a
  ret z

  call mariointeract
  jp c,.killmario

  ret

.killmario:

  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a
  
  ret


;Removed code from here to handle from other ROM position

;assume map is not bigger than page 1 and write the tile into page1 of ram
writenewyellowpillarrom:   
  
  
   ld hl,(pillary)
   ld e,(ix+offsets)
   ld d,0
   ld a,(ix+offsets+1)
   dec a
   call z,.sbc
   ld a,(ix+offsets+1)
   and a
   call z,.add

    ;ycoordinate of object in hl 16bits
    
    ld (ix+yobject),l
    ld (ix+yobject+1),h

    ;now write the whole pillar from position down get mapcoordinates first
    ld bc,8
    ld de,0
    call objecttilecheckfromrom
   
   push hl
 
  ld a,(ix+objectrelease)
  and a
  jp nz,.upwards ;in future maps there are also downwards ones but we will see what we will do then

.downwards:
    ;first clear the pillar top down
    
   ld a,(slot.page2rom)
  out ($a8),a


.killpillar:
  push hl ;save hl on stack
  ld a,(ix+sizeX) ;size of object
.loop:
  ld (hl),0
  inc hl
  sub 8
  jp nz,.loop
  pop hl

  ld de,(maplenght)
  ld bc,$8000
  add hl,de
  push hl
  xor a
  sbc hl,bc
  pop hl
  jp c,.killpillar

  pop hl

;  ld de,(maplenght)
;  add hl,de ;next row below which is found

.makepillar: ;now make the pillar
  push hl ;save hl on stack
.step3: ;the hard foreground in the pillar
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),224
  inc hl
.loop3:
  sub 8
  jp z,.end3
  ld (hl),176
  inc hl
  jp .loop3
.end3:
  ld (hl),226
  pop hl

  ld de,(maplenght)
  ld bc,$8000
  add hl,de
  push hl
  xor a
  sbc hl,bc
  pop hl
  jp c,.makepillar  
  


  ld a,(slot.page12rom)
  out ($a8),a
  
  ret

    
.upwards:
    ;first clear the pillar down to top
    
   pop hl
   ld de,(maplenght)
   xor a
   sbc hl,de
   sbc hl,de
   sbc hl,de
   push hl 
    
   ld a,(slot.page2rom)
  out ($a8),a


.killpillar2:
  push hl ;save hl on stack
  ld a,(ix+sizeX) ;size of object
.loop2:
  ld (hl),0
  inc hl
  sub 8
  jp nz,.loop2
  pop hl

  ld de,(maplenght)
  ld bc,$4000
  xor a
  sbc hl,de
  push hl
  xor a
  sbc hl,bc
  pop hl
  jp nc,.killpillar2

  pop hl

.makepillar2: ;now make the pillar
  push hl ;save hl on stack
.step32: ;the hard foreground in the pillar
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),224
  inc hl
.loop32:
  sub 8
  jp z,.end32
  ld (hl),176
  inc hl
  jp .loop32
.end32:
  ld (hl),226
  pop hl

  ld de,(maplenght)
  ld bc,$4000
  xor a
  sbc hl,de
  push hl
  xor a
  sbc hl,bc
  pop hl
  jp nc,.makepillar2  
  


  ld a,(slot.page12rom)
  out ($a8),a
  
  ret


.add:
  add hl,de
  ret
.sbc:
  xor a
  sbc hl,de  
  ret


animateyellowpillartiles:
  

  ld    a,level19animationsblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 
    ld a,(pillary)
    and 7
  
    exx

  cp 0
  ld hl,animationlvchr19+(8*119)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*119)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles   
  cp 1
  ld hl,animationlvchr19+(8*102)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*102)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles 
  cp 2
  ld hl,animationlvchr19+(8*85)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*85)             ;rotating tile animation 1 character address in ro  
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles
  cp 3
  ld hl,animationlvchr19+(8*68)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*68)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles
  cp 4
  ld hl,animationlvchr19+(8*51)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*51)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles
  cp 5
  ld hl,animationlvchr19+(8*34)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*34)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles
  cp 6
  ld hl,animationlvchr19+(8*17)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*17)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a
  jp z,updateyellowpillartiles
  cp 7
  ld hl,animationlvchr19+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in ro
    ld (pillaranimationstep),a


updateyellowpillartiles:
  
  exx 

  call getvrampageadressyellow
;  jp nz,setpage12tohandleobjectmovement
  


  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outix136
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outix136

  call setpage12tohandleobjectmovement

  ret

;/=================================================================================================


getvrampageadressyellow:


;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp z,.step1
  jp .step2

;character table at $4000
.step2:
  ld    hl,$4000+160*8
  ret

;character table at $0000
.step1:
  ld    hl,$0000+160*8
  ret



checkyellowpillartiles:

  ld a,(ix+objectrelease)
  and a
  jp nz,checkyellowpillartilesupwards

  ld a,(pillarmovementstep)
;  ld a,(ix+v3)
  cp 3
  jp z,.goingdown
  cp 4
  jp z,.goingdown
  cp 5
  jp z,.goingdown

.goingup:

  ld    bc,7                 ;vertical increase to tilecheck
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    0                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  ret nz

  ld a,(slot.page2rom)
  out ($a8),a

  call .step1 ;topline of pillar
  ld de,(maplenght)
  add hl,de

  call .step2 ;2nd or main line of pillartop
  ld de,(maplenght)
  add hl,de

  call .step3 ;the hard foreground in the pillar

  ld a,(slot.page12rom)
  out ($a8),a
  
  ret

.step0: ;black layer
  push hl
  ld a,(ix+sizeX) ;size of object
.loop0:
  ld (hl),0
  inc hl
  sub 8
  jp nz,.loop0
  pop hl
  ret

.step1: ;topline of pillar
  push hl
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),160+4
  inc hl
.loop:
  sub 8
  jp z,.end
  ld (hl),161+4
  inc hl
  sub 8
  jp z,.end
  ld (hl),162+4
  inc hl
  jp .loop
.end:
  ld (hl),163+4
  pop hl
  ret

.step2: ;2nd or main line of pillartop:
  push  hl
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),160
  inc hl
.loop2:
  sub 8
  jp z,.end2
  ld (hl),161
  inc hl
  sub 8
  jp z,.end2
  ld (hl),162
  inc hl
  jp .loop2
.end2:
  ld (hl),163
  pop hl
  ret

.step3: ;the hard foreground in the pillar
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),224
  inc hl
.loop3:
  sub 8
  jp z,.end3
  ld (hl),176
  inc hl
  jp .loop3
.end3:
  ld (hl),226
  ret

.goingdown:
  ld    bc,0+1                   ;vertical increase to tilecheck
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
;  cp    164                    ;check for tilenumber 242-246 (all diagonal tiles going down)
;  ret nz

  ld a,(slot.page2rom)
  out ($a8),a

  call .step0 ;black layer
  ld de,(maplenght)
  add hl,de

  call .step1 ;topline of pillar
  ld de,(maplenght)
  add hl,de

  call .step2 ;2nd or main line of pillartop

  ld a,(slot.page12rom)
  out ($a8),a
  ret
  
  
  
;The upwards motion
checkyellowpillartilesupwards:

  ld a,(pillarmovementstep)
;  ld a,(ix+v3)
  cp 3
  jp z,.goingdown
  cp 4
  jp z,.goingdown
  cp 5
  jp z,.goingdown


.goingup:

  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
;  cp    0                     ;check for tilenumber 242-246 (all diagonal tiles going down)
;  ret nz

  ld a,(ix+objectrelease)
  cp 2
  call z,.checkmax ;check the maximum height


  ld a,(slot.page2rom)
  out ($a8),a

  call .step3 ;topline of pillar
  ld de,(maplenght)
  add hl,de

  call .step2 ;2nd or main line of pillartop
  ld de,(maplenght)
  add hl,de

  call .step1 ;the hard foreground in the pillar
  ld de,(maplenght)
  add hl,de
  
  call .step0

  ld a,(slot.page12rom)
  out ($a8),a
  
  ret

.step0: ;black layer
  push hl
  ld a,(ix+sizeX) ;size of object
.loop0:
  ld (hl),0
  inc hl
  sub 8
  jp nz,.loop0
  pop hl
  ret

.step1: ;topline of pillar
  push hl
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),160+4+8
  inc hl
.loop:
  sub 8
  jp z,.end
  ld (hl),161+4+8
  inc hl
  sub 8
  jp z,.end
  ld (hl),162+4+8
  inc hl
  jp .loop
.end:
  ld (hl),163+4+8
  pop hl
  ret

.step2: ;2nd or main line of pillartop:
  push  hl
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),160+8
  inc hl
.loop2:
  sub 8
  jp z,.end2
  ld (hl),161+8
  inc hl
  sub 8
  jp z,.end2
  ld (hl),162+8
  inc hl
  jp .loop2
.end2:
  ld (hl),163+8
  pop hl
  ret

.step3: ;the hard foreground in the pillar
  push hl
  ld a,(ix+sizeX) ;size of object
  sub 8
  ld (hl),224
  inc hl
.loop3:
  sub 8
  jp z,.end3
  ld (hl),176
  inc hl
  jp .loop3
.end3:
  ld (hl),226
  pop hl
  ret

.goingdown:
  ld    bc,2                   ;vertical increase to tilecheck
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheckfromrom         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
;  cp    164                    ;check for tilenumber 242-246 (all diagonal tiles going down)
;  ret nz

  ld a,(ix+objectrelease)
  cp 2
  call z,.checkmax ;check the maximum height


  ld a,(slot.page2rom)
  out ($a8),a

  call .step3 ;fg layer
  ld de,(maplenght)
  add hl,de

  call .step2 ;black layer
  ld de,(maplenght)
  add hl,de

  call .step1 ;topline of pillar
  ld de,(maplenght)
  add hl,de

  call .step0 ;2nd or main line of pillartop



  ld a,(slot.page12rom)
  out ($a8),a
  ret


  ret


.checkmax:

  push hl

  ld de,8022+382+$4000 ;21* the mapheight
  xor a
  sbc hl,de
  pop hl
  jp c,.writetilesspecial


  ret

.writetilesspecial:

  ld a,(slot.page2rom)
  out ($a8),a

  ld a,(hl) ;get tilenumber
  cp 233
  jp z,.killstep1
  cp 225
  jp z,.killstep2
  cp 97
  jp z,.killstep3

  jp .return

.killstep1:
  ld de,(maplenght)
  add hl,de
  
  call .step2 ;2nd or main line of pillartop
  ld de,(maplenght)
  add hl,de

  call .step1 ;the hard foreground in the pillar
  ld de,(maplenght)
  add hl,de
  call .step0

  jp .return

.killstep2:

  ld de,(maplenght)
  add hl,de
  add hl,de
;  add hl,de
;  add hl,de

  call .step1 ;the hard foreground in the pillar
  ld de,(maplenght)
  add hl,de
  call .step0
  jp .return


.killstep3:
 
  ld de,(maplenght)
  add hl,de
  add hl,de
  add hl,de
  
  call .step0

.return:
  ld a,(slot.page12rom)
  out ($a8),a

  ;DANGEROUS but ohw well
  inc sp
  inc sp
  ret


yellowpillartable: db 5,1,1,0,0,1,0,0,1,0,0,0,1,0,0,0,5

moveyellowpillar:

  ld hl,(pillary)
  ld a,(ix+offsets)
  ld e,a
  ld d,0
  ld a,(ix+offsets+1) ;upwards or downwards offset 1 = - 0 = +
  dec a
  jp z,.sbc
  
  add hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ret

.sbc:

  xor a
  sbc hl,de


  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ret


  


  
MovementPattern160:     ;yoshi houseexit   
  
;must be standing on ground  
  ld a,(mariofloating)
  dec a
  ret z
  
  ld a,(fallspeed)
  and a
  ret nz
  
  ld hl,(mariox) ;exit left
  ld de,4
  xor a
  sbc hl,de
  jp c,.exit
  
  
  ld de,(mariox)
  ld hl,230
  xor a
  sbc hl,de
  jp c,.exit
  
  
  
  ret

  
.exit:

  inc (ix+v4)
  ld a,(ix+v4)
  
    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
  
  ld a,6
  ld (nocontrol),a
  
  ld a,(ix+v4)
  cp 60
  ret c


  ld a,1
  ld (levelend?),a
  
  xor a
  ld (fadeoutscreen?),a
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  ld (nocontrol),a
  ld (Sfxon2?),a ;darky related stuff and custom inthandlers
  
  ret
  
  
  
  
  
MovementPattern159:     ;apple   
  
  ld a,(framecounter)
  and 3
  ret nz
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  ld a,(ix+v7)
  inc (ix+v7)
  and 7
  cp 0
  jp z,.n1
;  cp 1
;  jp z,.2
  cp 2
  jp z,.n3
;  cp 3
;  jp z,.2
  cp 4
  jp z,.n1
;  cp 5
;  jp z,.2
  cp 6
  jp z,.n3
;  cp 7
;  jp z,.2
  
.n2:  
  ld    hl,spriteaddrex364  ;color address
  ld    bc,spriteaddrex364c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
  
.n1:  

  ld    hl,spriteaddrex365  ;color address
  ld    bc,spriteaddrex365c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.n3:  
  
 
  ld    hl,spriteaddrex366  ;color address
  ld    bc,spriteaddrex366c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
 
 
 ; ret
  
  

MovementPattern158:     ;yoshis fireplace  
  
  
  ld a,(ix+v4)
  and a
  jp z,.init
 
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
 
  ld a,(framecounter)
  and 15
  cp 8
  jp c,.fire2
  
  
  ld    hl,spriteaddrex362  ;color address
  ld    bc,spriteaddrex362c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
 
  
.fire2:
  
  
  ld    hl,spriteaddrex363  ;color address
  ld    bc,spriteaddrex363c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
  
 ; ret

  
.init:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,2
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  inc (ix+v4)
  ret
  
  
  
  
MovementPattern157:     ;redbird at yoshi's house, picks random color at start  
  
  ld a,(ix+objectrelease)
  cp 2
  jp nc,.continue
  inc (ix+objectrelease)
  
  ret
  
.continue:
  
  ld a,(ix+v4)
  and a
  jp z,.init
  cp 1
  jp z,.moveleft ;start with moving left
  cp 2
  jp z,.moveright
  cp 3
  jp z,.turnaround
  cp 4
  jp z,.dospecialleft
  cp 5
  jp z,.dospecialright
  
;fallthrough  
  ret
  
.moveleft:

  call makerandomnumber  
  ld b,a

  ld hl,yoshibirdmovementtable
  ld a,(ix+v7)
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.changetospecialleft

  ld (ix+v2),a
  ld (ix+v1),-1
  
  call addv1toxbject
  call addv2toyobject
  
  inc (ix+v7)

  ret
  
  

.moveright:


  call makerandomnumber  
  ld b,a

  ld hl,yoshibirdmovementtable
  ld a,(ix+v7)
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.changetospecialright

  ld (ix+v2),a
  ld (ix+v1),1
  
  call addv1toxbject
  call addv2toyobject
  
  inc (ix+v7)


  ret



.turnaround:

  ld a,(ix+v7)
  cp 3
  jp nc,.setmoveright

  inc (ix+v7)
  
  jp .animateturnaround

.dospecialleft:

  call makerandomnumber
  ld b,10
  add a,b
  ld b,a


  inc (ix+v7)

  ld a,(ix+v7)
  cp b;20;30
  jp nc,.setturnaround
  and 7
  ld b,a
  jp .animatespecialleft

  ;ret
  
.dospecialright:

  call makerandomnumber
  ld b,10
  add a,b
  ld b,a


  inc (ix+v7)

  ld a,(ix+v7)
  cp b;20;30
  jp nc,.setturnaround
  and 7
  ld b,a
  jp .animatespecialright



;ret
  
.setmoveright:


  call .animateright
  
  ld (ix+v7),0
  ld (ix+v4),2


  ret

  
.setturnaround:

  ld a,(ix+v3)
  dec a
  jp z,.ignoreright

  call makerandomnumber
  ld b,a
  cp 5
  jp c,.setleft

.ignoreright:

  ld (ix+v7),0
  ld (ix+v4),3
  ld (ix+v3),0 ;not needed but you never know

  ret
  
  
.changetospecialleft:

  inc (ix+v5)

  ld (ix+v7),0
  
  call addv1toxbject

;TODO: create left map boundary

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,48
  sbc hl,de
  jp c,.turnanyway
  
  ld a,(ix+v5)
  cp b
  ret c 
  
  ld (ix+v5),0
  ld (ix+v4),4
  
  ld (ix+v3),0
  ret

.turnanyway:  
  
  ld (ix+v5),0
  ld (ix+v4),4
  
  ld (ix+v3),1
  ret  
  
  
.changetospecialright:

  inc (ix+v5)

  ld (ix+v7),0
  
  call addv1toxbject

;TODO: create left map boundary

  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  ld hl,180
  sbc hl,de
  jp c,.turnleftanyway  
  
  
  ld a,(ix+v5)
  cp b
  ret c

  
  ld (ix+v3),1  
  ld (ix+v5),0
  ld (ix+v4),5
  ret

.turnleftanyway:

  ld (ix+v3),0
  ld (ix+v5),0
  ld (ix+v4),5
  ret
  
  
.setleft:

  call .animateturnaround

  ld (ix+v7),0
  ld (ix+v4),1

   jp .animateleft
  
  
.animatespecialleft:  
  
    ld (ix+objectrelease),0
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
    ld a,(ix+v6) ;this prevents us from making extra movementpatterns and objects
    cp 0
    jp z,.greenspecialleft
    cp 1
    jp z,.redspecialleft
    cp 2
    jp z,.yellowspecialleft
    

.redspecialleft:

  ld a,b
  cp 3
  jp z,.redspecialdown

  ld    hl,spriteaddrex348  ;color address
  ld    bc,spriteaddrex348c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.redspecialdown:
  
  ld    hl,spriteaddrex350  ;color address
  ld    bc,spriteaddrex350c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
  

.greenspecialleft:

  ld a,b
  cp 3
  jp z,.greenspecialdown


  ld    hl,spriteaddrex353  ;color address
  ld    bc,spriteaddrex353c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.greenspecialdown:  
  
  ld    hl,spriteaddrex355  ;color address
  ld    bc,spriteaddrex355c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
  

.yellowspecialleft:

  
  ld a,b
  cp 3
  jp z,.yellowspecialdown
  
  ld    hl,spriteaddrex358  ;color address
  ld    bc,spriteaddrex358c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
 
.yellowspecialdown:

  ld    hl,spriteaddrex360  ;color address
  ld    bc,spriteaddrex360c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
  
  
.animatespecialright:  
  
      ld (ix+objectrelease),0 ;hold for a little
  
  
    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
    ld a,(ix+v6) ;this prevents us from making extra movementpatterns and objects
    cp 0
    jp z,.greenspecialright
    cp 1
    jp z,.redspecialright
    cp 2
    jp z,.yellowspecialright
    

.redspecialright:

  ld a,b
  cp 3
  jp z,.redspecialdownright

  ld    hl,spriteaddrex349  ;color address
  ld    bc,spriteaddrex349c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.redspecialdownright:
  
  ld    hl,spriteaddrex351  ;color address
  ld    bc,spriteaddrex351c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset 
  
  

.greenspecialright:

  ld a,b
  cp 3
  jp z,.greenspecialdownright


  ld    hl,spriteaddrex354  ;color address
  ld    bc,spriteaddrex354c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset

  
.greenspecialdownright:  
  
  ld    hl,spriteaddrex356  ;color address
  ld    bc,spriteaddrex356c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
  

.yellowspecialright:

  
  ld a,b
  cp 3
  jp z,.yellowspecialdownright
  
  ld    hl,spriteaddrex359  ;color address
  ld    bc,spriteaddrex359c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
 
.yellowspecialdownright:

  ld    hl,spriteaddrex361  ;color address
  ld    bc,spriteaddrex361c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset  
  
   
  
.animateleft:  
  
        ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
    ld a,(ix+v6) ;this prevents us from making extra movementpatterns and objects
    cp 0
    jp z,.green
    cp 1
    jp z,.red
    cp 2
    jp z,.yellow
    

.red:

  ld    hl,spriteaddrex348  ;color address
  ld    bc,spriteaddrex348c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.green:

  ld    hl,spriteaddrex353  ;color address
  ld    bc,spriteaddrex353c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.yellow:

  
  ld    hl,spriteaddrex358  ;color address
  ld    bc,spriteaddrex358c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
 

.animateright:  
  
        ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
    ld a,(ix+v6) ;this prevents us from making extra movementpatterns and objects
    cp 0
    jp z,.greenright
    cp 1
    jp z,.redright
    cp 2
    jp z,.yellowright
    

.redright:

  ld    hl,spriteaddrex349  ;color address
  ld    bc,spriteaddrex349c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.greenright:

  ld    hl,spriteaddrex354  ;color address
  ld    bc,spriteaddrex354c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.yellowright:

  
  ld    hl,spriteaddrex359  ;color address
  ld    bc,spriteaddrex359c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
  
 
 
.animateturnaround:  
  
        ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
    ld a,(ix+v6) ;this prevents us from making extra movementpatterns and objects
    cp 0
    jp z,.greenturn
    cp 1
    jp z,.redturn
    cp 2
    jp z,.yellowturn
    

.redturn:

  ld    hl,spriteaddrex347  ;color address
  ld    bc,spriteaddrex347c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.greenturn:

  ld    hl,spriteaddrex352  ;color address
  ld    bc,spriteaddrex352c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset


.yellowturn:

  
  ld    hl,spriteaddrex357  ;color address
  ld    bc,spriteaddrex357c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset
 
  
 
 
  
;set bird colors on each spawn  
.init:  
  
  ld a,(staticpointer)
  inc a
  ld (staticpointer),a
  and 3
  ld hl,randombirdtable
  ld d,0
  ld e,a
  add hl,de
  ld a,(hl)
  
  ld (ix+v6),a ;set color type
  ld (ix+v4),1
  ld (ix+v7),0
  ld (ix+v5),0 ;count movements
 
    call .animateleft
 
  ret

  
randombirdtable: db 0,1,2
yoshibirdmovementtable: db 0,-1,-1,-2,2,1,1,5  
  

MovementPattern156:     ;yellow switch  / green switch / blue switch
 
 call initswitch ;each frame (cheap hack ;) )
;  ld a,(ix+v7)
;  and a
;  jp z,initswitch
  
  
  ld a,(ix+v4)
  and a
  jp nz,.timer
  
  
  call mariointeract
  jp c,.nextframe
  
  
  ret

.nextframe:  
  
  ld a,(mariofloating)
  and a
  jp z,blockmario
  
;  ld a,1
;  ld (touchside),a
;ret
  
;trip the screen
  ld a,1
  ld (tripscreen),a

  ld (ix+v4),1
  
;TODO: switch music here before exiting  
  ld a,1
  ld (playwinmusic),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,spriteaddrex346  ;color address
  ld    bc,spriteaddrex346c  ;character address
  ld    de,offsetsuperswitch
  ld    a,ingamespritesblock3
  jp    setcharcolandoffset



.timer:

  inc (ix+v4)
  ld a,(ix+v4)
  
  cp 30
  ret c
 
  push af 
 
    xor a
    ld (Controls),a 
    ld (mariospeed),a ;stop mario dead in his tracks
 

  ld a,5
  ld (nocontrol),a  
  pop af
  
  
  cp 160
  ret c
  
    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
  ld a,(ix+v4)
  cp 220
  ret c

  
;TODO: build in exit scene  
  ld a,1
  ld (levelend?),a

  ld a,6
  ld (nocontrol),a
  
  xor a
  ld (fadeoutscreen?),a
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  ld a,(currentlevel)
  cp 46
  jp z,.setgreenswitchactive
  cp 103
  jp z,.setblueswitchactive
  cp 105
  jp z,.setredswitchactive
  
  ld a,(yellowswitchactive)
  or 1
  ld (yellowswitchactive),a

  
  ret

.setgreenswitchactive:

  ld a,(yellowswitchactive)
  or 2
  ld (yellowswitchactive),a
  
  ret

.setblueswitchactive:

  ld a,(yellowswitchactive)
  or 8
  ld (yellowswitchactive),a
  
  ret  
  
.setredswitchactive:

  ld a,(yellowswitchactive)
  or 4
  ld (yellowswitchactive),a
  
  ret  

  
  
  
MovementPattern155:     ;coin special spawn

  ld a,(ix+v4)
  dec a
  jp z,.start


  ld a,(bluepboxactive)
  and a
  jp nz,.activate


  ld a,(ix+v4)
  and a
  ret z ;nothing to be happening yet

.start:

  ld a,(bluepboxactive)
  and a
  jp z,destroyobject


  call mariointeract
  jp c,.pickupcoin
  

  call .animate


  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  cp 4
  jp    z,.position1
  cp 8
  jp    z,.position2
  cp 12
  jp    z,.position2
  cp 13
  ret c


  xor a
  ld (ix+v3),a


.position0:
  ld    hl,spriteaddrex92  ;color address
  ld    bc,spriteaddrex92c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex93  ;color address
  ld    bc,spriteaddrex93c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex94  ;color address ;02
  ld    bc,spriteaddrex94c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex95  ;color address ;02
  ld    bc,spriteaddrex95c  ;character address
  ld    de,offsetyoshicoin
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ret

.pickupcoin:
  
  call handlecoin

  ;put whatever is needed here

  call Playsfx.coin


  jp destroyobjectwithsparkle

;ret


.activate:
  
  
  ld (ix+v4),1
  ld b,2
  call increasespritecount ;create the sprite



  ret




MovementPattern154:
  ld a,1
  ld (notileanimations),a

  call followmario


  ld    a,(framecounter)
  and   1
  jr    z,.step1

.step2:
  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  ld    hl,$0000
  jp    .setcharacterandcolortable

.step1:
  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  ld    hl,$4000

.setcharacterandcolortable:





;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    4     4    0   0  	A16 A15 A14 A13 0   0
	di
	out		($99),a         ;set character table address		
	ld		a,4+128
	out		($99),a

;    VDP() reg  7   6   5	  4   3	  2   1   0
;----------------------------------------------------------
;    3     3    A13 0  	0   1   1   1   1   1
;    10    10   0   0  	0   0   0   A16 A15 A14

  ld    a,b
	out		($99),a         ;set color table address			
	ld		a,10+128
  ei
	out		($99),a



  push hl

 
  
  ld a,(camerax)
  
  add a,a
  
  neg
  
  

;  inc a
  and 15
;  ld (animationcounter),a
 
  ld l,a
  ld h,0
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl ;*32
  ex de,hl
  
  
  ld a,worldmaptoppartofscreenblock
  call block12


;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
  ;ld    hl,$0000                                ;vram address of charactertile to write to
  pop hl
  push hl
  push de
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  pop de
  ld    hl,yellowswitchbkgchr         ;rotating tile animation 1 character address in rom
  add hl,de
  ld    b,8*4
  ld c,$98
  otir



;write the colors

;page 1
  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$2000                                ;vram address of charactertile to write to
  pop hl
  ld bc,$2000
  add hl,bc
  push de
  call  SetVdp_Write                                  ;set vdp to write at character of tile 106
  pop de
  ld    hl,yellowswitchbkgclr         ;rotating tile animation 1 character address in rom
    add hl,de
  ld    b,8*4
  ld c,$98
  otir



  ld a,romprogblock2
  call block12


  ret




MovementPattern153:     ;iggy ball

  ld a,(ix+v4)
  and a
  jp z,.init

;dirty hack. Fix the marioy position before entering mario interact
  ld hl,(marioy)
  push hl
  ld de,16
  xor a
  sbc hl,de
  ld (marioy),hl


  ld a,(mariotransforming)
  cp 10
  jp z,.skipinteract
  ld a,(mariogod)
  and a
  jp nz,.skipinteract


  call mariointeract
  jp c,.nextframe



.skipinteract:

  pop hl
  ld (marioy),hl


  inc (ix+v4)
  ld a,(ix+v4)
  cp 19
  ret c
  ld (ix+v4),20 ;overflow protection

 

  call addv1toxbject

  call .animate

  ld a,(ix+standingonspr)
  cp 4  
  push af
  call z,objectspritelock
  pop af
  jp nz,.fall

;bounce up and down
  ld hl,iggyballbouncetable
  ld a,(ix+v7)
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
 
  cp 10
  jp z,.resetbouncetable
 
  ld (ix+v2),a
  call addv2toyobject
   

  inc (ix+v7)
  
  ret

.fall:

;This is especially important when the ball needs to fall of the platform
  ld hl,fallingblocktable
  ld a,(ix+v5+1)
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
 
  ld (ix+v2),a
  call addv2toyobject
    
  cp 8
  ret z

  inc (ix+v5+1)

  ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v3)
  ld a,(ix+v3)
  and 7
  cp 1
  jp z,.position0
  cp 3
  jp z,.position1
  cp 5
  jp z,.position2

  ret

.position0:
  
  ld    hl,spriteaddrex343  ;color address
  ld    bc,spriteaddrex343c  ;character address
  ld    de,offsetiggyweapon
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  
  ld    hl,spriteaddrex344  ;color address
  ld    bc,spriteaddrex344c  ;character address
  ld    de,offsetiggyweapon
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
  
.position2:
  
  ld    hl,spriteaddrex345  ;color address
  ld    bc,spriteaddrex345c  ;character address
  ld    de,offsetiggyweapon
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


 ; ret


.resetbouncetable:

  ld (ix+v7),0
  ret


.nextframe:

  pop hl
  ld (marioy),hl


  ld a,(marioduck)
  and a
  jp nz,.killjump

  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell

  ret
 
.killjump:  
 
  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0

  ld (ix+movepat),6
  

  ret




.init:
  
  ld (ix+v4),1
  
  ld a,(ix+v5)
  dec a
  jp z,.right

.left:

  ld (ix+v1),-1

  ret

.right:
  
  ld (ix+v1),1
  
  ret



iggyballbouncetable:  db 0,0,1,2,3,2,1,0,0,10


MovementPattern152:     ;iggy

  ld a,(ix+objectrelease)
  and a
  jp nz,.endlevel

 
  ld hl,(mapheightx8)
  ld de,32
  xor a
  sbc hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.setendlevel
 
  

  ld a,(ix+v5)
  and a
  jp nz,.drop



  call mariointeract
  jp c,.setdrop


  ld a,(ix+v4)
  and a
  jp nz,.trow

;start abusing hitpoints as timer. We are out of vars
  inc (ix+hitpoints)
  ld a,(ix+hitpoints)
  cp 120
  jp nc,.settrow

  
  call setiggydirection ;see what we must do follow mario at all times with turntimer included

  call addv1toxbject


  ld a,(framecounter)
  and 3
  jp z,animateiggy

    
    ld hl,24*8 ;28*8 ;right boundary of map
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  jp c,.fall    
  

  ld a,(ix+standingonspr)
  cp 4  
  jp z,objectspritelock

.fall:  
  
;This is especially important when iggy needs to fall of the platform
  ld hl,fallingblocktable
  ld a,(ix+v5+1)
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
 
  ld (ix+v2),a
  call addv2toyobject
    
  cp 8
  ret z

  inc (ix+v5+1)


  
  ret 


.endlevel:



  ld l,(ix+v6)
  ld h,(ix+v7)
  dec hl
  ld (ix+v6),l
  ld (ix+v7),h
  ld de,0
  xor a
  sbc hl,de
  ret nz

;  inc (ix+v7)
;  ld a,(ix+v7)
;  cp 120
;  ret c

;TODO: build in exit scene  
  ld a,1
  ld (levelend?),a



  ret


.setendlevel:

  ld (ix+objectrelease),1 ;use objectrelease
  
  ld hl,350
  ld (ix+v6),l ;use this as timer
  ld (ix+v7),h ;use this as timer

  xor a
  ld (time),a ;reset timer
  

  ld a,2
  ld (playwinmusic),a

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  call Playsfx.yoshifireball

  ;make blanc sprite
  ld    hl,spriteaddrex345  ;color address
  ld    bc,spriteaddrex345c  ;character address
  ld    de,offsetiggygone
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset 



.setdrop:

  ld (ix+v5),1
  call makestarleft

  call Playsfx.chuckauw

  ret


.settrow:

  ld (ix+v4),1
  ld a,1
  ld (noplatformrotation),a
  ret


.trow:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld (ix+hitpoints),0

  inc (ix+v4)
  ld a,(ix+v4)
  cp 2
  jp z,.animatetrow0
  cp 4
  jp z,.animatetrow1
  cp 6
  jp z,.animatetrow2
  cp 8
  jp z,.animatetrow3
  cp 22
  jp z,.animatetrow4

  cp 26
  jp z,.resettrow
  ret

.resettrow:
  
  ld (ix+v4),0
  xor a
  ld (noplatformrotation),a
  ret



.animatetrow0:

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animatetrow0right


  ld    hl,spriteaddrex324  ;color address
  ld    bc,spriteaddrex324c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatetrow0right:

  ld    hl,spriteaddrex333  ;color address
  ld    bc,spriteaddrex333c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatetrow1:

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animatetrow1right


  ld    hl,spriteaddrex325  ;color address
  ld    bc,spriteaddrex325c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.animatetrow1right:

  ld    hl,spriteaddrex334  ;color address
  ld    bc,spriteaddrex334c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatetrow2:

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animatetrow2right


  ld    hl,spriteaddrex326  ;color address
  ld    bc,spriteaddrex326c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.animatetrow2right:

  ld    hl,spriteaddrex335  ;color address
  ld    bc,spriteaddrex335c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatetrow3:

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animatetrow3right


  ld    hl,spriteaddrex327  ;color address
  ld    bc,spriteaddrex327c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  call   setcharcolandoffset
  
  ;spawn the trowable object
  


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de

;  ld b,e
 
  push ix
 
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),137                ;set object number
  ld    (iy+6),0                ;set object number

  call  activateobject

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  dec hl
  dec hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v5),0


  pop ix

  ret

.animatetrow3right:

  ld    hl,spriteaddrex336  ;color address
  ld    bc,spriteaddrex336c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  call    setcharcolandoffset

  ;spawn the trowable object
  


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
 ; dec hl
 ; dec hl
  inc hl
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de

 ; ld b,e
 
  push ix
 
  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),137                ;set object number
  ld    (iy+6),0                ;set object number

  call  activateobject

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  dec hl
  dec hl
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  ld (ix+v5),1


  pop ix

  ret


  
.animatetrow4:

  ld a,(ix+v3)
  bit 1,a
  jp nz,.animatetrow4right


  ld    hl,spriteaddrex328  ;color address
  ld    bc,spriteaddrex328c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatetrow4right:

  ld    hl,spriteaddrex337  ;color address
  ld    bc,spriteaddrex337c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.drop:

  ld l,(ix+lockdata)
  ld h,(ix+lockdata+1)
  push hl
  pop iy


  call mariointeract ;keep it deadly
  
  inc (ix+v5)
  ld a,(ix+v5)
  cp 25
  jp nc,.resetdrop
  
  call animateiggy
 
  
  ld a,(iy+v3)
  cp 6
  jp c,.dropright
  
.dropleft:  
  
  ld (ix+v1),-2
  call addv1toxbject
  
  ld a,(ix+standingonspr)
  cp 4  
  jp z,objectspritelock
  
    
  jp .resetdrop  


.dropright:

  ld (ix+v1),2
  call addv1toxbject


  ld a,(ix+standingonspr)
  cp 4  
  jp z,objectspritelock

;ret


.resetdrop:

  ld (ix+v5),0
  ret


;set direction based on your position compared to iggy
setiggydirection:


  call .setspeed
  


  push bc
  call getmarioleftright
  pop bc
  jp c,.right



.left:

  ld a,c
  neg
  ld (ix+v1),a

  
  ld a,(ix+v3)
  bit 1,a
  ret z


  ld a,(ix+v3)
  res 1,a
  ld (ix+v3),a
  
  ld (ix+v6),1



  ret  



.right:

  ld (ix+v1),c


  ld a,(ix+v3)
  bit 1,a
  ret nz


  ld a,(ix+v3)
  set 1,a
  ld (ix+v3),a

  ld (ix+v6),1


  ret


.setspeed:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  ld c,0
  ret c

  ld c,1
  ld (ix+v7),0

  ret



animateiggy:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v5)
  and a
  jp nz,.animatedrop


  ld a,(ix+v6)
  and a
  jp nz,.animateturn


  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  ld    hl,spriteaddrex322  ;color address
  ld    bc,spriteaddrex322c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex323  ;color address
  ld    bc,spriteaddrex323c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex331  ;color address ;02
  ld    bc,spriteaddrex331c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex332  ;color address ;03
  ld    bc,spriteaddrex332c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animateturn:

  dec (ix+v6)
  
  ld a,(ix+v3)
  bit 1,a
  jp z,.turnright
  
  ld    hl,spriteaddrex330  ;color address ;03
  ld    bc,spriteaddrex330c  ;character address
  ld    de,offsetiggy
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.turnright:  

  ld    hl,spriteaddrex339  ;color address ;03
  ld    bc,spriteaddrex339c  ;character address
  ld    de,offsetiggyright
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.animatedrop:

  ld a,(framecounter)
  and 7
  cp 1
  jp z,.drop1
  cp 3
  jp z,.drop2
  cp 5
  jp z,.drop3

  ret

.drop1:  

  ld    hl,spriteaddrex340  ;color address ;03
  ld    bc,spriteaddrex340c  ;character address
  ld    de,offsetiggydrop
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.drop2:  

  ld    hl,spriteaddrex341  ;color address ;03
  ld    bc,spriteaddrex341c  ;character address
  ld    de,offsetiggydrop
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
  
.drop3:  

  ld    hl,spriteaddrex342  ;color address ;03
  ld    bc,spriteaddrex342c  ;character address
  ld    de,offsetiggydrop
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset  


; ret




MovementPattern151:     ;iggy platform rotation device

  ld a,(ix+v4)
  and a
  jp z,.init

  ;animate tiles on the framecounter
  call .animate

  xor a
  ld (specialsheertile),a
  ld c,3
  call platforminteract.objectonly ;check for objects only

  call mariointeract
  jp nc,.noplatforminteract

;secure against edge hopping  
  ld de,(mariox)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  xor a
  sbc hl,de
  ld a,e
  cp 192+16
  jp nc,.noplatforminteract
  ld a,d
  and a
  jp nz,.noplatforminteract
  


;check for lock distance and do not lock if mario is not close enough


  ld a,specialsheertilesblock
  call block12

  
  

.lock: 
;first obtain the xcoordinate of mario compared to the object he is standing on  
 
 
  ld a,(ix+v3) ;add offset from to table
  ld b,a
  add a,a
  add a,b ;*3
  add a,a ;*6
  add a,a ;*12
  ld d,0
  ld e,a ;continue in 16bits
  sla e
  rl d ;*24
  sla e
  rl d ;*48
  sla e
  rl d ;*96
  sla e
  rl d ;*192
  
  ld hl,iggysheertable
  add hl,de
 
 
 
 
  push hl
  
  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  xor a
  sbc hl,de
  ld a,h ;check for minus
  and a
  call nz,.setzero
  
  ld e,l ;maximum number is 64
  
  pop hl
  
  ;ld hl,sheertable
  add hl,de
  ld a,(hl) ;fetch the value from table

  ld e,a
  ld d,0
  
  push de
 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,24
  xor a
  sbc hl,de
  
  pop de
  
;  ld hl,(marioy)
  add hl,de ;substract table value from absolute lock
  
  ld de,8 ;substract base number as compensation
  xor a
  sbc hl,de

;lockvalue is now in HL

  ld de,(marioy)
  xor a
  sbc hl,de
  
;lockdistance is now in hl

  ld e,10
  ld d,0
  xor a
  sbc hl,de
  jp nc,.noplatforminteract



;/check for lock distance and do not lock if mario is not close enough


  
  
.platforminteract:

;value combination set standingonsprite for platform interaction
;set specialsheertile to make it special
  ld a,1
  ld (standingonsprite),a

;set sliding direction for mario

    ld a,(ix+v3)
    cp 6
    jp nc,.left

.right: 

  ld a,4
  ld (specialsheertile),a 

  jp .donesetdir

.left:

  ld a,3 ;iggy special
  ld (specialsheertile),a


.donesetdir:

  ld (spriteixvalue),ix

;  ld b,8
;  
;  in	a,($AA)
;	and	$F0		;only change bits 0-3
;	or	b		;take row number from B
;	out	($AA),a
;	in	a,($A9)		;read row into A
;  xor %11111111

  call readkeydirect

  bit 7,a
  jp nz,.doreset
  bit 4,a
  jp nz,.doreset

  ld a,1
  ld (mariosheerhold),a

  jp .noplatforminteract


.doreset:

  xor a
   ld (mariosheerhold),a

 ; ret
  
.noplatforminteract:


  ;inhibit platform rotation during trow
  ld a,(noplatformrotation)
  and a
  ret nz


 ; ret

  inc (ix+v7)

  ld a,(ix+v7)
  
  sub 90
  
  cp 1
  jp z,.step1
  cp 3
  jp z,.step2
  cp 5
  jp z,.step3
  cp 7
  jp z,.step4
  cp 9
  jp z,.step5
  cp 11
  jp z,.step6
  cp 13
  jp z,.step7
  cp 15
  jp z,.step8
  cp 17
  jp z,.step9
  cp 19
  jp z,.step10
  cp 21
  jp z,.step11
  cp 23
  jp z,.step12
  cp 25
  jp z,.step13
  cp 27
  jp z,.step14
  cp 29
  jp z,.step15 ;final step of first
  
  sub 90
  
  cp 31
  jp z,.step16
  cp 33
  jp z,.step17
  cp 35
  jp z,.step18
  cp 37
  jp z,.step19
  cp 39
  jp z,.step20
  cp 41
  jp z,.step21
  cp 43
  jp z,.step22
  cp 45
  jp z,.step23
  cp 47
  jp z,.step24
  cp 49
  jp z,.step25
  cp 51
  jp z,.step26
  cp 53
  jp z,.step27
  cp 55
  jp z,.step28 ;last step


  ret



.makeblackright:

  ld a,(slot.page2rom)
  out ($a8),a

  ld hl,$4000+857-32 ;mapadress of the tiles that needs changing
   
  ld a,1
  ld (hl),a
  inc hl
  ld a,0
  ld (hl),a
  inc hl
  ld a,1
  ld (hl),a

  
  call setpage12tohandleobjectmovement

  ret

.makegreyright:

  ld a,(slot.page2rom)
  out ($a8),a

  ld hl,$4000+857-32 ;mapadress of the tiles that needs changing
   
  ld a,3
  ld (hl),a
  inc hl
  ld a,2
  ld (hl),a
  inc hl
  ld a,3
  ld (hl),a

  
  call setpage12tohandleobjectmovement

  ret

.makegreyleft:

  ld a,(slot.page2rom)
  out ($a8),a

  ld hl,$4000+800+4 ;mapadress of the tiles that needs changing
   
  ld a,3
  ld (hl),a
  inc hl
  ld a,2
  ld (hl),a
  inc hl
  ld a,3
  ld (hl),a
  
  call setpage12tohandleobjectmovement

  ret


.makeblackleft:

  ld a,(slot.page2rom)
  out ($a8),a

  ld hl,$4000+800+4 ;mapadress of the tiles that needs changing
   
  ld a,1
  ld (hl),a
  inc hl
  ld a,0
  ld (hl),a
  inc hl
  ld a,1
  ld (hl),a
  
  call setpage12tohandleobjectmovement

  ret


.setzero:

  xor a
  ld l,a
  ret
 
;upload the tiledata to the vram in steps then switch the page after 1 frame. 
  
.step1:

  ld (ix+v3),0 ;parse lockdata through this value

  ld    a,level11animationblockadress ;switch to the next animations block
  
  ld hl,grapxl11a
  ld de,grapxl11aclr
  ld bc,$4000


  jp .writetiles
  


.step2:


  ld (ix+v3),1 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11b
  ld de,grapxl11bclr
  ld bc,0


  jp .writetiles


.step3:

  call .makeblackright

  ld (ix+v3),2 ;parse lockdata through this value


  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11c
  ld de,grapxl11cclr
  ld bc,$4000


  jp .writetiles

.step4:


  ld (ix+v3),3 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11d
  ld de,grapxl11dclr
  ld bc,0


  jp .writetiles

.step5:

  ld (ix+v3),4 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11e
  ld de,grapxl11eclr
  ld bc,$4000


  jp .writetiles


.step6:

  ld (ix+v3),5 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11e
  ld de,grapxl11eclr
  ld bc,0


  jp .writetiles


.step7:

  ld (ix+v3),6 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11f
  ld de,grapxl11fclr
  ld bc,$4000


  jp .writetiles


.step8:

  ld (ix+v3),7 ;parse lockdata through this value


  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11g
  ld de,grapxl11gclr
  ld bc,0


  jp .writetiles


.step9:

  ld (ix+v3),8 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11h
  ld de,grapxl11hclr
  ld bc,$4000


  jp .writetiles
  
.step10:

  ld (ix+v3),9 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11i
  ld de,grapxl11iclr
  ld bc,0


  jp .writetiles

.step11:

  ld (ix+v3),10 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11j
  ld de,grapxl11jclr
  ld bc,$4000


  jp .writetiles

.step12:

  ld (ix+v3),11 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11k
  ld de,grapxl11kclr
  ld bc,0


  jp .writetiles

.step13:

  call .makegreyleft
  

  ld (ix+v3),12 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11l
  ld de,grapxl11lclr
  ld bc,$4000


  jp .writetiles

.step14:

  ld (ix+v3),13 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress4 ;switch to the next animations block

  ld hl,grapxl11m
  ld de,grapxl11mclr
  ld bc,0


  jp .writetiles


.step15: ;last step of first

  ld (ix+v3),13 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11l
  ld de,grapxl11lclr
  ld bc,$4000


  jp .writetiles

.step16:

  call .makeblackleft


  ld (ix+v3),12 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11k
  ld de,grapxl11kclr
  ld bc,0


  jp .writetiles

.step17:

  ld (ix+v3),11 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11j
  ld de,grapxl11jclr
  ld bc,$4000


  jp .writetiles

.step18:

  ld (ix+v3),10 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress3 ;switch to the next animations block

  ld hl,grapxl11i
  ld de,grapxl11iclr
  ld bc,0


  jp .writetiles

.step19:

  ld (ix+v3),9 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11h
  ld de,grapxl11hclr
  ld bc,$4000


  jp .writetiles

.step20:

  ld (ix+v3),8 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11h
  ld de,grapxl11hclr
  ld bc,0


  jp .writetiles

.step21:

  ld (ix+v3),7 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11g
  ld de,grapxl11gclr
  ld bc,$4000


  jp .writetiles

.step22:

  ld (ix+v3),6 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11f
  ld de,grapxl11fclr
  ld bc,0


  jp .writetiles

.step23:

  ld (ix+v3),5 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress2 ;switch to the next animations block

  ld hl,grapxl11e
  ld de,grapxl11eclr
  ld bc,$4000


  jp .writetiles

.step24:

  ld (ix+v3),4 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11d
  ld de,grapxl11dclr
  ld bc,0


  jp .writetiles

.step25:

  ld (ix+v3),3 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11c
  ld de,grapxl11cclr
  ld bc,$4000


  jp .writetiles

.step26:

  call .makegreyright

  ld (ix+v3),2 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable


  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11b
  ld de,grapxl11bclr
  ld bc,0


  jp .writetiles

.step27:

  ld (ix+v3),1 ;parse lockdata through this value

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11a
  ld de,grapxl11aclr
  ld bc,$4000


  jp .writetiles

.step28:


  ld (ix+v3),0 ;parse lockdata through this value

  ld    a,%0000'1000    ;character table at $4000
	ld		b,%0000'0001    ;color table at $6000
  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11a
  ld de,grapxl11aclr
  ld bc,0

  ld (ix+v7),0 ;reset counter

  jp .writetiles


 ;hiero 

.writetiles:

  push de
  push bc
  push hl

  ld l,c
  ld h,b

  call  block12


  xor   a                                             ;reset carry, so vdp write is in page 0
  
  ld de,48
  add hl,de
  
  
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  pop hl
  
    ld de,48
  add hl,de
  
  ld c,$98
  call outi2048x

  pop hl
  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0

  ld de,48
  add hl,de

  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  pop hl

    ld de,48
  add hl,de


  ld c,$98
  call outi2048x

  ret




.init:
  
  ld (ix+v4),1

;set pixel precise height
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,4
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h

  call .makegreyright


  ld a,1 ;disable the tile animations on this map
  ld (ditileanimations),a

  ld    a,%0000'0000    ;character table at $0000
	ld		b,%0000'0000    ;color table at $2000

  call    AnimateTiles.setcharacterandcolortable

  ld    a,level11animationblockadress ;switch to the next animations block

  ld hl,grapxl11a
  ld de,grapxl11aclr
  ld bc,0

;  ld (ix+v7),0 ;reset counter

  jp .writetiles

;ret

.animate:

  ld    a,(framecounter)
  and   15
  jp    z,.tile1
  cp    4
  jp    z,.tile2
  cp    8
  jp    z,.tile3
  cp    12
  ret   nz

.tile4:

  ld a,animationtilesblock
  call block12

  ld    c,$98

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava4+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr4+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir

  jp .return

.tile1:


  ld a,animationtilesblock
  call block12

  ld    c,$98

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava1+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr1+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir

  jp .return



.tile2:


  ld a,animationtilesblock
  call block12

  ld    c,$98

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava2+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr2+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir

  jp .return



.tile3:


  ld a,animationtilesblock
  call block12

  ld    c,$98

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylava3+(8*00)         ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir
  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+0*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 0
  ld    hl,iggylavaclr3+(8*00)             ;rotating tile animation 1 character address in rom
  ld    b,8*06
  otir

 ; jp .return



;  ret

.return:

  call setpage12tohandleobjectmovement

  ret


outi2048x:


  ld b,64

.loop:

  outi : outi : outi : outi : outi : outi : outi : outi : ;*8
  outi : outi : outi : outi : outi : outi : outi : outi : ;*16
  outi : outi : outi : outi : outi : outi : outi : outi : ;*24
  outi : outi : outi : outi : outi : outi : outi : outi : ;*32
  
  djnz .loop
  
  ret








MovementPattern150:     ;falling block

  ld a,(ix+v4)
  dec a
  jp z,.fall




  call mariointeract
  jp c,.setfall





  ret


.setfall:

  ld (ix+v4),1
  ld b,2
  call increasespritecount

  call makebkgblock ;remove the original block


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ret

.fall:

  ld c,0
  call platforminteract ;make this object a platform


  ld hl,fallingblocktable
  ld a,(ix+v3)
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
 
  ld (ix+v2),a
  call addv2toyobject
    
  cp 8
  ret z

  inc (ix+v3)


  ret


fallingblocktable: db 0,1,1,1,1,2,3,3,3,4,4,4,5,6,7,8




;move the giant pillar back up
movepillarup:

    
  ;fetch tile position
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) ;add speed
  dec hl
  dec hl

  ld (ix+yobject),l
  ld (ix+yobject+1),h


  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld a,(ix+v5) ;fetch originial tile position
  ld e,a
  ld d,0
  
  xor a
  sbc hl,de ;get distance
  
  ld b,l ;pillar height in b
  ld a,b
  and a
  jp z,.destroyobject
  cp 19
  ret nc

  ld a,(pillarheight) ;only update tiles if it is truly needed
  cp l
  jp z,.nowritetile 
  ld a,l
  ld (pillarheight),a
  
  push bc
  ld bc,0
  ld de,0
  call objecttilecheckfromrom ;fetch base mapadress in hl
  push hl
  pop de
  pop bc
  
    ld    a,(slot.page2rom)                             ;all RAM except page 2
    out   ($a8),a  


;write back bkg

  push de ;save mapadress
  push bc ;save b value
  
  ;  ex de,hl ;mapadress in hl
;mapadress is now in de
  ex de,hl ;mapadress in hl
  ld de,(maplenght) ;jump 1 line lower
  add hl,de
  push hl ;temp save this obtained adress

  ld hl,pillarbkgbuffer
  ld a,(pillarheight)
  add a,a ;*2
  add a,a ;*4
  add a,a ;*8
  ld d,0
  ld e,a
  sla e
  rl d ;*16 
  add hl,de ;correct address set
 ; ex de,hl
  pop de
  
  ld bc,16
  ldir
  
  pop bc
  pop de ;get all values back
  




  ld hl,bigrocktiledata ;get the tiledata pointer

;1: Write the tiledata to the mapadres 
;2: add the position to tiledata
;3: substract maplenght from mapadres for writing to next line
;init
;hl = tiledata
;de = mapadress to write to
;bc = pillarheight
.writepillartomap:  

  push bc
  ld bc,16
  ldir
  pop bc  
  
  push hl
  ex de,hl ;now decrease the mapadress
  ld de,(maplenght)
  xor a
  sbc hl,de
  ld de,16
  xor a
  sbc hl,de
  ex de,hl
  pop hl  

  djnz .writepillartomap


.updatevram:


 call buildscreen





.nowritetile:
  
 jp MovementPattern149.nowritetile


.destroyobject:

  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+v5),0
  ld (ix+v6),0
 
  ld bc,0
  ld de,0
  call objecttilecheckfromrom ;fetch base mapadress in hl
  
    ld    a,(slot.page2rom)                             ;all RAM except page 2
    out   ($a8),a  


;write back bkg
  
  ld de,(maplenght) ;jump 1 line lower
  add hl,de
  push hl ;temp save this obtained adress

  ld hl,pillarbkgbuffer
  ld a,(pillarheight)
  pop de
  
  ld bc,16
  ldir
  
  jp destroyobject


animateiggyblocks:

  ld    a,animationtilesblock
  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,IggyCastleanimation4+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,IggyCastleanimationclr4+(8*00)             ;rotating tile animation 1 character address in ro
  ld a,168
  jp .updateblocks   

.step3:
  ld hl,IggyCastleanimation3+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,IggyCastleanimationclr3+(8*00)             ;rotating tile animation 1 character address in ro
  ld a,168  
  jp .updateblocks   

.step2:
  ld hl,IggyCastleanimation2+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,IggyCastleanimationclr2+(8*00)             ;rotating tile animation 1 character address in ro  
  ld a,168  
  jp .updateblocks   

.step1:
  ld hl,IggyCastleanimation1+(8*00)             ;rotating tile animation 1 character address in ro
  ld de,IggyCastleanimationclr1+(8*00)             ;rotating tile animation 1 character address in ro
  ld a,168  
  jp .updateblocks   

.updateblocks:
  
  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,animationlvchr19+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
;  ld   a,8
  call outix48
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,animationlvclr19+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
;  ld   a,8
  call outix48

  ret



MovementPattern149:     ;giant iggy block

  di
  call .iggyblock 
  ei
  
  ret
  
.iggyblock:

;WARNING: this call switches the general routine block away for RAM and other data
;do NOT use any calls from the generalcode!
  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  call mariointeract ;make deadly



  ld a,(ix+v4)
  and a
  jp z,.init
  cp 2
  jp z,.wait
  cp 3
  jp z,movepillarup

  ld a,(ix+v7)
  ld hl,giantpillarspeedtable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 8
  jp z,.nocount
  
  inc (ix+v7)

.nocount:
  
  ld e,a
  ld d,0
  
  ;fetch tile position
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) ;add speed
;  ld de,1 ;DEBUG
  add hl,de

  ld (ix+yobject),l
  ld (ix+yobject+1),h


  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld a,(ix+v5) ;fetch originial tile position
  ld e,a
  ld d,0
  
  xor a
  sbc hl,de ;get distance
  
  
  ld b,l ;pillar height in b
  ld a,b
  and a
  ret z ;do nothing
  cp 19
  jp nc,updategiantrocktiles.locktoground

  ld a,(pillarheight) ;only update tiles if it is truly needed
  cp l
  jp z,.nowritetile 
  ld a,l
  ld (pillarheight),a
  
  push bc
  ld bc,0
  ld de,0
  call objecttilecheckfromrom ;fetch base mapadress in hl
  push hl
  pop de
  pop bc
  
    ld    a,(slot.page2rom)                             ;all RAM except page 2
    out   ($a8),a  

  ld hl,bigrocktiledata ;get the adres

  push hl
  push de
  push bc

;mapadress is now in de
  push de
  ld hl,pillarbkgbuffer
  ld a,(pillarheight)
  dec a
  add a,a ;*2
  add a,a ;*4
  add a,a ;*8
  ld d,0
  ld e,a
  sla e
  rl d ;*16 
  add hl,de ;correct address set
  ex de,hl
  pop hl
  
  ld bc,16
  ldir
  pop bc
  pop de
  pop hl

;1: Write the tiledata to the mapadres 
;2: add the position to tiledata
;3: substract maplenght from mapadres for writing to next line
;init
;hl = tiledata
;de = mapadress to write to
;bc = pillarheight
.writepillartomap:  

  push bc
  ld bc,16
  ldir
  pop bc
  
  push hl
  ex de,hl ;now decrease the mapadress
  ld de,(maplenght)
  xor a
  sbc hl,de
  ld de,16
  xor a
  sbc hl,de
  ex de,hl
  pop hl  

  djnz .writepillartomap


;directly update the tiles in the vram
.updatevram:


  call buildscreen





.nowritetile:
  



    ld    a,(slot.page12rom)                             ;all RAM except page 2
    out   ($a8),a



  ld    a,hugetileanimationsblock
  call  block12


;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld a,(ix+yobject)
  and 7

  
    cp 0
  jp z,updategiantrocktiles.position0
    cp 1
  jp z,updategiantrocktiles.position1
    cp 2
  jp z,updategiantrocktiles.position2
    cp 3
  jp z,updategiantrocktiles.position3
    cp 4
  jp z,updategiantrocktiles.position4
    cp 5
  jp z,updategiantrocktiles.position5
    cp 6
  jp z,updategiantrocktiles.position6
    cp 7
  jp z,updategiantrocktiles.position7

  ret  


.wait:

  ld a,(ix+v7)
  cp 5
  inc (ix+v7)
  ret c
  
  inc (ix+v4)

  ret



.init:
;check the camera distance. return if we are not close enough

  ld hl,(camerax)
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8
  ld de,100 ;add some offset
  add hl,de

  ex de,hl


  ld h,(ix+xobject+1)
  ld l,(ix+xobject)

  
  xor a
  sbc hl,de
  ld a,h
  and a
  ret nz ;is larger than 255

  ld a,l
  cp 030
  ret nc


  ld (ix+v4),1

  ;store the tiledata Y in v5 
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld (ix+v5),l

  xor a
  ld (pillarheight),a ;initialize mem position

  ret



giantpillarspeedtable:  db 0,0,0,1,0,0,1,0,0,1,0,0,1,0,1,0,1,0,1,0,1,1,1,1,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,6,8


getvrampageadress:

;TODO: Adapt the whole idea in the engine
  ld a,(framecounter)
  and 1
  jp z,.step1
  jp .step2

;character table at $4000
.step2:
  ld    hl,$4000+71*8
  ret

;character table at $0000
.step1:
  ld    hl,$0000+71*8
  ret



updategiantrocktiles:

.set10:

  and %00000001
  ld c,a

  ld a,8
  add a,c
  ret



.position0:

  call getvrampageadress

  push hl


  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
 push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation1+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr1+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl

;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
;  ld hl,8*0 ;add pixel offset
;  add hl,de
;  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge

  call setpage12tohandleobjectmovement

  ret


.position1:


  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
 push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation2+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x

  pop hl

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr2+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl

;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*1 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge


  call setpage12tohandleobjectmovement

  ret

.position2:

  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
 push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation3+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr3+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl


;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*2 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge


  call setpage12tohandleobjectmovement

  ret


.position3:

  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
 push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation4+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x

  pop hl

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr4+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*3 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge



  call setpage12tohandleobjectmovement

  ret

.position4:

  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation5+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x

  pop hl

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr5+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*4 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge


  call setpage12tohandleobjectmovement

  ret

.position5:

  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation6+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr6+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*5 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge


  call setpage12tohandleobjectmovement

  ret


.position6:

  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
;  ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation7+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr7+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  pop hl
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
   
  ld hl,64*6 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge



  call setpage12tohandleobjectmovement

  ret

.position7:


  call getvrampageadress

  push hl

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
 push hl
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation8+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x

  pop hl

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr8+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x
  
  pop hl
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12

;change the vram adress into the correct one 
  ld de,71*8
  xor a
  sbc hl,de
  ld de,159*8
  add hl,de
  
  push hl
  
  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
;get the sheet and add the pillarheight  
  call .setpillarheight
  
  ld hl,64*7 ;add pixel offset
  add hl,de
  ex de,hl 
  
  ld hl,hugeblocktilesheet
  add hl,de
  
  ld c,$98
  call outix64
  pop hl

  push de

  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  
  ld a,(pillarheight)
  pop de
  
  ld hl,hugeblocktilesheetclr
  add hl,de
  
    ld c,$98
  call outix64
  
;/update the lower edge



  call setpage12tohandleobjectmovement

  ret


.locktoground:

  ;lock the object to ground
  ld l,(ix+yobject)
  ld a,l
  and %11111000
  ld (ix+yobject),a



    ld    a,(slot.page12rom)                             ;all RAM except page 2
    out   ($a8),a



  ld    a,hugetileanimationsblock
  call  block12


  ld (ix+v4),2

  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation8+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
  
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$4000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation8+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$8000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation8+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$c000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  ld    hl,bigblockanimation8+(8*00)         ;rotating tile animation 1 character address in rom
  ld c,$98
  ld   a,8
  call outi704x



  xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr8+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$6000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr8+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$a000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr8+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x
    xor   a                                             ;reset carry, so vdp write is in page 0
  ld    hl,$e000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186
  ld    hl,bigblockanimationclr8+(8*00)             ;rotating tile animation 1 character address in rom
    ld c,$98
  ld   a,8
  call outi704x

  ld a,1
  ld (tripscreen),a

  ld (ix+v7),0 ;reset counter
  ld (ix+deadly),3
  
  
;update the lower edge

  ld    a,hugetileanimationsblock2 ;switch to the next animations block
  call  block12
  
  xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$2000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148
  
  call .setpillarheight  
  ld hl,64*7 ;add pixel offset
  add hl,de
  ex de,hl 
  ld hl,hugeblocktilesheet
  add hl,de  
  ld c,$98
  call outix64


  push de


  xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$4000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheet
  add hl,de  
  ld c,$98
  call outix64

  push de

  xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$8000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheet
  add hl,de  
  ld c,$98
  call outix64

  push de

  xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$c000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheet
  add hl,de  
  ld c,$98
  call outix64

  push de




  xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$2000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheetclr
  add hl,de  
  ld c,$98
  call outix64

  push de
  
    xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$6000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheetclr
  add hl,de  
  ld c,$98
  call outix64

  push de
  
    xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$a000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheetclr
  add hl,de  
  ld c,$98
  call outix64

  push de
  
      xor   a              ;reset carry, so vdp write is in page 0
  ld    hl,$e000+159*8                                
  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  pop de

  ld hl,hugeblocktilesheetclr
  add hl,de  
  ld c,$98
  call outix64

  
  
;/update the lower edge


  call setpage12tohandleobjectmovement

  call Playsfx.explosion


  ret

.setpillarheight:

;get the sheet and add the pillarheight  
  ld a,(pillarheight)
  dec a
      cp 9
  call nc,.set10
  ld d,0
  add a,a ;*2
  add a,a ;*4
  add a,a ;*8
  ;from here on we cross the 16 bit barrier and thus require different techniques for multiplication
   ld e,a
  sla e
  rl d ;*16 
  sla e
  rl d ;*32
  sla e
  rl d ;*64
  sla e
  rl d ;*128
  sla e
  rl d ;*256
  sla e
  rl d ;*512  

  ret


outi704x:

.outi704x:

  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : outi : outi : 
  outi : outi : outi : outi : outi : outi : outi : outi : ;88 outi
  
  dec a
  
  jp nz,.outi704x


  ret




bigrocktiledata:

db 159,160,161,162,163,164,165,166,159,160,161,162,163,164,165,166 ;red line

db 143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158 ;two lowest lines
db 127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142

db 113,114,115,116,117,118,119,120,121,122,123,124,125,126,070,070 ;main section
db 099,100,101,102,103,104,105,106,107,108,109,110,111,112,070,070
db 085,086,087,088,089,090,091,092,093,094,095,096,097,098,070,070
db 071,072,073,074,075,076,077,078,079,080,081,082,083,084,070,070

db 113,114,115,116,117,118,119,120,121,122,123,124,125,126,070,070 ;main section
db 099,100,101,102,103,104,105,106,107,108,109,110,111,112,070,070
db 085,086,087,088,089,090,091,092,093,094,095,096,097,098,070,070
db 071,072,073,074,075,076,077,078,079,080,081,082,083,084,070,070

db 113,114,115,116,117,118,119,120,121,122,123,124,125,126,070,070 ;main section
db 099,100,101,102,103,104,105,106,107,108,109,110,111,112,070,070
db 085,086,087,088,089,090,091,092,093,094,095,096,097,098,070,070
db 071,072,073,074,075,076,077,078,079,080,081,082,083,084,070,070

db 113,114,115,116,117,118,119,120,121,122,123,124,125,126,070,070 ;main section
db 099,100,101,102,103,104,105,106,107,108,109,110,111,112,070,070
db 085,086,087,088,089,090,091,092,093,094,095,096,097,098,070,070
db 071,072,073,074,075,076,077,078,079,080,081,082,083,084,070,070






;=16 tiles wide


MovementPattern148:     ;static camerascroller

  ld a,(currentlevel)
;  cp 140
;  jp z,.noscroll
  cp 10
  call z,animateiggyblocks


  ld a,1
  ld (oldcamerascroll),a 

  
.noscroll:  
  
 ;follow mario around at all time
  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

;push mario if too much to the left
  ld a,(mariocoordinates+1)
  cp 20;16
  call c,.movemarioright


  ld a,(mariocoordinates+1)
  cp 10
  ret nc
  
  ld hl,(camerax) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8

  ld de,8
  add hl,de
  
  ;we missen een stukje informatie in camerax die wij toevoegen door de vdp van de camera uit te lezen
  ld		a,(VDP_8+19)
  ld e,a
  ld d,0
  xor a
  sbc hl,de
  
  
  ld (mariox),hl

  ret
  

.movemarioright:


  cp 13 ;was 8 but didn't work out so very well. I am aware that this is propably a bug but I am tired of this game!
  jp c,.kill
  
    xor a
    ld (mariospeed),a ;reset mariospeed
    ld (marioflying),a
  ld a,1
  ld (marioforcewalk),a
    ld (mariorunning),a ;reset running or else it looks wierd  

  ret

.kill:


  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a
  
  inc sp
  inc sp
  
  ret



MovementPattern147:     ;door

  ld a,(currentlevel)
  cp 35
  call z,.init

  ld a,(ix+v7)
  and a
  jp nz,.waitlonger


  ld a,(ix+v4)
  cp 40
  jp nc,.endlevel
  and a
  jp nz,.fadeout



  ld a,(ix+objectrelease)
  dec a
  jp z,.handlepboxdoor

.checkcontrols:

  ld a,(Controls)
  bit 0,a
  ret z
  
  call mariointeract
  ret nc ;no activation yet
  
  ld a,(mariospeed)
  and a
  ret nz
  
.fadeout:  
  
  ld a,5
  ld (nocontrol),a
  
;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  inc (ix+v4)
  
  ld a,(ix+objectrelease)
  cp 2
  jp z,.handlesecret
  
  call Playsfx.door
  
  ret

.handlesecret:

  ld (ix+v7),1
    ld a,1
    ld (completed),a
  
  xor a
  ld (isPlaying),a
  call stopmusic
  
  jp Playsfx.secretdoor


.endlevel:

  xor a
  ld (fadeoutscreen?),a


  ld a,1
  ld (levelend?),a

;  ld a,(ix+objectrelease)
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  ld a,l
  ld (exitx),a

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4

  ld a,l
  ld (exitx4),a ;delen door 32

  ret

.waitlonger:

  inc (ix+v7)

  cp 120
  ret c
  
  jp .endlevel


;  ret


.handlepboxdoor:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetsdoor
  ld    a,ingamespritesblock1
  call    setcharcolandoffset


  ld a,(bluepboxactive)
  and a
  ret z

  ld    hl,spriteaddrex513  ;color address
  ld    bc,spriteaddrex513c  ;character address
  ld    de,offsetsdoor
  ld    a,ingamespritesblock3
  call    setcharcolandoffset

  jp .checkcontrols
  
  ;ret

.init:

    ld a,(ix+v6)
    and a
    ret nz
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,60
    xor a
    sbc hl,de
    ret c
    
    
    ld l,(ix+xobject)
    ld h,(ix+xobject+1)
    ld de,8
    xor a
    sbc hl,de
    ld (ix+xobject),l
    ld (ix+xobject+1),h
    
    inc (ix+v6)
    ret
    



MovementPattern146:     ;bar

  ld a,(ix+v4)
  cp 20
  jp nc,destroyobject
  
  inc (ix+v4)


  ret



MovementPattern145:     ;flamesplash

  ld a,(ix+v4)
  and a
  jp z,.init

;  ld a,(ix+objectrelease)
;  dec a
;  jp z,.falldown


  call addv1toxbject

  ld a,(ix+v7)
  ld hl,flamesplashtable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,destroyobject
  
  ld (ix+v2),a
  
  call addv2toyobject

  inc (ix+v7)

.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  cp 8
  ret nc
  inc (ix+v3)
  and a
  jp z,.position0
  cp 3
  jp z,.position1
  cp 7
  jp z,.position2
  
  ret
  
.position0:
  ld    hl,spriteaddrex318  ;color address
  ld    bc,spriteaddrex318c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex319  ;color address
  ld    bc,spriteaddrex319c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex320  ;color address
  ld    bc,spriteaddrex320c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset



;  ret

;.falldown:
;
;  ld a,(ix+v7)
;  ld hl,flamesplashtable
;  ld e,a
;  ld d,0
;  add hl,de
;  ld a,(hl)
;  cp 10
;  jp z,destroyobject
;  
;  ld (ix+v2),a
;  
;  inc (ix+v7)
;  
;  call addv2toyobject
;  
;  jp .animate



.init:

  ld (ix+v4),1


  ld a,(randomtablepointer)
  inc a
  ld (randomtablepointer),a
  ld e,a
  ld d,0
  ld hl,randomtable
  add hl,de
  ld a,(hl)
  and 3
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld e,a
  ld d,0
  push af
  xor a
  sbc hl,de

  ld (ix+yobject),l
  ld (ix+yobject+1),h

  pop af

;  ld a,r
  and 1
  jp z,.left
  jp nz,.right


.left:
  
  ld (ix+v1),-1
  ret


.right:

  ld (ix+v1),1
  ret


flamesplashtable: db -1,-1,0,-1,0,0,0,1,0,1,1,10


MovementPattern144:     ;flame

  ld a,(ix+v4)
  cp 80
  inc (ix+v4)
  ret c

  ld a,(ix+v7)
  and a
  call z,.spawntail


  call mariointeract

  ld hl,flamemovementtable
  ld a,(currentlevel)
  cp 39
  call z,.sethigh
  ld a,(currentlevel)
  cp 70
  call z,.sethigh
  ld a,(currentlevel)
  cp 99
  call nc,.sethigh  
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.resettimer

  inc (ix+v7)
  
  ld (ix+v2),a

  and 15
  call z,.spawntailpart


  call addv2toyobject



;animate


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  cp 1
  jp z,.position1
  
.position0:
    
  ld a,(ix+v2)
  bit 7,a
  jp z,.position2  
    
  ld    hl,spriteaddrex314  ;color address
  ld    bc,spriteaddrex314c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  
  ld a,(ix+v2)
  bit 7,a
  jp z,.position3  
  
  ld    hl,spriteaddrex315  ;color address
  ld    bc,spriteaddrex315c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex316  ;color address
  ld    bc,spriteaddrex316c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex317  ;color address
  ld    bc,spriteaddrex317c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
  



 ; ret


.sethigh:

    ld a,(currentlevel)
    cp 151
    ret z

  ld hl,flamemovementtablehigh

  ret


.resettimer:


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld (ix+v7),0
  ld (ix+v4),0
  
  call .spawntail

;make the sprite invisible again
  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


;  ret


.spawntail:

  ld a,(ix+v4)
  and a
  call nz,Playsfx.fire

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  push de
  pop bc

 ; ld b,e


  push ix

  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc

  push bc
  push hl

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  push bc
  push hl

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145

  pop hl
  pop bc


  pop ix


  ret



.spawntailpart:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de


;  ld b,e

  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),73                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),145
;    ld (ix+objectrelease),1 ;use different movement


  pop ix

  ret


flamemovementtable: db -8,-8,-8,-8,-6,-6,-4,-4,-3,-3,-2,-1,-2,-1,-1,0,1,1,2,1,2,3,3,4,4,6,6,8,8,8,8,10
flamemovementtablehigh: db -8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-6,-6,-4,-4,-3,-3,-2,-1,-2,-1,-1,0,1,1,2,1,2,3,3,4,4,6,6,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,10


MovementPattern143:     ;blockwall (16x16)

  call mariointeract
  jp c,.push

  ret

.push:

;  ld a,(currentlevel)
;  cp 67
;  call z,.pushdown

  call getmarioleftright
  jp c,.right


.left:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,16
  xor a
  sbc hl,de
  ld (mariox),hl


  ret

.right:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,8+4
  add hl,de
  ld (mariox),hl

  ld a,(currentlevel)
  cp 53
  ret nz
  
  ld hl,(marioy)
  ld de,14
  xor a
  sbc hl,de
  ld (marioy),hl

  ret


;.pushdown:
;
;  ld a,1
;  ld (jumpingupsprite),a
;  ret




MovementPattern142:     ;climbing koopa  (vertical)

  ld a,(ix+hitpoints)
  and a
  jp z,.init

  ld a,(ix+v7)
  and a
  jp nz,.nointeract


  call mariointeract
  jp c,.otherobjectdeathjump

.nointeract:

;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,.otherobjectdeathjump
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  ld a,(ix+v4)
  cp 12
  jp c,.animatechangedir


;animate here
  
  ld a,(ix+v2)
  bit 7,a
  jp z,.positive

.negative:
  
  ld bc,08
  jp .done

.positive:
  
  ld bc,24
  
.done:  

  ld a,(ix+objectrelease)
  and a
  jp z,.checktile

  ld bc,24

  ;checkheight
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  sbc hl,de
  jp c,.setchangedir



.checktile:


  ld de,0
  call objecttilecheckfromrom
  cp 84
  call nc,.changedirwithtilecheck



  call addv2toyobject

  call animateclimbingkoopa


  ret


.animatechangedir:

  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v4)
  cp 5
  jp c,.position0
  cp 10
  jp c,.position1  

  cp 10
  jp z,.changedir

  ret

.position0:
  
  ld a,(ix+v7)
  dec a
  jp z,.position2
  
  ld    hl,spriteaddrex293  ;color address
  ld    bc,spriteaddrex293c  ;character address
  ld    de,offsetkoopaup
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:
  
  ld a,(ix+v7)
  dec a
  jp z,.position3
  
  ld    hl,spriteaddrex294  ;color address
  ld    bc,spriteaddrex294c  ;character address
  ld    de,offsetkoopaup
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset


.position2:
  ld    hl,spriteaddrex296  ;color address
  ld    bc,spriteaddrex296c  ;character address
  ld    de,offsetkoopaup
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex295  ;color address
  ld    bc,spriteaddrex295c  ;character address
  ld    de,offsetkoopaup
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

 ; ret


.setchangedir:

  cp 126
  ret nc


  ld (ix+v4),1


  ret


.changedirwithtilecheck:
 
  cp 126
  ret nc
  
  ld a,(ix+v2)
  neg
  ld (ix+v2),a

  call addv2toyobject
  call addv2toyobject
  
  ret

.changedir: 
  
  ld a,(ix+v7)
  xor 1
  ld (ix+v7),a
  
  
  ld a,(ix+v2)
  neg
  ld (ix+v2),a

  call addv2toyobject
  call addv2toyobject
  
  ret
  


.otherobjectdeathjump:
  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  
  ld    hl,spriteaddrex282  ;color address
  ld    bc,spriteaddrex282c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2 
  call    setcharcolandoffset
  
  
  jp otherobjectdeathjump

;ret

;save coordinates for upper fence change
.init:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  ld de,32
  xor a
  sbc hl,de
  
  
  ld (ix+v5),l
  ld (ix+v5+1),h

  ld (ix+hitpoints),1
  ld (ix+v4),15

  ret




animateclimbingkoopa:
  ;v3 = sprite variable
  ;v7 = 0 = in front of fence 1 = behind fence  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld    a,(ix+v7)                 ;v7 = 0 = in front of fence 1 = behind fence  
  or    a
  jp    z,.infrontoffence

 

  ld    a,(ix+objectrelease)
  and   a
  ld    iy,spriteaddrex285
  jp    z,.goanimate1
  cp 2
  jp    z,.goanimate1  
  ld    iy,spriteaddrex302

.goanimate1:
  ld    a,(ix+xobject)
  add   a,(ix+yobject)
  add   a,2
  and   7
  add   a,a                       ;a=0,2,4,6,8,10,12,14,16
  add   a,a                       ;a=0,4,8,12,16,20,24,28,
  ld    d,0
  ld    e,a
  add   iy,de

  ld    h,(iy+1)
  ld    l,(iy+0)
  inc   iy
  inc   iy
  ld    b,(iy+1)
  ld    c,(iy+0)

  ld    de,offsetkoopa
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset

.infrontoffence:

  ld    a,(ix+objectrelease)
  and   a
  ld    iy,spriteaddrex280
  jp    z,.goanimate2
  cp 2
  jp    z,.goanimate2
  ld    iy,spriteaddrex297

.goanimate2:
  ld    a,(ix+xobject)
  add   a,(ix+yobject)
  and   7
  cp    4
  ld    de,4
  jr    c,.set
  ld    de,0
.set:
  add   iy,de
  
  ld    h,(iy+1)
  ld    l,(iy+0)
  inc   iy
  inc   iy
  ld    b,(iy+1)
  ld    c,(iy+0)
  
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset

 ; ret



MovementPattern141:
;store in v7, In front of fence or behind fence?

  ld a,(ix+v7)
  and a
  jp nz,.nointeract


  call mariointeract
  jp c,.otherobjectdeathjump

.nointeract:


;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,.otherobjectdeathjump
  cp 3
  jp z,.otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  ld a,(ix+v4)
  cp 10
  jp nc,.negatedirection
  and a
  jp nz,.animatedirectionchange

  call addv1toxbject

  call z,animateclimbingkoopa
  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 102
  call nc,.setnegatedirection
 
 
   call animateclimbingkoopa
 
  
  ret




.negatedirection:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
 
  call addv1toxbject
  call addv1toxbject
  
  ld a,(ix+v3)
  xor 2
  ld (ix+v3),a
  
  ld a,(ix+v7) ;set behind fence
  xor 1
  ld (ix+v7),a
  
  
  ld (ix+v4),0
  ret


.setnegatedirection:

  cp 126
  ret nc ;tile not within boundary
  
  ld (ix+v4),1
  
 
  ret

  

.animatedirectionchange:

  inc (ix+v4)


  ld a,(ix+objectrelease)
  and a
  jp nz,.red

.green:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
   
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
;  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position0
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position2
  
  ret
  
.position0:
  
  ld    hl,spriteaddrex283  ;color address
  ld    bc,spriteaddrex283c  ;character address
  ld    de,offsetkoopaleft
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset


.position2:
  
  ld    hl,spriteaddrex284  ;color address
  ld    bc,spriteaddrex284c  ;character address
  ld    de,offsetkooparight
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset


 ; ret


.red:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
   
   
  ld    a,(ix+v3)               ;v3 is used as animation variable
  xor   1
;  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position00
  dec   a
  jp    z,.position00
  dec   a
  jp    z,.position02
  dec   a
  jp    z,.position02
  
  ret
  
.position00:
  
  ld    hl,spriteaddrex300  ;color address
  ld    bc,spriteaddrex300c  ;character address
  ld    de,offsetkoopaleft
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset


.position02:
  
  ld    hl,spriteaddrex301  ;color address
  ld    bc,spriteaddrex301c  ;character address
  ld    de,offsetkooparight
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset


 ; ret


.otherobjectdeathjump:


  ld a,(ix+objectrelease)
  and a
  jp nz,.jumpred

  
.jumpgreen:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  
  ld    hl,spriteaddrex282  ;color address
  ld    bc,spriteaddrex282c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2 
  call    setcharcolandoffset
  
  
  jp otherobjectdeathjump

;ret


.jumpred:  
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  
  ld    hl,spriteaddrex299  ;color address
  ld    bc,spriteaddrex299c  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock2 
  call    setcharcolandoffset
  
  
  jp otherobjectdeathjump

;ret



MovementPattern140:     ;fence support object for marioclimbing

  ld de,(mariox) ;follow mario around
  ld (ix+xobject),e
  ld (ix+xobject+1),d
  ld hl,(marioy)
;  ld de,16
;  add hl,de
  ld de,24
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
;  ld a,(ix+v4) ;small fall timeout to prevent bugs
;  cp 1;5
;  jp c,.wait

  ld (ix+v4),0


  ;see if the fence is touched
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  cp 163
  jp z,.fencefoundforsure ;black fence parts in mapleve7-01a
  cp 126 ;include the blue outlines as well
  jp nc,.fencefound


.nofencefound:

  ld a,(marioclimbing)
  and a
  ret z

  xor a
  ld (marioclimbing),a
  
  ld a,1
  ld (mariorunning),a ;TERMINATE RUNNING!
  
;  ld (ix+v4),1


  ret



.wait:


  inc (ix+v4)
  ret



.fencefound:

  cp 163
  jp nc,.nofencefound ;uppertile exclude

  ld bc,-8
  ld de,0
  call objecttilecheckfromrom  
  cp 126 ;check above mario and see if everything is allright this prevents very bad bugs
  ret c
  
.fencefoundforsure:  
  
  ld a,(marioclimbing)
  and a
  ret nz ;handle by different code
  
  
  ld a,(Controls)
  bit 0,a
  ret z
  bit 4,a
  ret nz
  bit 6,a
  ret nz;to prevent bunny hopping

  ld a,(marioshell)
  dec a
  ret z
  
  ld a,(marioyoshi)
  dec a
  ret z

  ld a,1
  ld (marioclimbing),a
  xor a
  ld (mariospinjump),a
  ld (marionotdown),a

  ret


MovementPattern139:     ;fish (ADHD)


  ld a,(ix+v4)
  and a
  jp z,.savey

;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  call animatefish

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call addv1toxbject
  call addv2toyobject
  
  ld a,(ix+v7) ;timer
  ld hl,fishadhdjumptable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.resetjumptable
  ld (ix+v2),a
  
  inc (ix+v7)


  ret

.resetjumptable:

  ld (ix+v7),0
  ld (ix+v2),0
;  ret


.spawnsplash:
 
;spawn the splash


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+v5) ;always at the fixed height of the waterlevel
  ld d,(ix+v5+1)
;  ld e,(ix+yobject)
;  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
 ; dec e

  inc de

 ; ld b,e


  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),74                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),125
    

  pop ix


  ret



;use the saved value only to determine the animation parameters
.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  ld de,12
  xor a
  sbc hl,de
  
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  ret

;its a little complicated because mario has different variable usage in the water.
;to prevent too much tampering on the mariointeract routine we simply take care of the hits via
;this route
.nextframe:

  ld a,(marioswim)
  dec a
  jp z,.setkillmario
  
  jp otherobjectdeathjump


  
  
.setkillmario:


  ld a,(mariogod)
  and a
  ret nz

  ld a,(marioyoshi)
  dec a
  jp z,handleyoshihit


  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell


    ret

fishadhdjumptable: db -3,-2,-2,-2,-1,-1,-1,0,1,1,1,2,2,2,3,5



MovementPattern138:     ;fish (dumb)


  ld (ix+hitpoints),1


  ld a,(ix+v3)
  and a
  jp nz,.right

.left:

  ld (ix+v1),-1

  jp .move

.right:

  ld (ix+v1),1
  


.move:

  call addv1toxbject

  inc (ix+v4)
  ld a,(ix+v4)
  cp 40
  call nc,.changedir



;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe

  ld a,(currentlevel)
  cp 53
  jp z,.animatefish ;make exclusion for this wierd map...
 
  ld a,(waterlevel)
  and a
  jp nz,.animatefish

  call animatefish 
 
 
  ret

.animatefish:

  ld a,(framecounter)
  and 3
  ret nz


 ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v7)               ;v3 is used as animation variable
  xor   1
  ld    (ix+v7),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3

.position0:
  
  ld a,(ix+v1)
  dec a
  jp z,.position2
  
  ld    hl,spriteaddrex253  ;color address
  ld    bc,spriteaddrex253c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position1:

  ld a,(ix+v1)
  dec a
  jp z,.position3  
  
  ld    hl,spriteaddrex254  ;color address
  ld    bc,spriteaddrex254c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex255  ;color address
  ld    bc,spriteaddrex255c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex256  ;color address
  ld    bc,spriteaddrex256c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

;  ret


 

;its a little complicated because mario has different variable usage in the water.
;to prevent too much tampering on the mariointeract routine we simply take care of the hits via
;this route
.nextframe:

  ld a,(marioswim)
  dec a
  jp z,.setkillmario
  
  jp otherobjectdeathjump
  
  
.setkillmario:


  ld a,(mariogod)
  and a
  ret nz

  ld a,(marioyoshi)
  dec a
  jp z,handleyoshihit


  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell

  ret

.changedir:

  ld a,(ix+v3)
  xor %00000001
  ld (ix+v3),a
  
  ld (ix+v4),0
  
  ret



MovementPattern129:    ;blockwallformap


  ld hl,(marioy)
  ld (ix+yobject),l
  ld (ix+yobject+1),h



  ld a,(marioleftright)
  and a
  ret z
 

  call mariointeract
  ret nc

  ld a,1
  ld (touchside),a
  ret



keepcoverup:

  ld l,(ix+movepat+1)
  ld a,(ix+movepat)
  xor l
  cp 123
  jp z,MovementPattern123    ;water coverupsprite

  ret



MovementPattern128:    ;brownblockdestroyer


  ld a,(bluepboxactive)
  and a
  ret z
  
  call transformintobrownblock

  jp destroyobject.objectreturns


  ret



MovementPattern127:    ;blockwall (for mario left)


  ld a,(marioleftright)
  dec a
  ret z


  call mariointeract
  ret nc

  ld a,2
  ld (touchside),a
  xor a
  ld (mariospeed),a
  ret



MovementPattern126:    ;blockwall (for mario right)

  ld a,(marioleftright)
  and a
  ret z
 
 

  call mariointeract
  ret nc

  ld a,1
  ld (touchside),a
  ret




animatefish:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  and   7
  cp    4
  ld    hl,spriteaddrex253 ;color address in air Right
  ld    bc,spriteaddrex253c ;character address in air Right
  ld    de,spriteaddrex261 ;color address in water alt Right
  exx
  ld    hl,spriteaddrex257 ;color address in water Right
  ld    bc,spriteaddrex257c ;character address in water Right
  ld    de,spriteaddrex261c ;character address in water alt Right
  exx
  jr    c,.characterandcoloraddressesfound
  ld    hl,spriteaddrex254 ;color address in air Right
  ld    bc,spriteaddrex254c ;character address in air Right
  ld    de,spriteaddrex262 ;color address in water alt Right
  exx
  ld    hl,spriteaddrex258 ;color address in water Right
  ld    bc,spriteaddrex258c ;character address in water Right
  ld    de,spriteaddrex262c ;character address in water alt Right
  exx
.characterandcoloraddressesfound:

  ld    a,(ix+hitpoints)
  dec   a
  jr    nz,.aircheckdone        ;fish is in the air

  ;fish is in water use the watersprite and set the correction depending on the framecounter
  exx                           ;in water
  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter, so does the waterfish sprite
  and   15
  cp    3
  jr    c,.watercorrectionfoundalt2
  cp    15
  jr    z,.watercorrectionfoundalt2
  cp    7
  jr    c,.watercorrectionfound2
  cp    11
  jr    nc,.watercorrectionfound2

.watercorrectionfoundalt2:
  push  de
  exx
  pop   bc
  push  de
  pop   hl
.watercorrectionfound2:
  ;/fish is in water use the watersprite and set the correction depending on the framecounter
  
.aircheckdone:
  ld    a,(ix+v3)               ;v3=0 fish going left, v3=1 fish going right
  or    a
  jr    z,.leftorrightcheckdone

  ld    de,192                  ;if sprites is looking right, then add 192 to hl and bc
  add   hl,de

  ex    de,hl
  add   hl,bc
  push  hl
  pop   bc
  ex    de,hl

.leftorrightcheckdone:
  ld    a,ingamespritesblock2 
  ld    de,offsetshell
  jp    setcharcolandoffset




MovementPattern125:    ;watersplash
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc   a
  ld    (ix+v3),a               ;v3 is used as animation variable
  cp    1
  jp    z,.position0
  cp    3
  jp    z,.position1
  cp    5
  jp    z,.position2
  cp    7
  jp    z,.position3
  cp    10
  jp    z,.position1
  cp    13
  jp    z,.position0
  cp    16
  ret c

  jp destroyobject
 

.position0:
  ld    hl,spriteaddrex265  ;color address
  ld    bc,spriteaddrex265c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2 
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex266  ;color address
  ld    bc,spriteaddrex266c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex267  ;color address ;02
  ld    bc,spriteaddrex267c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex268  ;color address ;02
  ld    bc,spriteaddrex268c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset
 
;ret


;crap out of variables. Use hitpoints to determine if waterlevel is reached
MovementPattern124:    ;fish (jumping up from water)

;buffer y coordinates for the first time in order to determine the maximum water level
  ld a,(ix+v4)
  and a
  jp z,.savey


  call .getwaterlevel ;see if fish is underwater

;the basic interaction stuff
  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcoin
  
  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump

;animate here

  call animatefish

  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe
    
;swimming around?
  ld a,(ix+v6)
  and a
  jp z,.swimleftright
  cp 16
  jp z,.jumpupanddown

  inc (ix+v6)


  ret

.jumpupanddown:

;use v7 as table pointer
  ld a,(ix+v7)
  ld hl,fishjumptable
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  cp 10
  jp z,.endjumpupanddown
  cp 0
  call z,.resetv4to1
  ld (ix+v2),a
  
  
  inc (ix+v7)

  push af
  call addv2toyobject
  pop af

  cp -8
  jp z,.getwaterlevel2 ;see if we are allready out of the water

  call .getwaterlevel ;see if fish is underwater

  ld a,(ix+hitpoints)
  cp 1
  call z,.spawnsplash


  ret
  
.endjumpupanddown:

  ld (ix+v6),0
  ld (ix+v7),0
;  ld (ix+hitpoints),0
;  ld (ix+v4),1

  ret

.spawnsplash:

  ld a,(ix+v4)
  cp 2
  ret z

  ld (ix+v4),2
;TODO: do not spawn the splash after the jump is done. spawn the splash when the water level is reached.
;do that only once per jump! use v4 for that do not reset v4 to 0
 
;spawn the splash


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+v5) ;always at the fixed height of the waterlevel
  ld d,(ix+v5+1)
;  ld e,(ix+yobject)
;  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc e
 ; dec e

 ; ld b,e


  push ix

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),74                ;set object number (23 is debris)
  ld    (iy+6),0                ;set object number (23 is debris)

  call  activateobject
  ld (ix+deathtimer),0
  ld (ix+v1),0
    ld (ix+v2),0
    ld (ix+movepat),125
    

  pop ix


  ret
 
.resetv4to1:

  ld (ix+v4),1
  ret


.getleftright:

  ld a,(ix+lockdata)
  dec a
  jp z,.right
  
.left:
 
  ld hl,fishswimtablel
  ret
 
.right:

  ld hl,fishswimtabler
  ret

.swimleftright:
  ld (ix+v4),1
  
  call .getleftright

;use v7 as table pointer
  ld a,(ix+v7)
;  ld hl,fishswimtablel
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)

  cp 10
  jp z,.endswimleftright

  
  ld (ix+v1),a
 
   inc (ix+v7)
  
  bit 7,a ;check for -
  jp z,.setv3right
  
  ld (ix+v3),0

 
  jp addv1toxbject

.setv3right:

  ld (ix+v3),1

 
  jp addv1toxbject



;  ret

.endswimleftright:

  ld a,(ix+v3) ;how did we end? buffer that
  ld (ix+lockdata),a

  ld (ix+v7),0
  ld (ix+v6),1
  ret


;use the saved value only to determine the animation parameters
.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  push hl
  
  dec hl
  dec hl
  dec hl
  dec hl
  
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  pop hl
  ld a,(ix+objectrelease)
  ld e,a
  ld d,0
  add hl,de
  
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  
  ret

;its a little complicated because mario has different variable usage in the water.
;to prevent too much tampering on the mariointeract routine we simply take care of the hits via
;this route
.nextframe:

  ld a,(marioswim)
  dec a
  jp z,.setkillmario
  
  jp otherobjectdeathjump
  
  
.setkillmario:


  ld a,(mariogod)
  and a
  ret nz

  ld a,(marioyoshi)
  dec a
  jp z,handleyoshihit


  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell


    ret


.getwaterlevel:

  ld (ix+hitpoints),0

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  xor a
  ex de,hl
  sbc hl,de
  ret nc

  ld (ix+hitpoints),1 ;we are in the water


  ret

.getwaterlevel2:


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  inc de
  xor a
;  ex de,hl
  sbc hl,de
  ret nc


  jp .spawnsplash



fishswimtablel:  db -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,10
fishswimtabler:  db 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,10
fishjumptable:   db -8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-8,-6,-6,-4,-3,-2,-3,-2,-2,-1,-2,-1,-1,0,1,1,2,1,2,2,3,2,3,4,6,6,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,10


MovementPattern123:    ;empty

  ret



MovementPattern122:    ;empty


  ret


MovementPattern132:    ;tube inxit (right left)

  ld a,1
  ld (nocontrol),a
  ld (mariospeed),a
  ld (yoshispecial),a

  ld hl,(mariox)
  inc hl
  ld (mariox),hl

  ld a,(ix+objectrelease)
  and a
  call nz,.moveleft

  ld a,1
  ld (spritehideractive),a

;move mario in 1px per frame
  ld a,(ix+v4)
  cp 20
  jp nc,.end


  inc (ix+v4)

  ret


  
.end:


  xor a
  ld (nocontrol),a
  ld (mariospeed),a
  ld (spritehideractive),a ;disable the spritehider
  ld (yoshispecial),a ;reset the byte

  ld (ix),0 ;HACK! I don't know whats going on here

  jp destroyobject



;ret


.moveleft:

  ld hl,(mariox)
  dec hl
  dec hl
  ld (mariox),hl


  ret


MovementPattern131:    ;waterplatform
 
 ;buffer y coordinates for the first time in order to determine the maximum water level
  ld a,(ix+v4)
  and a
  jp z,.savey
 
  call mariointeract
  call c,.unsetswim
  
  ld c,0
  call platforminteract ;WArning: objects that do checktilebelow will also be affected by this platform

  ld a,(ix+v3)
  dec a
  jp z,.noyadd


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ex de,hl
  xor a
  sbc hl,de
  jp c,.noyadd


  ld (ix+v2),-1
  call addv2toyobject


.noyadd:
  ld (ix+v3),0

  ld a,(ix+objectrelease)
  and a
  ret nz

;animate platform
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    hl,spriteaddrex274  ;color address
  ld    bc,spriteaddrex274c  ;character address
  exx
  ld    hl,spriteaddrex273  ;color address
  ld    bc,spriteaddrex273c  ;character address
  exx
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    hl,spriteaddrex273  ;color address
  ld    bc,spriteaddrex273c  ;character address
  exx
  ld    hl,spriteaddrex274  ;color address
  ld    bc,spriteaddrex274c  ;character address
  exx
  cp    7
  jr    c,.watercorrectionfound
  ld    hl,spriteaddrex274  ;color address
  ld    bc,spriteaddrex274c  ;character address
  exx
  ld    hl,spriteaddrex273  ;color address
  ld    bc,spriteaddrex273c  ;character address
  exx
  cp    11
  jr    c,.watercorrectionfound
  ld    hl,spriteaddrex273  ;color address
  ld    bc,spriteaddrex273c  ;character address
  exx
  ld    hl,spriteaddrex274  ;color address
  ld    bc,spriteaddrex274c  ;character address
  exx
.watercorrectionfound:

  ld    a,(ix+yobject)
  and   1
  jr    nz,.evenoddcheckdone
  exx
.evenoddcheckdone:
  
  ld    de,offsetwaterplatform
  ld    a,ingamespritesblock2 
  jp  setcharcolandoffset
;/animate platform

 ; ret

.unsetswim:

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  push de
  ld de,16
  add hl,de
  pop de
  xor a
  sbc hl,de
  ret nc


  xor a
  ld (marioswim),a

  ld (ix+v2),1
  ld (ix+v3),1
  call addv2toyobject


  ret 

.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  dec hl
  dec hl
  dec hl
  dec hl
  
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret



MovementPattern130:    ;giant watermine

  ld a,(ix+v4)
  and a
  jp z,.setv1


  call mariointeract
  jp c,.nextframe
  

  call addv1toxbject

;animate mine
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)        ;watercoverupsprite is depending on the framecounter
  and   15
  ld    hl,spriteaddrex269  ;color address
  ld    bc,spriteaddrex269c  ;character address
  cp    3
  jr    c,.watercorrectionfound
  cp    15
  jr    z,.watercorrectionfound
  ld    hl,spriteaddrex271  ;color address
  ld    bc,spriteaddrex271c  ;character address
  cp    7
  jr    c,.watercorrectionfound
  ld    hl,spriteaddrex270  ;color address
  ld    bc,spriteaddrex270c  ;character address
  cp    11
  jr    c,.watercorrectionfound
  ld    hl,spriteaddrex272  ;color address
  ld    bc,spriteaddrex272c  ;character address
.watercorrectionfound:
  
  ld    de,offsetmine
  ld    a,ingamespritesblock2 
  call  setcharcolandoffset
;/animate platform
  ret

;do this only once
.setv1:

  ld (ix+v4),1

  call getmarioleftright
  ret nc

  ld (ix+v1),2


  ret



.nextframe:

  ld a,(marioswim)
  dec a
  jp z,.setkillmario
  
  ret
  
  
.setkillmario:

  ld a,(mariogod)
  and a
  ret nz


  ld a,(marioyoshi)
  dec a
  jp z,handleyoshihit

  ld a,10
  ld (mariotransforming),a

  xor a
  ld (mariotransformp),a ;VERY IMPORTANT!!!! if not set a real MSX may become DAMAGED!
  ld (marioshell),a ;release the shell


    ret




MovementPattern134:     ;cactuscat main body


  ld a,(ix+v4)
  cp 5 ;amount of bodypieces to spawn
  jp c,.init 

  ld a,(ix+hitpoints)
  cp 3
  jp c,tempdropcactusobjects


  ld a,(ix+objectrelease)
  dec a
  jp z,.dropdead


  call objectinteract
  ld a,(iy+movepat)
  cp 7
  jp z,.killframe ;shell kills this guy


  call mariointeract
  jp c,.nextframe


  call checkobjecttilescheckwall
  jp    c,.wallfound            ;if wall found then change direction by inverting v1, and set animation

  call  checksheerobjecttiledown
  jp c,.nofloorcheck
  call checksheerobjecttile ;check for sheer tiles
  jp c,.nofloorcheck ;sheertile found

  call checkobjecttilesfloor

.nofloorcheck:

  call addv1toxbject
  call addv2toyobject
;    ld (ix+v2),0


  call .setdirectionleft
  call getmarioleftright
  call c,.setdirectionright
  

  ld hl,pokeyspeedtable
  ld a,(ix+v3)
  and a
  call z,.negate
  ld a,(ix+v6)
  cp 7
  call nc,.resetspeed
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld (ix+v1),a

  inc (ix+v6)


  jp handlecactusbodyparts


 ; ret

.resetspeed:

  ld (ix+v6),0

  ret


.negate:
  
  ld hl,pokeyspeedtableneg
  ret

  

.wallfound:

;  .hardforegroundfound:
  ;change direction
  ld    a,(ix+v3)               ;v1 is movement speed and direction
  xor 1
  ld    (ix+v3),a               ;v1 is movement speed and direction

  ld a,(ix+v1)
  neg 
  ld (ix+v1),a
  
  jp addv1toxbject

.setdirectionleft:

  call getmariodistance
  cp 40
  ret c

  ld (ix+v3),0


  ret


.setdirectionright:

  call getmariodistance
  cp 40
  ret c


  ld (ix+v3),1

  ret


.nextframe:

  ld a,(movedownsheertile)
  and a
  ret z
  
.killframe:

  call Playsfx.kickenemy


  ld    (ix+updatespr?),2
  ld (ix+objectreturn),1
  ld (ix+objectrelease),1


  ld (ix+deathtimer),0

  ld (ix+clipping),0

  ld a,b ;snelheid van wat het object had geraakt
  ;moet nog 3 vanaf
  rlca
  jp c,.left
  
  ld (ix+v1),3
  jp makestarright
  
  
;  ret
  
.left:

    ld (ix+v1),-3
    jp makestarleft
  



;  ret


.dropdead:


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

 call objectdeathjump

  jp handlecactusbodyparts

 ; ret




;spawn the body parts
.init:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc e
 ; dec e

  ld b,e


  push ix

;adjust b
  ld a,(ix+v4)
  push af
  add a,a ;*2
  ld c,a
  ld a,b
  sub c
  ld b,a
  

  
  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),118                ;set object number
  ld    (iy+6),0                ;set object number

  call  activateobject

  ld (ix+objectfp),0
  ld (ix+deathtimer),0
  pop af
  ld (ix+v3),a ;save the ID for the head in V3
  ld (ix+v1),a ;save the ID for the head in v1

  push ix
  pop de ;put new object ix into de

  pop hl ;pop main ix into hl

;save the main body ix into segments v5  
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  push hl
  pop ix ;restore main ix


;write part data into table
  push de ;save object IX data in de
  
  
  ld hl,offsets+20 ;skip first offsets bytes
  add a,a ;we write words!!
  ld e,a ;(ix+v4) still intact
  ld d,0
  add hl,de ;position to write to
  ex de,hl
  push ix : pop hl
  add hl,de ;pointer for table set
  pop de ;get the objects IX data
  ld (hl),e
  inc hl
  ld (hl),d
  
  and a
  jp nz,.skipstep ;only do this initwrite once

;make table from down to up
  ld (ix+offsets+4),8 ;define lower table limit

  ld (ix+offsets+5),0 ;object id
  ld (ix+offsets+6),0 ;object position
  ld (ix+offsets+7),1 ;object id
  ld (ix+offsets+8),1 ;object position
  ld (ix+offsets+9),2 ;object id
  ld (ix+offsets+10),2 ;object position
  ld (ix+offsets+11),3 ;object id
  ld (ix+offsets+12),3 ;object position
  ld (ix+offsets+13),4 ;object id
  ld (ix+offsets+14),4 ;object position
  
  ld (ix+offsets+15),7 ;define upper table limit
  
;5 = object = gone
;6 = deleted
;7 = table limit

.skipstep:

  inc (ix+v4) ;next object


  ret


pokeyspeedtable:    db 0,0,1,0,0,1,0,0
pokeyspeedtableneg: db 0,0,-1,0,0,-1,0,0



handlecactusbodyparts:

;check height compared to mapheight if absolute depth is reached tell the heads to die
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,24
  add hl,de
  ex de,hl
  ld hl,(mapheightx8)
  xor a
  sbc hl,de
  ld c,0
  jp c,.killoffbodyparts


  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  ld c,(ix+yobject)
  ld b,(ix+yobject+1)
  ld (devar),de
  ld (bcvar),bc


  ld de,offsets+5
  push ix
  pop hl ;get the pointer for our table
  add hl,de
  ld a,(hl)
  cp 5
  jp z,rearangecactus
  cp 6
  call nz,.movepart
  inc hl
  inc hl
  ld a,(hl)
  cp 5
  jp z,rearangecactus
  cp 6
  call nz,.movepart
  inc hl
  inc hl
  ld a,(hl)
  cp 5
  jp z,rearangecactus
  cp 6
  call nz,.movepart
  inc hl
  inc hl
  ld a,(hl)
  cp 5
  jp z,rearangecactus
  cp 6
  call nz,.movepart
  inc hl
  inc hl
  ld a,(hl)
  cp 5
  jp z,rearangecactus
  cp 6
  jp nz,.movepart 
  
  
  
  ret



.movepart:
  push hl


  push ix
  pop hl
  ld de,offsets+20
  add hl,de ;go to the datatable
  add a,a ;a*2
  ld e,a
  ld d,0
  add hl,de ;go to the object's pointer
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch the list
  push de
  pop iy


  ld hl,cactuswobbletable
  add a,a ;*4
  add a,a ;*8
  add a,a ;*16
  ld e,a
  ld d,0
  add hl,de ;add id number
  ld a,(framecounter)
  and %00001111 ;limit to 15 entries
  ld e,a
  ld d,0
  add hl,de
  ld a,(hl)
  ld e,a
  ld d,0

  ld hl,(devar)
  add hl,de ;add offset  
  ld (iy+xobject),l
  ld (iy+xobject+1),h
  
  pop hl
  push hl
  inc hl
  ld a,(hl)
  
  ld hl,(bcvar)
  
  add a,a ;*2
  add a,a ;*4
  add a,a ;*8
  add a,a ;*16
  
  ld e,a
  ld d,0
  xor a
  sbc hl,de
  
  ld (iy+yobject),l
  ld (iy+yobject+1),h
 
  pop hl
 
  ret


.killoffbodyparts:


;see if object is active
  ld de,offsets+5
  push ix
  pop hl
  add hl,de
  ld e,c
  ld d,0
  add hl,de
  inc c
  inc c
  ld a,(hl)
  cp 6
  jp z,.killoffbodyparts
  cp 5
  jp z,.killoffbodyparts 
  cp 7
  jp z,destroyobject.objectreturns
  dec c
  dec c
  
  push ix
  pop hl
  ld de,offsets+20
  add hl,de ;go to the datatable
  ld e,c
  ld d,0
  add hl,de ;go to the object's pointer
  ld e,(hl)
  inc hl
  ld d,(hl) ;fetch the list
  push de
  pop iy
  
  ld (iy+v4),1

  inc c
  inc c
  
  jp .killoffbodyparts

tempdropcactusobjects:

  inc (ix+hitpoints)

  ret



;start from the bottom and work up the positions
rearangecactus:


  ld (ix+hitpoints),1
;TODO: before doing this enable v4 in the above objects for about 3 frames before calling this. This will make the column
;of cactus drop smoothly ;)

  ld de,offsets+5
  push ix
  pop hl
  add hl,de

  ld c,0

  jp .start

.nextpart:

  inc hl
  inc hl

.start:

  ld a,(hl)
  cp 7
  jp z,.resetendoftable ;end of table reached. Set new end of table and remake the head
  cp 5
  call z,.setsix
  cp 6
  jp z,.nextpart
  ;active part found set position
  inc hl
  ld a,c
  ld (hl),a
  inc c
  dec hl
  
  jp .nextpart

;  ret


.setsix:

  ld a,6
  ld (hl),a
  ret

.resetendoftable:

  dec hl
  dec hl ;look at previous value
  
  ld a,(hl)
  cp 6
  jp z,.resetendoftable
  cp 8
  ret z ;everything is gone
  
;highest active object found  
  push ix
  pop hl
  ld de,offsets+20
  add hl,de
  add a,a ;*2
  ld e,a
  ld d,0
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl)
  push de
  pop iy
  
  ld (iy+v3),4 ;make new head

  ret

cactuswobbletable:  db 0,0,1,1,2,2,3,3,4,4,3,3,2,2,1,1,1,1,2,2,3,3,4,4,3,3,2,2,1,1,0,0,2,2,3,3,4,4,3,3,2,2,1,1,0,0,1,1,3,3,4,4,3,3,2,2,1,1,0,0,1,1,2,2,4,4,3,3,2,2,1,1,0,0,1,1,2,2,3,3


;only allow MINIMUM amount of code. each T-state within 135 is multiplied by 10!
MovementPattern135:     ;cactuscat bodypart

  ld a,r
  ld b,a
  ld a,(framecounter) ;save some clock
  add a,b
  and 3
  call mariointeract ;make deadly as fuck
  
  ld a,(ix+v4)
  dec a
  jp z,.fall
  
  ld a,(ix+v3) ;looks like it is the head!
  cp 4
  jp z,.makehead

  ld a,(ix+v7)
  and a
  jp nz,.poppart
  
  ret


.resettable:

  ld (ix+objectrelease),0
  ret
 
 
.fall:


  call addv2toyobject


  ret
 


.poppart:

  ld a,(ix+v7)
  cp 20
  jp nc,destroyobjectwithcloudnostar
  
  inc (ix+v7)

  call checkobjecttilesfloor
  call addv2toyobject
  ld (ix+v2),0

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld c,(ix+v1) ;our real ID
  ld de,offsets+5
  add hl,de
  ld a,(hl)
  cp c
  jp z,.removefromtable
  inc hl ;shift one word
  inc hl
  ld a,(hl)
  cp c
  jp z,.removefromtable
  inc hl ;shift one word
  inc hl
  ld a,(hl)
  cp c
  jp z,.removefromtable
  inc hl ;shift one word
  inc hl
  ld a,(hl)
  cp c
  jp z,.removefromtable
  inc hl ;shift one word
  inc hl
  ld a,(hl)
  cp c
  jp z,.removefromtable


  ret

.removefromtable:

  ld a,5
  ld (hl),a
  
  ret



.makehead:

  ld (ix+v3),0

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex278  ;color address ;02
  ld    bc,spriteaddrex278c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock2
  jp    setcharcolandoffset




;  ret
 


MovementPattern133: ;sheertileplatform

  ld a,(ix+v4)
  and a
  jp z,.sety


;  xor a
;  ld (specialsheertile),a
  ld c,(ix+objectrelease) ;1
  call platforminteract.objectonly ;check for objects only

  call mariointeract ;TODO: do not lock too soon!
  ret nc


  ld a,specialsheertilesblock
  call block12
  
;first obtain the xcoordinate of mario compared to the object he is standing on  
  ld hl,sheerselecttable
  ld a,(ix+objectrelease)  
  cp 7 ;tell the camera to follow target
  call nc,.setmax
  add a,a ;*2
  ld e,a
  ld d,0
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl
  
  push hl
  
  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)

  xor a
  sbc hl,de
  ld a,h ;check for minus
  and a
  call nz,.setzero
  
  ld d,0
  ld e,l ;maximum number is 64
  
  pop hl
  
  ;ld hl,sheertable
  add hl,de
  ld a,(hl) ;fetch the value from table
  
  ld e,a
  ld d,0
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld bc,24
  xor a
  sbc hl,bc
;  ld hl,(marioy) ;YOU CANNOT DO THIS!!! The lockvalue will constantly change at each mario position you must use the sprite value!!
  add hl,de ;add table value to absolute lock

    call setpage12tohandleobjectmovement ;get generalcode back
  
  ld de,8 ;substract base number as compensation
  xor a
  sbc hl,de

  ld a,(ix+objectrelease)
  
  cp 7
  jp nc,.lockspecial

  ;lockvalue is now in hl
  ld de,(marioy)
  xor a
  sbc hl,de
  
  ld de,10 ;less tight lock.
  xor a
  sbc hl,de
  ret nc ;if distance to lock is too high return and try again next frame

.lockdirect:
  
;value combination set standingonsprite for platform interaction
;set specialsheertile to make it special
  ld a,1
  ld (standingonsprite),a
  ld a,(ix+objectrelease) ;type of sheertile
  ld (specialsheertile),a

  ld (spriteixvalue),ix


;  ld b,8
;  
;  in	a,($AA)
;	and	$F0		;only change bits 0-3
;	or	b		;take row number from B
;	out	($AA),a
;	in	a,($A9)		;read row into A
;  xor %11111111

  call readkeydirect

  bit 7,a
  jp nz,.doreset
  bit 4,a
  jp nz,.doreset

  ld a,1
  ld (mariosheerhold),a

  ret

.lockspecial:

;jp .lockdirect

  ;lockvalue is now in hl
  ld de,(marioy)
  xor a
  sbc hl,de
  jp c,.lockdirect


  ld de,24
  xor a
  sbc hl,de
  ret nc ;if distance to lock is too high return and try again next frame



  jp .lockdirect


.doreset:

  xor a
   ld (mariosheerhold),a

  ret


.sety:


  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,7
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld (ix+v4),1
  
  ret

.setzero:

  xor a
  ld l,a
  ret



.setmax:

  push af
  ld a,1
  ld (maxtile),a
  pop af
  ret
  
  


MovementPattern136:     ;multicoinbox


  ld a,(ix+v4)
  cp 3
  jp c,.wait

  xor a
  ld (multiblockactive),a

  ld a,(ix+v7)
  and a
  jp z,.notimer
  dec (ix+v7)
  jp z,.destroyobject

.notimer:

;in case the box gets hit by a shell
  call objectinteract
  cp 3
  jp z,handlemultiboxinteract


.wait:

  ld a,(ix+v4)
  cp 15
  jp nc,.noinc

  inc (ix+v4)

.noinc:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.position0
  cp    4
  jr    z,.position1
  cp    8
  jr    z,.position2
  cp    12
  jr    z,.position3

  ret

.destroyobject:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  inc hl
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  call createbrownblock


  jp destroyobject


.animate:
 
  
.position0:
  ld    hl,spriteaddrex68  ;color address
  ld    bc,spriteaddrex68c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex69  ;color address
  ld    bc,spriteaddrex69c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex70  ;color address ;02
  ld    bc,spriteaddrex70c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex71  ;color address ;03
  ld    bc,spriteaddrex71c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset


  ;ret

handlemultiboxinteract:

;  ld a,(ix+hitpoints)
;  and a
;  jp z,.nomoremultiblock

  ld a,(ix+v7)
  and a
  jp nz,.notimerset

  ld (ix+v7),120 ;start timer

.notimerset:

  ld a,1
  ld (multiblockactive),a


.nomoremultiblock:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)


  ld (ix+movepat),137
  ld    hl,spriteaddrex78  ;color address
  ld    bc,spriteaddrex78c  ;character address 
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  call    setcharcolandoffset 

 
  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0


  call Playsfx.coin
  
  ret 



MovementPattern137: ;coin release multibox

  
  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    2                       ;4 frames moving upwards nu 3 looks better
  ret   nz



.spawnpowerup:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
;  inc e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),71

  pop ix

 ; ret



;wait until the powerup has fully risen from the block then destroy this object
.waitforpowerup:

;  ld a,(ix+hitpoints)
;  and a
;  jp z,destroyobject
;  dec a
;  ld (ix+hitpoints),a

  ld (ix+v1),0
  ld (ix+v2),4
  ld (ix+v3),0
  ld (ix+v4),0

  ld (ix+movepat),136
  
  
  ret


endmov:  
  
