;yep its about to happen ;S
;next block of objectmovements

HandleObjectMovement8:

;start = 269


;  ld a,(pauze)
;  dec a
;  ret z

;mario sterft alles stop zetten
;  ld a,(mariotransforming)
;  and a
;  ret nz


  ld    e,(ix+movepat)          ;get the movement pattern
  ld    d,(ix+movepat+1)
  ld    hl,movementpatternaddress8 - 269 * 3
  add   hl,de
  add   hl,de
  add   hl,de 
 
  jp (hl)


;;superfast jump to any condition. jp is a 3byte opcode so do the adress jumps x3
;  ld    l,(ix+movepat)          ;get the movement pattern
;  ld    h,(ix+movepat+1)
;  ld e,l
;  ld d,h ;-----> copy hl to de
;  add hl,hl
;  add hl,de
;  ex de,hl
;  ld    hl,movementpatternaddress8
;  add   hl,de
;  ;substract minimum base adres
;  ld de,269*3
;  xor a
;  sbc hl,de
;  jp    (hl)

  ret 
  

movementpatternaddress8:

  jp    MovementPattern269     ;magikoopa
  jp    MovementPattern270     ;magiball
  jp    MovementPattern271     ;lemmy block mover
  jp    MovementPattern272     ;single block sprite
  jp    MovementPattern273     ;red mushroom (static)
  jp    MovementPattern274     ;waterplatform (castle type)
  jp    MovementPattern275     ;moving flame (bouncing)
  jp    MovementPattern276     ;lemmy (boss)
  jp    MovementPattern277     ;special platform (maplevel4-01a)
  jp    MovementPattern278     ;special platform (maplevel4-01a) (right part)
  jp    MovementPattern279     ;special maplevel04-01a animator
  jp    MovementPattern280     ;square flattened platform (dropping)
  jp    MovementPattern281     ;smiley box (multicoin box)
  jp    MovementPattern282     ;square flattened platform (horizontal one way)
  jp    MovementPattern283     ;ridable cloud
  jp    MovementPattern284     ;square flattened platform (pathfollower)
  jp    MovementPattern285     ;yoshi wings
  jp    MovementPattern286     ;maplevel04-03b helper object
  jp    MovementPattern287     ;sliding guy (special) (maplevel04-04a)
  jp    MovementPattern288     ;sumokoopa
  jp    MovementPattern289     ;shard
  jp    MovementPattern290     ;longflame
  jp    MovementPattern291     ;piranjabullet
  jp    MovementPattern292     ;torpedoted
  jp    MovementPattern293     ;torpedoted (spawner)
  jp    MovementPattern294     ;empty (reserved position)
;  jp    MovementPattern295     ;maplevel04-06b (helper object)



.return:
  
  ret


MovementPattern294:     ;empty (reserved position)


  ret


MovementPattern293:     ;torpedoted (spawner)

;do not handle hand
  ld a,(ix+objectrelease)
  and a
  ret nz

  ld a,(ix+v4)
  dec a ;1
  jp z,.spawnhandandted
  dec a
  jp z,.initiate
  dec a
  jp z,.godown
  dec a 
  jp z,.wait
  dec a
  jp z,.release
  dec a
  jp z,.goup

;secure some maximum distance to keep the sprites less in screen
  call getmariodistance
  ld a,c
  cp 130
  ret nc

  inc (ix+v7)
  ld a,(ix+v7)
  cp 40
  ret c

  jp .incv4

;  ret

.initiate:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret c

  push ix

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+offsets+20)
  ld d,(ix+offsets+21)

  push de
  
  push hl
  pop ix
  
  ld    hl,coloraddresstorpedotedhandl1  ;color address ;03  
  ld    bc,characteraddresstorpedotedhandl1  ;character address
  
  ld a,(ix+hitpoints)
  and a
  call nz,.setright2
  
  ld    de,offsetkoopa
  ld    a,ingamespritesblock5
  call  setcharcolandoffset   

  pop ix

  ld    hl,coloraddresstorpedotedl3  ;color address ;03
  ld    bc,characteraddresstorpedotedl3  ;character address
  
  ld a,(ix+hitpoints)
  and a
  call nz,.setright3
  
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  call  setcharcolandoffset  
  
  
  pop ix
  
  jp .incv4
  
;ret

.setright2:

  ld    hl,coloraddresstorpedotedhandr1  ;color address ;03  
  ld    bc,characteraddresstorpedotedhandr1  ;character address
    ret

.setright3:

  ld    hl,coloraddresstorpedotedr3  ;color address ;03
  ld    bc,characteraddresstorpedotedr3  ;character address
    ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),0
  ret

.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  
  jp .incv4

  ;ret

.goup:

  push ix
  
  ld l,(ix+v5)
  ld h,(ix+v5+1)

  push hl
  pop ix ;handle hand
  
  ld (ix+v2),-1
  call addv2toybject

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  call hidemariospritedown

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17
  call z,destroyobject ;clear hand sprites out of the way and make room

  pop ix

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17
  ret nz

;reset object.  
  ld (ix+v4),0
  ld (ix+v7),0

  ret


.release:

  push ix

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+offsets+20)
  ld d,(ix+offsets+21)

  push de
  
  push hl
  pop ix

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresstorpedotedhandl2  ;color address ;03
  ld    bc,characteraddresstorpedotedhandl2  ;character address
  
  ld a,(ix+hitpoints)
  and a
  call nz,.setright4  
  
  
  ld    de,offsetkoopa
  ld    a,ingamespritesblock5
  call  setcharcolandoffset  

  pop ix


  ld (ix+nosprupdate),0  
  ld (ix+v7),0
  inc (ix+v4)
  
  pop ix
  
  jp .incv4

  ;ret

.setright4:

  ld    hl,coloraddresstorpedotedhandr2  ;color address ;03
  ld    bc,characteraddresstorpedotedhandr2  ;character address

    ret


;hiero
.godown:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 17+8
  jp z,.incv4


  push ix

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+offsets+20)
  ld d,(ix+offsets+21)
  
  push de
  
  push hl
  pop ix ;handle hand

  inc (ix+v6)
  ld a,(ix+v6)
  cp 19
  call z,.updatespritehand
  
  ld (ix+v2),1
  call addv2toybject
  
  pop ix ;handle torpedo ted

  ld (ix+v2),1
  call addv2toybject  

  inc (ix+v6)
  ld a,(ix+v6)
  cp 16
  jp z,.updatesprite
  cp 17
  jp nc,.skip

;selfexplanatory
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld b,(ix+v6)
  ld a,16
  sub b

  ld bc,0*32
  call unhideobjectspriteupdown

;selfexplanatory
  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 

  push ix
  pop iy

  ld b,(ix+v6)
  ld a,16
  sub b

  ld bc,2*32
  call unhideobjectspriteupdown

.skip:
 
  pop ix

  ret

.updatesprite:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  pop ix
  
  ret

.updatespritehand:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ret


.spawnhandandted:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
 ; inc de

  push hl
  push de
;spawn hand
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,289 ;something with 2 sprites
  ld    (iy+5),l               ;set object number (56 = coin)
  ld    (iy+6),h               ;set object number (56 = coin)
  
  call activateobject

  ld (ix+nosprupdate),2
  ld (ix+objectrelease),1
  ld hl,293
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresstorpedotedhandl1  ;color address ;03
  
    ld (ix+hitpoints),0
    push hl
;    push de
    call getmarioleftright
;    pop de
    pop hl
    call c,.setright
  
;  ld    bc,characteraddresstorpedotedhandl1  ;character address
  ld    bc,emptysprite94  ;character address
  ld    de,offsetkoopa
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  push ix
  pop hl

  pop ix

  ld (ix+v5),l
  ld (ix+v5+1),h
  
  pop de
  pop hl

  inc de
  dec hl
  

;spawn ted
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,284 ;something with 4 sprites
  ld    (iy+5),l               ;set object number (56 = coin)
  ld    (iy+6),h               ;set object number (56 = coin)
  
  call activateobject

  ld (ix+nosprupdate),2
  ld (ix+deadly),2
  ld (ix+sizeY),24
  ld hl,292
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresstorpedotedl3  ;color address ;03
  
    ld (ix+hitpoints),0
    push hl
;    push de
    call getmarioleftright
;    pop de
    pop hl
    call c,.setrightted  
  
;  ld    bc,characteraddresstorpedotedl3  ;character address
  ld    bc,emptysprite94  ;character address
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  push ix
  pop hl

  pop ix  
  
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  jp .incv4
  
;  ret

.setright:

  ld    hl,coloraddresstorpedotedhandr1  ;color address ;03
  inc (ix+hitpoints)
  ret

.setrightted:

  ld    hl,coloraddresstorpedotedr3  ;color address ;03
  inc (ix+hitpoints)
  ret


;spawn, make hand and torpedo. Destroy hand. Need spritecoverup code for this
MovementPattern292:     ;torpedoted

  call mariointeract
  
  ld a,(ix+v5)
  and a
  call nz,.animate


  ld a,(ix+v4)
  dec a
  jp z,.release
  dec a
  jp z,.accelerate
  dec a
  jp z,.move
  
  ret

.move:

  jp addv1toxbject


.accelerate:
  
  ld hl,.speedtable
  ld a,(ix+hitpoints)
  and a
  call nz,.setright
  
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.incv4
  
  ld (ix+v1),a

  inc (ix+v7)

  jp addv1toxbject
  
 ; ret

.speedtable: db 0,-1,0,-1,0,-1,0,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-2,-1,-2,-2,-2,-2,-2,-2,-2,-3,-3,-3,-3,-3,-4,5
.speedtable2: db 0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,2,1,2,1,2,2,2,2,2,2,2,3,3,3,3,3,4,5


.setright:

  ld hl,.speedtable2

  ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),0

  ret

.release:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 13
  jp nc,.incv4

  ;drop a few pixels and change the sprite
  ld (ix+v2),1
  ld (ix+v5),1 ;setanimate
  
  call addv2toybject

  ret


.animate:

  ld a,(framecounter)
  and 3
  ret nz

  ;add smoke sprites behind
  call .spawnsmoke

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  

  ld a,(ix+hitpoints)
  and a
  jp nz,.right


  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a

  jp z,.position1

  ld    hl,coloraddresstorpedotedl1  ;color address
  ld    bc,characteraddresstorpedotedl1  ;character address
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret

.position1:

  ld    hl,coloraddresstorpedotedl2  ;color address
  ld    bc,characteraddresstorpedotedl2  ;character address
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret


.right:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a

  jp z,.position2

  ld    hl,coloraddresstorpedotedr1  ;color address
  ld    bc,characteraddresstorpedotedr1  ;character address
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret

.position2:

  ld    hl,coloraddresstorpedotedr2  ;color address
  ld    bc,characteraddresstorpedotedr2  ;character address
  ld    de,offsetstuberight3
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret

  

.spawnsmoke:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld a,(ix+hitpoints)
  and a
  jp nz,.skip  
  inc hl
  inc hl
  inc hl
  
.skip:
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,280
  ld    (iy+5),l               ;set object number (56 = coin)
  ld    (iy+6),h               ;set object number (56 = coin)
  
  call activateobject


  ld (ix+deadly),3
  ld hl,22
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v3),1
   
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) 
  ld de,4
  xor a
  sbc hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  pop ix

  
  ret



piranjabullettable: db -2,-2,-2,-2,-2,-1,-1,-1,0,1,1,1,2,2,2,2,2,3,3,3,3,3,4,5

MovementPattern291:     ;piranjabullet

  ld a,(framecounter)
  and 3
  call z,.animate

  ld hl,piranjabullettable
  ld e,(ix+v7)
  ld d,0
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.setmax

  inc (ix+v7)

.setmax:

  ld (ix+v2),a
  
  call addv1toxbject
  call addv2toybject


  ret


.animate:
 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    a,(ix+v3)               ;v3 is used as animation variable
  inc a
  ld    (ix+v3),a               ;v3 is used as animation variable
  jp    z,.position0
  dec   a
  jp    z,.position1
  dec   a
  jp    z,.position2
  dec   a
  jp    z,.position3
  dec   a
  jp    z,.position4
  dec   a
  jp    z,.position5
  dec   a
  jp    z,.position6
  dec   a
  jp    z,.position7


  ld (ix+v3),0

.position0:
  ld    hl,spriteaddrex42  ;color address
  ld    bc,spriteaddrex42c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position1:
  ld    hl,spriteaddrex46  ;color address
  ld    bc,spriteaddrex46c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position2:
  ld    hl,spriteaddrex43  ;color address ;02
  ld    bc,spriteaddrex43c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position3:
  ld    hl,spriteaddrex47  ;color address ;03
  ld    bc,spriteaddrex47c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position4:
  ld    hl,spriteaddrex44  ;color address
  ld    bc,spriteaddrex44c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position5:
  ld    hl,spriteaddrex48  ;color address
  ld    bc,spriteaddrex48c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position6:
  ld    hl,spriteaddrex45  ;color address ;02
  ld    bc,spriteaddrex45c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset

.position7:
  ld    hl,spriteaddrex49  ;color address ;03
  ld    bc,spriteaddrex49c  ;character address
  ld    de,offsetbullet
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset





MovementPattern290:     ;longflame
  
  call mariointeract ;keep it deadly

  ld a,(framecounter)
  and 3
  call z,.animate
 
  ld a,(ix+v4)
  dec a ;1
  jp z,.spawnflames
  dec a ;2
  jp z,.wait
  dec a ;3
  jp z,.goup
  dec a ;4
  jp z,.wait  
  dec a ;5
  jp z,.goup2
  dec a ;6
  jp z,.wait  
  dec a ;7
  jp z,.goup3
  dec a ;8
  jp z,.wait  
  dec a ;9
  jp z,.flame5
  dec a ;10
  jp z,.wait
  dec a ;11
  jp z,.godown
  dec a ;12
  jp z,.wait
  dec a ;13
  jp z,.godown2
  dec a
  jp z,.wait
  dec a
  jp z,destroyobject
  
  
  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret nz

.incv4:
  
  inc (ix+v4)
  ld (ix+v7),0
 

  ret

.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  ret c
  
  jp .incv4


.godown2:

  call addv2toybject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  jp .incv4
  


.godown:

  ld (ix+v2),8
  call addv2toybject
  
  jp .flame3  


.goup3:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame7  ;color address ;03
  ld    bc,characteraddresslongflame7  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  jp .incv4

;  ret

.goup2:

;  ld (ix+v2),-8
  call addv2toybject

.flame5:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame5  ;color address ;03
  ld    bc,characteraddresslongflame5  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  jp .incv4

;  ret

.goup:

  ld (ix+v2),-8
  call addv2toybject

.flame3: 
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame3  ;color address ;03
  ld    bc,characteraddresslongflame3  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  jp .incv4

;  ret


.spawntwoflames:

  call .spawnleft
  jp .spawnright


.spawnflames:

  call .incv4

  ld a,(ix+v5)
  and a
  ret nz

  ld a,(ix+hitpoints)
  and a
  ret z

  dec (ix+hitpoints)

  ld a,(ix+v6)
  and a
  jp nz,.spawntwoflames

  ld a,(ix+objectrelease)
  and a
  jp nz,.spawnright


.spawnleft:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  dec hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  ld a,(ix+hitpoints)
  
  push af
  call activateobject
  pop af


  ld (ix+hitpoints),a
  ld (ix+deadly),2
  ld hl,290
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v6),0 
  ld (ix+objectrelease),0 
 ; ld (ix+v4),1
  
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  pop ix

  inc (ix+v5)
  
  ret

.spawnright:

  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de


;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  ld a,(ix+hitpoints)
  
  push af
  call activateobject
  pop af


  ld (ix+hitpoints),a
  ld (ix+deadly),2
  ld hl,290
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+v6),0   
  ld (ix+objectrelease),1
;  ld (ix+v4),1   
   
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  pop ix
  
  
  inc (ix+v5)
  
  ret




;animate using two values. The ix+v4 telling us where we are and v3 as usual.
.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;determine which height the flame has
  ld a,(ix+v4)
;  cp 15
;  jp nc,.step11
  cp 13
  jp nc,.step11
  cp 11
  jp nc,.step21
  cp 9
  jp nc,.step31
  cp 7
  jp nc,.step41  
  cp 5
  jp nc,.step31  
  cp 3
  jp nc,.step21




.step11:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
    jp z,.step12
  

  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset  
  
  
  
.step12:


  ld    hl,coloraddresslongflame2  ;color address ;03
  ld    bc,characteraddresslongflame2  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset 



;  ret


.step21:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
    jp z,.step22
  

  
  ld    hl,coloraddresslongflame3  ;color address ;03
  ld    bc,characteraddresslongflame3  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset  
  
  
  
.step22:


  ld    hl,coloraddresslongflame4  ;color address ;03
  ld    bc,characteraddresslongflame4  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset 



;  ret


.step31:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
    jp z,.step32
  

  
  ld    hl,coloraddresslongflame5  ;color address ;03
  ld    bc,characteraddresslongflame5  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset  
  
  
  
.step32:


  ld    hl,coloraddresslongflame6  ;color address ;03
  ld    bc,characteraddresslongflame6  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset 



;  ret


.step41:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
    jp z,.step42
  

  
  ld    hl,coloraddresslongflame7  ;color address ;03
  ld    bc,characteraddresslongflame7  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset  
  
  
  
.step42:


  ld    hl,coloraddresslongflame8  ;color address ;03
  ld    bc,characteraddresslongflame8  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset 



;  ret


MovementPattern289:     ;shard

  ld a,(ix+v4)
  and a
  jp nz,.killself


;make deadly
  call mariointeract
  
  call addv2toybject

;check floor   
  ld bc,20
  ld de,0
  call objecttilecheckfromrom
  cp 191
  ret c 

  call Playsfx.yoshifireball

  inc (ix+v4)

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddressshard2  ;color address ;03
  ld    bc,characteraddressshard2  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset



.killself:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret c

;turn into flame and parse objectspawn into the movementpattern

  ld hl,290
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+hitpoints),2 ;spawn 2 extra objects at each interval
  ld (ix+v6),1 ;this one spawns two shards

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddresslongflame1  ;color address ;03
  ld    bc,characteraddresslongflame1  ;character address
  ld    de,offsetwater
  ld    a,ingamespritesblock5
  jp  setcharcolandoffset

;  ret


MovementPattern288:     ;sumokoopa

  call mariointeract

  call objectinteract
  cp 2
  jp z,otherobjectdeathjump
  cp 3
  jp z,otherobjectdeathjump


  ld a,(ix+v4)
  dec a ;1
  jp z,.move
  dec a ;2
  jp z,.wait
  dec a ;3
  jp z,.stomp
  dec a ;4
  jp z,.releaseshard
  dec a ;5
  jp z,.wait2



;inititate
  ld (ix+v3),0
  ld (ix+v7),0
  ld (ix+v4),1
  
  call getmarioleftright
  jp c,.setright


  ld (ix+v1),-1
  ret

.setright:

  ld (ix+v3),1
  ld (ix+v1),1
  ret

.wait2:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 70
  ret c
  
  jp .incv4


;  ret


.wait:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 15
  ret c
  
  jp .incv4


;  ret


.releaseshard:



  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  inc de
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call activateobject

  ld (ix+deadly),2
  ld hl,289
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  
  ld (ix+v2),4
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld    hl,coloraddressshard  ;color address ;03
  ld    bc,characteraddressshard  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  call  setcharcolandoffset

  pop ix

  jp .incv4

;ret

.stomp:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  jp z,.step1
  cp 10
  jp z,.step2
  cp 15
  jp z,.step3  
  cp 20
  ret c


  ld a,1
  ld (tripscreen),a
  
  call Playsfx.explosion
  
  jp .incv4


  ;ret


.step1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  ld a,(ix+v3)
  and a
  jp nz,.step1r
  
  ld    hl,coloraddresssumokoopaleft3  ;color address ;03
  ld    bc,characteraddresssumokoopaleft3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.step2:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp nz,.step2r
 
  ld    hl,coloraddresssumokoopaleft4  ;color address ;03
  ld    bc,characteraddresssumokoopaleft4  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.step3:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp nz,.step3r
  
  ld    hl,coloraddresssumokoopaleft5  ;color address ;03
  ld    bc,characteraddresssumokoopaleft5  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


.step1r:
  
  ld    hl,coloraddresssumokooparight3  ;color address ;03
  ld    bc,characteraddresssumokooparight3  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.step2r:
  
  ld    hl,coloraddresssumokooparight4  ;color address ;03
  ld    bc,characteraddresssumokooparight4  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.step3r:
  
  ld    hl,coloraddresssumokooparight5  ;color address ;03
  ld    bc,characteraddresssumokooparight5  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset



.move:

  ld a,(framecounter)
  and 7
  jp z,.domove
  cp 4
  ret nz

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  and a
  jp nz,.mover

  ld    hl,coloraddresssumokoopaleft2  ;color address ;03
  ld    bc,characteraddresssumokoopaleft2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  ;ret

.mover:

  ld    hl,coloraddresssumokooparight2  ;color address ;03
  ld    bc,characteraddresssumokooparight2  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  ;ret


.domove:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v7)
  
  ld a,(ix+v7)
  cp 3
  jp z,.incv4
  
  call addv1toxbject ;move


  ld a,(ix+v3)
  and a
  jp nz,.domover

  ld    hl,coloraddresssumokoopaleft1  ;color address ;03
  ld    bc,characteraddresssumokoopaleft1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  ;ret

.domover:

  ld    hl,coloraddresssumokooparight1  ;color address ;03
  ld    bc,characteraddresssumokooparight1  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  ;ret
  
.incv4:

  ld (ix+v7),0
  inc (ix+v4)

  ret



MovementPattern287:    ;sliding guy (special) (maplevel04-04a)
;v4 word gebruikt om zijn status bij te houden

  call objectinteract ;kans lijkt me klein dat dit nodig is
  cp 2
  jp z,otherobjectdeathjump
  cp 5
  jp z,destroyobjectwithcloudnostar


  call checkmariointeractshoot
  jp c,otherobjectshotdeathjump


  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success
  jp c,.nextframe


  call  checksheerslidingguy
  jp c,slidingguyofframpspecial

  call .makespeed

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ret


.nextframe:
 
  ld a,(marioflyslide)
  dec a
  jp z,.setkilljump  
 
  ld a,(movedownsheertile)
  dec a
  call z,setrampjump  
  
  ld a,(mariospinjump)
  dec a
  jp z,destroyobjectwithcloud
  
    ld a,(marioyoshi)
    dec a
    jp z,destroyobjectwithcloud
 
  ld (ix+movepat),27  ;generic set
  ld (ix+movepat+1),0  ;generic set
  
  call Playsfx.kickenemy
 
 ;spat weghalen
  call removeobjectspat

;framecounter resetten voor constante tussenstukjes
  xor a
  ld (ix+deathtimer),a
 
  
;  call MovementPattern27.animate ;animeer
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex41  ;color address ;03
  ld    bc,spriteaddrex41c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset



;ret
.setkilljump:

  xor a
  ld (ix+deathtimer),a
  ld (ix+v1),a

  ld (ix+clipping),0
  ld (ix+movepat),6
  ld (ix+movepat+1),0
  
  ret

.makespeed:


;  sub b
  ld d,0
  ld e,(ix+v4)
  ld hl,slidingguytablespecial
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.endmakespeed
  
  neg
  ld (ix+v1),a
  neg
  ld (ix+v2),a


  ld a,(ix+v4)
  inc a
  ld (ix+v4),a

.endmakespeed:


  ret

slidingguytablespecial:   db 0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,2,1,2,1,2,2,2,2,2,2,3,3,3,4,3,4,4,3,4,4,5
slidingguytablefinalspecial:   db 4,4,4,4,3,4,3,4,3,4,3,4,3,3,3,3,3,3,3,2,3,2,3,2,3,2,2,2,2,2,1,2,1,2,1,2,1,1,1,1,0

slidingguyofframpspecial:

  ld a,(ix+objectrelease)
  and a
  jp nz,.jumpup


  ld (ix+v2),8


    ld a,(ix+v1)
    inc a
    cp -3
    jp z,.nospeed
    ld (ix+v1),a

.nospeed:    

;TODO: make the jump from here for the little fucker....
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  cp 250
  jp z,.setjumpup


  call  checkobjecttilesfloor
  jp nc,slidingguyfinalslidespecial

  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  ld (ix+v4),0

  ret

.setjumpup:

  inc (ix+objectrelease)

  ld (ix+v1),-4
  ld (ix+v2),-4
  

  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)


  ret

.jumpup:

  ld (ix+v1),-4
  ld e,(ix+v7)
  ld d,0
  ld hl,slidingguyjumptable
  add hl,de
  ld a,(hl)
  and a
  jp z,.max
  
  ld (ix+v2),a
  
  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)
  call  addv2toybject           ;y movement of object, this adds the value in v2 to yobject (16 bits)

  inc (ix+v7)

   ret

.max:

  ld (ix+objectrelease),0


  ret


;xy table
slidingguyjumptable: db -4,-4,-4,-4,-4,-4,-4,-4,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,3,4,0



slidingguyfinalslidespecial:

;  call mariointeract ;kijk of mario wat heeft uitgespookt. ret C=hitpoint op object success

  ;  sub b
  ld d,0
  ld e,(ix+v4)
  ld hl,slidingguytablefinalspecial
  add hl,de
  ld a,(hl)
  cp 0
  jp z,.endslide
  
  neg
  ld (ix+v1),a


  ld a,(ix+v4)
  inc a
  ld (ix+v4),a
  
  


  call  addv1toxbject           ;x movement of object, this adds the value in v1 to xobject (16 bits)

  ret

.endslide:

  ld (ix+movepat),26
  ld (ix+movepat+1),0  
  ld (ix+v4),0 ;v4 resetten gaan we als timertje gebruiken bij het opstaan
  ld (ix+v1),1

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,spriteaddrex37  ;color address
  ld    bc,spriteaddrex37c  ;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock1
  jp    setcharcolandoffset




MovementPattern286:     ;maplevel04-03b helper object

  ld e,(ix+v7)
  ld d,0
  ld hl,flyingyoshimoveuptable
  add hl,de
  ld a,(hl)
  and a
  jp z,.destroyobject
  ld hl,(marioy)
  ld e,a
  xor a
  ld d,0
  sbc hl,de
  ld (marioy),hl
  
  inc (ix+v7)
  
  ld hl,(mariox)
  inc hl
  ld (mariox),hl
  
  ld a,1
  ld (mariospeed),a
  
  ret

.destroyobject:

  xor a
  ld (nocontrol),a
  

  jp destroyobject



flyingyoshimoveuptable: db 8,8,8,8,8,8,8,8,8,8,8,8,8,8,7,7,7,7,7,7,7,7,6,6,6,4,4,4,4,3,3,3,3,3,3,2,2,2,2,2,2,2,2,1,1,0


MovementPattern285:     ;yoshi wings

  ld a,(ix+v5)
  and a
  call nz,.pullmario


  ld a,(ix+v4)
  add a,a
  ld e,a
  ld d,0
  ld hl,yoshiwingflytable
  add hl,de
  ld a,(hl)
  cp 5
  jp z,.max
  ld (ix+v1),a
  inc hl
  ld a,(hl)
  ld (ix+v2),a

  ld a,(framecounter) ;save some bytes work the table slower
  and 3
  jp nz,.max
  inc (ix+v4)

.max:

  call addv1toxbject
  call addv2toybject

  call mariointeract
  call c,.catchwing


  ld a,(framecounter)
  and 3
  call z,.animate

  ld a,(framecounter)
  and 3
  call z,.spawnsparkle


  ret

.catchwing:

  ld a,(ix+v5)
  and a
  ret nz

  ld a,(marioyoshi) ;bullshit you are not riding yoshi
  and a
  ret z

  ld a,(ix+v4)
  cp 3
  ret c

  ld a,5
  ld (nocontrol),a
  
  ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a
  ld (staticcamy),a 
  
  call Playsfx.secretdoor
  
  ld (ix+v5),1 ;tell object it is captured

;  ld hl,(cameray) ;store camera, we keep this value until mario is out of screen
;  ld (ix+offsets+25),l
;  ld (ix+offsets+26),h

  ret

.pullmario:

  ld a,(mariocoordinates) ;hide the mario sprite as soon as we exit the screen
  cp 8
  call c,hidemariospat
  
;  ld l,(ix+offsets+25)
;  ld h,(ix+offsets+26)
;  ld (cameray),hl ;keep camera at last current

  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld bc,8
  xor a
  sbc hl,bc
  ld (mariox),de ;center mario around the wings
  ld (marioy),hl


;compare the camera to the ycoordinates of this object if out of screen end level
  ld de,(cameray)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1) 
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld bc,8
  add hl,bc

  xor a
  sbc hl,de
  jp c,.setendlevel
  
  ret

;no wait needed
.setendlevel:

  ld a,1 ;exit number
  ld (levelend?),a

  jp destroyobject
  


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  jp z,.step1


    ld hl,coloraddressflywings1
    ld bc,characteraddressflywings1
    ld    de,offsetstuberight3
    ld    a,ingamespritesblock5
    jp    setcharcolandoffset


;  ret


.step1:

    ld hl,coloraddressflywings2
    ld bc,characteraddressflywings2
    ld    de,offsetstuberight3
    ld    a,ingamespritesblock5
    jp    setcharcolandoffset

;  ret


.spawnsparkle:
;make a nice tail of magic


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call activateobject

  call destroyobjectwithsparkle

  pop ix

  ret



;x and y coordinates are in here
yoshiwingflytable:  db 0,-1,0,-1,0,-1,0,-1,1,-1,1,-1,1,-1,2,-2,2,-2,3,-2,3,-2,3,-2,2,-2,2,-2,2,-2,2,-2,1,-3,1,-3,1,-3,1,-3,5


MovementPattern284:     ;square flattened platform (pathfollower)

  ld c,0
  call platforminteract

  ld a,(ix+v4)
  and a
  jp nz,.move
  
  call mariointeract
  ret nc
  
  ld a,(framecounter)
  and 7
  ret nz


  inc (ix+v4)
  inc (ix+v7)

  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
  ld hl,50*8
  xor a
  sbc hl,de
  jp c,.settable2


;set correct pathtable
  ld hl,pathtable1
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h

  
  ret

.settable2:

  ld hl,70*8
  xor a
  sbc hl,de
  jp c,.settable3


;set correct pathtable
  ld hl,pathtable2
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h

  ret

.settable3:
;set correct pathtable
  ld hl,pathtable3
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h

  ret



.move:

  inc (ix+v5)
  ld a,(ix+v5)
  and 63
  call z,.spawnfucker

  call addv1toxbject
  call addv2toybject

  ld a,(framecounter)
  and 7
  ret nz ;per tile
  
  ld a,(ix+v3)
  and a
  call z,.right
  dec a ;1
  call z,.rightup
  dec a ;2
  call z,.up
  dec a ;3
  call z,.updown
  dec a ;4
  call z,.down
  dec a ;5
  call z,.switchspeed
  
  dec (ix+v7)

  ld a,(ix+v7)
  and a
  ret nz


  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
;  ld hl,pathtable1 ;make this generic later
  ld e,(ix+hitpoints) ;path pointer
  ld d,0
  add hl,de
  ld a,(hl)
  and a
  jp z,.stop
  ld (ix+v7),a ;tilecounter
  inc hl
  ld a,(hl)
  ld (ix+v3),a ;movement type

  inc (ix+hitpoints)
  inc (ix+hitpoints)

  ret

.stop:

  ;ld (ix+v4),0
  ld (ix+v7),10
  ld hl,280
  ld (ix+movepat),l
  ld (ix+movepat+1),h



  ret


.spawnfucker:

  call mariointeract
  ret nc
  

  ld a,r
  and 3
  ret z


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,18
  add hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  ex de,hl
  ld bc,5
  xor a
  sbc hl,bc
  ex de,hl
  

  push hl
  push de
;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),157               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  ld a,r
  and 1
  call  z,activateobject

  pop ix

  pop de
  pop hl
  dec de
  dec de
  
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),157               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  ld a,r
  and 1
  call  z,activateobject

  pop ix
  
  

  ret



.switchspeed:

  ld a,(ix+objectrelease)
  xor 1
  ld (ix+objectrelease),a
  ret



.down:

  ld a,(ix+objectrelease)
  and a
  jp nz,.downslow

  ld (ix+v1),0
  ld (ix+v2),2  
  ret

.updown:

  ld a,(ix+objectrelease)
  and a
  jp nz,.downslow

  ld (ix+v1),2
  ld (ix+v2),2  
  ret

.up:

  ld a,(ix+objectrelease)
  and a
  jp nz,.upslow

  ld (ix+v1),0
  ld (ix+v2),-2  
  ret

.right:

  ld a,(ix+objectrelease)
  and a
  jp nz,.rightslow

  ld (ix+v1),2
  ld (ix+v2),0  
  ret

.rightup:

  ld a,(ix+objectrelease)
  and a
  jp nz,.rightupslow

  ld (ix+v1),2
  ld (ix+v2),-2
  ret


.downslow:

  ld (ix+v1),0
  ld (ix+v1),1  
  ret

.updownslow:

  ld (ix+v1),1
  ld (ix+v1),1  
  ret

.upslow:

  ld (ix+v1),0
  ld (ix+v1),-1  
  ret

.rightslow:

  ld (ix+v1),1
  ld (ix+v1),0  
  ret

.rightupslow:

  ld (ix+v1),1
  ld (ix+v1),-1
  ret




;how the pathtable works: 
;first byte tells us how many tiles we move the platform
;second byte tells us which movement
;0 = right
;1 = right to up / up to right
;2 = upwards
;3 = upwards to down / down to right
;4 = downwards
;5 = switch speed to 1 pixel per frame

pathtable1: db 57/2,0,1,3,109/2,0,1,5,1,1,2,2,1,1,1,3,2,4,1,3,1,1,2,2,1,1,1,3,2,4,1,3,1,1,2,2,1,1,1,3,2,4,1,3,1,1,2,2,1,1,1,3,2,4,1,3,1,1,2,2,1,1,1,3,2,4,10/2,0,1,5,1,1,2,2,1,1,1,0,1,3,2,4,0,0
pathtable2: db 41/2,0,1,3,36/2,0,1,5,1,1,2,2,1,1,2,0,1,3,1,4,1,3,1,0,1,5,1,1,2,2,1,1,1,3,1,4,1,3,18/2,0,1,5,1,1,2,2,1,1,1,3,1,4,1,3,6/2,0,2,1,10/2,0,1,1,2,0,10/2,0,1,3,2,1,2,3,2,1,2,3,1,1,8/2,0,1,5,1,3,2,4,1,4,1,1,1,0,1,2,1,1,5,0,1,5,1,3,2,4,2,4,1,1,1,0,2,2,1,1,5,0,0,0
pathtable3: db 16/2,0,1,3,2/2,0,1,5,1,1,2,2,1,1,1,0,1,3,2,4,1,3,1,0,1,5,1,1,3,2,1,1,1,0,1,3,2,4,1,3,6/2,0,3,1,27/2,0,2,3,3/1,0,1,5,1,1,2,2,1,1,1,0,1,3,1,4,1,3,2,0,2,1,102/2,0,2,1,0,0




MovementPattern283:     ;ridable cloud

  ld a,(ix+hitpoints)
  and a
  jp z,.init

;handle movement in separate mode because the cloud will never stop as soon as you activate it
  ld a,(ix+v3)
  and a
  call nz,.move

  ld a,(ix+v4)
  and a
  jp nz,.ride

  ld a,(marioyoshi)
  and a
  ret nz ;not allowed when on yoshi


;wait a few frames before checking
  inc (ix+v7)
  ld a,(ix+v7)
  cp 5
  ret c


  call mariointeract
  jp c,.activate

  ld (ix+v7),15

  ret


.ride:

  ld a,1
  ld (standingonplatform),a ;prevent wierd camera exit bugs


;sync object to mario
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,6
  add hl,de
  ld (mariox),hl  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  ld de,16
  sbc hl,de
  ld (marioy),hl


  ld a,(Controls)
  bit 0,a
  jp nz,.up
  bit 1,a
  jp nz,.down
  bit 4,a
  jp nz,.release
  bit 6,a
  jp nz,.release


  ret

.release:

  xor a
  ld (ix+v4),0
  ld (standingonplatform),a

  ld (ix+v7),0

;we exit through a vertical tube!
  ld a,(nocontrol)
  cp 1
  ret z

  xor a
  ld (nocontrol),a

  ret 



.up:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld de,8*14
  xor a
  sbc hl,de
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  xor a
  sbc hl,de 
  ret nc

  ld (ix+v2),-1
  call addv2toybject
  ret

.down:  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  xor a
  sbc hl,de
  ret c ;boundary reached


  ld (ix+v2),1
  call addv2toybject
  ret


.move:


;stop properly and move platform

  ld e,(ix+xobject)
  ld d,(ix+xobject+1)
;  ld hl,115*8 ;add value here from preset table in later maps
  ld l,(ix+offsets+20)
  ld h,(ix+offsets+21)
  xor a
  sbc hl,de
  jp c,.setstop

  jp addv1toxbject


.setstop:

  ld a,(ix+offsets+28)
  inc (ix+offsets+28)
  cp 60
  jp nc,.stop
  cp 0
  call z,.sprite2
  cp 20
  call z,.sprite3  
  cp 40
  call z,.sprite4    
  

  jp addv1toxbject

.stop:

  ld (ix+v1),0
  ld (ix+v3),0
  
  call .release ;and release mario
  
    jp destroyobject ;dissapear
  
;  ret

.sprite2:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    ld hl,coloraddressridecloud2
    ld bc,characteraddressridecloud2
    ld    de,offsetplatform
    ld    a,ingamespritesblock5
    jp    setcharcolandoffset


.sprite3:


    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    ld hl,coloraddressridecloud3
    ld bc,characteraddressridecloud3
    ld    de,offsetplatform
    ld    a,ingamespritesblock5
    jp    setcharcolandoffset

.sprite4:

    ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
    ld hl,coloraddressridecloud4
    ld bc,characteraddressridecloud4
    ld    de,offsetplatform
    ld    a,ingamespritesblock5
    jp    setcharcolandoffset



;grab mario on interact and start movement
.activate:

  ld a,(fallspeed)
  and a
  ret z


;object now Controls mario
    ld a,5
    ld (nocontrol),a
  inc (ix+v3)
  inc (ix+v4)

;stop all previous actions
  xor a
  ld (mariospeed),a
  ld (marioslide),a

  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (rampjump),a
  ld (uppersheercornerfound),a  
  ld (marioduck),a
  ld (mariolook),a
  ld (mariospinjump),a


  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a  
  
  
  ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a


  ret


.init:

  ;save height for max up and down
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld (ix+v5),l
  ld (ix+v5+1),h
  
  inc (ix+hitpoints)
 
;ja wat!!?? ik moet toch iets?? 
  
  ld hl,115*8 ;maximum boundary for the right
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  
  ld a,(currentlevel) ;on this map we have a fixed value
  cp 76
  ret z

;otherwise we are on that other map
  ld hl,337*8 ;maximum boundary for the right
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,360*8
  xor a
  sbc hl,de
  ret c ;next object if no carry

  ld hl,381*8 ;maximum boundary for the right
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,400*8
  xor a
  sbc hl,de
  ret c ;next object if no carry

  ld hl,435*8 ;maximum boundary for the right
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h
  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,472*8
  xor a
  sbc hl,de
  ret c ;next object if no carry

  ld hl,576*8 ;maximum boundary for the right
  ld (ix+offsets+20),l
  ld (ix+offsets+21),h  
  
  ret



;start moving on touch and exit the screen on the right
MovementPattern282:     ;square flattened platform (horizontal one way)

  ld a,(ix+v4)
  and a
  call z,.checkmariointeract

;make elevator
  ld c,0
  call platforminteract

  ld a,(ix+v4)
  and a
  ret z

;move platform  

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  dec hl
  ld (ix+v5),l
  ld (ix+v5+1),h   
  ld a,l
  or h
  jp z,.stop
 
  
  call addv1toxbject
  

  inc (ix+v7)

  ret 


.stop:

  ld (ix+v1),0

  ld hl,1
  ld (ix+v5),l
  ld (ix+v5+1),h   
  
  ret


.checkmariointeract:

  call mariointeract
  ret nc
  
  inc (ix+v4)
  ld hl,440
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret


;cookie cut pattern 40 and 70 and adapt those. write different sprites and blocks
MovementPattern281:     ;smiley box (multicoin box)


  ld a,(ix+v4)
  and a
  jp nz,.waitforpowerup

  
  inc   (ix+v3)                 ;y object
  ld    a,(ix+v3)               ;y object
  cp    2                       ;4 frames moving upwards nu 3 looks better
  ret   nz

  ld (ix+v4),1

  call removefromsprlstskipfirst
  ld (ix+amountspr),1

.spawnpowerup:


  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  dec de
;  inc e

;create the powerup itself, push the ix byte or else the eventhandler will completely loose
;track of this current object and giant system crashes will become a reality
  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),56               ;set object number (56 = coin)
  ld    (iy+6),0               ;set object number (56 = coin)

  call  activateobject

  ld (ix+deathtimer),0
  ld (ix+clipping),0
  ld (ix+movepat),71

  pop ix

  ret



;wait until the powerup has fully risen from the block then destroy this object
.waitforpowerup:

  dec (ix+hitpoints)
  jp z,destroyobject

;fetch mapadress  
  ld bc,8
  ld de,0
  call objecttilecheckfromrom
  ;mapadress is in hl
  ld a,200
  call writeanytile
  inc a
  inc hl
  call writeanytile
  inc a
  ld de,(maplenght)
  dec hl
  add hl,de
  call writeanytile
  inc a
  inc hl
  call writeanytile
  
  ld (ix+movepat),49
  ld (ix+movepat+1),0

  ld (ix+v1),0
  ld (ix+v2),0
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v7),0

  ret




;special platform that drops like a brick when standing on it
MovementPattern280:     ;square flattened platform (dropping)


  ld c,0
  call platforminteract
  
  
  ld a,(ix+v4)
  and a
  jp nz,.drop
  

  call mariointeract
  ret nc
  
  ld a,(standingonsprite)
  and a
  ret z
  
  inc (ix+v4)

  ret
  
.drop:

  call addv2toybject

  ld e,(ix+v7)
  ld d,0
  ld hl,.droptable
  add hl,de
  ld a,(hl)
  cp 9
  ret z

  ld (ix+v2),a

  inc (ix+v7)

  ret

.droptable: db 0,1,0,1,0,1,0,1,1,1,1,2,1,2,1,2,1,2,2,2,2,3,2,3,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,8,9


;we animate the rotating blocks at the middle of the map by hand
;only when this object is on screen. This will save us precious outs
MovementPattern279:     ;special maplevel04-01a animator

  ld    a,animationtilesblock
  call  block12

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,animationtilescharacteraddr+(8*2)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*2)             ;rotating tile animation 1 character address in ro
  ld a,188 
  jp .updateblocks 

.step3:
  ld hl,animationtilescharacteraddr+(8*12)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*12)             ;rotating tile animation 1 character address in ro
  ld a,188  
  jp .updateblocks 

.step2:
  ld hl,animationtilescharacteraddr+(8*22)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*22)             ;rotating tile animation 1 character address in ro
  ld a,188  
  jp .updateblocks 

.step1:
  ld hl,animationtilescharacteraddr+(8*32)             ;rotating tile animation 1 character address in ro
  ld de,animationtilescoloraddr+(8*32)             ;rotating tile animation 1 character address in ro
  ld a,188     

.updateblocks:

  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,lemmyblock+(8*144)         ;rotating tile animation 1 character address in rom
  ld c,$98
  call outix32
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,lemmyblockclr+(8*144)             ;rotating tile animation 1 character address in rom
    ld c,$98
  call outix32

  ret





;to avoid unnescessary code complexity
MovementPattern278:     ;special platform (maplevel4-01a) (right part)
  
  ld (ix+v3),0


  ld c,0
  call platforminteract ;interact at all times

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  call mariointeract
  call c,.movedown

  ld a,(iy+v3) ;stop
  and a
  ret nz

  call writespecialpillartomapright
  call animatespecialpillarright

  ret


;move down fast
.movedown:

  ld (ix+v3),1 ;read from other object
  ld (iy+v3),0
  ld (ix+v2),1
  call addv2toybject
  
 ;get the other pillar and move it the oposite way
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  dec hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h 

  ret



;get yposition and write the pillar to map.

writespecialpillartomapright:

;fetch mapadress  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  ;mapadress is in hl

  ld de,$4000
  push hl
  xor a
  sbc hl,de
  pop hl
  ret c ;NEVER WRITE BELOW THIS ADRESS!!!!

  push hl
  ex de,hl
  ld hl,$c000
  xor a
  sbc hl,de
  pop hl
  ret c ;NEVER exceed page3!!!


;now write tiledata
;first row with bkg data
  push hl

  xor a
  ld b,4
  ld c,1
  call writemanytile
  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 1  
  push hl
  
  ld c,144
  ld a,(ix+objectrelease)
  and a
  call nz,.setothertype
  
  
  ld a,c
  ld c,0
  ld b,4
  call writemanytile
  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 2  
  push hl
  

  ld b,4
  ld c,0
  call writemanytile
  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 3  
  push hl
  

  ld b,4
  ld c,0
  call writemanytile

  pop hl
  ld de,(maplenght)
  add hl,de
;line 4  
  push hl
  

  ld b,4
  ld c,0
  call writemanytile

  pop hl

  ret

.setothertype:

  ld c,144-32
  ret


animatespecialpillarright:

  ld a,(ix+yobject)  
  and 7
  cp 0  
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
;  cp 7
;  jp z,.step7  
        
  
;  ret

  jp .step7

.writestep:

  ld c,144
  ld a,(ix+objectrelease)
  and a
  call nz,.setothertype
  
  
  ld a,c
  ld b,16
  jp updatelemmyblocks


;  ret


.step0:

  ld hl,butterblock+(8*0)
  ld de,butterblockclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,butterblock+(8*16)
  ld de,butterblockclr+(8*16)   

  jp .writestep

.step2:

  ld hl,butterblock+(8*32)
  ld de,butterblockclr+(8*32)   

  jp .writestep

.step3:

  ld hl,butterblock+(8*48)
  ld de,butterblockclr+(8*48)   

  jp .writestep

.step4:

  ld hl,butterblock+(8*64)
  ld de,butterblockclr+(8*64)   

  jp .writestep

.step5:

  ld hl,butterblock+(8*80)
  ld de,butterblockclr+(8*80)   

  jp .writestep

.step6:

  ld hl,butterblock+(8*96)
  ld de,butterblockclr+(8*96)  

  jp .writestep
  
.step7:

  ld hl,butterblock+(8*112)
  ld de,butterblockclr+(8*112)   

  jp .writestep


.setothertype:

  ld c,144-32
  ret



;this is another of those superspecial objects that only ocur on one map!
;We will have to aproach it a little bit like the lemmy castle blocks.
MovementPattern277:     ;special platform (maplevel4-01a)

  
  ld a,(ix+v4)
  and a
  jp z,.init

  xor a
  ld (forcesetbackground),a  

  ld c,0
  call platforminteract ;interact at all times

  call mariointeract
  push af
  call c,.movedown
  pop af
  call nc,.moveup




  call writespecialpillartomap 
  call animatespecialpillar


  ret

.moveupright:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld a,(iy+v3)
  and a
  ret nz


;automaticly move back up when there is no interaction
  ld a,(framecounter)
  and 1
  ret z ;slooooooow

  ld e,(ix+offsets)
  ld d,(ix+offsets+1)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
;  dec hl
;  dec hl
;  ld hl,(pillary)
  xor a
  sbc hl,de
  jp z,.returnv3

  ld (ix+v2),1
  call addv2toybject

;get the other pillar and move it the oposite way
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  dec hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h

  ld (ix+v3),0


  ret


.returnv3:

  ld (ix+v3),1


  ret


.moveup:
 
;automaticly move back up when there is no interaction
  ld a,(framecounter)
  and 1
  ret z ;slooooooow

  ld e,(ix+offsets)
  ld d,(ix+offsets+1)
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  xor a
  sbc hl,de
  jp z,.returnv3
  jp c,.moveupright

  
  ld (ix+v3),0
  
  ld (ix+v2),-1
  call addv2toybject
 

;get the other pillar and move it the oposite way
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  inc hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h




  ret

.movedown:

  ld (ix+v3),0

  ld (ix+v2),1
  call addv2toybject
  

;get the other pillar and move it the oposite way
  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy
  
  ld l,(iy+yobject)
  ld h,(iy+yobject+1)
  dec hl
  ld (iy+yobject),l
  ld (iy+yobject+1),h


  ret
  

.init:

;wait so that wide platforms come in play as well
  inc (ix+v7)
  ld a,(ix+v7)
  cp 100
  ret c



  inc (ix+v4)

   
  ;spawn second elevator
  
  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a  

;save the y coordinate information somewhere as we need to know when to stop

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld (ix+offsets),l
  ld (ix+offsets+1),h



;spawn right object
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ld de,8
  ld a,(ix+hitpoints)
  and a
  call nz,.set16
  add hl,de
  
;  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
;  inc de
  


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,275
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)

  call  activateobject
  
  ld hl,278
  ld (ix+movepat),l
  ld (ix+movepat+1),h

  push ix
  pop hl

  pop ix

  ld (ix+v5),l
  ld (ix+v5+1),h

;parse local ix to second object
  push hl
  pop iy
  
  push ix
  pop hl
  
  ld (iy+v5),l
  ld (iy+v5+1),h
  ld a,(ix+objectrelease)
  ld (iy+objectrelease),a ;parse objectrelease


  
  ;cheap hack. Move elevator down 1 pixel to prevent some stupid gfx glitch
  ld (ix+v2),1
  call addv2toybject


  ret


.set16:

  ld de,16
  ret


;get yposition and write the pillar to map.

writespecialpillartomap:

  ld a,(ix+yobject)
  and 7  
  and a
  call z,forcerebuildbackground
  cp 6
  call z,forcerebuildbackground   
  cp 7
  call z,forcerebuildbackground


;fetch mapadress  
  ld bc,0
  ld de,0
  call objecttilecheckfromrom
  ;mapadress is in hl

  ld de,$4000
  push hl
  xor a
  sbc hl,de
  pop hl
  ret c ;NEVER WRITE BELOW THIS ADRESS!!!!

  push hl
  ex de,hl
  ld hl,$c000
  xor a
  sbc hl,de
  pop hl
  ret c ;NEVER exceed page3!!!


;now write tiledata
;first row with bkg data
  push hl

  xor a
  ld b,4
  ld c,1
  call writemanytile

  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 1  
  push hl

  ld c,128
  ld a,(ix+objectrelease)
  and a
  call nz,.setothertype

  
  ld a,c
  ld c,0
  ld b,4
  call writemanytile
  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 2  
  push hl
  

  ld b,4
  ld c,0
  call writemanytile
  
  pop hl
  ld de,(maplenght)
  add hl,de
;line 3  
  push hl
  
  ld b,4
  ld c,0
  call writemanytile

  pop hl
  ld de,(maplenght)
  add hl,de
;line 4  
  push hl
  

  ld b,4
  ld c,0
  call writemanytile

  pop hl

  ret

.setothertype:

  ld c,128-32
  ret


animatespecialpillar:


  ld a,(ix+v3)
  and a
  ret nz

  ld a,(ix+yobject)  
  and 7
  cp 0  
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
;  cp 7
;  jp z,.step7  
        
  
;  ret

  jp .step7

.writestep:

  ld c,128

  ld a,(ix+objectrelease)
  and a
  call nz,.setothertype


  ld a,c
  ld b,16
  jp updatelemmyblocks


;  ret


.step0:

  ld hl,butterblock+(8*0)
  ld de,butterblockclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,butterblock+(8*16)
  ld de,butterblockclr+(8*16)   

  jp .writestep

.step2:

  ld hl,butterblock+(8*32)
  ld de,butterblockclr+(8*32)   

  jp .writestep

.step3:

  ld hl,butterblock+(8*48)
  ld de,butterblockclr+(8*48)   

  jp .writestep

.step4:

  ld hl,butterblock+(8*64)
  ld de,butterblockclr+(8*64)   

  jp .writestep

.step5:

  ld hl,butterblock+(8*80)
  ld de,butterblockclr+(8*80)   

  jp .writestep

.step6:

  ld hl,butterblock+(8*96)
  ld de,butterblockclr+(8*96)  

  jp .writestep
  
.step7:

  ld hl,butterblock+(8*112)
  ld de,butterblockclr+(8*112)   

  jp .writestep

.setothertype:

  ld c,128-32
  ret



;animations for the small elevators in butter bridge 1
butterblock:                      incbin "../grapx/world4/level4-01a/animation.SC2",$0000+7,8*128  ;character table
butterblockclr:                   incbin "../grapx/world4/level4-01a/animation.SC2",$2000+7,8*128  ;color table


lemmybosstable: 
;the row number is the type
;              sprcharadress,                 sprcoladress,             sprcharadress2,               sprcoladress2,        torsoheight
dw      characteraddresslemmyfront1,  coloraddresslemmyfront1,  characteraddresslemmyfront2,  coloraddresslemmyfront2     db    26  
dw      characteraddresslemmyfront3,  coloraddresslemmyfront3,  characteraddresslemmyfront4,  coloraddresslemmyfront4     db    26  
dw      characteraddresslemmyleft1,   coloraddresslemmyleft1,   characteraddresslemmyleft2,   coloraddresslemmyleft2      db    29  
dw      characteraddresslemmyright1,  coloraddresslemmyright1,  characteraddresslemmyright2,  coloraddresslemmyright2     db    29  
dw      characteraddresslemmydoll1,   coloraddresslemmydoll1,   characteraddresslemmydoll1,   coloraddresslemmydoll1      db    26  

lenghtlemmybosstable: equ 9

spritechr1:        equ 0
spriteclr1:        equ 2
spritechr2:        equ 4
spriteclr2:        equ 6
torsoheight:       equ 8


MovementPattern276:     ;lemmy (boss)

  call .fetchiy


  call mariointeract
  jp c,.becomepissed

  ld a,(ix+v4)
  and a
  jp z,.init
  dec a
  dec a
  jp z,.riseup ;2
  dec a
  jp z,.wait ;3
  dec a
  jp z,.godown ;4
  dec a
  jp z,.ispissed ;5
  dec a
  jp z,.godownpissed ;6
  dec a
  jp z,.prepdie ;7
  dec a
  jp z,.die ;8  
  dec a
  jp z,.setendlevel ;9
  dec a
  jp z,.endlevel ;10


  ld a,(pillaranimationstep)
  and a
  ret nz

  inc (ix+v7)
  ld a,(ix+v7)
  cp 40
  ret c

  call .incv4
  
 
;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  


.fullhide:

  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite

  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  xor a
  call hidemariosprite 

  ret


.setfadeout:

    ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  ret

.endlevel:



  ld l,(ix+v5)
  ld h,(ix+v5+1)
  dec hl
  ld (ix+v5),l
  ld (ix+v5+1),h
  ld de,110
  xor a
  push hl
  sbc hl,de
  pop hl
  jp z,.setfadeout
  ld a,l
  or h
  ret nz




;TODO: build in exit scene  
  ld a,1
  ld (levelend?),a



  ret



.setendlevel:
  
  ;kill the flame. DANGER! we asume the flame is spawned first so this is kinda hackish and will only work on this map.
  ld iy,Objectlist
  ld (iy+yobject),0
  ld (iy+yobject+1),0
  
  
  
  ld hl,300
  ld (ix+v5),l ;use this as timer.... :(
  ld (ix+v5+1),h

  xor a
  ld (time),a ;reset timer

  ld a,2
  ld (playwinmusic),a

  jp .incv4

;  ret


.falldown:

  ld a,(ix+v7)
  cp 22
  jp nc,.incv4


  ld (ix+v2),8
  call addv2toybject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  inc (ix+v7)

  ret


.die:

  ld a,(ix+v7)
  cp 16
  jp nc,.falldown

  ld (ix+v2),1
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

;  ld a,(ix+v7)
;  inc a
;  inc a

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,4*32
  call unhideobjectspriteupdown


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 

  push ix
  pop iy

;  ld a,(ix+v7)
;  inc a
;  inc a

  ld b,(ix+v7)
  ld a,16
  sub b

  ld bc,6*32
  call unhideobjectspriteupdown

  inc (ix+v7)


  ret


.prepdie:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 3
  ret c

  call Playsfx.bossdies

  call .incv4

  ;make sure to init the color sprite data at first start during init  
  ld hl,coloraddresslemmydead
  ld bc,characteraddresslemmydead
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset



 ; ret



.godownpissed:


  ld a,(ix+v7)
  cp 16-5
  jp nc,.godownpissed2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret



.godownpissed2:

  ld a,(ix+v7)
  cp 32-6
  jp nc,.restartafterhit
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

.restartafterhit:

  ld a,(ix+objectrelease)
  and a
  jp nz,.restart


  dec (ix+hitpoints)
  jp z,.lemmydies
  
  jp .restart
  

.lemmydies:

  ld a,1
  ld (pillaranimationstep),a ;use this to kill of the puppet

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

;write dead lemmy
  ld hl,coloraddresslemmydead
  ld bc,emptysprite94
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

;set right above the underside of the tube
  ld a,20*8
  ld (ix+yobject),a
  ld (ix+yobject+1),0
  
  jp .incv4


;  ret


.ispissed:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30
  jp nc,.incv4


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames) 

  ld a,(ix+objectrelease)
  and a
  jp nz,.puppet


  ld a,(framecounter)
  and 3
  jp z,.ispissed2

  ld bc,characteraddresslemmypissed2
  ld hl,coloraddresslemmypissed2
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp   setcharcolandoffset


;  ret

.ispissed2:

  ld bc,characteraddresslemmypissed1
  ld hl,coloraddresslemmypissed1
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp   setcharcolandoffset
  
  ;ret


.puppet:

  ld a,(ix+v7)
  cp 3
  ret c


  ld bc,characteraddresslemmydoll3
  ld hl,coloraddresslemmydoll3
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp   setcharcolandoffset
  
  ;ret



.becomepissed:

  ld a,(ix+v4)
  cp 3
;  ret nz ;only when fully up
  jp nz,Playsfx.chuckstomp

  ld a,(iy+torsoheight)
  cp 27
  call nc,.movedown 

  ld a,(ix+objectrelease)
  and a
  jp nz,.crumblepuppet

;  push iy  
  call Playsfx.chuckauw
;  pop iy


  ld (ix+v4),5
  ld (ix+v7),0
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
   
  ld bc,characteraddresslemmypissed1
  ld hl,coloraddresslemmypissed1
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp   setcharcolandoffset 

;  ret


.crumblepuppet:

  call Playsfx.chuckstomp ;BUG: some wierd stuff happens here. not good

  ld (ix+v4),5
  ld (ix+v7),0
  

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)  
   
  ld bc,characteraddresslemmydoll2
  ld hl,coloraddresslemmydoll2
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp   setcharcolandoffset 

;  ret




.movedown:



  ld (ix+v2),3
  call addv2toybject
  


  ret


;sink back in
.godown:

  ld a,(iy+torsoheight)
  cp 27
  jp nc,.godownother ;other wait type


  ld a,(ix+v7)
  cp 16-5
  jp nc,.godown2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-7
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret

.setzero:

  xor a
  ret


.godown2:

  ld (ix+deadly),0

  ld a,(ix+v7)
  cp 32-6
  jp nc,.restart
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-7
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

.godownother:


  ld a,(ix+v7)
  cp 14
  jp nc,.godownother2

  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-4
  sub b
  call c,.setzero
  call hidemariosprite


  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,16-4
  sub b
  call c,.setzero  
  call hidemariosprite 

  inc (ix+v7)

  ret

.godownother2:

  ld (ix+deadly),0

  ld a,(ix+v7)
  cp 32-3
  jp nc,.restart
  
  ld (ix+v2),1
  call addv2toybject


  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-4
  sub b
  call c,.setzero   
  call hidemariosprite


  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 
  
  ld a,(ix+v7)
  ld b,a
  ld a,32-4
  sub b
  call c,.setzero   
  call hidemariosprite 

  inc (ix+v7)

  ret  

  

.restart:

  ld (ix+v4),0
  ld (ix+v7),0

  ret


;do nothing do some action then get the fuck out.
.wait:

  ld a,(iy+torsoheight)
  cp 27
  jp nc,.wait2 ;other wait type

  inc (ix+v7)
  ld a,(ix+v7)
  cp 20
  jp nc,.stopwait1


  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(framecounter)
  and 7
  jp z,.one
  and 3
  dec a
  ret z
  dec a
  ret z
  dec a
  ret z

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.one:

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr2) ;color data adress
  ld h,(iy+spriteclr2+1)
  ld c,(iy+spritechr2)
  ld b,(iy+spritechr2+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


;  ret


.stopwait1:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
  ld c,(iy+spritechr1)
  ld b,(iy+spritechr1+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset
  
  jp .incv4
  
  ;ret


.wait2:

  inc (ix+v7)
  ld a,(ix+v7)
  cp 20
  ret c
  cp 40
  jp nc,.incv4

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr2) ;color data adress
  ld h,(iy+spriteclr2+1)
  ld c,(iy+spritechr2)
  ld b,(iy+spritechr2+1) ;character data adress  
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


 ; ret



.fetchiy:

  ld l,(ix+v5)
  ld h,(ix+v5+1)
  push hl
  pop iy

  ret


.lemmyrandompositiontable:

  ;  x,  y
  db 3*8,  16*8
  db 7*8,  18*8
  db 11*8, 18*8
  db 15*8, 16*8
  db 19*8, 14*8
  db 23*8, 16*8
  db 27*8, 18*8
  db 11*8, 18*8 ;spare row

.init:

  ld (ix+deadly),0 ;to make it a little bit more friendly

 ;pick a random number and decide at which tube lemmy comes up. furhermore spawn the puppet lemmy and save the iy info somehwere. There are enough unused free positions in ram for that
  call makerandomnumber
  and 7
  inc a
  ld b,a

  ld hl,.lemmyrandompositiontable-2
  ld de,2
.getlemmypositiondata:
  add hl,de
  djnz .getlemmypositiondata

  ld a,(hl)
  ld (ix+xobject),a
  ld (ix+xobject+1),0

  inc hl
  
  ld a,(hl)
  dec a
  ld (ix+yobject),a
  ld (ix+yobject+1),0  
  
  

  call makerandomnumber
  and 3
  inc a
  ld b,a

  ld a,(ix+objectrelease)
  and a
  call nz,.initpuppet

;make sure that a random number is generated and sprites are picked from a table   
;write sprite data in place and directly hide it
 ; ld b,3 ;"random" generated number
  ld iy,lemmybosstable-lenghtlemmybosstable
  ld de,lenghtlemmybosstable
.getlemmydata:
  add iy,de
  djnz .getlemmydata
  
  push iy
  pop hl
  ld (ix+v5),l ;store data table
  ld (ix+v5+1),h

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  inc (ix+v4)

;make sure to init the color sprite data at first start during init  
  ld l,(iy+spriteclr1) ;color data adress
  ld h,(iy+spriteclr1+1)
;  ld c,(iy+spritechr1)
;  ld b,(iy+spritechr1+1) ;character data adress  
  ld bc,emptysprite94
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset


  ld a,(ix+v3)
  and a
  ret nz
 
  inc (ix+v3) ;only do this once


  xor a
  ld (pillaranimationstep),a

;spawn the puppet
  ld hl,80
  ld de,80

  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object   
  ld hl,274 
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)


  call  activateobject
  ld (ix+objectrelease),1
  ld (ix+v3),1

  pop ix


  ret


.initpuppet:

  ld b,5

  ret


.incv4:
  
  inc (ix+v4)
  ld (ix+v7),0
  ret


.riseup:

  ld a,(ix+v7)
  cp 16
  jp nc,.riseup2

  ld (ix+v2),-1 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,0
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  inc a
  inc a
  ld bc,0*32
  call unhideobjectsprite

  push ix
  pop hl
  ld bc,2
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  inc a
  inc a
  ld bc,2*32
  call unhideobjectsprite

  inc (ix+v7)

  ret

.riseup2:

  ld (ix+deadly),1 ;now we become pissed

  call .fetchiy
  ld c,(iy+torsoheight)
  ld a,(ix+v7)
  cp c
  jp nc,.incv4


  ld (ix+v2),-1 ;move at fast steps
  
  call addv2toybject
  
  push ix
  pop hl
  ld bc,4
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  sub a,16  
  inc a
  inc a
  ld bc,4*32
  call unhideobjectsprite

  push ix
  pop hl
  ld bc,6
  call getobjectspriteadress 

  push ix
  pop iy

  ld a,(ix+v7)
  sub a,16
  inc a
  inc a
  ld bc,6*32
  call unhideobjectsprite

  inc (ix+v7)

  ret




MovementPattern275:     ;moving flame (bouncing)

    ld a,(ix+v4)
    and a
    jp z,.init

  call mariointeract ;make it deadly

;works by looking at the v1 and v2 directions
  ld a,(framecounter)
  and 3
  call z,.animate

;call movement
  call addv1toxbject
  call addv2toybject


  ld bc,8
  ld de,10
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1

  ld bc,8
  ld de,-8
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1


  ld bc,0
  ld de,1
  call objecttilecheckfromrom
  cp 192
  call nc,.negatev2

  ld bc,16
  ld de,0
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev2

  ld bc,0
  ld de,10
  call objecttilecheckfromrom
  cp 192
  call nc,.negatev1

  ld bc,0
  ld de,-8
  call objecttilecheckfromrom
  cp 192
  jp nc,.negatev1


  ret

.negatev1:

  ld a,(ix+v1)
  neg
  ld (ix+v1),a
  ret

.negatev2:

  ld a,(ix+v2)
  neg
  ld (ix+v2),a
  ret


.animate:

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  jp z,.animate2

  ld a,(ix+v1) ;check left/right
  bit 7,a
  jp z,.upright



.upleft:

  ld a,(ix+v2)
  bit 7,a
  jp z,.downleft

  ld    hl,coloraddressmovingflame1upleft  ;color address
  ld    bc,characteraddressmovingflame1upleft;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.downleft:

  ld    hl,coloraddressmovingflame1downleft  ;color address
  ld    bc,characteraddressmovingflame1downleft;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  
.upright:

  ld a,(ix+v2)
  bit 7,a
  jp z,.downright

  ld    hl,coloraddressmovingflame1upright  ;color address
  ld    bc,characteraddressmovingflame1upright;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.downright:

  ld    hl,coloraddressmovingflame1downright  ;color address
  ld    bc,characteraddressmovingflame1downright;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset




  ;animate in direction not in steps

.animate2:


  ld a,(ix+v1) ;check left/right
  bit 7,a
  jp z,.upright2



.upleft2:

  ld a,(ix+v2)
  bit 7,a
  jp z,.downleft2

  ld    hl,coloraddressmovingflame2upleft  ;color address
  ld    bc,characteraddressmovingflame2upleft;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.downleft2:

  ld    hl,coloraddressmovingflame2downleft  ;color address
  ld    bc,characteraddressmovingflame2downleft;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

  
.upright2:

  ld a,(ix+v2)
  bit 7,a
  jp z,.downright2

  ld    hl,coloraddressmovingflame2upright  ;color address
  ld    bc,characteraddressmovingflame2upright;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.downright2:

  ld    hl,coloraddressmovingflame2downright  ;color address
  ld    bc,characteraddressmovingflame2downright;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset


.init:

    inc (ix+v4)

    ld a,(currentlevel)
    cp 90
    ret c

    ld (ix+v1),-2
    ld (ix+v2),-2

    

    ret


;  ret



MovementPattern274:    ;waterplatform (castle type)
 
 ;buffer y coordinates for the first time in order to determine the maximum water level
  ld a,(ix+v4)
  and a
  jp z,.savey
 
  call mariointeract
  call c,.unsetswim
  
  ld c,0
  call platforminteract ;WArning: objects that do checktilebelow will also be affected by this platform

  ld a,(ix+v3)
  dec a
  jp z,.noyadd


  ld l,(ix+v5)
  ld h,(ix+v5+1)
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ex de,hl
  xor a
  sbc hl,de
  jp c,.noyaddspecial


  ld (ix+v2),-1
  call addv2toyobject


.noyadd:
  ld (ix+v3),0
  ret


;wobble up and down
.noyaddspecial:
  
  ld a,(framecounter)
  and 15
  jp z,.up
  and 7
  jp z,.down
  jp .noyadd

.up:

  ld (ix+v2),-1
  call addv2toybject
  jp .noyadd

.down:

  ld (ix+v2),1
  call addv2toybject
  jp .noyadd

.unsetswim:

;we check if mario is above the box, if yes return to the other checks anyway
  ld e,(ix+yobject)
  ld d,(ix+yobject+1)
  ld hl,(marioy)
  push de
  ld de,16
  add hl,de
  pop de
  xor a
  sbc hl,de
  ret nc


  xor a
  ld (marioswim),a

  ld (ix+v2),1
  ld (ix+v3),1
  call addv2toyobject


  ret 

.savey:

  ld (ix+v4),1
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  
  dec hl
  dec hl
  dec hl
  dec hl
  
  ld (ix+v5),l
  ld (ix+v5+1),h


  ret


MovementPattern273:     ;red mushroom (static)

  call mariointeract
;  pop bc
;  ld b,(ix+objectrelease)
  ld b,2
  jp c,.settransform

      ld (ix+v2),0
  
  
    ;check down
  ld bc,17
  ld de,0
  call objecttilecheckfromrom
  cp 191
  jp nc,.resetfp
  
    call objectfallacceleration
    ld (ix+v2),c
    
    call addv2toybject
  

  ret


.resetfp:  
  
    ld h,(ix+yobject+1)
    ld a,(ix+yobject)
    and %11111000
 ;   ld l,a
    ld (ix+yobject+1),h
    ld (ix+yobject),a
  ld (ix+objectfp),0
  ret
 
 
;1up branch
.settransform:

  ld a,(ix+objectrelease)
  cp 5
  jp z,setextralife

  jp settransform

 ; ret


MovementPattern272:     ;single block sprite

;now this!! is shitty coding hahahahaha

  ;we know for a fact that this sprite comes second in line so it will sync (almost) perfectly with the helper object.
  ld hl,(pillary) ;no calculations simple assumptions
  ld de,128-16
  add hl,de
  ld (ix+yobject),l
  ld (ix+yobject+1),h
  
  ret



animatelemmylava:


;  ld    a,animationtilesblock
;  call  block12

;take a look at the last 3 bits of yobject and adjust the tiles pixle precise according to it 

  ld    a,(framecounter)
  inc a
  and   15
  jr    z,.step1
  cp    4
  jr    z,.step2
  cp    8
  jr    z,.step3
  cp    12
  ret   nz
  
    exx


.step4:
  ld hl,lemmyblock+(8*162)             ;rotating tile animation 1 character address in ro
  ld de,lemmyblockclr+(8*162)             ;rotating tile animation 1 character address in ro
  ld a,173 
  jp .updateblocks 

.step3:
  ld hl,lemmyblock+(8*156)             ;rotating tile animation 1 character address in ro
  ld de,lemmyblockclr+(8*156)             ;rotating tile animation 1 character address in ro
  ld a,173  
  jp .updateblocks 

.step2:
  ld hl,lemmyblock+(8*150)             ;rotating tile animation 1 character address in ro
  ld de,lemmyblockclr+(8*150)             ;rotating tile animation 1 character address in ro
  ld a,173  
  jp .updateblocks 

.step1:
  ld hl,lemmyblock+(8*144)             ;rotating tile animation 1 character address in ro
  ld de,lemmyblockclr+(8*144)             ;rotating tile animation 1 character address in ro
  ld a,173     

.updateblocks:

  exx 


  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  push hl

  call .doshit

  pop hl
  ld de,$4000
  add hl,de
 
;  jp .doshit

; ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
 ; ld    hl,$0000+71*8                                ;vram address of charactertile to write to
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl

;  ld    hl,lemmyblock+(8*144)         ;rotating tile animation 1 character address in rom
  ld c,$98
  call outix48
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0


 ; ld    hl,$2000+71*8                                ;vram address of charactertile to write to
  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

;  ld    hl,lemmyblockclr+(8*144)             ;rotating tile animation 1 character address in rom
    ld c,$98
  call outix48

  ret


;IN a = tilenumber IN = HL tilegfx adress DE = Tilegfx color address Changes all registers including the alternate ones
;b = in case of a loop
updatelemmyblocks:

  push bc
  exx 
  pop bc ;get bc in exx

  ld l,a  
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
 


  
  ld a,(framecounter)
  and 1
  jp z,.doshit
  
  ld de,$4000
  add hl,de
 ; jp .doshit



 ;ret 
  
.doshit:

  xor   a                                             ;reset carry, so vdp write is in page 0
  push hl

  call  SetVdp_Write                                  ;set vdp to write at character of tile 148

  exx
  push hl
  exx
  pop hl


  ld c,$98
  push bc
.write1:  
  
  push bc
  call outix8 ;outi modifies bc
  pop bc
  djnz .write1
  pop bc
  
  pop hl


  ld de,$2000
  add hl,de

  xor   a                                             ;reset carry, so vdp write is in page 0

  call  SetVdp_Write                                  ;set vdp to write at character of tile 186

  exx
  push de
  exx
  pop hl

  ld c,$98
    
.write2:   
  push bc
  call outix8
  pop bc
  djnz .write2

  exx

  ret



;animations for horizontal objects in map 03-11c
lemmyblock:                      incbin "../grapx/world3/level3-11c/animation.SC2",$0000+7,8*168  ;character table
lemmyblockclr:                   incbin "../grapx/world3/level3-11c/animation.SC2",$2000+7,8*168  ;color table

;ret


;move lemmy blocks on the screen
;these are vertical blocks that are controlled by 1 object over the entire map.
;scheme flow. Wehave a steady height of the lava. At that height we do not write any tiles
;this height is stored in (objectrelease)


MovementPattern271:     ;lemmy block mover
 
  xor a
  ld (forcesetbackground),a

  ld a,(ix+v4)
  and a
  jp z,.init


  call .murdermarioiflava
  call .followmario
  call animatelemmylava
  
  ;do a platforminteract only if mario is within the boundaries of the pillar
  call searchblocktable ;search through the blocktable and write the blocks in place when close enough in screen

  call animatelemmyblocks


  ;do movement here
  ld a,(framecounter)
  and 1
  ret z

  ld a,(ix+v3)
  and a
  call nz,.up
  call z,.down
  
  ld hl,(pillary)
  ld a,l
  or h
  jp z,.reverse
  srl h 
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 ;divide by 8 and compare to max value
  ld a,(mapheight)
  ld c,18
  sub c
  srl a ;/2 we use half the (mapheight-16) as our reference pointer
  cp l
  jp c,.reverse  
  
  ret

  

.down:
  
  ld hl,(pillary)
  inc hl
  ld (pillary),hl
  
  ret

.up:

  ld hl,(pillary)
  dec hl
  ld (pillary),hl
  
  ret
  


.reverse:

  ld a,(ix+v3)
  xor 1
  ld (ix+v3),a
  
  
  ret


.followmario: ;only follow for x. we handle Y separately

  ld hl,(mariox)
  ld (ix+xobject),l
  ld (ix+xobject+1),h

  ret

;check lava but check for bkg as well. blocks should crush mario
.murdermarioiflava:


  ld de,0
  ld b,3
  call checkgeneraltilefromhrom
;check downward crush
  cp 040
  jp z,.kill
  cp 041
  jp z,.kill
  cp 042
  jp z,.kill

  ld de,0
  ld b,4
  call checkgeneraltilefromhrom
  cp 130
  jp z,.kill
;check for upward crush  
  cp 216
  jp z,.kill
  cp 216
  jp z,.kill  

  
  cp 173
  jp nc,.check

  ret

.check:

  cp 179
  ret nc

.kill:

  
  ld a,10
  ld (mariotransforming),a
  ld a,60
  ld (dieatonce),a

  ret 
  
  
  

.init:

    ld a,(currentlevel)
    cp 133
    jp z,.setwendy ;set to wendy type

  ;copy pillar buffer to RAM copy from lemmypillartable to pillarbkgbuffer in page3 which is not in use.
  ld hl,lemmyblocktable
  ld de,pillarbkgbuffer
  ld bc,totallenghtlemmyblocktable
  ldir

  ld a,1 ;make sure that the engine knows we are only animating blocks
  ld (animatespecial),a

  inc (ix+v4)

  ret

.setwendy:

    ld hl,337
    ld (ix+movepat),l
    ld (ix+movepat+1),h
    
    ret
    


searchblocktable:

  ld iy,pillarbkgbuffer
.searchloop:  
  ld e,(iy)
  ld d,(iy+1) ;fetch x coordinates for this object

  ld a,e
  or d
  ret z ;end of table reached

;do not handle pillar if out of screen!
  push de
  call .getdistance
  pop de
  
  ld (iy+reset),0 ;reset this byte if mario is back in distance of object
  
  ld a,(iy+yposition) ;tile height for each object
  ld hl,(pillary) ;global for pillarheight Y 16bits pixel precise
  
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l  
  ;we now have the height of the object
  add a,l
  ld c,a

;see if object is within mario boundaries. if so adjust the Y coordinates and call the handleplatform interact code.
  push de
  push bc
  push iy
  call .checkmariorange
  pop iy
  pop bc
  pop de

;we check if we are at the edges of the 8 animation steps at that point
;the screen tiles get updated but there is sometimes a small mismatch inbetween frames
;this creates a ugly glitch where it looks as if the platform very quickly changes direction for 1 frame
;we force a screen rebuild after returning from the eventhandler so that the last animation step and tilewrite
;is in perfect sync.
  ld a,(pillary)
  and 7  
  and a
  call z,forcerebuildbackground 
  cp 7
  call z,forcerebuildbackground
;  cp 1
;  jp z,.skipobject  
  cp 2
  jp z,.skipobject ;optimize code a little by only writing the tiles when really needed (still slow as shit on MSX2 (+) )
  cp 3
  jp z,.skipobject
  cp 4
  jp z,.skipobject
  cp 5
  jp z,.skipobject
  cp 6
  jp z,.skipobject  

;skip pillarwriting at these frames to speed things up a little we are seeing screen tears

;we have two types of moving blocks. Here we branch to write the correct type in screen
  ld a,(iy+type)
  and a
  push af
  call z,handlepillar0type
  pop af
  dec a
  push af
  call z,handlepillar1type
  pop af
  dec a
  call z,handlepillar2type

.skipobject:

  ld de,lemmyblocktablelenght
  add iy,de
  jp .searchloop

;  ret

;problem: When the blocks are going down the black tiles are not written any longer resulting in bad tilebugs
;fix?? perhaps write black bkg once by setting a certain byte? pillarbkgbuffer is unused!
.getdistance:


  ld hl,(mariox)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8  
  and a
  sbc hl,de
  jp c,.negative

  ld de,32
  xor a
  sbc hl,de
  ret c

  jp .stop
  
  
.negative:

  ld de,0
  ex de,hl
  and a
  sbc hl,de

  ld de,32
  xor a
  sbc hl,de
  ret c

.stop:

  ld a,(iy+reset)
  and a
  call z,.destroypillartiles ;destroy only in one frame!

  inc sp
  inc sp
  pop de ;do not forget to pop the byte back!!
  
  
  ld de,lemmyblocktablelenght
  add iy,de
  jp .searchloop
  
  ;ret


.destroypillartiles:

  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1

  ld a,(iy+type)
  dec a
  jp z,.destroytype1 ;branch to other pillar type
  dec a
  jp z,.stoperase ;fuck this

;only fetch bkg adress when tiles need destruction
  ld a,(iy+yposition) ;tile height for each object
  ld hl,(pillary) ;global for pillarheight Y 16bits pixel precise
  
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8
  ;we now have the height of the object
  add a,l
  ld c,a
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  ld a,e
  or d
  jp z,.stoperase ;out of map! presearchtable should have a empty byte here any other value will be catched by the following checks
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found  

  ld e,(iy)
  ld d,(iy+1)
  ;Xcoordinate
  add hl,de


;do not even allow this if the pillar is below the lava!!!
  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
;  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;get the mapadress where are we?? 
  
  pop de
  xor a
  sbc hl,de
  exx

  jp c,.stoperase

;erase all lines
.completeline:
  
  ld b,(iy+xsize)
  push hl
.deleteloop:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .deleteloop
  pop hl

  ld de,(maplenght)
  add hl,de ;next row


  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
;  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr ;Y mapadress
  add hl,de

  ;get the mapadress back where are we??   
  pop de
  xor a
  sbc hl,de
  exx
  
  
  jp nc,.completeline


;write back lava
  ld b,(iy+xsize)
  srl b
.deletelavaloop:
  ld a,175
;  call writeanytile
  ld (hl),a
  inc hl
  ld a,176
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .deletelavaloop  



.stoperase:  
  
  ld (iy+reset),1
  
  ret



.destroytype1:

;BUG too many tiles are being cleared!!
; ret
  ld    hl,bgrmapaddr
  ld e,(iy)
  ld d,(iy+1)
  ;Xcoordinate
  add hl,de  ;fetch X keep Y at zero
  
  ld e,25 ;amount of lines to erase

.destroytype1loop:

  push de
  
  ld b,(iy+xsize)
  push hl
.deleteloop2:
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skip ;we are inside a special tile
  cp 213
  jp z,.skip ;we are inside a special tile  
  xor a
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .deleteloop2
.skip:
  pop hl

  ld de,(maplenght)
  add hl,de ;next row

  pop de

  dec e
  
  jp nz,.destroytype1loop

  


;pillar taken out work done
  jp .stoperase

  ;ret


;DO not change DE or BC!!
.checkmariorange:

  ld a,(iy+ignore)
  and a
  jp nz,.unsetignore ;ignore is being unset after 1 frame

  ld a,(iy+type)
  dec a
  ret z ;type 1 has no platform interactions

  ld hl,(mariox)
  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l   
  xor a
  push hl
  sbc hl,de
  pop hl
  ret c ;exceeds left boundary

  ld c,(iy+xsize)
  ld b,0
  ex de,hl
  add hl,bc
  xor a
  dec hl ;compensate for smoother step off
  sbc hl,de
  ret c; exceeds right boundary

  ;now get the exact coordinates of the objectheight in 16 bits and adjust the elevation to that
  
  ld l,(iy+yposition) ;tile height for each object and multiply this number by 8 then add pillary to it making it pixel precise 
  ld h,0
  add hl,hl ;*2
  add hl,hl ;*4
  add hl,hl ;*8
  ld de,(pillary)
  add hl,de
  ld de,10 ;compensate for pixels in the top layer ;10
  xor a
  sbc hl,de

  push hl
  ld de,(marioy)
  xor a
  sbc hl,de
  call nc,.setheight ;only for pillar type 2
  pop hl

;prevent the object from exiting the map 
  push hl
  ex de,hl
  ld hl,(mapheightx8)
  push de
  ld de,32
  xor a
  sbc hl,de
  pop de  
  pop de
  
  push de
  xor a
  sbc hl,de
  pop hl
  ret c

  ;write result to object
  ld (ix+yobject),l
  ld (ix+yobject+1),h


  ;everything good now check if mario is inside the pillar for some stupid reason and if so push him out
  ld de,1
  ld b,3
  call checkgeneraltilefromhrom
  cp 037
  call nc,.pushmarioup
  
  ld c,0
  jp platforminteract
  
;  ret


.setheight:

  ld a,(iy+type)
  cp 2
  ret nz ;only type 2

  push iy
  ld de,lemmyblocktablelenght
  add iy,de
  ld (iy+ignore),1 ;tell next object to ignore platform interactions
  pop iy
  ret

.unsetignore:

  ld (iy+ignore),0
  ret


.pushmarioup:

;if a type 1 pillar is above mario and a type 0 below jumping against the type 1 will results in problems so we must check height boundaries too
;  ld a,(standingonsprite)
;  and a
;  ret z
  cp 040
  ret z
  cp 041
  ret z
  cp 042
  ret z

  ld hl,(marioy)
  ld de,8
  xor a
  sbc hl,de
  ld (marioy),hl


  ret

;IN c = true Yposition of pillar. DE = Xcoordinates of pillar
handlepillar2type:
;very similar to type 0 however the size and writes are different. We do not check any boundaries and blindly build up an entire block and thats it
;write from up to down
;we are having this code like 3 times. Maybe we could compress it a little bit over here??!!

  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1


  push de
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found
  
  
  pop de
  add hl,de
  ;x adress found. We now have the mapadress

  push hl
  ld de,(maplenght)
  xor a
  sbc hl,de
 
  ld b,(iy+xsize)  
.line0:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .line0
  pop hl


  push hl

  ld a,037
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line1:
  inc hl
  ld a,038
;  call writeanytile
  ld (hl),a
  djnz .line1
  inc hl
  ld a,039
;  call writeanytile
  ld (hl),a
  
  pop hl
  
  ld de,(maplenght)
  add hl,de

  push hl

  ld a,221
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line2:
  inc hl
  ld a,222
;  call writeanytile
  ld (hl),a
  djnz .line2
  inc hl
  ld a,223
;  call writeanytile
  ld (hl),a
  
  pop hl  

  ld de,(maplenght)
  add hl,de

  push hl

  ld a,224
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line3:
  inc hl
  ld a,225
;  call writeanytile
  ld (hl),a
  djnz .line3
  inc hl
  ld a,226
;  call writeanytile
  ld (hl),a
  
  pop hl

  ld de,(maplenght)
  add hl,de

  push hl

  ld a,227
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line4:
  inc hl
  ld a,228
;  call writeanytile
  ld (hl),a
  djnz .line4
  inc hl
  ld a,229
;  call writeanytile
  ld (hl),a
   
  pop hl 

  
  ld de,(maplenght)
  add hl,de

  push hl

  ld a,040
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line5:
  inc hl
  ld a,041
;  call writeanytile
  ld (hl),a
  djnz .line5
  inc hl
  ld a,042
;  call writeanytile
  ld (hl),a
   
  pop hl 


  ld de,(maplenght)
  add hl,de

  ld b,(iy+xsize)
.line6:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .line6


  ret


;IN c = true Yposition of pillar. DE = Xcoordinates of pillar
handlepillar1type:
;lucky for us type 1 has no exit tiles meaning that the blocks are always in the foreground. 
;I am even happier about the fact that there is absolutely no need to check for mapexit!
;The pillars never exceed the maximum height of the map. This makes clearing quite easy too :)
;Limit the amount of tilewrites to a certain height in the map. This saves alot of writes to RAM
;preventing framedrops on MSX2(+)

;PLAN: each tile is written with call writeanytile. This requires 2xout and is a call with a return.
;Even on the turbo-R this gives a major slowdown. It is more optimal to switch page1 to RAM (lucky for us the map fits in 1 page)
;and do direct tilewrites from code saving us 1200 outs per frame!
  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1
    
  push de
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found
  
  
  pop de
  add hl,de
  ;x adress found. We now have the mapadress
  
  
  push hl
  
  ld de,(maplenght)
  add hl,de
  
  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline0
  cp 213
  jp z,.skipline0
  cp 210
  jp z,.skipline0
  cp 008
  jp z,.skipline0
  
;bottomline
  ld b,(iy+xsize)
.line0:
  xor a
;  call writeanytile
  ld (hl),a
  inc hl  
  djnz .line0  

.skipline0:

  pop hl


  push hl

  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline1  
  cp 210
  jp z,.skipline1

  ld a,040
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line1:
  inc hl
  ld a,041
;  call writeanytile
  ld (hl),a
  djnz .line1
  inc hl
  ld a,042
;  call writeanytile
  ld (hl),a

.skipline1:

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de



  push hl

  ;do not write this line if there is foreground
;  call readanytile
  ld a,(hl)
  cp 208
  jp z,.skipline2  
  cp 210
  jp z,.skipline2


  ld a,227
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line2:
  inc hl
  ld a,228
;  call writeanytile
  ld (hl),a
  djnz .line2
  inc hl
  ld a,229
;  call writeanytile
  ld (hl),a

.skipline2:

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de

.writeline3:

  push hl

  ld a,230
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line3:
  inc hl
  ld a,231
;  call writeanytile
  ld (hl),a
  djnz .line3
  inc hl
  ld a,232
;  call writeanytile
  ld (hl),a

  pop hl
  ld de,(maplenght)
  xor a
  sbc hl,de


  push hl
  exx

  ld hl,(marioy)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8  
  ld de,11
  xor a
  sbc hl,de
  
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,l ;lets say stop at where mario is so that the engine only places the tiles in screen that we see
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress
  pop de
  xor a
  sbc hl,de
  exx

  jp c,.writeline3


  ret


;IN c = true Yposition of pillar. DE = Xcoordinates of pillar
handlepillar0type:

  ld a,(ix+objectrelease) ;see if the height is below the lavay, if yes stop!
  cp c
  ret c

  ld a,(slot.page2rom)
  out ($a8),a ;fetch page1

    
  push de
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,c
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;Y adress found
  
  
  pop de
  add hl,de
  ;x adress found. We now have the mapadress
  
;skip black topline when fully upwards as well  
  push hl
  ld hl,(pillary)
  ld de,8
  xor a
  sbc hl,de
  pop hl
  jp c,.skipblacktopline
  
  
;black topline
  push hl
  ld de,(maplenght)
  xor a
  sbc hl,de  

  ld b,(iy+xsize)
.line0:
  ld a,0
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .line0

  pop hl

.skipblacktopline:

  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  cp c
  jp c,.writelavaback ;done so write the old lava back

  push hl

  ld a,037
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line1:
  inc hl
  ld a,038
;  call writeanytile
  ld (hl),a
  djnz .line1
  inc hl
  ld a,039
;  call writeanytile
  ld (hl),a

  pop hl
  ld de,(maplenght)
  add hl,de
  
  
  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  dec a
  cp c
  ret c ;done  
  
  
  push hl

  ld a,221
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line2:
  inc hl
  ld a,222
;  call writeanytile
  ld (hl),a
  djnz .line2
  inc hl
  ld a,223
;  call writeanytile
  ld (hl),a

  pop hl

  ld a,(ix+objectrelease) ;see if the height is below the lavay if yes stop!
  dec a
  dec a
  dec a
  cp c
  ret c ;done  


.completeline:

  push hl

  pop hl
  ld de,(maplenght)
  add hl,de


  push hl
  ld a,224
;  call writeanytile
  ld (hl),a
  ld b,(iy+xsize)
  dec b
  dec b
.line3:
  inc hl
  ld a,225
;  call writeanytile
  ld (hl),a
  djnz .line3
  inc hl
  ld a,226
;  call writeanytile
  ld (hl),a
  pop hl


  push hl
  exx
  
  ;now find the mapadress
  ;----> tabel pointer setten
  ld d,0
  ld e,(ix+objectrelease)
  dec e 
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  ;get the mapadress where are we?? 
  
  pop de
  xor a
  sbc hl,de
  exx
  
  jp nc,.completeline

  ld de,(maplenght)
  add hl,de

;write the lava tiles in place
  ld b,(iy+xsize)
  srl b
.line4:
  ld a,173
;  call writeanytile
  ld (hl),a
  inc hl
  ld a,174
;  call writeanytile
  ld (hl),a  
  inc hl
  djnz .line4
  
  


  ret


.writelavaback:

;write the lava tiles in place
  ld b,(iy+xsize)
  srl b
.line6:
  ld a,175
;  call writeanytile
  ld (hl),a
  inc hl
  ld a,176
;  call writeanytile
  ld (hl),a
  inc hl
  djnz .line6  

  ret

;copy this table to ram on init so we can write to it
lemmyblocktable: ;the blocktable that defines all the blocks used in lemmys castle
;all positions are defined in tiles
;xsize must at least be 3!!!!

  ;xposition,       type, xsize, yposition, reset, ignore
    dw 036      : db  0,    006,     28,       0,     0
    dw 042      : db  0,    004,     24,       0,     0
    dw 046      : db  0,    006,     16,       0,     0
    dw 052      : db  0,    004,     24,       0,     0
    dw 056      : db  0,    004,     28,       0,     0
    dw 082      : db  1,    015,     15,       0,     0
     dw 138      : db  2,    004,     13,       0,     0 ;type2 is a single special block to show the world how awesome we are
    dw 124      : db  0,    024,     24,       0,     0
    dw 124      : db  1,    010,     17,       0,     0
    dw 134      : db  1,    008,     06,       0,     0
    dw 142      : db  1,    004,     17,       0,     0
    dw 170      : db  0,    008,     11,       0,     0
    dw 197      : db  1,    008,     13,       0,     0
    dw 205      : db  1,    004,     21,       0,     0
    dw 209      : db  1,    010,     13,       0,     0
    dw 219      : db  0,    008,     17,       0,     0
    dw 227      : db  0,    008,     23,       0,     0
    dw 235      : db  0,    006,     27,       0,     0
    dw 241      : db  0,    006,     29,       0,     0
    dw 295      : db  0,    012,     16,       0,     0 ;final blocks 1
    dw 295      : db  1,    012,     08,       0,     0 
    dw 319      : db  0,    012,     16,       0,     0 ;final blocks 2
    dw 319      : db  1,    012,     08,       0,     0 
    dw 342      : db  0,    012,     16,       0,     0 ;final blocks 3
    dw 342      : db  1,    012,     08,       0,     0    
    dw 000 ;end of table       

totallenghtlemmyblocktable: equ $ - lemmyblocktable

lemmyblocktablelenght:  equ 7
type:                   equ 2
xsize:                  equ 3
yposition:              equ 4
reset:                  equ 5
ignore:                 equ 6

animatelemmyblocks:

  ld a,(pillary)
  and 7
  cp 0  
  jp z,.step0
  cp 1
  jp z,.step1
  cp 2
  jp z,.step2
  cp 3
  jp z,.step3
  cp 4
  jp z,.step4
  cp 5
  jp z,.step5
  cp 6
  jp z,.step6
;  cp 7
;  jp z,.step7  
        
  
;  ret

  jp .step7

.writestep:

  push hl
  push de

  ld a,221
  ld b,12
  call updatelemmyblocks
  
  pop hl
  ld de,8*12
  add hl,de
  ex de,hl
  pop hl
  ld bc,8*12
  add hl,bc
  
  
  
  ld a,37
  ld b,6
  call updatelemmyblocks

  ret


.step0:

  ld hl,lemmyblock+(8*0)
  ld de,lemmyblockclr+(8*0)   

  jp .writestep
  
  
.step1:

  ld hl,lemmyblock+(8*18)
  ld de,lemmyblockclr+(8*18)   

  jp .writestep

.step2:

  ld hl,lemmyblock+(8*36)
  ld de,lemmyblockclr+(8*36)   

  jp .writestep

.step3:

  ld hl,lemmyblock+(8*54)
  ld de,lemmyblockclr+(8*54)   

  jp .writestep

.step4:

  ld hl,lemmyblock+(8*72)
  ld de,lemmyblockclr+(8*72)   

  jp .writestep

.step5:

  ld hl,lemmyblock+(8*90)
  ld de,lemmyblockclr+(8*90)   

  jp .writestep

.step6:

  ld hl,lemmyblock+(8*108)
  ld de,lemmyblockclr+(8*108)  

  jp .writestep
  
.step7:

  ld hl,lemmyblock+(8*126)
  ld de,lemmyblockclr+(8*126)   

  jp .writestep
  



movemagiball:

  ld a,(ix+v1) ;a type is set 0 or 1
  cp 4
  jp z,.movey
  cp -4
  jp z,.movey  

  call addv2toybject

  ld a,(ix+v1)
  and a
  jp z,.movexleft

  ld h,(ix+xobject)
  ld a,(ix+xobject+1)
  ld l,(ix+v6)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld b,0
  call add_ahl_bde
  call add_ahl_bde
  call add_ahl_bde
  call add_ahl_bde

  ld (ix+v6),l
  ld (ix+xobject),h
  ld (ix+xobject+1),a

  ret

.movexleft:


  ld h,(ix+xobject)
  ld a,(ix+xobject+1)
  ld l,(ix+v6)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld b,0
  call sub_ahl_bde
  call sub_ahl_bde
  call sub_ahl_bde
  call sub_ahl_bde

  ld (ix+v6),l
  ld (ix+xobject),h
  ld (ix+xobject+1),a

  ret


.movey:

  call addv1toxbject

  ld a,(ix+v2)
  and a
  jp z,.moveydown

  ld h,(ix+yobject)
  ld a,(ix+yobject+1)
  ld l,(ix+v6)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld b,0
  call sub_ahl_bde
  call sub_ahl_bde
  call sub_ahl_bde
  call sub_ahl_bde

  ld (ix+v6),l
  ld (ix+yobject),h
  ld (ix+yobject+1),a

  ret  


.moveydown:

  ld h,(ix+yobject)
  ld a,(ix+yobject+1)
  ld l,(ix+v6)
  ld e,(ix+v5)
  ld d,(ix+v5+1)
  ld b,0
  call add_ahl_bde
  call add_ahl_bde
  call add_ahl_bde
  call add_ahl_bde

  ld (ix+v6),l
  ld (ix+yobject),h
  ld (ix+yobject+1),a

  ret 


MovementPattern270:     ;magiball

  ld a,(ix+v4)
  and a
  jp z,.init ;set the vectors of movement towards mario

  call mariointeract ;make deadly
  
  call checkmariointeractshoot
  jp c,destroyobjectwithcloudnostar

  ld bc,0
  ld de,0
  call objecttilecheckfromrom ;find the yellow blocks and transform them
  cp 200
  jp z,.transformblock
  cp 201
  jp z,.transformblockright
  cp 202
  jp z,.transformblockhigh  
  cp 203
  jp z,.transformblockhighright   
  cp 192
  jp nc,destroyobjectwithcloudnostar

  ld a,(framecounter)
  and 3
  call z,.animate


  call movemagiball ;handle all movements from here including the statics


  ret

.transformblockhighright:

  dec hl

.transformblockhigh:

  ld de,(maplenght)
  xor a
  sbc hl,de

  jp .transformblock

.transformblockright:

  dec hl

.transformblock:


  ;mapadress is allready in HL ;Write back bgk from list?? is possible if the map is small enough then we can copy the map to ram
  xor a
  call writeanytile
  inc hl
  call writeanytile
  dec hl
  ld de,(maplenght)
  add hl,de
  call writeanytile
  inc hl
  call writeanytile  


;spawn random object
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
;  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
;  inc de
;  inc de
  


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  
  ld a,(framecounter)
  and 3
  and a
  call z,.setshroom
  dec a
  call z,.setthimp
  dec a
  call z,.setkoopa  
  dec a
  call z,.setthimp
   
  ;ld hl,62
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),0                ;set object number (14 is red mushroom)

  call  activateobject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld a,(ix+movepat)
  cp 16
  call z,.setshroom2

  pop ix


  jp destroyobjectwithcloudnostar
  
  ;ret


.setshroom2:

  ld (ix+v4),50
  ld (ix+v1),2
  ld (ix+v2),0
  ld (ix+objectfp),0

  ret

.setshroom:

  ld l,14

  ret
  
.setthimp:

  ld l,218

  ret

.setkoopa:

  ld l,62

  ret

;set direction follow mario at all times. 
;Thank you grauw for your kind help on the fixed point theory and calculation of movement code.
.init:



  inc (ix+v4)

  call getmariodistance ;horizontal
  call getmarioheighty ;vertical
  sub c
  jp nc,.else

;dy = vertical / horizontal
  
  call getmarioheighty
  ld h,a
  ld l,0
  push hl
  call getmariodistance
;  ld c,a
;  pop hl
;
;
;  push hl
  push bc
  ld (ix+v1),-4 ;dX
  call getmarioleftright
  call c,.setxright   
  ld (ix+v2),0 ;dY static type
  call getmarioheighty ;vertical
  pop bc
  pop hl  
  jp c,.store
  ;below object
  ld (ix+v2),1 ;dY static type  

  jp .store

.else:

;dx = horizontal / vertical

  call getmariodistance
  ld h,a
  ld l,0
  push hl
  call getmarioheighty
  ld c,a
;  pop hl
;
;  push hl
  push bc
  ld (ix+v1),0 ;dX static type
  call getmarioleftright
  call c,.setxtyperight  
  ld (ix+v2),4 ;dY
  call getmarioheighty ;vertical
  pop bc
  pop hl  
  jp c,.store

  ld (ix+v2),-4 ;dY

.store:
  
  call HL_Div_C  
  ;in hl the division number
  ld (ix+v5),l ;dX
  ld (ix+v5+1),h
  ld (ix+v6),0 ;decimal for X


  ret

.setxtyperight:

  ld (ix+v1),1

  ret


.setxright:

  ld (ix+v1),4

  ret

;when v1 or 2 is set to static:
;V1
;0 = left
;1 = right
;V2
;0 = down
;1 = up


.animate:
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  
  
  ld a,(ix+v3)
  inc a
  and 7
  ld (ix+v3),a
  dec a
  jp z,.position1
  dec a
  jp z,.position2
  dec a
  jp z,.position3
  dec a
  jp z,.position4
  dec a
  jp z,.position5  
  dec a
  call z,.setzero


  ld    hl,coloraddressmagiball1  ;color address
  ld    bc,characteraddressmagiball1;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position1:

  ld    hl,coloraddressmagiball1  ;color address
  ld    bc,characteraddressmagiball2;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position2:

  ld    hl,coloraddressmagiball2  ;color address
  ld    bc,characteraddressmagiball3;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

.position3:

  ld    hl,coloraddressmagiball2  ;color address
  ld    bc,characteraddressmagiball4;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset
 
.position4:

  ld    hl,coloraddressmagiball3  ;color address
  ld    bc,characteraddressmagiball5;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset
  
.position5:

  ld    hl,coloraddressmagiball3  ;color address
  ld    bc,characteraddressmagiball6;character address
  ld    de,offsetshell
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset   


.setzero:

  xor a
  ld (ix+v3),0


  ret



;  ret


;make sure that only 1 magikoopa can be active at the same time; use the magikoopaactive variable as a global

MovementPattern269:     ;magikoopa

;magikoopa is a static appearing character that changes direction on basis of mario's movement
  ld (ix+v3),0
  call getmarioleftright
  jp nc,.start  
  ld (ix+v3),1 ;use bit 0 for left right
.start:

  call mariointeract
  jp c,.nextframe

  call objectinteract
  cp 3
  jp z,.otherobjectdeathjump
  cp 2
  jp z,.otherobjectdeathjump

  ld a,(ix+v4)
  dec a
  jp z,.spawn
  dec a
  jp z,.fire
  dec a
  jp z,.shoot
  dec a
  jp z,.dissapear
  dec a
  jp z,.wait

  inc (ix+v7)
  ld a,(ix+v7)
  cp 30 ;shorter??
  ret c
  
  call getmariodistance ;need a certain distance
  ld a,c
  cp 120
  ret nc

  ld bc,48-8
  ld de,8
  call objecttilecheckfromrom ;prevent spawning in mid air
  cp 192
  ret c

 
  ld a,(magikoopaactive) ;one is allready active
  and a
  ret nz
  ld a,1
  ld (magikoopaactive),a
  
  ld b,8
  call increasespritecount
  
  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
;start spawn cycle  
  inc (ix+v4)
  ld (ix+v7),0
  ld (ix+deadly),1

;set first step sprite left
  ld    hl,coloraddressmagikoopasleft1  ;color address
  ld    bc,characteraddressmagikoopasleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v3)
  and a
  ret z
  
;set first step sprite
  ld    hl,coloraddressmagikoopasright1  ;color address
  ld    bc,characteraddressmagikoopasright1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  jp    setcharcolandoffset

 ; ret


.otherobjectdeathjump:

  ld a,(ix+deadly)
  cp 3
  ret z

  jp otherobjectdeathjump

.nextframe:

  ld a,(ix+deadly)
  cp 3
  ret z
 
  jp otherobjectdeathjump

.wait:

  xor a
  ld (magikoopaactive),a ;deactivate
  ld (ix+v3),0
  ld (ix+v4),0
  ld (ix+v7),0
  ld (ix+deadly),3
  call removeobjectspat
  call removefromsprlst
  ld (ix+amountspr),0
  


  ret


.dissapear:

  inc (ix+v7)
  
  ld a,(ix+v3)
  and a
  jp nz,.dissapearright  
  
  
  ld a,(ix+v7)
  cp 15
  ret c


  ld a,(ix+v7)
  cp 3+15 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikoopasleft1  ;color address
  ld    bc,characteraddressmagikoopasleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasleft1  ;color address
  ld    bc,characteraddressmagikoopasleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasleft3  ;color address
  ld    bc,characteraddressmagikoopasleft3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

  ld a,(ix+v7)
  cp 12+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasleft1  ;color address
  ld    bc,emptysprite6c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  call    setcharcolandoffset
  
  inc (ix+v4)
  ld (ix+v7),0
  
  ret


.dissapearright:
  
  ld a,(ix+v7)
  cp 15
  ret c


  ld a,(ix+v7)
  cp 3+15 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikoopasright1  ;color address
  ld    bc,characteraddressmagikoopasright1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasright1  ;color address
  ld    bc,characteraddressmagikoopasright1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasright3  ;color address
  ld    bc,characteraddressmagikoopasright3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

  ld a,(ix+v7)
  cp 12+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasright1  ;color address
  ld    bc,emptysprite6c;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock4
  call    setcharcolandoffset
  
  inc (ix+v4)
  ld (ix+v7),0
  
  ret




.shoot:

  ;object240
  ;create the fireball
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  inc hl
  ld e,(ix+yobject)
  ld d,(ix+yobject+1) 
  srl   d
  rr    e                       ;/2
  srl   d
  rr    e                       ;/4
  srl   d
  rr    e                       ;/8
  inc de
  inc de
  


  push ix

  ld    iy,CustomObject
  ld    (iy),available ;always in the back
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld hl,256
  ld    (iy+5),l                ;set object number (14 is red mushroom)
  ld    (iy+6),h                ;set object number (14 is red mushroom)

  call  activateobject

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)
  ld hl,270
  ld (ix+movepat),l
  ld (ix+movepat+1),h
  ld (ix+deadly),2

  ld    hl,coloraddressmagiball1  ;color address
  ld    bc,characteraddressmagiball1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetendbarcoverup
  ld    a,ingamespritesblock5
  call    setcharcolandoffset
  
;  push ix
;  pop iy

  pop ix
  
  inc (ix+v4)
  ld (ix+v7),0
 
  call Playsfx.fireball
  
  ret  
  


.fire:

  inc (ix+v7)
  
  ld a,(ix+v3)
  and a
  jp nz,.fireright  
  
  
  ld a,(ix+v7)
  cp 15
  ret c

  
 ; ld a,(ix+v7)
  cp 3+15 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikoopaleft2  ;color address
  ld    bc,characteraddressmagikoopaleft2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopaleft3  ;color address
  ld    bc,characteraddressmagikoopaleft3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopaleft4  ;color address
  ld    bc,characteraddressmagikoopaleft4;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

  inc (ix+v4)
  ld (ix+v7),0
  
  ret


.fireright:

  
  ld a,(ix+v7)
  cp 15
  ret c

  
 ; ld a,(ix+v7)
  cp 3+15 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikooparight2  ;color address
  ld    bc,characteraddressmagikooparight2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikooparight3  ;color address
  ld    bc,characteraddressmagikooparight3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9+15 ;shorter??
  ret c

  ld    hl,coloraddressmagikooparight4  ;color address
  ld    bc,characteraddressmagikooparight4;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

;  inc (ix+v4)
;  ld (ix+v7),0
 jp .incv4
  
;  ret



.spawn: ;spawn in steps of 3 use the default timer to set the correct sprites use v3 to get the left or right data

  inc (ix+v7)
  ld a,(ix+v7)
  cp 1
  jp z,Playsfx.spinjump
  
  ld a,(ix+v3)
  and a
  jp nz,.spawnright
  
  ld a,(ix+v7)
  cp 3 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikoopasleft2  ;color address
  ld    bc,characteraddressmagikoopasleft2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasleft3  ;color address
  ld    bc,characteraddressmagikoopasleft3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasleft3  ;color address
  ld    bc,characteraddressmagikoopasleft3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

  ld a,(ix+v7)
  cp 12 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopaleft1  ;color address
  ld    bc,characteraddressmagikoopaleft1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset
  
;  inc (ix+v4)
;  ld (ix+v7),0
 jp .incv4
  
;  ret


.spawnright:

  ld a,(ix+v7)
  cp 3 ;shorter??
  ret c

  ld    (ix+updatespr?),2       ;update sprite (this takes 2 frames)

  ld    hl,coloraddressmagikoopasright2  ;color address
  ld    bc,characteraddressmagikoopasright2;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 6 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasright3  ;color address
  ld    bc,characteraddressmagikoopasright3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset

  ld a,(ix+v7)
  cp 9 ;shorter??
  ret c

  ld    hl,coloraddressmagikoopasright3  ;color address
  ld    bc,characteraddressmagikoopasright3;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset  

  ld a,(ix+v7)
  cp 12 ;shorter??
  ret c

  ld    hl,coloraddressmagikooparight1  ;color address
  ld    bc,characteraddressmagikooparight1;use empty char adress ;spriteaddrex590c  ;character address
  ld    de,offsetthwomp
  ld    a,ingamespritesblock5
  call    setcharcolandoffset
  
;  inc (ix+v4)
;  ld (ix+v7),0
 jp .incv4
 
;  ret


.incv4:

  inc (ix+v4)
  ld (ix+v7),0    
  ret

