;000=YosHou,001=YoIsl1,002=YoIsl2,003=YoIsl3,004=YoIsl4,005=IggyCa,006=YeSwPa,007=DoPla1
;first 2 bytes are to check if this level is available, refers to bytenumber,bitnumber of levelavailabilitylist
;cursorpressed(0=up,1=down,2=left,3=right) where does it lead to ?, what is your new location after the movement
LocationYosHou: db 0,7
                dw 00,00
                dw 00,00
                dw WM_marioFromYosHoutoYoIsl1,LocationYoIsl1
                dw WM_marioFromYosHoutoYoIsl2,LocationYoIsl2
                db 000 ;locationnumber used on level exit
                db "YOSHI'S HOUSE",0

LocationYoIsl1: db 0,6
                dw WM_marioFromYoIsl1toYelSwP,LocationYelSwP
                dw 00,00
                dw 00,00
                dw WM_marioFromYoIsl1toYosHou,LocationYosHou
                db 001 ;locationnumber used on level exit
                db "YOSHI'S ISLAND 1",0

LocationYoIsl2: db 0,5
                dw WM_marioFromYoIsl2toYoIsl3,LocationYoIsl3
                dw 00,00
                dw WM_marioFromYoIsl2toYosHou,LocationYosHou
                dw 00,00
                db 002 ;locationnumber used on level exit
                db "YOSHI'S ISLAND 2",0

LocationYelSwP: db 0,4
                dw 00,00
                dw WM_marioFromYelSwPtoYoIsl1,LocationYoIsl1
                dw 00,00
                dw 00,00
                db 003 ;locationnumber used on level exit
                db "YELLOW SWITCH PALACE",0

LocationYoIsl3: db 0,3
                dw 00,00
                dw WM_marioFromYoIsl3toYoIsl2,LocationYoIsl2
                dw 00,00
                dw WM_marioFromYoIsl3toYoIsl4,LocationYoIsl4
                db 004 ;locationnumber used on level exit
                db "YOSHI'S ISLAND 3",0

LocationYoIsl4: db 0,2
                dw WM_marioFromYoIsl4toIggyCa,LocationIggyCa
                dw WM_marioFromYoIsl4toYoIsl3,LocationYoIsl3
                dw 00,00
                dw 00,00
                db 005 ;locationnumber used on level exit
                db "YOSHI'S ISLAND 4",0

LocationIggyCa: db 0,1
                dw WM_marioFromIggyCatoDoPla1,LocationDoPla1
                dw 00,00
                dw 00,00
                dw WM_marioFromIggyCatoYoIsl4,LocationYoIsl4
                db 006 ;locationnumber used on level exit
                db "#1 IGGY'S CASTLE",0

LocationDoPla1: db 0,0
                dw WM_marioFromDoPla1toDoSec1,LocationDoSec1
                dw WM_marioFromDoPla1toIggyCa,LocationIggyCa
                dw WM_marioFromDoPla1toDoPla2,LocationDoPla2
                dw 00,00
                db 007 ;locationnumber used on level exit
                db "DONUT PLAINS 1",0

LocationDoPla2: db 1,7
                dw WM_marioFromDoPla2toDoGhos,DoPla2toDoGhos
                dw WM_marioFromDoPla2toDoPla1,LocationDoPla1
                dw WM_marioFromDoPla2toGrSwPa,LocationGrSwPa
                dw 00,00
                db 008 ;locationnumber used on level exit
                db "DONUT PLAINS 2",0

LocationDoSec1: db 1,6
                dw WM_marioFromDoSec1toDoGhos,DoSec1toDoGhos
                dw WM_marioFromDoSec1toDoPla1,LocationDoPla1
                dw 00,00
                dw WM_marioFromDoSec1toDoSeGh,LocationDoSeGh
                db 009 ;locationnumber used on level exit
                db "DONUT SECRET 1",0

LocationGrSwPa: db 1,5
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromGrSwPatoDoPla2,LocationDoPla2
                db 010 ;locationnumber used on level exit
                db "GREEN SWITCH PALACE",0

TopSectoDoGhos:
DoPla2toDoGhos: db 1,4
                dw WM_marioFromDoGhostoTopSec,LocationTopSec
                dw WM_marioFromDoGhostoDoSec1,DoGhostoDoSec1
                dw WM_marioFromDoGhostoDoPla2,DoGhostoDoPla2
                dw WM_marioFromDoGhostoDoPla3,DoGhostoDoPla3
                db 011 ;locationnumber used on level exit
                db "DONUT GHOST HOUSE",0

DoGhostoDoPla2: db 1,4
                dw WM_marioFromDoPla2toDoGhos,DoPla2toDoGhos
                dw WM_marioFromDoPla2toDoPla1,LocationDoPla1
                dw WM_marioFromDoPla2toGrSwPa,LocationGrSwPa
                dw 00,00
                db 008 ;locationnumber used on level exit
                db "DONUT PLAINS 2",0

DoSec1toDoGhos: db 1,3
                dw WM_marioFromDoGhostoTopSec,LocationTopSec
                dw WM_marioFromDoGhostoDoSec1,DoGhostoDoSec1
                dw WM_marioFromDoGhostoDoPla2,DoGhostoDoPla2
                dw WM_marioFromDoGhostoDoPla3,DoGhostoDoPla3
                db 011 ;locationnumber used on level exit
                db "DONUT GHOST HOUSE",0

DoGhostoDoSec1: db 1,3
                dw WM_marioFromDoSec1toDoGhos,DoSec1toDoGhos
                dw WM_marioFromDoSec1toDoPla1,LocationDoPla1
                dw 00,00
                dw WM_marioFromDoSec1toDoSeGh,LocationDoSeGh
                db 009 ;locationnumber used on level exit
                db "DONUT SECRET 1",0
                
LocationDoSeGh: db 1,2
                dw WM_marioFromDoSeGhtoDoSec1,LocationDoSec1
                dw WM_marioFromDoSeGhtoStarA1,LocationStarA1
                dw 00,00
                dw WM_marioFromDoSeGhtoTubeG1,LocationTubeG1
                db 012 ;locationnumber used on level exit
                db "DONUT SECRET HOUSE",0
                
LocationStarA1: db 1,1
                dw WM_marioFromStarA1toDoSeGh,LocationDoSeGh
                dw 00,00
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

LocationTubeG1: db 1,0
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeG1toDoSeGh,LocationDoSeGh
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationTubeG2: db 1,0
                dw WM_marioFromTubeG2toDoSec2,LocationDoSec2
                dw 00,00
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationDoSec2: db 1,0
                dw 00,00
                dw WM_marioFromDoSec2toTubeG2,LocationTubeG2
                dw 00,00
                dw WM_marioFromDoSec2toTubeH2,LocationTubeH2
                db 013 ;locationnumber used on level exit
                db "DONUT SECRET 2",0

LocationTopSec: db 2,7
                dw 00,00
                dw WM_marioFromTopSectoDoGhos,TopSectoDoGhos
                dw 00,00
                dw 00,00
                db 014 ;locationnumber used on level exit
                db "TOP SECRET AREA",0

DoPla4toDoPla3: db 0,0  ;this should always be accessable when coming from DoPla4 
                dw 00,00
                dw WM_marioFromDoPla3toTubeH1,LocationTubeH1
                dw WM_marioFromDoPla3toDoGhos,DoPla3toDoGhos
                dw WM_marioFromDoPla3toDoPla4,LocationDoPla4
                db 015 ;locationnumber used on level exit
                db "DONUT PLAINS 3",0
                                
DoGhostoDoPla3: db 2,6
                dw 00,00
                dw WM_marioFromDoPla3toTubeH1,LocationTubeH1
                dw WM_marioFromDoPla3toDoGhos,DoPla3toDoGhos
                dw WM_marioFromDoPla3toDoPla4,LocationDoPla4
                db 015 ;locationnumber used on level exit
                db "DONUT PLAINS 3",0

DoPla3toDoGhos: db 2,6
                dw WM_marioFromDoGhostoTopSec,LocationTopSec
                dw WM_marioFromDoGhostoDoSec1,DoGhostoDoSec1
                dw WM_marioFromDoGhostoDoPla2,DoGhostoDoPla2
                dw WM_marioFromDoGhostoDoPla3,DoGhostoDoPla3
                db 011 ;locationnumber used on level exit
                db "DONUT GHOST HOUSE",0

LocationTubeH1: db 2,5
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeH1toDoPla3,TubeH1toDoPla3
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationTubeH2: db 2,5
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeH2toDoSec2,LocationDoSec2
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

TubeH1toDoPla3: db 2,5
                dw 00,00
                dw WM_marioFromDoPla3toTubeH1,LocationTubeH1
                dw WM_marioFromDoPla3toDoGhos,DoPla3toDoGhos
                dw WM_marioFromDoPla3toDoPla4,LocationDoPla4
                db 015 ;locationnumber used on level exit
                db "DONUT PLAINS 3",0

MortCatoDoPla4:
LocationDoPla4: db 2,4
                dw WM_marioFromDoPla4toDoPla3,DoPla4toDoPla3
                dw 00,00
                dw 00,00
                dw WM_marioFromDoPla4toMortCa,LocationMortCa
                db 016 ;locationnumber used on level exit
                db "DONUT PLAINS 4",0

LocationMortCa: db 2,3
                dw WM_marioFromMortCatoVaDom1,LocationVaDom1
                dw 00,00
                dw WM_marioFromMortCatoDoPla4,LocationDoPla4
                dw 00,00
                db 017 ;locationnumber used on level exit
                db "#2 MORTON'S CASTLE",0

LocationVaDom1: db 2,2
                dw 00,00
                dw WM_marioFromVaDom1toMortCa,LocationMortCa
                dw WM_marioFromVaDom1toVaSec1,LocationVaSec1
                dw WM_marioFromVaDom1toVaDom2,LocationVaDom2
                db 018 ;locationnumber used on level exit
                db "VANILLA DOME 1",0

;deze heb ik aangepast zodat je altijd kunt lopen van StarW1 terug naar StarA2
;LocationStarA2: db 3,5
LocationStarA2: db 0,0
                dw WM_marioFromStarA2toStarW1,LocationStarW1
                dw 00,00
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

LocationVaDom2: db 3,4
                dw WM_marioFromVaDom2toVaGhos,LocationVaGhos
                dw WM_marioFromVaDom2toVaDom1,LocationVaDom1
                dw 00,00
                dw WM_marioFromVaDom2toReSwPa,LocationReSwPa
                db 019 ;locationnumber used on level exit
                db "VANILLA DOME 2",0
                
LocationVaSec1: db 3,3
                dw WM_marioFromVaSec1toTubeI2,LocationTubeI2
                dw WM_marioFromVaSec1toVaDom1,LocationVaDom1
                dw WM_marioFromVaSec1toStarB1,VaSec1toStarB1
                dw 00,00
                db 020 ;locationnumber used on level exit
                db "VANILLA SECRET 1",0

StarB1toVaSec1: db 3,1
                dw WM_marioFromVaSec1toTubeI2,LocationTubeI2
                dw WM_marioFromVaSec1toVaDom1,LocationVaDom1
                dw WM_marioFromVaSec1toStarB1,VaSec1toStarB1
                dw 00,00
                db 020 ;locationnumber used on level exit
                db "VANILLA SECRET 1",0

LocationReSwPa: db 3,2
                dw WM_marioFromReSwPatoVaDom2,LocationVaDom2
                dw 00,00
                dw 00,00
                dw 00,00
                db 021 ;locationnumber used on level exit
                db "RED SWITCH PALACE",0

LocationStarB1:
VaSec1toStarB1: db 3,1
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromStarB1toVaSec1,StarB1toVaSec1
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

LocationTubeI1: db 0,0
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeI1toVaSec2,LocationVaSec2
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationTubeI2: db 3,0
                dw 00,00
                dw WM_marioFromTubeI2toVaSec1,LocationVaSec1
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationVaGhos: db 4,7
                dw 00,00
                dw WM_marioFromVaGhostoVaDom2,LocationVaDom2
                dw 00,00
                dw WM_marioFromVaGhostoVaDom3,LocationVaDom3
                db 022 ;locationnumber used on level exit
                db "VANILLA GHOST HOUSE",0

LocationVaDom3: db 4,6
                dw WM_marioFromVaDom3toVaGhos,LocationVaGhos
                dw WM_marioFromVaDom3toVaDom4,LocationVaDom4
                dw 00,00
                dw 00,00
                db 023 ;locationnumber used on level exit
                db "VANILLA DOME 3",0

LocationVaDom4: db 4,5
                dw WM_marioFromVaDom4toVaDom3,LocationVaDom3
                dw WM_marioFromVaDom4toLemmCa,LocationLemmCa
                dw 00,00
                dw 00,00
                db 024 ;locationnumber used on level exit
                db "VANILLA DOME 4",0

LocationLemmCa: db 4,4
                dw WM_marioFromLemmCatoVaDom4,LocationVaDom4
                dw WM_marioFromLemmCatoTubeJ2,LocationTubeJ2
                dw 00,00
                dw 00,00
                db 025 ;locationnumber used on level exit
                db "#3 LEMMY'S CASTLE",0

LocationTubeJ2: db 4,3
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeJ2toLemmCa,LocationLemmCa
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

;opsplitsen
StarW1toStarB2: db 10,7
                dw 00,00
                dw WM_marioFromStarB2toStarW1,StarB2toStarW1
                dw 00,00
                dw WM_marioFromStarB2toStarW2,LocationStarW2
                db 065 ;locationnumber used on level exit
                db "STAR ROAD",0
LocationStarB2: db 4,1
                dw 00,00
                dw WM_marioFromStarB2toStarW1,StarB2toStarW1
                dw 00,00
                dw WM_marioFromStarB2toStarW2,LocationStarW2
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0
StarW2toStarB2: db 0,0
                dw 00,00
                dw WM_marioFromStarB2toStarW1,StarB2toStarW1
                dw 00,00
                dw WM_marioFromStarB2toStarW2,LocationStarW2
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

LocationVaSec2: db 0,0
                dw 00,00
                dw 00,00
                dw WM_marioFromVaSec2toTubeI1,LocationTubeI1
                dw WM_marioFromVaSec2toVaSec3,LocationVaSec3
                db 026 ;locationnumber used on level exit
                db "VANILLA SECRET 2",0

LocationVaSec3: db 4,1
                dw 00,00
                dw 00,00
                dw WM_marioFromVaSec3toVaSec2,LocationVaSec2
                dw WM_marioFromVaSec3toVaFort,LocationVaFort
                db 027 ;locationnumber used on level exit
                db "VANILLA SECRET 3",0

LocationVaFort: db 4,0
                dw 00,00
                dw 00,00
                dw WM_marioFromVaForttoVaSec3,LocationVaSec3
                dw WM_marioFromVaForttoButBr1,LocationButBr1
                db 028 ;locationnumber used on level exit
                db "VANILLA FORTRESS",0

LocationButBr1: db 5,7
                dw 00,00
                dw 00,00
                dw WM_marioFromButBr1toVaFort,LocationVaFort
                dw WM_marioFromButBr1toButBr2,LocationButBr2
                db 029 ;locationnumber used on level exit
                db "BUTTER BRIDGE 1",0

LocationButBr2: db 5,6
                dw 00,00
                dw 00,00
                dw WM_marioFromButBr2toButBr1,LocationButBr1
                dw WM_marioFromButBr2toLudwCa,ButBr2toLudwCa
                db 030 ;locationnumber used on level exit
                db "BUTTER BRIDGE 2",0

ButBr2toLudwCa: db 5,5
                dw 00,00
                dw WM_marioFromLudwCatoCookie,LudwCatoCookie
                dw WM_marioFromLudwCatoButBr2,LocationButBr2
                dw WM_marioFromLudwCatoForIl1,LudwCatoForIl1
                db 031 ;locationnumber used on level exit
                db "#4 LUDWIG CASTLE",0

LudwCatoButBr2: db 5,5
                dw 00,00
                dw 00,00
                dw WM_marioFromButBr2toButBr1,LocationButBr1
                dw WM_marioFromButBr2toLudwCa,ButBr2toLudwCa
                db 030 ;locationnumber used on level exit
                db "BUTTER BRIDGE 2",0

LocationTubeJ1: db 0,0
                dw WM_marioFromTubeJ1toCheese,LocationCheese
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeJ1toCheese,LocationCheese
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationCheese: db 0,0
                dw 00,00
                dw WM_marioFromCheesetoSodaLa,LocationSodaLa
                dw WM_marioFromCheesetoTubeJ1,LocationTubeJ1
                dw WM_marioFromCheesetoCookie,LocationCookie
                db 032 ;locationnumber used on level exit
                db "CHEESE BRIDGE AREA",0

LocationSodaLa: db 5,4
                dw WM_marioFromSodaLatoCheese,LocationCheese
                dw WM_marioFromSodaLatoStarC1,LocationStarC1 ;hiero
                dw 00,00
                dw 00,00
                db 033 ;locationnumber used on level exit
                db "SODA LAKE",0

LocationCookie: db 5,3
                dw 00,00
                dw WM_marioFromCookietoLudwCa,CookietoLudwCa
                dw WM_marioFromCookietoCheese,LocationCheese
                dw 00,00
                db 034 ;locationnumber used on level exit
                db "COOKIE MOUNTAIN",0

LudwCatoCookie: db 5,2
                dw 00,00
                dw WM_marioFromCookietoLudwCa,CookietoLudwCa
                dw WM_marioFromCookietoCheese,LocationCheese
                dw 00,00
                db 034 ;locationnumber used on level exit
                db "COOKIE MOUNTAIN",0

CookietoLudwCa: db 5,2
                dw 00,00
                dw WM_marioFromLudwCatoCookie,LocationCookie
                dw WM_marioFromLudwCatoButBr2,LudwCatoButBr2
                dw WM_marioFromLudwCatoForIl1,LudwCatoForIl1
                db 031 ;locationnumber used on level exit
                db "#4 LUDWIG CASTLE",0

LudwCatoForIl1: db 5,1
                dw WM_marioFromForIl1toLudwCa,LudwCatoForIl1
                dw 00,00
                dw WM_marioFromForIl1toFoGhos,ForIl1toFoGhos
                dw WM_marioFromForIl1toForIl2,ForIl1toForIl2
                db 035 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 1",0
FoGhostoForIl1: db 5,0
                dw WM_marioFromForIl1toLudwCa,CookietoLudwCa
                dw 00,00
                dw WM_marioFromForIl1toFoGhos,ForIl1toFoGhos
                dw WM_marioFromForIl1toForIl2,ForIl1toForIl2
                db 035 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 1",0
ForIl2toForIl1: db 6,7
                dw WM_marioFromForIl1toLudwCa,CookietoLudwCa
                dw 00,00
                dw WM_marioFromForIl1toFoGhos,ForIl1toFoGhos
                dw WM_marioFromForIl1toForIl2,ForIl1toForIl2
                db 035 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 1",0 
                                
ForIl1toFoGhos: db 5,0
                dw 00,00
                dw WM_marioFromFoGhostoForIl3,FoGhostoForIl3
                dw WM_marioFromFoGhostoForIl4,FoGhostoForIl4
                dw WM_marioFromFoGhostoForIl1,FoGhostoForIl1
                db 036 ;locationnumber used on level exit
                db "FOREST GHOST HOUSE",0       
ForIl3toFoGhos: db 6,4
                dw 00,00
                dw WM_marioFromFoGhostoForIl3,FoGhostoForIl3
                dw WM_marioFromFoGhostoForIl4,FoGhostoForIl4
                dw WM_marioFromFoGhostoForIl1,FoGhostoForIl1
                db 036 ;locationnumber used on level exit
                db "FOREST GHOST HOUSE",0    
ForIl4toFoGhos: db 6,3
                dw 00,00
                dw WM_marioFromFoGhostoForIl3,FoGhostoForIl3
                dw WM_marioFromFoGhostoForIl4,FoGhostoForIl4
                dw WM_marioFromFoGhostoForIl1,FoGhostoForIl1
                db 036 ;locationnumber used on level exit
                db "FOREST GHOST HOUSE",0   

ForIl2toForIl4: db 6,2
                dw WM_marioFromForIl4toFoGhos,ForIl4toFoGhos
                dw WM_marioFromForIl4toForSec,ForIl4toForSec
                dw 00,00
                dw WM_marioFromForIl4toForIl2,ForIl4toForIl2
                db 037 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 4",0  
FoGhostoForIl4: db 6,3
                dw WM_marioFromForIl4toFoGhos,ForIl4toFoGhos
                dw WM_marioFromForIl4toForSec,ForIl4toForSec
                dw 00,00
                dw WM_marioFromForIl4toForIl2,ForIl4toForIl2
                db 037 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 4",0  
ForSectoForIl4: db 6,1
                dw WM_marioFromForIl4toFoGhos,ForIl4toFoGhos
                dw WM_marioFromForIl4toForSec,ForIl4toForSec
                dw 00,00
                dw WM_marioFromForIl4toForIl2,ForIl4toForIl2
                db 037 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 4",0   

ForIl1toForIl2: db 6,7
                dw WM_marioFromForIl2toForIl1,LudwCatoForIl1
                dw WM_marioFromForIl2toForIl3,ForIl2toForIl3
                dw WM_marioFromForIl2toForIl4,ForIl2toForIl4
                dw WM_marioFromForIl2toBlSWPa,ForIl2toBlSwPa
                db 038 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 2",0    
ForIl3toForIl2: db 6,6
                dw WM_marioFromForIl2toForIl1,LudwCatoForIl1
                dw WM_marioFromForIl2toForIl3,ForIl2toForIl3
                dw WM_marioFromForIl2toForIl4,ForIl2toForIl4
                dw WM_marioFromForIl2toBlSWPa,ForIl2toBlSwPa
                db 038 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 2",0    
BlSwPatoForIl2: db 6,5
                dw WM_marioFromForIl2toForIl1,LudwCatoForIl1
                dw WM_marioFromForIl2toForIl3,ForIl2toForIl3
                dw WM_marioFromForIl2toForIl4,ForIl2toForIl4
                dw WM_marioFromForIl2toBlSWPa,ForIl2toBlSwPa
                db 038 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 2",0  
ForIl4toForIl2: db 6,2
                dw WM_marioFromForIl2toForIl1,LudwCatoForIl1
                dw WM_marioFromForIl2toForIl3,ForIl2toForIl3
                dw WM_marioFromForIl2toForIl4,ForIl2toForIl4
                dw WM_marioFromForIl2toBlSWPa,ForIl2toBlSwPa
                db 038 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 2",0   

LocationBlSwPa:
ForIl2toBlSwPa: db 6,5
                dw 00,00
                dw 00,00
                dw WM_marioFromBlSWPatoForIl2,BlSwPatoForIl2
                dw 00,00
                db 039 ;locationnumber used on level exit
                db "BLUE SWITCH PALACE",0   

ForIl2toForIl3: db 6,6
                dw 00,00
                dw WM_marioFromForIl3toRoysCa,ForIl3toRoysCa
                dw WM_marioFromForIl3toFoGhos,ForIl3toFoGhos
                dw WM_marioFromForIl3toForIl2,ForIl3toForIl2
                db 040 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 3",0   
FoGhostoForIl3: db 6,4
                dw 00,00
                dw WM_marioFromForIl3toRoysCa,ForIl3toRoysCa
                dw WM_marioFromForIl3toFoGhos,ForIl3toFoGhos
                dw WM_marioFromForIl3toForIl2,ForIl3toForIl2
                db 040 ;locationnumber used on level exit
                db "FOREST OF ILLUSION 3",0     

ForIl4toForSec: db 6,1
                dw WM_marioFromForSectoForIl4,ForSectoForIl4
                dw WM_marioFromForSectoForFor,ForSectoForFor
                dw 00,00
                dw 00,00
                db 041 ;locationnumber used on level exit
                db "FOREST SECRET AREA",0  
ForFortoForSec: db 6,0
                dw WM_marioFromForSectoForIl4,ForSectoForIl4
                dw WM_marioFromForSectoForFor,ForSectoForFor
                dw 00,00
                dw 00,00
                db 041 ;locationnumber used on level exit
                db "FOREST SECRET AREA",0  
                
ForSectoForFor: db 6,0
                dw 00,00
                dw 00,00
                dw WM_marioFromForFortoStarD1,ForFortoStarD1
                dw WM_marioFromForFortoForSec,ForFortoForSec
                db 042 ;locationnumber used on level exit
                db "FOREST FORTRESS",0  
StarD1toForFor: db 7,7
                dw 00,00
                dw 00,00
                dw WM_marioFromForFortoStarD1,ForFortoStarD1
                dw WM_marioFromForFortoForSec,ForFortoForSec
                db 042 ;locationnumber used on level exit
                db "FOREST FORTRESS",0  

LocationStarD1: db 0,0
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromStarD1toForFor,StarD1toForFor
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0
ForFortoStarD1: db 7,7
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromStarD1toForFor,ForSectoForFor
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

ForIl3toRoysCa: db 7,6
                dw 00,00
                dw WM_marioFromRoysCatoChoIs1,LocationChoIs1
                dw 00,00
                dw WM_marioFromRoysCatoForIl3,ForIl2toForIl3
                db 043 ;locationnumber used on level exit
                db "#5 ROY'S CASTLE",0  

LocationChoIs1: db 7,5
                dw WM_marioFromChoIs1toRoysCa,ForIl3toRoysCa
                dw 00,00
                dw WM_marioFromChoIs1toChoGho,LocationChoGho
                dw 00,00
                db 044 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 1",0  

LocationChoGho: db 7,4
                dw 00,00
                dw WM_marioFromChoGhotoChoIs2,LocationChoIs2
                dw 00,00
                dw WM_marioFromChoGhotoChoIs1,LocationChoIs1
                db 045 ;locationnumber used on level exit
                db "CHOCO GHOST HOUSE",0  

LocationChoIs2: db 7,3
                dw WM_marioFromChoIs2toChoGho,LocationChoGho
                dw WM_marioFromChoIs2toTubeK1,LocationTubeK1
                dw WM_marioFromChoIs2toChoIs3,LocationChoIs3
                dw 00,00
                db 046 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 2",0   

LocationTubeK1: db 7,2
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeK1toChoIs2,LocationChoIs2
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

LocationTubeK2: db 7,2
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeK2toChoSec,LocationChoSec
                dw 00,00
                db 00 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap

TubeL2toChoSec: db 10,3
                dw 00,00
                dw 00,00
                dw WM_marioFromChoSectoTubeL2,LocationTubeL2
                dw WM_marioFromChoSectoTubeK2,LocationTubeK2
                db 047 ;locationnumber used on level exit
                db "CHOCOLATE SECRET",0  
LocationChoSec: db 0,0
                dw 00,00
                dw 00,00
                dw WM_marioFromChoSectoTubeL2,LocationTubeL2
                dw WM_marioFromChoSectoTubeK2,LocationTubeK2
                db 047 ;locationnumber used on level exit
                db "CHOCOLATE SECRET",0  

LocationChoIs3: db 7,1
                dw 00,00
                dw WM_marioFromChoIs3toCircle,LocationCircle
                dw WM_marioFromChoIs3toChoFor,LocationChoFor
                dw WM_marioFromChoIs3toChoIs2,LocationChoIs2
                db 048 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 3",0   
LocationCircle: db 7,0
                dw 00,00
                dw WM_marioFromChoIs3toCircle,LocationCircle
                dw WM_marioFromChoIs3toChoFor,LocationChoFor
                dw WM_marioFromChoIs3toChoIs2,LocationChoIs2
                db 048 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 3",0   

LocationChoFor: db 8,7
                dw 00,00
                dw WM_marioFromChoFortoChoIs4,LocationChoIs4
                dw 00,00
                dw WM_marioFromChoFortoChoIs3,LocationChoIs3
                db 049 ;locationnumber used on level exit
                db "CHOCOLATE FORTRESS",0   
               
LocationChoIs4: db 8,6
                dw WM_marioFromChoIs4toChoFor,LocationChoFor
                dw 00,00
                dw WM_marioFromChoIs4toChoIs5,LocationChoIs5
                dw 00,00
                db 050 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 4",0           

LocationChoIs5: db 8,5
                dw WM_marioFromChoIs5toWendCa,LocationWendCa
                dw 00,00
                dw 00,00
                dw WM_marioFromChoIs5toChoIs4,LocationChoIs4
                db 051 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 5",0   
WendCatoChoIs5: db 8,4
                dw WM_marioFromChoIs5toWendCa,LocationWendCa
                dw 00,00
                dw 00,00
                dw WM_marioFromChoIs5toChoIs4,LocationChoIs4
                db 051 ;locationnumber used on level exit
                db "CHOCOLATE ISLAND 5",0   

SunkGhtoWendCa:
TubeL1toWendCa: db 0,0
                dw WM_marioFromWendCatoSunkGh,LocationSunkGh
                dw WM_marioFromWendCatoChoIs5,WendCatoChoIs5
                dw WM_marioFromWendCatoTubeL1,LocationTubeL1
                dw 00,00
                db 052 ;locationnumber used on level exit
                db "#6 WENDY'S CASTLE",0
LocationWendCa: db 8,4
                dw WM_marioFromWendCatoSunkGh,LocationSunkGh
                dw WM_marioFromWendCatoChoIs5,WendCatoChoIs5
                dw WM_marioFromWendCatoTubeL1,LocationTubeL1
                dw 00,00
                db 052 ;locationnumber used on level exit
                db "#6 WENDY'S CASTLE",0

LocationSunkGh: db 8,3
                dw WM_marioFromSunkGhtoValBo1,LocationValBo1
                dw 00,0
                dw WM_marioFromSunkGhtoWendCa,SunkGhtoWendCa
                dw 00,00
                db 053 ;locationnumber used on level exit
                db "SUNKEN GHOST SHIP",0

LocationValBo1: db 8,2
                dw 00,00
                dw 00,00
                dw WM_marioFromValBo1toValBo2,LocationValBo2
                dw WM_marioFromValBo1toSunkGh,LocationSunkGh
                db 054 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 1",0

LocationValBo2: db 8,1
                dw WM_marioFromValBo2toValFor,LocationValFor
                dw 00,00
                dw WM_marioFromValBo2toValGho,LocationValGho
                dw WM_marioFromValBo2toValBo1,LocationValBo1
                db 055 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 2",0

LocationValFor: db 8,0
                dw WM_marioFromValFortoBackDo,LocationBackDo
                dw WM_marioFromValFortoValBow,LocationValBo2
                dw 00,00
                dw 00,00
                db 056 ;locationnumber used on level exit
                db "VALLEY FORTRESS",0

LocationBackDo: db 9,7
                dw 00,00
                dw WM_marioFromBackDotoValFor,LocationValFor
                dw 00,00
                dw 00,00
                db 057 ;locationnumber used on level exit
                db "BACK DOOR",0

;opsplitsten
LarrCatoValGho: db 9,5
                dw WM_marioFromValGhotoLarrCa,LocationLarrCa
                dw 00,00
                dw WM_marioFromValGhotoValBo3,LocationValBo3
                dw WM_marioFromValGhotoValBo2,LocationValBo2
                db 058 ;locationnumber used on level exit
                db "VALLEY GHOST HOUSE",0
LocationValGho: db 9,6
                dw WM_marioFromValGhotoLarrCa,LocationLarrCa
                dw 00,00
                dw WM_marioFromValGhotoValBo3,LocationValBo3
                dw WM_marioFromValGhotoValBo2,LocationValBo2
                db 058 ;locationnumber used on level exit
                db "VALLEY GHOST HOUSE",0

;opsplitsten
FrontDtoLarrCa: db 9,1
                dw WM_marioFromLarrCatoFrontD,LocationFrontD
                dw WM_marioFromLarrCatoValGho,LarrCatoValGho
                dw WM_marioFromLarrCatoValBo4,LocationValBo4
                dw 00,00
                db 059 ;locationnumber used on level exit
                db "#7 LARRY'S CASTLE",0
ValBo4toLarrCa: db 9,0
                dw WM_marioFromLarrCatoFrontD,LocationFrontD
                dw WM_marioFromLarrCatoValGho,LarrCatoValGho
                dw WM_marioFromLarrCatoValBo4,LocationValBo4
                dw 00,00
                db 059 ;locationnumber used on level exit
                db "#7 LARRY'S CASTLE",0
LocationLarrCa: db 9,5
                dw WM_marioFromLarrCatoFrontD,LocationFrontD
                dw WM_marioFromLarrCatoValGho,LarrCatoValGho
                dw WM_marioFromLarrCatoValBo4,LarrCatoValBo4
                dw 00,00
                db 059 ;locationnumber used on level exit
                db "#7 LARRY'S CASTLE",0

LocationValBo3: db 9,4
                dw WM_marioFromValBo3toValBo4,LocationValBo4
                dw 00,00
                dw 00,00
                dw WM_marioFromValBo3toValGho,LocationValGho
                db 060 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 3",0

;opsplitsten
LarrCatoValBo4: db 9,0
                dw WM_marioFromValBo4toStarD1,LocationStarE1
                dw WM_marioFromValBo4toValBo3,LocationValBo3
                dw 00,00
                dw WM_marioFromValBo4toLarrCa,ValBo4toLarrCa
                db 061 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 4",0
LocationValBo4: db 9,3
                dw WM_marioFromValBo4toStarD1,LocationStarE1
                dw WM_marioFromValBo4toValBo3,LocationValBo3
                dw 00,00
                dw WM_marioFromValBo4toLarrCa,ValBo4toLarrCa
                db 061 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 4",0
StarD1toValBo4: db 9,2
                dw WM_marioFromValBo4toStarD1,LocationStarE1
                dw WM_marioFromValBo4toValBo3,LocationValBo3
                dw 00,00
                dw WM_marioFromValBo4toLarrCa,ValBo4toLarrCa
                db 061 ;locationnumber used on level exit
                db "VALLEY OF BOWSER 4",0

LocationStarE1: db 9,2
                dw 00,00
                dw WM_marioFromStarD1toValBo4,StarD1toValBo4
                dw 00,00
                dw WM_marioFromStarD1toFrontD,StarD1toFrontD
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

;opsplitsten
StarD1toFrontD: db 9,2
                dw 00,00
                dw WM_marioFromFrontDtoLarrCa,FrontDtoLarrCa
                dw WM_marioFromFrontDtoStarD1,LocationStarE1
                dw 00,00
                db 062 ;locationnumber used on level exit
                db "FRONT DOOR",0
LocationFrontD: db 9,1
                dw 00,00
                dw WM_marioFromFrontDtoLarrCa,FrontDtoLarrCa
                dw WM_marioFromFrontDtoStarD1,LocationStarE1
                dw 00,00
                db 062 ;locationnumber used on level exit
                db "FRONT DOOR",0

;opsplitsen
LocationStarW1: db 0,0
                dw WM_marioFromStarW1toStarB2,StarW1toStarB2
                dw WM_marioFromStarW1toStarA2,LocationStarA2
                dw WM_marioFromStarW1toStarB2,StarW1toStarB2
                dw 00,00
                db 063 ;locationnumber used on level exit
                db "STAR WORLD 1",0
StarB2toStarW1: db 10,7
                dw WM_marioFromStarW1toStarB2,StarW1toStarB2
                dw WM_marioFromStarW1toStarA2,LocationStarA2
                dw WM_marioFromStarW1toStarB2,StarW1toStarB2
                dw 00,00
                db 063 ;locationnumber used on level exit
                db "STAR WORLD 1",0

;opsplitsen
LocationStarW2: db 0,0
                dw WM_marioFromStarW2toStarC2,StarW2toStarC2
                dw 00,00
                dw WM_marioFromStarW2toStarB2,StarW2toStarB2
                dw WM_marioFromStarW2toStarC2,StarW2toStarC2
                db 064 ;locationnumber used on level exit
                db "STAR WORLD 2",0
StarW3toStarW2: db 10,6
                dw WM_marioFromStarW2toStarC2,StarW2toStarC2
                dw 00,00
                dw WM_marioFromStarW2toStarB2,StarW2toStarB2
                dw WM_marioFromStarW2toStarC2,StarW2toStarC2
                db 064 ;locationnumber used on level exit
                db "STAR WORLD 2",0
                
LocationStarC2: db 00,0
                dw 00,00
                dw 00,00
                dw WM_marioFromStarC2toStarW2,StarW3toStarW2
                dw WM_marioFromStarC2toStarW3,LocationStarW3
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0
StarW2toStarC2: db 10,6
                dw 00,00
                dw 00,00
                dw WM_marioFromStarC2toStarW2,StarW3toStarW2
                dw WM_marioFromStarC2toStarW3,LocationStarW3
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

LocationStarC1: db 10,1
                dw WM_marioFromStarC1toSodaLa,LocationSodaLa
                dw 00,00
                dw 00,00
                dw WM_marioFromStarC1toSodaLa,LocationSodaLa
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0

;opsplitsen
LocationStarW3: db 0,0
                dw WM_marioFromStarW3toStarC2,LocationStarC2
                dw 00,00
                dw WM_marioFromStarW3toStarC2,LocationStarC2
                dw WM_marioFromStarW3toStarD2,StarW3toStarD2
                db 065 ;locationnumber used on level exit
                db "STAR WORLD 3",0
StarD2toStarW3: db 10,5
                dw WM_marioFromStarW3toStarC2,LocationStarC2
                dw 00,00
                dw WM_marioFromStarW3toStarC2,LocationStarC2
                dw WM_marioFromStarW3toStarD2,StarW3toStarD2
                db 065 ;locationnumber used on level exit
                db "STAR WORLD 3",0

;opsplitsen
LocationStarD2: db 00,0
                dw 00,00
                dw WM_marioFromStarD2toStarW4,LocationStarW4
                dw WM_marioFromStarD2toStarW3,StarD2toStarW3
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0   
StarW3toStarD2: db 10,5
                dw 00,00
                dw WM_marioFromStarD2toStarW4,LocationStarW4
                dw WM_marioFromStarD2toStarW3,StarD2toStarW3
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0     
                
;opsplitsen
LocationStarW4: db 0,0
                dw WM_marioFromStarW4toStarD2,LocationStarD2
                dw WM_marioFromStarW4toStarE2,StarW4toStarE2
                dw 00,00
                dw 00,00
                db 066 ;locationnumber used on level exit
                db "STAR WORLD 4",0    
StarE2toStarW4: db 10,4
                dw WM_marioFromStarW4toStarD2,LocationStarD2
                dw WM_marioFromStarW4toStarE2,StarW4toStarE2
                dw 00,00
                dw 00,00
                db 066 ;locationnumber used on level exit
                db "STAR WORLD 4",0    
                
StarW4toStarE2: db 10,4
                dw WM_marioFromStarE2toStarW4,StarE2toStarW4
                dw 00,00
                dw WM_marioFromStarE2toStarW5,LocationStarW5
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0             

;opsplitsen
LocationStarW5: db 0,0
                dw WM_marioFromStarW5toStarF2,LocationStarF2
                dw WM_marioFromStarW5toStarE2,StarW4toStarE2
                dw 00,00
                dw WM_marioFromStarW5toStarE2,StarW4toStarE2
                db 067 ;locationnumber used on level exit
                db "STAR WORLD 5",0            
                
LocationTubeL2: db 10,3
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromTubeL2toChoSec,TubeL2toChoSec
                db 00 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap   

WendCatoTubeL1:
LocationTubeL1: db 0,0
                dw 00,00
                dw WM_marioFromTubeL1toWendCa,TubeL1toWendCa
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db 0 ;Tubes have no text on the worldmap    

LocationStarF2: db 10,2
                dw 00,00
                dw WM_marioFromStarF2toStarW5,LocationStarW5
                dw 00,00
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0           

LocationStarF1: db 0,0
                dw 00,00
                dw 00,00
                dw WM_marioFromStarF1toSpeci1,LocationSpeci1
                dw 00,00
                db 000 ;locationnumber used on level exit
                db "STAR ROAD",0         

LocationSpeci1: db 0,0
                dw 00,00
                dw 00,00
                dw WM_marioFromSpeci1toSpeci2,LocationSpeci2
                dw WM_marioFromSpeci1toStarF1,LocationStarF1
                db 068 ;locationnumber used on level exit
                db "SPECIAL 1",0                                                                                                

LocationSpeci2: db 0,0
                dw 00,00
                dw 00,00
                dw 00,00
                dw WM_marioFromSpeci2toSpeci1,LocationSpeci1
                db 069 ;locationnumber used on level exit
                db "SPECIAL 2",0                   

LocationSpeci3: db 0,0
                dw 00,00
                dw 00,00
                dw 00,00
                dw 00,00
                db 070 ;locationnumber used on level exit
                db "SPECIAL 3",0                   
