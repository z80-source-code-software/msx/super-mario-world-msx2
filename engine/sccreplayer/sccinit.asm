
;-------------------------------------
; Initialize the scc
;-------------------------------------
SccInit:
        ld      hl,Period60
        ld      (9880h),hl
        ld      (9882h),hl
        ld      (9884h),hl


        ld  	a,00100000b         ; Reset phase when freq is written
        ld  	(98E0h),a			; on SCC
        ld  	(98C0h),a			; cover SCC+ in SCC mode

        ld      a,15
        ld      (988Ah),a       ; volume ch1
        ld      (988Bh),a       ; volume ch2
        ld      (988Ch),a       ; volume ch3


SccMute:
        ld      hl,9800h
        ld      de,9801h
        ld      bc,32*4 -1

        ld      (hl),0
        ldir
   ;     ret
        
; Mute replayer
ReplayerMute:

        xor      a
        ld      (SccSfxOn),a
        ld      (988Fh),a	; all channels inactive
        ret