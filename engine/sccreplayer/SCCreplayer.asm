;SCC replayer coded by Artrag. All code you find withing
;our sources concerning SCC audio have been coded by artrag
;please contact artrag @ www.msx.org if you have interest 
;in his code or if you need help to get this into your code as well.

;-------------------------------------
; SCC and Slot management
;-------------------------------------


; konami 5 mapper

	; Bank 1: 5000h - 57FFh (5000h used)
	; Bank 2: 7000h - 77FFh (7000h used)
	; Bank 3: 9000h - 97FFh (9000h used)
	; Bank 4: B000h - B7FFh (B000h used)
	
Bank1:  equ      05000h			
	
; scc mapper for scc chip

Period60: equ       (3579545/32*1001/(60*1000)-1)

;-------------------------------------
; Initialize replayer
;
; in :
; l  # of Sfx
;
;-------------------------------------

ds   $2000


SfxTable:
		include "sccreplayer/SfxTable4.asm"

;-------------------------------------
; write 32 samples and moves sample pointer to next page
; NTSC version == 622 cycles 
;-------------------------------------


				
SccReplayerUpdate:

  ld a,(sfxactive)
  and a
  ret z

		ld      a,(SccSfxOn)		; play one sfx at the time
    or      a
;		ret		z
    jp z,.stop ;bug reproducible in emulation

        ld      a,(SamplePage)
        ld      (Bank1),a

        ld      hl,(SamplePos)
        ld      de,9800h

        ;622 cycles except at bank swap
        ; phase 0
        ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi ;	18*32

        ld     	a,h				; 5
		cp		60h				; 8
        jp 		nz,.s1			; 11
		
        ld      a,(SamplePage)
        inc     a
        ld      (SamplePage),a
        ld      (Bank1),a
        ld		h,40h
.s1:      
		ld      a,Period60 and 255	; 8
        ld      (9880h),a       	; 14
		
        ;622 cycles except at bank swap
        ; phase 1
        ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi ;	18*32

        ld     	a,h				; 5
		cp		60h				; 8
        jp 		nz,.s2			; 11
		
        ld      a,(SamplePage)
        inc     a
        ld      (SamplePage),a
        ld      (Bank1),a
        ld		h,40h
.s2:      
		ld      a,Period60 and 255
        ld      (9882h),a			
		
        ;622 cycles except at bank swap
        ; phase 2
        ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi : ldi ;	18*32

        ld     	a,h				; 5
		cp		60h				; 8
        jp 		nz,.s3			; 11
		
        ld      a,(SamplePage)
        inc     a
        ld      (SamplePage),a
        ld      (Bank1),a
        ld		h,40h
.s3:      
		ld      a,Period60 and 255
        ld      (9884h),a		

        ld      (SamplePos),hl

		ld      a,00000111b     ; channels 1-3 active
        ld      (988Fh),a
        ld      hl,(NumBlocksToPlay)
        dec     hl                       ; does NOT affect Z flag
        ld      (NumBlocksToPlay),hl

        ld      a,h
        or      l
		ret		nz

        xor      a
        ld      (SccSfxOn),a
.stop:        
        ld      (988Fh),a	; all channels inactive
        ret



;-------------------------------------
; Padding for rom player
;-------------------------------------
        ds	$8000 - $



;-------------------------------------
; Sample data
;-------------------------------------
SAMPLE_START:
         include "sccreplayer/DataTable.asm"
		 
SAMPLE_END:


;-------------------------------------
; Padding, align rom image to a power of two.
;-------------------------------------

SAMPLE_LENGTH:  equ SAMPLE_END - SAMPLE_START

	;	DS (84000h - $)
		DS (28000h - $) ;128 + 24 = 152 Kb




FINISH:
