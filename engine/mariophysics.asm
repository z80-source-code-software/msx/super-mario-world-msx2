
;====================================================================================================================
;handle the character sprite physics this includes jumping, falling, moving and sets the coresponding variables
;====================================================================================================================

;
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		    	Z 	C 		  X   	  right	  left	down	up	(keyboard)
;

;horror zone!! if you change any piece of code within this file it will be at YOUR OWN RISK!!
;even I, the maker, does not understand why it actually works because I wrote this in some kind of ZEN meditative
;state. If the maker does not understand this, you will certainly not.

endlevelbywinloop:

  ld a,(endlevelbywin)
  cp 2
  jp z,.waitformusicend


  ld a,(camreachedlimit)
  and a
  jp nz,.setwaitformusicend

;there are maps with multiple exits so time the walk then exit anyway
    ld a,(walktimer)
    dec a
    ld (walktimer),a
    jp z,.setwaitformusicend    
;  ld a,(currentlevel)
;  cp 77
;  jp z,.setwaitformusicend  

;we slow mario down by taking out the mariospeed each frame
  ld a,(mariospeed)
  cp 0
  jp z,.setspeedtoone
  dec a
  cp 1
  jp z,.speeddowndone
  ld (mariospeed),a
 
.speeddowndone:
 
  ld d,0
  ld e,a
  ld hl,(mariox)
  add hl,de
  ld (mariox),hl
  
;we abuse the framecounter for the final endloop  
  xor a
  ld (framecounter),a
  
  
  ret
  
.setspeedtoone: 
  ld a,2
  ld (mariospeed),a

  ret

.setwaitformusicend:

  ld a,2
  ld (endlevelbywin),a

  xor a
  ld (Controls),a
  ld (mariospeed),a
  ret

.waitformusicend:

  ld a,(framecounter)
  cp 50
  call z,.setpeace
  cp 100
  call z,.walkawayfadeoutandfinish
  cp 200
  jp nc,.endlevel

  ld a,(mariospeed)
  ld d,0
  ld e,a
  ld hl,(mariox)
  add hl,de
  ld (mariox),hl
  
  ret


.setpeace:

  ld a,1
  ld (mariopeace),a
  ret

.walkawayfadeoutandfinish:

;set the Controls
  ld a,%00001000
  ld (Controls),a

;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld (mariospeed),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps

  xor a
  ld (mariopeace),a

  ret

.endlevel:

  xor a
  ld (yoshicoin),a

  ld a,1
  ld (levelend?),a
  ret



;reset the Controls and end the level
endlevelnormal:

;  call killallenemies

  ld a,%00001000
  ld (Controls),a

  xor a
  ld (marioslide),a
  ld (marioshot),a
  ld a,1
  ld (marioleftright),a
  ld (marioleftrightspr),a

  call checktilebelowmario
  call c,.setendlevelbywin

  xor a ;reset the value for the endcheck
  ret

.setendlevelbywin:

  ld a,1
  ld (endlevelbywin),a
  ld a,150
    ld (walktimer),a
  ret
  


;mario gets launched from a tube
mariotubelaunch:

;wait some frames until we launch
  ld a,(tubelaunch)
  inc a
  ld (tubelaunch),a
  cp 50
  jp z,.stoplaunch
  cp 20
  jp z,Playsfx.explosion
  cp 20
  jp nc,.launchup

  ld a,1 ;inhibit the display of mario
  ld (nosprite),a

  ret


.launchup:

  xor a
  ld (nosprite),a
  ld a,1
  ld (mariofloating),a
  ld (marioleftright),a
  ld (marioleftrightspr),a
  ld (sfxplaying),a ;disable jumping sound
;  ld (marionotdown),a
  ld a,5
  ld (mariospeed),a

  ld hl,(mariox)
  ld de,6
  add hl,de
  ld (mariox),hl
  
  ld hl,(marioy)
  ld de,5
  xor a
  sbc hl,de
  ld (marioy),hl

  ret


.stoplaunch:

  xor a
  ld (tubelaunch),a
  ld (nosprite),a
  ret


spawnmariobullet:

  call Playsfx.bullet

  ld hl,(mariox)
  
  srl h
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8 
  
  
  ld bc,(marioy)
  
  srl b
  rr c   ;/2
  srl b
  rr c ;/4
  srl b
  rr c ;/8     
  
;  dec bc ;2 tiles hoger
;  dec bc
  inc bc
  inc bc
  inc bc


  ld    iy,CustomObject
  
  ld a,(bulletsingame)
  cp 2
  jp z,.force13
  ld    (iy),force12
  jp .force12
.force13:
  ld    (iy),force13
.force12:
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),c                ;set y custom object
  ld    (iy+4),b                ;set y custom object
  ld    (iy+5),22                ;set object number
  ld    (iy+6),0                ;set object number

;we are on page 1 block34 set this value before spawning the object!
  
  ld a,romprogblock
  ld (romprogblocknumber),a

  call  activateobject
  


  ld a,(mariospinjump)
  and a
  jp z,.norandom

  ld a,r
  bit 0,a
  jp z,.left
  jp nz,.right


.norandom:

  ld a,(marioleftrightspr)
  and a
  jp nz,.right
  
.left: 
 
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,4
  xor a
  sbc hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+v1),-5
  ld (ix+v4),0
  
  ret
  
.right:


  
  ld l,(ix+xobject)
  ld h,(ix+xobject+1)
  ld de,6
;  xor a
  add hl,de
  ld (ix+xobject),l
  ld (ix+xobject+1),h
  ld (ix+v1),5
  ld (ix+v4),0

  ret



mariospinshoot:

;check all conditions
  ld a,(mariospinjump)
  and a
  ret z
  
  ld a,(marioyoshi)
  dec a
  ret z
    
  ld a,(mariostate)
  cp 2
  ret nz ;TODO: branch code here for a special spawn that creates a killer object around mario for the yellow special boxes
  
;too many bullets in game

  ld a,(bulletsingame)
  cp 2
  ret z


  ld a,(framecounter)
  and 3
  ret nz


  ld a,(bulletsingame)
  inc a
  ld (bulletsingame),a

  call spawnmariobullet
  ret


;check of mario geschoten heeft of nog moet schieten. Handel het hele schieten gewoon af

marioshooting:

  ld a,(mariotransforming)
  and a
  jp nz,.nostate


;state resetten bij een shell anders gebeuren er bugged dingen
  ld a,(marioshell)
  and a
  jp nz,.nostate


;toets ingedrukt is altijd de routine opnieuw initieren
  ld a,(Controls)
  bit 5,a
  call z,.resetshoothold


;routine is al in werking    
  ld a,(marioshot)
  and a
  call nz,.checkmariostate

;toets is nog steeds ingedrukt
  ld a,(marioshoothold)
  dec a
  ret z

;we schieten zowieso niet
  
 ld a,(Controls)
 bit 5,a
 ret z 

;do not spawn bullets on yoshi
  ld a,(marioyoshi)
  dec a
  jp z,.setshot


  ld a,(mariostate)
  cp 3
  jp z,.setshot
  cp 2
  ret nz

.setshot:
  ld a,1
  ld (marioshot),a
  ld (marioshoothold),a ;schot vasthouden tot reset

;discriminate between fire and feather mario and then play the sound
  ld a,(mariostate)
  cp 3
  call z,Playsfx.spinjump

  ret
  


.checkmariostate:

;on yoshi we do not spawn bullets
  ld a,(marioyoshi)
  dec a
  jp z,.yoshi


  ld a,(mariostate)
  cp 2
  jp z,.fire
  cp 3
  jp z,.feather
  jp .nostate

.yoshi:

  ld a,(marioshot)
  inc a
  cp 15
  jp nc,.nostate
  ld (marioshot),a
  ld a,1
  ld (marioshoothold),a

  ret
  


.feather:

  ld a,(marioshot)
  cp 1
  call z,spawncape ;branch to spawncode and instead of do nothing spawn an object to initatie boxes    
  inc a
  cp 15
  jp nc,.nostate
  ld (marioshot),a
  
  ret

.fire:

  ld a,(marioshot)
  cp 1
  ret nz
  inc a
  ld (marioshot),a

;maximum bereikt
  ld a,(bulletsingame)
  cp 2
  ret z
  inc a ;bullet erbij gekomen
  ld (bulletsingame),a
  

  call spawnmariobullet

  ret


.nostate:

  xor a
  ld (marioshot),a
  ret

.resetshoothold:

  xor a
  ld (marioshoothold),a
  ret



mariophysics: 



  ld a,(endlevelbywin)
  and a
  jp nz,endlevelbywinloop


  ld a,(tubelaunch)
  and a
  jp nz,mariotubelaunch

  ld a,(nocontrol)
  cp 3
  call z,endlevelnormal
  and a
  ret nz 

  xor a
  ld (movedownsheertile),a

  call mariospinshoot

  call marioshooting

  call checktilebelowmario ;sheertile bepalen meer niet


  ld a,(pauze)
  dec a
  ret z


;wanneer mario transformeert moeten we helemaal niks meer ondernemen
;elke waarde die niet gelijk aan nul is betekend dat wij aan het transformeren zijn



  ld a,(mariotransforming)
  and a
  ret nz




;mario pleurt door de grond??
  ld a,(mariocoordinates)
  cp 216
  jp nc,.resetmariocoordinates

;aan de hand van de sheertile de keys setten

;  ld a,(mariospeed)
;  and a
;  call z,.resetmariosheerslide
  call .resetmariosheerslide


;het hoekje moet ook lekker sliden
  ld a,(tilecornertype)
  dec a
  call z,.mariosheerleft
  ld a,(tilecornertype)
  cp 2
  call z,.mariosheerright

  ld a,(mariosheertile)
  cp 1 ;going left??
  call z,.mariosheerleft
  ld a,(mariosheertile)
  cp 2 ;going right??
  call z,.mariosheerright

;slide when standing on specialsheertile

  ld a,(specialsheertile)
  cp 7
  push af
  call z,.mariosheerleft
  pop af
  jp z,.skipspecialsheertile
  cp 8
  push af
  call z,.mariosheerright
  pop af
  jp z,.skipspecialsheertile  
  and a
  call nz,.mariospecialsheerleft

.skipspecialsheertile:


;mario aan het klimmen??

  ld a,(marioclimbing)
  dec a
  jp z,mariophysicsclimbing


;is mario aan het zwemmen? dan gaan we naar een andere branch toe met hele andere regels  
  
  ld a,(marioswim)
  dec a
  jp z,mariophysicswater

;mario is een drop aan het doen niets meer verrichten
  ld a,(marioflydrop)
  dec a
  jp z,marioflyingdroptoground
 
;alle Controls worden uitgelezen en zo worden de juiste fysics toegepast
 
 
  ;speciale check of links OF rechts is ingedrukt. zo niet dan kunnen wij ook niet meer rennen
  ld a,(Controls)
  and %00001100
  call z,.terminaterunning 
  
  
  ld a,(marioslowdownpointer)
  and a
  call nz,slowmariodown
  
  
  ;beide toetsen links en rechts NIET ingedrukt dan gaan we mario vertragen
  ld a,(Controls)
  and %00001100
  ;call z,.slowdownstandingstill
  call z,slowmariodown.activateslowdown
  
;nog even controleren of wij stilstaan anders blijft onze sprite hangen in de slide vorm 
  ld a,(mariospeed)
  and a
  call z,.resetmarioslide 

;check of mario omhoog kijkt en omlaag
  ld a,(funloop)
  and a
  jp nz,.nolookreset

  xor a
  ld (mariolook),a

.nolookreset:

  ld a,(Controls)
  bit 1,a
  call nz,.mariodown
  ld a,(Controls)
  bit 0,a
  call nz,.mariolook

  
;terminate het rennen zodra de toets is losgelaten anders is cheaten mogelijk
;door eerst de ren toets ten alle tijden ingedrukt te houden zodat je altijd fullspeed houd  
  ld a,(Controls)
  bit 5,a ;running m STILL pressed??
  call z,.terminaterunning

  xor a
  ld (marioduckonground),a


;hier moeten we kijken of we gebukt op de grond staan. zo ja dan mag er helemaal niets gebeuren
;we kijken of mario valt of in de lucht is en deze in combinatie is met bukken
  ld a,(marioduck) ;moet 1 zijn
  xor %00000001 ;inverteren
  ld c,a
  ld a,(mariofloating) ;moet nul zijn
  ld b,a
  ld a,(fallspeed) ;moet 0 nul
  or b ;resultaat van de fallspeed en float moet 0 zijn
  or c ;eindresultaat moet 0 zijn
  call z,.marioduckonground ;alles nul mario staat gebukt op de grond


;jumpkeybitomzetten zodat we tijdens de gehele physics routine weten of er een jumpkey
;word vastgehouden handig wanneer mario moet vliegen enzo staat veel netter bij de checkout routine
;en scheelt een paar T states

  xor a
  ld (mariojumpkey),a

  ld a,(Controls)
  bit 4,a
  call nz,.setmariojumpkey
  bit 6,a
  call nz,.setmariojumpkey

;jumpkeyhold checken en setten bij de juiste condities

  call checkjumpkeyhold

;gaan wij rennen?? 
  ld a,(Controls)
  bit 5,a
  call nz,.running 


  ;mario aan het vliegen dan veranderen de regels vanaf dit punt    
  ld a,(marioflying)
  dec a
  jp z,mariophysicsflying 


  ;check if a box is pushing mario away
  ld a,(touchside)
  dec a
  call z,.lockspriteright
  dec a
  call z,.lockspriteleft 
  
  

;gaan wij naar links of naar rechts??
  
  ld a,(Controls)
  bit 2,a ;going left??
  call nz,.mariowalkleft
  ld a,(Controls)
  bit 3,a ;going right??
  call nz,.mariowalkright  
  
  
  ld a,(marioforcewalk)
  dec a
  call z,.mariowalkrightforced
  
 
;we bekijken het resultaat uit de voorgaande routines en gaan nu werkelijk de coordinaten zetten  
  ld a,(marioleftright)
  dec a
  call z,.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft


;alle left right up down zaken gedaan check nu op sprong en of het een spin jump is of normale jump
;deze routine is tricky. Hier word gekeken naar twee variabelen tegelijk
;wanneer mario nog in een jump zit, oftewel mario is not down dan komt dat bitje
;op 1 te staan. Vervolgens controleren we bit 6 van de Controls
;we stoppen eerst het marionotdown bitje in positie 6 inverteren alle waarden 
;en doen dan een and operatie waaruit blijkt dat beide condities true of false zijn
;beide condities goed? spinjump toelaten dit zelfde grapje doen we dus ook met de normale
;jump om ervoor te zorgen dat mario niet door de grond heen pleurt als je de jump toets vasthoud


;check if mario is on yoshi if so check on spinjump. In a positive case we treat the jump physics in a different way
  ld a,(marioyoshi)
  dec a
  call z,.yoshispinjump
    
 ;altijd jumpen zodra de routine is geinitieerd en mariofloat 1 is
 
  ld a,(mariofloating)
  dec a
  jp z,.jump

  ld a,(marionotdown)
  sla a
  sla a
  sla a
  sla a
  ld c,a ;bit 4 tijdelijk wegschrijven
  sla a
  sla a
  add c ;bit 4 erbij optellen
  xor %11111111 ;resultaat inverteren
  ld c,a ;resultaat wegschrijven


  ld a,(Controls)
  and c
  bit 4,a
  jp nz,.jump
  bit 6,a
  jp nz,.spinjump


;alle keyread gerelateerd acties verricht. gravitatie toepassen
  jp .fall


.yoshispinjump:
   
  ld a,(Controls)
  bit 6,a
  jp nz,.initiatespinjumpfromyoshi
  
  ret

.initiatespinjumpfromyoshi:

  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth

  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a
  
  jp .spinjump



.resetmariosheerslide:

;aan de hand van de sheertile de keys setten
  xor a
  ld (mariosheerslide),a
  ret

;/einde van alle Controls checks

.stairs:

  xor a
  ld (inhibitsprite),a

  ld a,(Controls)
  bit 3,a
  ret nz
  bit 2,a
  ret nz

  ld a,(marioduck)
  and a
  ret nz
  ld a,(mariofloating)
  and a
  ret nz

  ld a,1
  ld (mariospeed),a
  ld (marioleftright),a
  
  ld a,(marioslide)
  and a
  ret nz

  ld hl,mariowalktable
  
  ld a,(mariostate)
  and a
  jp nz,.handlebig  
  
  ld a,(hl)
  cp 3
  ret nz
 
  ld a,1
  ld (inhibitsprite),a

  ret

.handlebig:  

 ; ld hl,mariowalktable
  ld a,(hl)
  cp 0;3
  ret nz
 
  ld a,1
  ld (inhibitsprite),a

  ret


;nadat de routine heeft bekeken of mario op een schuine tile staat moeten we ook werkelijk
;ervoor zorgen dat mario van de tile afglijd. We zetten daarom ook gewoon de key daarvoor
;we moeten ook een bitje zetten voor de sprite engine
.mariosheerleft:

  ld a,(forcetile) ;do not slide when on a forced tile
  cp 2
  jp z,.stairs


  ld a,(marioduck)
  dec a
  call z,.forcerunning

  ld a,(Controls)
  bit 2,a
  call nz,.forcerunning 
  ret nz

  ld a,(Controls)
  bit 3,a
  ret nz

;nice het werkt alleen moet het mogelijk zijn om tegen deze shit in te sturen
  ld a,(Controls)
  xor %00000100
  ld (Controls),a
  
  ld a,1
  ld (mariosheerslide),a
  ld (mariosheerhold),a

  ret

.mariospecialsheerleft:

  ld a,(marioduck)
  and a
  ret z


  ld a,(Controls)
  bit 3,a
  ret nz

;nice het werkt alleen moet het mogelijk zijn om tegen deze shit in te sturen
  ld a,(Controls)
  xor %00000100
  ld (Controls),a
  
  ld a,1
  ld (mariosheerslide),a
  ld (mariosheerhold),a

  ret



.mariosheerright:

  ld a,(marioduck)
  dec a
  call z,.forcerunning


;deze routine negeren wanneer we toetsen hebben ingedrukt
  ld a,(Controls)
  bit 3,a
  call nz,.forcerunning 
  ret nz
  
  ld a,(Controls)
  bit 2,a
  ret nz


  ld a,(Controls)
  xor %00001000
  ld (Controls),a

  ld a,1
  ld (mariosheerslide),a
  ld (mariosheerhold),a 

  ret

/*
.mariospecialsheerright:

  ld a,(marioduck)
  and a
  ret z


  ld a,(Controls)
  bit 2,a
  ret nz


  ld a,(Controls)
  xor %00001000
  ld (Controls),a

  ld a,1
  ld (mariosheerslide),a
  ld (mariosheerhold),a 

  ret
*/

;forceer bit 5 in de Controls
.forcerunning:

  ld a,(Controls)
  and %11011111 ;eerst bitje mollen
  xor %00100000 ;dan inverteren
  ld (Controls),a ;wegschrijven bit 5 is nu ALTIJD 1
  ret
  

;marioccordinaten resetten zodra hij door de grond is gepleurd.
;dit is een tijdelijk routine later gaat mario gewoon dood.
.resetmariocoordinates:

  ld a,(currentlevel)
  cp 78
  jp z,.endlevel ;not on this map

  ld a,1
  ld (dieatonce),a

;is al dood aan het gaan
  ld a,(mariotransforming)
  cp 10
  ret z

  xor a
  ld (mariotransformp),a
  ld (mariostate),a ;no shrink mario dies at once
  
  ld a,10
  ld (mariotransforming),a
  ret

.endlevel:

  ld a,1 ;exit number
  ld (levelend?),a

  ret


;roep alle routines aan om mario af te laten remmen zonder slides en gelul omdat we immers bukken
.marioduckonground:


;geensheerduckresets wanneer we niet op een sheertile staan anders kun je lopen wanneer je gebukt staat
;  ld a,(mariosheertile)
;  and a
;  jp z,.skipsheerduckchecks


;je jumpt op vanuit de slide dus glij je niet meer op de rand
;  ld a,(Controls)
;  bit 4,a
;  jp nz,.resetmariosheerduck
;  bit 6,a
;  jp nz,.resetmariosheerduck 


;.skipsheerduckchecks:

;geen sheerduck wanneer we stilstaan
  ld a,(mariospeed)
  and a
  call z,.resetmariosheerduck


;het hoekje moet ook lekker sliden
  ld a,(tilecornertype)
  cp 2
  jp z,.resetleftkey
  cp 1
  jp z,.resetrightkey


;op een schuine tile glijden we netjes door
  ld a,(mariosheertile)
  cp 2
  jp z,.resetleftkey
  cp 1
  jp z,.resetrightkey

;special
  ld a,(specialsheertile)
  cp 1
  jp z,.resetrightkey ;left tile
  cp 2
  jp z,.resetleftkey ;right tile  
  cp 3
  jp z,.resetrightkey
  cp 4
  jp z,.resetleftkey
  cp 5
  jp z,.resetrightkey
  cp 6
  jp z,.resetleftkey
  cp 7
  jp z,.resetrightkey ;left tile  
  cp 8
  jp z,.resetleftkey ;right tile
 
  call .terminaterunning
  
  ld a,(Controls) ;geen toetsen meer ingedrukt
  and %11110011
;  res 2,a
;  res 3,a
  ld (Controls),a

  ld a,1
  ld (marioduckonground),a

  call slowmariodown.activatemarioduckslowdown
  
  ret


.resetleftkey:

;even de engine vertellen dat we gebukt staan op een sheertile
    ld a,1
    ld (mariosheerduck),a

;  call .slowdownstandingstill  
  call .terminaterunning

;  ld a,3
;  ld (marioslowdownp),a
;  ld a,7
;  ld (marioslowdownrp),a

  ld a,(Controls) ;geen toetsen meer ingedrukt
  and %11110011
  xor %00001000
;  res 2,a
  ld (Controls),a 

  ret

.resetrightkey:


;even de engine vertellen dat we gebukt staan op een sheertile
  ld a,1
  ld (mariosheerduck),a

 ; call .slowdownstandingstill  
  call .terminaterunning


;  ld a,3
;  ld (marioslowdownp),a
;  ld a,7
;  ld (marioslowdownrp),a


  ld a,(Controls) ;geen toetsen meer ingedrukt
  and %11110011
  xor %00000100
;  res 3,a
  ld (Controls),a


  ret

.resetmariosheerduck:

  xor a
  ld (mariosheerduck),a
  
  ret


.setmariojumpkey:
  ld a,1
  ld (mariojumpkey),a
  ret

;zet alleen de variabelen
.mariolook:

;je mag natuurlijk niet omhoog kunnen kijken wanneer je gebukt bent
  ld a,(marioduck)
  dec a
  ret z

  ld a,1
  ld (mariolook),a
  
  ld a,(scorebaractive)
  and a
  ret nz
  
  ld a,1
  ld (scorebaractive),a
  inc a
  ld (showpowerup),a ;add this extra functionality
  ret


.mariodown:
 
;mario mag niet bukken als we nog in een slide zitten period! 
  
  ld a,(marioslide)
  dec a
  ret z 


  ld a,(mariosheerduck)
  dec a
  jp z,.domariodownkeycheck


;BUG: mario reset zijn slide sheerslide niet goed na het afkomen van de ramp. Hier moet ergens een fix komen 
;om het zaakje goed te laten lopen s
  ld a,(mariosheertile)
  and a
  jp z,.skipmariodownkeycheck


.domariodownkeycheck:

   ;speciale check of links OF rechts is ingedrukt. zo niet dan kunnen wij ook niet meer bukken
   ;toetsen direct uitlezen vanuit de controller vanwege de slides die wel mogen
   ;dit grapje telt alleen op de ramps
   
  call readkeydirect
  xor 255
  
  bit 7,a
  jp z,.resetmarioduck
  bit 4,a
  jp z,.resetmarioduck


.skipmariodownkeycheck: 


;is mario niet in een sprong en is hij nog niet gebukt en maken we een spinjump? alles 0 dan mogen we bukken
  ld a,(mariospinjump)
  ld c,a
  ld a,(marioduck)
  ld b,a
  ld a,(mariofloating)
  or b
  or c
  ret nz


;alle condities goed dan mogen we bukken pas bij checkout word het gecancelled
  ld a,1
  ld (marioduck),a

;BUG: this works only for once, when you jump after hitting the ground the slowdown never gets reset and thus the slide becomes perpetueously non-activated
  ;also activate slide if mario is in a duck
  call slowmariodown.activatemarioduckslowdown

  ret




;tijdelijk opgeslagen mariospeed terughalen
.swapmariospeed:

  ld a,(tempspeed)
  ld (mariospeed),a
  ret


.removeshell:

        
        xor a
        ld (marioshell),a
  scf
  ret


.moveleft:


;prevent mario from getting out of the map on the left
  ld hl,(mariox)
  ld de,8
  xor a
  sbc hl,de
;  call c,.removeshell ;get rid of the shell since the object is out of screen
  jp c,lockmariotilex


;TODO: prevent yoshi from exiting the left of the screen with his head.

  call .checkforyoshi
    jp c,lockmariotilex



;check voor grote mario
  call checktileleftmariol  
    jp c,lockmariotilex


;normale tilecheck
  call checktileleftmario
  jp c,lockmariotilex 

  ld a,(touchside)
  cp 2
  jp z,.lockspriteleft


  ld a,(mariosheertile)
  cp 1
    jp z,.moveleftsheertileleft
  cp 2
  jp z,.moveleftsheertileright
 
  ld a,(oldcamerascroll)
  and a
  jp nz,.movestaticleft
 
 
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
 
 
   
   ret


.movestaticleft:

  ld a,(mariocoordinates+1)
  cp 16
  ret c
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl


  ret


.checkforyoshi:

  xor a ;reset carry bit

  ;mario is not on yoshi so skip this step
  ld a,(marioyoshi)
  and a
  ret z


  ld hl,(mariox)
  ld de,24
  xor a
  sbc hl,de

  ret

  



.moveleftsheertileleft:

   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl


   ld hl,(marioy)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (marioy),hl

  ld a,1
  ld (movedownsheertile),a

  ret

.moveleftsheertileright:

   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl


   ld hl,(marioy)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (marioy),hl


  ret


.moveright:


;prevent mario from getting out of the map on the right
  ld hl,(maplenghtx8)
  ld de,24
  xor a
  sbc hl,de
  ld de,(mariox)
  xor a
  sbc hl,de
;  call c,.removeshell ;get rid of the shell since the object is out of screen
  jp c,lockmariotilerightx


;  ld a,(rampjump)
;  cp 2
;  call z,.swapmariospeed 


;als we beginnen te vallen of jumpen doen we een hele aggressieve zijwaartse tilecheck
;  ld a,(marionotdown)
;  ld b,a
;  ld a,(fallspeed)
;  or b
;  call nz,checktilerightmarioh
;    jp c,lockmariotilerightx


;we herhalen de check nogmaals voor een grote mario
;  ld a,(marionotdown)
;  ld b,a
;  ld a,(fallspeed)
;  or b
;  call nz,checktilerightmariol
;    jp c,lockmariotilerightx

;check voor grote mario
  call checktilerightmariol  
    jp c,lockmariotilerightx



  call checktilerightmario
  jp c,lockmariotilerightx

  ld a,(touchside)
  dec a
  jp z,.lockspriteright


  ld a,(mariosheertile)
  cp 1
  jp z,.moverightsheertileleft
  cp 2
  jp z,.moverightsheertileright

  ld a,(oldcamerascroll)
  and a
  jp nz,.movestaticright


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl


   ret

.movestaticright:

  ld a,(mariocoordinates+1)
  cp 246
  ret nc



   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl


  ret



;is actually the right side of the block
.lockspriteleft:

  xor a
  ld (touchside),a

;force mario back a few pixels. This ensures that mario stays on the side of the block
  ld hl,(mariox)
  inc hl
  inc hl
;  inc hl
;  inc hl
  ld (mariox),hl


  ret


;is actually the left side of the block
.lockspriteright:

  xor a
  ld (touchside),a


;force mario back a few pixels. This ensures that mario stays on the side of the block
  ld hl,(mariox)
  dec hl
  dec hl
  dec hl
  dec hl
  ld (mariox),hl


  ret



.moverightsheertileleft:

   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl


   ld hl,(marioy)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (marioy),hl


  ret

.moverightsheertileright:


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl


   ld hl,(marioy)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (marioy),hl

  ld a,1
  ld (movedownsheertile),a

  ret

.setholdspritedirleft:

  ld a,(mariosheerhold)
  dec a
  ret z 

  ld a,1
  ld (holdspritedir),a
  xor a
  ld (marioleftrightspr),a

  ret
  
  
.setholdspritedirright:

  ld a,(mariosheerhold)
  dec a
  ret z

  ld a,1
  ld (holdspritedir),a
  ld (marioleftrightspr),a

  ret  



.setturnshellleft:


  ld a,(marioyoshi)
  dec a
  jp z,.setturnyoshileft


  ld a,(marioshell)
  and a
  ret z


  ld a,(marioleftrightspr)
  and a
  ret nz
  
;als je puur naar links beweegt  
 
  ld a,(marioleftright)
  and a
  ret z
  
  
  ld a,(marioturnshell)
  and a
  ret nz
  
  ld a,(turnshellactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnshell),a
  ld (turnshellactive),a


  ret

.setturnyoshileft:

  ld a,(marioyoshi)
  and a
  ret z


  ld a,(marioleftrightspr)
  and a
  ret nz
  
;als je puur naar links beweegt  
 
  ld a,(marioleftright)
  and a
  ret z
  
  
  ld a,(marioturnyoshi)
  and a
  ret nz
  
  ld a,(turnyoshiactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnyoshi),a
  ld (turnyoshiactive),a


  ret



.mariowalkleft:


;geen routines boven deze lijn!!
;we resetten de toets naar rechts zodat er geen rare dingen gebeuren wanneer we returnen uit deze routine


  res 3,a
  ld (Controls),a

  call .setholdspritedirleft

  call .setturnshellleft
 


;is mario in een slide? slowdown pointer resetten wanneer dat niet zo is
;  ld a,(marioslide)
;  and a
;  call z,.resetslowdownpointer


;BUG: wanneer we de toets aan tappen doet mario eerst een slide en vertraagd tot 0 speed en dan draaien 
;we om. Dit klopt in principe maar het is heel vreemd als we toets niet ingedrukt houden bij slide init.

;welke kant lopen we op
  ld a,(marioleftright)
  dec a
  jp z,.slowdowntoleft
 
 ;heel belangrijk is dat wanneer de slide onderbroken word door snelle keypresses
;dat de slide dan ook stopt en mario gewoon verder gaat met zijn ding  
  ld a,(marioleftright)
  and a
  call z,.resetmarioslide
 
 
  call .acceleratefromstandingstill ;we gaan versnellen vanuit stilstand

  ;reset de slowdown pointer naar 0 dit routinetje moet wel even anders vanwege de turn slowdown
;  xor a
;  ld hl,marioslowdownp
;  ld (hl),a


.setmariowalkleft:
 
  ;geef aan in de variabele dat mario naar links beweegt
  xor a
  ld (marioleftright),a
    ld (turnshellactive),a
    ld (turnyoshiactive),a 
    
  ret


.setturnshellright:

  ld a,(marioyoshi)
  dec a
  jp z,.setturnyoshiright


  ld a,(marioshell)
  and a
  ret z

  ld a,(marioleftrightspr)
  and a
  ret z
  
;als je puur naar links beweegt  
  ld a,(marioleftright)
  and a
  ret nz
  
  
  ld a,(marioturnshell)
  and a
  ret nz
  
  ld a,(turnshellactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnshell),a
  ld (turnshellactive),a


  ret

.setturnyoshiright:


  ld a,(marioyoshi)
  and a
  ret z

  ld a,(marioleftrightspr)
  and a
  ret z
  
;als je puur naar rechts beweegt  

  
  ld a,(marioleftright)
  and a
  ret nz
  
  
  ld a,(marioturnyoshi)
  and a
  ret nz
  
  ld a,(turnyoshiactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnyoshi),a
  ld (turnyoshiactive),a
  

  ret


.mariowalkrightforced:

  xor a
  ld (marioforcewalk),a

  ld a,1
  ld (mariospeed),a
 
  ld a,1
  ld (marioleftright),a
 
  ret


.mariowalkright:


  call .setholdspritedirright

  call .setturnshellright

;loopt mario de andere kant op? dan moet er een slide komen op basis van zijn snelheid
;  ld a,(marioslide)
;  and a
;  call z,.resetslowdownpointer


  
  ld a,(marioleftright)
  and a
  jp z,.slowdowntoright

  ld a,(marioleftright)
  dec a
  call z,.resetmarioslide  


;mario versnellen als we normaal vanuit stilstand opkomen. OOK als we rennen
  call .acceleratefromstandingstill


;  xor a
;  ld hl,marioslowdownp
;  ld (hl),a
  

.setmariowalkright:
 
  ld a,1
  ld (marioleftright),a
  xor a
      ld (turnshellactive),a 
      ld (turnyoshiactive),a 
  
  ret

.setmarioslide:

  ld a,1
  ld (marioslide),a
  ret

;wanneer we zien dat de pop naar rechts beweegt moeten we zorgen dat die een slide maakt
.slowdowntoleft:
  
;zowieso rennen eerst stoppen

  call .terminaterunning
 
;de engine moet even weten dat mario in een slide zit 
;maar niet toestaan in de lucht anders krijgen we glitches
  call .setmarioslide




  ld a,(mariospeed)
;  ld b,a
;  ld a,(marioshell)
;  or b
  and a
  
;  jp nz,.slowdownstandingstill ;hier vertragen we totdat we stilstaan en dan pas gaan we de slide enden
  jp nz,slowmariodown.activateslide


;dit hieronder lijkt overbodige code maakt het maakt het net iets smoother allemaal
;het zou zonder werken maar je ziet dan wat glitchy turnarounds


;mario slide is ended
  xor a
  ld (marioslide),a
  ld (mariospeed),a
;voorkom hakkelen van de char vanuit stilstand
   call .acceleratefromstandingstill 
  jp .setmariowalkleft




;wanneer we zien dat de pop naar rechts beweegt moeten we zorgen dat die een slide maakt
.slowdowntoright:
 
 
 ;zowieso rennen stoppen

  call .terminaterunning
 
;de engine moet even weten dat mario in een slide zit 
;maar niet toestaan in de lucht anders krijgen we glitches
  call .setmarioslide


   ;op schuine delen geen slides maken 
 ; ld a,(mariosheerduck)
 ; dec a
 ; jp z,.noslowdownslideright

  
;controleer of mario stilstaat zo niet dan gaan we sliden en de setspeed vermijden
;hier moeten we ook checken op aanwezigheid van schelpje

  
  ld a,(mariospeed)
;  ld b,a
;  ld a,(marioshell)
;  or b
  and a
  
;  jp nz,.slowdownstandingstill 
  jp nz,slowmariodown.activateslide
 

.noslowdownslideright:

;mario slide is ended
  xor a
  ld (marioslide),a  
  ld (mariospeed),a
;voorkom hakkelen van de char vanuit stilstand
  call .acceleratefromstandingstill
    
  jp .setmariowalkright


.nofallslowdown:

  ld a,1
  ld (mariospeed),a 


  ret


.slowdowntowalking:

;schijt aan als we rennen 
  ld a,(Controls)
  bit 5,a
  ret nz
 
;mario is aan het rennen en vertraagd nu naar loopsnelheid 
  
  ld d,0
  ld hl,marioslowdownrpp
  ld e,(hl)
   
  ld hl,marioslowdownfr
  add hl,de
  ld a,(hl)
  cp 3 ;einde pointer
  call .setspeed
  ret z
  
  inc e
  ld hl,marioslowdownrpp
  ld (hl),e


  ret

.resetslowdowntowalking:

  xor a
  ld (marioslowdownrpp),a
  ret


;/einde van alle vertragingen


.acceleratefromstandingstill:


;always reset the slowdown because there is no slowdown if we are accelerating
  xor a
  ld (marioslowdownpointer),a


;TODO: oeps deze routine vertraagd mario ten alle tijde ook wanneer je running ingedrukt houd...

;je moet dus kijken of de snelheid hoger is dan een bepaalde waarde en zo ja eerst de snelheid terugnemen
;voordat de max waarde word geset 

  ld a,(mariospeed)
  cp 4
  jp nc,.slowdowntowalking

;mario staat stil
  ld a,(mariospeed) 
  and a
  call z,.resetaccelerationpointer

  ld d,0
  ld hl,mariodspeedtablep
  ld e,(hl) 
   
  ld hl,mariodspeedtable 
  add hl,de
  ld a,(hl)
  cp 3 ;einde pointer
  call .setspeed
  ret z
  
  inc e
  ld hl,mariodspeedtablep
  ld (hl),e

 
   ret 

.resetaccelerationpointer:
  
  xor a
  ld (mariodspeedtablep),a

  ret
  

;Mario zijn speed word op running gezet. vergeleken en bij de hoogste waarde word de sprite waarde geset    
.running:

;zorgen dat mario goed vertraagd na het rennen 
  call .resetslowdowntowalking
  
;vanaf rennen pas accelereren wanneer we de basis snelheid hebben bereikt  
  
  ld a,(mariospeed)
  cp 3
  ret c
  
;check of mario in de lucht is. Zo ja dan mogen we niet versnellen
  ld a,(mariofloating)
  dec a
  jp z,.mariorunninginair
 
  ld a,(marioduck)
  dec a
  jp z,.accelerateonrampslide
 
 ;pointer resetten na klaar
  xor a
  ld (mariospeedtablerp),a
 
 
  ld d,0

  ld hl,mariospeedtablep
  ld e,(hl)

  ld hl,mariospeedtable
  add hl,de
  
  ld a,(hl) 
  
  cp 6
;  call z,.mariorunninginair
  jp z,.runningmax

  inc e
  ld hl,mariospeedtablep
  ld (hl),e
  
  ld (mariospeed),a
  ret


.accelerateonrampslide:

;prevent this from happening during normal tile operations
  ld a,(mariosheertile)
  and a
  ret z

  ld d,0

  ld hl,mariospeedtablerp
  ld e,(hl)

  ld hl,mariospeedtabler
  add hl,de
  
  ld a,(hl) 
  
  cp 8
;  call z,.mariorunninginair
  jp z,.runningmaxpower

  inc e
  ld hl,mariospeedtablerp
  ld (hl),e
   
 ;  ld a,20 ;test
  
  ld (mariospeed),a
  ret

.runningmaxpower:


  ld a,7
  ld (mariospeed),a
  ret

.runningmax:


;BUG: mario kan niet meer omsturen als hij op vliegt in de lucht bij een snelheid van 5!! WTF!!
  ld a,4
  ld (mariospeed),a


  ld a,(marioflying) 
  dec a
  ret z 


  ld a,5
  ld (mariospeed),a
  xor a
  ld (mariorunning),a
  
  
  ret
 
;mario zijn speedtablepointer moet resetten zodra hij springt maar zodrz zijn speed op 6
;weer juist niet 
;zodra je springt mag je niet versnellen maar bij de juiste snelheid moet wel worden
;doorgegeven dat mario zijn speed max is aan de engine 
.mariorunninginair:
 
  ld a,(mariorunning)
  and a
  jp z,.runningmax
 
  xor a
  ld (mariospeedtablep),a

  
  ret
 
.terminaterunning:  
  
;mario running pointer resetten want we zijn gestopt met rennen
  xor a
  ld (mariospeedtablep),a
   
  ld a,1
  ld (mariorunning),a
  
  ret

;defaultspeed setten 
.setdefaultspeed:


  ld a,3
    ld (mariospeed),a
  ret


.setspeed:

 ; ld a,1 ;DEBUG
  ld (mariospeed),a
  ret


.resetmarioslide:

;mario slide is ended


;op het moment dat de slide is beeindigd dus mario zijn snelheid behaald nul moet te allen tijde
;de sprite de andere kant op kijken je had immers de knop ingedrukt.

  ld a,(marioslide)
  dec a
  call z,.invertmarioleftright
    
  xor a
  ld (marioslide),a
   ld (mariofloatingpf),a
    
  ret

.invertmarioleftright:

  ld a,(mariofloatingpf)
  dec a
  ret z

;inversie plegen op mario zijn looprichting (hey ik kan ook een beetje wiskunde)
  ld a,(marioleftright)
  xor %00000001
  ld (marioleftright),a

  ret

.yoshifly:

  ld a,(Controls) ;move this one to the yoshifly routine after the checkstates are set
  bit 4,a
  ret z

;fall from start
  xor a
  ld (fallspeedtablep),a

;do not allow mario to exit the map from above
  ld hl,(marioy)
  ld a,h
  or l
  ret z

  call checktileabovemario
  ret c

  ld hl,(marioy)
  dec hl
  ld (marioy),hl


  ret


;als er geen jump word gedaan dan gaan we ervan uit dat we mogen vallen
;fall gaat dus automatisch door nadat er op jumpkeys is gecheckt.

.fall:
 
 
 ;jumpspeed resetten anders gaat mario hard zweven als je de jump te vroeg loslaat
 xor a
 ld (jumpspeed),a 
 ld (jumpingupsprite),a ;deze waarde resetten anders gaat alles fout

;mario is begonnen met vallen dus mag de jumproutine niks meer doen.
  ld a,1
  ld (marionotdown),a

;fly with yoshi
 ld a,(yoshiflying) ;move this one to the yoshifly routine after the checkstates are set
 and a
 call nz,.yoshifly

;heeft mario een veertje mariostate = 3 en is de jumptoets ingedrukt?

  ld a,(mariostate) ;eerst de state uitlezen 
  dec a ;was eerst 3 maar word dus 1 
  dec a 
  xor %00000001 ;laatste bit inverteren 
  ld b,a ;resultaat wegschrijven 
  
  ld a,(mariojumpkey) ;jumpkey uitlezen 
  xor %00000001 
  or b ;beide 0 en gehele resultaat 0?? 

  
  jp z,.fallslow
 
  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtable
  add hl,de
  ld a,(hl)
  cp 7 
;  ld b,a
  jp z,.maxspeed
 
;  ld a,2 ;DEBUG
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e
  
;in maxspeed word ook de resulterende fallspeed uitgelezen en opgeteld bij de marioy
.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 
;eens kijken of we van een schuine tile af komen
  ld a,(sheerfalltimer)
  cp 8
  call c,.sheerfall
 
 ;ook VAN TE VOREN checken of je al op een tile staat. zo ja dan mag er niets meer gebeuren
 ;naderhand ook checken anders lijkt het alsof je in de grond valt.
 call checktilebelowmario ;snelheid verdubbelen
 jp c,.nograv 
 
 ;we kijken dus of we op een sprite staan met clipping. Zo ja dan gaan we speciaal locken op die sprite 
 ld a,(standingonsprite)
 dec a
 jp z,.nogravwithspritelock
 
 
 xor a
 ld (tilelocked),a
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
;  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl


 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
 jp c,.nograv

 ;we kijken dus of we op een sprite staan met clipping. Zo ja dan gaan we speciaal locken op die sprite 
 ld a,(standingonsprite)
 dec a
 jp z,.nogravwithspritelock


   
  ret


.sheerfall:

;niet bij 0
  ld a,(sheerfalltimer)
  and a
  ret z

;niet bij groter dan 8  
  ld a,(sheerfalltimer)
  cp 8
  ret nc

;en ook niet op schuine tiles  
  ld a,(mariosheertile)
  and a
  ret nz

;locked?

  ld a,(tilelocked)
  dec a
  ret z

  ld a,(fallspeed)
  ld (mariospeed),a
  ret
  

;/hier word het vallen bepaald

;als we de jump toets vast blijven houden dan vallen we veel langzamer
.fallslow:

;fallspeedtablepointer resetten zodat bij een keyrelease de versnelling nog klopt
  xor a
  ld (fallspeedtablep),a 

;fallspeed locken
  ld a,2
  ld (fallspeed),a

;en gaan met die banaan  
  jp .maxspeed

  ret

  
.setrampjump:


    ld a,2
    ld (rampjump),a
    
  ret


.resetmariospeed:


  ld a,(mariosheerduck)
  and a
  ret nz

  xor a
  ld (mariospeed),a


  ret


.hardresetmarioduck:

  xor a
  ld (marioduck),a
  ld (mariosheerduck),a



  ret


.lowermariospeed:

  ld a,(mariospeed)
  cp 5
  ret nz
  
  ld a,4
  ld (mariospeed),a



  ret

  
;hier gebeuren alle jumps en bijbehorende problemen

.spinjump:

  ld a,(marioyoshi)
  ld b,a ;store the value for later reference

  ld a,0 ;this HAS to be ld,0 if you xor a here some wierd shit happens in the death routine...
  ld (marioyoshi),a
  ld (yoshiflying),a

;if mario is still on yoshi we ignore all the jump checks  
  ld a,b
  and a
  jp nz,.spinjumpanyway
  
;kleine check of mario nog vliegt. Dit voorkomt gezeik als we op allemaal toetsen gaan rammen terwijl
;mario nog in de lucht hangt we willen immers geen spinjump toelaten terwijl mario nog vliegt
  ld a,(mariofloating)
  dec a
  jp z,.jump

;en we mogen geen spinjumps toelaten wanneer wij een schelpje in ons handen hebben
  ld a,(marioshell)
  dec a
  jp z,.jump

.spinjumpanyway:

  call Playsfx.spinjump


;alle voorwaarden goed is dus spinjump zetten en toelaten
  ld a,1
  ld (mariospinjump),a
 
;even de pointer iets verder zetten omdat de spinjump iets minder hoog is  
  ld a,(mariojumpupp)
  add 16
  ld (mariojumpupp),a




.jump:



;prevent bunnyhopping

  ld a,(marioduck)
  and a
  call nz,.lowermariospeed


;reset the marioduck if mario is in a sheerslide
  ld a,(mariosheerduck)
  and a
  call nz,.hardresetmarioduck


  call Playsfx.jump

;effe slordig checken op een rampjump

    ld a,(rampjump)
    dec a
    call z,.setrampjump


;je mag nooit gebukt van een schuine tile af kunnen springen
  ld a,(mariosheertile)
  and a
  call nz,.skipduckchecks

;check of mario rent en zo ja dan gaan we vliegen

  ld a,(mariorunning)
  dec a
  call nz,.setmarioflying 

  call checkblockabovemario


;we checken de bovenkant om te kijken of mario wel verder kan jumpen
  call checktileabovemario
  call c,Playsfx.stomp
  jp c,.fall

  ld a,(jumpingupsprite)
  dec a
    call z,Playsfx.stomp
  jp z,.fall


;mario heeft nog niet zijn hoogste punt bereikt zodra dat wel zo is gaan we vallen ongeacht wat er gebeurd
  ld a,(marionotdown)
  dec a
  jp z,.fall

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  ld (mariofloatingpf),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 


;wanneer mario een rampjump doet zijn de regels anders
  ld a,(rampjump)
  cp 2
  jp z,.jumpupfromramp

;Ik moet wel. Binnen in de jumproutine moet worden nagekeken of mario nog wel jumpt. zo ja dan mag je hoger.

;key losgelaten is geen keyhold meer toestaan
  ld a,(mariojumprelease)
  dec a
  jp z,.jumpnormalonkeyrelease

 
  ld a,(mariojumpkey)
  dec a
  jp z,.jumponkeyhold


;key losgelaten klaar is kees
  ld a,1
  ld (mariojumprelease),a

  ;als mario dus rent op maximale snellheid gebruiken we een andere jump table 
  ld a,(mariorunning)
  and a
  jp z,.jumpupfromrunningmaxrelease 


.jumpnormalonkeyrelease:  

;normale jump wanneer we de toets hebben losgelaten
;wat is de snelheid van mario. Hoger dan 3 is rennen dus hoger jumpen  
  ld a,(mariospeed)
  cp 3
  jp nc,.jumpupfromrunningrelease


  ;we moeten checken of de pointer niet over een bepaalde boundary heen is
  ;zo ja dan gaan we gewoon fallen op keyrelease zonder discussie
;  ld a,(mariojumpupp)
;  cp 8 ;deze waarde moet maximaal zijn aan de maximum hoeveelheid slots beschikbaar in mariojumpupre
;  jp nc,.fall
 
 call getspinjumpvalue
 jp nc,.fall
  
  
  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariojumpupre 
  add hl,de
  ld a,(hl)
  cp 0 ;temp waarde moet nul zijn
  ld b,a
  jp z,.reenablegrav
 
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates
  
  ret



;als je de toets vasthoud dan gaat mario hoger
.jumponkeyhold:

;wat is de snelheid van mario. Hoger dan 3 is rennen dus hoger jumpen  
  ld a,(mariospeed)
  cp 4
  jp nc,.jumpupfromrunning 
  
  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariojumpup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates
  
  ret

;minimale springhoogte rensnelheid
.jumpupfromrunningrelease:


;  ld a,(mariojumpupp)
;  cp 8 ;deze waarde moet maximaal zijn aan de maximum hoeveelheid slots beschikbaar in mariojumpupre
;  jp nc,.fall


 call getspinjumpvalue
 jp nc,.fall


  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorjumpupre
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e


  jp .setjumpcoordinates
  
  ret
 

  
.jumpupfromrunning:


  ld a,(mariorunning)
  and a
  jp z,.jumpupfromrunningmax


  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorjumpup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e


  jp .setjumpcoordinates
  
  ret

;op volle vaart
.jumpupfromrunningmaxrelease:

;check of de pointer van de release op zijn maximum is en zo ja dan mogen we geen autojump meer doen!
;  ld a,(mariojumpupp)
;  cp 8 ;deze waarde moet maximaal zijn aan de maximum hoeveelheid slots beschikbaar in mariojumpupre
;  jp nc,.fall

 call getspinjumpvalue
 jp nc,.fall


  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorrjumpupre
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates

  ret 


;wanneer mario op max snelheid rent
.jumpupfromrunningmax:

  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorrjumpup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates

  ret 


.jumpupfromramp:


  ;we moeten checken of de pointer niet over een bepaalde boundary heen is
  ;zo ja dan gaan we gewoon fallen op keyrelease zonder discussie
;  ld a,(mariojumpupp)
;  cp 2 ;deze waarde moet maximaal zijn aan de maximum hoeveelheid slots beschikbaar in mariojumpupre
;  jp nc,.fall
  
  
  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariojumpupra 
  add hl,de
  ld a,(hl)
  cp 0 ;temp waarde moet nul zijn
  ld b,a
  jp z,.reenablegrav
 
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates


  ret


;hier worden de uiteindelijke mario coordinaten geset
.setjumpcoordinates:

  
  ;make sure that mario slows down ahead of the block
  push bc
  
  ld b,-2
  ld de,0
  call checkgeneraltilefromrom
  pop bc
  call c,.adjustjumpspeed


  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a


  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
  
;When mario jumps out of the map we must reset the ycoordinates at all time
;so basicly we must always check if there is a carry on 0 if so we do not write the new coordinates but simply 
;push a 0 into the mario coordinates
;WARNING: there are maps that propably will give a borrow because of their extreme height. We must change
;this call when that happens...

  ld a,h ;we have a borrow
  bit 7,a
  jp nz,.set0
   
  ret

.set0:

  ld hl,0
  ld (marioy),hl
  
  ret


.adjustjumpspeed:

  ld a,b
  cp 8
  ret c

  ld b,7
  ret
 

.setmarioflying:

  ;mariostate moet kloppen anders mag het niet
  ld a,(mariostate)
  cp 3
  ret nz

  ld a,1
  ld (marioflying),a

  ret

 
;we lock the marioy value on the yvalue of the sprite, first the ix value of the current sprite is fetched 
;then these coordinates are written into marioy 
.nogravwithspritelock:
  
  
;this was saved during the handleobjectmovement
  ld ix,(spriteixvalue)  
  
  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,24
  xor a 
  sbc hl,de
  ld (marioy),hl
  
;then we must always add the object speed to mario

  ld d,0
  ld a,(ix+v1)
  bit 7,a
  jp z,.nonegate
  neg 
  ld e,a
  ld hl,(mariox)
  xor a
  sbc hl,de
  ld (mariox),hl
  jp .nosubstract

.nonegate:
  ld e,a
  ld hl,(mariox)
 ; xor a
  add hl,de
  ld (mariox),hl

.nosubstract: 

 ;make a generic fast call preventing any unneeded overhead we handle the tiles from the handler itself
  ld a,(specialsheertile)
  and a
  call nz,handlespecialsheertile 
 
 
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (rampjump),a
  ld (sfxplaying),a ;TEMP
;  ld (mariospinjump),a ;hier al resetten anders eindigen we niet mooi
 
  ld a,(mariofloatingpf)
  dec a
  call z,.resetmarioslide

  call resetmariospinjump ;spinjump op 0 zetten en de richting on random veranderen 
 
  ld hl,0
  ld (jumpheightcounter),hl

;key checkout routine. Sommige acties mogen pas nadat een hele phycsics routine is gepaseert. Bijv een jump of fall
;we moeten dan ook weten of de toets ook is losgelaten. Dat geld in dit geval voor bukken en springen.
;Die mogen pas opnieuw NADAT de toetsen zijn losgelaten en mario aan de grond staat.
  ld a,(Controls)
  bit 1,a
  call z,.resetmarioduck
  

  ld a,(jumpkeyhold)
  dec a
  ret z


  xor a

  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a
;  ld (mariospinjump),a

  
  ret

 
 
  
;nograv word aangeroepen zodra mario een tile heeft aangeraakt aan de onderkant deze routine reset dan
;alle fall tables en set alle waarden zo dat mario opnieuw kan springen.
.nograv:

  ld a,(mariofloating)
  ld c,a
  ld a,(yoshistomp)
  and c
  call nz,marioflyingdroptoground.kaboom

  call lockmariotile ;eerst mario nog even locken op de tile
  ;hier moeten we checken of we een destroyable block hebben geraakt
  call checkforblockcrush
  jp c,mariointeract.setjump
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (rampjump),a
  ld (sfxplaying),a ;TEMP
  ld (uppersheercornerfound),a
;  ld (mariospinjump),a ;hier al resetten anders eindigen we niet mooi
 
  ld a,(mariofloatingpf)
  dec a
  call z,.resetmarioslide

  call resetmariospinjump ;spinjump op 0 zetten en de richting on random veranderen 
 
  ld hl,0
  ld (jumpheightcounter),hl

;key checkout routine. Sommige acties mogen pas nadat een hele phycsics routine is gepaseert. Bijv een jump of fall
;we moeten dan ook weten of de toets ook is losgelaten. Dat geld in dit geval voor bukken en springen.
;Die mogen pas opnieuw NADAT de toetsen zijn losgelaten en mario aan de grond staat.
  ld a,(Controls)
  bit 1,a
  call z,.resetmarioduck

;  ld a,(mariojumpkey)
;  dec a
;  ret z


  ld a,(jumpkeyhold)
  dec a
  ret z

;/wachten totdat spatie echt losgelaten is voordat de volgende jump mag

  xor a

  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a
;  ld (mariospinjump),a

  
  ret
  
  
;marionotdown word op 1 geset op het moment dat mario het hoogste punt heeft bereikt van zijn jump
;op dat moment kan mario alleen nog maar vallen totdat hij een tile heeft aangeraakt
 
.reenablegrav:

  ld a,1
  ld (marionotdown),a
  ret
  
.resetmarioduck:

;check of mario op een sheertile staat en zo ja dan mag dit niet gebeuren
;check ook of je Controls gebruikt lees maar direct uit ander word het echt te lastig
  call readkeydirect
  xor 255
	
  bit 7,a
  jp z,.skipduckchecks
  bit 4,a
  jp z,.skipduckchecks

  ld a,(mariosheertile)
  inc a ;kan 1 of twee zijn dus doen we een increment waardoor a = %0000001x
  srl a ;%0000000x
  dec a ;resultaat bepalen
  ret z ;bij nul staan we dus op een sheertile

;pas bij nul speed weer releasen wanneer we die sheerduck doen
;  ld a,(mariosheerduck)
;  ld b,a
;  ld a,(mariospeed)
;  and b
;  ret nz

;bij een sheerduck mogen we pas releasen als mario helemaal tot stilstand is gekomen
  ld a,(mariosheerduck)
  dec a
  jp z,.checkspeed
  


.skipduckchecks:

  xor a
  ld (marioduck),a  
  ld (mariosheerduck),a
  ld (slowdownfromrampend),a
     ;pointer resetten na klaar
  ld (mariospeedtablerp),a
  
  ret


.checkspeed:

  ld a,(mariospeed)
  and a
  jp z,.skipduckchecks
  
  ret
  

;===========================================[de complete physics wanneer mario gaat vliegen]========================
mariophysicsflying:

    
  ld a,(mariocoordinates+1)
  cp 240
  jp nc,.nograv ;On a static camera mario can fly out of the camera and then become into limbo. To prevent this at the very edge of the screen disable the flight



;branch naar special flying routine
  ld a,(marioflyspecial)
  dec a
  jp z,marioflyingspecial


;TODO: mariorunning word geterminate bij het veranderen van richting. Maar dat mag niet zolang je de running
;toets vastheb. De onderstaande testroutine werkt maar het moet anders kunnen..
  ld a,(mariorunning)
  ld c,a
  push bc


  ld a,(Controls)
  bit 2,a ;going left??
  call nz,mariophysics.mariowalkleft
  ld a,(Controls)
  bit 3,a ;going right??
  call nz,mariophysics.mariowalkright

  pop bc
  ld a,c
  ld (mariorunning),a 


;we bekijken het resultaat uit de voorgaande routines en gaan nu werkelijk de coordinaten zetten  
  ld a,(marioleftright)
  dec a
  call z,.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft
  

 ;altijd jumpen zodra de routine is geinitieerd en mariofloat 1 is
 ;
 ; ld a,(mariofloating)
 ; dec a
 ; jp z,.jump


  ld a,(marionotdown)
  sla a
  sla a
  sla a
  sla a
  ld c,a ;bit 4 tijdelijk wegschrijven
  sla a
  sla a
  add c ;bit 4 erbij optellen
  xor %11111111 ;resultaat inverteren
  ld c,a ;resultaat wegschrijven

  ld a,(Controls)
  and c
  bit 4,a
  jp nz,.jump
  bit 6,a
  jp nz,.spinjump


;alle keyread gerelateerd acties verricht. gravitatie toepassen
  jp .fall





.setmarioflyingspecial:

;not when mario is on yoshi
  ld a,(marioyoshi)
  dec a
  ret z

;bij niet meer rennen ook niet toestaan

  ld a,(mariorunning)
  dec a
  ret z

;eenmaal vallen is klaar met het verhaal geen special flight meer toestaan
  ld a,(fallspeed)
  cp 3
  ret nc

;geen spinjumps toelaten
  ld a,(mariospinjump)
  dec a
  ret z


  ld a,1
  ld (marioflyspecial),a
  ret



.moveleft:

;prevent mario from getting out of the map on the left
  ld hl,(mariox)
  ld de,8
  xor a
  sbc hl,de
  jp c,lockmariotilex


  call checktileleftmario
  jp c,lockmariotilex   
 
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
   
   ret


.moveright:

  call checktilerightmario
  jp c,lockmariotilerightx


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl
   ret





.spinjump:

  ld a,0
  ld (marioyoshi),a
  ld (yoshiflying),a

;kleine check of mario nog vliegt. Dit voorkomt gezeik als we op allemaal toetsen gaan rammen terwijl
;mario nog in de lucht hangt we willen immers geen spinjump toelaten terwijl mario nog vliegt
  ld a,(mariofloating)
  dec a
  jp z,.jump

;en we mogen geen spinjumps toelaten wanneer wij een schelpje in ons handen hebben
  ld a,(marioshell)
  dec a
  jp z,.jump

  call Playsfx.spinjump
  
;alle voorwaarden goed is dus spinjump zetten en toelaten
  ld a,1
  ld (mariospinjump),a
 
;even de pointer iets verder zetten omdat de spinjump iets minder hoog is  
  ld a,(mariojumpupp)
  inc a
  ld (mariojumpupp),a


.jump:

;prevent flying out of screen
  ld hl,(marioy)
  ld de,8
  xor a
  sbc hl,de
  jp c,.fall


;we checken de bovenkant om te kijken of mario wel verder kan jumpen
  call checktileabovemario
  jp c,.fall
  ld a,(mariospecialtile)
  dec a
  jp z,.fall ;tijdelijke branch hier word de speciale mario tile hit gedaan

  ld a,(jumpingupsprite)
  dec a
  jp z,.fall


;mario heeft nog niet zijn hoogste punt bereikt zodra dat wel zo is gaan we vallen ongeacht wat er gebeurd
  ld a,(marionotdown)
  dec a
  jp z,.fall

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  ld (mariofloatingpf),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 


;Ik moet wel. Binnen in de jumproutine moet worden nagekeken of mario nog wel jumpt. zo ja dan mag je hoger.

  ld a,(mariojumpkey)
  dec a
  jp z,.jumponkeyhold


;jump up normal on keyrelease:  
  ;we moeten checken of de pointer niet over een bepaalde boundary heen is
  ;zo ja dan gaan we gewoon fallen op keyrelease zonder discussie
;check of de pointer van de release op zijn maximum is en zo ja dan mogen we geen autojump meer doen!
  ld a,(mariojumpupp)
  cp 8  ;deze waarde moet maximaal zijn aan de maximum hoeveelheid slots beschikbaar in mariojumpupre
  jp nc,.fall


  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorrjumpupre
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates

  ret 



;als je de toets vasthoud dan gaat mario hoger
.jumponkeyhold:


  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorjumpup
  add hl,de
  ld a,(hl)
  cp 5  
  ld b,a
  ;jp z,.reenablegrav ;geen gravity mario vliegt
  jp z,.setjumpcoordinates
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates
  
  ret


;hier worden de uiteindelijke mario coordinaten geset
.setjumpcoordinates:

  ;mario mag niet hoger kunnen dan de bovenkant van de map dus wanneer de sprite het topje dreigt aan te raken
  ;gaan we gewoon vallen en doen we niets meer met de coordinaten van mario
  ld a,(mariocoordinates)
  cp 5
  jp c,.reenablegrav
  
  ;jumpcounter

  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a
 
  ld hl,(jumpheightcounter)
  ld c,b
  ld b,0
  add hl,bc ;jumpspeed in 16 bits
  ;register h word 1 bij 256+ we kunnen dus een and operatie doen
  ld (jumpheightcounter),hl ;wegschrijven voor later
  ld b,h ;transfer voor berekening hl->bc
  ld c,l
  ld hl,300 ;maximale jumpheight
  xor a ;reset carry
  sbc hl,bc ;300-jumpheight
  jp c,.reenablegrav ;niet meer jumpen als 300 is overschreden
  
  
  ld a,(jumpspeed)
  ld b,a
  
  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
  
  
  ret


;als er geen jump word gedaan dan gaan we ervan uit dat we mogen vallen
;fall gaat dus automatisch door nadat er op jumpkeys is gecheckt.

.fall:
 
 ;jumpspeed resetten anders gaat mario hard zweven als je de jump te vroeg loslaat
 xor a
 ld (jumpspeed),a 
 ld (jumpingupsprite),a ;deze waarde resetten anders gaat alles fout

;mario is begonnen met vallen dus mag de jumproutine niks meer doen.
  ld a,1
  ld (marionotdown),a

;zodra we beginnen te vallen gaan we in de special mode maar niet als we een schelpje hebben
  ld a,(marioshell)
  xor %00000001
  rlca
  rlca
  rlca
  rlca
  rlca
  ld b,a
  ld a,(Controls)
  and b
  bit 5,a
  call nz,.setmarioflyingspecial


  ld a,(mariojumpkey)
  dec a
  jp z,.fallslow

  
  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtable
  add hl,de
  ld a,(hl)
  cp 7
  ld b,a
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e

;TODO: hey wacht is effe valt mario wel!!?? in deze flying rouine!!??
  
;in maxspeed word ook de resulterende fallspeed uitgelezen en opgeteld bij de marioy
.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 
; call checktilebelowmario
; call c,.nograv
 
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
;  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
 call c,.nograv
   
  ld a,(standingonsprite)
  and a
  call nz,.nograv 
     
  ret
;/hier word het vallen bepaald


;als we de jump toets vast blijven houden dan vallen we veel langzamer
.fallslow:

;fallspeedtablepointer resetten zodat bij een keyrelease de versnelling nog klopt
  xor a
  ld (fallspeedtablep),a 

;fallspeed locken
  ld a,2
  ld (fallspeed),a

;en gaan met die banaan  
  jp .maxspeed

;ret

;nograv word aangeroepen zodra mario een tile heeft aangeraakt aan de onderkant deze routine reset dan
;alle fall tables en set alle waarden zo dat mario opnieuw kan springen.
.nograv:

  call lockmariotile ;eerst mario nog even locken op de tile
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (marioflying),a ;einde vliegverhaal
  ld (marioflydrop),a ;droppen is ook voorbij
  ld (marioflyspecial),a 
  
  call mariophysics.resetmarioslide
   
  ld hl,0
  ld (jumpheightcounter),hl

;key checkout routine. Sommige acties mogen pas nadat een hele phycsics routine is gepaseert. Bijv een jump of fall
;we moeten dan ook weten of de toets ook is losgelaten. Dat geld in dit geval voor bukken en springen.
;Die mogen pas opnieuw NADAT de toetsen zijn losgelaten en mario aan de grond staat. 

  ld a,(mariojumpkey)
  dec a
  ret z
;/wachten totdat spatie echt losgelaten is voordat de volgende jump mag

  xor a

  ld (mariojumpupp),a
  ld (marionotdown),a
  ld (mariospinjump),a

  
  ret
  
  
;marionotdown word op 1 geset op het moment dat mario het hoogste punt heeft bereikt van zijn jump
;op dat moment kan mario alleen nog maar vallen totdat hij een tile heeft aangeraakt


.reenablegrav:


  ld a,1
  ld (marionotdown),a
  ret
  
 
 
 
;==============================================[mario vliegt rechtdoor]============================================== 
marioflyingspecial:


  ld a,(marioflydrop)
  dec a
  jp z,marioflyingdroptoground


;we hebben een slide afgedwongen dus we doen niets anders meer
  ld a,(marioflyslide)
  dec a
  jp z,.slowdownfromair

;deze routine stoppen zodra we rennen loslaten
;maar wel de speed vasthouden zolang we vliegen dus deze toets vasthebben
  ld a,(Controls)
  bit 5,a
  call nz,.setmariospeedmax
  call z,.resetmarioflyingspecial

;niet toelaten dat mariorunning kan worden geterminate
  xor a
  ld (mariorunning),a
  
;misschien kan deze weg in de toekomst
;we zetten pas de maxspeed als mario niet op zijn hardst valt dat voorkomt wierde versnellingen tijdens de sprong
  ld a,(fallspeed)
  cp 4
  call c,.setmariospeedmax
  

;we kijken welke richting we opgingen
  ld a,(marioleftright)
  dec a
  call z,.moveright
;  call z,mariophysics.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft
 ; call z,mariophysics.moveleft


;het is de een of de ander en als we dus links en recht ingedrukt houden ondernemen we gewoon geen actie.
;dit voorkomt dat mario hem helemaal ballen tript als je op de toetsen gaat rammen
  ld a,(Controls)
  and %00001100
  cp 12 
  call z,.resetkey

   
;d = 1 mario gaat hoger d = 0 mario gaat droppen

 
  ld d,0 ;deze waarde moet echt geset worden anders weet mario niet van ophouden ;)
  
  ;mario naar links??
  ld a,(marioleftright)
  dec a
  call z,.readkeyright
  
  ld a,(marioleftright)
  and a
  call z,.readkeyleft
  
  ld a,d
  ld (marioflyup),a ;het resultaat van d opslaan in het geheugen

;en dan de jump toepassen indien nodig

;voor de grotere jump uiteraard
  ld a,(mariojumpuppd)
  cp 17
  jp z,.fall

  ld a,(mariojumpupp)
  cp 17
  jp z,.fall

  ld a,(marioflyhold)
  dec a
  jp z,.jump


  ld a,(marioflyup)
  dec a
  call z,Playsfx.marioflyrise
  jp z,.jump
  
;/bovenstaand horen bij elkaar scheiden van deze routines resulteerd in een uberbug!

;alle keyread gerelateerd acties verricht. gravitatie toepassen
  jp .fall


;speciale tilexlocker anders snapt de engine het gewoon niet meer
.lockmariotilex:

;nog netter als we niet kunnen rennen terwijl we tegen een muur staan
  xor a
  ld (mariospeed),a
  ld (marioflyslide),a ;slide stopt anders gaan we wierd ass bugs krijgen

  call mariophysics.terminaterunning

  call .resetmarioflyingspecial

  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  add 2 ;twee erbij
  ld (mariox),a
  
  ret

.lockmariotilerightx:

;nog netter als we niet kunnen rennen terwijl we tegen een muur staan
  xor a
  ld (mariospeed),a
  ld (marioflyslide),a ;slide stopt anders gaan we wierd ass bugs krijgen

  call mariophysics.terminaterunning

  call .resetmarioflyingspecial

  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  xor %00000111 ;laatste 7 erbij
  sub 2 ;twee eraf 
  ld (mariox),a
  
  ret



.mariodrop:

  ld a,(fallspeed)
  cp 3
  ret c

;anders tript de engine hem ballen omdat hij dan constant tussen de drop en de normale routine blijft
;schakelen 
  ld a,(marioflyupfromdrop)
  dec a
  ret z
  
;voor de zekerheid even de fallspeedtable resetten  
  xor a
  ld (fallspeedtablep),a 
  
  ld a,1
  ld (marioflydrop),a

  ret

;wip ;mario moet wat minder tolerant zijn met opjumpen

.readkeyleft:

  
  ld a,(Controls)
  bit 2,a
 ; call nz,.mariodrop
  jp nz,.mariodrop
   
  ld a,(Controls)
  bit 3,a
  ret z

  ld a,(marioflyupfromdrop)
  dec a
  jp z,.setdvalue

  ld d,2 ;andere waarde voor fallslow

  ld a,(fallspeed)
  cp 4;3
  ret c

  ld d,1


  ret


.readkeyright:

 
  ld a,(Controls)
  bit 3,a
;  call nz,.mariodrop
  jp nz,.mariodrop

  
  ld a,(Controls)
  bit 2,a
  ret z

  ld a,(marioflyupfromdrop)
  dec a
  jp z,.setdvalue

 
  ld d,2 ;andere waarde voor fallslow
   
;eerst moet je een beetje fallen voordat je mag opspringen 
  ld a,(fallspeed) 
  cp 4;3
  ret c

  ld d,1

  ret


.setdvalue:

  ld d,1
  ret


.resetkey:

  ld a,(Controls)
  and %11110011
 ld (Controls),a
  ret


.setmariospeedmax:

  ld a,5
  ld (mariospeed),a

  ret

.resetmarioflyingspecial:

  xor a
  ld (marioflyspecial),a
  ld (fallspeedtablep),a
  ret


.moveleft:

;prevent mario from getting out of the map on the left
  ld hl,(mariox)
  ld de,8
  xor a
  sbc hl,de
  jp c,.lockmariotilex



  call checktilebelowmarioahead
   jp c,.lockmariotilerightx


  call checktileleftmario
  jp c,.lockmariotilex   


  ld a,(specialsheertile) ;NEW COMMIT
  and a
  jp nz,.nograv
  
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
   
   ret


.moveright:

 ;  ld a,(mariocoordinates)
 ;  cp 10
 ;  ret nc


;prevent mario from getting out of the map on the right
  ld hl,(maplenghtx8)
  ld de,24
  xor a
  sbc hl,de
  ld de,(mariox)
  xor a
  sbc hl,de
;  call c,.removeshell ;get rid of the shell since the object is out of screen
  jp c,.lockmariotilerightx


  call checktilebelowmarioahead
   jp c,.lockmariotilerightx

  call checktilerightmario
  jp c,.lockmariotilerightx

  ld a,(specialsheertile) ;NEW COMMIT
  and a
  jp nz,.nograv


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl
   ret


.jump:
  
 
  ld a,1
  ld (marioflyhold),a
 
;ik weet niet meer waarom ik dit heb gedaan... ik denk dat dit stukje op kan zouten. 
;  ld a,(fallspeed)
;  cp 4
;  jp nc,.fall


    ;mario mag niet hoger kunnen dan de bovenkant van de map dus wanneer de sprite het topje dreigt aan te raken
  ;gaan we gewoon vallen en doen we niets meer met de coordinaten van mario
  ;De jump mag niet plaatsvinden wanneer mario de top van de map aanraakt
  ld a,(mariocoordinates)
  cp 5
  jp c,.fall


;we checken de bovenkant om te kijken of mario wel verder kan jumpen
  call checktileabovemario
  jp c,.fall
  ld a,(mariospecialtile)
  dec a
  jp z,.fall ;tijdelijke branch hier word de speciale mario tile hit gedaan

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a

 ;fallspeedtable resetten want we zitten immers in een jump
  
  xor a
  ld (fallspeedtablep),a 
  ld a,2
  ld (fallspeed),a
   
 
  ld a,(marioflyupfromdrop)
  dec a
  jp z,.jumpupfromflydrop
  
  
  ;we zorgen ervoor dat de jumpupfromflydrop zich niet kan herhalen.
  xor a
  ld (marioflyupfromdrop),a

  ld d,0
  ld hl,mariojumpupp
  ld e,(hl)
  
  ld hl,mariorrjumpupfs
  add hl,de
  ld a,(hl)
  cp 0 
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpupp
  ld (hl),e

  jp .setjumpcoordinates

 ; ret 
  
;wip: speciale jump wanneer je hard dropt
.jumpupfromflydrop:

  ld d,0
  ld hl,mariojumpuppd
  ld e,(hl)
  
  ld hl,mariojumpupdrop
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,mariojumpuppd
  ld (hl),e

  jp .setjumpcoordinates

  ret 


;hier worden de uiteindelijke mario coordinaten geset
.setjumpcoordinates:


  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a
  
  
  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
 
  
  ret


.killjumpuppointer:

;checkout routine voor de keypresses  
  ld a,(Controls)
  bit 2,a
  ret nz
  bit 3,a
  ret nz


  xor a ;kill de jumpup pointer
  ld (mariojumpupp),a
  ld (mariojumpuppd),a
  ld (marioflyupfromdrop),a


  ret


;als er geen jump word gedaan dan gaan we ervan uit dat we mogen vallen
;fall gaat dus automatisch door nadat er op jumpkeys is gecheckt.

.fall:
 ;jumpspeed resetten anders gaat mario hard zweven als je de jump te vroeg loslaat
 xor a
 ld (jumpspeed),a 
 
 ;mario jump niet meer
 ld (marioflyhold),a

;langzaam vallen wanneer we de toets vasthouden
  ld a,(marioflyup)
  dec a
  jp z,.fallslow
    ld a,(marioflyup)
  cp 2
  jp z,.fallslow

  call .killjumpuppointer ;jumpuppointer killen op basis van keypresses is dus een speciale checkout routine

.fallnormal:
  
  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtablesp
  add hl,de
  ld a,(hl)
  cp 6 
  ld b,a
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e
  
;in maxspeed word ook de resulterende fallspeed uitgelezen en opgeteld bij de marioy
.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
;  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
 jp c,.nograv

   
  ret
;/hier word het vallen bepaald


;als we de jump toets vast blijven houden dan vallen we veel langzamer
.fallslow:
  
;niets toelaten wanneer we al te hard vallen
;  ld a,(fallspeed)
;  cp 4
;  jp nc,.fallnormal

;fallspeedtablepointer resetten zodat bij een keyrelease de versnelling nog klopt
  xor a
  ld (fallspeedtablep),a 

;fallspeed locken
  ld a,2
  ld (fallspeed),a

;en gaan met die banaan  
  jp .maxspeed

  ret

;nograv word aangeroepen zodra mario een tile heeft aangeraakt aan de onderkant deze routine reset dan
;alle fall tables en set alle waarden zo dat mario opnieuw kan springen.

.nograv:

  call lockmariotile ;eerst mario nog even locken op de tile
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk 
  ld (marioslowdownrdp),a ;mochten we een slide moeten doen
  ld (mariojumpupp),a
  ld (mariojumpuppd),a
  ld (marionotdown),a
  ld (mariospinjump),a
  ld (marioflydrop),a
  ld (marioflyupfromdrop),a ;voor de zekerheid

  ld hl,0
  ld (jumpheightcounter),hl

;vanaf hier moeten we de mariospeed checken en bij fullspeed een monsterlijke slide initieren

  ld a,(specialsheertile)
  and a
  jp nz,.noslide

  ld a,(mariospeed)
  cp 5
  jp z,.setmarioslide

.noslide:

  xor a
  ld (marioflyspecial),a ;deze hele routine killen  
  ld (marioflying),a ;einde vliegverhaal
  
  ret



.slowdownfromair:

;slide cancellen als we een muurtje tegenkomen
  call checktilerightmario
   jp c,.lockmariotilex
  call checktileleftmario
   jp c,.lockmariotilerightx

  ld a,(standingonsprite)
  and a
  call nz,.resetmarioslide

;als we een shuine tile aanraken gaan we niet sliden. Dan hebben we gewoon dikke pech zodat we
;niet door de grond pleuren en dat soort ongein.
  ld a,(mariosheertile)
  and a
  call nz,.resetmarioslide
  ;nogmaals een check doen en deze hele routine overslaan als we in een sheertile staan
   ld a,(mariosheertile)
  and a
  jp nz,.fall ;geen reset ander blijven we hangen

;hoekje checken is HEEEEEEL belangrijk!!!!
  ld a,(tilecorner)
  and a
  jp nz,.resetmarioslide 

  call .fallspecial
  
  ld a,(mariosecondslide)
  dec a
  jp z,.skipfallspeedcheck
  
  
  ld a,(fallspeed)
  and a
  ret nz 
  
  
.skipfallspeedcheck: 
  
  ld d,0
  ld hl,marioslowdownrdp
  ld e,(hl)
   
  ld hl,marioslowdownff 
  add hl,de
  ld a,(hl)
  cp 0 ;einde pointer
  call .setspeed
  jp z,.resetmarioslide
  
  inc e
  ld hl,marioslowdownrdp
  ld (hl),e

  ld a,(marioleftright)
  dec a
  call z,.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft

  ret 


.setspeed:

  ld (mariospeed),a
  ld a,1
  ld (mariosecondslide),a
  
 ret

;we moeten bijzonder vallen wanneer we een dikke slide maken bij neerkomen zodat we geen airhangs krijgen
.fallspecial:


  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtablesp
  add hl,de
  ld a,(hl)
  cp 6 
  ld b,a
  jp z,.maxspeedspecial
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e

.maxspeedspecial: 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld b,a
 
  ld hl,(marioy)
  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
  call c,.resetfallspeed

  ld a,(standingonsprite)
  and a
  call nz,.resetfallspeed

   
  ret
;/hier word het vallen bepaald

.resetfallspeed:

  call lockmariotile ;ALTIJD LOCKEN!! anders pleuren we door de grond

  xor a
  ld (fallspeed),a

;even fallspeed table ressen  
  ld (fallspeedtablep),a 
  
  ret



;we zetten een slide in zolang de speed nog volledig is dan is er geen weg meer terug totdat de slide voorbij is
.setmarioslide:

  ld a,(marioflydrop)
  dec a
  ret z


  ld a,1
  ld (marioflyslide),a


  ret


.resetslidepointer:

   
;we resetten van te voren de slowdown pointer mochten we vallen dan sliden we namelijk opnieuw
;maar we willen wel dat de pointer een beetje hoog staat anders sliden we te ver door!
  ld a,15
  ld (marioslowdownrdp),a


  ret

.resetmarioslide:
  xor a
  ld (mariosecondslide),a ;HET WERKT!!


;in de lucht kun je nog doorsliden en dan stopt het op een verkeerd moment dus fallspeed moet 0 zijn 
  ld a,(fallspeed)
  and a
;  ret nz
  jp nz,.resetslidepointer

  xor a
  ld (marioflyslide),a
  ld (marioflyspecial),a ;deze hele routine killen  
  ld (marioflying),a ;einde vliegverhaal
  ld (marioflydrop),a
  ;anders kun je jumpen in mid air na deze slide
  ld a,1
  ld (marionotdown),a
  call mariophysics.terminaterunning ;runnen terminaten na deze slide
  
  ret



.reenablegrav:

  xor a
  ld (marioflyupfromdrop),a
;  ld (marioflyhold),a

;de andere branches moeten weten dat je weer valt
  ld a,1
  ld (marionotdown),a
  ret



;=====================================wanneer mario uit de lucht dropt========================================

;wip

marioflyingdroptoground:

  ld a,(standingonsprite)
  and a
  jp nz,.nograv


;we kijken welke richting we opgingen
  ld a,(marioleftright)
  dec a
  call z,.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft

;  call slowmariodown
;  call slowmariodown.activateslowdownfromair
  jp .fall 


;mario moet nog wel kunnen sliden in de air
;TODO: leuk bedacht maar wat als wij simultaan keys gaan rammen :O
.moveleft:

  ld a,(Controls)
  bit 3,a
  jp nz,.jumpup

  ld hl,(mariox)
  ld de,16
  sbc hl,de
  jp c,.lockmariotilex 


  call checktileleftmario
  jp c,.lockmariotilex   
 
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
   
;als we geen toetsen indrukken gaat mario terug vanuit de drop     
  ld a,(Controls) 
  and %00001100
;  xor %00001100
  jp z,.nokeypressed

  xor a
  ld (keypresscounter),a
   
   
   ret


.moveright:

  ld a,(Controls)
  bit 2,a
  jp nz,.jumpup

  ld hl,(maplenghtx8)
  ld de,16
  xor a
  sbc hl,de
  xor a
  ld de,(mariox)  
  sbc hl,de
  jp c,.lockmariotilerightx

  call checktilerightmario
  jp c,.lockmariotilerightx


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl
   
  
  ld a,(Controls) 
  and %00001100
;  xor %00001100
  jp z,.nokeypressed

  xor a
  ld (keypresscounter),a
   
   ret

;speciale tilexlocker anders snapt de engine het gewoon niet meer
.lockmariotilex:

;nog netter als we niet kunnen rennen terwijl we tegen een muur staan
  xor a
  ld (mariospeed),a

  call mariophysics.terminaterunning

  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  add 2 ;twee erbij
  ld (mariox),a
  
  ret


.lockmariotilerightx:

;nog netter als we niet kunnen rennen terwijl we tegen een muur staan
  xor a
  ld (mariospeed),a

  call mariophysics.terminaterunning

  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  xor %00000111 ;laatste 7 erbij
  sub 2 ;twee eraf 
  ld (mariox),a
  
  ret


.fall:
 
 ;jumpspeed resetten anders gaat mario hard zweven als je de jump te vroeg loslaat
 xor a
 ld (jumpspeed),a 
  
  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtablespd
  add hl,de
  ld a,(hl)
  cp 7  
  ld b,a
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,fallspeedtablep
  ld (hl),e
  
;in maxspeed word ook de resulterende fallspeed uitgelezen en opgeteld bij de marioy
.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!

 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
 jp c,.nograv

 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
;  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 call checktilebelowmario
 jp c,.nograv


  ret


;steeds sneller naar beneden donderen 
;.slowdownfromair:
;  
;  ld d,0
;  ld hl,marioslowdownrdp
;  ld e,(hl)
;   
;  ld hl,marioslowdownffd 
;  add hl,de
;  ld a,(hl)
;  cp 0 ;einde pointer
;  call mariophysics.setspeed
;;  jp z,.resetmarioslide
;  ret z
;  
;  inc e
;  ld hl,marioslowdownrdp
;  ld (hl),e
;
;ret  

.nograv:

  call lockmariotile ;eerst mario nog even locken op de tile
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk 
  ld (marioslowdownrdp),a ;mochten we een slide moeten doen
  ld (mariojumpupp),a
  ld (marionotdown),a
  ld (mariospinjump),a
  ld (marioflydrop),a
  ld (marioflyspecial),a ;deze hele routine killen  
  ld (marioflying),a ;einde vliegverhaal
  ld (marioflyupfromdrop),a
  
  ld hl,0
  ld (jumpheightcounter),hl
  ;alle slowdownpointer killen anders krijg je van die vreemde loopjes
;  ld a,3
;  ld (marioslowdownp),a
;  ld a,7
;  ld (marioslowdownrp),a
  call mariophysics.terminaterunning ;rennen terminaten

.kaboom:  
  
;spawn a huge object on the floor below mario that kills other objects
  
  ld hl,(mariox) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8  

  ld de,6
  xor a
  sbc hl,de

  ld de,(marioy) ;16 bits

  srl d ;nu moet je hl delen door 8
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8  

 ; inc e
 ; inc e

;create the object at mario that kills the other objects 
 
  push ix
    
  ld    iy,CustomObject
  ld    (iy),available 
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),e                ;set y custom object
  ld    (iy+4),d                ;set y custom object
  ld    (iy+5),39               ;set object number
  ld    (iy+6),0               ;set object number

;set memory correct
  ld a,romprogblock
  ld (romprogblocknumber),a

  call  activateobject

  ld (ix+deathtimer),0

  pop ix
 
  call Playsfx.explosion
 
  ld a,1
  ld (tripscreen),a ;bitje setten zodat de event handler weet dat het scherm moet trippen  
    
  ret


;we maken toch nog een jump dan gelden er dus andere regels
.jumpup:



  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk 
  ld (marioslowdownrdp),a ;mochten we een slide moeten doen
  ld (mariojumpupp),a
;  ld (marionotdown),a
  ld (mariospinjump),a
  ld (marioflydrop),a
  
  ;dit kunnen we pas toelaten wanneer we een minimum snelheid hebben bereikt
  ld d,0
  ld hl,fallspeedtablep
  ld e,(hl)
  
  ld hl,fallspeedtablespd
  add hl,de
  ld a,(hl)
  cp 6
  ret c
  
  ld a,1
  ld (marioflyupfromdrop),a ;even aangeven dat we een speciale jump gaan doen


  
  ret
 
;wanneer we helemaal geen toetsen vasthebben gaat mario wat langzamer vallen en niet meer droppen
.nokeypressed:


  ld a,(keypresscounter)
  inc a
  ld (keypresscounter),a

    cp 7 ;was 5 maar 7 geeft iets lekkerder mee bij reversed keypresses
    ret c


  xor a
  ld (marioflydrop),a
  
  ret
  
 
 
;===================================================================================================================== 
;De complete mario physics wanneer mario zich in het water bevind
;=====================================================================================================================
;
; bit	7	  6	  5		    4		    3		    2		  1		  0
;		    	Z 	C 		  X   	  right	  left	down	up	(keyboard)
;
;TODO: alles weghalen wat wij niet nodig hebben!

mariophysicswater:
 
;alle control worden uitgelezen en zo worden de juiste fysics toegepast 
  
  ;beide toetsen links en rechts NIET ingedrukt dan gaan we mario vertragen
  ld a,(Controls)
  ld (controlbuffer),a ;buffer Controls for later checks
  and %00001100
  call z,.slowdownstandingstill
  
 ;speciale check of links OF rechts is ingedrukt. zo niet dan kunnen wij ook niet meer rennen
;  ld a,(Controls)
;  and %00001100
;  call z,.terminaterunning
    call .terminaterunning
  
;nog even controleren of wij stilstaan anders blijft onze sprite hangen in de slide vorm 
  ld a,(mariospeed)
  and a
  call z,.resetmarioslide 

;check of mario omhoog kijkt en omlaag
  xor a
  ld (mariolook),a
  ld (marioduck),a

  ld a,(Controls)
  bit 0,a
  call nz,.mariolook
  ld a,(Controls)
  bit 1,a
  call nz,.mariodown

  
;terminate het rennen zodra de toets is losgelaten anders is cheaten mogelijk
;door eerst de ren toets ten alle tijden ingedrukt te houden zodat je altijd fullspeed houd  
 ; ld a,(Controls)
 ; bit 5,a ;running m STILL pressed??
 ; call z,.terminaterunning

  ld a,(watercurrent)
  dec a
  call z,.setkeyleft

;gaan wij naar links of naar rechts??
 
  ld a,(Controls)
  bit 2,a ;going left??
  call nz,.mariowalkleft
  ld a,(Controls)
  bit 3,a ;going right??
  call nz,.mariowalkright
 
;gaan wij rennen?? 
 ; ld a,(Controls)
 ; bit 5,a
 ; call nz,.running 
 
;we bekijken het resultaat uit de voorgaande routines en gaan nu werkelijk de coordinaten zetten  
  ld a,(marioleftright)
  dec a
  call z,.moveright
  
  ld a,(marioleftright)
  and a
  call z,.moveleft


;ik denk dat we iets moeten gaan inbouwen dat mario altijd weer mag jumpen nu word er puur op de Controls
;gekeken maar we zouden eigenlijk een bitje moeten setten die kijkt wat een jump doet.  


  ld a,(Controls)
  bit 4,a
  call z,.resetmarionotdown

  ld a,(Controls)
  bit 4,a
  jp nz,.jump
;  bit 6,a ;BUG: for some reason the jump with the spinjump button does wierd things...
;  jp nz,.jump


;alle keyread gerelateerd acties verricht. gravitatie toepassen
  jp .fall


;/einde van alle Controls checks


.setkeyleft:

  ld a,(Controls)
  set 2,a
  ld (Controls),a

  ret


;zorgen datje altijd opnieuw kan kjumpen
.resetmarionotdown:

  xor a
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  ld (marioswimupp),a
  ld (marionotdown),a
  ret


.resetjump:

  call lockmariotile ;eerst mario nog even locken op de tile
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (marioswimdownp),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk


;key checkout routine. Sommige acties mogen pas nadat een hele phycsics routine is gepaseert. Bijv een jump of fall
;we moeten dan ook weten of de toets ook is losgelaten. Dat geld in dit geval voor bukken en springen.
;Die mogen pas opnieuw NADAT de toetsen zijn losgelaten en mario aan de grond staat.
  ld a,(Controls) 
  bit 4,a
  ret nz
  ld a,(Controls) 
  bit 6,a
  ret nz
;/wachten totdat spatie echt losgelaten is voordat de volgende jump mag

  xor a


  ld (marioswimupp),a
  ld (marionotdown),a
;  ld (mariospinjump),a
 
  ret



;zet alleen de variabelen
.mariolook:

  ld a,1
  ld (mariolook),a
  ret


.mariodown:


;alle condities goed dan mogen we bukken pas bij checkout word het gecancelled
  ld a,1
  ld (marioduck),a
  ret


.moveleft:


;prevent mario from getting out of the map on the left
  ld hl,(mariox)
  ld de,8
  xor a
  sbc hl,de
 ; call c,.removeshell ;get rid of the shell since the object is out of screen
  jp c,lockmariotilex


  ld a,(touchside)
  and a
  jp nz,lockmariotilex


  call checktileleftmario
  jp c,lockmariotilex   

;TODO: add some check that checks for currents in the water and causes the appropiate effects
  ld a,(watercurrent)
  dec a
  call z,.addcurrentleft
 
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
   
   ret

.addcurrentleft:

  ld a,(controlbuffer)
  bit 2,a
  ret z

  ld a,(mariospeed)
  inc a
 ; cp 4
 ; ret nc
  ld (mariospeed),a

  ret

.addcurrentright:

  ld a,(marioshell)
  dec a
  ret z
  
  ld a,1
  ld (mariospeed),a



  ret


.moveright:

  ld a,(touchside)
  and a
  jp nz,lockmariotilerightx


  call checktilerightmario
  jp c,lockmariotilerightx


   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl
   ret




.setholdspritedirleft:

  ld a,(mariosheerhold)
  dec a
  ret z 

  ld a,1
  ld (holdspritedir),a
  xor a
  ld (marioleftrightspr),a

  ret
  
  
.setholdspritedirright:

  ld a,(mariosheerhold)
  dec a
  ret z

  ld a,1
  ld (holdspritedir),a
  ld (marioleftrightspr),a

  ret  

.checkkey:


  ld a,(controlbuffer)
  bit 2,a
  ret z

  ld a,(marioyoshi)
  dec a
  jp z,.setturnyoshileft


  ld a,(marioshell)
  and a
  ret z

  ld a,(marioleftrightsprw)
  and a
  ret z


  jp .dothat


.setturnshellleft:

  ld a,(watercurrent)
  dec a
  jp z,.checkkey


  ld a,(marioyoshi)
  dec a
  jp z,.setturnyoshileft


  ld a,(marioshell)
  and a
  ret z


  ld a,(marioleftrightspr)
  and a
  ret nz
  
;als je puur naar links beweegt  
 
  ld a,(marioleftright)
  and a
  ret z

.dothat:   
  
  ld a,(marioturnshell)
  and a
  ret nz
  
  ld a,(turnshellactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnshell),a
  ld (turnshellactive),a


  ret

.setturnyoshileft:

  ld a,(marioyoshi)
  and a
  ret z


  ld a,(marioleftrightspr)
  and a
  ret nz
  
;als je puur naar links beweegt  
 
  ld a,(marioleftright)
  and a
  ret z
  
  
  ld a,(marioturnyoshi)
  and a
  ret nz
  
  ld a,(turnyoshiactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnyoshi),a
  ld (turnyoshiactive),a


  ret


.setturnshellright:

  ld a,(marioyoshi)
  dec a
  jp z,.setturnyoshiright


  ld a,(marioshell)
  and a
  ret z

;  ld a,(marioleftrightspr)
;  and a
;  ret z
  
  
  ld a,(marioleftrightsprw)
  and a
  ret nz
  
  
;als je puur naar rechts beweegt  
  ld a,(marioleftright)
  and a
  ret nz
 
  
  ld a,(marioturnshell)
  and a
  ret nz
  
  ld a,(turnshellactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnshell),a
  ld (turnshellactive),a


  ret

.setturnyoshiright:


  ld a,(marioyoshi)
  and a
  ret z

  ld a,(marioleftrightspr)
  and a
  ret z
  
;als je puur naar rechts beweegt  

  
  ld a,(marioleftright)
  and a
  ret nz
  
  
  ld a,(marioturnyoshi)
  and a
  ret nz
  
  ld a,(turnyoshiactive)
  dec a
  ret z
  
  ld a,1
  ld (marioturnyoshi),a
  ld (turnyoshiactive),a
  

  ret






.mariowalkleft:

  ld a,(Controls)
  bit 3,a
  ret nz


  call .setholdspritedirleft

  call .setturnshellleft


;is mario in een slide?
  ld a,(marioslide)
  and a
  call z,.resetslowdownpointer



;welke kant lopen we op
  ld a,(marioleftright)
  dec a
  jp z,.slowdowntoleft
 
 ;heel belangrijk is dat wanneer de slide onderbroken word door snelle keypresses
;dat de slide dan ook stopt en mario gewoon verder gaat met zijn ding  
  ld a,(marioleftright)
  and a
  call z,.resetmarioslide
    
 
  call .acceleratefromstandingstill

  ;reset de slowdown pointer naar 0 dit routinetje moet wel even anders vanwege de turn slowdown
  xor a
  ld hl,marioslowdownp
  ld (hl),a


.setmariowalkleft:
 
  ;geef aan in de variabele dat mario naar links beweegt
  xor a
  ld (marioleftright),a
    ld (turnshellactive),a
    ld (turnyoshiactive),a 

  ld a,(controlbuffer)
  bit 2,a
  ret z
  
  
    xor a
    ld (marioleftrightsprw),a

  ret

.mariowalkright:

;we resetten de toets naar links zodat er geen rare dingen gebeuren wanneer we returnen uit deze routine
 ; res 2,a
 ; ld (Controls),a

  call .setholdspritedirright

  call .setturnshellright

      ld a,1
      ld (marioleftrightsprw),a



;loopt mario de andere kant op? dan moet er een slide komen op basis van zijn snelheid
  ld a,(marioslide)
  and a
  call z,.resetslowdownpointer


  ld a,(marioleftright)
  and a
  jp z,.slowdowntoright

  ld a,(marioleftright)
  dec a
  call z,.resetmarioslide


;mario versnellen als we normaal vanuit stilstand opkomen. OOK als we rennen
  call .acceleratefromstandingstill
  
  ld a,(watercurrent)
  dec a
  call z,.addcurrentright


  xor a
  ld hl,marioslowdownp
  ld (hl),a
  

.setmariowalkright:
 
  ld a,1
  ld (marioleftright),a
  xor a
      ld (turnshellactive),a 
      ld (turnyoshiactive),a 
    
  
  ret

;wanneer we zien dat de pop naar rechts beweegt moeten we zorgen dat die een slide maakt
.slowdowntoleft:
  
;zowieso rennen eerst stoppen

  call .terminaterunning
 
;de engine moet even weten dat mario in een slide zit  
  ld a,1
  ld (marioslide),a
  
;controleer of mario stilstaat en geen schelpje vastheeft zo niet dan gaan we sliden en de setspeed vermijden
;heeft mario een schelpje ja dit word een or operatie...

  ld a,(mariospeed)
  ld b,a
  ld a,(marioshell)
  or b

  
  call nz,.slowdownstandingstill
  ret nz

;dit hieronder lijkt overbodige code maakt het maakt het net iets smoother allemaal
;het zou zonder werken maar je ziet dan wat glitchy turnarounds


;mario slide is ended
  xor a
  ld (marioslide),a
  ld (mariospeed),a
;voorkom hakkelen van de char vanuit stilstand
   call .acceleratefromstandingstill 
  jp .setmariowalkleft




;wanneer we zien dat de pop naar rechts beweegt moeten we zorgen dat die een slide maakt
.slowdowntoright:
 
 ;zowieso rennen stoppen

  call .terminaterunning
 
;de engine moet even weten dat mario in een slip zit  
  ld a,1
  ld (marioslide),a

  
;controleer of mario stilstaat zo niet dan gaan we sliden en de setspeed vermijden
;hier moeten we ook checken op aanwezigheid van schelpje
  
  ld a,(mariospeed)
  ld b,a
  ld a,(marioshell)
  or b
  
  call nz,.slowdownstandingstill 
  ret nz 

;mario slide is ended
  xor a
  ld (marioslide),a  
  ld (mariospeed),a
;voorkom hakkelen van de char vanuit stilstand
   call .acceleratefromstandingstill
    
  jp .setmariowalkright

 
  
.slowdownstandingstill:

;snelheid verlagen als er niet word gelopen

  ld a,(marioslowingdown)
  dec a
  jp z,.slowdownfromrunning

  ld a,(mariospeed)
  cp 5
  jp z,.slowdownfromrunning
 
  ld d,0
  ld hl,marioslowdownp
  ld e,(hl)
   
  ld hl,marioslowdown
  add hl,de
  ld a,(hl)
  cp 0
  call .setspeed
  ret z
  
  inc e
  ld hl,marioslowdownp
  ld (hl),e
  
  ret

.slowdownfromrunning:
 
 
;snelheid verlagen wanneer er voorheen gerend was 
 
  ld a,1
  ld (marioslowingdown),a
 
  ld d,0
  ld hl,marioslowdownrp
  ld e,(hl)
   
  ld hl,marioslowdownr 
  add hl,de
  ld a,(hl)
  cp 3 ;einde pointer
  call .setspeed
  jp z,.resetmarioslowdown
  
  inc e
  ld hl,marioslowdownrp
  ld (hl),e


   ret

;mario is weer in normale slowdown 
.resetmarioslowdown:

  xor a
  ld (marioslowingdown),a
  ret

 
   
.slowdowntowalking:
 
;mario is aan het rennen en vertraagd nu naar loopsnelheid 
  
  ld d,0
  ld hl,marioslowdownrpp
  ld e,(hl)
   
  ld hl,marioslowdownfr
  add hl,de
  ld a,(hl)
  cp 3 ;einde pointer
  call .setspeed
  ret z
  
  inc e
  ld hl,marioslowdownrpp
  ld (hl),e


  ret

.resetslowdowntowalking:

  xor a
  ld (marioslowdownrpp),a
  ret


;reset de slowdown pointer in de vorm van een call
.resetslowdownpointer:

  xor a
  ld (marioslowdownp),a
  ld (marioslowdownrp),a
  ld (marioslowdowndp),a
  ld (marioslowdownrdp),a
  
  
  ret

;/einde van alle vertragingen


.acceleratefromstandingstill:

;je moet dus kijken of de snelheid hoger is dan een bepaalde waarde en zo ja eerst de snelheid terugnemen
;voordat de max waarde word geset 

;je hebt bijv net het schelpje losgelaten
 ; ld a,(mariospeed)
 ; cp 3
 ; jp nc,.slowdowntowalking

  ld a,(mariospeed)
  and a
  call z,.resetaccelerationpointer

  ld d,0
  ld hl,marioswimtablep
  ld e,(hl) 
   
  ld hl,marioswimtable 
  add hl,de
  ld a,(hl)
  cp 2 ;einde pointer
  call .setspeed
  ret z
  
  inc e
  ld hl,marioswimtablep
  ld (hl),e

 
   ret 

.resetaccelerationpointer:
  
  xor a
  ld (marioswimtablep),a


  ret


;Mario zijn speed word op running gezet. vergeleken en bij de hoogste waarde word de sprite waarde geset    
.running:
 

;zorgen dat mario goed vertraagd na het rennen 
 ; call .resetslowdowntowalking
 
 
;we moeten een schelpje in onze handen hebben anders mogen we niet versnellen

  ld a,(marioshell)
  dec a
  ret z
  
;vanaf rennen pas accelereren wanneer we de basis snelheid hebben bereikt  
  
;  ld a,(mariospeed)
;  cp 3
;  ret c
 
  ld a,3
  ld (mariospeed),a
  ret
 
 
  ret
 
;  ld d,0
;
;  ld hl,mariospeedtablep
;  ld e,(hl)
;
;  ld hl,mariospeedtable
;  add hl,de
;  
;  ld a,(hl) 
;  
;  cp 6
;;  call z,.mariorunninginair
;  jp z,.runningmax
;
;  inc e
;  ld hl,mariospeedtablep
;  ld (hl),e
  
;  ld a,4
;  ld (mariospeed),a
;  ret

;.runningmax:
;
;  ld a,5
;  ld (mariospeed),a
;  xor a
;  ld (mariorunning),a
;  
;  
;  ret
 
 
.terminaterunning:

  
;mario running pointer resetten want we zijn gestopt met rennen
  xor a
  ld (mariospeedtablep),a
   
  ld a,1
  ld (mariorunning),a
  
  ret

.setdefaultspeed:


  ld a,2
  call .setspeed
  ret


.setspeed:

  ld (mariospeed),a
  ret


.resetmarioslide:

;mario slide is ended

;op het moment dat de slide is beeindigd dus mario zijn snelheid behaald nul moet te allen tijde
;de sprite de andere kant op kijken je had immers de knop ingedrukt.

  ld a,(marioslide)
  dec a
  call z,.invertmarioleftright
    
  xor a
  ld (marioslide),a
  
  ret

.invertmarioleftright:


;inversie plegen op mario zijn looprichting (hey ik kan ook een beetje wiskunde)
  ld a,(marioleftright)
  xor %00000001
  ld (marioleftright),a
  
  ret
  


;als er geen jump word gedaan dan gaan we ervan uit dat we mogen vallen
;fall gaat dus automatisch door nadat er op jumpkeys is gecheckt. dit kan niet!

.fall:
 
 ;jumpspeed resetten anders gaat mario hard zweven als je de jump te vroeg loslaat
 xor a
 ld (jumpspeed),a
  
  ld a,(marioshell)
  dec a
  jp z,.fallreverse
  
  ld a,(marioduck)
  dec a
  jp z,.fallfast

;overflow beveiliging om de pointer altijd op zijn plaats te houden
  ld a,(fallspeed)
  cp 5
  call z,.resetswimdownpointer

  
  ld d,0
  ld hl,marioswimdownp
  ld e,(hl)
   
  ld hl,marioswimdown
  add hl,de
  ld a,(hl)
  cp 2 
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,marioswimdownp
  ld (hl),e
  
;in maxspeed word ook de resulterende fallspeed uitgelezen en opgeteld bij de marioy
.maxspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 

 
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;altijd de fallspeed even inlezen anders krijg je gelazer
  ld c,a
 
  ld hl,(marioy)
;  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan

  ld a,(standingonsprite)
  and a
  jp nz,mariophysics.nogravwithspritelock

 call checktilebelowmario
 jp c,.nograv
   
  ret
 
 
.minspeed:  ;maximale versnelling bereikt mag niet harder meer vallen!
 

 call checktileabovemario ;moet andersom gecheckt worden
 call c,.nograv
 
 ;16bits versie van mariocoordinates
  ld a,(fallspeed) ;van fallspeed jumpspeed maken anders snapt de camera het niet!
  ld b,a
 
  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
 
 ;tilenummer groter dan 191 is FG dus we gaan mario locken op zijn tile en daar laten staan
 


  ld a,(fallspeed) ;fallspeed inverteren naar jumpspeed voor de camera
  ld (jumpspeed),a
  
      
  ret  
  
  
.fallfast:

  ld d,0
  ld hl,marioswimdownp
  ld e,(hl)
   
  ld hl,marioswimdown
  add hl,de
  ld a,(hl)
  cp 4 
  jp z,.maxspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,marioswimdownp
  ld (hl),e

  jp .maxspeed
  

.fallreverse:

  ld a,(maxwaterlevel)
  dec a
  ret z
;  jp z,.float ;TODO

  
  ld a,(fallspeed)
  cp 5
  call z,.resetswimdownpointer

  
  ld d,0
  ld hl,marioswimdownp
  ld e,(hl)
   
  ld hl,marioswimdown
  add hl,de
  ld a,(hl)
  cp 3 
  jp z,.minspeed
 
  ld (fallspeed),a
 
   
  inc e
  ld hl,marioswimdownp
  ld (hl),e
  ret


.resetswimdownpointer:

  xor a
  ld (marioswimdownp),a
  ret

  
;/hier word het vallen bepaald





;hier gebeuren alle jumps en bijbehorende problemen

;Ok de jumproutine moet dus helemaal anders. De checkout moet anders maar ook de marionotdown regels 
;moeten worden aangepast. Je mag dus voordat je de tile raakt weer jumpen en zo dus hogerop komen
.jump:


;tijdelijk staan we helemaal geen jump toe met schelpje de bedoeling is dat we een reverse jump maken
  ld a,(marioshell)
  dec a
  jp z,.reversejump

  call checkblockabovemario

;we checken de bovenkant om te kijken of mario wel verder kan jumpen
  call checktileabovemario
  jp c,.fall
  ld a,(mariospecialtile)
  dec a
  jp z,.fall ;tijdelijke branch hier word de speciale mario tile hit gedaan
  ld a,(maxwaterlevel)
  dec a
  jp z,.fall

  ld hl,marioswimupp
  ld a,(hl)
  and a
  call z,Playsfx.swim

;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a
  

;mario heeft nog niet zijn hoogste punt bereikt zodra dat wel zo is gaan we vallen ongeacht wat er gebeurd
  ld a,(marionotdown)
  dec a
  jp z,.fall


 ;fallspeedtable resetten want we zitten immers in een jump
  xor a
  ld (marioswimdownp),a 

  
  ld d,0
  ld hl,marioswimupp
  ld e,(hl)
  
  ld hl,marioswimup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,marioswimupp
  ld (hl),e

  jp .setjumpcoordinates
  
  ret
 
 
.reversejump:

;we checken de bovenkant om te kijken of mario wel verder kan jumpen
  call checktilebelowmario
  jp c,.fall
  ld a,(mariospecialtile)
  dec a
  jp z,.fall ;tijdelijke branch hier word de speciale mario tile hit gedaan


;mario is nu officieel in de lucht. Zolang mario in een sprong zit blijft deze variabele op 1 staan
  ld a,1
  ld (mariofloating),a


;mario heeft nog niet zijn hoogste punt bereikt zodra dat wel zo is gaan we vallen ongeacht wat er gebeurd
  ld a,(marionotdown)
  dec a
  jp z,.fall


 ;fallspeedtable resetten want we zitten immers in een jump
  xor a
  ld (marioswimdownp),a 

  
  ld d,0
  ld hl,marioswimupp
  ld e,(hl)
  
  ld hl,marioswimup
  add hl,de
  ld a,(hl)
  cp 0
  ld b,a
  jp z,.reenablegrav
   
  inc e
  ld hl,marioswimupp
  ld (hl),e

  jp .setrevjumpcoordinates
  
  ret

;reverse jumpen
.setrevjumpcoordinates:


  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a
  
  
  ld hl,(marioy)
  ld c,b
  ld b,0
  add hl,bc
  ld (marioy),hl

;jumpspeed inverten voor de camera  
  ld a,(jumpspeed)
  ld (fallspeed),a
  
  
  ret




;hier worden de uiteindelijke mario coordinaten geset
.setjumpcoordinates:


  ;jumpspeed setten als resultaat van de berekening
  ld a,b
  ld (jumpspeed),a
  
  
  ld hl,(marioy)
  ld c,b
  ld b,0
  xor a
  sbc hl,bc
  ld (marioy),hl
  
  
  ret
  
;nograv word aangeroepen zodra mario een tile heeft aangeraakt aan de onderkant deze routine reset dan
;alle fall tables en set alle waarden zo dat mario opnieuw kan springen.
.nograv:

  call lockmariotile ;eerst mario nog even locken op de tile

.nogravobject:
  
  xor a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (marioswimdownp),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth
  ld (jumpspeed),a ;ook even de jumpspeed op 0 zetten natuurlijk
  
 ; ld a,(marioshell)
  ld (marioswim),a ;swimming uitzetten want je staat op de grond uiteraard alleen wanneer je geen schelpje vasthebt

  ld hl,0
  ld (jumpheightcounter),hl

;key checkout routine. Sommige acties mogen pas nadat een hele phycsics routine is gepaseert. Bijv een jump of fall
;we moeten dan ook weten of de toets ook is losgelaten. Dat geld in dit geval voor bukken en springen.
;Die mogen pas opnieuw NADAT de toetsen zijn losgelaten en mario aan de grond staat.
  ld a,(Controls) 
  bit 4,a
  ret nz
  ld a,(Controls) 
  bit 6,a
  ret nz
;/wachten totdat spatie echt losgelaten is voordat de volgende jump mag

  xor a

  ld (marioswimupp),a
  ld (marionotdown),a
;  ld (mariospinjump),a
 
  ret
  
  
;marionotdown word op 1 geset op het moment dat mario het hoogste punt heeft bereikt van zijn jump
;op dat moment kan mario alleen nog maar vallen totdat hij een tile heeft aangeraakt
 
.reenablegrav:

  ld a,1
  ld (marionotdown),a
  ret


;==================================================[mario is aan het klimmen]===========================
mariophysicsclimbing:

;TODO: check met de general tilecheck rondom mario of hij nog in de climbarea bevind zo niet dan gaan
;we terug naar normale modus. Verder moeten we ook checken of mario op de grond staat en dus ook exiten


;eerst rennen uitschakelen, sliden uitschakelen etc

  xor a
  ld (marioslide),a
  ld (jumpspeed),a
  ld (fallspeed),a
  ld (marioduck),a
  ld (mariospeed),a
  ld (marionotdown),a
  inc a
  ld (mariorunning),a
 
 ;standaard snelheid
  ld a,1
  ld (mariospeed),a
  
;controls uitlezen en daar de actie aan bepalen
;gaan wij rennen?? 
;  ld a,(Controls)
;  bit 5,a
;  call nz,.setrunning 
  
  ;snelheidmax
  ld a,(mariorunning)
  and a
  call z,.setspeedrunning
  
  xor a
  ld b,a ;bregister resetten, we gaan bc gebruiken

;omhoog omlaag  
  ld a,(Controls)
  bit 0,a
  call nz,.moveup
  ld a,(Controls)
  bit 1,a
  call nz,.movedown

;we gaan even links of rechts controleren en bij beide ingedrukt links resetten
  ld a,(Controls)
  and %00001100
  xor %00001100
  call z,.resetleftkey
  

;links rechts
  ld a,(Controls) ;13 Tstates controls uitlezen
  bit 2,a
  call nz,.moveleft
  ld a,(Controls)
  bit 3,a
  call nz,.moveright ;mariophysics
 
 
;gaan we jumpen?? dan moeten we uit deze routine   
  ld a,(Controls)
  bit 4,a
  jp nz,.exitclimb
  bit 6,a
  jp nz,.exitclimb


;voor de rest van de routines zoals de sprite engine even de leftright aangeven
;  ld a,(Controls)
  bit 2,a
  jp nz,mariophysics.setmariowalkleft 
  bit 3,a
  jp nz,mariophysics.setmariowalkright
  

  ret


.exitclimb:

;   ld a,(Controls)
;   bit 0,a
;   ret nz

;exit climb only allow at certain conditions
  

  xor a
  ld (mariospeed),a
  ld (fallspeed),a ;set de fall speed weer op 0
  ld (mariofloating),a ;mario is niet meer in de lucht
  ld (fallspeedtablep),a ;altijd die fallspeed table even resetten dan valt mario nice and smooth

  ld (mariojumprelease),a
  ld (mariojumpupp),a
  ld (marionotdown),a


;  xor a
  ld (marioclimbing),a
  call mariophysics.terminaterunning ;altijd running terminaten!!
  
 ; ld hl,(marioy) ;cheap fix against a huge fence bug. makes matters worse
 ; ld de,10
 ; add hl,de
 ; ld (marioy),hl
  
  
  ret


.moveup:

  ld a,(mariospeed)
  push af

  call checktileabovemario
  call c,.locktileabovemario

  push bc
  pop hl

  ld de,(maplenght)
  add hl,de ;check 1 tile lower

  call checkforclimbedge.up
  call c,.locktileabovemario

 
  ld a,(mariospeed)
  ld c,a
  ld hl,(marioy)
  xor a ;weg met de carry
  ld b,a
  sbc hl,bc
  ld (marioy),hl
  
  pop af
  ld (mariospeed),a
  
  ret

.movedown:

  call checktilebelowmario
  jp c,lockmariotile
  
  
  ld b,0
  ld a,(mariospeed)
  ld c,a
  ld hl,(marioy)
  add hl,bc
  ld (marioy),hl
  
  ret

;speciale locketile omdat je tegen het plafond aan loopt
.locktileabovemario:

  ld a,1
  ld (mariospeed),a
  
  ld hl,(marioy)
  ld a,l
  and %00000111
  sub 3
  cp 0
  ret nz
  xor a
  ld (mariospeed),a
  
  ret
  

.setspeedrunning:

  ld a,2
  ld (mariospeed),a
  ret

.setrunning:

    xor a
    ld (mariorunning),a

  ret

.resetleftkey:

  ld a,(Controls)
  and %11111011
  ld (Controls),a
  ret



.moveleft:

  ld a,(vineactive)
  and a
  jp z,.nocheckleft



;normale tilecheck
  ld b,0
  ld de,0
  call checkgeneraltilefromrom
  cp 252
  ret c

  ld a,(touchside)
  cp 2
  jp z,.lockspriteleft

.nocheckleft:
 
 
 ;check left for edges
  ld b,3
  ld de,-1
  call checkgeneraltilefromrom
  cp 103
  ret z
  cp 105
  ret z
  cp 107
  ret z
  cp 109
  ret z 
  cp 98
  ret z
  cp 97
  ret z
  cp 93
  ret z
  ld b,2
  ld de,-1
  call checkgeneraltilefromrom
  cp 98
  ret z
  cp 93
  ret z
 
 
  
   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   xor a
   sbc hl,bc
   ld (mariox),hl
 
 
   
   ret




.moveright:

  ld a,(vineactive)
  and a
  jp z,.nocheckright


  ld b,0
  ld de,1
  call checkgeneraltilefromrom
  cp 252
  ret c


  ld a,(touchside)
  dec a
  jp z,.lockspriteright


.nocheckright:


 ;check right for edges
  ld b,3
  ld de,2
  call checkgeneraltilefromrom
  cp 104
  ret z
  cp 106
  ret z
  cp 108
  ret z
  cp 110
  ret z 
  cp 96
  ret z
  cp 97
  ret z
  cp 85
  ret z
 ;check right for edges
  ld b,2
  ld de,2
  call checkgeneraltilefromrom
  cp 96
  ret z
  cp 85
  ret z



   ld hl,(mariox)
   ld a,(mariospeed)
   ld c,a
   ld b,0
   add hl,bc
   ld (mariox),hl


   ret


;is actually the right side of the block
.lockspriteleft:

  xor a
  ld (touchside),a

;force mario back a few pixels. This ensures that mario stays on the side of the block
  ld hl,(mariox)
  inc hl
  inc hl
;  inc hl
;  inc hl
  ld (mariox),hl


  ret


;is actually the left side of the block
.lockspriteright:

  xor a
  ld (touchside),a


;force mario back a few pixels. This ensures that mario stays on the side of the block
  ld hl,(mariox)
  dec hl
  dec hl
  dec hl
  dec hl
  ld (mariox),hl


  ret



;/MARIOphysics climbing ==================================================================================================================================



;mario spinjump resetten en zijn richting on random wijzigen dit is niet een echte richtingsverandering. Alleen de sprite veranderd van richting.
resetmariospinjump:

  ld a,(mariospinjump)
  and a
  ret z

  xor a
  ld (mariospinjump),a
  
  call mariophysics.resetmarioslide ;weet niet zeker of dit nodig is
  
  ld a,1
  ld (mariosheerhold),a ;we gebruiken de DoCopy routine van de slide sprite engine


  ld a,r
  bit 0,a
  jp z,.left
  jp nz,.right
  

.left:

    xor a
    ld (marioleftrightspr),a ;we veranderen niet werkelijk van richting we draaien alleen de sprite.
    ret

.right:

     ld a,1
     ld (marioleftrightspr),a
     ret


;check in which part of the table we must look when performing a jump
getspinjumpvalue:

    ld a,(mariojumpupp)
    sub 16 ;substract the value that was added. Any value below 16 will cause a carry to ocur
    jp nc,.spinjumpactive

    add 16 ;faster than reloading the actual value ;)
    
    cp 8   ;return non carry if higher then 8

  ret


.spinjumpactive:

   cp 8 ;value was now substracted so check again with the correct value
   ret
  


;slow mario down in certain conditions. This routine is always executed under two conditions.

;1: the player is steering against the direction mario is moving
;2: the player is no longer pressing the direction keys on the Dpad (left or right)

;This code must be merged with mariophysics after it is completed.

;the code is working in the following steps:
;1: the above two checks are performed by the physics engine and then calls for this routine
;2: the routine checks if it is allready initiated and finishes the cycle if so
;3: in case no slowdown cycle is active. There is checked wheter this is a slide invoked by steering against movement
;4: the condition is checked and then the speed from there jumps are made.
;5: the jump sets a byte which tells us a cycle is active.
;6: the cycle can only be changed or cancelled by steering against it. This is invoked through
;marioslowdownleft and right from the engine.

;even though these 6 steps apply it is still not that simple as there are numerous rules on how mario behaves in many different settings...

slowmariodown:

;flying boxes cancel all of this
  ld a,(touchside)
  and a
  jp nz,endslowdown


;this makes sure that mario can stand on a flying box
  ld a,(standingonsprite)
  and a
  ;jp nz,endslowdown
  jp nz,handlemarioslowdown


;this check can safely be removed
;  ld a,(marioflydrop)
;  and a
;  jp nz,.slowdown


;prevent bugs if we slide or land on a sheercorner
  ld a,(uppersheercornerfound)
  and a
  jp nz,.noslowdown


;during slide we ALWAYS slow down only in air special rules apply
  ld a,(marioslide)
  and a
  jp nz,.checkkeys


;check if we are in a slide and if that is not the case we do not slow down if mario is falling
  call checktilebelowmario
  ret nc
  
.slowdown:  


;when we slide of a ramp basicly a key is pressed but the rampslide is constantly being slown down
;When one of the Controls are pressed we disable all possible ways of slowing down
  ld a,(mariosheertile)
  and %00000001
  ld b,a
  ld a,(marioslide)
  xor %00000001
  and b
  ret nz


;all conditions are ok? check if the pointer was really set and jump to the slowdownhandler which sets themariospeed
  ld a,(marioslowdownpointer)
  and a
  jp nz,handlemarioslowdown

  ret


.checkkeys:


;if we are standing on the ground we ignore this story and commit slowdown
  call checktilebelowmario
  jp c,.slowdown


;if we release the control during a slide in the air we mantain the current speed until we either continue pressing or hit the ground
  ld a,(Controls)
  bit 2,a
  jp nz,.slowdown
  bit 3,a
  jp nz,.slowdown


  ret


.noslowdown:

;als we gaan sturen in de lucht moet de slowdown stoppen zodat de feel beter is
  ld a,(Controls)
  bit 2,a
  jp nz,endslowdown
  bit 3,a
  jp nz,endslowdown



  ld a,1
  ld (mariospeed),a
  xor a
  ld (marioslowdownpointer),a
  ret

;DEPRACATED: this routine can safely be removed
;.activateslowdownfromair:
;
;  ld a,(mariospeed)
;  cp 5
;  ret c
;
;;if the slowdown is allready activated then simply return
;  ld a,(marioslowdownpointer)
;  and a
;  ret nz
;
;;  jp hang
;
;  ld a,87
;  ld (marioslowdownpointer),a
;
;  ret
;/DEPRACATED

.activateslowdown:


;if the slowdown is allready activated then simply return
  ld a,(marioslowdownpointer)
  and a
  ret nz


 ;fetch the mariospeed
  ld a,(mariospeed)
  and a ;0
  ret z ;no action required
  cp 5
  jp z,.maximumslowdown
  cp 3
  jp nc,.runningslowdown
  cp 1
  jp nc,.normalslowdown 
  
  ret


.activatemarioduckslowdown:

;if the slowdown is allready activated then simply return
  ld a,(marioslowdownpointer)
  and a
  ret nz

 ;fetch the mariospeed 
  ld a,(mariospeed)
  and a ;0
  ret z ;no action required
  cp 5
  jp z,.maximumduckslowdown
  cp 3
  jp nc,.runningduckslowdown
  cp 3
  jp z,.normalduckslowdown 
  cp 4
  jp c,.normalslowdown 

  ret

.maximumslowdown:

  ld a,27
  ld (marioslowdownpointer),a
  ret

.runningslowdown:

  ld a,20
  ld (marioslowdownpointer),a
  ret

.normalslowdown:

  ld a,16
  ld (marioslowdownpointer),a
  ret

.maximumduckslowdown:

  ld a,64
  ld (marioslowdownpointer),a
  ret

.runningduckslowdown:

  ld a,51
  ld (marioslowdownpointer),a
  ret

.normalduckslowdown:

  ld a,42
  ld (marioslowdownpointer),a
  ret


.activateslide:

;if a slidetype was previously set we can ignore this setting
  ld a,(marioslowdownpointer)
  and a
  jp nz,slowmariodown
  
  
;fetch the mariospeed, the state of mario doesn't matter here  
  ld a,(mariospeed)
  and a ;0
  ret z ;no action required
  cp 5
  jp z,.maximumslide
  cp 3
  jp nc,.runningslide
  cp 1
  jp nc,.normalslide

  ret


.maximumslide:

  ld a,10
  ld (marioslowdownpointer),a
  ret

.runningslide:

  ld a,5
  ld (marioslowdownpointer),a
  ret

.normalslide:

  ld a,1
  ld (marioslowdownpointer),a
  ret




;this table holds all the slowdown types. The pointer is set directly from the code
marioslowdowntable:   db 0 ;if the pointer is set at 0 there is no slide
                      db 3,2,1,0 ;normal slide ;1
                      db 4,3,2,1,0 ;running slide ;5
                      db 5,4,3,2,1,0 ;maxspeed slide ;10                    
;the normal slowdown tables
                      db 3,2,1,0 ;normal slowdown ;16
                      db 4,3,2,2,1,1,0 ;running slowdown ;20
                      db 5,4,4,4,3,4,3,2,3,2,2,1,1,1,0 ;maxspeed slowdown ;27
;the duckslowdowntables
                      db 3,3,3,3,2,2,1,1,0 ;normalduckslowdown ;42
                      db 4,4,4,3,3,3,3,2,2,2,1,1,0 ;runningduck slowdown ;51
                      db 5,4,4,4,4,4,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,0 ;maxspeedduckslowdown ;64
;drop from air
                      db 5,4,4,4,4,3,3,3,3,3,3,2,2,2,1,0 ;slowdown from airdrop ;87

                      

;0 = no slide 
;1 = normal slide 
;2 = running slide 
;3 = maxspeed slide

;4 = normal slowdown
;5 = running slowdown
;6 = max speed slowdown

;7 = normal duck
;8 = running duck
;9 = max speed duck

;10 = ultra high speed slide (the one that gets invoked when mario does the rampslide.)


handlemarioslowdown:


  ld hl,marioslowdowntable
  ld d,0
  ld a,(marioslowdownpointer)
  ld e,a
  
  add hl,de
  ld a,(hl)
  cp 0
  jp z,endslowdown
  inc e
  ld (mariospeed),a
  ld a,e
  ld (marioslowdownpointer),a

 
  ret


endslowdown:

  xor a
  ld (marioslowdownpointer),a
  ld (mariospeed),a

  ret



mariophysicsend:


;handle the special locking of the wierd sheer tiles
handlespecialsheertile:

  ld a,specialsheertilesblock
  call block12


  ld a,(specialsheertile)
  cp 1
  jp z,.leftsmall
  cp 2
  jp z,.rightsmall
  cp 3
  jp z,.iggyspecial ;left
  cp 4
  jp z,.iggyspecial ;right
  ;both iggy specials do the same this is just for the slide discrimination
  cp 5
  jp z,.leftmedium
  cp 6
  jp z,.rightmedium
  cp 7
  jp z,.leftmax
  cp 8
  jp z,.rightmax
  cp 9
  jp z,.leftnormal
  cp 10
  jp z,.rightnormal
  
  ret

.rightnormal:

  ld hl,sheertablenormalright
  
  jp .lock  

.leftnormal:

  ld hl,sheertablenormalleft

  jp .lock    
  
.rightmax:

  ld hl,sheertablemaxright
  
  jp .lock  

.leftmax:

  ld hl,sheertablemaxleft
  
  jp .lock  
  

.leftmedium:

  ld hl,sheertablemediumleft
  
  jp .lock
  
  
.leftsmall:

  ld hl,sheertableslightleft

  jp .lock

.rightsmall:

  ld hl,sheertableslightright

  jp .lock
  

.rightmedium:


  ld hl,sheertablemediumright
  
  jp .lock  
  

.lock: 
;first obtain the xcoordinate of mario compared to the object he is standing on  


  push hl
  
  ld hl,(mariox)
  ld e,(ix+xobject)
  ld d,(ix+xobject+1)

  xor a
  sbc hl,de
  ld a,h ;check for minus
  and a
  call nz,.setzero
  
  ld d,0
  ld e,l ;maximum number is 64
  
  pop hl
  
  ;ld hl,sheertable
  add hl,de
  ld a,(hl) ;fetch the value from table
  
  ld e,a
  ld d,0
  ld hl,(marioy)
  add hl,de ;substract table value from absolute lock
  
  ld de,8 ;substract base number as compensation
  xor a
     ld (marioswim),a ;cancel swimming   
  sbc hl,de
  
  ld (marioy),hl ;write back to ram
  

  
  ret
 
.setzero:

  xor a
  ld l,a
  ret

  
  
.iggyspecial:

  
  ld a,(ix+v3) ;add offset from to table
  ld b,a
  add a,a
  add a,b ;*3
  add a,a ;*6
  add a,a ;*12
  ld d,0
  ld e,a ;continue in 16bits
  sla e
  rl d ;*24
  sla e
  rl d ;*48
  sla e
  rl d ;*96
  sla e
  rl d ;*192
  
  ld hl,iggysheertable
  add hl,de


  jp .lock



