;Display logo et al. this baby trows a screen5 image on screen. This whole thing should be able to fit in 1 page of 
;ROM it uses engine page functions and thus must be loaded after init.

showlogo:

  ;switch to screen 5
  ld    a,(VDP_0)
  or    %0000'0010		      ;m3=1
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 5 
 
  ld a,$1f ; Page 0 vram
  out ($99),a
  ld a,128 + 2
  out ($99),a

  call clearvrampage0and1

  ;upload logo to VRAM
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
  xor a
  out ($9b),a
  out ($9b),a ;X = 0
 
  ld hl,80+256
;  ld a,80
  ld a,l
  out ($9b),a
  ;xor a
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;X width
  ld a,1
  out ($9b),a  
  
  ld a,36 ;Y height
  out ($9b),a
  xor a
  out ($9b),a
  
  ld hl,logodataadress+5
  ld a,(hl)
  out ($9b),a
  
  xor a
  out ($9b),a
  
  ld a,%11110000
  out ($9b),a

  ld de,$1154-1-10  
  inc hl
  ld c,$99
  

.output:
  outi
  ld a,44+128
  out ($99),a
  
  dec de
  ld a,d
  or e
  jp nz,.output


  xor a
  out ($99),a
  ld a,46+128
  out ($99),a ;stop any commands


  ei
  
  call enablescreen

  call doflash
  
;  ld hl,logodataadress+$1155
  call writeintropalette
  ld hl,currentpalette
  call SetPalette

;TODO: copy parts into page 0 use a command for this and do this as many times as required
  ld e,0 ;its either 0 or 18 0 = upper half 18 = lower half
  ld b,0
  ld c,80
  ld l,9
  exx
  ld e,18
  ld b,80 + (7*5)
  ld c,80
  exx
.dotest:
  push hl
    call copylogopart
    pop hl
    inc b
    inc b
    inc b
    inc b
    inc b
    inc b
    inc b

    halt ;wait one interrupt
 exx
   push hl
    call copylogopart
    pop hl
    dec b
    dec b
    dec b
    dec b
    dec b
    dec b
    dec b
    exx
    
    
    
    ld a,l
    dec a
    ld l,a
    halt
    jp nz,.dotest
    

  ld a,$3f ; Page 1 vram
  out ($99),a
  ld a,128 + 2
  out ($99),a 

;hiero
   call writewhitepalette ;set current palette to full white
   call writeintropalette2 ;set editablepalette to the correct one  

  call doflashspecial

  call waitfortimeandspace

  call fadeoutintro

  call disablescreen

;switch to screen 4
  ld    a,(VDP_0)
  and   %1111'1101		      ;m3=0
  ld    (VDP_0),a
  di
  out   ($99),a
  ld    a,128
  ei
  out   ($99),a
;/switch to screen 4


  ret


writeintropalette:

  ld de,logodataadress+$1155
  ld hl,currentpalette
  ld b,32
.write: ;WHY NOT LDIR!!??
  ld a,(de)
  ld (hl),a
  inc hl
  inc de
  djnz .write  

  ret

writeintropalette2:

  ld de,logodataadress+$1155
  ld hl,editablepalette
  ld b,32
.write:
  ld a,(de)
  ld (hl),a
  inc hl
  inc de
  djnz .write  



  ret


writewhitepalette:

  ld c,%01110111
  ld e,%01110111
  ld hl,currentpalette
  ld b,16
.write:
  ld (hl),c
  inc hl
  ld (hl),e
  inc hl
  djnz .write  



  ret


waitfortimeandspace:

  ld e,150

.waitforspace:

  ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A

  bit 0,a
  ret z

  dec e
  ld a,e
  and a

  halt
  halt

  jp nz,.waitforspace
  ret


doflash:

  ld b,7
  ld c,%01110111 
.darkenloop:
  push bc
  ld hl,currentpalette
  ld b,32
.darken:
  ld a,c ;yes trust me this has to be done
  ld (hl),a
  inc hl
  djnz .darken
  ld hl,currentpalette
  call SetPalette
  pop bc
  ld a,c
  dec a
  sub 16
  ld c,a
  halt
  halt
  halt
  halt
  halt
  djnz .darkenloop
  
  ret


;copy part of the logo into the screen. BC used. B = X C = Y
;E = logo part 
copylogopart:


  ld a,32
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active
  
;source 
  ld a,57
  out ($9b),a ;32
  xor a
  out ($9b),a ;33
  ld hl,80+256
  ld d,0
  add hl,de
  ld a,l
  out ($9b),a ;34
  ld a,h
  out ($9b),a ;35
  
;destination
  ld a,b
  out ($9b),a ;36
  xor a
  out ($9b),a ;37
  ld a,c
  add a,e
  out ($9b),a ;38
  xor a
  out ($9b),a ;39

  
;number of dots
  ld a,140
  out ($9b),a ;40
  xor a
  out ($9b),a ;41
  ld a,18
  out ($9b),a ;42
  xor a
  out ($9b),a ;43
  
  out ($9b),a ;44
  out ($9b),a ;45
  ld a,%11010000
  out ($9b),a ;46

  ei
  ret

clearvrampage0and1: ;clear page 0 and 1

  ;clear entire page with black
  ld a,36
  di
  out ($99),a
  ld a,17+128
  out ($99),a ;indirect port active

  xor a
  out ($9b),a ;36
  out ($9b),a
  out ($9b),a
  out ($9b),a
  
  ;256
  out ($9b),a ;40
  ld a,1
  out ($9b),a
  
  ld hl,512
  ld a,l
  out ($9b),a ;42
  ld a,h
  out ($9b),a
  
  xor a
  out ($9b),a ;44
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a
  
  ei
  
  halt : halt : halt : halt : halt : halt : halt : halt : halt : halt : halt
  ;wait for it
  ret




;screenfader. in DE = alternative pallete
fadeoutintro:

  ld b,8
  
.dofade:
  push bc
  ld    hl,currentpalette                 ;fade out the current palette
  call  .darkenpalette

  ld    hl,currentpalette
  call  SetPalette
    pop bc
  halt
  halt
  halt
  halt
  halt
  halt
  halt
  halt
  halt
  halt
  djnz .dofade
  
  ret

.darkenpalette:
  ld    b,16                              ;amount of colors
.loop:
  call  .darkencolors
  djnz  .loop
  ret  
  
.darkencolors:
  ;first make blue darker
  ld    a,(hl)
  and   %0000'1111                        ;blue
  jr    z,.endDarkenBlue                  ;there is no blue at all in this palletecolor
  dec   a
.endDarkenBlue:  
  ld    c,a                               ;store the darker blue in d

  ;then make red darker
  ld    a,(hl)
  and   %1111'0000                        ;red
  jr    z,.endDarkenRed                   ;there is no red at all in this palletecolor
  sub   a,16
.endDarkenRed:  
  ;then add blue and red together again
  or    c
  ld    (hl),a
  
  inc   hl                                ;green
  ;and finally make green darker
  ld    a,(hl)
  and   %0000'1111                        ;green
  jr    z,.endDarkenGreen                 ;there is no green at all in this palletecolor
  dec   a
.endDarkenGreen:  
  ld    (hl),a
  
  inc   hl                                ;next color
  ret

;screenfader. in DE = alternative pallete
doflashspecial:

  ld b,8
  
.dofade:
  push bc
  ld    de,editablepalette
  ld    hl,currentpalette                 ;fade out the current palette
  call  .darkenpalette

  ld    hl,currentpalette
  call  SetPalette
    pop bc
  halt
  halt
  halt
  halt
  djnz .dofade
  
  ret

.darkenpalette:
  ld    b,16                              ;amount of colors
.loop:
  push bc
  call  .darkencolors
  pop bc
  djnz  .loop
  ret  
  
.darkencolors:
  ;first make blue darker
  ld    a,(hl)
  and   %0000'1111                        ;blue
  ld c,a
  ld a,(de)
  and   %0000'1111
  cp c
  ld a,c
  jr    z,.endDarkenBlue                  ;there is no blue at all in this palletecolor
  dec   a
.endDarkenBlue:  
  ld    b,a                               ;store the darker blue in d

  ;then make red darker
  ld    a,(hl)
  and   %1111'0000                        ;red
  ld c,a
  ld a,(de)
  and   %1111'0000  
  cp c
    ld a,c
  jr    z,.endDarkenRed                   ;there is no red at all in this palletecolor
  sub   a,16
.endDarkenRed:  
  ;then add blue and red together again
  or    b
  ld    (hl),a
  
  inc   hl                                ;green
  inc   de
  ;and finally make green darker
  ld    a,(hl)
  and   %0000'1111                        ;green
  ld c,a
  ld a,(de)
  and   %0000'1111  
  cp c
    ld a,c
  jr    z,.endDarkenGreen                 ;there is no green at all in this palletecolor
  dec   a
.endDarkenGreen:  
  ld    (hl),a
  
  inc   hl                                ;next color
  inc   de
  ret




logodataadress:
incbin "../logo/noramoslogo.SC5"