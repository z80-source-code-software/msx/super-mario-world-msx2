;-----------------------------------------------------------------------------
;	KINROU 5th. Version 2.00p
;	Programmed By K.Kuroda.
;	Modified by Manuel Pazos
;	Copyright (C) 1996,1997 Keiichi Kuroda / BTO(MuSICA Laboratory)
;	Copyright (C) 2006 Manuel Pazos
;	All rights reserved.
;	KINROU5 MuSICA driver
;
;	Desensamblado, interpretado y	modificado por Manuel Pazos 2006
;
; Modified for SMW, all PSG and SCC functionality removed
; optimized to run faster. Other unneeded functions removed
;-----------------------------------------------------------------------------
		
startfm:
					
;		ld	hl, music_data+7
;		ld	de, (music_data+1)
    ld hl,$8000+7+1 ;asume the file is always on $8000
    ld de,($8000+1+1)

		call	musicPlay2
		;ld	hl, (music_data+1)
		;call	musicPlay1
    ret

;------------------------------------------------
; INITAL
; Inicializa el	driver y busca el FM y el SCC
; In:
;     None
; Out:
;     None
; Modify:
;     All
;------------------------------------------------

initAll:	
		di
		ld	hl, slotFM
		ld	de, slotFM+1
		ld	bc, endDriver-slotFM
		ld	(hl),0
		ldir			; Limpia variables

;Do not assume FM present. Search and if found return other value than 0		
		ld	de, searchFM
		ld	hl, slotFM
		call	searchSlots
		
  ld a,1
  ld (musicactive),a ;force music back on. It is forced off by the introdemo
  ld (musicactiveoption),a ;force music back on. It is forced off by the introdemo
  xor a
  ld (optionsecondary+2),a 		
  
        ld hl,slotFM
        ld a,(hl)
        and a
        call z,musicdisable
		
		
		ld	hl, 2Dh		; BIOS ID
		ld	a, (0FCC1h)
		call	RDSLT
		ld	hl, wrFMZ80
		cp	3
		jr	c, InitAll3
		ld	hl, wrFMR800	; Si es	un turboR a�ade	esperas	al acceder al FM

InitAll3:	
		ld	(wrFMAd), hl
		call	stopfm
		ld	hl, volumeFM-1
		ld	de, volumeFM
		ld	bc, 11h
		ld	(hl), 0
		di
		ldir
		ret
		
musicdisable:

  xor a
  ld (musicactive),a ;force music back on. It is forced off by the introdemo
  ld (musicactiveoption),a ;force music back on. It is forced off by the introdemo  
  ld a,1
  ld (optionsecondary+2),a  
  
    ret

; ---------------------------------------------------------------------------
; Play music 1
; In:
;     HL = BGM data address
;      A = Replay times	( 0 = loop)
; Out:
;     Cf = Error
; Nota:
; La musica tiene que estar compilada en la direccion indicada
; ---------------------------------------------------------------------------

musicPlay1:	
		push	hl
		pop	de
		
; ---------------------------------------------------------------------------
; Play music 2
; In:
;     HL = BGM data memory address
;     DE = BGM data compiled address
;      A = Replay times	( 0 = loop)
; Out:
;     Cf = Error
; Nota:
; Si la musica no esta compilada en la misma direccion que se ha cargado
; ralentiza el proceso
; ---------------------------------------------------------------------------

musicPlay2:	

;do not disable interupts or the screen will give a horrible flickr due to lineint fail
    ;di
		call	loc_6160
		ret	c

		ld	hl, 1
		ld	(playedTimes), hl
		
		ld	a, l
		ld	(byte_606F), a
		or	a
		ret

loc_6160:	
		ld	(loopTimes), a
		ld	(bgmAddress), hl
		push	hl
		push	de
		call	initSoundChips

		ld	hl, (bgmAddress)
		ld	c, 0
		call	getParameter

		ld	a, 0
		ld	(clearHL),a
		jr	c, loc_617B
		ld	a, l

loc_617B:	
		ld	(byte_6070), a
		pop	de
		pop	hl
		ld	a, (hl)
		ld	(flagBateriaFM), a
		cp	2
		ccf
		ret	c
		or	a
		sbc	hl, de
		ld	(word_6056), hl
		call	loc_62D7

loc_6191:	
		ld	hl, (bgmAddress)
		ld	ix, chFMArea
		ld	b, 11h
		inc	hl

loc_619B:	
		push	bc
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		inc	hl
		ld	a, e
		or	d
		jr	z, loc_61AB
		ex	de, hl
		ld	bc, (word_6056)
		add	hl, bc
		ex	de, hl

loc_61AB:	
		ld	(ix+0),	e
		ld	(ix+1),	d
		ld	bc, 20h
		add	ix, bc
		pop	bc
		djnz	loc_619B

		ld	a, (slotFM)
		or	a
		ld	hl, chFMArea
		ld	b, 9
		call	z, disableChArea

		ld	b, 11h
		ld	ix, chFMArea

loc_61D7:	
		push	bc
		call	loc_622A
		ld	bc, 20h
		add	ix, bc
		pop	bc
		djnz	loc_61D7

		call	loc_6265

		ld	hl, chFMArea+7
		ld	bc, 1101h

loc_61FC:	
		ld	a, (hl)
		or	a
		jr	z, loc_6207
		ld	de, 20h
		add	hl, de
		djnz	loc_61FC
		dec	c

loc_6207:	
		ld	a, c
		ld	(isPlaying), a
		ret

loc_622A:	
		ld	l, (ix+0)
		ld	h, (ix+1)
		ld	(ix+2),	l
		ld	(ix+3),	h
		ld	a, h
		or	l
		ld	a, 3
		jr	z, loc_623D
		xor	a

loc_623D:	
		ld	(ix+7),	a
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		inc	hl
		ld	a, (hl)
		ex	de, hl
		ld	bc, (word_6056)
		add	hl, bc
		ld	(ix+4),	l
		ld	(ix+5),	h
		ld	(ix+6),	a
		push	ix
		pop	hl
		ld	de, 8
		add	hl, de
		ex	de, hl
		ld	hl, word_62AF
		ld	bc, 13h
		ldir
		ret

loc_6265:	
		ld	a, (byte_6070)
		or	a
		ret	z

		and	2
		jr	z, loc_6273
		
		ld	a, 21h		; Z80 opcode. LD HL,xxxx
		;ld	(loc_69BC), a
		ld	(clearHL), a
loc_6273:	
		ld	a, (byte_6070)
		and	1
		ret	z

		ld	hl, (bgmAddress)
		ld	de, 25h
		add	hl, de
		ld	b, 11h
		ld	ix, chFMArea

loc_6286:	
		push	bc
		ld	a, (ix+0)
		or	(ix+1)
		jr	z, loc_62A4
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		dec	hl
		ld	a, e
		or	d
		jr	z, loc_62A4
		ex	de, hl
		ld	bc, (word_6056)
		add	hl, bc
		ld	(ix+0),	l
		ld	(ix+1),	h
		ex	de, hl

loc_62A4:	
		inc	hl
		inc	hl
		ld	bc, 20h
		add	ix, bc
		pop	bc
		djnz	loc_6286
		ret


word_62AF:	dw 0
		db 0
		db 0
		db 8
		db 0
		db 0
		db 0
		db 0
		db 0Ah
		db 0
		db 1
		db 0
		db 0
		db 0
		db 0
		db 0
		db 0
		db 0


disableChArea:	
		ld	(hl), 0
		inc	hl
		ld	(hl), 0
		ld	de, 1Fh
		add	hl, de
		djnz	disableChArea
		ret

loc_62D7:	
		ld	a, (flagBateriaFM)
		or	a
		jr	nz, loc_62F9
		ld	hl, FMdata
		ld	b, 7

loc_62E2:	
		ld	a, (hl)
		inc	hl
		ld	e, (hl)
		inc	hl
		push hl
		call	wrFM
		pop hl
		djnz	loc_62E2

		ld	hl, chDrumArea+15h
		ld	de, chDrumArea+16h
		ld	bc, 4
		ld	(hl), 0
		ldir
		ret

loc_62F9:	
		ld	a, 0Eh
		ld	e, 0
		call	wrFM
		ret
; Register, data
FMdata:		db 0Eh,	20h	
		db 16h,	54h
		db 17h,	54h
		db 18h,	54h
		db 26h,	8
		db 27h,	8
		db 28h,	4
;------------------------------------------------------
;
; FADE OUT
; In: A	= Velocidad (1-63)
;
;------------------------------------------------------

fadeOut:	
		cp	40h
		jp	c, fadeOut2
		ld	a, 40h

fadeOut2:	
		or	a
		jr	nz, fadeOut3
		inc	a

fadeOut3:	
		di
		ld	(fadeOutSpd), a
		ld	(fadeOutCnt), a
		ld	(flagFade), a
		ret

;------------------------------------------------------
;
; Set master volume
; In: A	= Volume (0-15)
;
;------------------------------------------------------

masterVolume:	
		cp	10h
		ret	nc
		di

masterVolume2:
		ld	hl, volumeFM
		ld	de, volumeFM+1
		ld	bc, 10h
		ld	(hl), a
		ldir

		ld	a, (isPlaying)
		or	a
		ret z


;------------------------------------------------------
;
; Cambia el volumen de un canal
; In: A	= Volumen
;     C	= N�mero de canal
;	 FM = 0-8
;	 PSG = 9-11
;	 SCC = 12-17
;
;------------------------------------------------------

chanelVolume:	
		cp	10h
		ret	nc
		ld	e, a
		ld	a, c
		cp	11h
		ret	nc
		ld	hl, volumeFM-1
		ld	b, 0
		inc	c
		add	hl, bc
		ld	(hl), e
		ret

;------------------------------------------------------
;
; Obtiene un par�metro de la m�sica
;
; In:
;    HL	= BGM data address
;    C = Parameter
;	 C = 1:	HL = Compiled address
;	 C = 2:	HL = Title address
;	 C = 3:	HL = Memo address
; Out:
;    Cy	= Error
;------------------------------------------------------

getParameter:	
		ld	(BGMaddress_), hl
		call	loc_6546
		ld	a, e
		or	d
		ex	de, hl
		scf
		ret	z
		ld	de, 64h
		or	a
		sbc	hl, de
		ret	c
		ld	hl, 4Eh
		ld	de, (BGMaddress_)
		add	hl, de
		ld	a, (hl)
		rrca
		dec	a
		cp	c
		ret	c
		ld	hl, (BGMaddress_)
		ld	de, 50h
		add	hl, de
		ld	e, c
		add	hl, de
		add	hl, de
		ld	a, c
		or	a
		jr	z, loc_651E
		dec	a
		jr	z, loc_651E
		dec	a
		jr	z, loc_6524
		dec	a
		jr	z, loc_6524
		scf
		ret

loc_651E:	
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		or	a
		ret

loc_6524:	
		push	hl
		ld	hl, (BGMaddress_)
		ld	c, 1
		call	getParameter
		ex	de, hl
		ld	hl, (BGMaddress_)
		or	a
		sbc	hl, de
		ex	de, hl
		pop	hl
		ld	c, (hl)
		inc	hl
		ld	b, (hl)
		ld	hl, 0
		ld	a, c
		or	b
		scf
		ret	z
		ex	de, hl
		add	hl, bc
		or	a
		ret
BGMaddress_:	dw 0

loc_6546:	
		ld	de, 49h
		add	hl, de
		ld	e, 0
		ld	a, (hl)
		cp	42h
		ret	nz
		inc	hl
		ld	a, (hl)
		cp	54h
		ret	nz
		inc	hl
		ld	a, (hl)
		cp	4Fh
		ret	nz
		push	bc
		inc	hl
		call	loc_656D
		ld	a, (hl)
		call	loc_6574
		inc	hl
		call	loc_656D
		ld	a, (hl)
		call	loc_6574
		pop	bc
		ret

loc_656D:	
		ld	a, (hl)
		and	0F0h
		rrca
		rrca
		rrca
		rrca

loc_6574:	
		and	0Fh
		ex	de, hl
		add	hl, hl
		push	hl
		add	hl, hl
		add	hl, hl
		pop	bc
		add	hl, bc
		add	a, l
		ld	l, a
		ex	de, hl
		ret	nc
		inc	d
		ret
;------------------------------------------------------
; Write	FM general
;------------------------------------------------------

wrFM:		
		push	hl ;???
		ld		hl,(wrFMAd)
		ex		(sp),hl
		ret

;------------------------------------------------------
; Write	FM R800
;------------------------------------------------------

wrFMR800:	
		push	bc
		push	af
		out	(7Ch), a
		ld	b, 2
		call	.wait
		ld	a, e
		out	(7Dh), a
		ld	b, 7
		call	.wait
		pop	af
		pop	bc
		ret
;------------------------------------------------------
; Wait
;------------------------------------------------------

.wait:		
		in	a, (0E6h)
		ld	c, a

.wait2:		
		in	a, (0E6h)
		sub	c
		cp	b
		jr	c, .wait2
		ret
;------------------------------------------------------
; Write	FM Z80
;------------------------------------------------------

wrFMZ80:	
		push	af
		out	($7C), a
		nop
		nop
		ld	a, e
		out	($7D), a
		nop
		nop
		pop	af
		ret

;------------------------------------------------------
; STOP music
;------------------------------------------------------

stopfm:	
		di
		call	initSoundChips
		ret

;------------------------------------------------------
; Inicializa chips de sonido y player
;------------------------------------------------------

initSoundChips:	
		ld	a, 0Eh
		out	(0ABh),	a
		ld	a, 9
		out	(0ABh),	a
		xor	a
		ld	(isPlaying), a
		ld	(flagFade), a
		ld	(fadeVol), a
		ld	(playedTimes), a
		ld	(playedTimes+1), a

		xor	a
		ld	e, a
		ld	b, 4
		call	wrFMinc

		ld	a, 4
		ld	e, 0FFh
		ld	b, 4
		call	wrFMinc

		ld	a, 10h
		ld	e, 0
		call	wrFMall

		ld	a, 20h
		ld	e, 0
		call	wrFMall

		ld	a, 0Eh
		ld	e, 0
		call	wrFM

		ld	a, 30h
		ld	e, 0FFh
		call	wrFMall

		ld	a, 0Eh
		ld	e, 20h
		call	wrFM

		ld	a, 36h
		ld	e, 0FFh
		ld	b, 3
		call	wrFMinc

    ret
;------------------------------------------------------

wrFMall:	
		ld	b, 9
; In: A	= Register
;     B	= Number of registers
;     E	= Data

wrFMinc:	
		call	wrFM
		inc	a
		djnz	wrFMinc
		ret

; ---------------------------------------------------------
; Busca	en todos los slots y subslots usando la	funcion	apuntada por DE
; Guarda el resultado en la posici�n apuntada por HL
; ---------------------------------------------------------

searchSlots:	
		ld	(jmpRutAd), de
		ld	(resultAd), hl
		
		ld	(hl), 0
		ld	b, 4

searchSlots2:	
		push	bc
		ld	a, 4
		sub	b
		ld	c, a
		ld	hl, 0FCC1h
		add	a, l
		ld	l, a
		ld	a, (hl)
		add	a, a
		jr	nc, searchSlots5
		ld	b, 4

searchSlots3:	
		push	bc
		ld	a, 24h
		sub	b
		rlca
		rlca
		or	c
		ld	(slotTmp), a
	
		call	jumpRutine
		
		ld	hl,(resultAd)
		ld	a, (hl)
		or	a
		pop	bc
		jr	nz, searchSlots6
		djnz	searchSlots3

searchSlots4:	
		pop	bc
		djnz	searchSlots2
		ret

searchSlots5:	
		ld	a, c
		ld	(slotTmp), a
	
		call	jumpRutine
	
		ld	hl,(resultAd)
		ld	a, (hl)
		or	a
		jp	z, searchSlots4

searchSlots6:	
		pop	bc
		ret
		
jumpRutine:
		ld	hl,(jmpRutAd)
		jp	(hl)

;------------------------------------------------------
; Busca	FM
;------------------------------------------------------

searchFM:	
		ld	b, 8
		ld	hl, txtAPRL
		ld	de, 4018h
		call	cmpString
		ret	nc
		ld	b, 4
		ld	hl, txtOPLL
		ld	de, 401Ch
		call	cmpString
		ret	c
		ld	hl, 7FF6h
		push	af
		push	hl
		call	RDSLT
		or	1
		ld	e, a
		pop	hl
		pop	af
		call	WRSLT
		or	a
		ret

cmpString:	
		ld	a, (slotTmp)
		ld	c, a

loc_6712:	
		push	bc
		push	hl
		push	de
		ld	a, c
		ex	de, hl
		call	RDSLT
		pop	de
		pop	hl
		pop	bc
		cp	(hl)
		scf
		ret	nz
		inc	hl
		inc	de
		djnz	loc_6712
		ld	a, c
		ld	(slotFM), a
		or	a
		ret
		
txtAPRL:	db	"APRL"
txtOPLL:	db	"OPLL"

;------------------------------------------------------
; Update music
; Debe ser llamada a cada interrupci�n
;------------------------------------------------------

DRVOPLL:	
    ld a,(musicactive)
    and a
    ret z
	
	
		ld	a, (byte_606F)
		or	a
		jp	z, musicUpdate2
		dec	a
		ld	(byte_606F), a
		ret

musicUpdate2:	
		ld	a, (isPlaying)
		or	a
		ret	z		; No esta sonando una musica

		ld	a, (flagFade)
		or	a
		call	nz, doFade	; Esta haciendo	un fade	out

		ld	a, (isPlaying)
		or	a
		ret	z		; Ha terminado de hacer	el fade	out


updateFM:

		xor	a
		ld	(numCanal), a	; Canales FM (0-8)
		ld	ix, chFMArea
		ld	a, (flagBateriaFM)
		or	a
		push	af
		ld	b, 6		; 6 canales con	bateria
		jr	z, updateFM2
		ld	b, 9		; 9 canales FM sin bateria

updateFM2:	
		push	bc
		call	updateChannel
		ld	bc, 20h
		add	ix, bc
		ld	hl, numCanal
		inc	(hl)
		pop	bc
		djnz	updateFM2
		pop	af
		ld	ix, chDrumArea
		call	z, loc_6AF7


chkEndMusic:	
		ld	hl, chFMArea+7
		ld	b, 11h

chkEndMusic2:	
		ld	a, (hl)
		and	1
		ret	z		; Aun hay algo sonando

		ld	de, 20h
		add	hl, de		; Pasa al siguiente canal
		djnz	chkEndMusic2

		ld	hl, (playedTimes)
		inc	hl
		ld	(playedTimes), hl ; Incrementa el numero de veces que se ha tocado la musica

		ld	hl, loopTimes
		ld	a, (hl)
		or	a
		jr	z, replayMusic	; Si es	igual a	0 hace loop infinito

		dec	(hl)
		jr	nz, replayMusic	; Si es	otro valor, decrementa el contador de reproducciones restantes

		call	initSoundChips	; Si ha	terminado, finaliza reproduccion
		xor	a
		ld	(isPlaying), a
		ret
;------------------------------------------------------
;
; Reinicia la musica
;
;------------------------------------------------------

replayMusic:	

  ld hl,$8000
  ld a,(hl)
  and a
  jp z,loc_6191 ;if not a loopsong just restart

  cp 2
  jp z,hugeloop
  
  ld hl,$8000+7+1000 ;asume the file is always on $8000+1000
  ld de,($8000+1+1000)
  xor a ;infinite loop!

	jp	musicPlay2

hugeloop:

  ld hl,$8000+7+5000 ;asume the file is always on $8000+5000
  ld de,($8000+1+5000)
  xor a ;infinite loop!

	jp	musicPlay2
  
  

;		ld	a, (byte_6070)
;		and	1
;		jp	z, loc_6191 ;reset player is in musicplay2
;
;		ld	b, 11h		; Numero maximo	de canales
;		ld	ix, chFMArea
;		ld	de, 20h		; Channel data size
;
;replayMusic2:	
;		ld	a, (ix+0)
;		or	(ix+1)
;		jr	z, replayMusic3
;		xor	a
;		ld	(ix+7),	a
;
;replayMusic3:	
;		add	ix, de
;		djnz	replayMusic2
;		ret

;------------------------------------------------------
; Fade
;------------------------------------------------------

doFade:		
		ld	hl, fadeOutCnt
		dec	(hl)
		ret	nz

		ld	a, (fadeVol)
		inc	a
		ld	(fadeVol), a
		cp	0Fh
		jp	nc, initSoundChips

		ld	a, (fadeOutSpd)
		ld	(fadeOutCnt), a
;		call	resumeMusic
		ret

loc_68CB:	
		ld	l, (ix+4)
		ld	h, (ix+5)
		ld	a, (hl)
		inc	hl
		ld	(ix+4),	l
		ld	(ix+5),	h
		ret

loc_68DA:	
		ld	b, a
		ld	a, (numCanal+1)
		add	a, b
		cp	0Fh
		jp	c, loc_68E7
		ld	a, 0Fh
		ret

loc_68E7:	
		ld	b, a
		ld	a, (fadeVol)
		add	a, b
		cp	0Fh
		ret	c
		ld	a, 0Fh
		ret

loc_68F2:	
		ld	bc, (numCanal+1)
		sub	c
		jr	nc, loc_68FB
		xor	a
		ret

loc_68FB:	
		ld	bc, (fadeVol)
		sub	c
		ret	nc
		xor	a
		ret

;------------------------------------------------------
; Actualiza la frecuencia de un	canal
;------------------------------------------------------

setFrecuency:	
		ld	a, (numCanal)
		cp	9
		jr	c, setFrecFM

    ret


setFrecFM:	
		call	loc_6A62
		ld	e, l
		ld	a, (numCanal)
		add	a, 10h
		call	wrFM		; Parte	baja de	la frecuencia
		add	a, 10h
		ld	e, h
		jp	wrFM		; Parte	alta

;------------------------------------------------------
; Cambia el voluen de un canal
;------------------------------------------------------

setChVol:	
setFMVol:	
		call	getFMinstVol
		ld	e, a
		ld	a, (numCanal)
		add	a, 30h		; Registros de intrumento/volumen FM
		jp	wrFM


;------------------------------------------------------

loc_6988:	
		set	6, (ix+0Ch)
		jr	loc_6992

loc_698E:	
		res	6, (ix+0Ch)

loc_6992:	
		ld	hl, 0

loc_6995:	
		ex	de, hl
		call	loc_68CB
		ex	de, hl
		ld	d, 0
		ld	e, a
		add	hl, de
		inc	a
		jr	z, loc_6995
		ld	de, 0
		ld	a, (ix+0Ch)
		ld	b, a
		and	60h
		jr	nz, loc_69DA
		ld	a, b
		and	0Fh
		cp	8
		jr	z, loc_69DA
		inc	e
		or	a
		jr	z, loc_69DA
		push	hl
		push	hl
		ld	b, a
		ld	d, h
		ld	e, l
	
		ld	a,(clearHL)
		or	a
		jp	z,keepHL
		
		ld	hl, 0

keepHL:	
		add	hl, de
		djnz	keepHL
		
		srl	h
		rr	l
		srl	h
		rr	l
		srl	h
		rr	l
		ld	a, h
		or	l
		jr	nz, loc_69D3
		inc	l

loc_69D3:	
		ex	de, hl
		pop	hl
		or	a
		sbc	hl, de
		ex	de, hl
		pop	hl

loc_69DA:	
		dec	hl
		ld	(ix+8),	l
		ld	(ix+9),	h
		ld	(ix+0Ah), e
		ld	(ix+0Bh), d
		ret

loc_69E8:	
		ld	a, (ix+6)
		dec	a
		jr	z, loc_6A02
		ld	(ix+6),	a
		ld	l, (ix+2)
		ld	h, (ix+3)
		call	loc_6A55
		ld	(ix+4),	e
		ld	(ix+5),	d
		scf
		ret

loc_6A02:	
		ld	l, (ix+2)
		ld	h, (ix+3)
		inc	hl
		inc	hl
		inc	hl
		ld	(ix+2),	l
		ld	(ix+3),	h
		call	loc_6A55
		inc	hl
		ld	a, (hl)
		ld	(ix+4),	e
		ld	(ix+5),	d
		ld	(ix+6),	a
		push	hl
		ld	hl, (word_6056)
		call	cpHL_DE
		pop	hl
		scf
		ret	nz
		ld	a, 3
		ld	(ix+7),	a
		ld	a, (byte_6070)
		and	1
		ret	z
		ld	l, (ix+0)
		ld	h, (ix+1)
		ld	(ix+2),	l
		ld	(ix+3),	h
		ld	a, 1
		ld	(ix+7),	a
		call	loc_6A55
		inc	hl
		ld	a, (hl)
		ld	(ix+4),	e
		ld	(ix+5),	d
		ld	(ix+6),	a
		scf
		ret

loc_6A55:	
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		push	bc
		ex	de, hl
		ld	bc, (word_6056)
		add	hl, bc
		ex	de, hl
		pop	bc
		ret

loc_6A62:	
		ld	l, (ix+0Eh)
		ld	a, (ix+0Dh)
		add	a, a
		add	a, a
		add	a, a
		add	a, a
		and	30h
		or	(ix+0Fh)
		ld	h, a
		ret
		
		
		
;------------------------------------------------------
; Devuelve el intrumento y volumen en A
; nibble alto =	Instrumento
; nibble bajo =	volumen
;------------------------------------------------------

getFMinstVol:	
		ld	a, (ix+10h)
		call	loc_68DA
		ld	b, a
		ld	a, (ix+11h)
		add	a, a
		add	a, a
		add	a, a
		add	a, a
		or	b
		ret

loc_6AC8:	
		ld	bc, (chDrumArea+15h)
		ld	h, 36h
		call	loc_6ADE
		ld	bc, (chDrumArea+16h)
		inc	h
		call	loc_6ADE
		ld	bc, (chDrumArea+18h)
		inc	h

loc_6ADE:	
		ld	a, b
		call	loc_68DA
		add	a, a
		add	a, a
		add	a, a
		add	a, a
		ld	l, a
		ld	a, c
		call	loc_68DA
		or	l
		ld	e, a
		ld	a, h
		jp	wrFM

cpHL_DE:	
		push	hl
		or	a
		sbc	hl, de
		pop	hl
		ret

loc_6AF7:	
		ld	a, (ix+7)
		and	2
		ret	nz
		res	7, (ix+0Dh)
		ld	a, (volumeFMDrums)
		ld	(numCanal+1), a
		ld	l, (ix+8)
		ld	h, (ix+9)
		ld	a, l
		or	h
		jr	z, loc_6B19
		dec	hl
		ld	(ix+8),	l
		ld	(ix+9),	h
		ret

loc_6B19:	
		call	loc_68CB
		ld	(ix+1Ah), a
		cp	0FFh
		jr	z, loc_6B87
		cp	0C0h
		jr	z, loc_6B8E
		cp	0C1h
		jp	z, loc_6988
		bit	7, a
		jp	nz, loc_6B9D
		push	af
		ld	e, 20h
		ld	a, 0Eh
		call	wrFM
		pop	af
		push	af
		and	1Fh
		or	20h
		ld	e, a
		ld	a, 0Eh
		call	wrFM
		call	loc_698E
		set	7, (ix+0Dh)
		ld	a, 0Fh
		ld	(ix+10h), a
		pop	af
		rra
		ld	bc, (chDrumArea+17h)
		call	c, loc_6B7B
		rra
		ld	bc, (chDrumArea+18h)
		call	c, loc_6B7B
		rra
		ld	bc, (chDrumArea+19h)
		call	c, loc_6B7B
		rra
		ld	bc, (chDrumArea+16h)
		call	c, loc_6B7B
		rra
		ld	bc, (chDrumArea+15h)
		call	c, loc_6B7B
		ret

loc_6B7B:	
		push	af
		ld	a, c
		cp	(ix+10h)
		jr	nc, loc_6B85
		ld	(ix+10h), c

loc_6B85:	
		pop	af
		ret

loc_6B87:	
		call	loc_69E8
		jp	c, loc_6B19
		ret

loc_6B8E:	
		call	loc_68CB
		ld	b, a
		call	loc_68CB
		ld	e, a
		ld	a, b
		call	wrFM
		jp	loc_6B19

loc_6B9D:	
		ld	b, a
		call	loc_68CB
		and	0Fh
		bit	4, b
		jr	z, loc_6BAA
		ld	(chDrumArea+15h), a

loc_6BAA:	
		bit	3, b
		jr	z, loc_6BB1
		ld	(chDrumArea+16h), a

loc_6BB1:	
		bit	2, b
		jr	z, loc_6BB8
		ld	(chDrumArea+19h), a

loc_6BB8:	
		bit	1, b
		jr	z, loc_6BBF
		ld	(chDrumArea+18h), a

loc_6BBF:	
		bit	0, b
		jr	z, loc_6BC6
		ld	(chDrumArea+17h), a

loc_6BC6:	
		call	loc_6AC8
		jp	loc_6B19

updateChannel:	
		ld	a, (ix+7)
		and	2
		ret	nz

		res	7, (ix+0Dh)
		ld	hl, numCanal+1
		ld	d, h
		ld	e, l
		ld	bc, (numCanal)
		ld	b, 0
		add	hl, bc
		inc	hl
		ld	a, (hl)
		ld	(de), a
		ld	l, (ix+8)
		ld	h, (ix+9)
		ld	e, (ix+0Ah)
		ld	d, (ix+0Bh)
		call	cpHL_DE
		jr	nz, loc_6C11

		ld	a, (ix+0Ch)
		and	60h
		jr	nz, loc_6C11

		res	0, (ix+0Dh)
		call	setFrecuency
		jp	loc_6C11


loc_6C11:	
		ld	l, (ix+8)
		ld	h, (ix+9)
		ld	a, h
		or	l
		jr	z, loc_6C25
		dec	hl
		ld	(ix+8),	l
		ld	(ix+9),	h
		jp	loc_6C73

loc_6C25:	
		call	loc_68CB
		ld	(ix+1Ah), a
		cp	0FFh
		jr	z, loc_6C6D
		cp	60h
		jp	c, loc_6E0B
		cp	70h
		jp	c, loc_6F86
		cp	80h
		jp	c, loc_6F91
		sub	80h
		cp	0Eh
		jr	nc, loc_6C25
		add	a, a
		ld	d, 0
		ld	e, a
		ld	hl, off_6C51
		add	hl, de
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		jp	(hl)


off_6C51:	dw loc_6FAD	
		dw loc_6FB4
		dw loc_6FBB
		dw setInstrument
		dw loc_707A
		dw loc_7081
		dw loc_7088
		dw loc_709A
		dw loc_70A3
		dw loc_70BC
		dw loc_70C9
		dw loc_70CC
		dw loc_70D8
		dw loc_6988

loc_6C6D:	
		call	loc_69E8
		jp	c, loc_6C25

loc_6C73:	
		bit	6, (ix+0Ch)
		jr	nz, loc_6C89
		call	loc_6C99
		call	loc_6D7E
		ld	a, (numCanal)
		cp	9
		jr	c, loc_6C89

loc_6C89:	
		ld	a, (ix+14h)
		or	a
		jr	nz, loc_6C95
		ld	a, (ix+13h)
		ld	(ix+14h), a

loc_6C95:	
		dec	(ix+14h)
		ret

loc_6C99:	
		ld	a, (ix+15h)
		or	a
		ret	z
		ld	a, (ix+14h)
		or	a
		ret	nz
		ld	a, (ix+16h)
		neg
		ld	(ix+16h), a
		ld	e, a
		rlca
		sbc	a, a
		ld	d, a
		ld	l, (ix+17h)
		ld	h, (ix+18h)
		add	hl, de
		ld	(ix+0Eh), l
		ld	(ix+0Fh), h
		jp	setFrecuency

;------------------------------------------------------
; Get low nibble
; In:
;    A = data (DDDDdddd)
; Out:
;    A = 0000dddd (low nibble)
;    B = 0000DDDD (high	nibble)
;------------------------------------------------------

getLowNibble:	
		ld	c, a
		rra
		rra
		rra
		rra
		and	0Fh
		ld	b, a
		ld	a, c
		and	0Fh
		ret

loc_6D7E:	
		ld	a, (ix+19h)
		or	a
		ret	z
		ld	a, (ix+14h)
		or	a
		ret	nz
		ld	e, (ix+0Eh)
		ld	d, (ix+0Fh)
		ld	l, (ix+17h)
		ld	h, (ix+18h)
		call	cpHL_DE
		jp	z, loc_6E02
		jp	c, loc_6DCE
		ld	l, (ix+19h)
		ld	h, 0
		add	hl, de
		ld	a, (numCanal)
		cp	9
		jr	nc, loc_6DC0
		push	hl
		ld	a, h
		and	1
		ld	h, a
		ld	de, 156h
		or	a
		sbc	hl, de
		pop	hl
		jr	c, loc_6DC0
		ld	a, h
		and	0Eh
		add	a, 2
		ld	h, a
		ld	l, 0ABh

loc_6DC0:	
		ld	e, (ix+17h)
		ld	d, (ix+18h)
		call	cpHL_DE
		jr	c, loc_6E02
		jp	loc_6E01

loc_6DCE:	
		ld	l, (ix+19h)
		ld	h, 0
		ex	de, hl
		or	a
		sbc	hl, de
		ld	a, (numCanal)
		cp	9
		jr	nc, loc_6DF6
		push	hl
		ld	a, h
		and	1
		ld	h, a
		ld	de, 0ABh
		or	a
		sbc	hl, de
		pop	hl
		jr	nc, loc_6DF6
		ld	a, h
		and	0Eh
		sub	2
		or	1
		ld	h, a
		ld	l, 55h

loc_6DF6:	
		ld	e, (ix+17h)
		ld	d, (ix+18h)
		call	cpHL_DE
		jr	nc, loc_6E02

loc_6E01:	
		ex	de, hl

loc_6E02:	
		ld	(ix+0Eh), l
		ld	(ix+0Fh), h
		jp	setFrecuency

loc_6E0B:	
		bit	5, (ix+0Ch)
		jr	nz, loc_6E18
		or	a
		jr	z, loc_6E18
		set	7, (ix+0Dh)

loc_6E18:	
		or	a
		jr	z, loc_6E8F
		dec	a
		ld	l, a
		ld	h, 0
		ld	b, 8

loc_6E2B:	
		add	hl, hl
		ld	a, h
		sub	0Ch
		jr	c, loc_6E33
		inc	l
		ld	h, a

loc_6E33:	
		djnz	loc_6E2B
		push	hl
		sla	h
		ld	e, h
		ld	d, 0
		ld	hl, word_6E9C
		add	hl, de
		ld	c, (hl)
		inc	hl
		ld	b, (hl)
		inc	hl
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ld	a, (ix+15h)
		or	a
		jr	z, loc_6E54
		push	de
		call	loc_6F62
		pop	de
		ld	(ix+16h), a

loc_6E54:	
		ld	a, (ix+12h)
		or	a
		ld	hl, 0
		jr	z, loc_6E63
		call	loc_6F62
		ld	h, 0
		ld	l, a

loc_6E63:	
		add	hl, bc
		pop	de
		ld	a, e
		add	a, a
		or	h
		ld	h, a
		ld	(ix+17h), l
		ld	(ix+18h), h
		set	0, (ix+0Dh)
		call	loc_698E
		ld	a, (ix+19h)
		or	a
		jp	nz, loc_6C73
		ld	a, (ix+17h)
		ld	(ix+0Eh), a
		ld	a, (ix+18h)
		ld	(ix+0Fh), a
		call	setFrecuency
		jp	loc_6C73

loc_6E8F:	
		res	0, (ix+0Dh)
		call	loc_698E
		call	setFrecuency
		jp	loc_6C73

word_6E9C:	dw 0ABh
		dw 0B6h
		dw 0C0h
		dw 0CCh
		dw 0D8h
		dw 0E5h
		dw 0F2h
		dw 101h
		dw 110h
		dw 120h
		dw 132h
		dw 144h
		dw 156h

loc_6F62:	
		ex	de, hl
		or	a
		sbc	hl, bc
		jp	loc_6F6E

loc_6F69:	
		ld	h, b
		ld	l, c
		or	a
		sbc	hl, de

loc_6F6E:	
		ex	de, hl
		ld	hl, 0

loc_6F72:	
		or	a
		jr	z, loc_6F81
		srl	a
		jr	nc, loc_6F7A
		add	hl, de

loc_6F7A:	
		sla	e
		rl	d
		jp	loc_6F72

loc_6F81:	
		ld	a, h
		or	a
		ret	nz
		inc	a
		ret

loc_6F86:	
		sub	60h
		ld	(ix+10h), a
		call	setChVol
		jp	loc_6C25

loc_6F91:	
		sub	70h
		ld	(ix+11h), a
		call	getFMinstVol
		ld	e, a
		ld	a, (numCanal)
		add	a, 30h
		call	wrFM
		jp	loc_6C25

loc_6FAD:	
		res	1, (ix+0Dh)
		jp	loc_6C25

loc_6FB4:	
		set	1, (ix+0Dh)
		jp	loc_6C25

loc_6FBB:	
		call	loc_68CB
		cp	6
		jp	nc, loc_6C25
		add	a, a
		ld	e, a
		ld	d, 0
		ld	hl, off_6FF8
		add	hl, de
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		jp	(hl)

loc_6FD0:	
		in	a, (0AAh)
		and	10h
		jr	nz, loc_6FDD

loc_6FD6:	
		ld	a, 9
		out	(0ABh),	a
		jp	loc_6C25

loc_6FDD:	
		ld	a, 8
		out	(0ABh),	a
		jp	loc_6C25

loc_6FE4:	
		in	a, (0AAh)
		and	80h
		jr	z, loc_6FF1

loc_6FEA:	
		ld	a, 0Eh
		out	(0ABh),	a
		jp	loc_6C25

loc_6FF1:	
		ld	a, 0Fh
		out	(0ABh),	a
		jp	loc_6C25

off_6FF8:	dw loc_6FD0	
		dw loc_6FD6
		dw loc_6FDD
		dw loc_6FE4
		dw loc_6FEA
		dw loc_6FF1
;------------------------------------------------------

setInstrument:	
		call	loc_68CB
		ld	e, a
		call	loc_68CB
		ld	d, a
		ex	de, hl
		push	bc
		ld	bc, (word_6056)
		add	hl, bc
		pop	bc
		ld	a, (numCanal)

		xor	a

setFMInst:	
		ld	e, (hl)
		call	wrFM
		inc	a
		inc	hl
		cp	8
		jp	c, setFMInst
		jp	loc_6C25


;------------------------------------------------------

loc_707A:	
		res	5, (ix+0Ch)
		jp	loc_6C25

loc_7081:	
		set	5, (ix+0Ch)
		jp	loc_6C25

loc_7088:	
		call	loc_68CB
		and	1Fh
		ld	b, a
		ld	a, (ix+0Ch)
		and	0E0h
		or	b
		ld	(ix+0Ch), a
		jp	loc_6C25

loc_709A:	
		call	loc_68CB
		ld	(ix+12h), a
		jp	loc_6C25

loc_70A3:	
		call	loc_68CB
		ld	(ix+19h), a
		ld	(ix+15h), 0
		ld	l, (ix+17h)
		ld	h, (ix+18h)
		ld	(ix+0Eh), l
		ld	(ix+0Fh), h
		jp	loc_6C25

loc_70BC:	
		call	loc_68CB
		ld	(ix+15h), a
		ld	(ix+19h), 0
		jp	loc_6C25

loc_70C9:	
		jp	loc_6C25

loc_70CC:	
		call	loc_68CB
		ld	(ix+13h), a
		ld	(ix+14h), a
		jp	loc_6C25

loc_70D8:	
		call	loc_68CB
		ld	b, a
		call	loc_68CB
		ld	e, a
;		ld	a, (numCanal)
		ld	a, b
		call	wrFM
		jp	loc_6C25



;-----------------------------------------------------------------------------
; Channel work area always comes at the end of variablespage3
;-----------------------------------------------------------------------------

slotFM:		equ	endenginepage3variablesspecial ;1 ;startadress of workarea
slotTmp:	equ	endenginepage3variablesspecial+3;1
clearHL:	equ	endenginepage3variablesspecial+5;1
volumeTmp:	equ	endenginepage3variablesspecial+6;2
jmpRutAd:	equ	endenginepage3variablesspecial+8;2
resultAd:	equ	endenginepage3variablesspecial+10;2
wrFMAd:		equ	endenginepage3variablesspecial+12;2
isPlaying:	equ	endenginepage3variablesspecial+14;1
loopTimes:	equ	endenginepage3variablesspecial+15;1
flagFade:	equ	endenginepage3variablesspecial+16;1
fadeVol:	equ	endenginepage3variablesspecial+17;1
fadeOutSpd:	equ	endenginepage3variablesspecial+18;1
fadeOutCnt:	equ	endenginepage3variablesspecial+19;1
bgmAddress:	equ	endenginepage3variablesspecial+20;2
word_6056:	equ	endenginepage3variablesspecial+22;2
numCanal:	equ	endenginepage3variablesspecial+24;2
volumeFM:	equ	endenginepage3variablesspecial+26;6
volumeFMDrums:	equ	endenginepage3variablesspecial+32;3	
volumeSCCch:	equ	endenginepage3variablesspecial+38-3;5
playedTimes:	equ	endenginepage3variablesspecial+44-4;2
flagBateriaFM:	equ	endenginepage3variablesspecial+46-4;1
byte_606F:	equ	endenginepage3variablesspecial+47-4;1
byte_6070:	equ	endenginepage3variablesspecial+48-4;1
initFlag:	equ	endenginepage3variablesspecial+49-4;1
chFMArea:	equ	endenginepage3variablesspecial+50-4; 32*6
chDrumArea:	equ	endenginepage3variablesspecial+242-4; 32*3
chSCCarea:	equ	endenginepage3variablesspecial+434-96-4;32*5
envelopeSCC:	equ	endenginepage3variablesspecial+621-27-96-4;9*5
txtSlot:	equ	endenginepage3variablesspecial+666-27-96-4;4
endDriver:	equ	endenginepage3variablesspecial+670-27-96-4;0



	
