

;global variables, these ones remain as long as the MSX is on
globalvars:

standingonexit:     db 0 ;we staan op een exittube
yellowswitchactive: db 0 ;is the yellow palace switch activated??
nocontrol:          db 0 ;mario heeft geen controlle meer. De physics staan uit
;nocontrol defines a lot more then just causing your physics to be disabled
; 1 = mario exits the level through a vertical tube. This can be both sides
; 2 = mario dies
; 3 = mario has finished the level
; 4 = mario exits the level through a horizontal tube
exitx:              db 0 ;contains the rough last mariocoordinate when level was exited
exitx4:             db 0 ;contains the rough last mariocoordinate when level was exited/2
fadeoutscreen?:     db  0
fadeoutscreenstep:  ds  1

powerreserve:       db 0 ;1 betekend dat er een powerup op reserve staat

mariostate:         db 0 ;0 = klein 1 = groot 2 = vuur 3 = vliegen;;; 4 = yoshi klein 5 = yoshi groot 6 = yoshi vuur 7 = yoshi vliegen

;status of mario
marioyoshi:           db 0 ;mario is on yoshi
marioshell:           db 0 ;heeft mario een schelpje in zijn handen?
marioblueblock:       db 0 ;mario is holding a blue smiley block
mariopbox:            db 0 ;mario is holding a blue pbox
marioleftright:       db 0 ;1 = rechts; 0 = links
marioduck:            db 0 ;mario gebukt belangrijk voor later! 0 = niet; 1 = wel
mariolaser:           db 0 ;fireflower behavioural characteristics.

;status of yoshi
yoshitype:            db 0 ;0 = green yoshi, 1 = red yoshi, 2 = yellow yoshi, 3 = blue yoshi, 4 = perma blue yoshi

;game completed
gamecompleted:        db 0 ;mario finished the entire freaking game
finalscenecompleted:  db 0 ;final scene played out.

;some bullshit variables
hlvar:                dw 0 ;temphlstack
bcvar:                dw 0 ;tempbcstack
devar:                dw 0 ;tempdestack

tubelaunch:           db 0 ;mario gets launched out of a tube
tubeupordown:         db 0 ;0 = down 1 = up

currentmusicblock:    db 0 ;the current music block required by the player

endsingleglobalvars: equ $ - globalvars ;size of the list of undefined global vars

demo:               db  0 ;demo is playing

;moonpowerup:        db 0 ;keeps track if the powerup was allready taken
yoshicoinpickuptable: dw 0,0,0,0,0,999 ;999 is end of table
lenghtyoshicoinpickuptable: equ $ - yoshicoinpickuptable
 ;this table keeps the x and y coordinates of the picked up yoshi coins
;upon exiting the worldmap this table is being cleared on interloads however not. on load the loader
;checks if any of these coordinates match and removes them from the levelobjectslist               


;variables for world map

Worldmapspat:						;sprite attribute table
	db		000,000,000,0	,000,000,004,0	,226,000,008,0	,226,000,012,0
	db		226,000,016,0	,226,000,020,0	,226,000,024,0	,226,000,028,0
	db		226,000,032,0	,226,000,036,0	,226,000,040,0	,226,000,044,0
	db		226,000,048,0	,226,000,052,0	,226,000,056,0	,226,000,060,0

	db		226,000,064,0	,226,000,068,0	,226,000,072,0	,226,000,076,0
	db		226,000,080,0	,226,000,084,0	,226,000,088,0	,226,000,092,0
	db		226,000,096,0	,226,000,100,0	,226,000,104,0	,226,000,108,0
	db		226,000,112,0	,226,000,116,0	,226,000,120,0	,226,000,124,0


copytile:
  db    000,000,000,003   ;sx,--,sy,spage
  db    000,000,000,000   ;dx,--,dy,dpage
  db    008,000,008,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

copybackground:
  db    016,000,000,000   ;sx,--,sy,spage
  db    000,000,000,001   ;dx,--,dy,dpage
  db    256-16,000,016,000   ;nx,--,ny,--
  db    000,000,$d0       ;fast copy

;copytile2:
;  db    032,000,000,003   ;sx,--,sy,spage
;  db    256-16,000,000,000   ;dx,--,dy,dpage
;  db    008,000,008,000   ;nx,--,ny,--
;  db    000,000,$d0       ;fast copy

;worldmap control
makelevelavailable?:db 0;0=no,1=LocationYelSwP,2=LocationYoIsl3,3=LocationYoIsl4,4=LocationIggyCa,5=LocationDoPla1,6=LocationDoPla2,7=LocationDoSec1
;8=LocationGrSwPa,9=DoPla2toDoGhos,10=DoSec1toDoGhos,11=LocationDoSeGh,12=LocationStarA1,13=LocationTubeG1,14=LocationTopSec,15=DoGhostoDoPla3
;16=TubeH1toDoPla3,17=LocationDoPla4,18=LocationMortCa,19=LocationVaDom1,20=YeSwPaFinished,21=GrSwPaFinished,22=ReSwPaFinished,23=BlSwPaFinished
;24=LocationVaDom2,25=LocationVaSec1,26=LocationReSwPa,27=VaSec1toStarB1,28=LocationTubeI1,29=LocationVaGhos,30=LocationVaDom3,31=LocationVaDom4
;32=LocationLemmCa,33=LocationTubeJ2,34=LocationStarB1,35=LocationVaSec3,36=LocationButBr1,37=LocationButBr2,38=ButBr2toLudwCa,39=LocationSodaLa
;40=LocationCookie,41=CookietoLudwCa,42=LudwCatoForIl1,43=ForIl1toFoGhos,44=FoGhostoForIl4,45=ForIl1toForIl2,46=LocationBlSWPa,47=ForIl2toForIl3 ;47 and 45 are swapped??
;48=ForIl3toFoGhos,49=ForIl4toForIl2,50=ForIl4toForSec,51=ForSectoForFor,52=ForFortoStarC1,53=ForIl3toRoysCa,54=RoysCatoChoIs1,55=ChoIs1toChoGho
;56=ChoGhotoChoIs2,57=ChoIs2toTubeK1,58=ChoIs2toChoIs3,59=ChoIs3toCircle,60=ChoIs3toChoFor,61=ChoFortoChoIs4,62=ChoIs4toChoIs5,63=ChoIs5toWendCa
;64=WendCatoSunkGh,65=SunkGhtoValBo1,66=ValBo1toValBo2,67=ValBo2toValFor,68=ValFortoBackDo,69=ValBo2toValGho,70=ValGhotoLarrCa,71=ValGhotoValBo3
;72=ValBo3toValBo4,73=ValBo4toStarD1,74=LarrCatoFrontD,75=ValBo4toLarrCa,76=StarW1toStarB2,77=StarW2toStarC2,78=StarW3toStarD2,79=StarW4toStarE2
;80=StarW5toStarF2
levelavailabilitylistposition:  equ $ - globalvars ;this is to determine the position in SRAM
levelavailabilitylist:
if (allmapsavailable = 1)
;bit7654 3210         7              6              5              4                3              2              1              0
db %1111'1111 ;byte0: LocationYosHou,LocationYoIsl1,LocationYoIsl2,LocationYelSwP,  LocationYoIsl3,LocationYoIsl4,LocationIggyCa,LocationDoPla1
db %1111'1111 ;byte1: LocationDoPla2,LocationDoSec1,LocationGrSwPa,DoPla2toDoGhos,  DoSec1toDoGhos,LocationDoSeGh,LocationStarA1,LocationTubeG1
db %1111'1111 ;byte2: LocationTopSec,DoGhostoDoPla3,TubeH1toDoPla3,LocationDoPla4,  LocationMortCa,LocationVaDom1,YeSwPaFinished,GrSwPaFinished
db %1111'1111 ;byte3: ReSwPaFinished,BlSwPaFinished,LocationStarA2,LocationVaDom2,  LocationVaSec1,LocationReSwPa,VaSec1toStarB1,LocationTubeI2
db %1111'1111 ;byte4: LocationVaGhos,LocationVaDom3,LocationVaDom4,LocationLemmCa,  LocationTubeJ2,LocationStarB1,LocationVaSec3,LocationVaFort
db %1111'1111 ;byte5: LocationButBr1,LocationButBr2,ButBr2toLudwCa,LocationSodaLa,  LocationCookie,CookietoLudwCa,LudwCatoForIl1,ForIl1toFoGhos
db %1111'1111 ;byte6: ForIl1toForIl2,ForIl2toForIl3,ForIl2toBlSwPa,ForIl3toFoGhos,  FoGhostoForIl4,ForIl4toForIl2,ForIl4toForSec,ForSectoForFor
db %1111'1111 ;byte7: ForFortoStarC1,ForIl3toRoysCa,RoysCatoChoIs1,ChoIs1toChoGho,  ChoGhotoChoIs2,ChoIs2toTubeK1,ChoIs2toChoIs3,ChoIs3toCircle
db %1111'1111 ;byte8: ChoIs3toChoFor,ChoFortoChoIs4,ChoIs4toChoIs5,ChoIs5toWendCa,  WendCatoSunkGh,SunkGhtoValBo1,ValBo1toValBo2,ValBo2toValFor
db %1111'1111 ;byte9: ValFortoBackDo,ValBo2toValGho,ValGhotoLarrCa,ValGhotoValBo3,  ValBo3toValBo4,ValBo4toStarD1,LarrCatoFrontD,ValBo4toLarrCa
db %1111'1100 ;byte10: StarW1toStarB2,StarW2toStarC2,StarW3toStarD2,StarW4toStarE2, ChoSectoTubeL2,StarW5toStarF2,locationStarC1
endif
if (allmapsavailable = 2)
db %1111'1111 ;byte0: LocationYosHou,LocationYoIsl1,LocationYoIsl2,LocationYelSwP,  LocationYoIsl3,LocationYoIsl4,LocationIggyCa,LocationDoPla1
db %1111'1111 ;byte1: LocationDoPla2,LocationDoSec1,LocationGrSwPa,DoPla2toDoGhos,  DoSec1toDoGhos,LocationDoSeGh,LocationStarA1,LocationTubeG1
db %1111'1111 ;byte2: LocationTopSec,DoGhostoDoPla3,TubeH1toDoPla3,LocationDoPla4,  LocationMortCa,LocationVaDom1,YeSwPaFinished,GrSwPaFinished
db %1111'1100 ;byte3: ReSwPaFinished,BlSwPaFinished,LocationStarA2,LocationVaDom2,  LocationVaSec1,LocationReSwPa,VaSec1toStarB1,LocationTubeI2
db %0000'0000 ;byte4: LocationVaGhos,LocationVaDom3,LocationVaDom4,LocationLemmCa,  LocationTubeJ2,LocationStarB1,LocationVaSec3,LocationVaFort
db %0000'0000 ;byte5: LocationButBr1,LocationButBr2,ButBr2toLudwCa,LocationSodaLa,  LocationCookie,CookietoLudwCa,LudwCatoForIl1,ForIl1toFoGhos
db %0000'0000 ;byte6: ForIl1toForIl2,ForIl2toForIl3,ForIl2toBlSwPa,ForIl3toFoGhos,  FoGhostoForIl4,ForIl4toForIl2,ForIl4toForSec,ForSectoForFor
db %0000'0000 ;byte7: ForFortoStarC1,ForIl3toRoysCa,RoysCatoChoIs1,ChoIs1toChoGho,  ChoGhotoChoIs2,ChoIs2toTubeK1,ChoIs2toChoIs3,ChoIs3toCircle
db %0000'0000 ;byte8: ChoIs3toChoFor,ChoFortoChoIs4,ChoIs4toChoIs5,ChoIs5toWendCa,  WendCatoSunkGh,SunkGhtoValBo1,ValBo1toValBo2,ValBo2toValFor
db %0000'0000 ;byte9: ValFortoBackDo,ValBo2toValGho,ValGhotoLarrCa,ValGhotoValBo3,  ValBo3toValBo4,ValBo4toStarD1,LarrCatoFrontD,ValBo4toLarrCa
db %0000'0000 ;byte10: StarW1toStarB2,StarW2toStarC2,StarW3toStarD2,StarW4toStarE2, ChoSectoTubeL2,StarW5toStarF2,locationStarC1
endif
if (allmapsavailable = 0)
db %1110'0000 ;byte0: LocationYosHou,LocationYoIsl1,LocationYoIsl2,LocationYelSwP,  LocationYoIsl3,LocationYoIsl4,LocationIggyCa,LocationDoPla1
db %0000'0000 ;byte1: LocationDoPla2,LocationDoSec1,LocationGrSwPa,DoPla2toDoGhos,  DoSec1toDoGhos,LocationDoSeGh,LocationStarA1,LocationTubeG1
db %0000'0000 ;byte2: LocationTopSec,DoGhostoDoPla3,TubeH1toDoPla3,LocationDoPla4,  LocationMortCa,LocationVaDom1,YeSwPaFinished,GrSwPaFinished
db %0000'0000 ;byte3: ReSwPaFinished,BlSwPaFinished,LocationStarA2,LocationVaDom2,  LocationVaSec1,LocationReSwPa,VaSec1toStarB1,LocationTubeI2
db %0000'0000 ;byte4: LocationVaGhos,LocationVaDom3,LocationVaDom4,LocationLemmCa,  LocationTubeJ2,LocationStarB1,LocationVaSec3,LocationVaFort
db %0000'0000 ;byte5: LocationButBr1,LocationButBr2,ButBr2toLudwCa,LocationSodaLa,  LocationCookie,CookietoLudwCa,LudwCatoForIl1,ForIl1toFoGhos
db %0000'0000 ;byte6: ForIl1toForIl2,ForIl2toForIl3,ForIl2toBlSwPa,ForIl3toFoGhos,  FoGhostoForIl4,ForIl4toForIl2,ForIl4toForSec,ForSectoForFor
db %0000'0000 ;byte7: ForFortoStarC1,ForIl3toRoysCa,RoysCatoChoIs1,ChoIs1toChoGho,  ChoGhotoChoIs2,ChoIs2toTubeK1,ChoIs2toChoIs3,ChoIs3toCircle
db %0000'0000 ;byte8: ChoIs3toChoFor,ChoFortoChoIs4,ChoIs4toChoIs5,ChoIs5toWendCa,  WendCatoSunkGh,SunkGhtoValBo1,ValBo1toValBo2,ValBo2toValFor
db %0000'0000 ;byte9: ValFortoBackDo,ValBo2toValGho,ValGhotoLarrCa,ValGhotoValBo3,  ValBo3toValBo4,ValBo4toStarD1,LarrCatoFrontD,ValBo4toLarrCa
db %0000'0000 ;byte10: StarW1toStarB2,StarW2toStarC2,StarW3toStarD2,StarW4toStarE2, ChoSectoTubeL2,StarW5toStarF2,locationStarC1
endif

;playerinfo
lives:                      db    5
gold:                       db    0
firstrun:                   db    0 ;first time player starts the game


mapdatatable: ;table that keeps track of all the mapdata
 ;mainid,       moonpowerup taken?,   completed, middle reached?, yoshicoins collected
 db 00,               00,                  00,          00,           00
mapdatatablelenght: equ $ - mapdatatable ;size of the list
 db 01,               00,                  00,          00,           00
 db 02,               00,                  00,          00,           00 
 db 03,               00,                  00,          00,           00 
 db 04,               00,                  00,          00,           00 
 db 05,               00,                  00,          00,           00 
 db 06,               00,                  00,          00,           00 
 db 07,               00,                  00,          00,           00 
 db 08,               00,                  00,          00,           00 
 db 09,               00,                  00,          00,           00 
 db 10,               00,                  00,          00,           00 
 db 11,               00,                  00,          00,           00 
 db 12,               00,                  00,          00,           00 
 db 13,               00,                  00,          00,           00 
 db 14,               00,                  00,          00,           00 
 db 15,               00,                  00,          00,           00 
 db 16,               00,                  00,          00,           00 
 db 17,               00,                  00,          00,           00 
 db 18,               00,                  00,          00,           00 
 db 19,               00,                  00,          00,           00 
 db 20,               00,                  00,          00,           00 
 db 21,               00,                  00,          00,           00 
 db 22,               00,                  00,          00,           00 
 db 23,               00,                  00,          00,           00 
 db 24,               00,                  00,          00,           00 
 db 25,               00,                  00,          00,           00 
 db 26,               00,                  00,          00,           00 
 db 27,               00,                  00,          00,           00 
 db 28,               00,                  00,          00,           00 
 db 29,               00,                  00,          00,           00 
 db 30,               00,                  00,          00,           00 
 db 31,               00,                  00,          00,           00 
 db 32,               00,                  00,          00,           00 
 db 33,               00,                  00,          00,           00 
 db 34,               00,                  00,          00,           00 
 db 35,               00,                  00,          00,           00 
 db 36,               00,                  00,          00,           00 
 db 37,               00,                  00,          00,           00 
 db 38,               00,                  00,          00,           00 
 db 39,               00,                  00,          00,           00 
 db 40,               00,                  00,          00,           00 
 db 41,               00,                  00,          00,           00 
 db 42,               00,                  00,          00,           00 
 db 43,               00,                  00,          00,           00 
 db 44,               00,                  00,          00,           00 
 db 45,               00,                  00,          00,           00 
 db 46,               00,                  00,          00,           00 
 db 47,               00,                  00,          00,           00 
 db 48,               00,                  00,          00,           00 
 db 49,               00,                  00,          00,           00 
 db 50,               00,                  00,          00,           00 
 db 51,               00,                  00,          00,           00 
 db 52,               00,                  00,          00,           00 
 db 53,               00,                  00,          00,           00 
 db 54,               00,                  00,          00,           00 
 db 55,               00,                  00,          00,           00 
 db 56,               00,                  00,          00,           00 
 db 57,               00,                  00,          00,           00 
 db 58,               00,                  00,          00,           00 
 db 59,               00,                  00,          00,           00 
 db 60,               00,                  00,          00,           00 
 db 61,               00,                  00,          00,           00 
 db 62,               00,                  00,          00,           00 
 db 63,               00,                  00,          00,           00 
 db 64,               00,                  00,          00,           00 
 db 65,               00,                  00,          00,           00 
 db 66,               00,                  00,          00,           00 
 db 67,               00,                  00,          00,           00 
 db 68,               00,                  00,          00,           00 
 db 69,               00,                  00,          00,           00 
 db 70,               00,                  00,          00,           00 
 db 71,               00,                  00,          00,           00 
 db 72,               00,                  00,          00,           00 
 db 73,               00,                  00,          00,           00 
 db 74,               00,                  00,          00,           00 
 db 255 ;end of list 

 
 
 
finalend:
 
