
;darky replayer same stuff different port. Resides on the same adress and is called by loader to the engine
; Filed:SEE3EDPL.asm             Projectd:SEE v3.00 release A    Updated:31/10/94
; Sound Effect Driver v3.00A    codingd:SHADOW                  (c) FUZZY LOGIC
; Replay a SEE file (v3.xx only!)

; Song structure OFFSETS
;SEEIDd: equ   $00             	;"SEE3EDIT"
;HISPTd: equ   $08             	;Highest used Start_pattern
;HIPTAd: equ   $0A             	;Higest used pattern+1_address
;HISfxd: equ   $0C             	;Highest used Sfx (not used)

;SEEadrPattsd: equ  $8000+$0210 ;Base address + Pattern data (max 1024 patts)
;SEEadrpostbd: equ  $8000+$0010 ;Base address + Table with Sfx start pattern
;+-----------------------------------------------------------------------------+

DoForceSfxd:
  ld    (Sfxnr),a
  xor   a
  ld    (ForceSfx?),a
  ld    a,(ForceSfx_PR)
  ld    (SfxPriority),a
  jp    SetSfxd.DoSetSfx		

SetSfxd:

  call SetSfxd2

  ld    a,(SetSfx?)           ;should a Sfx be played?
  or    a
  ret   z
  xor   a
  ld    (SetSfx?),a

  ld    a,(ForceSfx?)
  or    a
  jr    nz,DoForceSfxd		

  ld    a,(Sfxon?)            ;check if a Sfx is currently
  or    a
  jp    z,.DoSetSfx           ;Sfx is not being played, so do set Sfx
  
  ld    a,(SfxpreviousPriority)
  ld    b,a                   ;check priority of previous Sfx
  ld    a,(SfxPriority)       ;check priority of new Sfx
  cp    b
  ret   c                     ;new priority is lower, dont play Sfx

.DoSetSfx:
  ld    a,(SfxPriority)
  ld    (SfxpreviousPriority),a

  ld    a,(Sfxnr)
  add   a,a                   ;Sfxnr*2
  ld    h,0
  ld    l,a
  ld    de,SEEadrpostb
  add   hl,de                 ;Sfx base address + Table with Sfx start pattern

  ld    c,(hl)                ;Get start pattern
  inc   hl
  ld    b,(hl)

  ; Calculate pattern address
  ; In:  bc=Pat_nr, pattern nr (0-$3FF)
  ld l,c
  ld h,b
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl
  sbc hl,bc
  ld bc,SEEadrPatts
  add hl,bc
  ld    (PATADD),hl

  ld    hl,0
  ld    (tempo),hl
  ld    a,1
  ld    (Sfxon?),a            ;Sfx on
  ret

  
DoForceSfxd2:
  ld    (Sfxnr2),a
  xor   a
  ld    (ForceSfx2?),a
  ld    a,(ForceSfx_PR2)
  ld    (SfxPriority2),a
  jp    SetSfxd2.DoSetSfx2		

SetSfxd2:

    ld a,(turbor?) ;only on turbo r
    and a
    ret z


  ld    a,(SetSfx2?)           ;should a Sfx be played?
  or    a
  ret   z
  xor   a
  ld    (SetSfx2?),a

  ld    a,(ForceSfx2?)
  or    a
  jr    nz,DoForceSfxd2		

  ld    a,(Sfxon2?)            ;check if a Sfx is currently
  or    a
  jp    z,.DoSetSfx2           ;Sfx is not being played, so do set Sfx
  
  ld    a,(SfxpreviousPriority2)
  ld    b,a                   ;check priority of previous Sfx
  ld    a,(SfxPriority2)       ;check priority of new Sfx
  cp    b
  ret   c                     ;new priority is lower, dont play Sfx  
  
  
;run sound on second PSG
.DoSetSfx2:
  ld    a,(SfxPriority2)
  ld    (SfxpreviousPriority2),a

  ld    a,(Sfxnr2)
  add   a,a                   ;Sfxnr*2
  ld    h,0
  ld    l,a
  ld    de,SEEadrpostb
  add   hl,de                 ;Sfx base address + Table with Sfx start pattern

  ld    c,(hl)                ;Get start pattern
  inc   hl
  ld    b,(hl)

  ; Calculate pattern address
  ; In:  bc=Pat_nr, pattern nr (0-$3FF)
  ld l,c
  ld h,b
  add hl,hl
  add hl,hl
  add hl,hl
  add hl,hl
  sbc hl,bc
  ld bc,SEEadrPatts
  add hl,bc
  ld    (PATADD2),hl

  ld    hl,0
  ld    (tempo2),hl
  ld    a,1
  ld    (Sfxon2?),a            ;Sfx on
  ret  



MainSfxIntRoutined: 
  
  
  ld a,(sfxactive) ;sfx is active
  and a
  ret z   
  
  call MainSfxIntRoutined2  
  
  ld    a,(Sfxon?)
  or    a
  ret   z



  ld    a,(tempo+1)
  sub   a,1
  ld    (tempo+1),a
  ret   nc
  ld    a,(tempo+0)
  ld    (tempo+1),a
  ld    hl,(PATADD)

  ld    a,(_HALT)             ;Halt counter
  and   a
  jr    z,.Main

  dec   A
  ld    (_HALT),a
  ret   nz
  jp    .Main2                ;previously a HALT Event was done, now do PSG

.Main:
  call  Eventd                 ;Do operation command

.Main2: 
  call  SetPsgd                ;Set PSG reg.
  ld    (PATADD),hl
  ret

;+----- Event Commands -----+
Eventd: 
  ld    a,(hl)                ;Get Event
  ld    c,a
  and   $70
  ret   z                     ;No command

  cp    $10
  jr    z,.EV_hlT             ;Halt
  cp    $20
  jp    z,.EV_For             ;Loop start
  cp    $30
  jp    z,.EV_Nxt             ;Loop next
  cp    $40
  jr    z,.EV_Str             ;Continue Loop start
  cp    $50
  jr    z,.EV_Rer             ;Continue Loop next
  cp    $60
  jr    z,.EV_Tem             ;tempo

  ;- Effect end
.EV_End: 
  xor   a
  ld    (Sfxon?),a
  pop   af                    ;restore stack
  ret

  ;- Halt "hltd:x"
.EV_hlT: 
  ld    a,c
  and   $0f
  ld    (_HALT),a
  pop   af                    ;restore stack
  ret

  ;- Loop start "ford:x"
.EV_For: 
  ld    a,(loopnr)            ;inc loop nr
  inc   a
  and   $03
  ld    (loopnr),a

  ex		de,hl

  ld    hl,loopbf

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc			
      
  ld		a,(de)
  and   $0f
  ld    (hl),a                ;[Counter]
  inc   hl
  ld    (hl),e                ;Set address
  inc   hl
  ld    (hl),d

  ex		de,hl
  ret

  ;- Loop next "next"
.EV_Nxt: 
  ld    a,(loopnr)
  and   $03

  ex    de,hl
  ld    hl,loopbf

  ld		b,a
  add		a,a
  add		a,b
  ld		c,a
  ld    b,0
  add		hl,bc				

  dec   (hl)                  ;dec. counter
  jr    z,.Ev_Nxt1            ;Loop end
      
  inc   hl
  ld    c,(hl)                ;get address
  inc   hl
  ld    h,(hl)
  ld    l,c
  ret

  ; Loop done, now remove this loop
.Ev_Nxt1: 
  ld    a,(loopnr)            ;dec loopnr
  dec   a
  and   3
  ld    (loopnr),a

  ex		de,hl
  ret

  ;- Continue loop start "start"
.EV_Str: 
  ld    (clpadr),hl           ;Store Patt adr.
  ret

  ;- Continue loop next "rerun"
.EV_Rer: 
  ld    hl,(clpadr)
  ret

  ;- tempo "tmpd:x"
.EV_Tem: 
  ld    a,c
  and   $0f
  ld    (tempo+0),a
  ld    (tempo+1),a
  ret

;+----- Set all PSG regs.-----+
SetPsgd:                       
  ld	  a,(SfxpreviousPriority)
  cp	  254			              ;Sfx being played that
  jp    Z,SetPsg3d		          ;requires 3 channels ??

  ld    de,PsgReg+4

  ld    bc,5          
  add   hl,bc                 ;start with reg.4 (PSG Channel 3)

  ;reg 0 - 5. [sound frequency]       
  ld    c,(hl)                ;reg.0, 2 or 4
  inc   hl
  ld    b,(hl)                ;reg.1, 3 or 5
  
;add turbo support, sigh even more clockcycles wasted!
 ; ld a,(turbor?)
 ; cp 2
 ; call nc,setturbofreq15d ;branch in here we want minimal clockcycles wasted on this one

  bit   7,(hl)                ;check and handle Tuning
  jp    Z,.Dont_TUNWUP

  ;TUNWUPd: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  add   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl
.Dont_TUNWUP:

  bit   6,(hl)
  jp    z,.Dont_TUNWDW     
  ;TUNWDWd: 
  push  hl
  ld    a,(de)
  ld    l,a
  inc   de
  ld    a,(de)
  ld    h,a
  dec   de

  xor   a
  sbc   hl,bc
  ld    b,h             
  ld    c,l
  pop   hl

.Dont_TUNWDW:
  inc   hl

  ld    a,4                   ;reg.4   
  out   ($44),a
  ld    a,c
  out   ($45),a
  ld    (de),a
  inc   de        

  ld    a,5                   ;reg.5   
  out   ($44),a                
  ld    a,b
  and   $0f
  out   ($45),a
  ld    (de),a
  inc   de         

;Use reg.6 ONLY when a soundeffect needs specific noise	
;		ld    a,6             ;reg.6   
;		out   ($44),a
		
;		ld    a,(hl)          ;reg.6 [Rustle frequency]
;		bit   7,a
;		jp    Z,.DontTuneUp
		
;		ex    de,hl
;		add   a,(hl)          ; Up
;		ex    de,hl
;.DontTuneUpd:
		
;		bit   6,a
;		jp    Z,.DontTuneDown
;		ex    de,hl
;		sub   (hl)            ; Down
;		ex    de,hl
;		NEG
;.DontTuneDownd:
		
;		and   $1F
;		out   ($45),a
;		ld    (de),a

  inc   hl             
  inc   de

  ld    a,7             ;reg.7 [set channel ON/OFF]
  out   ($44),a         ;read r#7, which channels are used
  in    a,($46)         ;by SCC-blaffer at this moment?
  ;       B A  Noise C B A  Tone C B A
  and   %11'011'011    ;0=sound output  
  ld    c,a
  ld    a,(hl)
  and   %00'100'100
  or    c
  out   ($45),a       ;write value to reg.7

  inc   hl
  inc   hl
  inc   hl
  inc   de
  inc   de
  inc   de

  ;reg 8-10 [Volume]
  ld    a,10             ;reg.10   
  out   ($44),a
				
  ld    a,(hl)
  inc   hl
  ld    c,a
  and   $0f
  bit   7,c
  jp    z,.No_TUN_UP
  ;TUN_UPd:
  ex    de,hl
  add   a,(hl)
  ex    de,hl
.No_TUN_UP:
  bit   6,c
  jp    z,.No_VOL_DW
  ;VOL_DWd: 
  and   $0f
  ld    b,a
  ld    a,(de)
  sub   a,b
  jp    nc,.No_VOL_DW
  xor   a
.No_VOL_DW:
  ;FIXVOLd: 
  and   $0f
  ld    (de),a

;Set Maximum PSG Volume
;.SetMaxPSGVolumed:	Equ	$+1
;  sub   a,1
;  jp    nc,.SetPsgvolume
;  xor   a
;.SetPsgvolumed:
;/Set Maximum PSG Volume

  out   ($45),a               ;write volume value
 
;reg 11-13 [volume effect]
  ld    c,$a1                 ;out port

  ld    a,11                  ;reg.11   
  out   ($44),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.12   
  out   ($44),a
  outi                        ;out  (c),(hl)  and  inc hl

  inc   a                     ;reg.13   
  out   ($44),a
  outi                        ;out  (c),(hl)  and  inc hl
  ret

; Shut up PSG!
;PSGOFFd: 
;Only mute Channel 3
;        ld      a,7             ;reg.7 [set channel ON/OFF]
;        out     ($44),a         ;read r#7, which channels are used
;        IN      a,($46)         ;by SCC-blaffer at this moment?
;       I/O       B A  Noise C B A  Tone C B A
;        and     %00'111'111    ;0=sound output  
;        or      %00'100'100    ;shut up channel C
;        out     ($45),a

;        ld    a,7               ;reg.7 [set channel ON/OFF]  
;        out   ($44),a           ;I/O B A Noise C B A Tone C B A
;        ld    a,                %00'111'111  ;0=sound output  
;        out   ($45),a
;        ret


; Write PSG (not ROM-BIOS compatible!)
; Ind:   a, data
;       E, port
;      IX, PSG store table
; outd:  E+1
;      IX+1
;WRTPSGd: ld    (IX),a
;        inc   IX
;        push  af
;        ld    a,E
;        out   ($44),a
;        inc   E
;        pop   af
;        out   ($45),a
;        ret

;+----- Set all 3 PSG regs.-----+
SetPsg3d:                       
        LD    IX,PsgReg
        LD    E,0             ;Start reg.0
        INC   hl              ;[skip Event byte]

        LD    B,3             ;First 6 reg. [sound frequency]
STPS.n0d: PUSH  bc
        LD    C,(hl)          ;reg.0, 2 or 4
        INC   hl
        LD    B,(hl)          ;reg.1, 3 or 5
        BIT   7,(hl)          ;check and handle Tuning
        call  NZ,TUNWUPd
        BIT   6,(hl)
        call  NZ,TUNWDWd
        INC   hl
        LD    a,C
        call  WRTPSGd
        LD    a,B
        AND   $0F
        call  WRTPSGd
        pop   bc
        DJNZ  STPS.n0d

        LD    a,(hl)          ;reg.6 [Rustle frequency]
        INC   hl
        BIT   7,a
        call  NZ,TUN_UPd
        BIT   6,a
        call  NZ,TUN_DWd
        AND   $1F
        call  WRTPSGd

        LD    a,(hl)          ;reg.7 [set channel ON/OFF]
        AND   $3F
        OR    $80             ;set bit 7 is necessary to write the PSG!!
        INC   hl
        call  WRTPSGd

        LD    B,3             ;reg 8-10 [Volume]
STPS.n1d: PUSH  bc
        LD    a,(hl)
        INC   hl
;        BIT   5,a
;        jp    NZ,STPS.2       ;Volume pattern is set.
        LD    C,a
        AND   $0F
        BIT   7,C
        call  NZ,TUN_UPd
        BIT   6,C
        call  NZ,VOL_DWd
;        call  FIXVOL


; Make relative volume (depending on "SEEVOL")
; Ind:  a, volume
; Outd: a, new volume [seevol - (15-volume)]
FIXVOLd: AND   $0F

		LD    (IX),a


;Set Maximum PSG Volume
.SetMaxPSGVolume:	equ	$+1
		SUB		a,1
		jp		NC,.SetPsgvolume
		XOR		A
.SetPsgvolume:
;/Set Maximum PSG Volume


STPS.n2d: 
		AND   $1F
;		call  WRTPSG

        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($44),a
        INC   E
        pop   AF
        OUT   ($45),a

		pop   bc
		DJNZ  STPS.n1d

        LD    B,3             ; reg 11-13 [volume effect]
STPS_2d: LD    a,(hl)
        INC   hl
        call  WRTPSGd
        DJNZ  STPS_2d
        ret


; Tuning
;- byte
TUN_UPd: ADD   a,(IX)          ; Up
        ret
TUN_DWd: SUB   (IX)            ; Down
        NEG
        ret
;- word
TUNWUPd: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        ADD   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret
TUNWDWd: PUSH  hl
        LD    l,(IX)
        LD    h,(IX+1)
        XOR   A
        sbc   hl,bc
        PUSH  hl
        pop   bc
        pop   hl
        ret

; Volume Down
VOL_DWd: AND   $0F
        LD    B,a
        LD    a,(IX+0)
        SUB   B
        ret   NC
        XOR   A
        ret


; Write PSG (not ROM-BIOS compatible!)
; Ind:   a, data
;       E, port
;      IX, PSG store table
; Outd:  E+1
;      IX+1
WRTPSGd: LD    (IX),a
        INC   IX
        PUSH  AF
        LD    a,E
        OUT   ($44),a
        INC   E
        pop   AF
        OUT   ($45),a
        ret


setturbofreq15d:
;frequency divide by 2 lets see what happens
;so the story is lowering the value makes the freq higher
;turning bc up lowers the frequency
;we need to multiply by 1,5 d:S
  cp 3
  jp z,.setx2

   push hl
   push de
   
   push bc
   pop de
   srl d
   rr e ;/2
   
   push bc
   pop hl
   add hl,de 
   push hl 
   pop bc

   pop de
   pop hl
   
   ret
   
.setx2:

   sla c
   rl b ;*2

  ret
