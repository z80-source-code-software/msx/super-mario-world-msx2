;OOOOOOHW YEAH!! this is the part that does the demo recording :) :)
;trow the ReadControls data in a separate page of the RAM mapper. 
recorddemo:

  in a,($a8)
  ex af,af'


  ld a,(slot.ram)
  out ($A8),a


;set mapper
  ld a,4
  out ($FE),a


  ld a,(Controls)
  ld hl,(demopointer)
  ld (hl),a ;write Controls to RAM
  inc hl
  ld (demopointer),hl



;restore mapper
  ld a,1
  out ($FE),a

;restore page settings
  ex af,af'
  out ($a8),a


  ret

playdemo:

  
  in a,($a8)
  ex af,af'
  
  ld a,(slot.page12rom)
  out ($a8),a
  
  ld a,demodatablock
  call block12
  
  ld hl,(demopointer)
  ld a,(hl)
  cp 255 ;end of demo detected go to next demo
  jp z,.enddemo
  ld (Controls),a
  inc hl
  ld (demopointer),hl
  
  ex af,af'
  out ($a8),a
  
  
    ld b,8
  
  in	a,($AA)
	and	$F0		;only change bits 0-3
	or	b		;take row number from B
	out	($AA),a
	in	a,($A9)		;read row into A
	
	bit 0,a
	
	call z,.stopdemo
  
  ret
 
.stopdemo:

  ld a,(sfxactive)
  and a
  jp z,.skipwait

  ld a,(Sfxon?)
  and a
  jp nz,.stopdemo ;wait for any sound to finish

    ld a,(darky?) ;exclude darky in case some custom inthandler or anything is in play
    and a
    jp z,.skipwait
  ld a,(Sfxon2?)
  and a
  jp nz,.stopdemo ;wait for any sound to finish

.skipwait:  
  
  ld a,(introdemoactive)
  dec a
  jp z,.gointromenu ;TEMP Later on make branch with the help of a preset variable

  ld a,1
  ld (levelend?),a
  xor a
  ld (demo),a ;write 0
  ret


.gointromenu:
  
    ld a,(flashrom?)
    and a
    jp z,.exittoworldmap ;if no sram is available we skip all the sram code
  
  
    ld a,(slot.page12rom)
    out ($a8),a

    ld a,demodatablock
    call block12
    
;    ld a,optionsmenublock
;    call block34
    
    jp intromenu
    
   ; ret

.exittoworldmap:

    call clearglobalvars


  ld a,1
  ld (levelend?),a
  ld (demoend?),a
  
.waitsfx:  

  ld a,(sfxactive)
  and a
  ret z

  ld a,(Sfxon?)
  and a
  jp nz,.waitsfx ;wait for any sound to finish

    ld a,(darky?) ;exclude darky in case some custom inthandler or anything is in play
    and a
    ret z 
  ld a,(Sfxon2?)
  and a
  jp nz,.waitsfx ;wait for any sound to finish  
  
  ret


.enddemo:
 
;  ld a,(Sfxon?)
;  and a
;  jp nz,.enddemo ;wait for any sound to finish
      ;set the fadeout
  ld a,1
  ld (fadeoutscreen?),a
  ld a,8
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  
  ld a,(enddemotimer)
  inc a
  ld (enddemotimer),a
  cp 60
  ret c

  ld a,1
  ld (levelend?),a
    xor a
  ld (fadeoutscreen?),a
  ld (fadeoutscreenstep),a                ;fade out in 8 steps
  ld (nocontrol),a
  ld (Sfxon2?),a ;just in case
  
  ret
  
  
startdemo:

  ld a,15     ;black bkg
  di
  out ($99),a
  ld a,7+128
  ei
  out ($99),a	

;  ld a,(slot.page12rom)
;  out ($a8),a

    ld a,(musicactiveoption)
    ld (musicactive),a ;set music after detection

  ld a,1
  ld (demo),a
  ld (sfxactive),a
 ; ld (musicactive),a
  ld (introdemoactive),a

  call detectflashrom
 ; ld (flashrom?),a ;temp debug!
  
.loopdemo:

  ld hl,introdemo
  ld (demopointer),hl
    xor a
  ld (enddemotimer),a
  
;menuscreen
;  ld a,$5b
    xor a ;destroy yoshi!!!
    ld (marioyoshi),a
    ld hl,0
    ld (noyoshi),hl
  ld a,$5b : ld (currentmusicblock),a : ld de,0 : ld ix,level18startingcoordinates  : ld iy,animate0 : ld hl,objectlistlv018 : ld bc,lenghtobjectlistlv018 : ld a,18 : call	levelloader

  xor a
  ld (introdemoactive),a  

 call clearnondefinedglobals

  ld a,(demoend?)
  dec a
  jp z,.enddemo

  ld a,(demo)
  and a
  jp z,.enddemo


  ld hl,level1demo
  ld (demopointer),hl
    xor a
  ld (enddemotimer),a

;demo1
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level00startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv000 : ld bc,lenghtobjectlistlv000 : ld a,15 : call	levelloader
 
 call clearnondefinedglobals

  ld a,(demo)
  and a
  jp z,startdemo


  ld hl,level2demo
  ld (demopointer),hl
    xor a
  ld (enddemotimer),a

;demo2
  ld a,$2e : ld (currentmusicblock),a : ld de,0 : ld ix,level02startingcoordinates  : ld iy,level00animationadress : ld hl,objectlistlv002 : ld bc,lenghtobjectlistlv002 : ld a,02 : call	levelloader

 
 call clearnondefinedglobals

  
  jp startdemo

.enddemo:
  
  
  xor a ;reset scorebar values
  ld (lineintheight),a
  ld (scorebaractive),a
  ld (powerreserve),a
  ld (demo),a

    call readsramuserdata
  

;DEBUG SHIZZLE REMOVE AFTER DONE!!!!  
  
; ld a,3
; ld (mariostate),a
; ld a,$ff
; ld (yellowswitchactive),a
; ld a,1
; ld (marioyoshi),a
; ld a,4
; ld (yoshitype),a     
;/DEBUG SHIZZLE REMOVE AFTER DONE!!!!      
    
  jp loader

  
clearnondefinedglobals: 
 ;clear the special nondefined global vars
  ld hl,globalvars
  ld de,globalvars+1
  ld bc,endsingleglobalvars-1
  ld (hl),0
  ldir 
  
  ret

  ;detect flashrom by writing 1 single value to a predefined adress in ROM

detectflashrom:  

  ;  xor a
  ;  ld (flashrom?),a
  
  
    ld a,3 ;block2
    call block12

    ld a,($4000) ;check first byte in this block
    and a
    jp z,.flashromdetected
    ld a,($4001) ;check second byte in this block
    and a
    jp z,.flashromrbscdetected
    
    di
    ;now write to the flashrom
    
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    ld a,$50
    ld (hl),a
    
    ;reset command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$f0
    ld ($4000),a
    
    ;write command
    ld a,$aa
    ld ($4aaa),a
    ld a,$55
    ld ($4555),a
    ld a,$a0
    ld ($4aaa),a

    xor a
    ld ($4000),a
    
    ld bc,$FFFF ;we cannot check for a successfull write since we do not know on which device we are running yet
.wait:
    ld ix,0 ;lekker traag
    dec bc
    ld a,b
    or c
    jp nz,.wait
    
    ;when running in openmsx on ESE v1 mapper we should unlock writing to that specific flashrom 
    ld hl,$7FFF
    xor a
    ld (hl),a    
    
    
    ld a,($4000) ;check first byte in this block
    and a
    jp z,.flashromdetected   
    

    ;write command
    ld a,$aa
    ld ($4555),a
    ld a,$55
    ld ($42aa),a
    ld a,$a0
    ld ($4555),a

    xor a
    ld ($4001),a
 
 
    ld bc,$FFFF ;we cannot check for a successfull write since we do not know on which device we are running yet
.wait2:
    ld ix,0 ;lekker traag
    dec bc
    ld a,b
    or c
    jp nz,.wait2
 
    
    
    ld a,($4001) ;check second byte in this block
    and a
    jp z,.flashromrbscdetected      
    
    xor a
    ld (flashrom?),a

    ei
    ret

.flashromdetected: ;MFR +

    ld a,1
    ld (flashrom?),a

    ei
    ret
    
.flashromrbscdetected: ;RBSC

    ld a,2
    ld (flashrom?),a

    ei
    ret


