savemenuxposition: equ 64-8
savemenuyposition: equ 80
savemenuxsize:  equ 128
savemenuysize:  equ 48
menuarrowx:  equ 64-8+4
menuarrowy:  equ 80+8+2


;the menu that gets popped inside the worldmap asking the player if he likes to save his/her progress
savemenu:

  xor a
  ld (NewPrControls),a ;prevent accidental save
  ld (forcesavemenu),a ;reset force byte
  ;ld (completedfirsttime),a ;reset force byte

  ;wait a few seconds before dropping down the menu or anything like that and play a one up sound
  call Playsfx.oneup
  
  ld a,15
.waitloop:
    halt : halt : 
    dec a
    jp nz,.waitloop
  
  

  call polllineintflag ;wait for lineint

  ld a,$1f ; Page 0 vram visible
  out ($99),a
  ld a,128 + 2
  out ($99),a


  call pollvdpcmd ;wait for any command to finish

  call polllineintflag ;wait for lineint
  call saveactivescreeninfo ;save the page0 info to page 2 

  call polllineintflag ;wait for lineint
  call copymenutoactivevram ;copy the entire menu bkg to the active vram page

  call polllineintflag ;wait for lineint
  call putarrowtovram ;make the little arrow 

  xor a
  ld (optionsmenuposition),a ;reuse optionsmenu variable

  ld a,menuarrowy
  ld (optionsmenuheight),a


savemenuloop:

  call ReadControls
;  ld a,(NewPrControls)
  ld a,(Controls)
  bit 1,a
  push af
  call nz,.movedown
  pop af
  push af
  bit 0,a
  call nz,.moveup
  pop af
  bit 4,a
  jp nz,.quitsavemenu


  call polllineintflag ;wait for interupt

  jp savemenuloop

.moveup:

  ld a,(optionsmenuposition)
  and a
  ret z ;arrow is allready at this position
  dec a
  ld (optionsmenuposition),a

  ld a,(optionsmenuheight)
  sub 16
  ld (optionsmenuheight),a

  call movesavearrow
  
       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
  
  ret

  


.movedown:

  ld a,(optionsmenuposition)
  and a
  ret nz ;arrow is allready at this position
  inc a
  ld (optionsmenuposition),a

  ld a,(optionsmenuheight)
  add a,16
  ld (optionsmenuheight),a

  call movesavearrow
  
       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
  
  ret



.quitsavemenu:

  call ReadControls
  ld a,(Controls)
  ;ld a,(NewPrControls)
  bit 4,a
  jp nz,.quitsavemenu
  
  
  call Playsfx.checkpoint
  
  call restoreactivescreeninfo

  ld a,(optionsmenuposition)
  and a
  ret nz

  call polllineintflag ;wait for lineint

;VERY dangerous:  If a interrupt occurs during writing to sram we may expect a huge engine crash
;we do wait for the lineint so no interrupt should occur but that simply taking big risks so we set the
;tempisr just to make sure this never happens

  call Settempisr

  ld a,loaderblock
  call block34
  

  call writesramuserdata

  
  ld a,worldmapengineblock
  call block34

  
  call enableworldmapinterrupt
  
  ret



movesavearrow:

;make part of vram black first

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ld a,menuarrowx
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd

  ld a,savemenuyposition
  out ($9b),a ;Yd
  xor a
  out ($9b),a ;Yd

  ld a,6
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,48
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  ld a,$77
  out ($9b),a ;color black
  xor a
  out ($9b),a ;45
  
  ld a,%11000000
  out ($9b),a ;execute command
  
  ei
  
  call pollvdpcmd 


;write the menu bkg to the active VRAM

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ld a,menuarrowx
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,(optionsmenuheight)
  out ($9b),a ;Yd
  xor a
  out ($9b),a ;Yd

  ld a,6
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,5
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  ld hl,saveoptionitemarrow
  ld a,(hl)
  out ($9b),a ;44 color
  inc hl
  
  xor a
  out ($9b),a ;45 whatever
  
  ld a,%11110000
  out ($9b),a ;command  

  ld b,15-1

.firstcopyloop:

  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  
  inc hl
  
  djnz .firstcopyloop  
  
  ei


  ret
  
  



putarrowtovram:

;write the menu bkg to the active VRAM

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ld a,menuarrowx
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,menuarrowy
  out ($9b),a ;Yd
  xor a
  out ($9b),a ;Yd

  ld a,6
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,5
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  ld hl,saveoptionitemarrow
  ld a,(hl)
  out ($9b),a ;44 color
  inc hl
  
  xor a
  out ($9b),a ;45 whatever
  
  ld a,%11110000
  out ($9b),a ;command  

  ld b,15-1

.firstcopyloop:

  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  
  inc hl
  
  djnz .firstcopyloop  
  
  ei


  ret



copymenutoactivevram:

;write the menu bkg to the active VRAM

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,36
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ld a,savemenuxposition
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,savemenuyposition
  out ($9b),a ;Yd
  xor a
  out ($9b),a ;Yd

  ld a,savemenuxsize
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,savemenuysize
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  ld hl,savemenugfxadress
  ld a,(hl)
  out ($9b),a ;44 color
  inc hl
  
  xor a
  out ($9b),a ;45 whatever
  
  ld a,%11110000
  out ($9b),a ;command
 
  ei  

  ld b,63

.firstcopyloop:

  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  
  inc hl
  
  djnz .firstcopyloop

   ld de,64
   add hl,de


  call polllineintflag ;wait for lineint

  ld c,48-1

.maincopyloop:

  ld b,64

.copyloop:

  ld a,(hl)
  out ($99),a
  ld a,44+128
  out ($99),a
  
  inc hl
  
  djnz .copyloop

   ld de,64
   add hl,de
  
    call polllineintflag ;wait for lineint
  
  
  dec c
  
  jp nz,.maincopyloop


;  ei
;  call pollvdpcmd

  ret




restoreactivescreeninfo:

;save the active part we need back later in page 2

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,32
  out ($99),a
  ld a,17+128
  out ($99),a
  
  xor a
  out ($9b),a ;Xs
  out ($9b),a ;Xs

  ld a,100
  out ($9b),a ;Ys
  ld a,2
  out ($9b),a ;Ys
  
  ld a,savemenuxposition
  out ($9b),a ;Xd
  xor a
  out ($9b),a ;Xd
  
  ld a,savemenuyposition
  out ($9b),a ;Yd
  xor a
  out ($9b),a ;Yd

  ld a,savemenuxsize
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,savemenuysize
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  
  xor a
  out ($9b),a ;44 color
  out ($9b),a ;45 whatever
  
  ld a,%11010000
  out ($9b),a ;command
 
  ei  

  call pollvdpcmd

  ret



saveactivescreeninfo:

;save the active part we need back later in page 2

  ;set register 17 for indirect writing with autoincrement
  di
  ld a,32
  out ($99),a
  ld a,17+128
  out ($99),a
  
  ld a,savemenuxposition
  out ($9b),a ;Xs
  xor a
  out ($9b),a ;Xs

  ld a,savemenuyposition
  out ($9b),a ;Ys
  xor a
  out ($9b),a ;Ys
  
  xor a
  out ($9b),a ;Xd
  out ($9b),a ;Xd
  
  ld a,100
  out ($9b),a ;Yd
  ld a,2
  out ($9b),a ;Yd

  ld a,savemenuxsize
  out ($9b),a ;Xsize
  xor a
  out ($9b),a ;Xsize

  ld a,savemenuysize
  out ($9b),a ;Ysize
  xor a
  out ($9b),a ;Ysize

  
  xor a
  out ($9b),a ;44 color
  out ($9b),a ;45 whatever
  
  ld a,%11010000
  out ($9b),a ;command
 
  ei  

  call pollvdpcmd

  ret


polllineintflag:
;poll for the lineintflag. Must start at the lineint or else bad stuff gets visible
  ld a,(lineintflag)
  ld e,a
.polllineintflag:
  ld a,(lineintflag)
  cp e
  jp z,.polllineintflag

  ret


savemenugfxadress:
incbin "../grapx/worldmap/savemenu.SC5",$0000+7,6144

saveoptionitemarrow:
			db $55,$77,$77
			db $55,$55,$77
			db $55,$55,$55
			db $55,$55,$77
			db $55,$77,$77 
