;intromenu this one is a engine completely run from rom. It has its own interrupt handler calling the music and
;sfx player like the optionsmenu but also performs a spritesplit making it possible for the menu to exsist.
;it effectivly wipes all characters of screen except for the big sprites above.

intromenu:

;wipe the NewPrControls to prevent accidental selection
  xor a
  ld (NewPrControls),a
  ld (Controls),a

;store the previous controller
  ld hl,ReadControls+1
  ld a,(hl)
  ld e,a
  inc hl
  ld a,(hl)
  ld d,a
  ld (cdevice),de
  
  ;set Controls to keyboard
  ld hl,ReadControls+1 ;write the jump
  ld de,ReadControls.keyboard
  ld (hl),e
  inc hl
  ld (hl),d

  ld    a,64 ;set the linesplit correctly
  di
  out   ($99),a
  ld    a,19+128
;  ei
  out   ($99),a

;  di
  ld    hl,intromenuisr ;set menu InterruptHandler
  ld    ($38+1),hl
  ld    a,$c3
  ld    ($38),a 
  ei

;build static menu items first
  ld    a,intromenuspritesblock
	call	block34               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 1+2

  
  ld b,9 ;double sprite count to be upload
  ld de,0
  
.uploadsprites: ;load all the required sprites in the VRAM

  push de
  exx
   
  ;set vdp address to write at color table and write spritecolor
	ld		hl,$5400 ;set color address
   exx ;get de
   ex de,hl
   add hl,hl ;*2
   add hl,hl ;*4
   add hl,hl ;*8
   add hl,hl ;*16
   add hl,hl ;*32
   ex de,hl
   push de
   exx
   pop de
   push de
   add hl,de
	ld		a,1
	call	SetVdp_Write
    
	ld hl,coloraddressa
	pop de
	add hl,de
	push de
    
  ld		c,$98
	ld    a,2
	call colorloop

    ;set vdp address to write at character table and write spritecharacter
	ld    hl,$5800
	exx
	pop hl
	add hl,hl;*2
	push hl
	exx
	pop de
	push de
	add hl,de
	ld		a,1
	call	SetVdp_Write
  
  
	ld hl,characteraddressa
	pop de
	add hl,de
  ld		c,$98
  ld    a,2
  call characterloop
 
  exx
  pop de
  inc de
 
  djnz .uploadsprites

  
;TODO: get each number with the levelcount and upload the correct sprite  
  ld a,loaderblock
  call block34 ;get the sram module

  call readlevelprogress ;read the amount of bits set by the worldmap
  

  ld a,scorebarblock
  call block34 ;get the numbers
  
  ld a,(levelprogress)
  call convertnumbertodigitintro ;convert the progress to a number

 ld hl,$5400+224+32+32
 call uploadnumbers
 
   ld a,(levelprogress+1)
  call convertnumbertodigitintro ;convert the progress to a number
  
 
 ld hl,$5400+224+64+32+32
 call uploadnumbers
 
   ld a,(levelprogress+2)
  call convertnumbertodigitintro ;convert the progress to a number
 
 ld hl,$5400+224+128+32+32
 call uploadnumbers
 
;write SPAT menu positions  
  ld a,80
  ld (upperspat),a
  ld (upperspat+4),a
  ld (upperspat+24),a
  ld (upperspat+28),a
  ld a,80-4
  ld (upperspat+56+8+8),a ;the yellow numbers pos a
  ld (upperspat+60+8+8),a
  ld (upperspat+64+8+8),a ;the yellow numbers digit 2 pos a
  ld (upperspat+68+8+8),a
  ld a,80+16
  ld (upperspat+8),a
  ld (upperspat+12),a
  
  ld a,80+16-4
  ld (upperspat+72+8+8),a ;the yellow numbers pos b
  ld (upperspat+76+8+8),a
  ld (upperspat+80+8+8),a ;the yellow numbers digit 2 pos b
  ld (upperspat+84+8+8),a
  
  ld a,80+16+16
  ld (upperspat+16),a
  ld (upperspat+20),a
  
  ld a,80+16+16-4
  ld (upperspat+88+8+8),a ;the yellow numbers pos c
  ld (upperspat+92+8+8),a
  ld (upperspat+96+8+8),a ;the yellow numbers digit 2 pos c
  ld (upperspat+100+8+8),a
  
  
  ld a,80+16+16+16
  ld (upperspat+32),a ;continue
  ld (upperspat+36),a
  ld (upperspat+40),a
  ld (upperspat+44),a
  ld (upperspat+48),a
  ld (upperspat+52),a
  ld a,120
  ld (upperspat+1),a
  ld (upperspat+5),a
  ld (upperspat+9),a
  ld (upperspat+13),a
  ld (upperspat+17),a
  ld (upperspat+21),a
  ld (upperspat+33),a
  ld (upperspat+37),a
  ld (upperspat+57),a ;erase
  ld (upperspat+61),a
  ld a,120+16
  ld (upperspat+41),a
  ld (upperspat+45),a
  ld (upperspat+65),a ;erase
  ld (upperspat+69),a
  
  ld (upperspat+57+8+8),a ;the yellow numbers pos a
  ld (upperspat+61+8+8),a
  ld (upperspat+73+8+8),a ;the yellow numbers pos b
  ld (upperspat+77+8+8),a
  ld (upperspat+89+8+8),a ;the yellow numbers pos c
  ld (upperspat+93+8+8),a
  
  ld a,120+16+8
  ld (upperspat+65+8+8),a ;the yellow numbers digit 2 pos a
  ld (upperspat+69+8+8),a
  ld (upperspat+81+8+8),a ;the yellow numbers digit 2 pos b
  ld (upperspat+85+8+8),a
  ld (upperspat+97+8+8),a ;the yellow numbers digit 2 pos c
  ld (upperspat+101+8+8),a
  ld a,120+16+16
  ld (upperspat+49),a
  ld (upperspat+53),a
  ld a,120-16 ;pijltje
  ld (upperspat+25),a
  ld (upperspat+29),a
  

;update spat	
	ld		hl,$5600		;sprite attribute table in VRAM ($15600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,upperspat			;sprite attribute table
  ld    c,$98
	call	outix128
  
  xor a
  ld (intromenuposition),a ;clear the menuposition
  
  ld a,loaderblock
  call block34 ;get the sram module
  
  
  call intromenuloop


  xor a ;reset linesplit value
  di
  out   ($99),a
  ld    a,19+128
  out   ($99),a

  ld    hl,demointhandler ;set engine InterruptHandler
  ld    ($38+1),hl
  ld    a,$c3
  ld    ($38),a 
  ei 

  ret

;NOTE: this is a jp from a call and thus if a ret is given the menu exits
erasemenu:

       ld    de,03*256 + sfxchuckstomp
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number

  ;remove the continue sprite
    ld a,226
  ld (upperspat+32),a
  ld (upperspat+36),a
  ld (upperspat+40),a
  ld (upperspat+44),a
  ld (upperspat+48),a
  ld (upperspat+52),a 

  ;put erase sprite in place
    ld a,80+16+16+16
  ld (upperspat+56),a
  ld (upperspat+60),a
  ld (upperspat+64),a
  ld (upperspat+68),a
  
  
    call intromenuloop.updatespat

.erasemenuloop:

  call ReadControls
  ld a,(NewPrControls)
 ; ld a,(Controls)
  bit 4,a
  jp nz,.exittoworldmap
  bit 6,a
  jp nz,.exittoworldmap
  bit 1,a
  call nz,intromenuloop.movearrowdown
  ld a,(NewPrControls)
  bit 0,a
  call nz,intromenuloop.movearrowup



  jp .erasemenuloop

.exittoworldmap:


  ld a,(intromenuposition)
  cp 0
  call z,intromenuloop.sramposa
  ld a,(intromenuposition)
  cp 1
  call z,intromenuloop.sramposb
  ld a,(intromenuposition)
  cp 2
  call z,intromenuloop.sramposc  
  cp 3
  jp z,intromenuloop.setcontinue
  
  
  call erasesramdata

;TODO: check at which menuposition we are and execute the right code

  ld a,1
  ld (levelend?),a
  ld (demoend?),a
  
.waitsfx:  

  ld a,(Sfxon?)
  and a
  jp nz,.waitsfx ;wait for any sound to finish
    

  ret


intromenuloop:

;get numbers from the scorebar block get the statics from the intromenuspritesblock!
  call ReadControls
  ld a,(NewPrControls)
  bit 4,a
  jp nz,.exittoworldmap
  bit 6,a
  jp nz,.exittoworldmap
  bit 1,a
  call nz,.movearrowdown
  ld a,(NewPrControls)
  bit 0,a
  call nz,.movearrowup


  jp intromenuloop


.exittoworldmap:

    ;restore Controls
    ld de,(cdevice)
  ld hl,ReadControls+1 ;write the jump
  ld (hl),e
  inc hl
  ld (hl),d


  ld a,(intromenuposition)
  cp 0
  call z,.sramposa
  ld a,(intromenuposition)
  cp 1
  call z,.sramposb
  ld a,(intromenuposition)
  cp 2
  call z,.sramposc  
  ld a,(intromenuposition)
  cp 3
  jp z,erasemenu

;TODO: check at which menuposition we are and execute the right code

  ld a,1
  ld (levelend?),a
  ld (demoend?),a
  
.waitsfx:  

  ld a,(Sfxon?)
  and a
  jp nz,.waitsfx ;wait for any sound to finish
    

  ret

.setcontinue:

  ld a,80+16+16+16
  ld (upperspat+32),a
  ld (upperspat+36),a
  ld (upperspat+40),a
  ld (upperspat+44),a
  ld (upperspat+48),a
  ld (upperspat+52),a

  ld a,226 ;remove erase sprite
  ld (upperspat+56),a
  ld (upperspat+60),a
  ld (upperspat+64),a
  ld (upperspat+68),a
  
  
  call intromenuloop.updatespat

  jp intromenuloop
  
  
.sramposa:

  ld hl,$F8;$E7
  ld (srampointer),hl
  ret


.sramposb:

  ld hl,$F9;$EF
  ld (srampointer),hl
  ret

.sramposc:

  ld hl,$FA;$F7
  ld (srampointer),hl
  ret




.movearrowdown:

  ld a,(intromenuposition)
  cp 3
  ret nc ;not allowed to go further down

  inc a
  ld (intromenuposition),a

  ld a,(upperspat+24)
  ld c,16
  add a,c
  ld (upperspat+24),a
  ld (upperspat+28),a

         ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number
  
  jp .updatespat


;  ret

.movearrowup:

  ld a,(intromenuposition)
  and a
  ret z ;not allowed to go further up

  dec a
  ld (intromenuposition),a

  ld a,(upperspat+24)
  ld c,16
  sub a,c
  ld (upperspat+24),a
  ld (upperspat+28),a

       ld    de,02*256 + sfxstomp5
       call  SetSfxnrAndPriority ;Set sfx, in: d=priority, e=Sfx number



 ; ret


.updatespat:
;update spat	
	ld		hl,$5600		;sprite attribute table in VRAM ($15600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,upperspat			;sprite attribute table
  ld    c,$98
	call	outix128
  ret


characterloop:
	call	outix32                 ;out 32*a bytes (1 sprite=32 bytes) of sprite characterdata, hl-> adress on block
  dec   a
  jp    nz,characterloop
  ret
  
  
colorloop:
	call	outix16                 ;out 16*a bytes (1 sprite=16 bytes) of sprite colordata, hl-> adress on block
  dec   a
  jp    nz,colorloop
  ret


yellownrtableintro:	dw coloraddressnumber0yellow,characteraddressnumber0yellow,coloraddressnumber1yellow,characteraddressnumber1yellow
		dw coloraddressnumber2yellow,characteraddressnumber2yellow,coloraddressnumber3yellow,characteraddressnumber3yellow
		dw coloraddressnumber4yellow,characteraddressnumber4yellow,coloraddressnumber5yellow,characteraddressnumber5yellow
		dw coloraddressnumber6yellow,characteraddressnumber6yellow,coloraddressnumber7yellow,characteraddressnumber7yellow
		dw coloraddressnumber8yellow,characteraddressnumber8yellow,coloraddressnumber9yellow,characteraddressnumber9yellow
		
		
;convert number to digits in = a the number that needs conversion out = the digits in numbertable
convertnumbertodigitintro:

  ;find first digit
  ld hl,numbertable
  ld b,0
  ld c,a ;save this value too in case loop is smaller than 100
.loop100:
  sub 100
  jp c,.done100
  inc b
  ld c,a ;save current value
  jp .loop100

;write data and set values for next digit  
.done100:
  ld a,b
  ld (hl),a
  inc hl ;next digit
  ld b,0
  ld a,c
.loop10:
  sub 10
  jp c,.done10
  inc b
  ld c,a
  jp .loop10

.done10:
  ld a,b
  ld (hl),a
  inc hl
  
  ld a,c
  ld (hl),a

  ret
  

uploadnumbers:

  push hl
    
    exx
    pop hl
    push hl
    ld de,$5400 ;base address
    xor a
    sbc hl,de ;now we have the difference in hl
    add hl,hl ;*2
    ex de,hl
    ;push hl
    exx

    

    ;set vdp address to write at color table and write spritecolor
;	ld		hl,$5400+224
	ld		a,1
	call	SetVdp_Write

	ld hl,numbertable+1
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,yellownrtableintro
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;			      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call colorloop
	

	ld hl,numbertable+2
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,yellownrtableintro
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;			      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call colorloop	
  
  
  
  ;set vdp address to write at character table and write spritecharacter
;  ld    hl,$5800+448

	exx
	pop hl
	ld hl,$5800
	add hl,de
	push hl
	exx
	pop hl


	ld		a,1
	call	SetVdp_Write
	

	ld hl,numbertable+1
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,yellownrtableintro
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call characterloop


	ld hl,numbertable+2
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,yellownrtableintro
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call characterloop
  
	ret
  
  
  
  

