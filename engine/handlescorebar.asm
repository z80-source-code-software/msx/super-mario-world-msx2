handlepowerreserve:
  
  ld a,(droppowerup)
  and a
  ret z ;nothing to do

;in here remove the powerup from the scorebar and spawn the powerup that drops down for mario
 
  xor a
  ld (droppowerup),a
;  ld (powerreserve),a
 
  push ix

  ld hl,(camerax)
  ld b,10

  ld    iy,CustomObject
  ld    (iy),available
  ld    (iy+1),l                ;set x custom object
  ld    (iy+2),h
  ld    (iy+3),b                ;set y custom object
  ld    (iy+4),0                ;set y custom object
  ld    (iy+5),14                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)
  ld    (iy+6),0                ;set object number (10 is questionmark, 11 is exclamationmark, 12 is rotating block)

  call  activateobject


  ld (ix+movepat),54
  ld (ix+v2),1
  ld a,(powerreserve)
  ld (ix+v4),a

  ld hl,(camerax) ;scherm positie bepalen waar is onze camera
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8
  
  
  ;we missen een stukje informatie in camerax die wij toevoegen door de vdp van de camera uit te lezen
  ld		a,(VDP_8+19)
  ld c,a
  ld b,0
  xor a
  sbc hl,bc
;  ld b,h ;transfer hl->bc
;  ld c,l
 
  ld de,128
  add hl,de ;position the sprite to the middle of the screen
 
 
  ld (ix+xobject),l
  ld (ix+xobject+1),h


  ld hl,(cameray) ;scherm positie bepalen waar is onze camera
  ld h,0
  
  
  sla l
  rl h ;*2
  sla l
  rl h ;*4
  sla l
  rl h ;*8

  ld		a,(VDP_8+15) ;verticale offset fixen
  ld b,0
  ld c,a
  add hl,bc

  ld de,2;10
  add hl,de


  ld (ix+yobject),l
  ld (ix+yobject+1),h
;  ld    (ix+updatespr?),2
  
  pop ix

  xor a
  ld (powerreserve),a

  ld a,226 ;kill the spat as well
  ld    (upperspat),a
  ld    (upperspat+4),a

  ret

handlescorebarsprites:
 
  ld a,(scorebaractive)
  and a
  ret z
 
  ld a,(showpowerup)
  cp 2
  jp z,.handletime
 
  ld a,(showx)
  and a
  jp nz,.handlex 
  ld a,(showtime)
  and a
  jp nz,.handletime
  ld a,(showpowerup)
  and a ;preserve the byte
  jp nz,.handlepowerup
  ld a,(showcoin)
  and a
  jp nz,.handlecoin
  ld a,(showyoshi)
  and a
  jp nz,.handleyoshi 
   
  ret
   

.handlex:


  ret

.handletime:


;wip
  ld a,(time)
  call convertnumbertodigit

   ;set ingamespritesblock in page 1+2
;	ld		a,(slot.page12rom)	    ;all RAM except page 1+2
;	out		($a8),a	
  ld    a,scorebarblock
	call	block34               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 1+2
  
    ;set vdp address to write at color table and write spritecolor
	ld		hl,$5400+256 ;one further ;$5656
	ld		a,1
	call	SetVdp_Write
    
	ld hl,coloraddresstime
    
  	ld		c,$98
	ld    a,2
	call .colorloop
  ;/set vdp address to write at color table and write spritecolor
;handle the numbers themselves

	ld hl,numbertable 
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call .colorloop

	ld hl,numbertable+1
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;			      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call .colorloop
	

	ld hl,numbertable+2
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;			      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call .colorloop	
  
  
  
  ;set vdp address to write at character table and write spritecharacter
  ld    hl,$5800+512
	ld		a,1
	call	SetVdp_Write
  
      ld hl,characteraddresstime
	ld		c,$98
  ld    a,2
  call .characterloop

  
  	ld hl,numbertable
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;	      ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call .characterloop

	ld hl,numbertable+1
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call .characterloop


	ld hl,numbertable+2
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,yellownrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call .characterloop
  
  
;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

  ret

.handlepowerup:
.handlecoin:


  ld a,(gold)
  call convertnumbertodigit  
  
    ;set ingamespritesblock in page 1+2
;	ld		a,(slot.page12rom)	    ;all RAM except page 1+2
;	out		($a8),a	
  ld    a,scorebarblock
	call	block34               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 1+2
  
    ;set vdp address to write at color table and write spritecolor
	ld		hl,$5400+32 ;one further
	ld		a,1
	call	SetVdp_Write
    
	ld hl,coloraddresscoinx
    
  	ld		c,$98
	ld    a,2
	call .colorloop
  ;/set vdp address to write at color table and write spritecolor
;handle the numbers themselves

	ld hl,numbertable+1 
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,whitenrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call .colorloop

	ld hl,numbertable+2
	ld a,(hl)
	add a,a ;*2
	add a,a ;*4
	ld d,0
	ld e,a
	ld hl,whitenrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;			      ld hl,coloraddressnumber0white
	ld		c,$98
	ld    a,2
	call .colorloop
	
  
  
  
  
  ;set vdp address to write at character table and write spritecharacter
  ld    hl,$5800+64
	ld		a,1
	call	SetVdp_Write
  
      ld hl,characteraddresscoinx
	ld		c,$98
  ld    a,2
  call .characterloop

  
  	ld hl,numbertable+1
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,whitenrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;	      ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call .characterloop

	ld hl,numbertable+2
	ld a,(hl)
	add a,a
	add a,a ;*4 
	inc a
	inc a
	ld d,0
	ld e,a
	ld hl,whitenrtable
	add hl,de
	ld e,(hl)
	inc hl
	ld d,(hl)
	ex de,hl
	
	;output digit
;		ld hl,characteraddressnumber0white
	ld		c,$98
	ld    a,2
	call .characterloop
  
  
  
;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

  ret



.handleyoshi:

  
  
    ;set ingamespritesblock in page 1+2
;	ld		a,(slot.page12rom)	    ;all RAM except page 1+2
;	out		($a8),a	
  ld    a,scorebarblock
	call	block34               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 1+2
  
    ;set vdp address to write at color table and write spritecolor
	ld		hl,$5400+128 ;two further
	ld		a,1
	call	SetVdp_Write
    
		 ld hl,coloraddresscoin
    
  	ld		c,$98
	ld    a,2
	call .colorloop
		ld hl,coloraddresscoin
    
  	ld		c,$98
	ld    a,2
	call .colorloop
		ld hl,coloraddresscoin
    
  	ld		c,$98
	ld    a,2
	call .colorloop
		ld hl,coloraddresscoin
    
  	ld		c,$98
	ld    a,2
	call .colorloop
  ;/set vdp address to write at color table and write spritecolor
;handle the numbers themselves

  
  
  ;set vdp address to write at character table and write spritecharacter
	ld    hl,$5800+256
	ld		a,1
	call	SetVdp_Write
  
	    ld hl,characteraddresscoin
      ld		c,$98
      ld    a,2
      call .characterloop
            ld hl,characteraddresscoin
      ld		c,$98
      ld    a,2
      call .characterloop
            ld hl,characteraddresscoin
      ld		c,$98
      ld    a,2
      call .characterloop
            ld hl,characteraddresscoin
      ld		c,$98
      ld    a,2
      call .characterloop
  
  
  
;  ld		a,(slot.ram)	          ;back to full RAM
;  out		($a8),a

  ret


.showtimeandpwr:
   

  ret

  
.characterloop:
	call	outix32                 ;out 32*a bytes (1 sprite=32 bytes) of sprite characterdata, hl-> adress on block
  dec   a
  jp    nz,.characterloop
  ret
  
  
.colorloop:
	call	outix16                 ;out 16*a bytes (1 sprite=16 bytes) of sprite colordata, hl-> adress on block
  dec   a
  jp    nz,.colorloop
  ret

handlescorebar:

;PROBLEM: the uploading of the sprites must happen at vblank at the start of the first int BEFORE the spritesplit is set. If this takes too long (it allready is)
;a flicker is seen in the scorebar at the moment the spat is being updated. The spat MUSt be updated before the first spritesplit.
;A good option is to update the sprites after the spritesplit. The update process will be invisible but difficult to time. Doing this on the lineint is dangerous.
;best is to do the update at any position where it is certain that we are below the lineint. This can be timed fairly well. Its crappy but it is the safest way

;no need to update any info on the scorebar if not active save VDP and cpu time
  ld a,(scorebaractive)
  and a
  ret z
  
  ld a,(removebar)
  dec a
  jp z,.removebar

  ld a,(mariolook)
  dec a
  jp z,.keepactive
  
  ld a,(scorebaractive)
  inc a
  ld (scorebaractive),a
  cp 80;100
  jp nc,.deactivate

.activatescorebar:
  
  ld    a,(lineintheight)   ;set lineinterrupt height
  cp 16
  jp z,.handlescorebar
  add a,2
  ld (lineintheight),a
  di
  out   ($99),a
  ld    a,19+128
  ei
  out   ($99),a

  ld a,1
  ld (upperspatmain),a
  
.handlescorebar:

 ;Here we need additional steps. All dictated by the fact that some routine elsewhere activated
 ;scorebaractive by setting it to 1
 ;then there is the activation type byte which is set by the corresponding caller
 ;showtime, showx, showcoin, showpowerup, showyoshicoin
 ;jump to the handler which will return. The handler must set the spat positions spriteaddresses
 ;then must call the sprite uploader which will upload the spat and tell the vdp what to do.

 ;difficulty nr2. We must not show some items when others are shown. We need to handle priorities on them
 
  ld a,(showpowerup)
  cp 2
  jp z,.showtimeandpwr
 
  ld a,(showx)
  and a
  jp nz,.handlex 
  ld a,(showtime)
  and a
  jp nz,.handletime
  ld a,(showpowerup)
  and a ;preserve the byte
  call nz,.handlepowerup
  ld a,(showcoin)
  and a
  jp nz,.handlecoin
  ld a,(showyoshi)
  and a
  jp nz,.handleyoshi
  
  ret ;do nothing if nothing set

;==============================================================================================

;TODO: when time is fixed make the time show up first then powerup and coins at the same time
.showtimeandpwr:


  ld a,(scorebaractive)
  cp 60
  jp nc,.setshowpower
  cp 30
  jp nc,.dohandleyoshi


  jp .handletime
  
 
.dohandleyoshi:

  xor a
  ld (showtime),a
  ld a,226
  ld    (upperspat+64),a ;timex
  ld    (upperspat+68),a
  ld	(upperspat+72),a ;digit1
  ld	(upperspat+76),a
  ld 	(upperspat+80),a ;digit2
  ld 	(upperspat+84),a
  ld 	(upperspat+88),a ;digit3
  ld 	(upperspat+92),a
  
  jp .handleyoshi
  
.setshowpower:



.next:
  xor a
 ; ld (showtime),a
  ld a,1
  ld (showcoin),a
  ld (showpowerup),a
  ld (upperspatmain),a
  
  call .handlepowerup
  ld a,226
  ld    (upperspat+64),a ;timex
  ld    (upperspat+68),a
  ld	(upperspat+72),a ;digit1
  ld	(upperspat+76),a
  ld 	(upperspat+80),a ;digit2
  ld 	(upperspat+84),a
  ld 	(upperspat+88),a ;digit3
  ld 	(upperspat+92),a
  ld    (upperspat+32),a ;yoshicoin
  ld    (upperspat+36),a
  ld	(upperspat+40),a
  ld	(upperspat+44),a
  ld 	(upperspat+48),a
  ld 	(upperspat+52),a
  ld 	(upperspat+56),a
  ld 	(upperspat+60),a
  ret
  

.handlex:

 ;dummy as well

  ret


.handleyoshi:
  
  
  ;put spat to ram
  ld		a,(VDP_8+15)
  ld    b,a

;add vertical scroll offset
  
  ld a,(yoshicoin)
  and a
  jp z,.handlespat
  
  ld a,1
  add a,b
  ld    (upperspat+32),a
  ld    (upperspat+36),a
    ld a,(yoshicoin)
    cp 2
    jp c,.handlespat
    ld a,1
    add a,b
  ld	(upperspat+40),a
  ld	(upperspat+44),a
    ld a,(yoshicoin)
    cp 3
    jp c,.handlespat
    ld a,1
    add a,b
  ld 	(upperspat+48),a
  ld 	(upperspat+52),a
    ld a,(yoshicoin)
    cp 4
    jp c,.handlespat
    ld a,1
    add a,b
  ld 	(upperspat+56),a
  ld 	(upperspat+60),a
;/add vertical scroll offset
  jp .handlespat
  

 ; ret


.handletime:

  
	
;put spat to ram
  ld		a,(VDP_8+15)
  ld    b,a

;add vertical scroll offset
;  ld a,(upperspat)
  ld    a,1            ;y
  add   a,b                             ;add vert scroll offset
  ld    (upperspat+64),a ;timex
  ld    (upperspat+68),a
  ld	(upperspat+72),a ;digit1
  ld	(upperspat+76),a
  ld 	(upperspat+80),a ;digit2
  ld 	(upperspat+84),a
  ld 	(upperspat+88),a ;digit3
  ld 	(upperspat+92),a
;/add vertical scroll offset
  jp .handlespat
  
  


;  ret


.handlecoin:


	
;put spat to ram
  ld		a,(VDP_8+15)
  ld    b,a

;add vertical scroll offset
;  ld a,(upperspat)
  ld    a,1            ;y
  add   a,b                             ;add vert scroll offset
  ld    (upperspat+8),a ;coinx
  ld    (upperspat+12),a
  ld	(upperspat+16),a ;digit1
  ld	(upperspat+20),a
  ld 	(upperspat+24),a ;digit2
  ld 	(upperspat+28),a
;/add vertical scroll offset

  jp .handlespat
  
  

 ; ret



.handlepowerup:

  
;  push af

  ;set ingamespritesblock in page 3+4
  ld    a,scorebarblock
	call	block34               ;set 4 blocks of $2000 each in page 1+2 ($4000- $bfff)
  ;/set ingamespritesblock in page 3+4

  ;set vdp address to write at color table and write spritecolor
	ld		hl,$5400
	ld		a,1
	call	SetVdp_Write


 ; pop af
  ld a,(powerreserve)
  dec a
 
  ld hl,poweruptable
  ld b,0
  add a,a ;*2
  add a,a ;*4 ;the table is in multiples of 4 bytes
  ld c,a
  add hl,bc
  push hl ;store for later

  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl
;	  ld hl,coloraddresstime
	ld		c,$98
  ld    a,2
  call .colorloop
  ;/set vdp address to write at color table and write spritecolor

  ;set vdp address to write at character table and write spritecharacter
  ld    hl,$5800
	ld		a,1
	call	SetVdp_Write

  pop hl
  inc hl
  inc hl
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl
  
;  ld hl,characteraddresstime
	ld		c,$98
  ld    a,2
  call .characterloop

;  ld		a,(slot.ram)	          ;back to full RAM
;	out		($a8),a

.handlepowerupnosprupdate:
	
	ld a,(powerreserve) ;in case you get hit before the fadout remove the powerup from the scorebar
	and a
	jp z,.destroyspat
	
;put spat to ram
	ld		a,(VDP_8+15)
  ld    b,a

;add vertical scroll offset
;  ld a,(upperspat)
  ld    a,1            ;y
  add   a,b                             ;add vert scroll offset
  ld    (upperspat),a
  ld    (upperspat+4),a
;/add vertical scroll offset

  ld a,(showpowerup)
  cp 3
  jp z,.handlespat
  ret
  
.destroyspat:

  ld    a,226            ;y
  ld    (upperspat),a
  ld    (upperspat+4),a

  ld a,(showpowerup)
  cp 3
  jp z,.handlespat
  ret
  
  
;=============================================================================================
;bug: This works but there are complications.. during the update of the spat table the update
;is visible during the split. Solution is to write to a invisible table adress and flip the tables at each frame
.v9958found:

;32 entries 4 bytes wide

  ld hl,upperspat+1 ;first table entry
  ld a,(VDP_8+10)
  ld c,a ;horizontal vdp offset
  ld b,31 ;32-1
  ld de,emptyupperspat+1
.v9958loop: 
   
  ld a,(de) 
  add   a,c
  sub   a,7
  ld (hl),a ;write back
  inc hl
  inc hl
  inc hl
  inc hl
  inc de
  inc de
  inc de
  inc de
  
  djnz .v9958loop
  
  ret


.handlespat:

;V9938 support in the menu bar we need to loop trough all the spat values and write them back accordingly
;make the horizontal coordinates count

  ld    a,(v9958found?)
  and    a
  call  z,.v9958found
  
  
;sprite split spat	
	ld		hl,$5600		;sprite attribute table in VRAM ($15600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,upperspat			;sprite attribute table
	ld    c,$98
	call	outix128

	
	
  ret


.characterloop:
	call	outix32                 ;out 32*a bytes (1 sprite=32 bytes) of sprite characterdata, hl-> adress on block
  dec   a
  jp    nz,.characterloop
  ret
  
  
.colorloop:
	call	outix16                 ;out 16*a bytes (1 sprite=16 bytes) of sprite colordata, hl-> adress on block
  dec   a
  jp    nz,.colorloop
  ret

.keepactive: ;force active if you keep looking up

  ld a,2
  ld (scorebaractive),a 


  ret
  

.deactivate:
  
  
;put spat to ram
	ld		a,(VDP_8+15)
  ld    b,a

;add vertical scroll offset
  ld a,(upperspatmain)
  inc a
  inc a
  ld (upperspatmain),a
  add   a,b                             ;add vert scroll offset
  cp 16
  ld b,a ;use result
  push af
  
  ld a,(upperspat)
  cp 128
  jp nc,.n1
  ld a,b
  ld    (upperspat),a
  ld    (upperspat+4),a
.n1:
  ld a,(upperspat+8)
  cp 128
  jp nc,.n2
  ld a,b
  ld    (upperspat+8),a ;coinx
  ld    (upperspat+12),a
  ld	(upperspat+16),a ;digit1
  ld	(upperspat+20),a
  ld 	(upperspat+24),a ;digit2
  ld 	(upperspat+28),a
.n2:
;  ld a,(upperspat+32)
;  cp 128
  ld a,(yoshicoin)
  cp 1
  jp c,.n3
  ld a,b
  ld    (upperspat+32),a ;yoshicoin
  ld    (upperspat+36),a
    ld a,(yoshicoin)
    cp 2
    jp c,.n3
    ld a,b
  ld	(upperspat+40),a
  ld	(upperspat+44),a
    ld a,(yoshicoin)
    cp 3
    jp c,.n3
    ld a,b
  ld 	(upperspat+48),a
  ld 	(upperspat+52),a
    ld a,(yoshicoin)
    cp 4
    jp c,.n3
    ld a,b
  ld 	(upperspat+56),a
  ld 	(upperspat+60),a
  
.n3:  
  
  pop af
  jp nc,.deactivatedirect


    jp .handlespat
  
  
  ;ret

  
  

.deactivatedirect:

;  xor a
;  ld (scorebaractive),a

  ld a,1
  ld (removebar),a

;prevent sprite flashing after disabling the spritesplit 
  ld a,226
  ld    (upperspat),a
  ld    (upperspat+4),a
  ld    (upperspat+8),a ;coinx
  ld    (upperspat+12),a
  ld	(upperspat+16),a ;digit1
  ld	(upperspat+20),a
  ld 	(upperspat+24),a ;digit2
  ld 	(upperspat+28),a
  ld    (upperspat+32),a ;yoshicoin
  ld    (upperspat+36),a
  ld	(upperspat+40),a
  ld	(upperspat+44),a
  ld 	(upperspat+48),a
  ld 	(upperspat+52),a
  ld 	(upperspat+56),a
  ld 	(upperspat+60),a
;/add vertical scroll offset

;sprite split spat	
	ld		hl,$5600		;sprite attribute table in VRAM ($15600) waarde word opgehaald via .setspritetables
	ld		a,1
	call	SetVdp_Write	
	ld		hl,upperspat			;sprite attribute table
  ld    c,$98
	call	outix128
  
  ret


.removebar:

  ld a,(lineintheight)
  sub a,2
  ld (lineintheight),a
  and a
  jp z,.done

  ret


.done:

  xor a
  ld (scorebaractive),a
  ld (removebar),a
;  ld (powerreserve),a
  ld (showpowerup),a
  ld (showcoin),a
  ld (showyoshi),a
  ld (showx),a
  ld (showtime),a

  ld    hl,currentpalette
  call  SetPalette ;restore pallete


  ;fix Sprite split this will prevent the nasty sprite flash visible after scorebar disappearance
	ld		a,(VDP_0+5)
	di
	out		($99),a
	ld		a,5+128
	out		($99),a
	ld		a,(VDP_8+3)
	out		($99),a
	ld		a,11+128
	out		($99),a
	
	ld		a,(VDP_0+6)
	out		($99),a
	ld		a,6+128
	ei
	out		($99),a

  ret


poweruptable: dw coloraddressgreenmushroom2,characteraddressgreenmushroom2,coloraddressredmushroom2,characteraddressredmushroom2
              dw coloraddressyellowflowerleft2,characteraddressyellowflowerleft2,coloraddressfeatherleft2,characteraddressfeatherleft2
              dw coloraddressstaryellow2,characteraddressstaryellow2
yellownrtable:	dw coloraddressnumber0yellow,characteraddressnumber0yellow,coloraddressnumber1yellow,characteraddressnumber1yellow
		dw coloraddressnumber2yellow,characteraddressnumber2yellow,coloraddressnumber3yellow,characteraddressnumber3yellow
		dw coloraddressnumber4yellow,characteraddressnumber4yellow,coloraddressnumber5yellow,characteraddressnumber5yellow
		dw coloraddressnumber6yellow,characteraddressnumber6yellow,coloraddressnumber7yellow,characteraddressnumber7yellow
		dw coloraddressnumber8yellow,characteraddressnumber8yellow,coloraddressnumber9yellow,characteraddressnumber9yellow
whitenrtable:	dw coloraddressnumber0white,characteraddressnumber0white,coloraddressnumber1white,characteraddressnumber1white
		dw coloraddressnumber2white,characteraddressnumber2white,coloraddressnumber3white,characteraddressnumber3white
		dw coloraddressnumber4white,characteraddressnumber4white,coloraddressnumber5white,characteraddressnumber5white
		dw coloraddressnumber6white,characteraddressnumber6white,coloraddressnumber7white,characteraddressnumber7white
		dw coloraddressnumber8white,characteraddressnumber8white,coloraddressnumber9white,characteraddressnumber9white
		
		
;heldenpoging in = a het getal out = de digits in numbertable
convertnumbertodigit:

  ;find first digit
  ld hl,numbertable
  ld b,0
  ld c,a ;save this value too in case loop is smaller than 100
.loop100:
  sub 100
  jp c,.done100
  inc b
  ld c,a ;save current value
  jp .loop100

;write data and set values for next digit  
.done100:
  ld a,b
  ld (hl),a
  inc hl ;next digit
  ld b,0
  ld a,c
.loop10:
  sub 10
  jp c,.done10
  inc b
  ld c,a
  jp .loop10

.done10:
  ld a,b
  ld (hl),a
  inc hl
  
  ld a,c
  ld (hl),a

  ret
  

emptyupperspat:						;sprite attribute table above the spritesplit
	db		226,120,000,0	,226,120,004,0	,226,200,008,0	,226,200,012,0 ;coinx+powerup
	db		226,216,016,0	,226,216,020,0	,226,224,024,0	,226,224,028,0 ;showcoin
	db		226,150,032,0	,226,150,036,0	,226,158,040,0	,226,158,044,0 ;yoshicoin
	db		226,166,048,0	,226,166,052,0	,226,174,056,0	,226,174,060,0 ;yoshicoin

	db		226,040-24,064,0	,226,040-24,068,0	,226,048-24,072,0	,226,048-24,076,0 ;timex + time
	db		226,056-24,080,0	,226,056-24,084,0	,226,064-24,088,0	,226,064-24,092,0 ;time
	db		226,000,096,0	,226,000,100,0	,226,000,104,0	,226,000,108,0 ;x +xdigit
	db		226,000,112,0	,226,000,116,0	,226,000,120,0	,226,000,124,0 ;xdigit	  	
	
;===================================================================[move to ROM area]=============================================================================
