showsmwversion:

  ld a,1
  call CHSCR ;change to screen 1 
  
  xor a
  call cls
  
  ld hl,version
  
.looptekst:
  ld a,(hl)
  and a
  ld b,250
  jp z,.done
  call $a2
  
  inc hl
  jp .looptekst
  
.done:

    halt
    djnz .done
    ret
    
    
    
    

version: db "SMW VERSION: May release-1",0 
