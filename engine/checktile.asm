;================================================[Alle tilecheck routines beginnen vanaf hier]========================

checktileleftmarioh:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktileleftmariohram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktilerightmarioh:

  ld a,(ditileanimations)
  and a
  ret nz

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktilerightmariohram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret
  
  
checktileleftmariol:

  ld a,(ditileanimations)
  and a
  ret nz

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktileleftmariolram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktilerightmariol:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktilerightmariolram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret

checktileleftmario:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktileleftmarioram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktilerightmario:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktilerightmarioram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktilebelowmarioahead:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktilebelowmarioaheadram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktilebelowmario:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktilebelowmarioram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


checktileabovemario:

  ld a,(ditileanimations)
  and a
  ret nz


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checktileabovemarioram
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret


;read mapdata and get block info will return zero if the block = 191
;tilewaarde komt in (hl)
checktilebelowmarioram:


 ld a,(forcetile)
 and a
 jp nz,.setcarry

;tilecorner van te voren resetten
  xor a
  ld (tilecorner),a
  ld (tilecornertype),a
;mario moet door de map heen kunnen pleuren bij een diep gat
  ld a,(mariocoordinates)
  cp 180
  ret nc

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
  
;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat


;  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem 
;  cp (hl)
;  jp c,excludesheertile

  call nosheercheck
  push af
  call z,.resetmariosheertile
  pop af
  jp z,.skip241check


  ld a,138
  cp (hl)
  jp z,.setcarryandfall
  inc hl
  ld a,138
  cp (hl)
  jp z,.setcarryandfall
  dec hl


;TODO: deze tiledetectie detecteert een niet schuine tile onder een rechtse schuine tile.
  inc hl
  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  jp c,excludesheertile

  dec hl ;hl goed zetten voor volgende tilecheck

;deze tile ligt lager dunno waarom
  ld a,246 ;tiles naar rechts
  cp (hl)
  jp c,excludesheertile
  


;reset de sheertile value bij elke frame
  xor a
  ld (mariosheertile),a;reset de sheertile value bij elke frame


  ;nu verhogen we de coordinaten een beetje zodat mario in het bovenste laagje komt. In een hoekje zal een beetje
  ;vaagheid zichtbaar zijn maar het is gewoon niet anders
  ;als we hem in zijn geheel niet doen dan werkt alles perfect op opgaande schuine ramp
  ;belangrijk is dat het onderste laagje nog wel word gezien door de 191 check dus die moet je met een nocarry excluden  
  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
;  jp c,.nocarry
 jp c,.incmarioy

.skip241check: 



  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret c   ;foreground tile found, then return
  inc hl
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret 


.resetmariosheertile:

  xor a
  ld (mariosheertile),a
  


  ret

  
.nocarry:

  xor a
  ret

.setcarryandfall:

  ld a,(currentlevel)
  and a
  ret nz
  

  ld a,1
  ld (mariospeed),a
  ld (uppersheercornerfound),a
  
  xor a
  ret

.incmarioy:

  ld hl,(marioy)
  ld de,4
  xor a
  sbc hl,de
  ld (marioy),hl
  xor a
  ret

.setcarry:

  scf

  ret

;Deze routine kijkt welke kant op we schuin staan en zet dan een bitje
excludesheertile:


;when the vine bit is active we ignore the sheertiles in total
  ld a,(vineactive)
  dec a
  jp z,.setclimbing


;wel van te voren checken of we al aan het vallen zijn. In dat geval moeten we floatcheck negeren
;anders krijgen we een soort dubbele landing op de schuine tiles. 
  ld a,(fallspeed)
  cp 0
  jp nz,.skipfloatcheck  

;geen sheertile bit uitgeven wanneer mario in een jump zit anders krijg je wierdo jumps zodra je 
;een schuine tile aanraakt
  ld a,(mariofloating)
  dec a
  ret z


.skipfloatcheck:


;TODO: Mario lockt direct op de tile zodra deze gedetected word. Wat dus moet gebeuren is
;dat mario zijn snelheid word aangepast en de lock pas plaatvind wanneer mario echt op de tile staat.


  ;hier gaan we bepalen of de sheer tile naar rechts of naar links staat
    ld a,246 ;242 - 250 ;eerst hoge checken 
  cp (hl)
  jp c,.excludesheertileright  
    ld a,241 ;242 - 250 ;dan lage
  cp (hl)
  jp c,.excludesheertileleft
  
;  jp .nocarry
  
  ret


;set mario to climbing mode
.setclimbing:

;no climbing if on yoshi!!
  ld a,(marioyoshi)
  dec a
  jp z,.nocarry

;if we are jumping we do not revert into this mode
  ld a,(Controls)
  bit 4,a
  jp nz,.nocarry
  bit 0,a
  jp z,.nocarry ;we need to look up as well
  
;we cannot spinjump into the vine  
  ld a,(mariospinjump)
  and a
  ret nz


  ld a,1
  ld (marioclimbing),a
  
  ;reset the carry
  xor a
  ret



.excludesheertileright:


  call checkforsheercorner
  
  ;je staat op een special place ignore de corner check volledig
  ld a,(tiletypeignore)
  dec a
  jp z,.nocarry 
  
  ld a,(tilecorner)
  dec a 
  jp z,.setcarry

  ld a,2
  ld (mariosheertile),a


;wanneer mario rent dan kloppen de onderstaande berekeningen gewoon niet meer en kan mario dus door de schuine tiles heen
;het is lastig om het voor elkaar te krijgen maar het kan dus moeten we altijd een carry terugeven bij hoge snelheden 
  ld a,(mariospeed)
  cp 4
  jp nc,.setcarry


  push bc
  push hl

  ld hl,(mariox)
  ld a,l
  and %00000111 ;we need the last bits
  ld b,a ;wegschrijven
;we weten nu de afstand tot het midden
;nu moeten we de afstand van de y coordinaten uitlezen

  ld hl,(marioy)
  ld a,l
  and %00000111 ;7 pixels
  ld c,a ;y distance vanaf bovenkant
  
  ld a,b
  sub c
  ld b,a ;y van de x distance aftrekken. Nu weten we de true distance vanaf de rand
  
  ld a,(fallspeed)
  ld c,a
  ld a,b
  sub c
;  and %10000000 ;pollen op negatief getal
  rlca ;pollen op negatief getal
  pop bc
  pop hl 
  ret

  
;  jp .setcarry

;  ret

.excludesheertileleft:


;check of mario in het hoekje staat zo ja fuck deze shit!
  call checkforsheercorner
  ld a,(tilecorner)
  dec a
  jp z,.setcarry

;de schoonheidsprijs winnen we door eerst te kijken waar mario zich bevind en dan zijn huidige snelheid 
;uit te lezen en te vergelijken. We skippen de detectie als mario dichter in de buurt van de tile kan komen zonder
;er doorheen te vallen

  ld a,1
  ld (mariosheertile),a
 
;wanneer mario rent dan kloppen de onderstaande berekeningen gewoon niet meer en kan mario dus door de schuine tiles heen
;het is lastig om het voor elkaar te krijgen maar het kan dus moeten we altijd een carry terugeven bij hoge snelheden 
  ld a,(mariospeed)
  cp 4
  jp nc,.setcarry
 

  push bc
  push hl

  ld hl,(mariox)
  ld a,l
  and %00000111 ;we need the last bits
  ld b,a
  ld a,7 ;maar waarom werkt het wel als dit 7 is??!!
  sub b
  ld b,a ;wegschrijven
;we weten nu de afstand tot het midden
;nu moeten we de afstand van de y coordinaten uitlezen

  ld hl,(marioy)
  ld a,l
  and %00000111 ;7 pixels
  ld c,a ;y distance vanaf bovenkant
  
  ld a,b
  sub c
  ld b,a ;y van de x distance aftrekken. Nu weten we de true distance vanaf de rand
  
  ld a,(fallspeed)
  ld c,a
  ld a,b
  sub c
;  and %10000000 ;pollen op negatief getal
  rlca ;pollen op negatief getal
  
  pop bc
  pop hl
  ret
  
  
;  jp nc,.nocarry

;  jp .setcarry


.setcarry:

   xor a
   cp 1
   ret

.nocarry:


  xor a
  ret



;=========================bekijk rechts links en beneden en tel daarbij het resultaat op en geef daarvoor een nummer 
;terug==
;we checken dus of mario op een hoekje staat en zo ja wat voor een hoekje het is en of we dan mogen pleuren
;ja of nee.
checkforsheercorner:


;alle waarden op nul zetten
  xor a
  ld (tilecorner),a
  ld (tilecornertype),a
  ld (tiletypebelow),a
  ld (tiletypebelowtwo),a
  ld (tiletypeignore),a



;onderste tile checken en dan de tile ernaast ze moeten beide verschillend zijn 
  call checktilebelowmario2
  
  ld a,(tiletypebelow)
  ld b,a
  ld a,(tiletypebelowtwo)
  and b
  ret z

 
 
  ld a,1
  ld (tilecorner),a
  ld (tilecornertype),a
  
  ld a,(tiletypebelowtwo)
  dec a
  ret nz
  
  ld a,2
  ld (tilecornertype),a
 
 ;we kijken puur onder de voet van mario en als hij niet op een schuin vlak staan zetten we de type alsnog niet
  call checktilebelowmario3
  ret c
  
  xor a
  ld (tilecornertype),a
 
 
  ret

  

checktilebelowmario2:


;mario moet door de map heen kunnen pleuren bij een diep gat
  ld a,(mariocoordinates)
  cp 180
  ret nc

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;in het laagje tussen twee 2 schuine tiles in detect hij verschil en dat hoort niet

  call nosheercheck
  ret z


  ld a,246 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  jp c,.settilebelowone
    
  inc  hl  ;also check the next tile
  ld a,246
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  call c,.settilebelowone
  
  dec hl

  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  jp c,.settilebelowtwo

    
  inc  hl  ;also check the next tile
  ld a,241
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.settilebelowtwonext

  ret


.settilebelowone:

  ld a,1
  ld (tiletypebelow),a
  ret
  
.settilebelowtwo:

  ld a,1
  ld (tiletypebelowtwo),a
  ret


.settilebelowtwonext:

  cp (hl)
  jp c,.setignore

  ld a,1
  ld (tiletypebelowtwo),a
  ret

;we zien dat mario tussen twee rechtsom tiles instaat dus negeren we gewoon het resultaat
.setignore:
  
;check of mario wel echt op een schuine tile staat voordat we de ignoretype zetten  
  call checktilebelowmario4
  jp c,.setbothtiletypes  
    
  ld a,1
  ld (tiletypeignore),a
  ret

;we moeten toch een corner uitgeven dus we vertellen de routine dat we een verschil hebben gevonden.
.setbothtiletypes:
  
  ld a,1
  ld (tiletypebelow),a
  ld (tiletypebelowtwo),a
    ret

checktilebelowmario3:


;mario moet door de map heen kunnen pleuren bij een diep gat
  ld a,(mariocoordinates)
  cp 180
  ret nc

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

  call nosheercheck
  jp z,setcarry

  ld a,241
  cp  (hl) ;kijk of de huidige tile groter is dan 241

  ret
  
  
checktilebelowmario4:


;mario moet door de map heen kunnen pleuren bij een diep gat
  ld a,(mariocoordinates)
  cp 180
  ret nc

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

  ld a,191
  cp  (hl) ;kijk of de huidige tile groter is dan 191

  ret 



;================================================================================




;zelfde als voor below maar nu checken we de tiles boven mario
checktileabovemarioram:

;speciale actie is nog niet aanwezig
    xor a
    ld (mariospecialtile),a


  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l
  
  ld  a,l ;a is dus nu marioy/8
  add a,1 ;1 tile lager, look above mario's head
  ld e,a ;tijdelijk wegschrijven
  
;grote mario??  
  ld a,(mariostate)
  and a
  call nz,.addonetile 
  
  ld a,e ;temp result terughalen
  
;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in de

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8
  
   
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat
 
  push hl
  pop bc ;return this
 

  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

  ld    a,191 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of de huidige tile groter is dan 191
 ;NEW COMMIT 
  dec hl
  ;check again now lower. Maybe mario went through the first row and needs to be stopped!
  xor a
  ld de,(maplenght)
  add hl,de


  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

  ld    a,191 ;alle hardtiles
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of de huidige tile groter is dan 191
  
  
  ret   
  
  
.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret
  
 
.addonetile:
  
  ld a,(marioduck)
  and a
  ret nz
  
  ld  a,l ;a is dus nu marioy/8
    ld e,a  
  
  ret



;rechts van mario
checktilerightmarioram:

  ld hl,(marioy) ;16 bits
  
;ld de,7
;add hl,de


  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,3 ;1 tile lager, look above mario's head
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8
  inc de ;meer naar links kijken
;  inc de


  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;hiero
  call nosheercheck
  jp z,.skip241check ;BUG: sometimes a zero flag is being returned anyway. It is fixed by issuing a inc a before ret


  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  call c,excludesheertile
    
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  call c,excludesheertile

;tja zo kunnen we nooit in dat dode hoekje belanden


;  dec hl ;hl goed zetten voor volgende tilecheck

.skip241check:

;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt
  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile



  dec hl ;hl goed zetten voor volgende tilecheck


  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

;een te hoge mariospeed maakt de per pixel tilechecks onbetrouwbaar en moet dus worden vermeden
  ld a,(mariospeed)
  cp 3
  jp nc,.skippixelcheck


  ld a,(mariox)
  and %00000111
  ld b,a
  ld a,8
  sub b
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  
  cp 4
  jp nc,.resetcarry
  
.skippixelcheck:

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret

.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret
  


.resetcarry:

  xor a
  ret

.fixmariospeed:

  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret



;links van mario
checktileleftmarioram:

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,3;3 ;look above mario's head
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in de

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8

  dec de ;meer naar links kijken
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt

;TODO: de hele tilemap is weer opnieuw in de war gegooid dus deze excludes kloppen niet helemaal

  call nosheercheck
  jp z,.skip241check


  inc hl

  ld a,246 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  call c,excludesheertile
  
  dec hl  


.skip241check:
  
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  call c,excludesheertile
;
;  dec hl ;hl goed zetten voor volgende tilecheck


  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

;  ld a,191 ;192 - 203 en op uitsluiten
;  cp (hl)
;  jp c,.resetcarry
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  jp c,.resetcarry
;
;  dec hl

;we checken de tile way ahead en als die er is dan gaan we de mariospeed aanpassen zodat we geen clipping
;bugs krijgen
  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

  ld a,(mariox)
  and %00000111
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  cp 4
  jp nc,.resetcarry ;afstand nog niet dichtbij genoeg van de mariox is dus geen carry teruggeven

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret

;TODO: deze jump is overbodig geworden alles gewoon naar resetcarry routen  
.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret

.resetcarry:

  xor a
  ret

.fixmariospeed:

;mario gaat harder dan 3? dan moeten we alles negeren en een carry bit terug geven zodat mario direct lockt.
  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret



;een agressiever tilecheck  
checktilerightmariohram:

;nog een keer checken om jumphangs te voorkomen!
  call checktilebelowmario
  jp c,.resetcarry


  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8
  inc de ;meer naar links kijken
;  inc de
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt

  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

;  ld a,191 ;192 - 203 en op uitsluiten
;  cp (hl)
;  jp c,.resetcarry
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  jp c,.resetcarry
;
;  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

;een te hoge mariospeed maakt de per pixel tilechecks onbetrouwbaar en moet dus worden vermeden
  ld a,(mariospeed)
  cp 3
  jp nc,.skippixelcheck


  ld a,(mariox)
  and %00000111
  ld b,a
  ld a,8
  sub b
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  
  cp 4
  jp nc,.resetcarry
  
.skippixelcheck:

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret

.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret
  


.resetcarry:

  xor a
  ret

.fixmariospeed:

  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret


;aggressive naar links
checktileleftmariohram:


;nog een keer checken om jumphangs te voorkomen!
  call checktilebelowmarioram
  jp c,.resetcarry

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;1 tile lager, look above mario's head ;was vier
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in de

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8

  dec de ;meer naar links kijken
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt

  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

;  ld a,191 ;192 - 203 en op uitsluiten
;  cp (hl)
;  jp c,.resetcarry
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  jp c,.resetcarry
;
;  dec hl


  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

  ld a,(mariox)
  and %00000111
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  cp 4
  jp nc,.resetcarry

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret
  
.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret

.resetcarry:

  xor a
  ret

.fixmariospeed:

  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret

  
;naar links voor de grote mario
checktileleftmariolram:

  ld a,(mariostate)
  and a
  ret z


  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,1;3 ;look above mario's head
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in de

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8

  dec de ;meer naar links kijken
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt

;TODO: de hele tilemap is weer opnieuw in de war gegooid dus deze excludes kloppen niet helemaal

  call nosheercheck
  jp z,.skip241check


  inc hl

  ld a,246 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  call c,excludesheertile
  
  dec hl  


.skip241check:
 
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  call c,excludesheertile
;
;  dec hl ;hl goed zetten voor volgende tilecheck


  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

;  ld a,191 ;192 - 203 en op uitsluiten
;  cp (hl)
;  jp c,.resetcarry
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  jp c,.resetcarry
;
;  dec hl

;we checken de tile way ahead en als die er is dan gaan we de mariospeed aanpassen zodat we geen clipping
;bugs krijgen
  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

  ld a,(mariox)
  and %00000111
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  cp 4
  jp nc,.resetcarry ;afstand nog niet dichtbij genoeg van de mariox is dus geen carry teruggeven

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret

;TODO: deze jump is overbodig geworden alles gewoon naar resetcarry routen  
.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret

.resetcarry:

  xor a
  ret

.fixmariospeed:

;mario gaat harder dan 3? dan moeten we alles negeren en een carry bit terug geven zodat mario direct lockt.
  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret



;speciale check voor de grote mario
checktilerightmariolram:


  ld a,(mariostate)
  and a
  ret z ;mario is klein dus gewoon niet doen


  ld hl,(marioy) ;16 bits
  
;ld de,7
;add hl,de


  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,1 ;1 tile lager, look above mario's head
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de


;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8
  inc de ;meer naar links kijken
;  inc de


  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat

  call nosheercheck
  jp z,.skip241check



  ld a,241 ;242 - 250 is schuin dus moeten we een speciale lock doen ik denk dat ik hier een variabele voor neem
  cp (hl)
  call c,excludesheertile
    
    
.skip241check:  
    
;  inc  hl  ;also check the next tile
;  cp  (hl) ;kijk of de huidige tile groter is dan 191
;  call c,excludesheertile

;tja zo kunnen we nooit in dat dode hoekje belanden


;  dec hl ;hl goed zetten voor volgende tilecheck



;ook deze uitsluiten zodat je geen hobbelige bewegingen krijgt wanneer je door een softile springt
  ld a,233 ;234 en op uitsluiten
  cp (hl)
  jp c,.excludesofttile
  inc  hl  ;also check the next tile
  cp  (hl) ;kijk of de huidige tile groter is dan 191
  jp c,.excludesofttile

  dec hl ;hl goed zetten voor volgende tilecheck

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  call c,.fixmariospeed

;een te hoge mariospeed maakt de per pixel tilechecks onbetrouwbaar en moet dus worden vermeden
  ld a,(mariospeed)
  cp 3
  jp nc,.skippixelcheck


  ld a,(mariox)
  and %00000111
  ld b,a
  ld a,8
  sub b
  ;nu weten we de afstand van mario ten opzichte van de muur in pixels
  
  cp 4
  jp nc,.resetcarry
  
.skippixelcheck:

  dec hl

  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret   c   ;foreground tile found, then return
  inc   hl  ;also check the next tile
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret

.excludesofttile:
    xor a ;carry resetten want je mag er immers doorheen
  
  ret
  


.resetcarry:

  xor a
  ret

.fixmariospeed:

  ld a,(mariospeed)
  cp 4
  ret c

  
  ld a,1
  ld (mariospeed),a
  ret


  
lockmariotile: 

;de routine is opgevraagd 
  ld a,1
  ld (tilelocked),a    
 
  ld a,(mariosheertile) ;links of rechts?
  cp 0
  jp z,.nosheerlock ;op deze manier is het sneller want je doet twee conditie checks niet
;  jp .nosheerlock
  cp 1
  jp z,.left
  cp 2
  jp z,.right


.left:


;call bkggreen

  ld hl,(mariox)
  ld a,l
  and %00000111 ;we need the last bits
  ld b,a
  ld a,7 ;maar waarom werkt het wel als dit 7 is??!!
  sub b
  ld b,a ;wegschrijven
  
  ld hl,(marioy) ;y inlezen
  ld a,l
  and %11111000 ;bitjes weghalen
  add b ;resultaat erbij
  ld l,a
  ld (marioy),hl


 ret

.right:

;call bkgblue

  ld hl,(mariox)
  ld a,l
  and %00000111 ;we need the last bits
  ld b,a ;wegschrijven
  
  ld hl,(marioy) ;y inlezen
  ld a,l
  and %11111000 ;bitjes weghalen
  add b ;resultaat erbij
  ld l,a
  ld (marioy),hl

  ret

;standaard lock op rechte tile 
.nosheerlock:  
 
 
;call bkgpurple 
; ret
;  call checktilebelowmario4 ;we checken een tile ahead en als die schuin is gaan we niet locken
;  ret nc
;   call checktilebelowmario5
;  ret nc
   
  ld a,(marioy)
  and %11111000
  ld (marioy),a
 
 
  
  ret


;ik merk dat er ook een kleine x offset is
lockmariotilex:

;nog netter als we niet kunnen rennen of lopen terwijl we tegen een muur staan

  xor a
  ld (mariospeed),a
  ld (marioslowdownpointer),a

  call mariophysics.terminaterunning

  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  add 2 ;twee erbij
  ld (mariox),a
  ret

lockmariotilerightx:

;nog netter als we niet kunnen rennen of lopen terwijl we tegen een muur staan
 

  xor a
  ld (mariospeed),a
  ld (marioslowdownpointer),a

  call mariophysics.terminaterunning


  ld a,(mariox)
  and %11111000 ;laatste 7 weghalen
  xor %00000111 ;laatste 7 erbij
  sub 2 ;twee eraf 
  ld (mariox),a
  ret




checktilebelowmarioaheadram:


  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,4 ;4 tiles lager, look below mario's feet
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
  
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat


  call nosheercheck
  ret z



  ld    a,241
  cp    (hl) ;kijk of te huidige tile groter is dan 191
  ret
  
;  inc   hl  ;also check the next tile
;  cp    (hl) ;kijk of te huidige tile groter is dan 191
 ; ret  


;--------------> in b, vertical tilecheck increase. de, horizontal increase out tilenumber in a and carry flag if foreground found
checkgeneraltilefromhrom:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checkgeneraltile
  
;  ld    a,191
;  cp    (hl) ;kijk of te huidige tile groter is dan 191   
  
  ld a,(hl) ;store the info in a
;  push af
  
  call setpage12tohandleobjectmovement

;  pop af
  
  ret

;--------------> in b, vertical tilecheck increase. de, horizontal increase out tilenumber in a and carry flag if foreground found
checkgeneraltilefromrom:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call checkgeneraltile
  
  ld    a,191
  cp    (hl) ;kijk of te huidige tile groter is dan 191   
  
  ld a,(hl) ;store the info in a
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock
  call  block12

  pop af
  
  ret



checkgeneraltile:   ;in: b=tiles below mario, de=tiles right of mario, out: hl->tile, changes: af,bc,de,hl
  push  de

  ld hl,(marioy) ;16 bits

  srl h ;nu moet je hl delen door 8
  rr l   ;/2
  srl h
  rr l ;/4
  srl h
  rr l ;/8    ;hl=marioy/8 , we gaan er hiervan uit dat marioy/8 nooit groter is dan 256, dus hebben de h niet meer nodig, alleen l

  ld  a,l ;a is dus nu marioy/8
  add a,b
  
;  ld hl,bgrmapaddr ;(das $4000)
;  ld de,(maplenght) ;das 638 voor de map van level00
;  or a
;  jr z,.yset ;indien marioy/8 gelijk is aan nul dan mag de volgende stap overgeslagen worden
;  
;  ld b,a
;.searchmapy:
;  add hl,de ;blijf net zolang de maplenght toevoegen aan het beginadres($4000) van de map totdat we de verticale positie hebben gevonden waar mario zich nu bevindt
; djnz  .searchmapy
;.yset:

;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld d,0
  ld e,a
    
  ld hl,presearchytable
  ;tis een word dus 2 x doen
  sla e
  rl  d
  add hl,de
  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de

;setmapwidthx:
  ld    de,(mariox) ;hl is in gebruik, dus stoppen we mariox in DE

  srl d
  rr e   ;/2
  srl d
  rr e ;/4
  srl d
  rr e ;/8    ;de= mariox/8  
 
  add hl,de ;hl wijst nu naar de tile in de map waar mario op staat
  pop de
  add hl,de
  ret

;we checken hier rechts en links of er blocks zijn die kapot kunnen. Zo ja dan destroyen we die gewoon
;we hoeven geen carry terug te geven alleen de destruction af te handelen
checkdestructionblock:

  ld l,(ix+yobject)
  ld h,(ix+yobject+1)
  ld de,16+16 ;tighten it up some more because somehow chuck can still cause illegal ramwrites
  add hl,de
  ex de,hl
  ld hl,(mapheightx8)
  xor a
  sbc hl,de
  ret c ;DO NEVER CHECK FOR BLOCKS BELOW THE MAP OR ELSE THE ENTIRE ENGINE FUCKS UP!!!
  
;map tijdelijk terughalen
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  ld    a,(ix+v1)               ;v1 is movement speed and direction
  or    a
  jp    p,.movingright          ;check if sprite moves left or right
  
.movingleft:
  ;check collision with wall left
  ld    b,0
  ld    c,(ix+sizeY)            ;vertical increase to tilecheck
;  dec bc
  ld    de,-16                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 201
  jp z,.destroyblockleft
  cp 203
  jp z,.destroyblockleft2
  ld de,(maplenght)
  xor a
  sbc hl,de
  sbc hl,de
  ld a,(hl)
  cp 201
  jp z,.destroyblockleft
  cp 203
  jp z,.destroyblockleft2


  
    call setpage12tohandleobjectmovement
  xor a


  ret


.movingright:
  ;check collision with wall right
  ld    b,0
  ld c,(ix+sizeY)
;  dec bc
  ld    de,016                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 200
  jp z,.destroyblockright
  cp 202
  jp z,.destroyblockright2
  ld de,(maplenght)
  xor a
  sbc hl,de
  sbc hl,de
  ld a,(hl)
  cp 200
  jp z,.destroyblockright
  cp 202
  jp z,.destroyblockright2


    call setpage12tohandleobjectmovement

  xor a

  ret

.destroyblockleft:

  dec hl

  push ix
  call crushblock
  pop ix


    call setpage12tohandleobjectmovement
    
    scf
    
  ret

.destroyblockright:

  push ix
  call crushblock
  pop ix

    call setpage12tohandleobjectmovement

  scf

  ret

.destroyblockleft2:

  dec hl
  ld de,(maplenght)
  xor a
  sbc hl,de

  push ix
  call crushblock
  pop ix


    call setpage12tohandleobjectmovement
    scf
    
  ret

.destroyblockright2:

  ld de,(maplenght)
  xor a
  sbc hl,de

  push ix
  call crushblock
  pop ix

    call setpage12tohandleobjectmovement

  scf

  ret

yoshitonguetilecheck:
  
  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

  call objecttilecheck
  
  push af
  
  ld    a,(slot.page12rom)                             ;all RAM except page 12
  out   ($a8),a        
  ld    a,romprogblock2
  call  block12 

  pop af
  
  ret

;====================================================[objecttilechecks]==============================================
objecttilecheck:                ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), 
;out:c=foreground found 

  ;set map address x
  ld    l,(ix+xobject)          ;x value of sprite (16 bits)
  ld    h,(ix+xobject+1)        ;x value of sprite (16 bits)
  add   hl,de                   ;add horizontal increase to object�s x


  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8
  ;/set map address x

  push  hl                      ;horizontal increase/8

  ;set map address y
  ld    l,(ix+yobject)          ;y value of sprite (16 bits)
  ld    h,(ix+yobject+1)        ;y value of sprite (16 bits)
  add   hl,bc                   ;add vertical increase to object�s y

  srl   h
  rr    l                       ;/2
  srl   h
  rr    l                       ;/4
  srl   h
  rr    l                       ;/8

;  ld    a,l ;height to check in tiles
  ld a,(mapheight)
  dec a
  cp l
  jp c,.returnnul

.temphack:  
;we halen de ywaarde direct uit een precalculated tabel dat betekend dat we de map iets langer moeten laden
;maar de ingame framerate gaat vet hard omhoog! 

;----> tabel pointer setten
  ld h,0
  add hl,hl
  ld de,presearchytable
  add hl,de

  ld e,(hl)
  inc hl
  ld d,(hl) 
  
  ld    hl,bgrmapaddr
  add hl,de
  
  pop   de                      ;horizontal increase/8

  
  add   hl,de                   ;add vertical map position to horizontal map position
  ld    a,(hl)


;back to rom
 
  ret

.returnnul:

  ld a,(currentlevel) ;temp hack until signed byte mystery is resolved
  cp 120
  jp z,.temphack

  pop hl
  ld hl,fakemap+5 ;return 0 0 0 

  xor a
  ret

fakemap:  dw 0,0,0,0,0,0,0,0,0,0,0


;stel maplenght is een getal wat we van te voren uitrekenen. We hoeven dan alleen aan de hand van een tabel
;direct de waarde in DE te laden en vervolgens toe te voegen aan de bkgaddres
;presearchytable: dw 0,0,0,0,0,0,0,0,0,0
;                 dw 0,0,0,0,0,0,0,0,0,0
;                 dw 0,0,0,0,0,0,0,0,0,0
;                 dw 0,0,0,0,0,0,0,0,0,0
;                 dw 0,0,0,0,0,0,0,0,0,0
;                 dw 0,0,0,0,0,0,0,0,0,0
;moved to page3variables

lockobjectx:
.left:
    ld a,(ix+xobject)
    and %11111000
    ld (ix+xobject),a  

    ld de,8       ;add 8 (16 bits)
    ld h,(ix+xobject+1)
    ld l,(ix+xobject)
    add hl,de
    ld (ix+xobject+1),h
    ld (ix+xobject),l
    ret
       
.right:
    
    
    ld a,(ix+xobject)
    and %11111000
    ld (ix+xobject),a
    
    
    ld de,2       ;add 8 (16 bits)
    ld h,(ix+xobject+1)
    ld l,(ix+xobject)
    xor a
    sbc hl,de
    ld (ix+xobject+1),h
    ld (ix+xobject),l
    
      
    ret



;routine om onze object te locken op de y tile

lockyobject:


;jp .forcelock

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;niet locken als er een muurtje ahead is
  ld    b,0
  ld    c,(ix+sizeY)            ;vertical increase to tilecheck
  ld    de,-08                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192
  call c,setpage12tohandleobjectmovement
  ret c
  inc hl
  inc hl
  inc hl
  inc hl
  ld a,(hl)
  cp 192
  call c,setpage12tohandleobjectmovement
  ret c
 
  
;  ld    b,0
;  ld    c,(ix+sizeY)            ;vertical increase to tilecheck
;  ld    de,07                  ;horizontal increase to tilecheck
;  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
;  cp    192
;  ret c
  
.forcelock:

  ld a,(ix+standingonspr)
  and a
  jp nz,objectspritelock


  ld  a,(ix+yobject)      ;first we sub 8, then and %1111111111111100 then add 7
  and %1111'1000
  ld (ix+yobject),a

    ld de,-1        ;add -1 (16 bits)
    ld h,(ix+yobject+1)
    ld l,(ix+yobject)
    add hl,de
    ld (ix+yobject+1),h
    ld (ix+yobject),l

  ret


locksheertile:

.left:
;ret   
  ld a,(ix+xobject)
  and %00000111
  ld b,a

  ld a,(ix+yobject)
  and %11111000
  add 7
  sub b
  ld (ix+yobject),a 
  ret
  

.right:

  ld a,(ix+xobject)
  and %00000111
  ld b,a

  ld a,(ix+yobject)
  and %11111000
  add b
  ld (ix+yobject),a 
  ret




;================================================[special checktiles]================================================
;dit zijn de routines die voor sommige specifieke doeleinden worden aangeroepen uit de rom


pattern11rammaproutineleft:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-2  ;dit werkt alleen goed als die zo staat                 ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
    call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.ignoreleft ;softtiles
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.nextframespecialleft

  
  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,008                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.ignoreleft
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.nextframespecialleft


      call setpage12tohandleobjectmovement

  jp MovementPattern11.ignoreleft


pattern11rammaproutineright:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                  ;vertical increase to tilecheck
  ld    de,000                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.nextframespecialright

      ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-09                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern11.nextframespecialright


      call setpage12tohandleobjectmovement

  jp MovementPattern11.ignoreright


pattern82rammaproutineleft:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-2  ;dit werkt alleen goed als die zo staat                 ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
    call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.ignoreleft ;softtiles
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.nextframespecialleft

  
  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,008                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.ignoreleft
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.nextframespecialleft


      call setpage12tohandleobjectmovement

  jp MovementPattern82.ignoreleft


pattern82rammaproutineright:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                  ;vertical increase to tilecheck
  ld    de,000                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.nextframespecialright

      ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-09                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern82.nextframespecialright


      call setpage12tohandleobjectmovement

  jp MovementPattern82.ignoreright



pattern94rammaproutineleft:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-2  ;dit werkt alleen goed als die zo staat                 ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
    call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.ignoreleft ;softtiles
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.nextframespecialleft

  
  ld    bc,16                   ;vertical increase to tilecheck
  ld    de,008                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.kickupsheertileleft
  cp 234
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.ignoreleft
  cp    192
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.nextframespecialleft


      call setpage12tohandleobjectmovement

  jp MovementPattern94.ignoreleft


pattern94rammaproutineright:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,16                  ;vertical increase to tilecheck
  ld    de,000                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.nextframespecialright

      ld    bc,16                   ;vertical increase to tilecheck
  ld    de,-09                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.kickupsheertileright
  cp 234
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.ignoreright ;softtiles
  cp    192
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern94.nextframespecialright


      call setpage12tohandleobjectmovement

  jp MovementPattern94.ignoreright



checkforsolid:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


  ld    bc,0                   ;vertical increase to tilecheck
  ld    de,0                  ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.solidfound
  


      call setpage12tohandleobjectmovement
  
    xor a
  ret

.solidfound:

      call setpage12tohandleobjectmovement
  
    scf
      
  ret






checktilesmovpattern81:

  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a

;schuine tiles afvangen bij het vallen

  ld bc,16+8
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
      call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
  ld de,maplenght
 
 
  ld bc,0+8
  ld    de,6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
 
 
  ld bc,16
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
 
  ld bc,0
  ld    de,6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
;/schuine tiles afvangen bij het vallen 
 
 
 
  ld    b,0
  ld    c,(ix+sizeY)
  ld    de,-7                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.nextframe
  inc  hl
  inc  hl
  ld a,(hl)
  cp 247
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefoundright
  cp 242
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern81.nextframe

    call setpage12tohandleobjectmovement


  jp MovementPattern81.returnfrompage3



checktilesmovpattern88:


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a 
 
 
  ld    b,0
  ld    c,(ix+sizeY)
  ld    de,-7                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern88.nextframe
  inc  hl
  inc  hl
  ld a,(hl)
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
        call nc,setpage12tohandleobjectmovement
  jp nc,MovementPattern88.nextframe

    call setpage12tohandleobjectmovement


  jp MovementPattern88.returnfrompage3

;something is being overwritten from this point on


checktilesmovpattern13:

  ld a,(ix+standingonspr)
  and a
  jp nz,MovementPattern13.nextframe


  ld		a,(slot.ram)	;back to full RAM
	out		($a8),a


;schuine tiles afvangen bij het vallen

  ld bc,16+8
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.sheertilefoundright
  cp 242
  jp nc,.sheertilefound
  ld de,maplenght
 
 
  ld bc,0+8
  ld    de,6                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.sheertilefoundright
  cp 242
  jp nc,.sheertilefound



 
 
  ld bc,16
  ld    de,0                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.sheertilefoundright
  cp 242
  jp nc,.sheertilefound
 
  ld bc,0
  ld    de,6                ;horizontal increase to tilecheck          
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.sheertilefoundright
  cp 242
  jp nc,.sheertilefound
;/schuine tiles afvangen bij het vallen  
 

 
  ld    b,0
  ld    c,(ix+sizeY)
  ld    de,-7                ;horizontal increase to tilecheck
  call  objecttilecheck         ;in bc:vertical increase (pixel precise), de:horizontal increase (pixel precise), out:c=foreground found
  cp 247
  jp nc,.sheertilefoundright
  cp 242
  jp nc,.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.nextframe
  inc  hl
  inc  hl
  ld a,(hl)
  cp 247
        jp nc,.sheertilefoundright 
  cp 242 
        jp nc,.sheertilefound
  cp    192                     ;check for tilenumber 242-246 (all diagonal tiles going down)
  jp nc,.nextframe




    call setpage12tohandleobjectmovement
  jp MovementPattern13.returnfrompage3



.nextframe:


    call setpage12tohandleobjectmovement
  jp MovementPattern13.nextframe


.sheertilefound:

    call setpage12tohandleobjectmovement
  jp MovementPattern13.sheertilefound


.sheertilefoundright:

    call setpage12tohandleobjectmovement
  jp MovementPattern13.sheertilefoundright






;================================================[Alle tilecheck routines eindigen hier]=============================





