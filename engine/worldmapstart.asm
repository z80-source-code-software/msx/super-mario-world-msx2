

setworldmapstart:


  ;  jp vanilladome
  ;  jp starw
  ;  jp msxworld ;debug stuff
  ;  jp msxworldspecial
  
    ld hl,levelavailabilitylist+8
    ld a,(hl)
    bit 1,a
    jp nz,bowsersvalley
    dec hl ;7
    ld a,(hl)
    bit 5,a
    jp nz,chocolateisland
    dec hl
    dec hl ;5
    ld a,(hl)
    bit 1,a
    jp nz,forestofillusion
    dec hl
    dec hl
    dec hl ;2
    ld a,(hl)
    bit 2,a
    jp nz,vanilladome
    dec hl
    dec hl
    ld a,(hl)
    bit 0,a
    jp nz,donutplains
    



yoshishouse:
;from start of game walking to yoshis house
  ld    hl,112 -8
  ld    (WMmariox),hl
  ld    hl,164
  ld    (WMmarioy),hl
  ld    a,1                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationYosHou
  ld    (CurrentLocation),hl
  ld    a,1
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  
  
vanilladome:  
;vanilla dome 1
  ld    hl,104 -8
  ld    (WMmariox),hl
  ld    hl,128 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationVaDom1
  ld    (CurrentLocation),hl  
  ld    a,3
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3  
  

  ret
  
forestofillusion:
;forest of illusion
  ld    hl,18*8 -8
  ld    (WMmariox),hl
  ld    hl,00*8 + 24
  ld    (WMmarioy),hl
  ld    a,8                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LudwCatoForIl1
  ld    (CurrentLocation),hl
  ld    a,5
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion

  ret
  
donutplains:
;donut plains 1
  ld    hl,9*8 -8
  ld    (WMmariox),hl
  ld    hl,33*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationDoPla1
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  
cheesebridge:  
;vanilla secret 2
  ld    hl,23*8 -8
  ld    (WMmariox),hl
  ld    hl,5*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationVaSec2
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  
cookiemountain:  
;cheese bridge area
  ld    hl,39*8 -8
  ld    (WMmariox),hl
  ld    hl,9*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationCheese
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  
chocolateisland:  
;chocolate island
  ld    hl,47*8 -8
  ld    (WMmariox),hl
  ld    hl,43*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationChoIs1
  ld    (CurrentLocation),hl  
  ld    a,2
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  
bowsersvalley:  
;bowser valley 
  ld    hl,24*8 -8
  ld    (WMmariox),hl
  ld    hl,15*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationValBo1
  ld    (CurrentLocation),hl  
  ld    a,7
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

  ret
  ;only for testing purposes
  

;star world

starw:
  ld    hl,5*8-8
  ld    (WMmariox),hl
  ld    hl,17*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa
  ld    (automoveatstartworldmap?),a    
  ld    hl,LocationStarA2
  ld    (CurrentLocation),hl
  ld    a,8
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3       
  
  ret


  
msxworld:
;forest of illusion
  ld    hl,21*8 -8
  ld    (WMmariox),hl
  ld    hl,8*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationStarF1
  ld    (CurrentLocation),hl
  ld    a,9
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion

  ret  

msxworldspecial:
;forest of illusion
  ld    hl,26*8 -8
  ld    (WMmariox),hl
  ld    hl,3*8 + 24
  ld    (WMmarioy),hl
  ld    a,0                 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa, 6=to VaDom1
  ld    (automoveatstartworldmap?),a
  ld    hl,LocationSpeci3
  ld    (CurrentLocation),hl
  ld    a,9
  ld    (worldmapworld),a   ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3, 5 = forest of illusion

  ret   
  
  
  /*  
;tube K2 
 ld hl,26*8 -8 
 ld (WMmariox),hl 
 ld hl,19*8 + 24 
 ld (WMmarioy),hl 
 ld a,0 ;1=to YosHou, 2=to YelSwP, 3= to YoIsl1, 4=to DoPla1, 5=to IggyCa 
 ld (automoveatstartworldmap?),a 
 ld hl,LocationTubeK2 
 ld (CurrentLocation),hl 
 ld a,7 
 ld (worldmapworld),a ;1 = world1, 2 = world 2 (big worldmap), 3 = world 3

*/
