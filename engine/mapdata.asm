
mapromdata:

;spritehider = 2? this is a special condition in where the spritehider does get copied to the ram but only becomes
;active on routine request. Exit tube right in or out.

;what about the noblocks byte? it is parsed into the (noblock) global variable and read by the blockmaker
;if set to 1 the yellowblocks are the main blocks for that map
;if set to 2 the greenblock are the main blocks for that map
;if set to 3 the redblocks are the main blocks for that map
;if set to 4 the blueblocks are the main blocks for that map
;TIME is depracated. We will use a different way of setting it

;mapinfolist
;   level, nosheer, noblocks, mapwidth,mapheight,  mainid,        mapblock,     disallowyoshi/spritehider,     adresses?,       ? ,              ?
  db    00,   1,        0 :   dw 032,     028 :   db 007,      blockbackgrl14,    0,0   : dw backgrl14  : db grpxblckl14 : dw grpx14addr ;yoshis house
  db    01,   1,        1 :   dw 066,     032 :   db 000,      blockbackgrl01,    0,2   : dw backgrl01  : db grpxblckl01 : dw grpx01addr ;yoshi 1 b
  db    02,   0,        1 :   dw 566,     052 :   db 001,      blockbackgrl02,    0,0   : dw backgrl02  : db grpxblckl02 : dw grpx02addr ;yoshi 2 a
  db    03,   1,        1 :   dw 066,     032 :   db 001,      blockbackgrl03,    0,2   : dw backgrl03  : db grpxblckl03 : dw grpx03addr ;yoshi 2 b
  db    04,   1,        1 :   dw 655,     050 :   db 002,      blockbackgrl04,    0,0   : dw backgrl04  : db grpxblckl04 : dw grpx04addr ;yoshi 3 a
  db    05,   0,        1 :   dw 066,     029 :   db 002,      blockbackgrl05,    0,2   : dw backgrl05  : db grpxblckl05 : dw grpx05addr ;yoshi 3 b
  db    06,   1,        1 :   dw 354,     072 :   db 003,      blockbackgrl06,    0,2   : dw backgrl06  : db grpxblckl06 : dw grpx06addr ;yoshi 4 a
  db    07,   1,        1 :   dw 048,     028 :   db 003,      blockbackgrl07,    0,2   : dw backgrl07  : db grpxblckl06 : dw grpx06addr ;yoshi 4 b
  db    08,   0,        1 :   dw 132,     072 :   db 003,      blockbackgrl08,    0,0   : dw backgrl08  : db grpxblckl08 : dw grpx08addr ;yoshi 4 c
  db    09,   0,        1 :   dw 251,     035 :   db 004,      blockbackgrl09,    1,0   : dw backgrl09  : db grpxblckl09 : dw grpx09addr ;iggy a
  db    10,   0,        1 :   dw 118,     033 :   db 004,      blockbackgrl10,    1,0   : dw backgrl10  : db grpxblckl10 : dw grpx10addr ;iggy b
  db    11,   0,        0 :   dw 032,     027 :   db 004,      blockbackgrl11,    1,0   : dw backgrl11  : db grpxblckl11 : dw grpx11addr ;iggy c
  db    12,   1,        1 :   dw 642,     051 :   db 005,      blockbackgrl12,    0,0   : dw backgrl12  : db grpxblckl12 : dw grpx12addr ;donut 1a
  db    13,   1,        0 :   dw 097,     028 :   db 006,      blockbackgrl13,    0,2   : dw backgrl13  : db grpxblckl13 : dw grpx13addr ;yellow switch
  db    14,   1,        0 :   dw 097,     065 :   db 006,      blockbackgrl13,    0,2   : dw backgrl13  : db grpxblckl13 : dw grpx13addr ;yellow switch
  db    15,   0,        1 :   dw 682,     048 :   db 000,      blockbackgrl00,    0,0   : dw backgrl00  : db grpxblckl00 : dw grpx00addr ;yoshi 1 a
  db    16,   1,        1 :   dw 450,     058 :   db 005,      blockbackgrl16,    0,0   : dw backgrl16  : db grpxblckl16 : dw grpx16addr ;donut 1b
  db    17,   1,        0 :   dw 032,     053 :   db 011,      blockbackgrl17,    0,2   : dw backgrl17  : db grpxblckl13 : dw grpx13addr ;tic tac toe
  db    18,   1,        0 :   dw 032,     028 :   db 008,      blockbackgrl18,    0,0   : dw backgrl18  : db grpxblckl18 : dw grpx18addr ;demomap
  db    19,   0,        1 :   dw 382,     051 :   db 009,      blockbackgrl19,    0,2   : dw backgrl19  : db grpxblckl19 : dw grpx19addr ;donut 2a
  db    20,   1,        1 :   dw 132,     051 :   db 009,      blockbackgrl20,    0,2   : dw backgrl20  : db grpxblckl20 : dw grpx20addr ;donut 2b
  db    21,   1,        0 :   dw 105,     051 :   db 009,      blockbackgrl21,    0,0   : dw backgrl21  : db grpxblckl21 : dw grpx21addr ;donut 2c
  db    22,   1,        0 :   dw 225,     061 :   db 010,      blockbackgrl22,    1,0   : dw backgrl22  : db grpxblckl22 : dw grpx22addr ;donut ghost
  db    23,   0,        0 :   dw 083,     061 :   db 010,      blockbackgrl22b,   1,0   : dw backgrl22b : db grpxblckl22b : dw grpx22baddr ;donut ghost
  db    24,   0,        0 :   dw 083,     061 :   db 010,      blockbackgrl22c,   1,0   : dw backgrl22c : db grpxblckl22c : dw grpx22caddr ;donut ghost
  db    25,   1,        0 :   dw 032,     061 :   db 010,      blockbackgrl22d,   1,0   : dw backgrl22d : db grpxblckl22d : dw grpx22daddr ;donut ghost
  db    26,   1,        0 :   dw 066,     037 :   db 010,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;donut ghost exit
  db    27,   1,        0 :   dw 616,     053 :   db 012,      blockbackgrl24,    0,0   : dw backgrl24  : db grpxblckl24 : dw grpx24addr ;donut 3
  db    28,   0,        0 :   dw 610,     051 :   db 013,      blockbackgrl25,    0,0   : dw backgrl25  : db grpxblckl25 : dw grpx25addr ;donut4 a
  db    29,   0,        1 :   dw 100,     050 :   db 200,      blockbackgrl25,    0,2   : dw backgrltm  : db grpxblckltm : dw grpxtmaddr ;testmap
  db    30,   1,        0 :   dw 420,     037 :   db 014,      blockbackgrl26,    0,0   : dw backgrl26  : db grpxblckl26 : dw grpx26addr ;donut secret 1a
  db    31,   0,        0 :   dw 068,     160 :   db 014,      blockbackgrl27,    0,0   : dw backgrl27  : db grpxblckl27 : dw grpx27addr ;donut secret 1b
  db    32,   0,        0 :   dw 500,     054 :   db 015,      blockbackgrl28,    0,0   : dw backgrl28  : db grpxblckl28 : dw grpx28addr ;donut secret 2a 2-07a
  db    33,   0,        0 :   dw 128,     053 :   db 015,      blockbackgrl29,    0,0   : dw backgrl29  : db grpxblckl29 : dw grpx29addr ;submap donut secret 2
  db    34,   0,        0 :   dw 176,     054 :   db 016,      blockbackgrl30,    1,0   : dw backgrl30  : db grpxblckl22 : dw grpx22addr ;donut secret house a
  db    35,   0,        0 :   dw 161,     053 :   db 016,      blockbackgrl31,    1,0   : dw backgrl31  : db grpxblckl22 : dw grpx22addr ;donut secret house b
  db    36,   0,        0 :   dw 095,     038 :   db 016,      blockbackgrl32,    1,0   : dw backgrl32  : db grpxblckl22 : dw grpx22addr ;donut secret house c
  db    37,   0,        0 :   dw 032,     030 :   db 016,      blockbackgrl33,    1,0   : dw backgrl33  : db grpxblckl22 : dw grpx22addr ;donut secret house d
  db    38,   0,        1 :   dw 096,     054 :   db 017,      blockbackgrl34,    1,2   : dw backgrl34  : db grpxblckl30 : dw grpx30addr ;donut castle a
  db    39,   0,        0 :   dw 096,     054 :   db 017,      blockbackgrl35,    1,0   : dw backgrl35  : db grpxblckl30 : dw grpx30addr ;donut castle b
  db    40,   0,        2 :   dw 032,     245 :   db 017,      blockbackgrl36,    1,0   : dw backgrl36  : db grpxblckl31 : dw grpx31addr ;donut castle c
  db    41,   0,        0 :   dw 032,     027 :   db 017,      blockbackgrl37,    1,0   : dw backgrl37  : db grpxblckl32 : dw grpx32addr ;donut castle d
  db    42,   0,        0 :   dw 097,     032 :   db 018,      blockbackgrl38,    0,2   : dw backgrl38  : db grpxblckl13 : dw grpx13addr ;green switch palace
  db    43,   0,        0 :   dw 128,     055 :   db 019,      blockbackgrl39,    0,0   : dw backgrl39  : db grpxblckl25 : dw grpx25addr ;donut plain 4 b
  db    44,   0,        0 :   dw 067,     036 :   db 019,      blockbackgrl40,    0,2   : dw backgrl40  : db grpxblckl01 : dw grpx01addr ;donut plain 4 c
  db    45,   0,        0 :   dw 640,     051 :   db 020,      blockbackgrl41,    0,0   : dw backgrl41  : db grpxblckl33 : dw grpx33addr ;vanilla 04a
  db    46,   1,        0 :   dw 097,     065 :   db 021,      blockbackgrl13,    0,2   : dw backgrl13  : db grpxblckl13 : dw grpx13addr ;green switch palace second part
  db    47,   1,        0 :   dw 032,     028 :   db 022,      blockbacktops,     0,0   : dw backgrlts  : db grpxblckl02 : dw grpx02addr ;top secret area
  db    48,   0,        3 :   dw 298,     056 :   db 023,      blockbackgrl42,    0,2   : dw backgrl42  : db grpxblckl34 : dw grpx34addr ;vanilla 01a
  db    49,   1,        2 :   dw 416,     055 :   db 023,      blockbackgrl43,    0,2   : dw backgrl43  : db grpxblckl35 : dw grpx35addr ;vanilla 01b
  db    50,   1,        0 :   dw 192,     054 :   db 023,      blockbackgrl44,    0,2   : dw backgrl44  : db grpxblckl36 : dw grpx36addr ;vanilla 01c
  db    51,   0,        3 :   dw 575,     054 :   db 024,      blockbackgrl45,    0,2   : dw backgrl45  : db grpxblckl37 : dw grpx37addr ;vanilla 03a
  db    52,   1,        3 :   dw 477,     054 :   db 024,      blockbackgrl48,    0,0   : dw backgrl48  : db grpxblckl38 : dw grpx38addr ;vanilla 03b
  db    53,   0,        1 :   dw 630,     052 :   db 025,      blockbackgrl46,    0,0   : dw backgrl46  : db grpxblckl39 : dw grpx39addr ;vanilla 02a
  db    54,   1,        1 :   dw 128,     054 :   db 025,      blockbackgrl47,    0,0   : dw backgrl47  : db grpxblckl28 : dw grpx28addr ;vanilla 02b
  db    55,   1,        3 :   dw 192,     054 :   db 020,      blockbackgrl51,    0,2   : dw backgrl51  : db grpxblckl42 : dw grpx42addr ;vanilla 04b
  db    56,   0,        1 :   dw 352,     054 :   db 026,      blockbackgrl52,    1,0   : dw backgrl52  : db grpxblckl22 : dw grpx22addr ;vanilla 05a ;ghosthouse
  db    57,   0,        1 :   dw 256,     054 :   db 026,      blockbackgrl53,    1,0   : dw backgrl53  : db grpxblckl22 : dw grpx22addr ;vanilla 05b
  db    58,   0,        4 :   dw 064,     224 :   db 027,      blockbackgrl54,    0,2   : dw backgrl54  : db grpxblckl43 : dw grpx43addr ;vanilla 06a
  db    59,   0,        4 :   dw 096,     054 :   db 027,      blockbackgrl55,    0,2   : dw backgrl55  : db grpxblckl43 : dw grpx43addr ;vanilla 06b
  db    60,   0,        4 :   dw 096,     054 :   db 027,      blockbackgrl56,    0,2   : dw backgrl56  : db grpxblckl43 : dw grpx43addr ;vanilla 06c
  db    61,   0,        0 :   dw 128,     054 :   db 024,      blockbackgrl61,    0,0   : dw backgrl61  : db grpxblckl28 : dw grpx28addr ;vanilla 03c
  db    62,   0,        1 :   dw 620,     052 :   db 028,      blockbackgrl62,    0,0   : dw backgrl62  : db grpxblckl62 : dw grpx62addr ;vanilla 07a
  db    63,   0,        0 :   dw 160,     044 :   db 028,      blockbackgrl63,    0,0   : dw backgrl63  : db grpxblckl42 : dw grpx42addr ;vanilla 07b
  db    64,   1,        0 :   dw 512,     054 :   db 029,      blockbackgrl64,    0,2   : dw backgrl64  : db grpxblckl64 : dw grpx64addr ;vanilla 08a
  db    65,   1,        0 :   dw 064,     054 :   db 029,      blockbackgrl65,    0,2   : dw backgrl65  : db grpxblckl64 : dw grpx64addr ;vanilla 08b
;bug  
  db    66,   1,        0 :   dw 256,     030 :   db 030,      blockbackgrl66,    1,2   : dw backgrl66  : db grpxblckl66 : dw grpx66addr ;vanilla fortress a
  db    67,   1,        0 :   dw 256,     054 :   db 030,      blockbackgrl67,    1,0   : dw backgrl67  : db grpxblckl67 : dw grpx67addr ;vanilla fortress b
  db    68,   0,        0 :   dw 032,     027 :   db 030,      blockbackgrl68,    1,0   : dw backgrl68  : db grpxblckl68 : dw grpx68addr ;vanilla fortress c (reznor)
  db    69,   1,        0 :   dw 315,     052 :   db 031,      blockbackgrl69,    1,0   : dw backgrl69  : db grpxblckl30 : dw grpx30addr ;lemmys castle a
  db    70,   1,        0 :   dw 384,     038 :   db 031,      blockbackgrl70,    1,0   : dw backgrl70  : db grpxblckl31 : dw grpx31addr ;lemmys castle c
  db    71,   1,        0 :   dw 064,     034 :   db 031,      blockbackgrl71,    1,0   : dw backgrl71  : db grpxblckl30 : dw grpx30addr ;lemmys castle b
  db    72,   1,        0 :   dw 032,     032 :   db 031,      blockbackgrl72,    1,0   : dw backgrl72  : db grpxblckl72 : dw grpx72addr ;lemmys castle d
  db    73,   1,        3 :   dw 409,     054 :   db 032,      blockbackgrl73,    0,0   : dw backgrl73  : db grpxblckl73 : dw grpx73addr ;butter bridge 1a
  db    74,   1,        0 :   dw 064,     054 :   db 032,      blockbackgrl74,    0,2   : dw backgrl74  : db grpxblckl73 : dw grpx73addr ;butter bridge 1b
;bug  
  db    75,   1,        2 :   dw 640,     050 :   db 033,      blockbackgrl75,    0,0   : dw backgrl75  : db grpxblckl75 : dw grpx75addr ;butter bridge 2a
  db    76,   1,        0 :   dw 128,     034 :   db 033,      blockbackgrl76,    0,0   : dw backgrl76  : db grpxblckl75 : dw grpx75addr ;butter bridge 2b
  db    77,   1,        0 :   dw 704,     046 :   db 034,      blockbackgrl77,    0,0   : dw backgrl77  : db grpxblckl77 : dw grpx77addr ;cheesebridge 3a
  db    78,   1,        0 :   dw 192,     054 :   db 034,      blockbackgrl78,    0,0   : dw backgrl78  : db grpxblckl78 : dw grpx78addr ;cheesebridge 3b
  db    79,   1,        0 :   dw 128,     054 :   db 034,      blockbackgrl79,    0,0   : dw backgrl79  : db grpxblckl77 : dw grpx77addr ;cheesebridge 3c
  db    80,   0,        3 :   dw 672,     048 :   db 035,      blockbackgrl80,    0,0   : dw backgrl80  : db grpxblckl80 : dw grpx80addr ;cookiemountain 4a
  db    81,   1,        0 :   dw 167,     052 :   db 035,      blockbackgrl81,    0,0   : dw backgrl81  : db grpxblckl81 : dw grpx81addr ;cookiemountain 4b
  db    82,   0,        0 :   dw 384,     054 :   db 036,      blockbackgrl82,    0,2   : dw backgrl82  : db grpxblckl82 : dw grpx82addr ;sodalake 5a
  db    83,   1,        0 :   dw 064,     054 :   db 036,      blockbackgrl83,    0,0   : dw backgrl83  : db grpxblckl25 : dw grpx25addr ;sodalake 5b
  db    84,   1,        0 :   dw 192,     054 :   db 037,      blockbackgrl84,    1,0   : dw backgrl84  : db grpxblckl84 : dw grpx84addr ;ludwig 6a
  db    85,   1,        0 :   dw 339,     038 :   db 037,      blockbackgrl85,    1,0   : dw backgrl85  : db grpxblckl84 : dw grpx84addr ;ludwig 6b
  db    86,   1,        0 :   dw 064,     160 :   db 037,      blockbackgrl86,    1,0   : dw backgrl86  : db grpxblckl84 : dw grpx84addr ;ludwig 6c
  db    87,   1,        0 :   dw 048,     027 :   db 037,      blockbackgrl87,    1,0   : dw backgrl87  : db grpxblckl85 : dw grpx85addr ;ludwig 6d (boss)
  db    88,   1,        0 :   dw 639,     032 :   db 038,      blockbackgrl88,    0,1   : dw backgrl88  : db grpxblckl86 : dw grpx86addr ;illusion1
  db    89,   1,        1 :   dw 448,     054 :   db 039,      blockbackgrl89,    0,0   : dw backgrl89  : db grpxblckl87 : dw grpx87addr ;illusion2a
  db    90,   1,        0 :   dw 640,     032 :   db 040,      blockbackgrl90,    0,1   : dw backgrl90  : db grpxblckl88 : dw grpx88addr ;illusion3a
  db    91,   1,        1 :   dw 032,     032 :   db 040,      blockbackgrl91,    0,2   : dw backgrl91  : db grpxblckl03 : dw grpx03addr ;illusion3b
  db    92,   1,        0 :   dw 630,     052 :   db 041,      blockbackgrl92,    0,0   : dw backgrl92  : db grpxblckl89 : dw grpx89addr ;illusion4a
  db    93,   1,        0 :   dw 032,     032 :   db 041,      blockbackgrl93,    0,0   : dw backgrl93  : db grpxblckl03 : dw grpx03addr ;illusion4b
  db    94,   1,        0 :   dw 128,     054 :   db 041,      blockbackgrl94,    0,0   : dw backgrl94  : db grpxblckl82 : dw grpx82addr ;illusion4c
  db    95,   1,        0 :   dw 344,     047 :   db 042,      blockbackgrl95,    1,0   : dw backgrl95  : db grpxblckl22 : dw grpx22addr ;illusion5a ;forest ghost house
  db    96,   1,        0 :   dw 256,     054 :   db 042,      blockbackgrl96,    1,0   : dw backgrl96  : db grpxblckl22 : dw grpx22addr ;illusion5b
  db    97,   1,        1 :   dw 320,     054 :   db 043,      blockbackgrl97,    0,0   : dw backgrl97  : db grpxblckl90 : dw grpx90addr ;illusion6a ;secret level
  db    98,   1,        0 :   dw 192,     054 :   db 044,      blockbackgrl98,    1,0   : dw backgrl98  : db grpxblckl91 : dw grpx91addr ;illusion7a ;forest fortress
  db    99,   1,        1 :   dw 510,     054 :   db 044,      blockbackgrl99,    1,0   : dw backgrl99  : db grpxblckl92 : dw grpx92addr ;illusion7b
  db    100,  1,        0 :   dw 480,     054 :   db 045,      blockbackgrl100,   1,0   : dw backgrl100 : db grpxblckl30 : dw grpx30addr ;illusion8a ; roys castle
  db    101,  0,        1 :   dw 100,     050 :   db 200,      blockbackgrl25,    0,2   : dw backgrltm  : db grpxblckltm : dw grpxtmaddr ;testmap
  db    102,  1,        0 :   dw 097,     028 :   db 046,      blockbackgrl102,   0,2   : dw backgrl102 : db grpxblckl13 : dw grpx13addr ;blue switch palace
  db    103,  1,        0 :   dw 097,     065 :   db 046,      blockbackgrl13,    0,2   : dw backgrl13  : db grpxblckl13 : dw grpx13addr ;blue switch palace part 2
  db    104,  1,        0 :   dw 097,     028 :   db 047,      blockbackgrl104,   0,2   : dw backgrl104 : db grpxblckl13 : dw grpx13addr ;red switch palace
  db    105,  1,        0 :   dw 097,     065 :   db 047,      blockbackgrl13,    0,2   : dw backgrl13  : db grpxblckl13 : dw grpx13addr ;red switch palace part 2
  
  db    106,  0,        3 :   dw 640,     051 :   db 048,      blockbackgrl105,   0,0   : dw backgrl105 : db grpxblckl93 : dw grpx93addr ;chocolate1a
  db    107,  1,        0 :   dw 127,     051 :   db 048,      blockbackgrl106,   0,0   : dw backgrl106 : db grpxblckl94 : dw grpx94addr ;chocolate1b
  
  db    108,  0,        0 :   dw 131,     051 :   db 049,      blockbackgrl107,   0,0   : dw backgrl107 : db grpxblckl95 : dw grpx95addr ;chocolate2a
  db    109,  0,        0 :   dw 224,     055 :   db 049,      blockbackgrl108,   0,0   : dw backgrl108 : db grpxblckl95 : dw grpx95addr ;chocolate2b
  db    110,  0,        0 :   dw 224,     055 :   db 049,      blockbackgrl109,   0,0   : dw backgrl109 : db grpxblckl95 : dw grpx95addr ;chocolate2c
  db    111,  1,        0 :   dw 224,     055 :   db 049,      blockbackgrl110,   0,0   : dw backgrl110 : db grpxblckl95 : dw grpx95addr ;chocolate2d
  db    112,  1,        0 :   dw 128,     055 :   db 049,      blockbackgrl111,   0,0   : dw backgrl111 : db grpxblckl95 : dw grpx95addr ;chocolate2e
  db    113,  0,        0 :   dw 128,     055 :   db 049,      blockbackgrl112,   0,0   : dw backgrl112 : db grpxblckl95 : dw grpx95addr ;chocolate2f
  db    114,  1,        0 :   dw 128,     055 :   db 049,      blockbackgrl113,   0,0   : dw backgrl113 : db grpxblckl95 : dw grpx95addr ;chocolate2g
  db    115,  1,        0 :   dw 128,     055 :   db 049,      blockbackgrl114,   0,0   : dw backgrl114 : db grpxblckl95 : dw grpx95addr ;chocolate2h
  db    116,  1,        3 :   dw 128,     055 :   db 049,      blockbackgrl115,   0,0   : dw backgrl115 : db grpxblckl95 : dw grpx95addr ;chocolate2i
  
  db    117,  0,        2 :   dw 660,     049 :   db 050,      blockbackgrl116,   0,0   : dw backgrl116 : db grpxblckl96 : dw grpx96addr ;chocolate3a
  db    118,  1,        0 :   dw 071,     052 :   db 050,      blockbackgrl117,   0,0   : dw backgrl117 : db grpxblckl62 : dw grpx62addr ;chocolate3b  
  
  db    119,  0,        3 :   dw 480,     055 :   db 051,      blockbackgrl118,   0,0   : dw backgrl118 : db grpxblckl97 : dw grpx97addr ;chocolate4a
  db    120,  1,        3 :   dw 064,     383 :   db 051,      blockbackgrl119,   0,1   : dw backgrl119 : db grpxblckl43 : dw grpx43addr ;chocolate4b
  
  db    121,  1,        0 :   dw 600,     053 :   db 052,      blockbackgrl120,   0,0   : dw backgrl120 : db grpxblckl98 : dw grpx98addr ;chocolate5a
  db    122,  1,        0 :   dw 117,     054 :   db 052,      blockbackgrl121,   0,0   : dw backgrl121 : db grpxblckl99 : dw grpx99addr ;chocolate5b
  
  db    123,  0,        0 :   dw 432,     054 :   db 053,      blockbackgrl122,   1,0   : dw backgrl122 : db grpxblckl101 : dw grpx101addr ;chocolate6a ;ghosthouse
  db    124,  1,        0 :   dw 193,     054 :   db 053,      blockbackgrl123,   1,0   : dw backgrl123 : db grpxblckl22 : dw grpx22addr ;chocolate6b
  
  db    125,  0,        0 :   dw 255,     055 :   db 054,      blockbackgrl124,   0,2   : dw backgrl124 : db grpxblckl37 : dw grpx37addr ;chocolate7a
  db    126,  0,        0 :   dw 064,     319 :   db 054,      blockbackgrl125,   0,0   : dw backgrl125 : db grpxblckl37 : dw grpx37addr ;chocolate7b
  db    127,  0,        0 :   dw 470,     054 :   db 054,      blockbackgrl126,   0,2   : dw backgrl126 : db grpxblckl36 : dw grpx36addr ;chocolate7c
  db    128,  1,        0 :   dw 116,     094 :   db 054,      blockbackgrl127,   0,0   : dw backgrl127 : db grpxblckl04 : dw grpx04addr ;chocolate7d
  db    129,  1,        0 :   dw 096,     054 :   db 054,      blockbackgrl128,   0,0   : dw backgrl128 : db grpxblckl43 : dw grpx43addr ;chocolate7e
  
  db    130,  1,        3 :   dw 265,     032 :   db 055,      blockbackgrl129,   1,0   : dw backgrl129 : db grpxblckl100 : dw grpx100addr ;chocolate8a;
  db    131,  1,        3 :   dw 348,     032 :   db 055,      blockbackgrl130,   1,0   : dw backgrl130 : db grpxblckl100 : dw grpx100addr ;chocolate8b;

  db    132,  1,        3 :   dw 384,     035 :   db 056,      blockbackgrl131,   1,0   : dw backgrl131 : db grpxblckl102 : dw grpx102addr ;chocolate9a;
  db    133,  1,        2 :   dw 256,     042 :   db 056,      blockbackgrl132,   1,0   : dw backgrl132 : db grpxblckl31  : dw grpx31addr ;chocolate9b;  
  db    134,  1,        0 :   dw 032,     032 :   db 056,      blockbackgrl133,   1,0   : dw backgrl133 : db grpxblckl72  : dw grpx72addr ;chocolate9c; (lemmys castle d gfx)

  db    135,  1,        1 :   dw 096,     054 :   db 057,      blockbackgrl134,   1,0   : dw backgrl134 : db grpxblckl103 : dw grpx103addr ;bowserva1a;  
  db    136,  1,        1 :   dw 256,     033 :   db 057,      blockbackgrl135,   1,0   : dw backgrl135 : db grpxblckl104 : dw grpx104addr ;bowserva1b;  
  db    137,  1,        1 :   dw 062,     255 :   db 057,      blockbackgrl136,   1,0   : dw backgrl136 : db grpxblckl39  : dw grpx39addr ;bowserva1c;

  db    138,  0,        1 :   dw 576,     051 :   db 058,      blockbackgrl137,   0,0   : dw backgrl137 : db grpxblckl39  : dw grpx39addr ;bowserva2a; 
  db    186,  1,        0 :   dw 096,     054 :   db 058,      blockbackgrl128,   0,0   : dw backgrl128 : db grpxblckl43 : dw grpx43addr ;bowserva2b  
  
  db    139,  0,        1 :   dw 321,     054 :   db 059,      blockbackgrl138,   0,2   : dw backgrl138 : db grpxblckl36  : dw grpx36addr ;bowserva3a;  
  db    140,  1,        1 :   dw 461,     054 :   db 059,      blockbackgrl139,   0,2   : dw backgrl139 : db grpxblckl36  : dw grpx36addr ;bowserva3b;  
  db    141,  1,        1 :   dw 284,     054 :   db 059,      blockbackgrl140,   0,2   : dw backgrl140 : db grpxblckl36  : dw grpx36addr ;bowserva3c; 
  db    187,  1,        0 :   dw 096,     054 :   db 059,      blockbackgrl128,   0,0   : dw backgrl128 : db grpxblckl43 : dw grpx43addr ;bowserva3d;  
 
  db    142,  1,        0 :   dw 606,     054 :   db 060,      blockbackgrl141,   0,0   : dw backgrl141 : db grpxblckl105 : dw grpx105addr ;bowserva4;  
  
  db    143,  0,        1 :   dw 618,     053 :   db 061,      blockbackgrl142,   0,0   : dw backgrl142 : db grpxblckl106 : dw grpx106addr ;bowserva5a;  
  db    144,  0,        3 :   dw 144,     044 :   db 061,      blockbackgrl143,   0,0   : dw backgrl143 : db grpxblckl28  : dw grpx28addr ;bowserva5b;
  
  db    145,  0,        1 :   dw 158,     053 :   db 062,      blockbackgrl144,   1,0   : dw backgrl144 : db grpxblckl22  : dw grpx22addr ;bowserva6a; ;ghosthouse 
  db    146,  1,        1 :   dw 318,     053 :   db 062,      blockbackgrl145,   1,0   : dw backgrl145 : db grpxblckl22  : dw grpx22addr ;bowserva6b;  
  db    147,  1,        1 :   dw 159,     053 :   db 062,      blockbackgrl146,   1,0   : dw backgrl146 : db grpxblckl22  : dw grpx22addr ;bowserva6c;  

  db    148,  1,        2 :   dw 519,     038 :   db 063,      blockbackgrl147,   1,0   : dw backgrl147 : db grpxblckl147  : dw grpx147addr ;bowserva7a;  
  
  db    149,  1,        1 :   dw 256,     054 :   db 064,      blockbackgrl148,   1,0   : dw backgrl148 : db grpxblckl30  : dw grpx30addr ;bowserva8a;  
  db    150,  0,        1 :   dw 320,     037 :   db 064,      blockbackgrl149,   1,0   : dw backgrl149 : db grpxblckl149  : dw grpx149addr ;bowserva8b;  
  db    151,  0,        0 :   dw 032,     027 :   db 064,      blockbackgrl11,    1,0   : dw backgrl11  : db grpxblckl11 : dw grpx11addr ;bowserva8c
  
  ;cutscenes!
  db    152,  1,        0 :   dw 032,     028 :   db 065,      blockbackgrl160,   0,1   : dw backgrl160 : db grpxblckl160 : dw grpx160addr ;castle1
  db    153,  1,        0 :   dw 032,     028 :   db 065,      blockbackgrl161,   0,1   : dw backgrl161 : db grpxblckl160 : dw grpx160addr ;castle2
  db    154,  1,        0 :   dw 032,     028 :   db 065,      blockbackgrl162,   0,1   : dw backgrl162 : db grpxblckl161 : dw grpx161addr ;ghost
  
  db    155,  1,        0 :   dw 032,     028 :   db 007,      blockbackgrl163,   1,0   : dw backgrl163 : db grpxblckl162 : dw grpx162addr ;intromap
  
  db    156,  1,        1 :   dw 320,     064 :   db 066,      blockbackgrl164,   0,0   : dw backgrl164 : db grpxblckl20 : dw grpx20addr ;starworld1
  db    157,  1,        1 :   dw 384,     054 :   db 067,      blockbackgrl165,   0,2   : dw backgrl165 : db grpxblckl163 : dw grpx163addr ;starworld2
  db    158,  1,        1 :   dw 096,     054 :   db 068,      blockbackgrl166,   0,0   : dw backgrl166 : db grpxblckl165 : dw grpx165addr ;starworld3
  db    159,  1,        1 :   dw 605,     054 :   db 069,      blockbackgrl167,   0,0   : dw backgrl167 : db grpxblckl166 : dw grpx166addr ;starworld4
  db    160,  1,        1 :   dw 553,     054 :   db 070,      blockbackgrl168,   0,0   : dw backgrl168 : db grpxblckl166 : dw grpx166addr ;starworld5
  
  db    161,  1,        0 :   dw 032,     053 :   db 011,      blockbackgrl17,    1,2   : dw backgrl17  : db grpxblckl13 : dw grpx13addr ;tic tac toe (without yoshi!!)
  
  
  db    162,  1,        0 :   dw 196,     033 :   db 071,      blockbackgrl169,   1,0   : dw backgrl169 : db grpxblckl167 : dw grpx167addr ;bowsers castle 1a  
  db    163,  1,        0 :   dw 102,     033 :   db 071,      blockbackgrl170,   1,0   : dw backgrl170 : db grpxblckl167 : dw grpx167addr ;bowsers castle 1b  
  db    164,  1,        0 :   dw 196,     033 :   db 071,      blockbackgrl171,   1,0   : dw backgrl171 : db grpxblckl167 : dw grpx167addr ;bowsers castle 1c  
  db    165,  1,        0 :   dw 129,     033 :   db 071,      blockbackgrl172,   1,0   : dw backgrl172 : db grpxblckl167 : dw grpx167addr ;bowsers castle 1d  
  db    166,  1,        0 :   dw 256,     040 :   db 071,      blockbackgrl173,   1,0   : dw backgrl173 : db grpxblckl169  : dw grpx169addr ;bowsers castle 1e  
  db    167,  1,        0 :   dw 128,     032 :   db 071,      blockbackgrl174,   1,0   : dw backgrl174 : db grpxblckl30  : dw grpx30addr ;bowsers castle 1f  
  db    168,  1,        0 :   dw 128,     054 :   db 071,      blockbackgrl175,   1,0   : dw backgrl175 : db grpxblckl30  : dw grpx30addr ;bowsers castle 1g  
  db    169,  1,        0 :   dw 096,     034 :   db 071,      blockbackgrl176,   1,0   : dw backgrl176 : db grpxblckl30  : dw grpx30addr ;bowsers castle 1h  
  db    170,  1,        0 :   dw 096,     034 :   db 071,      blockbackgrl177,   1,0   : dw backgrl177 : db grpxblckl84  : dw grpx84addr ;bowsers castle 1i  
  db    171,  1,        0 :   dw 160,     032 :   db 071,      blockbackgrl178,   1,2   : dw backgrl178 : db grpxblckl66  : dw grpx66addr ;bowsers castle 1j  
  db    172,  1,        0 :   dw 160,     054 :   db 071,      blockbackgrl179,   1,1   : dw backgrl179 : db grpxblckl168 : dw grpx168addr ;bowsers castle 1k
  db    173,  1,        0 :   dw 032,     032 :   db 071,      blockbackgrl180,   1,0   : dw backgrl180 : db grpxblckl170 : dw grpx170addr ;bowsers castle 1l
  
  db    174,  1,        0 :   dw 352,     054 :   db 072,      blockbackgrl181,   0,0   : dw backgrl181 : db grpxblckl171 : dw grpx171addr ;special1a
  db    175,  1,        0 :   dw 048,     032 :   db 072,      blockbackgrl182,   0,0   : dw backgrl182 : db grpxblckl171 : dw grpx171addr ;special1b
  
  db    176,  0,        0 :   dw 300,     054 :   db 073,      blockbackgrl183,   0,0   : dw backgrl183 : db grpxblckl172 : dw grpx172addr ;special2a
  
  db    177,  0,        0 :   dw 300,     054 :   db 074,      blockbackgrl184,   0,0   : dw backgrl184 : db grpxblckl173 : dw grpx173addr ;special3a
  db    178,  0,        0 :   dw 032,     032 :   db 074,      blockbackgrl185,   0,0   : dw backgrl185 : db grpxblckl173 : dw grpx173addr ;special3b
  
  db    179,  0,        0 :   dw 032,     032 :   db 073,      blockbackgrl186,   0,0   : dw backgrl186 : db grpxblckl173 : dw grpx173addr ;special2b
  
  db    180,   1,        0 :   dw 066,     037 :   db 016,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;donut secret ghost exit  
  db    181,   1,        0 :   dw 066,     037 :   db 026,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;vanilla ghost exit  
  db    182,   1,        0 :   dw 066,     037 :   db 042,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;illusion ghost exit  
  db    183,   1,        0 :   dw 066,     037 :   db 053,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;chocolate ghost exit  
  db    184,   1,        0 :   dw 066,     037 :   db 062,      blockbackgrl23,    1,0   : dw backgrl23  : db grpxblckl23 : dw grpx23addr ;bowsers ghost exit  

  db    185,  1,        0 :   dw 032,     028 :   db 065,      blockbackgrl187,   0,1   : dw backgrl187 : db grpxblckl187 : dw grpx187addr ;castle3 (chocolate valley) 
 
 

;what about the noblocks byte? it is parsed into the (noblock) global variable and read by the blockmaker
;if set to 0 I have no clue.
;if set to 1 the yellowblocks are the main blocks for that map
;if set to 2 the greenblock are the main blocks for that map
;if set to 3 the redblocks are the main blocks for that map
;if set to 4 the blueblocks are the main blocks for that map
;TIME is depracated. We will use a different way of setting it
  
  
;what is mainid? it tells the engine to which main level the map belongs. This is required for complicated maps that
;can be cleared and returned to.
  
mapdata:        equ engaddr
  
.level:         equ 0
.empty1:        equ 1
.empty2:        equ 2
.maplengt:      equ 3
.mapheight:     equ 5
.empty3:        equ 7
.backgrmapblock:equ 8
.empty4:        equ 9
.backgrmapaddr: equ 11
.grpxblck:      equ 13
.grpxaddr:      equ 14


lenghtmapdata:  equ 16


endmapdata:
