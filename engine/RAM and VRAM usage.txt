RAM:
engine:                   $3e   - $3fff   RAM
enginepage3:              $c000 - $dfff   RAM
map data:                 $4000 - $afff   RAM
loader:                   $8000 - $bfff   ROM
movement patterns enemies:$8000 - $bfff   ROM
enemy sprites:            $4000 - $bfff   ROM

VRAM:
screen5 usage:
page0:                    $00000 - $07fff
page1:                    $08000 - $0ffff
page2:                    $10000 - $17fff
page3:                    $18000 - $1ffff

;sprites
sprcoltableaddress:       $17400  at end of page 2
spratttableaddress:       $17600
sprchatableaddress:       $17800
invissprcoltableaddress:  $16c00
invisspratttableaddress:  $16e00
invissprchatableaddress:  $17000
;/sprites

;grapx
character pattern table:  $0000 - $17FF
1st name table:	          $1800 - $1AFF
2nd name table:	          $1C00 - $1EFF
color table:              $2000 - $37FF

2nd:
character pattern table:  $4000 - $57FF
color table:              $6000 - $77FF
3d:
character pattern table:  $8000 - $97FF
color table:              $a000 - $b7FF
4th:
character pattern table:  $c000 - $d7FF
color table:              $e000 - $f7FF
;/grapx

pattern name table = 768 bytes

mode pattern names are 8 bits in lenght, a maximum of 
256 pattern definitions may be addressed

the screen:
3 parts of 256 pattern positions each

pattern generator table:
3 blocks of 2048 (256*8)bytes each

color table:
3 blocks of 2048 (256*8)bytes each

color table address is controlled by loading MSB of color
base in VDP Register 3. (LSB must be set to all 1s)

M1=0 M2=0 M3=1 screen 2

Static romprogblock for objectmovement
romprogblock2
variable romprogblocks
romprogblock3 =handleobjectmovement
romprogblock4 =handleobjectmovement2
romprogblock5 =handleobjectmovement3



